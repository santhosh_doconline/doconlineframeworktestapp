////
////  CallSessionViewController.swift
////  DocOnline
////
////  Created by Kiran Kumar on 20/12/17.
////  Copyright © 2017 ConversionBug. All rights reserved.
////
//
//import UIKit
//import CallKit
//import OpenTok
//
//class CallSessionViewController: UIViewController {
//
//    
//    @IBOutlet var vw_publisher: UIView!
//    
//    @IBOutlet var bt_video_enable: ControlButton!
//    @IBOutlet var bt_mic: ControlButton!
//    @IBOutlet var bt_end_call: UIButton!
//    
//   // let callController = CXCallController()
//    
//    var audioEnabled = false
//    var detectAudio : Timer?
//    var publishedState = false
//    
//    // MARK: Actions
//   
////    func end(uuid: UUID) {
////        let endCallAction = CXEndCallAction(call: uuid)
////        let transaction = CXTransaction()
////        transaction.addAction(endCallAction)
////        
////        requestTransaction(transaction, action: "endCall")
////    }
////    
////    func setHeld(call: UUID, onHold: Bool) {
////        let setHeldCallAction = CXSetHeldCallAction(call: call, onHold: onHold)
////        let transaction = CXTransaction()
////        transaction.addAction(setHeldCallAction)
////        
////        requestTransaction(transaction, action: "holdCall")
////    }
////    
////    private func requestTransaction(_ transaction: CXTransaction, action: String = "") {
////        callController.request(transaction) { error in
////            if let error = error {
////                print("Error requesting transaction: \(error)")
////            } else {
////                print("Requested transaction \(action) successfully")
////            }
////        }
////    }
//
//    
//    // MARK: Actions
//    var session: OTSession?
//    ///OpenTok publisher object reference declaration with settings
//    lazy var publisher: OTPublisher! = {
//        let settings = OTPublisherSettings()
//        settings.name = UIDevice.current.name
//        return OTPublisher(delegate: self, settings: settings)!
//    }()
//    var subscriber: OTSubscriber?
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
////        AppDelegate.shared.providerDelegate?.callSessionDelegate = self
//        
//        self.connectToOpenTok()
//    }
//    
//    func connectToOpenTok() {
//        OTAudioDeviceManager.setAudioDevice(OTDefaultAudioDevice.sharedInstance())
//        
//        if session == nil {
//            session = OTSession(apiKey: pApiKey, sessionId: pSessionID, delegate: self)
//        }
//        
//        var error: OTError?
//        session?.connect(withToken: pTokenId, error: &error)
//        if error != nil {
//            print(error!)
//        }
//        print("Session connected")
//      //  self.detectAudio = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkAudioEnabled), userInfo: nil, repeats: true)
//    }
//
//   @objc func checkAudioEnabled() {
//        if self.audioEnabled && !self.publishedState{
//            startAudio()
//            if self.detectAudio != nil {
//                self.detectAudio?.invalidate()
//                self.detectAudio = nil
//            }
//        }
//    }
//    
//    func startAudio() {
//        var error: OTError?
//        session?.publish(publisher, error: &error)
//        if error != nil {
//            print(error!)
//        }
//        
//        guard let publisherView = publisher?.view else {
//            return
//        }
//        //  let screenBounds = UIScreen.main.bounds
//        self.publishedState = true
//        publisherView.frame =  self.vw_publisher.bounds
//        self.vw_publisher.addSubview(publisherView)
//    }
//   
//    
//    func endCall() {
//        /*
//         Simulate the end taking effect immediately, since
//         the example app is not backed by a real network service
//         */
//        if let publisher = publisher {
//            var error: OTError?
//            session?.unpublish(publisher, error: &error)
//            if error != nil {
//                print(error!)
//            }
//        }
//        publisher = nil
//        
//        if let session = session {
//            var error: OTError?
//            session.disconnect(&error)
//            if error != nil {
//                print(error!)
//            }
//        }
//        session = nil
//    }
//    
//    @IBAction func endCallTapped(_ sender: UIButton) {
//        
//    }
//    
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
//
//extension CallSessionViewController : CallKitSessionDelegates {
//    func didReset(uuid: UUID) {
//        print("*********\n \(#function) tapped from callSession view\n*********")
//        endCall()
//        end(uuid:calluuid!)
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func didUserEndCall(uuid: UUID) {
//         print("*********\n \(#function) tapped from callSession view\n*********")
//        endCall()
//         end(uuid:calluuid!)
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func didUserAnsweredCall(uuid: UUID) {
//         print("*********\n \(#function) tapped from callSession view\n*********")
//    }
//    
//    func didAudioSessionIsActivated(uuid: UUID) {
//         print("*********\n \(#function) tapped from callSession view\n*********")
//        startAudio()
//    }
//    
//    func didAudioSessionIsDeactivated(uuid: UUID) {
//         print("*********\n \(#function) tapped from callSession view\n*********")
//    }
//    
//    func didUserMutedAudio(uuid: UUID, isMuted: Bool) {
//         print("*********\n \(#function) tapped from callSession view\n*********")
//         publisher?.publishAudio = isMuted
//    }
//    
//    func didUserKeptCallOnHold(uuid: UUID, isKeptOnHold: Bool) {
//         print("*********\n \(#function) tapped from callSession view\n*********")
//          publisher?.publishAudio = isKeptOnHold
//        setHeld(call:calluuid! , onHold: isKeptOnHold)
//    }
//}
//
//// MARK: - OTSessionDelegate callbacks
//extension CallSessionViewController: OTSessionDelegate {
//    func sessionDidConnect(_ session: OTSession) {
//        print("The client connected to the OpenTok session.")
//        
//    }
//    
//    func sessionDidDisconnect(_ session: OTSession) {
//        print("The client disconnected from the OpenTok session.")
//    }
//    
//    func session(_ session: OTSession, didFailWithError error: OTError) {
//        print("The client failed to connect to the OpenTok session: \(error).")
//    }
//    func session(_ session: OTSession, streamCreated stream: OTStream) {
//        print("A stream was created in the session.")
//        
//        subscriber = OTSubscriber(stream: stream, delegate: self)
//        guard let subscriber = subscriber else {
//            return
//        }
//        
//        var error: OTError?
//        session.subscribe(subscriber, error: &error)
//        guard error == nil else {
//            print(error!)
//            return
//        }
//        
//        guard let subscriberView = subscriber.view else {
//            return
//        }
//        subscriberView.frame = UIScreen.main.bounds
//        view.insertSubview(subscriberView, at: 0)
//       // publisher?.publishVideo = true
//    }
//    
//    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
//        print("A stream was destroyed in the session.")
//        endCall()
//        end(uuid:calluuid!)
//        self.dismiss(animated: true, completion: nil)
//    }
//}
//
//// MARK: - OTPublisherDelegate callbacks
//extension CallSessionViewController: OTPublisherDelegate {
//    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
//        print("The publisher failed: \(error)")
//    }
//    
//    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
//        print("The publisher stream created")
//    }
//    
//    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
//        print("Publisher stream destroyed")
//    }
//}
//
//// MARK: - OTSubscriberDelegate callbacks
//extension CallSessionViewController: OTSubscriberDelegate {
//    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
//        print("The subscriber did connect to the stream.")
//    }
//    
//    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
//        print("The subscriber failed to connect to the stream.")
//    }
//}
//
//
//
//
