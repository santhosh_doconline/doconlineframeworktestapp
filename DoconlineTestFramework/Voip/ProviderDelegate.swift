/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	CallKit provider delegate class, which conforms to CXProviderDelegate protocol
*/

import Foundation
import UIKit
import CallKit
import AVFoundation
import OpenTok
//import Firebase

var calluuid : UUID?
//var pApiKey = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.OPENTOK_KEY).stringValue!
var pApiKey = "45750702"

var pTokenId = "T1==cGFydG5lcl9pZD00NTc1MDcwMiZzaWc9ZjRhYmE3N2U3YTNmZGJlYmQ5ZWFlZjhkOTZmZjJjOWI3NDE1Y2RhMjpzZXNzaW9uX2lkPTFfTVg0ME5UYzFNRGN3TW41LU1UVXhORFV6TURBeE56VXlNMzVEVGxFNU5HZDVWMlI1WVdwVFIybHpabFZIWVRkWGEyZC1VSDQmY3JlYXRlX3RpbWU9MTUxNDUzMDAyNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNTE0NTMwMDI0LjQ2MjcxMTA0MDQ1MzI5JmNvbm5lY3Rpb25fZGF0YT0lN0IlMjJhcHBvaW50bWVudF9pZCUyMiUzQTEwNiU3RA=="
var pSessionID = "1_MX40NTc1MDcwMn5-MTUxNDUzMDAxNzUyM35DTlE5NGd5V2R5YWpTR2lzZlVHYTdXa2d-UH4"



protocol CallKitSessionDelegates {
    func didUserAnsweredCall(uuid:UUID)
    func didUserEndCall(uuid:UUID)
    func didAudioSessionIsActivated(uuid:UUID)
    func didAudioSessionIsDeactivated(uuid:UUID)
    func didReset(uuid:UUID)
    func didUserKeptCallOnHold(uuid:UUID,isKeptOnHold:Bool)
    func didUserMutedAudio(uuid:UUID,isMuted:Bool)
}


 final class ProviderDelegate: NSObject, CXProviderDelegate {

    let callManager: SpeakerboxCallManager
      let provider: CXProvider

    var callSessionDelegate : CallKitSessionDelegates?
    
    var toEndCallAfterTimeout : SpeakerboxCall?
    var ringingTimer : Timer!
    
    
     init(callManager: SpeakerboxCallManager) {
       self.callManager = callManager
        provider = CXProvider(configuration: type(of: self).providerConfiguration)
        
        super.init()
        
        provider.setDelegate(self, queue: nil)
    }

    /// The app's provider configuration, representing its CallKit capabilities
    static var providerConfiguration: CXProviderConfiguration {
        let localizedName = NSLocalizedString("DocOnline", comment: "DocOnline") //incomingCallType == 1 ? "Audio" : "Video"
        let providerConfiguration = CXProviderConfiguration(localizedName: localizedName)

        providerConfiguration.supportsVideo = incomingCallType == 1 ? false : true

        providerConfiguration.maximumCallsPerCallGroup = 1
        
        providerConfiguration.supportedHandleTypes = [.generic]

        providerConfiguration.iconTemplateImageData = #imageLiteral(resourceName: "Doctor").pngData()

        providerConfiguration.ringtoneSound = "ringtone.caf"
        
        return providerConfiguration
    }

    // MARK: Incoming Calls

    /// Use CXProvider to report the incoming call to the system
    func reportIncomingCall(uuid: UUID, handle: String, hasVideo: Bool = false, completion: ((NSError?) -> Void)? = nil) {
        // Construct a CXCallUpdate describing the incoming call, including the caller.
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: handle)
        update.supportsGrouping = false  //Newly added
        update.supportsHolding = false   //Newly added
        update.hasVideo = hasVideo
        NSLog("report incoming call-provider delegate")
        // pre-heat the AVAudioSession
        
        OTDefaultAudioDevice.sharedInstance().typeOfCall = incomingCallType == 1 ? "audio" : "video"
        OTAudioDeviceManager.setAudioDevice(OTDefaultAudioDevice.sharedInstance())
        
        // Report the incoming call to the system
        provider.reportNewIncomingCall(with: uuid, update: update) { error in
            /*
                Only add incoming call to the app's list of calls if the call was allowed (i.e. there was no error)
                since calls may be "denied" for various legitimate reasons. See CXErrorCodeIncomingCallError.
             */
            if error == nil {
                let call = SpeakerboxCall(uuid: uuid)
                call.handle = handle
               self.toEndCallAfterTimeout = SpeakerboxCall(uuid: uuid)
               self.callManager.addCall(call)

               // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"CallEndTapped"), object: nil)
              //  NotificationCenter.default.addObserver(self, selector: #selector(self.timoutEndCall), name: NSNotification.Name(rawValue:"CallEndTapped"), object: nil)
                print("Reporting incoming call view :\(#function)")
                
            }
            
            completion?(error as NSError?)
        }
    }
    
    @objc func timoutEndCall() {
        print("End call it's time out")
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                
                if  self.ringingTimer != nil {
                    self.ringingTimer.invalidate()
                    self.ringingTimer = nil
                }
            }
        }else {
            print("calls array is emtpy")
        }
    }

    // MARK: CXProviderDelegate

     func providerDidBegin(_ provider: CXProvider) {
        print("\(#function) called")
     //  self.ringingTimer = Timer.scheduledTimer(timeInterval: 35, target: self, selector: #selector(self.timoutEndCall), userInfo: nil, repeats: false)
        NSLog("providerdidbegin")
    }
    
     func providerDidReset(_ provider: CXProvider) {
        print("Provider did reset")
        NSLog("providerdidreset")

        /*
            End any ongoing calls if the provider resets, and remove them from the app's list of calls,
            since they are no longer valid.
        */
        
        for call in callManager.calls {
            call.endCall()
        }

        // Remove all calls from the app's list of calls.
       callManager.removeAllCalls()
      //  self.callSessionDelegate?.didReset(uuid: calluuid!)
        //speaker-End
    }

//    var outgoingCall: SpeakerboxCall?
//    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
//        // Create & configure an instance of SpeakerboxCall, the app's model class representing the new outgoing call.
//        let call = SpeakerboxCall(uuid: action.callUUID, isOutgoing: true)
//        call.handle = action.handle.value
//
//        /*
//            Configure the audio session, but do not start call audio here, since it must be done once
//            the audio session has been activated by the system after having its priority elevated.
//         */
//        // https://forums.developer.apple.com/thread/64544
//        // we can't configure the audio session here for the case of launching it from locked screen
//        // instead, we have to pre-heat the AVAudioSession by configuring as early as possible, didActivate do not get called otherwise
//        // please look for  * pre-heat the AVAudioSession *
//        configureAudioSession()
//        /*
//            Set callback blocks for significant events in the call's lifecycle, so that the CXProvider may be updated
//            to reflect the updated state.
//         */
//
//        call.hasStartedConnectingDidChange = { [weak self] in
//            self?.provider.reportOutgoingCall(with: call.uuid, startedConnectingAt: call.connectingDate)
//        }
//        call.hasConnectedDidChange = { [weak self] in
//            self?.provider.reportOutgoingCall(with: call.uuid, connectedAt: call.connectDate)
//        }
//
//        self.outgoingCall = call
//
//        // Signal to the system that the action has been successfully performed.
//        action.fulfill()
//    }

    var answerCall: SpeakerboxCall?
     func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        // Retrieve the SpeakerboxCall instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        if  self.ringingTimer != nil {
            self.ringingTimer.invalidate()
            self.ringingTimer = nil
        }
        
        if AppConfig.shared?.callRefreshTimer != nil {
            AppConfig.shared?.callRefreshTimer?.invalidate()
            AppConfig.shared?.callRefreshTimer = nil
        }
        
        /*
            Configure the audio session, but do not start call audio here, since it must be done once
            the audio session has been activated by the system after having its priority elevated.
         */
        
        // https://forums.developer.apple.com/thread/64544
        // we can't configure the audio session here for the case of launching it from locked screen
        // instead, we have to pre-heat the AVAudioSession by configuring as early as possible, didActivate do not get called otherwise
        // please look for  * pre-heat the AVAudioSession *
        configureAudioSession()

        self.answerCall = call
        
        var topController : UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!
        }
        NSLog("presenting callconnecting vc")
        let storyboard = UIStoryboard(name: "MainSto", bundle: Bundle(for: CallConnectingViewController.self))
        let view = storyboard.instantiateViewController(withIdentifier: "CallView") as! CallConnectingViewController
//        self.present(vc, animated: true, completion: nil)

//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)  //CallSession //CallView
//        let view = storyBoard.instantiateViewController(withIdentifier: "CallView") as! CallConnectingViewController
        view.callManager = self.callManager
        view.answerAction = action
        
        topController.present(view, animated: true) {
            isCallingViewPresenting = true
            //self.callSessionDelegate?.didUserAnsweredCall(uuid: calluuid!)
        }
        
        let queue1 = DispatchQueue(label: "com.doconline.doconline.callqueue2", qos: DispatchQoS.background)
        queue1.async {
            ///call accepted send status to server
            CallStatus.sharedInstance.perfromCallRecievedRequest(callStatus: CallingStatus.CALL_STATUS_PATIENT_ACCEPTED)
        }
        
        // Signal to the system that the action has been successfully performed.
        action.fulfill()
    }


    
     func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        // Retrieve the SpeakerboxCall instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        if AppConfig.shared?.callRefreshTimer != nil {
            AppConfig.shared?.callRefreshTimer?.invalidate()
            AppConfig.shared?.callRefreshTimer = nil
        }
        
        self.callSessionDelegate?.didUserEndCall(uuid: call.uuid)
        
        let queue1 = DispatchQueue(label: "com.doconline.doconline.callqueue3", qos: DispatchQoS.background)
        queue1.async {
            ///call eneded send status to server
            CallStatus.sharedInstance.perfromCallRecievedRequest(callStatus: CallingStatus.CALL_STATUS_PATIENT_REJECTED)
        }
        
        isCallingViewPresenting = false
        // Trigger the call to be ended via the underlying network service.
        call.endCall()
        //self.callSessionDelegate?.didUserEndCall(uuid:  calluuid!)
        // Signal to the system that the action has been successfully performed.
        action.fulfill()
        print("User declined the call")
       // Remove the ended call from the app's list of calls.
       // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"CallEndTapped"), object: nil)
       
        callManager.removeCall(call)
    }

     func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        // Retrieve the SpeakerboxCall instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        print("\(#function) called")
        // Update the SpeakerboxCall's underlying hold state.
        call.isOnHold = action.isOnHold

        // Stop or start audio in response to holding or unholding the call.
        call.isMuted = call.isOnHold

       // self.callSessionDelegate?.didUserKeptCallOnHold(uuid:  calluuid!, isKeptOnHold: action.isOnHold)
        // Signal to the system that the action has been successfully performed.
        action.fulfill()
    }
    
     func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        // Retrieve the SpeakerboxCall instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        call.isMuted = action.isMuted
        
        self.callSessionDelegate?.didUserMutedAudio(uuid:  call.uuid, isMuted: action.isMuted)
        
        // Signal to the system that the action has been successfully performed.
        action.fulfill()
    }

     func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        print("Timed out \(#function)")

        // React to the action timeout if necessary, such as showing an error UI.
        action.fulfill()
    }

    
    
     func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print("Received audio session \(#function)")
        NSLog("provider-didactive")
        // Start call audio media, now that the audio session has been activated after having its priority boosted.
       
     //   self.callSessionDelegate?.didAudioSessionIsActivated(uuid:  calluuid!)
//        outgoingCall?.startCall(withAudioSession: audioSession) { success in
//            if success {
//                self.callManager.addCall(self.outgoingCall!)
//                self.outgoingCall?.startAudio()
//            }
//        }

        answerCall?.answerCall(withAudioSession: audioSession) { success in
            if success {
                self.answerCall?.startAudio()
            }
        }
    }

     func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        print("Received \(#function)")

        /*
             Restart any non-call related audio now that the app's audio session has been
             de-activated after having its priority restored to normal.
         */
     //   outgoingCall?.endCall()
      //  outgoingCall = nil
        answerCall?.endCall()
        answerCall = nil
        callManager.removeAllCalls()
       // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"CallEndTapped"), object: nil)
    }
    
    func configureAudioSession() {
        // See https://forums.developer.apple.com/thread/64544
        let session = AVAudioSession.sharedInstance()
        try? session.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord)), mode: .videoChat)
        try? session.setMode(AVAudioSession.Mode.voiceChat)
        try? session.setPreferredSampleRate(44100.0)
        try? session.setPreferredIOBufferDuration(0.005)
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
