/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	Model class representing a single call
*/

import Foundation
import OpenTok
import SwiftMessages
import CallKit

protocol SpeakerboxCallDelegate {
    func sessionConnected(session: OTSession)
    func sessionDidDisConnected(session: OTSession)
    func subscriberJoined(subscriber:OTSubscriber,stream:OTStream)
    func subscriberVideoDisabled(subscriber:OTSubscriberKit)
    func subscriberVideoEnabled(subscriber:OTSubscriberKit)
    func sessionHasAlreadyAUser(message:String)
    func subscriberDidConnect(subscriber:OTSubscriberKit)

    
}

final class SpeakerboxCall: NSObject {


    // MARK: Metadata Properties

    let uuid: UUID
    let isOutgoing: Bool
    var handle: String?

    var speakerDelegate : SpeakerboxCallDelegate?
    
    
    
//    let callController = CXCallController()
//
//    func endNative(call: UUID) {
//
//        let endCallAction = CXEndCallAction(call: call)
//        let transaction = CXTransaction()
//        transaction.addAction(endCallAction)
//
//        requestTransaction(transaction, action: "endCall")
//    }
//
//    private func requestTransaction(_ transaction: CXTransaction, action: String = "") {
//        callController.request(transaction) { error in
//            if let error = error {
//                print("Error requesting transaction: \(error)")
//            } else {
//                print("Requested transaction \(action) successfully")
//            }
//        }
//    }
    
    
    // MARK: Call State Properties

    var connectingDate: Date? {
        didSet {
            stateDidChange?()
            hasStartedConnectingDidChange?()
        }
    }
    var connectDate: Date? {
        didSet {
            stateDidChange?()
            hasConnectedDidChange?()
        }
    }
    var endDate: Date? {
        didSet {
            stateDidChange?()
            hasEndedDidChange?()
        }
    }
    var isOnHold = false {
        didSet {
            stateDidChange?()
        }
    }
    
    var isMuted = false {
        didSet {
            publisher?.publishAudio = !isMuted
        }
    }

    // MARK: State change callback blocks

    var stateDidChange: (() -> Void)?
    var hasStartedConnectingDidChange: (() -> Void)?
    var hasConnectedDidChange: (() -> Void)?
    var hasEndedDidChange: (() -> Void)?
    var audioChange: (() -> Void)?

    // MARK: Derived Properties

    var hasStartedConnecting: Bool {
        get {
            return connectingDate != nil
        }
        set {
            connectingDate = newValue ? Date() : nil
        }
    }
    var hasConnected: Bool {
        get {
            return connectDate != nil
        }
        set {
            connectDate = newValue ? Date() : nil
        }
    }
    var hasEnded: Bool {
        get {
            return endDate != nil
        }
        set {
            endDate = newValue ? Date() : nil
        }
    }
    var duration: TimeInterval {
        guard let connectDate = connectDate else {
            return 0
        }

        return Date().timeIntervalSince(connectDate)
    }

    // MARK: Initialization

    init(uuid: UUID, isOutgoing: Bool = false) {
        self.uuid = uuid
        self.isOutgoing = isOutgoing
        NSLog("speakerbox call init")
    }

    // MARK: Actions
    var session: OTSession?
    var publisher: OTPublisher?
    var subscriber: OTSubscriber?
    
    var startCallCompletion: ((Bool) -> Void)?
    func startCall(withAudioSession audioSession: AVAudioSession, completion: ((_ success: Bool) -> Void)?) {
        
        OTDefaultAudioDevice.sharedInstance(with: audioSession).typeOfCall = incomingCallType == 1 ? "audio" : "video"
        OTAudioDeviceManager.setAudioDevice(OTDefaultAudioDevice.sharedInstance(with: audioSession))
        NSLog("startcall-speakerboxcall")
        if session == nil {
            session = OTSession(apiKey: pApiKey, sessionId: pSessionID, delegate: self)
            
        }
        if session == nil{
            NSLog("inside start call-OTSession not created")
        }else{
            NSLog("inside start call-OTSession-OTSession created")
        }
//        assert(session == nil, "session not created")
        
        startCallCompletion = completion
        
        var error: OTError?
        hasStartedConnecting = true
        session?.connect(withToken: pTokenId, error: &error)
        if error != nil {
            print(error!)
//            assert(error != nil, "session unable to connect")
//            UIApplication.topViewController()?.showAlert(msg: "inside start call-OTSession-error")
            NSLog("session connect error")
        }else{
            NSLog("session connect no error")
        }
    }
    
    var answerCallCompletion: ((Bool) -> Void)?
    func answerCall(withAudioSession audioSession: AVAudioSession, completion: ((_ success: Bool) -> Void)?) {
        
        OTDefaultAudioDevice.sharedInstance(with: audioSession).typeOfCall = incomingCallType == 1 ? "audio" : "video"
        OTAudioDeviceManager.setAudioDevice(OTDefaultAudioDevice.sharedInstance(with: audioSession))
        
        
        NSLog("Session check url:\(AppURLS.URL_CONSULTATION_CALL_CHECK + callSessionID)")
        
        NetworkCall.performGet(url: AppURLS.URL_CONSULTATION_CALL_CHECK + callSessionID) { (success, response, status, error) in
            
            if status == 202 {
                
                if self.session == nil {
                    self.session = OTSession(apiKey: pApiKey, sessionId: callSessionID, delegate: self)
                }
                
                if self.session == nil{
                    NSLog("inside answer call-OTSession not created")
                }else{
                    NSLog("inside answer call-OTSession-OTSession created")
                }
                
                self.answerCallCompletion = completion
                
                var error: OTError?
                self.hasStartedConnecting = true
                self.session?.connect(withToken: callTokenid, error: &error)
                if error != nil {
                    print("Error connecting to opentok:\(error!)")
//                    assert(error != nil, "opentok answer call error")
                    NSLog("inside answer call-OTSession-error")
                }
                
//                let queue1 = DispatchQueue(label: "com.doconline.doconline.callqueue1", qos: DispatchQoS.background)
//                queue1.async {
//                    ///call accepted send status to server
//                    CallStatus.sharedInstance.perfromCallRecievedRequest(callStatus: CallingStatus.CALL_STATUS_PATIENT_ACCEPTED)
//                }
            }else {
                var message = ""
                if let data = response {
                    if let mesg = data.object(forKey: Keys.KEY_MESSAGE ) as? String {
                        message = mesg
                        NSLog("Message from server:\(message)")
                    }
                }
                
                DispatchQueue.main.async {
                  // self.showStatusLine(color: .red, message: message, duration: .seconds(seconds: 3))
                   self.speakerDelegate?.sessionHasAlreadyAUser(message: message)
                    
                    if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
                        for call in calls {
                            AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                             isCallingViewPresenting = false
                            // call.endCall()
                        }
                    }
                }
            }
            
            if let err = error {
                
                print("Error:=>\(err.localizedDescription)")
            }
        }
    }
    
    ///Ends the current runnig callkit
    func endCallkit() {
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                isCallingViewPresenting = false
                // call.endCall()
            }
        }
    }
    
    /**
     Method shows status line message when view loads
     */
    func showStatusLine(color:UIColor,message:String , duration : SwiftMessages.Duration) {
        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
        status.backgroundView.backgroundColor = color
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: message)
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.duration = duration
        statusConfig.presentationContext = .window(windowLevel: .statusBar)
        SwiftMessages.show(config: statusConfig, view: status)
    }
    
    func startAudio() {
        if publisher == nil {
            let settings = OTPublisherSettings()
            settings.name = UIDevice.current.name
            settings.audioTrack = true
            settings.videoTrack = incomingCallType == 1 ? false : true   //changed
            publisher = OTPublisher.init(delegate: self, settings: settings)
            publisher?.networkStatsDelegate = self
        }
        
        var error: OTError?
        session?.publish(publisher!, error: &error)
        if error != nil {
            print(error!)
        }
        
        print("*********\nAudio started\nPublisher created:\(publisher == nil ? true : false)\n*********")
    }
    
    func endCall() {
        /*
         Simulate the end taking effect immediately, since
         the example app is not backed by a real network service
         */
        
        if let publisher = publisher {
            var error: OTError?
            session?.unpublish(publisher, error: &error)
            if error != nil {
                print(error!)
            }

            if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
                for call in calls {
                    AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                    // call.endCall()
                }
            }
           // print("End Native call from end call :\(AppDelegate.shared.currentCallUUID!)")
        
        }
        publisher = nil
        
        if let session = session {
            var error: OTError?
            session.disconnect(&error)
            if error != nil {
                print(error!)
            }

            self.endCallkit()
        }
        session = nil
        
        hasEnded = true
    }
}

extension SpeakerboxCall: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print(#function)
        NSLog("session did connect")
        hasConnected = true
        startCallCompletion?(true)
        answerCallCompletion?(true)
        speakerDelegate?.sessionConnected(session: session)
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print(#function)
        print("End Native call from \(#function) uuid:\(AppConfig.shared?.currentCallUUID!)")
      //  endNative(call: AppDelegate.shared.currentCallUUID!)
        
        speakerDelegate?.sessionDidDisConnected(session: session)
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print(#function, error)
         speakerDelegate?.sessionDidDisConnected(session: session)
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print(#function)
        NSLog("session stream connect")
        subscriber = OTSubscriber.init(stream: stream, delegate: self)
        subscriber?.networkStatsDelegate = self
        subscriber?.subscribeToVideo = true
        if let subscriber = subscriber {
            var error: OTError?
            session.subscribe(subscriber, error: &error)
            if error != nil {
                print(error!)
            }else {
                speakerDelegate?.subscriberJoined(subscriber: subscriber, stream: stream)
            }
        }
        
    }

    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("\(#function) call UUID :\(AppConfig.shared?.currentCallUUID)")
       
//        if let calls  = AppDelegate.shared.providerDelegate?.callManager.calls {
//            for call in calls {
//                AppDelegate.shared.providerDelegate?.callManager.end(call: call)
//            }
//        }
        endCall()
        print("End Native call from \(#function) uuid:\(AppConfig.shared?.currentCallUUID!)")
      //  endNative(call: AppDelegate.shared.currentCallUUID!)
        
        speakerDelegate?.sessionDidDisConnected(session: session)
    }
    
    func sessionDidBeginReconnecting(_ session: OTSession) {
        showStatusLine(color: UIColor.red, message: "Re-Connecting...", duration: .forever)
    }
    
    func sessionDidReconnect(_ session: OTSession) {
        DispatchQueue.main.async {
            SwiftMessages.hide()
        }
    }
}

extension SpeakerboxCall: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print(#function)
    }
    
    func publisher(_ publisher: OTPublisher, didChangeCameraPosition position: AVCaptureDevice.Position) {
        if position == .front {
            print("changed to front")
        }else if position == .back {
            print("changed to back")
        }
    }
}

extension SpeakerboxCall: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print(#function)
        NSLog("subscriber did connect")
        speakerDelegate?.subscriberDidConnect(subscriber: subscriber)
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print(#function)
    }
    
    func subscriberVideoEnabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        print("video enabled**")
        speakerDelegate?.subscriberVideoEnabled(subscriber: subscriber)
    }
    
    func subscriberVideoDisabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        print("video disabled**")
        speakerDelegate?.subscriberVideoDisabled(subscriber: subscriber)
    }
    
    func subscriberDidReconnect(toStream subscriber: OTSubscriberKit) {
        //self.stopAnimating()
        SwiftMessages.hide()
    }
}

extension SpeakerboxCall : OTSubscriberKitNetworkStatsDelegate {
    
    func subscriber(_ subscriber: OTSubscriberKit, videoNetworkStatsUpdated stats: OTSubscriberKitVideoNetworkStats) {
        print("videoNetworkStatsUpdated in SB")
        
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, audioNetworkStatsUpdated stats: OTSubscriberKitAudioNetworkStats) {
        print("audioNetworkStatsUpdated in SB")
    }
}

extension SpeakerboxCall : OTPublisherKitNetworkStatsDelegate {
    func publisher(_ publisher: OTPublisherKit, audioNetworkStatsUpdated stats: [OTPublisherKitAudioNetworkStats]) {
        print("Publisher audioNetworkStatsUpdated in SB")
    }
    
    func publisher(_ publisher: OTPublisherKit, videoNetworkStatsUpdated stats: [OTPublisherKitVideoNetworkStats]) {
        print("Publisher videoNetworkStatsUpdated in SB")
    }
}
