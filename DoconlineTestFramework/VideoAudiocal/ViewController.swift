//
//  ViewController.swift
//  DocOnline
//
//  Created by dev-3 on 08/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import OpenTok
import AVFoundation
import UserNotifications
import SwiftMessages
//import Firebase



// Replace with your OpenTok API key
//var kApiKey =  RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.OPENTOK_KEY).stringValue!
var kApiKey = "45750702"
// Replace with your generated session ID
var kSessionId =  callSessionID
// Replace with your generated token
var kToken = callTokenid

let TIME_WINDOW = 1000 // 15 seconds
let AUDIO_ONLY_TEST_DURATION = 10000 // 10 seconds

class ControlButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.size.width / 2
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
    }
}

///**Deprecated** class : initially used for video and audio call
class ViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var topConnectingView: UIView!
    @IBOutlet weak var endCallOutlet: UIButton!
   
    @IBOutlet weak var bt_message: ControlButton!
    @IBOutlet weak var bt_video: ControlButton!
    @IBOutlet weak var bt_mic: ControlButton!
    @IBOutlet weak var bt_flip_camera: ControlButton!
    @IBOutlet weak var bt_speaker_enable: ControlButton!
    @IBOutlet weak var bt_connecting_call_end: UIButton!
    
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var connectingView : UIView!
    @IBOutlet weak var topConnectingViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var activityView: NVActivityIndicatorView!
    @IBOutlet weak var lb_timer_display: UILabel!
    
    @IBOutlet weak var lb_audio_call_caller_name: UILabel!
    
   // let customAudioDevice = DefaultAudioDevice.sharedInstance
    
    ///custom audio device shared instance reference declaration
    let audioDevice : OTDefaultAudioDevice = OTDefaultAudioDevice.sharedInstance()
    
    ///OpenTok session object reference declaration with key,session id
    lazy var session: OTSession! = {
        return OTSession(apiKey: kApiKey, sessionId: callSessionID, delegate: self)!
    }()
    
    ///checks the session has video
    var hasVideo = true
    ///check the session has audio
    var hasAudio = true
    ///Boolean var for speaker enabled or not for aduio call
    var isSpeakerEnabledForAudioCall = false
    ///boolean value for checking rear camera is on or not
    var isBackCameraOn = false
   
    ///OpenTok publisher object reference declaration with settings
    lazy var publisher: OTPublisher! = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    
    /// Subscriber object reference
    var subscriberMain: OTSubscriber!
    ///avatar image reference
    var avatarImage : UIImageView!
    
    //incomingCall
    @IBOutlet weak var incomingProfilePic: UIImageView!
    @IBOutlet weak var callerName: UILabel!
    @IBOutlet weak var callerDescription: UILabel!
    @IBOutlet weak var declineOutlet: UIButton!
    @IBOutlet weak var declineLabel: UILabel!
    @IBOutlet weak var acceptButtonOutlet: UIButton!
    @IBOutlet weak var acceptLabel: UILabel!
    @IBOutlet weak var acceptImageView: UIImageView!
    @IBOutlet weak var lb_network_connectivity_status: UILabel!
    
    @IBOutlet weak var vw_subscriber: UIView!
    @IBOutlet weak var vw_publisher: UIView!
    
    var sessionID = ""
    var tokenID = ""
    
    var bookedfor : Int!
    var calltype  : Int!
    var scheduledat = ""
    var attachments = [String]()
    
    ///call timer reference
    var inCallTimer: Timer?
    ///call timer total seconds
    var totalSeconds : Int = 0

    ///audio player object reference
    var audioPlayer = AVAudioPlayer()
    ///call ringing duration
//    var callRingingDuration = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.RINGING_DURATION).stringValue!
    var callRingingDuration = "30"

    ///call ringing timer reference
    var ringingTimer : Timer!
    ///ringing seconds
    var ringSeconds : Int = 0
    
    ///Opentok session connect timer reference
    var connectionTimer : Timer!
    ///ringing seconds
    var sessionConnectSeconds : Int = 0
    
    
    //MARK:- Network Stats variables
    var prevVideoTimestamp : Double = 0
    var prevVideoBytes : UInt64 = 0
    var prevAudioTimestamp : Double = 0
    var prevAudioBytes : UInt64 = 0
    var prevVideoPacketsLost : UInt64 = 0
    var prevVideoPacketsRcvd : UInt64 = 0
    var prevAudioPacketsLost : UInt64 = 0
    var prevAudioPacketsRcvd : UInt64 = 0
    var video_bw : CLong = 0
    var audio_bw : CLong = 0
    var video_pl_ratio : Double = -1
    var audio_pl_ratio : Double = -1
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if incomingCallType == 2 {
            audioDevice.typeOfCall = "video"
            OTAudioDeviceManager.setAudioDevice(audioDevice)
            UIDevice.current.isProximityMonitoringEnabled = false
            self.lb_timer_display.textColor = UIColor.white
            UIApplication.shared.isIdleTimerDisabled = true
        }else if incomingCallType == 1{
            UIDevice.current.isProximityMonitoringEnabled = true
            audioDevice.typeOfCall = "audio"
            OTAudioDeviceManager.setAudioDevice(audioDevice)
            self.lb_timer_display.textColor = UIColor.black
        }

       
        isCallingViewPresenting = true
        ///have to check when voip notification
        self.topConnectingView.isHidden = false
        print("Session id:\(sessionID) token id:\(tokenID)")
        
        hideViews(hide:false)
        
        endCallOutlet.isEnabled = true
        endCallOutlet.layer.cornerRadius = endCallOutlet.bounds.width / 2
        
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
       // UNUserNotificationCenter.current().delegate = self
        runCallRingingTimer()
        
        
        NotificationCenter.default.addObserver(forName: UIDevice.proximityStateDidChangeNotification, object: nil, queue: OperationQueue.main) { (notification) in
            print("The proximity sensor :\(UIDevice.current.proximityState ? "will now blank the screen" : "will now restore the screen")");
        }
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.batteryLevelChanged(notification:)), name: NSNotification.Name.UIDeviceBatteryLevelDidChange, object: nil)
        
        //SystemSoundID.playFileNamed(fileName: "ringtone", withExtenstion: "mp3")
        //connectToAnOpenTokSession()
    }
    
    /**
     Method catches the battery level drop notification
     */
    @objc func batteryLevelChanged(notification: NSNotification) {
        let batteryString = String(format: "%f", UIDevice.current.batteryLevel)
        var error: OTError?
        self.session.signal(withType: "signal", string: batteryString, connection: nil, error: &error)
        if error != nil {
            print("failed to alert Battery level drop")
        }
     }
    
    /**
     Method adds image view if video stream is not available
     */
    func addAvatarImageIfVideoNotAvailable() {
        self.avatarImage = UIImageView()
        self.avatarImage.image = UIImage(named: "avatar")
        self.avatarImage.contentMode = .scaleAspectFit
        self.avatarImage.backgroundColor = .white
        self.avatarImage.frame = UIScreen.main.bounds
       // self.avatarImage.backgroundColor = UIColor.black
        self.view.insertSubview(self.avatarImage, aboveSubview: subscriberMain.view!)
        self.avatarImage.translatesAutoresizingMaskIntoConstraints = false
        
        if #available(iOS 10.0, *) {
            let top =  NSLayoutConstraint(item: self.avatarImage, attribute: .top , relatedBy: .equal, toItem: self.view.topAnchor, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom =  NSLayoutConstraint(item: self.avatarImage, attribute: .bottom, relatedBy: .equal, toItem: self.view.bottomAnchor, attribute: .top, multiplier: 1, constant: 0)
            let trailing =  NSLayoutConstraint(item: self.avatarImage, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            let leading =  NSLayoutConstraint(item: self.avatarImage, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, trailing, leading])
        } else { // Fallback on earlier versions
            let top =  NSLayoutConstraint(item: self.avatarImage, attribute: .top , relatedBy: .equal, toItem: view.safeAreaLayoutGuide.topAnchor, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom =  NSLayoutConstraint(item: self.avatarImage, attribute: .bottom, relatedBy: .equal, toItem: view.safeAreaLayoutGuide.bottomAnchor, attribute: .top, multiplier: 1, constant: 0)
            let trailing =  NSLayoutConstraint(item: self.avatarImage, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            let leading =  NSLayoutConstraint(item: self.avatarImage, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, trailing, leading])
        }
    }
    
    /**
      Method toggles the audio for audio call
     */
    func audioToggleSession() {
        let mySession : AVAudioSession = AVAudioSession.sharedInstance()
        let routePort : AVAudioSessionPortDescription = mySession.currentRoute.outputs[0]
        let portType = convertFromAVAudioSessionPort(routePort.portType)
        print("portType :",portType)
        
        if (portType == "Receiver")
        {
            try? mySession.overrideOutputAudioPort(.speaker)
            bt_speaker_enable.setImage(  UIImage(named: "Speaker") , for: .normal)
        }
        else
        {
            try? mySession.overrideOutputAudioPort(.none)
             bt_speaker_enable.setImage( UIImage(named: "NoAudio") , for: .normal)
        }
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        isCallingViewPresenting = false
        App.isIncomingCallRecieved = false
        UIApplication.shared.isIdleTimerDisabled = false
//        AppUtility.lockOrientation(.portrait)
        callSessionID = ""
        callTokenid = ""
        incomingNotificationType = ""
        App.call_appointment_id = ""
        incomingCallType = 0
        kSessionId = ""
        kToken = ""
        DispatchQueue.main.async {
            self.stopAnimating()
            SwiftMessages.hide()
        }
    }
    
    /**
     Method shows status line message when view loads
     */
    func showStatusLine(color:UIColor,message:String , duration : SwiftMessages.Duration) {
        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
        status.backgroundView.backgroundColor = color
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: message)
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.duration = duration
        statusConfig.presentationContext = .window(windowLevel: .statusBar)
        SwiftMessages.show(config: statusConfig, view: status)
    }



    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
      //  callDoctorModal = nil
//        callSessionID = ""
//        callTokenid = ""
//        incomingNotificationType = ""
//        App.call_appointment_id = ""
//        incomingCallType = 0
//        kSessionId = ""
//        kToken = ""
        
    }
    
    /**
     Method runs timer for ringing time
     */
    func runSessionConnectedTimer() {
        self.connectionTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateSessionConnectTimer), userInfo: nil, repeats: true)
    }
    
    /**
     updates timer for ringing timer
     */
   @objc func updateSessionConnectTimer()
    {
        sessionConnectSeconds += 1
        print("Session status:\(sessionConnectSeconds)")
        if sessionConnectSeconds == 27 {
            showStatusLine(color: UIColor.red, message: "we're unable to connect you to doctor", duration: .seconds(seconds: 2))
        }else if sessionConnectSeconds == 30 {
            if self.connectionTimer != nil {
                self.connectionTimer.invalidate()
                sessionConnectSeconds = 0
                self.stopAnimating()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    
    /**
     Method runs timer for ringing time
     */
    func runCallRingingTimer() {
        print("running ringing timer and duration:\(callRingingDuration)")
        self.ringingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateRingingTimer), userInfo: nil, repeats: true)
        
    }
    
    /**
      updates timer for ringing timer
     */
   @objc  func updateRingingTimer()
    {
        ringSeconds += 1
        print("ringing:\(ringSeconds)")
        if ringSeconds == Int(self.callRingingDuration) {
            ringSeconds = 0
            isCallingViewPresenting = false
            self.ringingTimer.invalidate()
            self.showMissedCallNotification()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    /**
      Method fires missed call notification if user doesn't answered call
     */
    func showMissedCallNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = "Missed call"
        content.body = "You just missed a call from \(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)"
        content.categoryIdentifier = "CallCategory"
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 4.0, repeats: false)
        
        let requestIdentifier = "MissedCallAlert"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
                print("Notification scheduling error:\(error?.localizedDescription)")
            }else{
                print("Appointment notification scheduled successfully")
            }
        }
    }
    
    /**
     Method plays sound file with file name and tag
      - Parameter fileName: pass music file name
      - Parameter tag: to know type of file is playings and make loops of playing
     */
    func playSound(fileName:String,tag:Int) {
        let soundURL = URL(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: "mp3")!)
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            if tag == 0 {
                audioPlayer.numberOfLoops = 15
            }
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch let error {
            print("error while playing sound:\(error.localizedDescription)")
        }
        print("Sound url:\(soundURL)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        playSound(fileName: "ringtone", tag: 0)
        
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            if callDoctorModal != nil {
                self.callerName.text = "\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)"
                
                if callDoctorModal.avatar_url.isEmpty != true {
                    self.incomingProfilePic.isHidden = false
//                    self.incomingProfilePic.kf.setImage(with: URL(string: callDoctorModal.avatar_url! )!)
                    self.incomingProfilePic.layer.cornerRadius = self.incomingProfilePic.frame.size.width / 2
                    self.incomingProfilePic.clipsToBounds = true
//                    self.backImage.kf.setImage(with: URL(string: callDoctorModal.avatar_url! )!)
                }else{
                    self.incomingProfilePic.isHidden = true
                }
                
                if incomingCallType == 2 {
                    self.callerDescription.text = "Video call"
                    self.bt_video.isHidden = false
                    self.bt_flip_camera.isHidden = false
                    self.bt_speaker_enable.isHidden = false
                    self.bt_speaker_enable.isEnabled = true
                    bt_speaker_enable.setImage(  UIImage(named: "Speaker") , for: .normal)
                    self.lb_audio_call_caller_name.isHidden = true
                    self.backImage.isHidden = true
                }else if incomingCallType == 1{
                    self.callerDescription.text = "Audio call"
                    self.bt_video.isHidden = true
                    self.bt_flip_camera.isHidden = true
                    self.bt_speaker_enable.isEnabled = true
                    self.bt_speaker_enable.isHidden = false
                    audioToggleSession()
                    self.lb_audio_call_caller_name.isHidden = false
                    self.lb_audio_call_caller_name.text = "\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)"
                     self.backImage.isHidden = false
                }
            }else {
                print("Not yet loaded doctor details")
            }
        
    }

    /**
      method hides which should hide on call accept
     */
    func hideViews(hide:Bool) {
        DispatchQueue.main.async {
            self.incomingProfilePic.isHidden = hide
            self.callerName.isHidden = hide
            self.callerDescription.isHidden = hide
            self.declineOutlet.isHidden = hide
            self.declineLabel.isHidden = hide
            self.acceptButtonOutlet.isHidden = hide
            self.acceptLabel.isHidden = hide
            self.acceptImageView.isHidden = hide
        }
    }
    
    /**
     method runs in call time duration timer
     */
    func runInCallTimer() {
        self.inCallTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (ViewController.updateIncallTimer), userInfo: nil, repeats: true)
    }
    
    /**
     method updates in call time duration
     */
   @objc func updateIncallTimer()
    {
        totalSeconds += 1
        timeString(time: TimeInterval(totalSeconds))
    }
    
    /**
     method displays in call time duratio on view
     */
    func timeString(time:TimeInterval)  {
        //  let day = Int(time) / 86400
        let hours = Int(time) % 86400 / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        if hours == 0 {
            self.lb_timer_display.text = String(format:"%02i:%02i", minutes,seconds)
        }else {
            self.lb_timer_display.text =  String(format:"%02i:%02i:%02i", hours,minutes,seconds)
        }
    }

    @IBAction func enabeSpeakerTapped(_ sender: ControlButton) {
        if incomingCallType == 2 {
            if subscriberMain.subscribeToAudio {
                 subscriberMain.subscribeToAudio = false
                 bt_speaker_enable.setImage(  UIImage(named: "NoAudio") , for: .normal)
            }else {
                 subscriberMain.subscribeToAudio = true
                 bt_speaker_enable.setImage(  UIImage(named: "Speaker") , for: .normal)
            }
            
        }else if incomingCallType == 1{
            audioToggleSession()
        }
    }
    
    @IBAction func flipCameraTapped(_ sender: ControlButton) {
        if hasVideo {
            if isBackCameraOn {
                publisher.cameraPosition = .front
                isBackCameraOn = false
            }else {
                publisher.cameraPosition = .back
                isBackCameraOn = true
            }
        }
    }
    
    @IBAction func acceptTapped(_ sender: UIButton) {
       audioPlayer.stop()
       hideViews(hide:true)
       ringSeconds = 0
       self.ringingTimer.invalidate()
    
       startAnimating(CGSize(width:100,height:120), message: "Connecting \n\n\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)", messageFont:  UIFont(name: "Helvetica Neue", size: 15) , type: NVActivityIndicatorType.ballPulseSync, color: UIColor.red, padding: 25, displayTimeThreshold: 10, minimumDisplayTime: 5, backgroundColor: UIColor.clear, textColor: UIColor.white)
        print("Session check url:\(AppURLS.URL_CONSULTATION_CALL_CHECK + callSessionID)")
        
        NetworkCall.performGet(url: AppURLS.URL_CONSULTATION_CALL_CHECK + callSessionID) { (success, response, status, error) in
            
            if status == 202 {
                
                DispatchQueue.main.async {
                    self.runSessionConnectedTimer()
                }
                self.connectToAnOpenTokSession()
                
               let queue1 = DispatchQueue(label: "com.doconline.doconline.callqueue1", qos: DispatchQoS.background)
                queue1.async {
                    ///call accepted send status to server
                    CallStatus.sharedInstance.perfromCallRecievedRequest(callStatus: CallingStatus.CALL_STATUS_PATIENT_ACCEPTED)
                }
            }else {
                var message = ""
                if let data = response {
                    if let mesg = data.object(forKey: Keys.KEY_MESSAGE ) as? String {
                        message = mesg
                        print("Message from server:\(message)")
                    }
                }
                
                DispatchQueue.main.async {
                    self.stopAnimating()
                    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                    let Okay = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(Okay)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            if let err = error {
                print("Error:=>\(err.localizedDescription)")
            }
        }
       
       // self.bt_connecting_call_end.isHidden = false
    }
    
    @IBAction func declineTapped(_ sender: UIButton) {
         audioPlayer.stop()
         isCallingViewPresenting = false
         ringSeconds = 0
         self.ringingTimer.invalidate()
        
        let queue2 = DispatchQueue(label: "com.doconline.doconline.callqueue2", qos: DispatchQoS.background)
        queue2.async {
            ///call rejected send status to server
            CallStatus.sharedInstance.perfromCallRecievedRequest(callStatus: CallingStatus.CALL_STATUS_PATIENT_REJECTED)
        }
        
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func connectingCallEndTapped(_ sender: UIButton) {
//         sessionDisconnect()
//         stopAnimating()
//         self.dismiss(animated: true, completion: nil)
    }
    
    /**
     Method hides connecting view and shows video call view of subscriber
     */
    func showPublisher() {
       print("Success****")
        if connectionTimer != nil {
            self.connectionTimer.invalidate()
            self.sessionConnectSeconds = 0
        }
       // self.bt_connecting_call_end.isHidden = true
        DispatchQueue.main.async {
            self.stopAnimating()
            self.bt_mic.isEnabled = true
            self.bt_video.isEnabled = true
            self.bt_flip_camera.isEnabled = true
            self.runInCallTimer()
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.showHideTransitionViews, animations: {
                //self.topConnectingView.removeFromSuperview()
                self.topConnectingView.isHidden = true
            }, completion: nil)
        }
    }
    
    /**
     Method toggles if user tapped disable video by passing boolean value
      - Parameter hide: Takes True boolean value to hide or to show false should be passed
     */
    func addPublisherView(hide:Bool) {
        guard let publisherView = publisher.view else {
            return
        }
        
        DispatchQueue.main.async {
            if hide {
                publisherView.removeFromSuperview()
                self.vw_publisher.isHidden = hide
            }else {
                self.vw_publisher.isHidden = hide
                //            let screenBounds = UIScreen.main.bounds
                //            publisherView.frame = CGRect(x: screenBounds.width - 120 - 20, y: screenBounds.height - 120 - 20, width: 120, height: 120)
                //            publisherView.layer.cornerRadius = publisherView.frame.size.width / 2
                publisherView.frame =  self.vw_publisher.bounds
                self.vw_publisher.addSubview(publisherView)
            }
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        print("Device changed to landscape : \(UIDevice.current.orientation.isLandscape)")
    }
    
    /**
     Method connects to the opentok session
     */
    func connectToAnOpenTokSession() {
        print("Connecting call**")
        //session = OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)
        var error: OTError?
        session.connect(withToken: callTokenid, error: &error)
        if error != nil {
            print(error!)
        }
    }
    
    /**
     Method updates audio icon on UI on button tap
     */
    func updatePublisherAudio(connected: Bool) {
        DispatchQueue.main.async {
             self.bt_mic.setImage(connected ? #imageLiteral(resourceName: "mic") : #imageLiteral(resourceName: "mutedMic"), for: .normal)
        }
    }
    
    /**
     Method updates Video icon on UI on button tap
     */
    func updatePublisherVideo(connected: Bool) {
        DispatchQueue.main.async {
            self.bt_video.setImage(connected ? #imageLiteral(resourceName: "video") : #imageLiteral(resourceName: "noVideo"), for: .normal)
            if !connected {
                self.backImage.isHidden = false
            }
            self.backImage.isHidden = true
        }
    }

    
    @IBAction func videoTapped(_ sender: ControlButton) {
        print("Video tapped")
        if hasVideo {
            publisher.publishVideo = false
            hasVideo = false
            self.addPublisherView(hide: true)
        }else {
            publisher.publishVideo = true
            hasVideo = true
            self.addPublisherView(hide: false)
             // updatePublisherVideo(connected: (self.hasAudio))
        }
         bt_video.setImage(hasVideo ? #imageLiteral(resourceName: "video") : #imageLiteral(resourceName: "noVideo"), for: .normal)
    }
    
    @IBAction func audioTapped(_ sender: ControlButton) {
        if hasAudio {
            publisher.publishAudio = false
            hasAudio = false
        }else {
            publisher.publishAudio = true
            hasAudio = true
        }
        
        DispatchQueue.main.async {
            self.bt_mic.setImage(self.hasAudio ? #imageLiteral(resourceName: "mic") : #imageLiteral(resourceName: "mutedMic"), for: .normal)
            print("Adudio tapped")
        }
    }
    
    /**
     Method called to disconect session of opentok when end call tapped
     */
    func sessionDisconnect() {
        var error: OTError?
        session.disconnect(&error)
        
        if error != nil {
            print("Error while disconnecting session:\(error!.localizedDescription)")
//            session = nil
//            publisher = nil
            print("Session initialization removed:\(session == nil ? true : false) publisher removed:\(publisher == nil ? true : false)")
        }
    }
    
    @IBAction func endCallTapped(_ sender: UIButton) {
        
        playSound(fileName: "beep", tag: 1)
        sessionDisconnect()
        App.timeDuration = self.lb_timer_display.text!
        DispatchQueue.main.async {
           self.dismiss(animated: true, completion: nil)
        }
    }
    
    /**
     Method adds publisher view if video call on session connected
     */
    func addPublisheVideo(){
        var error: OTError?
        session.publish(publisher, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        if incomingCallType == BookingConsultation.CALL_TYPE_AUDIO {
            publisher.publishVideo = false
            self.addPublisherView(hide: true)
        }else {
            publisher.publishVideo = true
            self.addPublisherView(hide: false)
        }
    }
    
    
    
//    deinit {
//        //session = nil
//       // print("Session initialization removed:\(session == nil ? true : false)")
//      //  callDoctorModal = nil
//        print("Destroying instance of ViewController")
//    }
}

// MARK: - UNUserNotificationCeterDelate methods
extension ViewController : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
}


// MARK: - OTSessionDelegate callbacks
extension ViewController: OTSessionDelegate {
    
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
        
//        let settings = OTPublisherSettings()
//        settings.name = UIDevice.current.name
//        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
//            return
//        }
        
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
        sessionDisconnect()
        DispatchQueue.main.async {
            App.timeDuration = self.lb_timer_display.text!
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error.localizedDescription).")
//        let alert = UIAlertController(title: "", message: "The client failed to connect to the DocOnline,Please check your internet connection", preferredStyle: UIAlertControllerStyle.alert)
//        let okayAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) { (UIAlertAction) in
//            self.sessionDisconnect()
//            App.timeDuration = self.lb_timer_display.text!
//            self.dismiss(animated: true, completion: nil)
//        }
//        alert.addAction(okayAction)
//        self.present(alert, animated: true, completion: nil)
        self.sessionDisconnect()
        DispatchQueue.main.async {
            App.timeDuration = self.lb_timer_display.text!
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
        
        addPublisheVideo()
       
        
        subscriberMain = OTSubscriber(stream: stream, delegate: self)
        guard let subscriber = subscriberMain else {
            return
        }
        
        var error: OTError?
        session.subscribe(subscriber, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        //network stats delegate
        subscriber.networkStatsDelegate = self
        
        guard let subscriberView = subscriber.view else {
            return
        }
        
        if incomingCallType == BookingConsultation.CALL_TYPE_AUDIO {
            print("Only adio call")
            publisher.publishVideo = false
        }else {
            publisher.publishVideo = true
            //subscriberView.frame = vw_subscriber.bounds
            subscriberView.translatesAutoresizingMaskIntoConstraints = false
            view.insertSubview(subscriberView, at: 0)
            if #available(iOS 10.0, *) {
                let top =  NSLayoutConstraint(item: subscriberView, attribute: .top , relatedBy: .equal, toItem: self.view.topAnchor, attribute: .bottom, multiplier: 1, constant: -20)
                let bottom =  NSLayoutConstraint(item: subscriberView, attribute: .bottom, relatedBy: .equal, toItem: self.view.bottomAnchor, attribute: .top, multiplier: 1, constant: 0)
                let trailing =  NSLayoutConstraint(item: subscriberView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
                let leading =  NSLayoutConstraint(item: subscriberView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
                
                NSLayoutConstraint.activate([top, bottom, trailing, leading])
            } else {
                // Fallback on earlier versions
                let top =  NSLayoutConstraint(item: subscriberView, attribute: .top , relatedBy: .equal, toItem: view.safeAreaLayoutGuide.topAnchor, attribute: .bottom, multiplier: 1, constant: -20)
                let bottom =  NSLayoutConstraint(item: subscriberView, attribute: .bottom, relatedBy: .equal, toItem: view.safeAreaLayoutGuide.bottomAnchor, attribute: .top, multiplier: 1, constant: 0)
                let trailing =  NSLayoutConstraint(item: subscriberView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
                let leading =  NSLayoutConstraint(item: subscriberView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
                
                NSLayoutConstraint.activate([top, bottom, trailing, leading])
            }
           
            
//             AppUtility.lockOrientation(.allButUpsideDown)
        }
        
        if stream.hasVideo {
            if avatarImage != nil {
                self.avatarImage.removeFromSuperview()
                self.avatarImage = nil
                self.lb_timer_display.textColor = .white
            }
        }else {
            if incomingCallType == 2 {
                addAvatarImageIfVideoNotAvailable()
                self.lb_timer_display.textColor = .black
            }
        }
        
        showPublisher()
    }
    
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
        sessionDisconnect()
        App.timeDuration = self.lb_timer_display.text!
        DispatchQueue.main.async {
            SwiftMessages.hide()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func sessionDidBeginReconnecting(_ session: OTSession) {
       showStatusLine(color: UIColor.red, message: "Re-Connecting...", duration: .forever)
       //startAnimating(CGSize(width:100,height:120), message: "Re-Connecting", messageFont:  UIFont(name: "Helvetica Neue", size: 15) , type: NVActivityIndicatorType.ballPulseSync, color: UIColor.red, padding: 25, displayTimeThreshold: 10, minimumDisplayTime: 5, backgroundColor: UIColor.clear, textColor: UIColor.white)
    }
    
    func sessionDidReconnect(_ session: OTSession) {
        DispatchQueue.main.async {
            self.stopAnimating()
            SwiftMessages.hide()
        }
    }
    
}

// MARK: - OTPublisherDelegate callbacks
extension ViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher failed: \(error.localizedDescription)")
        
    }
    
    func publisher(_ publisher: OTPublisher, didChangeCameraPosition position: AVCaptureDevice.Position) {
        if position == .front {
            print("changed to front")
        }else if position == .back {
            print("changed to back")
        }
    }
}

// MARK: - OTSubscriberDelegate callbacks
extension ViewController: OTSubscriberDelegate {
    
    /**
     * Sent when a frame of video has been decoded. Although the
     * subscriber will connect in a relatively short time, video can take
     * more time to synchronize. This message is sent after the
     * <[OTSubscriberKitDelegate subscriberDidConnectToStream:]> message is sent.
     * @param subscriber The subscriber that generated this event.
     */
    
    func subscriberVideoDataReceived(_ subscriber: OTSubscriber) {
       // print("video data recieved")
    }

    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream.")
    }
    
    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber failed to connect to the stream.")
        
//        let alert = UIAlertController(title: "", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
//        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
//            self.sessionDisconnect()
//            App.timeDuration = self.lb_timer_display.text!
//            self.dismiss(animated: true, completion: nil)
//        }
//
//        alert.addAction(ok)
//        self.present(alert, animated: true, completion: nil)
      //  self.didShowAlert(title: "", message: error.localizedDescription)
        
    }
    
    
    func subscriberVideoEnabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
         print("video enabled**")
        DispatchQueue.main.async {
            if self.avatarImage != nil {
                self.avatarImage.removeFromSuperview()
                self.avatarImage = nil
                self.lb_timer_display.textColor = .white
            }
        }
    }
    
    func subscriberVideoDisabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
         print("video disabled**")
        DispatchQueue.main.async {
            self.addAvatarImageIfVideoNotAvailable()
            self.lb_timer_display.textColor = .black
        }
    }
    
    func subscriberDidDisconnect(fromStream subscriber: OTSubscriberKit) {
        self.lb_network_connectivity_status.isHidden = true
        //showStatusLine(color: UIColor.red, message: "Re-Connecting...", duration: .forever)
    }
    
    
    func subscriberDidReconnect(toStream subscriber: OTSubscriberKit) {
        //self.stopAnimating()
        SwiftMessages.hide()
    }
    
}

extension ViewController : OTSubscriberKitNetworkStatsDelegate {
    
    func subscriber(_ subscriber: OTSubscriberKit, videoNetworkStatsUpdated stats: OTSubscriberKitVideoNetworkStats) {
        
        if (prevVideoTimestamp == 0)
        {
            prevVideoTimestamp = stats.timestamp;
            prevVideoBytes = stats.videoBytesReceived;
        }
        
        if stats.timestamp - prevVideoTimestamp >= Double(TIME_WINDOW) {
            let temp1:Double = Double(stats.videoBytesReceived)
            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevVideoBytes))
            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats.timestamp - prevVideoTimestamp)
            video_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
         
            self.processStats(stats: stats)
            prevVideoTimestamp = stats.timestamp
            prevVideoBytes = stats.videoBytesReceived
            print(String(format: "videoBytesReceived %llu, bps %ld, packetsLost %.2f", arguments: [stats.videoBytesReceived, video_bw, video_pl_ratio]))
            
            checkVideoQualityShowInfo()
        }
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, audioNetworkStatsUpdated stats: OTSubscriberKitAudioNetworkStats) {
        if (prevAudioTimestamp == 0)
        {
            prevAudioTimestamp = stats.timestamp
            prevAudioBytes = stats.audioBytesReceived
        }

        if stats.timestamp - prevAudioTimestamp >= Double(TIME_WINDOW)
        {
            let temp1:Double = Double(stats.audioBytesReceived)
            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevAudioBytes))
            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats.timestamp - prevAudioTimestamp)
            audio_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))

            self.processStats(stats: stats)
            prevAudioTimestamp = stats.timestamp
            prevAudioBytes = stats.audioBytesReceived
            print(String(format: "audioBytesReceived %llu, bps %ld, packetsLost %.2f", arguments: [stats.audioBytesReceived, audio_bw,audio_pl_ratio]))
        }
    }
    
    private func checkVideoQualityShowInfo() {
        DispatchQueue.main.async {
            let canDoVideo : Bool =  self.video_bw >= 150000 && self.video_pl_ratio <= 0.03   //video_bw < 150000 || video_pl_ratio > 0.03
            let canDoAudio : Bool  = self.audio_bw >= 25000 && self.audio_pl_ratio <= 0.05
            
            if !canDoVideo && incomingCallType == 2 {
                print("video bandwidth:\(self.video_bw)  PL-Ratio:\(self.video_pl_ratio)")
                self.lb_network_connectivity_status.isHidden = true
                //self.lb_network_connectivity_status.text = "Bandwidth is too low for video"
                self.lb_network_connectivity_status.text = "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call."
                self.showMessage(message: "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call.", duration: .forever)
                //showStatusLine(color: UIColor.red, message: "Bandwidth is too low for video", duration: .forever)
                print("***\n Connectiviy is poor\n VIDEO CAN NOT BE RECIEVED \n****")
            }else if !canDoAudio && incomingCallType == 1 {
                print("video bandwidth:\(self.audio_bw)  PL-Ratio:\(self.audio_pl_ratio)")
                self.lb_network_connectivity_status.isHidden = true
                //self.lb_network_connectivity_status.text = "Bandwidth is too low for audio"
                self.lb_network_connectivity_status.text = "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call."
                self.showMessage(message: "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call.", duration: .forever)
                //showStatusLine(color: UIColor.red, message: "Bandwidth is too low for video", duration: .forever)
                print("***\n Connectiviy is poor\n Audio CAN NOT BE RECIEVED \n****")
            }else {
                print("***\n Connectiviy is Good\n VIDEO or Audio CAN BE RECIEVED \n****")
                self.lb_network_connectivity_status.isHidden = true
                //SwiftMessages.hide()
            }
        }
    }
    
    func showMessage(message:String,duration:SwiftMessages.Duration) {
        let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
        view.configureTheme(.warning)
        view.configureDropShadow()
        view.configureContent(title: "", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = duration
        SwiftMessages.show(config: config, view: view)
    }
    
    private func processStats(stats:AnyObject) {
        
        if stats.isKind(of:OTSubscriberKitVideoNetworkStats.self) {
            video_pl_ratio = -1
            let videoStats : OTSubscriberKitVideoNetworkStats = stats as! OTSubscriberKitVideoNetworkStats
            if (prevVideoPacketsRcvd != 0) {
                let pl : UInt64  = videoStats.videoPacketsLost - prevVideoPacketsLost
                let pr : UInt64  = videoStats.videoPacketsReceived - prevVideoPacketsRcvd
                let pt : UInt64  = pl + pr
                if pt > 0 {
                  video_pl_ratio = Double(pl / pt)
                }
            }
            prevVideoPacketsLost = videoStats.videoPacketsLost;
            prevVideoPacketsRcvd = videoStats.videoPacketsReceived;
        }
        else if stats.isKind(of:OTSubscriberKitAudioNetworkStats.self)
        {
            audio_pl_ratio = -1
            let audioStats : OTSubscriberKitAudioNetworkStats = stats as! OTSubscriberKitAudioNetworkStats
            if (prevAudioPacketsRcvd != 0) {
                let pl : UInt64 = audioStats.audioPacketsLost - prevAudioPacketsLost
                let pr : UInt64  = audioStats.audioPacketsReceived - prevAudioPacketsRcvd
                let pt : UInt64  = pl + pr
                if pt > 0 {
                  audio_pl_ratio =  Double(pl) / Double(pt)
                }
            }
            prevAudioPacketsLost = audioStats.audioPacketsLost
            prevAudioPacketsRcvd = audioStats.audioPacketsReceived
        }
        
    }
    
}


extension SystemSoundID {
    static func playFileNamed(fileName: String, withExtenstion fileExtension: String) {
        var sound: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: fileName, withExtension: fileExtension) {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &sound)
            AudioServicesRemoveSystemSoundCompletion(sound)
        
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionPort(_ input: AVAudioSession.Port) -> String {
    return input.rawValue
}
