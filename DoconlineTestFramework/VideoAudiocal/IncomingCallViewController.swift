////
////  IncomingCallViewController.swift
////  DocOnline
////
////  Created by dev-3 on 16/06/17.
////  Copyright © 2017 ConversionBug. All rights reserved.
////
//
//import UIKit
//import AudioToolbox
//import AVFoundation
//import OpenTok
//import NVActivityIndicatorView
//import CallKit
//
//class IncomingCallViewController: UIViewController , NVActivityIndicatorViewable {
//
//    ///custom audio device shared instance reference declaration
//    let audioDevice : OTDefaultAudioDevice = OTDefaultAudioDevice.sharedInstance()
//    
//    ///OpenTok session object reference declaration with key,session id
//    lazy var session: OTSession! = {
//        return OTSession(apiKey: pApiKey, sessionId: pSessionID, delegate: self)!
//    }()
//    
//    ///Boolean var for speaker enabled or not for aduio call
//    var isSpeakerEnabledForAudioCall = false
//    ///boolean value for che
//    var isBackCameraOn = false
//    ///OpenTok publisher object reference declaration with settings
//    lazy var publisher: OTPublisher! = {
//        let settings = OTPublisherSettings()
//        settings.name = UIDevice.current.name
//        return OTPublisher(delegate: self, settings: settings)!
//    }()
//    
//    /// Subscriber object reference
//    var subscriberMain: OTSubscriber!
//    ///avatar image reference
//    var avatarImage : UIImageView!
//    ///call timer reference
//    var inCallTimer: Timer?
//    ///call timer total seconds
//    var totalSeconds : Int = 0
//    
//    
//    
//    
//    @IBOutlet weak var backImage: UIImageView!
//    @IBOutlet weak var vw_publisher: UIView!
//    @IBOutlet weak var topConnectingView: UIView!
//    @IBOutlet weak var endCallOutlet: UIButton!
//    @IBOutlet weak var incomingProfilePic: UIImageView!
//    @IBOutlet weak var callerName: UILabel!
//    @IBOutlet weak var callerDescription: UILabel!
//    @IBOutlet weak var lb_timer_display: UILabel!
//    
//    
//    
//    
////    let callController = CXCallController()
////
////    func end(call: UUID) {
////        let endCallAction = CXEndCallAction(call: call )
////        let transaction = CXTransaction()
////        transaction.addAction(endCallAction)
////
////        requestTransaction(transaction, action: "endCall")
////    }
////    
////    private func requestTransaction(_ transaction: CXTransaction, action: String = "") {
////        callController.request(transaction) { error in
////            if let error = error {
////                print("Error requesting transaction: \(error)")
////            } else {
////                print("Requested transaction \(action) successfully")
////            }
////        }
////    }
//    
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//       
////        if calluuid != nil {
////            end(call: calluuid!)
////        }
//
//        startAnimating()
//        OTAudioDeviceManager.setAudioDevice(audioDevice)
//        
//        connectToAnOpenTokSession()
//     }
//
//    
//    override func viewDidAppear(_ animated:Bool) {
//        super.viewDidAppear(animated)
//       
//    }
//    
//    
//    /**
//     Method connects to the opentok session
//     */
//    func connectToAnOpenTokSession() {
//        print("Connecting call**")
//    
//        var error: OTError?
//        session.connect(withToken: pTokenId, error: &error)
//        if error != nil {
//            print(error!)
//        }
//    }
//    
//    /**
//     method runs in call time duration timer
//     */
//    func runInCallTimer() {
//        self.inCallTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (ViewController.updateIncallTimer), userInfo: nil, repeats: true)
//    }
//    
//    /**
//     method updates in call time duration
//     */
//    @objc func updateIncallTimer()
//    {
//        totalSeconds += 1
//        timeString(time: TimeInterval(totalSeconds))
//    }
//    /**
//     method displays in call time duratio on view
//     */
//    func timeString(time:TimeInterval)  {
//        //  let day = Int(time) / 86400
//        let hours = Int(time) % 86400 / 3600
//        let minutes = Int(time) / 60 % 60
//        let seconds = Int(time) % 60
//        if hours == 0 {
//            self.lb_timer_display.text = String(format:"%02i:%02i", minutes,seconds)
//        }else {
//            self.lb_timer_display.text =  String(format:"%02i:%02i:%02i", hours,minutes,seconds)
//        }
//    }
//    
//    /**
//     Method called to disconect session of opentok when end call tapped
//     */
//    func sessionDisconnect() {
//        var error: OTError?
//        session.disconnect(&error)
//        
//        if error != nil {
//            print("Error while disconnecting session:\(error!.localizedDescription)")
//            //            session = nil
//            //            publisher = nil
//            print("Session initialization removed:\(session == nil ? true : false) publisher removed:\(publisher == nil ? true : false)")
//            
//        }
//    }
//    
//    /**
//     Method adds publisher view if video call on session connected
//     */
//    func addPublisheVideo(){
//        var error: OTError?
//        session.publish(publisher, error: &error)
//        guard error == nil else {
//            print(error!)
//            return
//        }
//        
//        if incomingCallType == BookingConsultation.CALL_TYPE_AUDIO {
//            publisher.publishVideo = false
//            self.addPublisherView(hide: true)
//        }else {
//            publisher.publishVideo = true
//            self.addPublisherView(hide: false)
//        }
//    }
//   
//    /**
//     Method toggles if user tapped disable video by passing boolean value
//     - Parameter hide: Takes True boolean value to hide or to show false should be passed
//     */
//    func addPublisherView(hide:Bool) {
//        guard let publisherView = publisher.view else {
//            return
//        }
//        if hide {
//            publisherView.removeFromSuperview()
//            vw_publisher.isHidden = hide
//        }else {
//            vw_publisher.isHidden = hide
//            publisherView.frame = vw_publisher.bounds
//            vw_publisher.addSubview(publisherView)
//        }
//    }
//    
//    
//    /**
//     Method adds image view if video stream is not available
//     */
//    func addAvatarImageIfVideoNotAvailable() {
//        self.avatarImage = UIImageView()
//        self.avatarImage.image = UIImage(named: "avatar")
//        self.avatarImage.contentMode = .scaleAspectFit
//        self.avatarImage.backgroundColor = UIColor.white
//        self.avatarImage.frame = UIScreen.main.bounds
//        // self.avatarImage.backgroundColor = UIColor.black
//        self.view.insertSubview(self.avatarImage, aboveSubview: subscriberMain.view!)
//        self.avatarImage.translatesAutoresizingMaskIntoConstraints = false
//        
//        let top =  NSLayoutConstraint(item: self.avatarImage, attribute: .top , relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0)
//        let bottom =  NSLayoutConstraint(item: self.avatarImage, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0)
//        let trailing =  NSLayoutConstraint(item: self.avatarImage, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
//        let leading =  NSLayoutConstraint(item: self.avatarImage, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
//        
//        NSLayoutConstraint.activate([top, bottom, trailing, leading])
//    }
//    
//    /**
//     Method hides connecting view and shows video call view of subscriber
//     */
//    func showPublisher() {
//        print("Success****")
////        if connectionTimer != nil {
////            self.connectionTimer.invalidate()
////            self.sessionConnectSeconds = 0
////        }
//        // self.bt_connecting_call_end.isHidden = true
//        stopAnimating()
//       // self.bt_mic.isEnabled = true
//       // self.bt_video.isEnabled = true
//      //  self.bt_flip_camera.isEnabled = true
//        runInCallTimer()
//        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.showHideTransitionViews, animations: {
//            //self.topConnectingView.removeFromSuperview()
//            self.topConnectingView.isHidden = true
//        }, completion: nil)
//    }
//    
//    
//    @IBAction func callEndTapped(_ sender: UIButton) {
//       sessionDisconnect()
//    }
//    
//    
// 
//    override func viewWillDisappear(_ animated: Bool)  {
//       super.viewWillDisappear(animated)
//        if self.inCallTimer != nil {
//            self.inCallTimer?.invalidate()
//            self.inCallTimer = nil
//        }
//    }
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
//
//// MARK: - OTSessionDelegate callbacks
//extension IncomingCallViewController : OTSessionDelegate {
//    
//    func sessionDidConnect(_ session: OTSession) {
//        print("The client connected to the OpenTok session.")
//    }
//    
//    func sessionDidDisconnect(_ session: OTSession) {
//        print("The client disconnected from the OpenTok session.")
//        sessionDisconnect()
//      //  App.timeDuration = self.lb_timer_display.text!
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func session(_ session: OTSession, didFailWithError error: OTError) {
//        print("The client failed to connect to the OpenTok session: \(error.localizedDescription).")
//        self.sessionDisconnect()
//       // App.timeDuration = self.lb_timer_display.text!
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func session(_ session: OTSession, streamCreated stream: OTStream) {
//        print("A stream was created in the session.")
//        
//        addPublisheVideo()
//        
//        
//        subscriberMain = OTSubscriber(stream: stream, delegate: self)
//        guard let subscriber = subscriberMain else {
//            return
//        }
//        
//        var error: OTError?
//        session.subscribe(subscriber, error: &error)
//        guard error == nil else {
//            print(error!)
//            return
//        }
//        
//        //network stats delegate
//        subscriber.networkStatsDelegate = self
//        
//        guard let subscriberView = subscriber.view else {
//            return
//        }
//        
//        if incomingCallType == BookingConsultation.CALL_TYPE_AUDIO {
//            print("Only adio call")
//            publisher.publishVideo = false
//        }else {
//            publisher.publishVideo = true
//            //subscriberView.frame = vw_subscriber.bounds
//            subscriberView.translatesAutoresizingMaskIntoConstraints = false
//            view.insertSubview(subscriberView, at: 0)
//            let top =  NSLayoutConstraint(item: subscriberView, attribute: .top , relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: -20)
//            let bottom =  NSLayoutConstraint(item: subscriberView, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0)
//            let trailing =  NSLayoutConstraint(item: subscriberView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
//            let leading =  NSLayoutConstraint(item: subscriberView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
//            
//            NSLayoutConstraint.activate([top, bottom, trailing, leading])
//            
//            AppUtility.lockOrientation(.allButUpsideDown)
//        }
//        
//        if stream.hasVideo {
//            if avatarImage != nil {
//                self.avatarImage.removeFromSuperview()
//                self.avatarImage = nil
//            }
//        }else {
//            if incomingCallType == 2 {
//                addAvatarImageIfVideoNotAvailable()
//            }
//        }
//        
//        showPublisher()
//       
//    }
//    
//    
//    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
//        print("A stream was destroyed in the session.")
//        sessionDisconnect()
//        //App.timeDuration = self.lb_timer_display.text!
//      //  SwiftMessages.hide()
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    
//    func sessionDidBeginReconnecting(_ session: OTSession) {
//       // showStatusLine(color: UIColor.red, message: "Re-Connecting...", duration: .forever)
//        //startAnimating(CGSize(width:100,height:120), message: "Re-Connecting", messageFont:  UIFont(name: "Helvetica Neue", size: 15) , type: NVActivityIndicatorType.ballPulseSync, color: UIColor.red, padding: 25, displayTimeThreshold: 10, minimumDisplayTime: 5, backgroundColor: UIColor.clear, textColor: UIColor.white)
//    }
//    
//    func sessionDidReconnect(_ session: OTSession) {
//       // self.stopAnimating()
//       // SwiftMessages.hide()
//    }
//    
//}
//
//// MARK: - OTPublisherDelegate callbacks
//extension IncomingCallViewController: OTPublisherDelegate {
//    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
//        print("The publisher failed: \(error.localizedDescription)")
//        
//    }
//    
//    func publisher(_ publisher: OTPublisher, didChangeCameraPosition position: AVCaptureDevice.Position) {
//        if position == .front {
//            print("changed to front")
//        }else if position == .back {
//            print("changed to back")
//        }
//    }
//}
//
//// MARK: - OTSubscriberDelegate callbacks
//extension IncomingCallViewController: OTSubscriberDelegate {
//    
//    /**
//     * Sent when a frame of video has been decoded. Although the
//     * subscriber will connect in a relatively short time, video can take
//     * more time to synchronize. This message is sent after the
//     * <[OTSubscriberKitDelegate subscriberDidConnectToStream:]> message is sent.
//     * @param subscriber The subscriber that generated this event.
//     */
//    
//    func subscriberVideoDataReceived(_ subscriber: OTSubscriber) {
//        // print("video data recieved")
//    }
//    
//    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
//        print("The subscriber did connect to the stream.")
//    }
//    
//    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
//        print("The subscriber failed to connect to the stream.")
//        
//        //        let alert = UIAlertController(title: "", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
//        //        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
//        //            self.sessionDisconnect()
//        //            App.timeDuration = self.lb_timer_display.text!
//        //            self.dismiss(animated: true, completion: nil)
//        //        }
//        //
//        //        alert.addAction(ok)
//        //        self.present(alert, animated: true, completion: nil)
//        //  self.didShowAlert(title: "", message: error.localizedDescription)
//        
//    }
//    
//    
//    func subscriberVideoEnabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
//        print("video enabled**")
//        if avatarImage != nil {
//            self.avatarImage.removeFromSuperview()
//            self.avatarImage = nil
//        }
//    }
//    
//    func subscriberVideoDisabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
//        print("video disabled**")
//        addAvatarImageIfVideoNotAvailable()
//    }
//    
//    func subscriberDidDisconnect(fromStream subscriber: OTSubscriberKit) {
//      //  self.lb_network_connectivity_status.isHidden = true
//        //showStatusLine(color: UIColor.red, message: "Re-Connecting...", duration: .forever)
//    }
//    
//    
//    func subscriberDidReconnect(toStream subscriber: OTSubscriberKit) {
//        //self.stopAnimating()
//       // SwiftMessages.hide()
//    }
//    
//}
//
//extension IncomingCallViewController : OTSubscriberKitNetworkStatsDelegate {
//    
////    func subscriber(_ subscriber: OTSubscriberKit, videoNetworkStatsUpdated stats: OTSubscriberKitVideoNetworkStats) {
////
////        if (prevVideoTimestamp == 0)
////        {
////            prevVideoTimestamp = stats.timestamp;
////            prevVideoBytes = stats.videoBytesReceived;
////        }
////
////        if stats.timestamp - prevVideoTimestamp >= Double(TIME_WINDOW) {
////            let temp1:Double = Double(stats.videoBytesReceived)
////            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevVideoBytes))
////            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats.timestamp - prevVideoTimestamp)
////            video_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
////
////            self.processStats(stats: stats)
////            prevVideoTimestamp = stats.timestamp
////            prevVideoBytes = stats.videoBytesReceived
////            print(String(format: "videoBytesReceived %llu, bps %ld, packetsLost %.2f", arguments: [stats.videoBytesReceived, video_bw, video_pl_ratio]))
////
////            checkVideoQualityShowInfo()
////        }
////    }
////
////    func subscriber(_ subscriber: OTSubscriberKit, audioNetworkStatsUpdated stats: OTSubscriberKitAudioNetworkStats) {
////        if (prevAudioTimestamp == 0)
////        {
////            prevAudioTimestamp = stats.timestamp
////            prevAudioBytes = stats.audioBytesReceived
////        }
////
////        if stats.timestamp - prevAudioTimestamp >= Double(TIME_WINDOW)
////        {
////            let temp1:Double = Double(stats.audioBytesReceived)
////            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevAudioBytes))
////            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats.timestamp - prevAudioTimestamp)
////            audio_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
////
////            self.processStats(stats: stats)
////            prevAudioTimestamp = stats.timestamp
////            prevAudioBytes = stats.audioBytesReceived
////            print(String(format: "audioBytesReceived %llu, bps %ld, packetsLost %.2f", arguments: [stats.audioBytesReceived, audio_bw,audio_pl_ratio]))
////        }
////    }
////
////    private func checkVideoQualityShowInfo() {
////        let canDoVideo : Bool =  video_bw >= 150000 && video_pl_ratio <= 0.03   //video_bw < 150000 || video_pl_ratio > 0.03
////        let canDoAudio : Bool  = audio_bw >= 25000 && audio_pl_ratio <= 0.05
////
////        if !canDoVideo && incomingCallType == 2 {
////            print("video bandwidth:\(video_bw)  PL-Ratio:\(video_pl_ratio)")
////            self.lb_network_connectivity_status.isHidden = false
////            self.lb_network_connectivity_status.text = "Bandwidth is too low for video"
////            //showStatusLine(color: UIColor.red, message: "Bandwidth is too low for video", duration: .forever)
////            print("***\n Connectiviy is poor\n VIDEO CAN NOT BE RECIEVED \n****")
////        }else if !canDoAudio && incomingCallType == 1 {
////            print("video bandwidth:\(audio_bw)  PL-Ratio:\(audio_pl_ratio)")
////            self.lb_network_connectivity_status.isHidden = false
////            self.lb_network_connectivity_status.text = "Bandwidth is too low for audio"
////            //showStatusLine(color: UIColor.red, message: "Bandwidth is too low for video", duration: .forever)
////            print("***\n Connectiviy is poor\n Audio CAN NOT BE RECIEVED \n****")
////        }else {
////            print("***\n Connectiviy is Good\n VIDEO or Audio CAN BE RECIEVED \n****")
////            self.lb_network_connectivity_status.isHidden = true
////            //SwiftMessages.hide()
////        }
////    }
////
////    private func processStats(stats:AnyObject) {
////
////        if stats.isKind(of:OTSubscriberKitVideoNetworkStats.self) {
////            video_pl_ratio = -1
////            let videoStats : OTSubscriberKitVideoNetworkStats = stats as! OTSubscriberKitVideoNetworkStats
////            if (prevVideoPacketsRcvd != 0) {
////                let pl : UInt64  = videoStats.videoPacketsLost - prevVideoPacketsLost
////                let pr : UInt64  = videoStats.videoPacketsReceived - prevVideoPacketsRcvd
////                let pt : UInt64  = pl + pr
////                if pt > 0 {
////                    video_pl_ratio = Double(pl / pt)
////                }
////            }
////            prevVideoPacketsLost = videoStats.videoPacketsLost;
////            prevVideoPacketsRcvd = videoStats.videoPacketsReceived;
////        }
////        else if stats.isKind(of:OTSubscriberKitAudioNetworkStats.self)
////        {
////            audio_pl_ratio = -1
////            let audioStats : OTSubscriberKitAudioNetworkStats = stats as! OTSubscriberKitAudioNetworkStats
////            if (prevAudioPacketsRcvd != 0) {
////                let pl : UInt64 = audioStats.audioPacketsLost - prevAudioPacketsLost
////                let pr : UInt64  = audioStats.audioPacketsReceived - prevAudioPacketsRcvd
////                let pt : UInt64  = pl + pr
////                if pt > 0 {
////                    audio_pl_ratio =  Double(pl) / Double(pt)
////                }
////            }
////            prevAudioPacketsLost = audioStats.audioPacketsLost
////            prevAudioPacketsRcvd = audioStats.audioPacketsReceived
////        }
////
////    }
//    
//}

