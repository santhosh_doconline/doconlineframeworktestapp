//
//  CallConnectingViewController.swift
//  DocOnline
//
//  Created by dev-3 on 23/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import CallKit
import SwiftMessages


@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornRadius: CGFloat = 3
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.black.withAlphaComponent(0.2)
    @IBInspectable var shadowOpacity: Float = 0.8
    
    override func layoutSubviews() {
        layer.cornerRadius = cornRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}


class CallConnectingViewController: UIViewController ,NVActivityIndicatorViewable {
 
    @IBOutlet var sw_allowAccessForDocs: UISwitch!
    @IBOutlet var lb_callerName: UILabel!
    @IBOutlet var lb_bw: UILabel!
    @IBOutlet var lbPub_bw: UILabel!
    @IBOutlet var iv_imageView: UIImageView!
    
    @IBOutlet var vw_publisher: UIView!
    @IBOutlet var bt_endCall: UIButton!
    
    @IBOutlet weak var vw_doctorRating: CosmosView!
    @IBOutlet var vw_doctorDetails: CardView!
    @IBOutlet var vw_navigation: UIView!
    @IBOutlet var lb_drNavName: UILabel!
    @IBOutlet var bt_info: UIButton!
    @IBOutlet var iv_doctorImage: UIImageView!
    @IBOutlet var lb_doctorName: UILabel!
    @IBOutlet var lb_specialization: UILabel!
    @IBOutlet var lb_drPractitionerNo: UILabel!
    @IBOutlet var lb_rating: UILabel!
    @IBOutlet var LC_drDetailsViewWidth: NSLayoutConstraint!
    @IBOutlet var LC_drDetailsViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var bt_closeDoctorDetails: UIButton!
    
    @IBOutlet weak var lb_mciNo: UILabel!
    
    @IBOutlet var backImage: UIImageView!
    @IBOutlet var lb_incall_timer: UILabel!
    @IBOutlet weak var topConnectingView: UIView!
    @IBOutlet weak var incomingProfilePic: UIImageView!
    @IBOutlet weak var callerName: UILabel!
    @IBOutlet weak var callerDescription: UILabel!
    
    @IBOutlet weak var bt_video: UIButton!
    @IBOutlet weak var bt_mic: UIButton!
    @IBOutlet weak var bt_flip_camera: UIButton!
    @IBOutlet weak var bt_speaker_enable: UIButton!
    
    var readmoreFontColor = UIColor.white

    var callManager : SpeakerboxCallManager!
    
    var calls = [SpeakerboxCall]()
    
    
    //CXCALLUPDATE
    var answerAction : CXAnswerCallAction?
    
    //current call instance
    var currentOngoingCall : SpeakerboxCall!
    
    ///avatar image reference
    var avatarImage : UIImageView!
    var subscriberView : UIView!
    
    ///checks the session has video
    var hasVideo = true
    ///check the session has audio
    var hasAudio = true
    ///boolean value for checking rear camera is on or not
    var isBackCameraOn = false
    ///Boolean var for speaker enabled or not for aduio call
    var isSpeakerEnabledForAudioCall = false
    
    ///call timer reference
    var inCallTimer: Timer?
    ///call timer total seconds
    var totalSeconds : Int = 0
    ///Opentok session connect timer reference
    var connectionTimer : Timer!
    ///ringing seconds
    var sessionConnectSeconds : Int = 0
    
    //MARK:- Network Stats variables
    var prevVideoTimestamp : Double = 0
    var prevVideoBytes : UInt64 = 0
    var prevAudioTimestamp : Double = 0
    var prevAudioBytes : UInt64 = 0
    var prevVideoPacketsLost : UInt64 = 0
    var prevVideoPacketsRcvd : UInt64 = 0
    var prevAudioPacketsLost : UInt64 = 0
    var prevAudioPacketsRcvd : UInt64 = 0
    var video_bw : CLong = 0
    var audio_bw : CLong = 0
    var video_pl_ratio : Double = -1
    var audio_pl_ratio : Double = -1
    
    //Mark :- Publisher Network Stats Variables
    var prevPubVideoTimestamp : Double = 0
    var prevPubVideoBytes : UInt64 = 0
    var prevPubAudioTimestamp : Double = 0
    var prevPubAudioBytes : UInt64 = 0
    var prevPubVideoPacketsLost : UInt64 = 0
    var prevPubVideoPacketsSent : UInt64 = 0
    var prevPubAudioPacketsLost : UInt64 = 0
    var prevPubAudioPacketsRcvd : UInt64 = 0
    var videoPub_bw : CLong = 0
    var audioPub_bw : CLong = 0
    var video_pub_pl_ratio : Double = -1
    var audio_pub_pl_ratio : Double = -1
    
    
    
//Callkit update
    let callController = CXCallController()
    
    func endNative(call: UUID) {
        
        let endCallAction = CXEndCallAction(call: call)
        let transaction = CXTransaction()
        transaction.addAction(endCallAction)
        
        requestTransaction(transaction, action: "endCall")
    }
    
    func setMute(call:UUID,isMuted:Bool) {
        let setMuteCall = CXSetMutedCallAction(call: call, muted: isMuted)
        let transaction = CXTransaction()
        transaction.addAction(setMuteCall)
        
        requestTransaction(transaction, action: "setMute")
    }
 
    private func requestTransaction(_ transaction: CXTransaction, action: String = "") {
        callController.request(transaction) { error in
            if let error = error {
                print("Error requesting transaction: \(error)")
            } else {
                print("Requested transaction \(action) successfully")
                if action.isEqual("setMute") {
                    DispatchQueue.main.async {
                         self.bt_mic.setImage(self.hasAudio ? #imageLiteral(resourceName: "mic_off") : #imageLiteral(resourceName: "mic_on"), for: .normal)
                      }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "avatar")?.withRenderingMode(.alwaysTemplate)
        self.backImage.image = image
        self.backImage.tintColor = .white
        self.backImage.backgroundColor = .black
        
//        FloatingButtonWindow.sharedInstance.removeTimer()
        let vwGrad1 = GradientView()
        vwGrad1.frame =  CGRect(x: 0.0, y: 0.0, width: self.vw_navigation.frame.size.width, height: self.vw_navigation.frame.size.height)
        vwGrad1.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.vw_navigation.insertSubview(vwGrad1, at: 0)
        
       self.vw_publisher.isHidden = true
       self.iv_imageView.isHidden = true
       self.topConnectingView.isHidden = false
        isCallingViewPresenting = true
        addShadowToNavView()
        bt_endCall.isEnabled = true
        bt_endCall.layer.cornerRadius = bt_endCall.bounds.width / 2
        
        startAnimating(CGSize(width:100,height:120), message: "Connecting", messageFont:  UIFont(name: "Helvetica Neue", size: 15) , type: NVActivityIndicatorType.ballPulseSync, color: UIColor.white, padding: 25, displayTimeThreshold: 10, minimumDisplayTime: 5, backgroundColor: UIColor.clear, textColor: UIColor.white)
        
        self.runSessionConnectedTimer()
        
//        let vwGrad = GradientView()
//        vwGrad.frame =  CGRect(x: 0.0, y: 0.0, width: self.topConnectingView.frame.size.width, height: self.topConnectingView.frame.size.height)
//        vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        self.topConnectingView.addSubview(vwGrad)
        
//              let vwGrad = DiagonalGradientView()
//            vwGrad.frame =  CGRect(x: 0.0, y: 0.0, width: self.topConnectingView.frame.size.width, height: self.topConnectingView.frame.size.height)
//            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            self.topConnectingView.addSubview(vwGrad)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if callDoctorModal != nil {
            if let fullName = callDoctorModal.full_name {
                self.callerName.text = fullName
                self.lb_drNavName.text = fullName
                self.lb_doctorName.text = fullName
            }
            self.lb_mciNo.text = callDoctorModal.mciCode
//            self.vw_doctorRating.rating = callDoctorModal.ratings == nil ? 0.0 : callDoctorModal.ratings
            if let fullName = callDoctorModal.specialisation {
                self.lb_specialization.text = fullName
                self.lb_specialization.isHidden = false
            }else {
                  self.lb_specialization.isHidden = true
            }
            
            if let prctNum = callDoctorModal.practitionerNumber {
                self.lb_drPractitionerNo.text = "\(prctNum)"
            }else {
                self.lb_drPractitionerNo.text = "xxxxx"
            }
            
            if let ratings = callDoctorModal.ratings {
                self.lb_rating.text = "\(ratings) / 5"
            }else {
                self.lb_rating.text = "0.0"
            }
            
            
           // self.callerName.text = "\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)"
            
            if callDoctorModal.avatar_url.isEmpty != true {
                self.incomingProfilePic.isHidden = false
//                self.incomingProfilePic.kf.setImage(with:  URL(string: callDoctorModal.avatar_url ?? "" ), placeholder: UIImage(named:"doctor_placeholder_nocircle") , options: nil, progressBlock: nil, completionHandler: nil)
                self.incomingProfilePic.image = UIImage(named:"doctor_placeholder_nocircle")
                self.incomingProfilePic.layer.cornerRadius = self.incomingProfilePic.frame.size.width / 2
                self.incomingProfilePic.clipsToBounds = true
                
//                self.iv_doctorImage.kf.setImage(with:  URL(string: callDoctorModal.avatar_url ?? "" ), placeholder: UIImage(named:"doctor_placeholder_nocircle") , options: nil, progressBlock: nil, completionHandler: nil)
                self.iv_doctorImage.image = UIImage(named:"doctor_placeholder_nocircle")

                self.iv_doctorImage.layer.cornerRadius = self.iv_doctorImage.frame.size.width / 2
                self.iv_doctorImage.clipsToBounds = true
                
            }else{
                self.incomingProfilePic.isHidden = true
            }
            
            if incomingCallType == 2 {
                self.callerDescription.text = "Video call"
                self.bt_video.isHidden = false
                self.bt_flip_camera.isHidden = false
                self.bt_speaker_enable.isHidden = false
                self.bt_speaker_enable.isEnabled = true
                bt_speaker_enable.setImage(  UIImage(named: "EnableSpeaker") , for: .normal)
                self.lb_callerName.isHidden = true
                self.backImage.isHidden = true
                
                UIDevice.current.isProximityMonitoringEnabled = false
                UIApplication.shared.isIdleTimerDisabled = true
                
            }else if incomingCallType == 1{
                UIDevice.current.isProximityMonitoringEnabled = true
                self.callerDescription.text = "Audio call"
                self.bt_video.isHidden = true
                self.bt_flip_camera.isHidden = true
                self.bt_speaker_enable.isEnabled = true
                self.bt_speaker_enable.isHidden = false
                self.vw_publisher.isHidden = true
                
                self.lb_callerName.isHidden = false
                self.lb_callerName.text = "\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)"
                self.backImage.isHidden = false
//                self.backImage.image = UIImage(named:"avatar")
            }
        }
        
        NotificationCenter.default.addObserver(forName: UIDevice.proximityStateDidChangeNotification, object: nil, queue: OperationQueue.main) { (notification) in
            print("The proximity sensor :\(UIDevice.current.proximityState ? "will now blank the screen" : "will now restore the screen")");
        }
        
//        self.sw_allowAccessForDocs.set(width: 0.0, height: 0.0) //changed as per ticket
    }
    
//    func postConsentChange(allow:Int) {
//        let dic = ["document_consent": allow]
//        if let data = try? JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted) {
//            DispatchQueue.global(qos: .background).async {
//                NetworkCall.performPATCH(url: AppURLS.URL_EHR_CONSENT, data: data) { (success, response, statusCode, error) in
//                    if let err = error {
//                        print("Error while posting consent:\(err.localizedDescription)")
//                        DispatchQueue.main.async {
//                            self.didShowAlert(title:"",message:"Couldn't update the access. Please try again")
//                            self.sw_allowAccessForDocs.isOn = !self.sw_allowAccessForDocs.isOn
//                        }
//                    }else {
//                        let check = self.check_Status_Code(statusCode: statusCode ?? 0, data: response)
//
//                        if check , let resp = response ,
//                            let data =  resp.object(forKey: Keys.KEY_DATA ) as? [String:Any] ,
//                            let consent =  data["ehr_document_consent"] as? Int {
//                            print("Access updated:\(resp)")
//                            DispatchQueue.main.async {
//                                self.sw_allowAccessForDocs.isOn =  consent == 1 ? true : false
//                                App.appSettings?.ehrDocumentConsent = consent
//                            }
//                        }else {
//                            DispatchQueue.main.async {
//                                self.didShowAlert(title:"",message:"Couldn't update the access. Please try again")
//                                self.sw_allowAccessForDocs.isOn = !self.sw_allowAccessForDocs.isOn
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 0.5) {
//                self.sw_allowAccessForDocs.isOn = (App.appSettings?.ehrDocumentConsent ?? 0) == 1 ? true : false
//            }
//        }
        
        if let callsOnline = AppConfig.shared?.providerDelegate?.callManager.calls {
             calls = callsOnline
            if callsOnline.count == 0 {
                self.dismiss(animated: true, completion: nil)
            }else {
                 callsOnline[0].speakerDelegate = self
                currentOngoingCall = callsOnline[0]
                AppConfig.shared?.providerDelegate?.callSessionDelegate = self
            }
        }
        
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                if let publisher = call.publisher {
                    DispatchQueue.main.async {
                        self.bt_mic.setImage(self.hasAudio ? #imageLiteral(resourceName: "mic_off") : #imageLiteral(resourceName: "mic_on"), for: .normal)
                    }
                }
            }
        }
        
        let tokens = "callsOnline \(calls.count) uuid \(AppConfig.shared?.currentCallUUID) api_key \(kApiKey) session_id \(callSessionID) token_id \(callTokenid)"
//        let alert = UIAlertController(title: "tokens", message: tokens, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
//        alert.addAction(okAction)
//        self.present(alert, animated: true, completion: nil)
        self.showAlert(msg: tokens)
        if !self.checkCameraPermission() && !self.checkAudioPermission()
        {
            self.showAlertForDeniedPermissions( message: "Please provide access for the camera and microphone for video and audio")
        }else if !self.checkAudioPermission() {
            self.showAlertForDeniedPermissions( message: "Please provide access for the microphone for audio access")
        }else if !self.checkCameraPermission(){
             self.showAlertForDeniedPermissions( message: "Please provide access for the camera for video")
        }
//
//        if !NetworkUtilities.isConnectedToNetwork()
//        {
//           let alert = UIAlertController(
//        }
    }
    
    
    
    @IBAction func allowAccessForDocsTapped(_ sender: UISwitch) {
//        self.postConsentChange(allow: sender.isOn ? 1 : 0)
    }
    

    ///Shows alert for asking permission if denied
    func showAlertForDeniedPermissions(message:String) {
        if let settingsUrl = URL(string: UIApplication.openSettingsURLString)  {
            let alert = UIAlertController(title: "Access Needed!", message: message, preferredStyle: UIAlertController.Style.alert)
            let okay = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                // If camera or library settings are disabled then open general settings
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            })
            alert.addAction(okay)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SwiftMessages.hide()
        if self.inCallTimer != nil {
            self.inCallTimer?.invalidate()
            self.inCallTimer = nil
        }
        
        self.stopAnimating()
        AppConfig.shared?.providerDelegate?.callManager.removeAllCalls()
        isCallingViewPresenting = false
        App.isIncomingCallRecieved = false
        UIApplication.shared.isIdleTimerDisabled = false
        UIDevice.current.isProximityMonitoringEnabled = false
//        AppUtility.lockOrientation(.portrait)
        callSessionID = ""
        callTokenid = ""
        incomingNotificationType = ""
        App.call_appointment_id = ""
        incomingCallType = 0
       
        if self.lb_incall_timer.text!.isEqual("00:00") {
             App.timeDuration = ""
        }else {
             App.timeDuration = self.lb_incall_timer.text!
        }
    }
    
    
    /**
     Method toggles the audio for audio call
     */
    func audioToggleSession() {
        let mySession : AVAudioSession = AVAudioSession.sharedInstance()
        let routePort : AVAudioSessionPortDescription = mySession.currentRoute.outputs[0]
        let portType = convertFromAVAudioSessionPort(routePort.portType)
        print("portType :",portType)
        
        if (portType == "Receiver")
        {
            try? mySession.overrideOutputAudioPort(.speaker)
            bt_speaker_enable.setImage(  UIImage(named: "EnableSpeaker") , for: .normal)
        }
        else
        {
            try? mySession.overrideOutputAudioPort(.none)
            bt_speaker_enable.setImage( UIImage(named: "DisableSpeaker") , for: .normal)
        }
    }
    
    /**
     Method ends the current running call
     */
    func endCall() {
       for call in self.calls {
           // AppDelegate.shared.providerDelegate?.callManager.end(call: call)
            call.endCall()
        }
        
    //  self.endNative(call: AppDelegate.shared.currentCallUUID)

      self.dismiss(animated: true, completion: nil)
    }
    
    
    
    /**
     Method adds image view if video stream is not available
     */
    func addAvatarImageIfVideoNotAvailable() {
        self.avatarImage = UIImageView()
        let image = UIImage(named: "avatar")?.withRenderingMode(.alwaysTemplate)
        self.avatarImage.image = image
        self.avatarImage.tintColor = .white
        self.avatarImage.contentMode = .scaleAspectFit
        self.avatarImage.backgroundColor = UIColor.black
        self.avatarImage.frame = UIScreen.main.bounds
        // self.avatarImage.backgroundColor = UIColor.black
        self.view.insertSubview(self.avatarImage, aboveSubview: self.subscriberView)
        self.avatarImage.translatesAutoresizingMaskIntoConstraints = false
        
        let top =  NSLayoutConstraint(item: self.avatarImage, attribute: .top , relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: -20)
        let bottom =  NSLayoutConstraint(item: self.avatarImage, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0)
        let trailing =  NSLayoutConstraint(item: self.avatarImage, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        let leading =  NSLayoutConstraint(item: self.avatarImage, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([top, bottom, trailing, leading])
    }
    
    /**
     Method shows status line message when view loads
     */
    func showStatusLine(color:UIColor,message:String , duration : SwiftMessages.Duration) {
        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
        status.backgroundView.backgroundColor = color
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: message)
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.duration = duration
        statusConfig.presentationContext = .window(windowLevel: .statusBar)
        SwiftMessages.show(config: statusConfig, view: status)
    }

    
    /**
     Method runs timer for ringing time
     */
    func runSessionConnectedTimer() {

        self.connectionTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateSessionConnectTimer), userInfo: nil, repeats: true)
    }
    
    /**
     updates timer for ringing timer
     */
    @objc func updateSessionConnectTimer()
    {
        sessionConnectSeconds += 1
        print("Session status:\(sessionConnectSeconds)")
        if sessionConnectSeconds == 27 {
            showStatusLine(color: UIColor.red, message: "we're unable to connect you to doctor", duration: .seconds(seconds: 2))
        }else if sessionConnectSeconds == 30 {
            
            if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
                for call in calls {
                    AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                }
            }
            
            if self.connectionTimer != nil {
                self.connectionTimer.invalidate()
                sessionConnectSeconds = 0
                self.stopAnimating()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    /**
     method runs in call time duration timer
     */
    func runInCallTimer() {
        self.inCallTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (ViewController.updateIncallTimer), userInfo: nil, repeats: true)
    }
    
    /**
     method updates in call time duration
     */
    @objc func updateIncallTimer()
    {
        totalSeconds += 1
        timeString(time: TimeInterval(totalSeconds))
    }
    
    /**
     method displays in call time duratio on view
     */
    func timeString(time:TimeInterval)  {
        //  let day = Int(time) / 86400
        let hours = Int(time) % 86400 / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        if hours == 0 {
            self.lb_incall_timer.text = String(format:"%02i:%02i", minutes,seconds)
        }else {
            self.lb_incall_timer.text =  String(format:"%02i:%02i:%02i", hours,minutes,seconds)
        }
    }
   
    /**
     Method hides connecting view and shows video call view of subscriber
     */
    func showPublisher() {
        print("Success****")
        
        if connectionTimer != nil {
            self.connectionTimer.invalidate()
            self.connectionTimer = nil
            self.sessionConnectSeconds = 0
        }
    
        DispatchQueue.main.async {
            self.stopAnimating()
            self.bt_mic.isEnabled = true
            self.bt_video.isEnabled = true
            self.bt_flip_camera.isEnabled = true
            self.runInCallTimer()
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.showHideTransitionViews, animations: {
                //self.topConnectingView.removeFromSuperview()
                self.topConnectingView.isHidden = true
            }, completion: nil)
        }
    }
   
    /**
     Method toggles if user tapped disable video by passing boolean value
     - Parameter hide: Takes True boolean value to hide or to show false should be passed
     */
    func addPublisherView(hide:Bool) {
        guard let publisherView = self.currentOngoingCall.publisher?.view else {
            return
        }
        
        DispatchQueue.main.async {
            if hide {
                publisherView.removeFromSuperview()
                self.vw_publisher.isHidden = hide
            }else {
                self.vw_publisher.isHidden = hide
                //            let screenBounds = UIScreen.main.bounds
                //            publisherView.frame = CGRect(x: screenBounds.width - 120 - 20, y: screenBounds.height - 120 - 20, width: 120, height: 120)
                //            publisherView.layer.cornerRadius = publisherView.frame.size.width / 2
                publisherView.frame =  self.vw_publisher.bounds
                self.vw_publisher.addSubview(publisherView)
            }
        }
    }
    
    
    func addShadowToNavView() {
        self.vw_navigation.layer.shadowRadius = 1
        self.vw_navigation.layer.shadowOpacity = 0.5
        self.vw_navigation.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.vw_navigation.clipsToBounds = false
        
    
    }
    
    @objc func didSwiperRight(_ gesture:UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizer.Direction.right:
            print("Swiped right")
             showDoctorDetails(show: false)
        case UISwipeGestureRecognizer.Direction.down:
          
            print("Swiped down>>>>>")
        case UISwipeGestureRecognizer.Direction.left:
            print("Swiped left")
             showDoctorDetails(show: true)
        case UISwipeGestureRecognizer.Direction.up:
            print("Swiped up")
        default:
            break
        }
    }

    
    func showDoctorDetails(show:Bool) {
        let screenWidth = UIScreen.main.bounds.width * 0.5
        self.LC_drDetailsViewWidth.constant = screenWidth
    
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            if show {
                self.LC_drDetailsViewTrailingConstraint.constant = 0
            }else {
                self.LC_drDetailsViewTrailingConstraint.constant = 500
            }
            
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
            
        })
    }
    
    func userTappedEndCall() {
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                // AppDelegate.shared.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                call.endCall()
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func doctorInfoTapped(_ sender: UIButton) {
        showDoctorDetails(show: true)
    }
    
    @IBAction func closeDoctorDetailsTapped(_ sender: UIButton) {
         showDoctorDetails(show: false)
    }
    
    @IBAction func videoTapped(_ sender: ControlButton) {
        print("Video tapped")
        if let publisher = self.currentOngoingCall.publisher {
            if hasVideo {
                publisher.publishVideo = false
                hasVideo = false
                self.addPublisherView(hide: true)
            }else {
                publisher.publishVideo = true
                hasVideo = true
                self.addPublisherView(hide: false)
                // updatePublisherVideo(connected: (self.hasAudio))
            }
            bt_video.setImage(hasVideo ? #imageLiteral(resourceName: "video_on") : #imageLiteral(resourceName: "video_off"), for: .normal)
        }
    }
    
    @IBAction func audioTapped(_ sender: ControlButton) {
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                if let publisher = call.publisher {
                    if hasAudio {
                        publisher.publishAudio = false
                        hasAudio = false
                        // AppDelegate.shared.providerDelegate?.callManager.setHeld(call: self.currentOngoingCall, onHold: hasAudio)
                    }else {
                        publisher.publishAudio = true
                        hasAudio = true
                        //  AppDelegate.shared.providerDelegate?.callManager.setHeld(call: self.currentOngoingCall, onHold: hasAudio)
                    }
                    
                    DispatchQueue.main.async {
                        self.setMute(call: call.uuid, isMuted: self.hasAudio)
                        self.bt_mic.setImage(publisher.publishAudio ? #imageLiteral(resourceName: "mic_off") : #imageLiteral(resourceName: "mic_on"), for: .normal)
                        print("Adudio tapped")
                    }
                }
            }
        }
    }
    
    @IBAction func enabeSpeakerTapped(_ sender: ControlButton) {
        if incomingCallType == 2 {
            if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
                for call in calls {
                    if let subscriber = call.subscriber {
                        if subscriber.subscribeToAudio {
                            subscriber.subscribeToAudio = false
                            bt_speaker_enable.setImage(  UIImage(named: "DisableSpeaker") , for: .normal)
                        }else {
                            subscriber.subscribeToAudio = true
                            bt_speaker_enable.setImage(  UIImage(named: "EnableSpeaker") , for: .normal)
                        }
                    }
                }
            }
        }else if incomingCallType == 1{
            audioToggleSession()
        }
    }
    
    @IBAction func flipCameraTapped(_ sender: ControlButton) {
        if hasVideo {
            if let publisher = self.currentOngoingCall.publisher {
                if isBackCameraOn {
                    publisher.cameraPosition = .front
                    isBackCameraOn = false
                }else {
                    publisher.cameraPosition = .back
                    isBackCameraOn = true
                }
            }
        }
    }
    
    @IBAction func endCallTapped(_ sender: UIButton) {
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
               // AppDelegate.shared.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                call.endCall()
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }else {
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CallConnectingViewController : SpeakerboxCallDelegate {
    
    func sessionHasAlreadyAUser(message:String) {
        print("Session has already a user:\(message)")
        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                //call.endCall()
            }
        }
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            let oka = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                App.timeDuration = ""
                  if self.connectionTimer != nil {
                    self.connectionTimer.invalidate()
                    self.sessionConnectSeconds = 0
                    self.stopAnimating()
                  }
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(oka)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- speaker delegate methods
    func sessionConnected(session: OTSession) {
        NSLog("*#*#*#== Session connected ==*#*#*#*")
//        self.showAlert(msg: "session connected")
    }
    
    func sessionDidDisConnected(session: OTSession) {
       // print("*#*#*#== Session disconnected <UUID> \(AppDelegate.shared.currentCallUUID!) ==*#*#*#*")
 
      //  print("End Native call from \(#function) uuid:\(AppDelegate.shared.currentCallUUID)")

        if let calls  = AppConfig.shared?.providerDelegate?.callManager.calls {
            for call in calls {
                AppConfig.shared?.providerDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                //call.endCall()
            }
        }
     
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    

    func subscriberJoined(subscriber: OTSubscriber,stream:OTStream) {
        NSLog("subscriberJoined")
//        self.showAlert(msg: "subscriber joined")

        if self.calls.count != 0 {
            let onCall = self.calls[0]
            
            self.lb_callerName.text = onCall.handle
            
            if onCall.hasConnected {
                if incomingCallType == 1{
                    print("Audio call")
                    if let publisher = onCall.publisher {
                         publisher.publishVideo = false
                        publisher.networkStatsDelegate = self
                    }
                }else if incomingCallType == 2{
                    if let publisherView = onCall.publisher?.view ,let publisher = onCall.publisher{
                        
                        publisherView.frame =  self.vw_publisher.bounds
                        self.vw_publisher.addSubview(publisherView)
                        self.vw_publisher.isHidden = false
                        publisher.publishVideo = true
                        publisher.networkStatsDelegate = self
                    }
                }

                guard let subscriberView = onCall.subscriber?.view else {
                    return
                }
                
                //network stats delegate
                subscriber.networkStatsDelegate = self
                print("subscriber : ",subscriber)
                
                subscriberView.translatesAutoresizingMaskIntoConstraints = false
                view.insertSubview(subscriberView, at: 0)
                let top =  NSLayoutConstraint(item: subscriberView, attribute: .top , relatedBy: .equal, toItem: self.vw_navigation, attribute: .bottom, multiplier: 1, constant: 0) ///changed
                let bottom =  NSLayoutConstraint(item: subscriberView, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0)
                let trailing =  NSLayoutConstraint(item: subscriberView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
                let leading =  NSLayoutConstraint(item: subscriberView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
                
                NSLayoutConstraint.activate([top, bottom, trailing, leading])
                self.subscriberView = subscriberView
                
                ///Adding Swipe gestures to doctor details view
                let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwiperRight(_:)))
              
                self.subscriberView.addGestureRecognizer(swipeRight)
             

                self.answerAction?.fulfill(withDateConnected: Date())
                print("No instance for answer call \( self.answerAction == nil ? true : false)")
  
//                AppUtility.lockOrientation(.allButUpsideDown)
                
                if stream.hasVideo {
                    if avatarImage != nil {
                        self.avatarImage.removeFromSuperview()
                        self.avatarImage = nil
                    }
                }else {
                    if incomingCallType == 2 {
                        addAvatarImageIfVideoNotAvailable()
                    }
                }
                
                 self.showPublisher()
            }
        }
    }
    
    func subscriberVideoDisabled(subscriber: OTSubscriberKit) {
        DispatchQueue.main.async {
            self.addAvatarImageIfVideoNotAvailable()
        }
    }
    
    func subscriberVideoEnabled(subscriber: OTSubscriberKit) {
        DispatchQueue.main.async {
            if self.avatarImage != nil {
                self.avatarImage.removeFromSuperview()
                self.avatarImage = nil
            }
        }
    }
    
    func subscriberDidConnect(subscriber: OTSubscriberKit) {
        print("subscriberDidConnect:")
        //checkVideoQualityShowInfo()
    }
    
}

extension CallConnectingViewController : OTSubscriberKitNetworkStatsDelegate {
    
    func subscriber(_ subscriber: OTSubscriberKit, videoNetworkStatsUpdated stats: OTSubscriberKitVideoNetworkStats) {
        NSLog("videoNetworkStatsUpdated")
        if (prevVideoTimestamp == 0)
        {
            prevVideoTimestamp = stats.timestamp;
            prevVideoBytes = stats.videoBytesReceived;
        }
        
        if stats.timestamp - prevVideoTimestamp >= Double(TIME_WINDOW) {
            let temp1:Double = Double(stats.videoBytesReceived)
            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevVideoBytes))
            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats.timestamp - prevVideoTimestamp)
            video_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
            
            self.processStats(stats: stats)
            prevVideoTimestamp = stats.timestamp
            prevVideoBytes = stats.videoBytesReceived
            print(String(format: "videoBytesReceived %llu, bps %ld, packetsLost %.2f", arguments: [stats.videoBytesReceived, video_bw, video_pl_ratio]))
            
            checkVideoQualityShowInfo()
        }
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, audioNetworkStatsUpdated stats: OTSubscriberKitAudioNetworkStats) {
        NSLog("audioNetworkStatsUpdated")
        if (prevAudioTimestamp == 0)
        {
            prevAudioTimestamp = stats.timestamp
            prevAudioBytes = stats.audioBytesReceived
        }
        
        if stats.timestamp - prevAudioTimestamp >= Double(TIME_WINDOW){
            let temp1:Double = Double(stats.audioBytesReceived)
            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevAudioBytes))
            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats.timestamp - prevAudioTimestamp)
            audio_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
            
            self.processStats(stats: stats)
            prevAudioTimestamp = stats.timestamp
            prevAudioBytes = stats.audioBytesReceived
            print(String(format: "audioBytesReceived %llu, bps %ld, packetsLost %.2f", arguments: [stats.audioBytesReceived, audio_bw,audio_pl_ratio]))
            if incomingCallType == 1
            {
                checkVideoQualityShowInfo()
            }
        }
    }
    
    private func checkVideoQualityShowInfo() {
        print("checkVideoQualityShowInfo")
        DispatchQueue.main.async {
            var canDoVideo : Bool = false
            if incomingCallType == 2{
                canDoVideo = self.video_bw >= 150000 && self.video_pl_ratio <= 0.03   //video_bw < 150000 || video_pl_ratio > 0.03
            }
            let canDoAudio : Bool  = self.audio_bw >= 25000 && self.audio_pl_ratio <= 0.05
            
            if !canDoVideo && incomingCallType == 2 {
                print("video bandwidth:\(self.video_bw)  PL-Ratio:\(self.video_pl_ratio)")
                self.showMessage(message: "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call.", duration: .seconds(seconds: 4))
    
                print("***\n Connectiviy is poor\n VIDEO CAN NOT BE RECIEVED \n****")
            }else if !canDoAudio && incomingCallType == 1 {
                print("video bandwidth:\(self.audio_bw)  PL-Ratio:\(self.audio_pl_ratio)")
                self.showMessage(message: "Your data bandwidth is low, our doctor will call you shortly using regular call.", duration: .seconds(seconds: 4))
                
                print("***\n Connectiviy is poor\n Audio CAN NOT BE RECIEVED \n****")
            }else {
                print("***\n Connectiviy is Good\n VIDEO or Audio CAN BE RECIEVED \n****")
               
            }
            
            self.lb_bw.text = "Internet Speed : " //+ String(self.video_bw) + " bps"
            
            if incomingCallType == 2
            {
                print("video bandwidth:\(self.video_bw)  PL-Ratio:\(self.video_pl_ratio)")
                print("video bandwidth kpbs %.2f : ",(Float(self.video_bw)/1024.0))
                print("video bandwidth mpbs %.2f : ",((Float(self.video_bw)/1024.0)/1024.0))
                let readmoreFont = UIFont.boldSystemFont(ofSize: 15.0)
//                var readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                self.lb_bw.addTrailing(with: String(self.video_bw), moreText: "bps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                
                //if self.video_bw > 1000{
                    //self.lb_bw.text = "Internet Speed : " + String(format:"%.2f",(Float(self.video_bw)/1024.0)) + " kbps"
                    if (self.video_bw/1024) >= 200
                    {
                        self.lb_bw.text = "Internet Speed : "
//                        readmoreFontColor = self.hexStringToUIColor(hex: "FFA500")
//                        self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.video_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                        //self.showMessage(message: "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call.", duration: .seconds(seconds: 4))
                    }
                    else
                    {
                        self.lb_bw.text = "Internet Speed : "
//                        readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                        self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.video_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                    }
                
                    if (self.video_bw/1024) >= 500
                    {
                        self.lb_bw.text = "Internet Speed : "
//                        readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                        self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.video_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                    }
                
                //}
                if (self.video_bw/1024) > 1000
                {
                    //self.lb_bw.text = "Internet Speed  : " + String(format:"%.2f",(Float(self.video_bw)/1024.0)/1024.0) + " mbps"
                    self.lb_bw.text = "Internet Speed : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.video_bw)/1024.0)/1024.0), moreText: " mbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
            }
            else if incomingCallType == 1
            {
                print("audio_bw bandwidth:\(self.audio_bw)  PL-Ratio:\(self.audio_pl_ratio)")
                print("audio_bw bandwidth kpbs %.2f : ",(Float(self.audio_bw)/1024.0))
                print("audio_bw bandwidth mpbs %.2f : ",((Float(self.audio_bw)/1024.0)/1024.0))
                let readmoreFont = UIFont.boldSystemFont(ofSize: 15.0)
//                var readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                self.lb_bw.addTrailing(with: String(self.audio_bw), moreText: "bps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                
                if (self.audio_bw/1024) >= 25
                {
                    self.lb_bw.text = "Internet Speed : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "FFA500")
//                    self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.audio_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                    //self.showMessage(message: "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call.", duration: .seconds(seconds: 4))
                }
                else
                {
                    self.lb_bw.text = "Internet Speed : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                    self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.audio_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                
                if (self.audio_bw/1024) >= 30
                {
                    self.lb_bw.text = "Internet Speed : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.audio_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                
                
                if (self.audio_bw/1024) > 1000
                {
                    //self.lb_bw.text = "Internet Speed  : " + String(format:"%.2f",(Float(self.audio_bw)/1024.0)/1024.0) + " mbps"
                    self.lb_bw.text = "Internet Speed : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lb_bw.addTrailing(with: String(format:"%.2f",(Float(self.audio_bw)/1024.0)/1024.0), moreText: " mbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
            }
        }
    }
    
    func showMessage(message:String,duration:SwiftMessages.Duration) {
        let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
        view.configureTheme(.warning)
        view.configureDropShadow()
        view.configureContent(title: "", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = duration
        SwiftMessages.show(config: config, view: view)
    }
    
    private func processStats(stats:AnyObject) {
         print("processStats")
        if stats.isKind(of:OTSubscriberKitVideoNetworkStats.self) {
            video_pl_ratio = -1
            let videoStats : OTSubscriberKitVideoNetworkStats = stats as! OTSubscriberKitVideoNetworkStats
            if (prevVideoPacketsRcvd != 0) {
                let pl : UInt64  = videoStats.videoPacketsLost - prevVideoPacketsLost
                let pr : UInt64  = videoStats.videoPacketsReceived - prevVideoPacketsRcvd
                let pt : UInt64  = pl + pr
                if pt > 0 {
                    video_pl_ratio = Double(pl / pt)
                }
            }
            prevVideoPacketsLost = videoStats.videoPacketsLost;
            prevVideoPacketsRcvd = videoStats.videoPacketsReceived;
        }
        else if stats.isKind(of:OTSubscriberKitAudioNetworkStats.self)
        {
            audio_pl_ratio = -1
            let audioStats : OTSubscriberKitAudioNetworkStats = stats as! OTSubscriberKitAudioNetworkStats
            if (prevAudioPacketsRcvd != 0) {
                let pl : UInt64 = audioStats.audioPacketsLost - prevAudioPacketsLost
                let pr : UInt64  = audioStats.audioPacketsReceived - prevAudioPacketsRcvd
                let pt : UInt64  = pl + pr
                if pt > 0 {
                    audio_pl_ratio =  Double(pl) / Double(pt)
                }
            }
            prevAudioPacketsLost = audioStats.audioPacketsLost
            prevAudioPacketsRcvd = audioStats.audioPacketsReceived
        }
        
    }
    
}

extension CallConnectingViewController : OTPublisherKitNetworkStatsDelegate{
    func publisher(_ publisher: OTPublisherKit, audioNetworkStatsUpdated stats: [OTPublisherKitAudioNetworkStats]) {
        print("Publisher audioNetworkStatsUpdated")
        print("stats.audioBytesSent : ",stats[0].audioBytesSent)
        if (prevPubAudioTimestamp == 0)
        {
            prevPubAudioTimestamp = stats[0].timestamp
            prevPubAudioBytes = UInt64(stats[0].audioBytesSent)
        }
        
        if stats[0].timestamp - prevPubAudioTimestamp >= Double(TIME_WINDOW){
            let temp1:Double = Double(stats[0].audioBytesSent)
            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevPubAudioBytes))
            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats[0].timestamp - prevPubAudioTimestamp)
            audioPub_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
            
            self.processPubStats(stats: stats[0])
            prevPubAudioTimestamp = stats[0].timestamp
            prevPubAudioBytes = UInt64(stats[0].audioBytesSent)
        print(String(format: "audioBytesSent %llu, bps %ld, packetsLost %.2f", arguments: [stats[0].audioBytesSent, audioPub_bw,audio_pub_pl_ratio]))
            if incomingCallType == 1
            {
                checkPubVideoQualityShowInfo()
            }
        }
        
    }
    
    func publisher(_ publisher: OTPublisherKit, videoNetworkStatsUpdated stats: [OTPublisherKitVideoNetworkStats]) {
        print("Publisher videoNetworkStatsUpdated")
        print("stats.videoBytesSent : ",stats[0].videoBytesSent)
        if (prevPubVideoTimestamp == 0)
        {
            prevPubVideoTimestamp = stats[0].timestamp
            prevPubVideoBytes = UInt64(stats[0].videoBytesSent)
        }
        
        if stats[0].timestamp - prevPubVideoTimestamp >= Double(TIME_WINDOW) {
            let temp1:Double = Double(stats[0].videoBytesSent)
            let temp12:CUnsignedLongLong = CUnsignedLongLong(temp1-Double(prevPubVideoBytes))
            let temp2:CUnsignedLongLong = CUnsignedLongLong(stats[0].timestamp - prevPubVideoTimestamp)
            videoPub_bw = CLong((8 * temp12) / (temp2 / CUnsignedLongLong(1000)))
            
            self.processPubStats(stats: stats[0])
            prevPubVideoTimestamp = stats[0].timestamp
            prevPubVideoBytes = UInt64(stats[0].videoBytesSent)
            print(String(format: "videoBytesSent %llu, bps %ld, packetsLost %.2f", arguments: [stats[0].videoBytesSent, videoPub_bw, video_pub_pl_ratio]))
            
            checkPubVideoQualityShowInfo()
        }
    }
    private func checkPubVideoQualityShowInfo() {
        print("checkPubQualityShowInfo")
        DispatchQueue.main.async {
            var canDoVideo : Bool = false
            if incomingCallType == 2{
                canDoVideo = self.videoPub_bw >= 150000 && self.video_pub_pl_ratio <= 0.03   //videoPub_bw < 150000 || video_pub_pl_ratio > 0.03
            }
            let canDoAudio : Bool  = self.audioPub_bw >= 25000 && self.audio_pub_pl_ratio <= 0.05
            
            if !canDoVideo && incomingCallType == 2 {
                print("video bandwidth:\(self.videoPub_bw)  PL-Ratio:\(self.video_pub_pl_ratio)")
                self.showMessage(message: "Your data bandwidth is low, please book an audio appointment. Else our doctor will call you shortly using regular call.", duration: .seconds(seconds: 4))
                
                print("***\n Connectiviy is poor\n VIDEO CAN NOT BE RECIEVED \n****")
            }else if !canDoAudio && incomingCallType == 1 {
                print("video bandwidth:\(self.audioPub_bw)  PL-Ratio:\(self.audio_pub_pl_ratio)")
                self.showMessage(message: "Your data bandwidth is low, our doctor will call you shortly using regular call.", duration: .seconds(seconds: 4))
                
                print("***\n Connectiviy is poor\n Audio CAN NOT BE RECIEVED \n****")
            }else {
                print("***\n Connectiviy is Good\n VIDEO or Audio CAN BE RECIEVED \n****")
                
            }
            
            self.lbPub_bw.text = "Your Speed : " //+ String(self.videoPub_bw) + " bps"
            
            if incomingCallType == 2
            {
                print("video Pub bandwidth:\(self.videoPub_bw)  PL-Ratio:\(self.video_pub_pl_ratio)")
                print("video Pub bandwidth kpbs %.2f : ",(Float(self.videoPub_bw)/1024.0))
                print("video Pub bandwidth mpbs %.2f : ",((Float(self.videoPub_bw)/1024.0)/1024.0))
                let readmoreFont = UIFont.boldSystemFont(ofSize: 15.0)
//                 readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                self.lbPub_bw.addTrailing(with: String(self.videoPub_bw), moreText: "bps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                
                //if self.videoPub_bw > 1000{
                //self.lbPub_bw.text = "Internet Speed : " + String(format:"%.2f",(Float(self.videoPub_bw)/1024.0)) + " kbps"
                if (self.videoPub_bw/1024) >= 200
                {
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "FFA500")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.videoPub_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                else
                {
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.videoPub_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                
                if (self.videoPub_bw/1024) >= 500
                {
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.videoPub_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                
                //}
                if (self.videoPub_bw/1024) > 1000
                {
                    //self.lbPub_bw.text = "Internet Speed  : " + String(format:"%.2f",(Float(self.videoPub_bw)/1024.0)/1024.0) + " mbps"
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.videoPub_bw)/1024.0)/1024.0), moreText: " mbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
            }
            else if incomingCallType == 1
            {
                print("audioPub_bw bandwidth:\(self.audioPub_bw)  PL-Ratio:\(self.audio_pub_pl_ratio)")
                print("audioPub_bw bandwidth kpbs %.2f : ",(Float(self.audioPub_bw)/1024.0))
                print("audioPub_bw bandwidth mpbs %.2f : ",((Float(self.audioPub_bw)/1024.0)/1024.0))
                let readmoreFont = UIFont.boldSystemFont(ofSize: 15.0)
//                var readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                self.lbPub_bw.addTrailing(with: String(self.audioPub_bw), moreText: "bps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                
                if (self.audioPub_bw/1024) >= 25
                {
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "FFA500")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.audioPub_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                else
                {
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "FF0000")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.audioPub_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                
                if (self.audioPub_bw/1024) >= 30
                {
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.audioPub_bw)/1024.0)), moreText: " kbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                
                
                if (self.audioPub_bw/1024) > 1000
                {
                    //self.lbPub_bw.text = "Internet Speed  : " + String(format:"%.2f",(Float(self.audioPro_bw)/1024.0)/1024.0) + " mbps"
                    self.lbPub_bw.text = "Streaming stats : "
//                    readmoreFontColor = self.hexStringToUIColor(hex: "6DBE45")
//                    self.lbPub_bw.addTrailing(with: String(format:"%.2f",(Float(self.audioPub_bw)/1024.0)/1024.0), moreText: " mbps", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
            }
        }
    }
    
    private func processPubStats(stats:AnyObject) {
        print("processStats")
        if stats.isKind(of:OTPublisherKitVideoNetworkStats.self) {
            video_pub_pl_ratio = -1
            let videoStats : OTPublisherKitVideoNetworkStats = stats as! OTPublisherKitVideoNetworkStats
            if (prevPubVideoPacketsSent != 0) {
                let pl : UInt64  = UInt64(videoStats.videoBytesSent) - prevPubVideoPacketsLost
                let pr : UInt64  = UInt64(videoStats.videoBytesSent) - prevPubVideoPacketsSent
                let pt : UInt64  = pl + pr
                if pt > 0 {
                    video_pub_pl_ratio = Double(pl / pt)
                }
            }
            prevPubVideoPacketsLost = UInt64(videoStats.videoPacketsLost)
            prevPubVideoPacketsLost = UInt64(videoStats.videoPacketsSent)
        }
        else if stats.isKind(of:OTSubscriberKitAudioNetworkStats.self)
        {
            audio_pub_pl_ratio = -1
            let audioStats : OTPublisherKitAudioNetworkStats = stats as! OTPublisherKitAudioNetworkStats
            if (prevPubAudioPacketsRcvd != 0) {
                let pl : UInt64 = UInt64(audioStats.audioPacketsLost) - prevPubAudioPacketsLost
                let pr : UInt64  = UInt64(audioStats.audioBytesSent) - prevPubAudioPacketsRcvd
                let pt : UInt64  = pl + pr
                if pt > 0 {
                    audio_pub_pl_ratio =  Double(pl) / Double(pt)
                }
            }
            prevPubAudioPacketsLost = UInt64(audioStats.audioPacketsLost)
            prevPubAudioPacketsRcvd = UInt64(audioStats.audioBytesSent)
        }
        
    }
}


extension CallConnectingViewController : CallKitSessionDelegates {
    
    ///Mark Call session delegates
    
    func didUserEndCall(uuid: UUID) {
        print("\(#function)")
        //self.endNative(call: uuid) 123
       // AppDelegate.shared.providerDelegate?.callManager.end(call: <#T##SpeakerboxCall#>)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didUserAnsweredCall(uuid: UUID) {
        
    }
    
    func didAudioSessionIsActivated(uuid: UUID) {
        if incomingCallType == 1 {
            self.audioToggleSession()
        }
    }
    
    func didAudioSessionIsDeactivated(uuid: UUID) {
        
    }
    
    func didReset(uuid: UUID) {
        
    }
    
    func didUserKeptCallOnHold(uuid: UUID, isKeptOnHold: Bool) {
        
    }
    
    func didUserMutedAudio(uuid: UUID, isMuted: Bool) {
        DispatchQueue.main.async {
            self.bt_mic.setImage(isMuted ? #imageLiteral(resourceName: "mic_off") : #imageLiteral(resourceName: "mic_on"), for: .normal)
        }
    }
}











// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionPort(_ input: AVAudioSession.Port) -> String {
    return input.rawValue
}


extension UIViewController{
    
    
    func checkCameraPermission() -> Bool
    {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
            
        case .authorized:
            // Access is granted by user.
            return true
        case .notDetermined:
            // It is not determined until now.
            return true
            
        case .restricted:
            // User do not have access to camera.
            return false
            
        case .denied:
            // User has denied the permission.
            return false
        }
    }
    
    
    func checkAudioPermission() -> Bool {
        let cameraMediaType = AVMediaType.audio
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
            
        case .authorized:
            // Access is granted by user.
            return true
        case .notDetermined:
            // It is not determined until now.
            return true
            
        case .restricted:
            // User do not have access to camera.
            return false
            
        case .denied:
            // User has denied the permission.
            return false
        }
    }
    
    func showAlert(msg: String){
        let tokens = msg
        let alert = UIAlertController(title: "tokens", message: tokens, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}


