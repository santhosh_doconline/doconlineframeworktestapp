//
//  UIHomeCollectionViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 08/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class UIHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lb_title: UILabel!
    @IBOutlet var iv_image: UIImageView!
    @IBOutlet var tv_info: UILabel!
    
    @IBOutlet var vw_back: UIView!
    @IBOutlet var lb_strightLine: UILabel!
    
    @IBOutlet var iv_appIcon: UIImageView!
    @IBOutlet var lb_appTime: UILabel!
    @IBOutlet var lb_doctorName: UILabel!
    @IBOutlet var lb_doctorSpecialisation: UILabel!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.vw_back.layer.cornerRadius = 3.0
        self.vw_back.layer.shadowRadius = 0.5
        self.vw_back.layer.shadowOpacity = 0.8
        self.vw_back.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.vw_back.clipsToBounds = false
        
        let vwGrad = GradientView()
        vwGrad.frame = CGRect(x: 0.0, y: 0.0, width: self.lb_strightLine.frame.size.width, height: self.lb_strightLine.frame.size.height)
        vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.lb_strightLine.addSubview(vwGrad)
        
    }
    
    
}
