//
//  UIOrederProcessViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 06/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class UIOrederProcessViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet var lb_firstStepLine: UILabel!
    @IBOutlet var lb_secondStepLine: UILabel!
    @IBOutlet var lb_thirdStepLine: UILabel!
    
    
    ///Top Buttons
    @IBOutlet var bt_enterPincode: UIButton!
    @IBOutlet var bt_editMedicines: UIButton!
    @IBOutlet var bt_updateAddress: UIButton!
    @IBOutlet var bt_confirmOrder: UIButton!
    
    //top Labels
    @IBOutlet var lb_enterPincode: UILabel!
    @IBOutlet var lb_editMedicines: UILabel!
    @IBOutlet var lb_updateAddress: UILabel!
    @IBOutlet var lb_confirmOrder: UILabel!
    
    ///Views to show
    @IBOutlet var vw_enterPincode: UIView!
    @IBOutlet var vw_medicines: UIView!
    @IBOutlet var vw_address: UIView!
    @IBOutlet var vw_placeOrder: UIView!
    
    //Layout constraints
    @IBOutlet var LC_enterPincodeHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet var LC_placeOrderHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet var LC_addressHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet var LC_medicinesHorizontalConstraint: NSLayoutConstraint!
    

    ///view tags
    let PINCODE_VIEW_TAG = 1
    let EDIT_MEDICINES_VIEW_TAG = 2
    let ADDRESS_VIEW_TAG = 3
    let PLACE_ORDER_VIEW_TAG = 4
    
    //active view
    var activViewTag = 1
    
    //finihsed views
    var finishedFillingViews = [Int]()
    
    ///Instance for appointment id
    var appointment_id = ""
    
    //MARK:- Pincode field step one outlets
    @IBOutlet var tf_enterPincode: UITextField!
    @IBOutlet var bt_pincodeNext: UIButton!
    @IBOutlet var lb_deliveryMessage: UILabel!
    
    //MARK:- Medicine field step two outlets
    @IBOutlet var medicineTableView: UITableView!
    @IBOutlet var bt_medicinesNext: UIButton!
    @IBOutlet var lb_noMedicinesStatus: UILabel!
    
    //MARK:- address field step three outlets
    @IBOutlet var tf_addressLine1: UITextField!
    @IBOutlet var tf_addressLine2: UITextField!
    @IBOutlet var bt_addressStepNext: UIButton!
    
    //MARK:- Confirm order step four outlets
    @IBOutlet var lb_pincode: UILabel!
    @IBOutlet var lb_orderDate: UILabel!
    @IBOutlet var lb_mobile_no: UILabel!
    @IBOutlet var lb_address_line1: UILabel!
    @IBOutlet var lb_addressLine2: UILabel!
    @IBOutlet var placeOrderTableView: UITableView!
    @IBOutlet var LC_medicineTableViewHeight: NSLayoutConstraint!
    @IBOutlet var lb_subtotal: UILabel!
    @IBOutlet var bt_placeOrder: UIButton!
    
    ///Subtotal instance
    var subTotal : Double = 0.0
    ///instance of string type dictionary
    var dataDict = [String:String]()
    ///Entire concatenated addrress
    var totalAddress = ""
    ///Only line one for textfields
    var address1 = ""
    ///Only line two for textfields
    var address2 = ""
    var orderID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lb_deliveryMessage.text = "Where the medicine should be delivered\nMinimum order Rs.200/-\nPayment Method: COD Only"
        setUpViewActiveOnLoad()
        
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipeRightAction(recognizer:)))
        swipeRightGesture.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRightGesture)
        
        medicineTableView.tableFooterView = UIView(frame: CGRect.zero)
        placeOrderTableView.tableFooterView = UIView(frame: CGRect.zero)
//        self.bt_updateAddress.isEnabled = false
//        self.bt_confirmOrder.isEnabled = false
        
        self.checkPrescriptionIsAvailableToOrder()
    }
    
    
    /**
     Unwind segue action
     */
    @IBAction func unwindToMedicineOrderView(segue:UIStoryboardSegue) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_pincodeNext.backgroundColor = Theme.buttonBackgroundColor
        bt_addressStepNext.backgroundColor = Theme.buttonBackgroundColor
        bt_placeOrder.backgroundColor = Theme.buttonBackgroundColor
    }
    
    /// Checks the appointment prescription is available to order
    func checkPrescriptionIsAvailableToOrder() {
        NetworkCall.performGet(url: AppURLS.PRESCRIPTION_HAS_ORDER + self.appointment_id) { (success, response, statusCode, error) in
            if success ,let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? Bool {
                let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String
                if data {
                    DispatchQueue.main.async {
                      self.didShowAlert(title: "", message: message ?? "")
                    }
                }
            }
        }
    }
    
    
    /**
     Method returns country name by id
     - Parameter countryID: pass the country id
     - Returns: country name
     */
    func getCountryNameById(countryID:Int) -> String{

        if countryID == 0 {
            print("no country selected")
            return ""
        }
        var countryToShow = ""
        
        if let countries = UserDefaults.standard.object(forKey: "ListCountry") as? NSDictionary {
            if let ids = countries.allKeys as? [String] {
                for id in ids {
                    if id == "\(countryID)" {
                        countryToShow = countries.object(forKey: id) as! String
                    }
                }
            }
        }else {
            print("Not stored list")
        }
        return countryToShow
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let currentDate = "\(Date())"
        let dateString = currentDate.components(separatedBy: " ")
        let formatedDate = getFormattedDateOnly(dateString: "\(dateString[0])")
        self.lb_orderDate.text = formatedDate
        
        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
            let userDataModel  = User.get_user_profile_default()
            if let mobile = userDataModel.phone {
                self.lb_mobile_no.text = mobile
            }else {
                self.lb_mobile_no.text = ""
            }

            if let address1 = userDataModel.address1, let city = userDataModel.city,let state = userDataModel.state ,let country = userDataModel.country{
                self.totalAddress = ""
                
                self.tf_addressLine1.text = address1
                
                if let address2 = userDataModel.address2 {
                    self.totalAddress = address2
                }
                print("Line1:\(address1) Line2:\(self.totalAddress) city:\(city) state:\(state) country:\(country)")
                if !city.isEmpty {
                    totalAddress += totalAddress.isEmpty ? "\(city)" : ",\(city)"
                }
                
                if !state.isEmpty {
                    totalAddress += ",\(state)"
                }
                
                if country != 0 {
                    totalAddress +=  totalAddress.isEmpty ? "" : ",\(getCountryNameById(countryID: country))"
                }
                
                self.tf_addressLine2.text = totalAddress
                self.tf_enterPincode.text = userDataModel.pincode
            }
        }
    }

    ///Setups the horizontal constraints of all views on viewDidLoad
    func setUpViewActiveOnLoad() {
        self.setViewWithTagActive(tag: 1)
        self.setGreenCheckForFinishedViews(tag: 1)
    }
    
    
    ///Setups the image and text colors for finished views
    func setGreenCheckForFinishedViews(tag:Int) {
        switch tag {
        case self.PINCODE_VIEW_TAG :
            self.navigationItem.title = "Enter Pincode"
            self.bt_enterPincode.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_enterPincode.textColor =  hexStringToUIColor(hex: "#6DBF00")
            self.bt_editMedicines.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.lb_editMedicines.textColor = UIColor.darkGray
            self.bt_updateAddress.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.lb_updateAddress.textColor = UIColor.darkGray
            self.bt_confirmOrder.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.lb_confirmOrder.textColor = UIColor.darkGray
            self.lb_firstStepLine.backgroundColor = UIColor.darkGray
            self.lb_secondStepLine.backgroundColor = UIColor.darkGray
            self.lb_thirdStepLine.backgroundColor = UIColor.darkGray
            
        case self.EDIT_MEDICINES_VIEW_TAG :
            self.navigationItem.title = "Edit Medicines"
            self.bt_enterPincode.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_enterPincode.textColor =  hexStringToUIColor(hex: "#6DBF00")
            self.bt_editMedicines.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_editMedicines.textColor =  hexStringToUIColor(hex: "#6DBF00")
            self.bt_updateAddress.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.lb_updateAddress.textColor = UIColor.darkGray
            self.bt_confirmOrder.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.lb_confirmOrder.textColor = UIColor.darkGray
            self.lb_firstStepLine.backgroundColor = hexStringToUIColor(hex: "#6DBF00")
            self.lb_secondStepLine.backgroundColor = UIColor.darkGray
            self.lb_thirdStepLine.backgroundColor = UIColor.darkGray
           // self.bt_updateAddress.isEnabled = true
            
        case self.ADDRESS_VIEW_TAG :
            self.navigationItem.title = "Update Address"
            self.bt_enterPincode.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_enterPincode.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.bt_editMedicines.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_editMedicines.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.bt_updateAddress.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_updateAddress.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.bt_confirmOrder.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.lb_confirmOrder.textColor = UIColor.darkGray
            self.lb_firstStepLine.backgroundColor = hexStringToUIColor(hex: "#6DBF00")
            self.lb_secondStepLine.backgroundColor =  hexStringToUIColor(hex: "#6DBF00")
            self.lb_thirdStepLine.backgroundColor = UIColor.darkGray
           // self.bt_confirmOrder.isEnabled = true
            
        case self.PLACE_ORDER_VIEW_TAG :
            self.navigationItem.title = "Confirm Order"
            self.bt_enterPincode.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_enterPincode.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.bt_editMedicines.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_editMedicines.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.bt_updateAddress.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_updateAddress.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.bt_confirmOrder.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.lb_confirmOrder.textColor = hexStringToUIColor(hex: "#6DBF00")
            self.lb_firstStepLine.backgroundColor = hexStringToUIColor(hex: "#6DBF00")
            self.lb_secondStepLine.backgroundColor =  hexStringToUIColor(hex: "#6DBF00")
            self.lb_thirdStepLine.backgroundColor = hexStringToUIColor(hex: "#6DBF00")
            
        default :
            print("Don't which view to set active")
        }
    }
    
    
    ///Changes the horizontal constraints for views to animate when clicked on next
    func setViewWithTagActive(tag:Int) {
        
        self.view.layoutIfNeeded()
        
        switch tag {
            
        case self.PINCODE_VIEW_TAG :
            
            self.LC_enterPincodeHorizontalConstraint.constant = 0
            self.LC_medicinesHorizontalConstraint.constant = 500
            self.LC_addressHorizontalConstraint.constant = 500
            self.LC_placeOrderHorizontalConstraint.constant = 500
            
        case self.EDIT_MEDICINES_VIEW_TAG :
            
            self.LC_enterPincodeHorizontalConstraint.constant = -1000
            self.LC_medicinesHorizontalConstraint.constant = 0
            self.LC_addressHorizontalConstraint.constant = 500
            self.LC_placeOrderHorizontalConstraint.constant = 500
            
        case self.ADDRESS_VIEW_TAG :
            
            self.LC_enterPincodeHorizontalConstraint.constant = -1000
            self.LC_medicinesHorizontalConstraint.constant = -1000
            self.LC_addressHorizontalConstraint.constant = 0
            self.LC_placeOrderHorizontalConstraint.constant = 500
            
        case self.PLACE_ORDER_VIEW_TAG :
            
            self.LC_enterPincodeHorizontalConstraint.constant = -1000
            self.LC_medicinesHorizontalConstraint.constant = -1000
            self.LC_addressHorizontalConstraint.constant = -1000
            self.LC_placeOrderHorizontalConstraint.constant = 0
            
        default :
            print("Don't which view to set active")
            
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) in
            if finished {
                self.activViewTag = tag
                //   self.finishedFillingViews.append(tag)
                print("Active veiw tag:\(self.activViewTag)")
            }
        })
    }
    
    
    func isPicodeFieldEmpty() -> Bool{
       return false
    }
    
    func selectedTopOption(tag:Int) {
        if tag ==  self.PINCODE_VIEW_TAG {
            self.setGreenCheckForFinishedViews(tag: tag)
            self.setViewWithTagActive(tag: tag)
        }
       else  if tag ==  self.EDIT_MEDICINES_VIEW_TAG {
            self.setGreenCheckForFinishedViews(tag: tag)
            self.setViewWithTagActive(tag: tag)
        }else if tag == self.ADDRESS_VIEW_TAG {
            self.setGreenCheckForFinishedViews(tag: tag)
            self.setViewWithTagActive(tag: tag)
        }else if tag == self.PLACE_ORDER_VIEW_TAG {
            self.setGreenCheckForFinishedViews(tag: tag)
            self.setViewWithTagActive(tag: tag)
        }
    }
    
    func validateTheFieldsBeforeTransition(tag:Int) {
        switch  tag {
        case self.PINCODE_VIEW_TAG:
            print("picode view is showing")
            if self.activViewTag > self.PINCODE_VIEW_TAG {
                self.selectedTopOption(tag: tag)
            }
        case self.EDIT_MEDICINES_VIEW_TAG:
            print("medicines view is showing")
            if self.activViewTag > self.EDIT_MEDICINES_VIEW_TAG{
                self.selectedTopOption(tag: tag)
            }
//            else if self.activViewTag == self.PINCODE_VIEW_TAG {
//                let picode = self.tf_enterPincode.text! as String
//                if picode.isEmpty {
//                    self.didShowAlert(title: "", message: "please enter pincode")
//                }else {
//                    self.selectedTopOption(tag: tag)
//                }
//            }
        case self.ADDRESS_VIEW_TAG:
            print("address view is showing")
            if self.activViewTag > self.ADDRESS_VIEW_TAG{
                self.selectedTopOption(tag: tag)
            }
//            else if self.activViewTag == self.ADDRESS_VIEW_TAG {
//                self.validateMedicinesForOutOfStock()
//            }
        case self.PLACE_ORDER_VIEW_TAG:
            print("Place order view is showing")
            if self.activViewTag == self.ADDRESS_VIEW_TAG  {
                self.validateAddressFileds()
            }
        default:
             print("no view is show")
        }
       
    }
    
    ///Checks the medicines are out of stock or not
    func validateMedicinesForOutOfStock() {
        if App.prescribedMedicines.count == 0 {
            self.didShowAlert(title: "", message: "It looks like, You don't have a list of medicines to proceed or order.")
            // AlertView.sharedInsance.showInfo(title: "", message: "it Looks like, you don't have a list of medicines to proceed or order.")
            return
        }
        
        for medicine in App.prescribedMedicines {
            print("Having quanitity is:\(medicine.availableQty!)")
            let availableQuantity = Double(medicine.availableQty!) == nil ? 0 : Double(medicine.availableQty!)!
            print("Available quantity :\(availableQuantity)")
            if medicine.availableQty! == "0.00" || medicine.availableQty! == "0.0" || medicine.availableQty! == "0" || availableQuantity == 0 {
                print("\(medicine.name!)Medicine is out of stock")
                self.didShowAlert(title: "", message: "\(medicine.name!) is out of stock, Please remove it to continue.")
                //  AlertView.sharedInsance.showInfo(title: "", message: "\(medicine.name!) is out of stock, Please remove it to continue.")
                return
            }
                //            else if medicine.maxPacks! > availableQuantity
                //            {
                //                print("\(medicine.name!)Medicine quantity is zero")
                //                self.didShowAlert(title: "", message: "\(medicine.name!) Selected quanity is more than stock available.You can not order more than \(Int(availableQuantity)) packs")
                //                return
                //            }
            else if medicine.qty! == 0 {
                print("\(medicine.name!)Medicine quantity is zero")
                self.didShowAlert(title: "", message: "Quantity can not be zero.Please select quantity for \(medicine.name!) or delete it by swiping left")
                //   AlertView.sharedInsance.showInfo(title: "", message: "Quantity can not be zero.Please select quantity for \(medicine.name!) or delete it by swiping left")
                return
            }
        }
        var subTtl = 0.0
        for medicine in App.prescribedMedicines {
            let total =  Double(medicine.maxPacks!) * Double(medicine.price!)!
            subTtl += total
        }
        if(subTtl < 200.0)
        {
            self.didShowAlert(title: "", message: "Minimum Order Rs.200/-")
            return
        }
        
       // App.isFromProcureMedicine = true
        self.selectedTopOption(tag: 3)
    }
    
    ///Removes all out of stock medicines from list
    func removeAllOutOfStockMedicines() {
        for (index,medicine) in App.prescribedMedicines.enumerated() {
            print("Having quanitity is:\(medicine.availableQty!)")
            let availableQuantity = Double(medicine.availableQty!) == nil ? 0 : Double(medicine.availableQty!)!
            print("Available quantity :\(availableQuantity)")
            if medicine.availableQty! == "0.00" || medicine.availableQty! == "0.0" || medicine.availableQty! == "0" || availableQuantity == 0 {
                print("\(medicine.name!)Medicine is out of stock")
              
            }
        }
    }
    
    ///Verifies the address fields or not empty and performs action
    func validateAddressFileds() {
        let address1 = self.tf_addressLine1.text! as String
        _ = self.tf_addressLine2.text! as String
        
        if address1.isEmpty {
            self.didShowAlert(title: "Info", message: "Address line1 required")
        }else if address1.count < 10  {
            self.didShowAlert(title: "Address line1", message: "Must contain atleast 10 characters")
        }else {
            self.lb_pincode.text = self.tf_enterPincode.text
            self.lb_address_line1.text = self.tf_addressLine1.text
            self.lb_addressLine2.text = self.tf_addressLine2.text
            self.placeOrderTableView.reloadData()
            self.calculateSubTotal()
            
            self.selectedTopOption(tag: 4)
        }
    }
    
    ///Handles right swipe gesture
    @objc func handleSwipeRightAction(recognizer:UIPanGestureRecognizer) {
        if self.activViewTag > 1 {
            let swipeToTag = self.activViewTag - 1
             self.selectedTopOption(tag: swipeToTag)
        }
    }
    
    
//MARK:- Enter pincode step functions
    
    ///Performs rest call for entered pincode
    /// - Parameter pinData: takes the pincode string
    func postPincode(pinData:String) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        startAnimating()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let procureURL = AppURLS.URL_Procure_medicine + appointment_id
        var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = pinData.data(using: String.Encoding.utf8)
        session.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    print("Error==> : \(error.localizedDescription)")
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus
                    {
                        print("performing error handling procure medicine\(resultJSON)")
                        if httpResponse.statusCode == 412 {
                            if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                    DispatchQueue.main.async {
                                        self.stopAnimating()
                                        App.isFromView = FromView.MedineOrder
                                      //  self.showVerifyMobileMessage(message: mobile, fromView: FromView.MedineOrder)
                                         self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.MedineOrder)
                                    }
                                }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
                                    
                                    DispatchQueue.main.async {
                                        self.stopAnimating()
                                        self.didShowAlert(title: "", message: email)
                                    }
                                }
                            }
                        }else if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                            DispatchQueue.main.async(execute: {
                                self.didShowAlert(title: "", message: message)
                            })
                        }
                    }
                    else {
                        print("procure medicine response:\(resultJSON)")
                        App.prescribedMedicines.removeAll()
                        if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                            for (key,_) in data {
                                if let values = data.object(forKey: key) as? NSDictionary
                                {
                                    print("medicine key: \(key as! String)")
                                    var discount = 0
                                    var available = 0
                                    
                                    if let disc = values.object(forKey: Keys.KEY_DISCOUNT_PERCENTAGE) as? Int
                                    {
                                        discount = disc
                                    }
                                    else {
                                        print("exception : Discount")
                                    }
                                    
                                    if let availableQty = values.object(forKey: Keys.KEY_AVAILABLE_QUANTITY ) as? Int {
                                        available = availableQty
                                    }else {
                                        print("exception : available quantity")
                                        if let value =  values.object(forKey: Keys.KEY_AVAILABLE_QUANTITY ) as? String {
                                            available = Int(value)!
                                        }
                                    }
                                    
                                    
                                    
                                    if let manufacture = values.object(forKey: Keys.KEY_MANUFACTERER) as? String
                                    {
                                        if let mrp = values.object(forKey: Keys.KEY_MRP) as? NSNumber
                                        {
                                            if let name = values.object(forKey: Keys.KEY_NAME) as? String
                                            {
                                                if let packsize = values.object(forKey: Keys.KEY_PACK_SIZE) as? Int
                                                {
                                                    if let price = values.object(forKey: Keys.KEY_PRICE) as? String
                                                    {
                                                        if let product_id = values.object(forKey: Keys.KEY_PRODUCT_ID) as? String
                                                        {
                                                            if let qty = values.object(forKey: Keys.KEY_QTY) as? Int
                                                            {
                                                                var packs = 0.0
                                                                
                                                                if packsize == 0 {
                                                                    let pcks : Double = Double(qty) / Double(1)
                                                                    let totalPacks = pcks.rounded(.up)
                                                                    print("Packs : \(pcks) total :\(totalPacks)")
                                                                    packs = totalPacks
                                                                }else {
                                                                    let pcks : Double = Double(qty) / Double(packsize)
                                                                    let totalPacks = pcks.rounded(.up)
                                                                    print("Packs : \(pcks) total :\(totalPacks)")
                                                                    packs = totalPacks
                                                                }
                                                                //
                                                                
                                                                print("Availability Quanity: \(available)")
                                                                let medicine = Medicines(app_id:self.appointment_id,available_qty: String(available), discount: discount, manufaturer: manufacture, mrp: mrp.doubleValue, medicine_name: name, pack_size: packsize, price: price, product_id: product_id, quantity: qty,docPreQty:packs,max_packs:packs)
                                                                App.prescribedMedicines.append(medicine)
                                                            }else {
                                                                print("exception : quantity")
                                                            }
                                                        }else {
                                                            print("exception : product id")
                                                        }
                                                    }else {
                                                        print("exception : price")
                                                    }
                                                }else {
                                                    print("exception : pack size")
                                                }
                                            }else {
                                                print("exception : Name")
                                            }
                                        }else {
                                            print("exception : MRP")
                                        }
                                    }else {
                                        print("exception : Manufature")
                                    }
                                    
                                }
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                App.shippingPincode = self.tf_enterPincode.text! as String
                                self.lb_noMedicinesStatus.isHidden = App.prescribedMedicines.count == 0 ? false : true
                                self.medicineTableView.reloadData()
                                self.selectedTopOption(tag: 2)
                                if App.prescribedMedicines.count == 0 {
                                    self.medicineTableView.isHidden = true
                                    self.bt_medicinesNext.isEnabled = false
                                    self.bt_medicinesNext.backgroundColor = UIColor.gray
                                }else {
                                    self.medicineTableView.isHidden = false
                                    self.bt_medicinesNext.isEnabled = true
                                    self.bt_medicinesNext.backgroundColor = Theme.buttonBackgroundColor
                                }
                            })
                        }
                    }
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    
                }
                
            }
            }.resume()
    }
    
    @IBAction func pincodeNextTapped(_ sender: UIButton) {

        let pincode = self.tf_enterPincode.text! as String
        if pincode.isEmpty == true {
            self.didShowAlert(title: "", message: "Please enter pincode")
        }else {
            var pinCode = pincode
            if let code = pincode.convertStringToNumberalIfDifferentLanguage() {
                pinCode = code.stringValue
                print("Entered Pincode numer:\(pinCode)")
            }
            let data = "\(Keys.KEY_PINCODE)=\(pinCode)"
            self.postPincode(pinData: data)
        }
    }
    
    
    //MARK:- Medicines step two
    ///Decreases the quantity of selected medicine
    @objc func decrementTapped(sender:UIButton) {
        let medicine = App.prescribedMedicines[sender.tag]
        if medicine.maxPacks! == 1.0 {
            print("Quantiy finished")
        }else {
            let increasedQty = medicine.maxPacks! - 1
            App.prescribedMedicines[sender.tag].maxPacks = increasedQty
            self.medicineTableView.reloadData()
        }
    }
    
    ///increments the quantity of selected medicine
    @objc func incrementTapped(sender:UIButton){
        let medicine = App.prescribedMedicines[sender.tag]
        if medicine.maxPacks! == medicine.doctorPrescribedQty! {
            self.didShowAlert(title: "", message: "You can not order more than \(Int(medicine.maxPacks!)) packs")
            //  AlertView.sharedInsance.showInfo(title: "", message: "You can not order more than \(Int(medicine.maxPacks!)) packs")
        }else {
            let increasedQty = medicine.maxPacks! + 1.0
            App.prescribedMedicines[sender.tag].maxPacks = increasedQty
            self.medicineTableView.reloadData()
        }
    }
    
    @objc func clickBtnDelete(sender: UIButton) {
        App.prescribedMedicines.remove(at: sender.tag)
        medicineTableView.reloadData()
        
        if App.prescribedMedicines.count == 0 {
            self.lb_noMedicinesStatus.isHidden = false
            self.medicineTableView.isHidden = true
            self.bt_medicinesNext.isEnabled = false
            self.bt_medicinesNext.backgroundColor = UIColor.gray
        }else {
            self.lb_noMedicinesStatus.isHidden = true
            self.medicineTableView.isHidden = false
            self.bt_medicinesNext.isEnabled = true
            self.bt_medicinesNext.backgroundColor = Theme.buttonBackgroundColor
        }
    }
    
    @IBAction func medinesStepNextTapped(_ sender: UIButton) {
        self.validateMedicinesForOutOfStock()
    }
    
    
 
  //MARK:- Enter Address step three functions
    ///Method calculates the subtotal and creates the dictionary with product id and packs
    func calculateSubTotal() {
        self.subTotal = 0.0
        for medicine in App.prescribedMedicines {
            let total =  Double(medicine.maxPacks!) * Double(medicine.price!)!
            self.subTotal += total
            
            dataDict[medicine.product_id!] = String(Int(medicine.maxPacks!))
            
            self.appointment_id = medicine.appointment_id!
        }
        self.lb_subtotal.text = String(format: "₹%.2f", self.subTotal)
        
        self.placeOrderTableView.reloadData()
        UIView.animate(withDuration: 0.5) {
            self.LC_medicineTableViewHeight.constant = self.placeOrderTableView.contentSize.height
            self.view.layoutIfNeeded()
        }
        
       // print("Items Data : \(dataDict) total:\(self.subTotal)")
    }
    
    @IBAction func addressStepNextTapped(_ sender: UIButton) {
        self.validateAddressFileds()
    }
    
    
    //MARK:- Place order step Four functions
    @IBAction func placeOrderTapped(_ sender: UIButton) {
        let add1 = self.tf_addressLine1.text! as String
        let add2 = self.tf_addressLine2.text! as String
        let mobile = self.lb_mobile_no.text!
        if add2.isEmpty {
            self.totalAddress = "\(add2),\(mobile)"
        }else {
             self.totalAddress = add1 + ",\(add2),\(mobile)"
        }
        
        let object = [ Keys.KEY_PINCODE : self.tf_enterPincode.text!,
                       Keys.KEY_ITEMS : self.dataDict ,
                       Keys.KEY_ADDRESS_1 : add1 ,
                       Keys.KEY_ADDRESS_2 : self.totalAddress ] as [String : Any]
        var jsonData : Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch {
            print("Error while converting to json")
        }
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
            
        }
        
        startAnimating()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let procureURL = AppURLS.URL_Order_Medicines + self.appointment_id
        var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = jsonData
        
        do {
            if let json = try JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary
            {
                print("MEdicine data:\(json)")
            }
        }catch {
            print("Error while converting to json")
        }
        
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async(execute: {
                    print("Error==> : \(error.localizedDescription)")
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                do   {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus      {
                        print("performing error handling procure medicine\(resultJSON)")
                        
                        if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                self.didShowAlert(title: "", message: message)
                                //AlertView.sharedInsance.showFailureAlert(title: "", message: message)
                                // AlertView.sharedInsance.showInfo(title: "", message: message)
                            })
                        }
                    }else {
                        print("Place order response:\(resultJSON)")
                        if let bookedData = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                            // let custId = bookedData.object(forKey: Keys.KEY_CUSTOMER_ID) as! Int
                            // let orderAmount = bookedData.object(forKey: Keys.KEY_ORDER_AMOUNT) as! NSNumber
                            if let orderId = bookedData.object(forKey: Keys.KEY_ORDER_ID) as? String {
                                self.orderID = orderId
                            }
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICINE_ORDER_SUCCESS, sender: self)
                            })
                        }
                    }
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                }
            }
            }.resume()
    }
    
    
    @IBAction func topOptionButtonTapped(_ sender: UIButton) {
        self.validateTheFieldsBeforeTransition(tag: sender.tag)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_MEDICINE_ORDER_SUCCESS {
            if let destVC = segue.destination as? MedicineOrderSuccessViewController {
                destVC.addressLine1 = self.tf_addressLine1.text! as String
                destVC.addressLine2 = self.tf_addressLine2.text! as String
                destVC.orderID = self.orderID
            }
        }else if segue.identifier == StoryboardSegueIDS.ID_VERIFY_EMAIL {
            let destVC = segue.destination as! EmailVerificationViewController
            destVC.delegate = self
        }
    }
    

}

extension UIOrederProcessViewController : EmailVerificationDelegate {
    func didVerifyEmail(_ email: String, _ success: Bool) {
        if success {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
extension UIOrederProcessViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return App.prescribedMedicines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: MedicineTableViewCell!
        let values = App.prescribedMedicines[indexPath.row]
        let mrpPrice = String(format: "₹%.2f", values.mrp!)

        if tableView.tag == 1 && tableView == self.medicineTableView{
             cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.MEDICINES , for: indexPath) as! MedicineTableViewCell
            cell.plus_button.tag = indexPath.row
            cell.minus_button.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            
            cell.medicine_name.text = values.name!
            //      cell.medicine_id.text = values.product_id!
             cell.pack_size.text = "\(values.packsize ?? 0)"
            //  cell.manufacturer.text = values.manf!
            cell.qty.text = "\(Int(values.maxPacks ?? 0))"
            //   cell.lb_doctor_prescribed_qty.text = "Prescribed Quantity: \(values.qty!) | Pack Size : \(values.packsize!)"
            cell.lb_doctor_prescribed_qty.text = "\(values.qty ?? 0)"
            cell.mrp_price.text = mrpPrice
            cell.discount.text = "\(values.discountPercentage ?? 0)"
            cell.price.text = "\(values.price ?? "")"
            
            let availableQuantity = Double(values.availableQty ?? "0")
            if availableQuantity == 0.0 {
                cell.lb_stock_status.text = "Out of Stock"
                cell.lb_stock_status.textColor = UIColor.red
              //  cell.lb_availableQty.text = "0"
            }else {
                cell.lb_stock_status.text = "\(availableQuantity == nil ? "Out of Stock" : "In Stock")"
                //cell.lb_availableQty.text = values.availableQty!
                cell.lb_stock_status.textColor = availableQuantity == nil ? UIColor.red  :  hexStringToUIColor(hex: "#409A3C")
                
            }
            
            cell.plus_button.addTarget(self, action: #selector(self.incrementTapped(sender:)), for: .touchUpInside)
            
            cell.minus_button.addTarget(self, action: #selector(self.decrementTapped(sender:)), for: .touchUpInside)
            
            cell.btnDelete.addTarget(self, action: #selector(self.clickBtnDelete(sender:)), for: .touchUpInside)
            
            if values.maxPacks ?? 0 == 1 {
                cell.minus_button.isEnabled = false
                cell.plus_button.isEnabled = true
            }else if values.maxPacks ?? 0 > 1{
                cell.minus_button.isEnabled = true
                cell.plus_button.isEnabled = true
            }
        }else if tableView.tag == 2 && tableView == self.placeOrderTableView{
            cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.MEDICINEORDER , for: indexPath) as! MedicineTableViewCell
            cell.medicine_name.text = values.name ?? ""
            cell.qty.text = "\(Int(values.maxPacks ?? 0))"
            cell.mrp_price.text = mrpPrice
            cell.price.text = "₹\(values.price ?? "")"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.tag == 2 ? 89 : 122
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //return tableView.tag == 1 ? true : false
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            if editingStyle == UITableViewCell.EditingStyle.delete
            {
                App.prescribedMedicines.remove(at: indexPath.row)
                tableView.reloadData()
            }
            
            if App.prescribedMedicines.count == 0 {
                self.lb_noMedicinesStatus.isHidden = false
                self.medicineTableView.isHidden = true
                self.bt_medicinesNext.isEnabled = false
                self.bt_medicinesNext.backgroundColor = UIColor.gray
            }else {
                self.lb_noMedicinesStatus.isHidden = true
                self.medicineTableView.isHidden = false
                self.bt_medicinesNext.isEnabled = true
                self.bt_medicinesNext.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.GREEN)
            }
         
        }
    }
}

extension UIOrederProcessViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.tf_enterPincode
        {
            let newLength = (self.tf_enterPincode.text?.count)! + string.count - range.length
            return newLength <= 6
            
        }
        return true
    }
}



