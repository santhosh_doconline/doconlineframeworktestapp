//
//  MedicineOrderSuccessViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 24/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class MedicineOrderSuccessViewController: UIViewController {

    
    @IBOutlet var placeOrderTableView: UITableView!
    @IBOutlet var LC_medicineTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet var lb_address: UILabel!
    @IBOutlet var lb_mobile: UILabel!
    @IBOutlet var lb_pincode: UILabel!
    @IBOutlet var lb_orderid: UILabel!
    
    var addressLine1 = ""
    var addressLine2 = ""
    var orderID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lb_orderid.text = "Order Confirmed\n Order ID: \(orderID)"
        self.lb_pincode.text = App.shippingPincode
        
        self.lb_address.text = "Address Line1: \(addressLine1)\nAddress Line2: \(addressLine2)"
        
        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
            let userDataModel  = User.get_user_profile_default()
            if let mobile = userDataModel.phone {
                self.lb_mobile.text = mobile
            }else {
                self.lb_mobile.text = "Mobile:"
            }
        }
        
        self.placeOrderTableView.reloadData()
        UIView.animate(withDuration: 0.5) {
            self.LC_medicineTableViewHeight.constant = self.placeOrderTableView.contentSize.height
            self.view.layoutIfNeeded()
        }
    }

   
    @IBAction func doneTapped(_ sender: UIBarButtonItem) {
        App.prescribedMedicines.removeAll()
        App.shippingPincode = ""
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_GOHOME_FROM_SUCCESS, sender: sender)
    }
    
    //testrdd

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MedicineOrderSuccessViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return App.prescribedMedicines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let values = App.prescribedMedicines[indexPath.row]
        
        let   cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.MEDICINEORDER , for: indexPath) as! MedicineTableViewCell
        cell.medicine_name.text = values.name!
        cell.qty.text = "\(Int(values.maxPacks!))"
        cell.mrp_price.text = "₹\(values.mrp!)"
        cell.price.text = "₹" + values.price!
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
