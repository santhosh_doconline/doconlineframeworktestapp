//
//  LanguagesTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 03/11/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class LanguagesTableViewCell: UITableViewCell {

    @IBOutlet weak var bt_check_box: UIButton!
    
    @IBOutlet weak var lb_language: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
