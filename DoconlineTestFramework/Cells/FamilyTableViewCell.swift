//
//  FamilyTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 11/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

protocol FamilyTableViewCellDelegate : class {
    func didTapRequestLink(_ sender:UIButton)
}

class FamilyTableViewCell: UITableViewCell {

    @IBOutlet weak var lb_familyMemName: UILabel!
    @IBOutlet weak var lb_date_of_birth: UILabel!
    @IBOutlet weak var lb_email: UILabel!
    @IBOutlet weak var lb_age: UILabel!
    @IBOutlet weak var lb_gender: UILabel!
    @IBOutlet weak var iv_profile: UIImageView!
    @IBOutlet var lb_mobileNo: UILabel!
    @IBOutlet var iv_mobileIconImage: UIImageView!
    @IBOutlet var lc_mobileIconHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lb_familyCount: UILabel!
    
    @IBOutlet weak var iv_email: UIImageView!
    
    @IBOutlet weak var bt_edit_tapped: UIButton!
  
    @IBOutlet var bt_requestLink: UIButton!
    
    var cellDelegate: FamilyTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bt_requestLink.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func requestLinkTapped(_ sender: UIButton) {
        self.cellDelegate?.didTapRequestLink(sender)
    }
    

}
