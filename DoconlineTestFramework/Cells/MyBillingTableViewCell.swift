//
//  MyBillingTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 07/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class MyBillingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lb_order_id: UILabel!
    @IBOutlet weak var lb_payment_status: UILabel!
    @IBOutlet weak var lb_issued_on: UILabel!
    @IBOutlet weak var lb_amount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
