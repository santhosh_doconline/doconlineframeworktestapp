//
//  InvoiceTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 07/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var lb_plan_name: UILabel!
    @IBOutlet weak var lb_item_id: UILabel!
    @IBOutlet weak var lb_count: UILabel!
    
    @IBOutlet weak var lb_plan_amount: UILabel!
    @IBOutlet weak var lb_plan_net_amount: UILabel!
    @IBOutlet weak var lb_tax_amount: UILabel!
    @IBOutlet weak var lb_currency: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
