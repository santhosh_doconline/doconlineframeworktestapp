//
//  MyAppointmentsTableViewCell.swift
//  DocOnline
//
//  Created by dev-3 on 09/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class MyAppointmentsTableViewCell: UITableViewCell {

    @IBOutlet var lb_date: UILabel!
    @IBOutlet var lb_month: UILabel!
    @IBOutlet var lb_doctorName: UILabel!
    @IBOutlet var lb_specialisation: UILabel!
    @IBOutlet var lb_patientPic: UIImageView!
    @IBOutlet var lb_patientName: UILabel!
    @IBOutlet var lb_time: UILabel!
    
    @IBOutlet weak var lb_dateLabel: UILabel!
    
    @IBOutlet weak var bt_time: UIButton!
    @IBOutlet weak var iv_call_icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellSetup(app:Appointments,vc:UIViewController) {
        let dateString = app.scheduled_at
        let convertedDate = vc.stringToDateConverter(date: dateString!)
        
        let calendar = Calendar.current
        
        let month = calendar.component(.month, from: convertedDate)
        let day = calendar.component(.day, from: convertedDate)
        
        let monthName = month.getMonthName() // self.getMonthName(day: month)
        
        var patientDetails = ""
        if app.age.isEmpty || app.age.isEqual("0"){
            if let patientname = app.patientName , let patientgender = app.patientGender {
                patientDetails = "\(patientname)"// \(patientgender)"
            }
        }else {
            if let patientname = app.patientName , let patientgender = app.patientGender , let patientage = app.age  {
                patientDetails = "\(patientname)" //\(patientgender),\(patientage)"
            }
        }
        
        //new UI
        self.lb_date.text = "\(day)"
        self.lb_month.text = monthName
        self.lb_patientName.text = patientDetails
        self.lb_doctorName.text = app.doctorName
        self.lb_specialisation.text = app.doctorSpecialisation
        if let time = dateString?.components(separatedBy: " ") {
            let convertedTime = vc.getTimeInAMPM(date: time[1])
            self.lb_time.text = convertedTime
        }
        
        self.lb_patientPic.layer.cornerRadius = self.lb_patientPic.frame.size.width / 2
        self.lb_patientPic.clipsToBounds = true
        if let imgeURl = URL(string:app.patientAvatar) {
            self.lb_patientPic.kf.setImage(with: imgeURl)
        }else {
            self.lb_patientPic.image = UIImage(named:"Default-avatar")
        }
    }
    
    func prescriptionDetails(prescription: Consultation,vc:UIViewController) {
        let dateString = prescription.createdAt
        let convertedDate = vc.stringToDateConverter(date: dateString ?? "")
        
        let calendar = Calendar.current
        
        let month = calendar.component(.month, from: convertedDate)
        let day = calendar.component(.day, from: convertedDate)
        
        let monthName = month.getMonthName() // self.getMonthName(day: month)
        
        var patientDetails = ""
        if let patientname = prescription.patient?.firstName {
            patientDetails = patientname
        }
            
        if let gender = prescription.patient?.gender {
            if gender.lowercased().isEqual("male") {
                patientDetails += " M"
            }else if gender.lowercased().isEqual("female") {
                patientDetails += " F"
            }else if gender.lowercased().isEqual("transgender") {
                patientDetails += " T"
            }
        }
        
        if let patientage = prescription.patient?.fullDetails?.age  {
            patientDetails += ", \(patientage)"
        }
        
        //new UI
        self.lb_date.text = "\(day)"
        self.lb_month.text = monthName
        self.lb_patientName.text = patientDetails
        self.lb_doctorName.text = prescription.schedule?.doctor?.full_name
        self.lb_specialisation.text = prescription.schedule?.doctor?.specialisation
        if let time = dateString?.components(separatedBy: " ") {
            let convertedTime = vc.getTimeInAMPM(date: time[1])
            self.lb_time.text = convertedTime
        }
        
        self.lb_patientPic.layer.cornerRadius = self.lb_patientPic.frame.size.width / 2
        self.lb_patientPic.clipsToBounds = true
        if let imgeURl = URL(string:prescription.patient?.avatar_url ?? "") {
            self.lb_patientPic.kf.setImage(with: imgeURl)
        }else {
            self.lb_patientPic.image = UIImage(named:"Default-avatar")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
