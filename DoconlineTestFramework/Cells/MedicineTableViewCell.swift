//
//  MedicineTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 25/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class MedicineTableViewCell: UITableViewCell {

    @IBOutlet weak var medicine_name: UILabel!
    @IBOutlet weak var medicine_id: UILabel!
    @IBOutlet weak var manufacturer: UILabel!
    @IBOutlet weak var pack_size: UILabel!
    @IBOutlet weak var qty: UILabel!
    
    @IBOutlet var lb_availableQty: UILabel!
    @IBOutlet weak var plus_button: UIButton!
    @IBOutlet weak var minus_button: UIButton!
    
    @IBOutlet weak var mrp_price: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var lb_stock_status: UILabel!
   @IBOutlet weak var lb_doctor_prescribed_qty: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
}
