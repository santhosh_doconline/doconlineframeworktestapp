//
//  PlaceOrderTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 26/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class PlaceOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var lb_medicineName: UILabel!
    
    @IBOutlet weak var lb_quantity: UILabel!
    @IBOutlet weak var lb_price: UILabel!
    @IBOutlet weak var lb_total: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
