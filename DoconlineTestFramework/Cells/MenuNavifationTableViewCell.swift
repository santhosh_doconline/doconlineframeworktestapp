//
//  MenuNavifationTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 30/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class MenuNavifationTableViewCell: UITableViewCell {

    
    @IBOutlet var lb_menuName: UILabel!
    @IBOutlet var iv_menuIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
