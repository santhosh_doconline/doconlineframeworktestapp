//
//  DropDownTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class DropDownTableViewCell: UITableViewCell {

    @IBOutlet var iv_profilePic: UIImageView!
    @IBOutlet var lb_name: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
