//
//  SubscriptionPlansCollectionViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 23/11/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class SubscriptionPlansCollectionViewCell: UITableViewCell {
    
    @IBOutlet weak var lb_plan_period: UILabel!
    @IBOutlet weak var lb_plan_amount: UILabel!
    
    @IBOutlet weak var lb_per_month: UILabel!
    @IBOutlet weak var lb_offer_text: UILabel!
    @IBOutlet weak var lb_cross_price: UILabel!
    @IBOutlet weak var vw_back: UIView!
    @IBOutlet weak var tv_description: UITextView!
    @IBOutlet weak var lb_price_period: UILabel!
    @IBOutlet weak var bt_subscription: UIButton!
    
    @IBOutlet var bt_upgrade_now: UIButton!
    @IBOutlet var lb_activePlan: UILabel!
    @IBOutlet var LC_choose_plan_height: NSLayoutConstraint!
    
    @IBOutlet var LC_discountText_height: NSLayoutConstraint!
    @IBOutlet weak var lb_subscribed_status: UILabel!
    
    @IBOutlet var lb_recommended_status: UILabel!
    @IBOutlet var lc_recommended_height: NSLayoutConstraint!
    
    @IBOutlet var iv_subscriptionIcon: UIImageView!
    
    @IBOutlet var iv_limitedPeriodOfferRibbon: UIImageView!
    @IBOutlet var lb_amountPerMonth: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        bt_subscription.backgroundColor = Theme.buttonBackgroundColor
        bt_upgrade_now.backgroundColor = Theme.buttonBackgroundColor
//        let crossLineView = UIView(
//            frame: CGRect(x: 0,
//                          y: lb_cross_price.bounds.size.height / 2,
//                          width: lb_cross_price.bounds.size.width,
//                          height: 1
//            )
//        )
//        crossLineView.backgroundColor = .black
//        lb_cross_price.addSubview(crossLineView)
        
//        self.layer.cornerRadius = 3.0
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.8
//        layer.shadowOffset = CGSize(width: 0, height: 0)
//        self.clipsToBounds = false
    }
    
}
