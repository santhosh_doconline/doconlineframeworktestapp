//
//  OrderDetailsTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class OrderDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lb_item_type : UILabel!
    @IBOutlet weak var lb_medicine_name : UILabel!
    @IBOutlet weak var lb_manufacturer : UILabel!
    @IBOutlet weak var lb_pack_size: UILabel!
    @IBOutlet weak var lb_mrp : UILabel!
    @IBOutlet weak var lb_discount : UILabel!
    @IBOutlet weak var lb_quantity : UILabel!
    @IBOutlet weak var lb_price: UILabel!
    @IBOutlet weak var lb_total: UILabel!
    @IBOutlet weak var lb_product_id: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
