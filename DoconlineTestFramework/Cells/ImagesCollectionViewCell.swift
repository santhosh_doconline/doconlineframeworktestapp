//
//  ImagesCollectionViewCell.swift
//  DocOnline
//
//  Created by dev-3 on 12/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

protocol ImagesCollectionViewDelegate : class{
    func didTapRemoveImge(at:Int)
    func didTapVieFile(at:Int)
}

class ImagesCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lb_fileSize: UILabel!
    @IBOutlet var lb_fileSizeExceedMsg: UILabel!
    
    @IBOutlet var bt_viewFile: UIButton!
    @IBOutlet weak var iv_image: UIImageView!
    @IBOutlet weak var bt_delete: UIButton!
    //textfiled
    @IBOutlet var tf_caption: UITextField!
    @IBOutlet var containerOfWebView: UIView!
    var wv_webView: WKWebView!
   // @IBOutlet var webView: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    //@IBOutlet var wkWebView: WKWebView!
    
    ///Booking Consultation : Language cell
     @IBOutlet weak var lb_language: UILabel!
    
    weak var cellDelegate: ImagesCollectionViewDelegate?
    
    func cellSetup(vm:ImagesCollectionViewCellModal) {
        DispatchQueue.main.async {
            if vm.picture?.type?.rawValue  == FileType.file.rawValue {
                self.iv_image.image = UIImage(named:"File")
            }else {
                if let pic = vm.picture?.pic {
                    self.iv_image.image = pic
                }else {
                    if let url = URL(string:vm.picture?.imageUrl ?? "") {
                        self.iv_image.kf.setImage(with: url)
                    }
                }
            }
            
            if self.bt_delete != nil {
                // self.bt_delete.tag = vm.index ?? 0
               let cancelImage = UIImage(named: "Cancel")?.withRenderingMode(.alwaysTemplate)
                self.bt_delete.setImage(cancelImage, for: .normal)
                self.bt_delete.tintColor = UIColor.red
            }
            
            if self.tf_caption != nil {
                //  self.tf_caption.tag = vm.index ?? 0
                
                self.tf_caption.isEnabled = vm.picture?.pic == nil && vm.picture?.imageUrl != nil ? false : true
                self.tf_caption.text = vm.picture?.picCaption
                self.tf_caption.addTarget(self, action: #selector(self.fieldStateChanged), for: .editingChanged)
            }
        }
    }
    
    func webViewCellSetup(vm:ImagesCollectionViewCellModal) {
        DispatchQueue.main.async {
            if self.iv_image != nil {
                self.iv_image.image = UIImage(named:"File")
            }
            
            if self.bt_delete != nil {
                let cancelImage = UIImage(named: "Cancel")?.withRenderingMode(.alwaysTemplate)
                self.bt_delete.setImage(cancelImage, for: .normal)
                self.bt_delete.tintColor = UIColor.red
            }
            
            if vm.picture?.type?.rawValue  == FileType.file.rawValue && self.containerOfWebView != nil {
                var urlString = ""
                if let fileUrl = vm.picture?.fileURl {
                    urlString = fileUrl
                }
                
                if let fileUrl = vm.picture?.imageUrl {
                    urlString = fileUrl
                }
                
                if let url = URL(string: urlString ) {
                    let preferences = WKPreferences()
                    preferences.javaScriptEnabled = true
                    
                    let config = WKWebViewConfiguration()
                    config.preferences = preferences
                    self.wv_webView = WKWebView(frame: self.containerOfWebView.bounds, configuration: config)
                    self.wv_webView.clipsToBounds = true
                    self.wv_webView.navigationDelegate = self
                    self.containerOfWebView.addSubview(self.wv_webView)
                    self.wv_webView.navigationDelegate = self
                    let request = URLRequest(url: url)
                    self.wv_webView.load(request)
                }
            }
            
            if self.containerOfWebView != nil && vm.picture?.pic == nil {
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.webViewTapped))
                tapRecognizer.numberOfTapsRequired = 1
                
                var urlString = ""
                if let fileUrl = vm.picture?.fileURl {
                    urlString = fileUrl
                }
                
                if let fileUrl = vm.picture?.imageUrl {
                    urlString = fileUrl
                }
                
//                DispatchQueue.main.async {
                    if let url = URL(string: urlString ) {
                        let preferences = WKPreferences()
                        preferences.javaScriptEnabled = true
                        
                        let config = WKWebViewConfiguration()
                        config.preferences = preferences
                        self.wv_webView = WKWebView(frame: self.containerOfWebView.bounds, configuration: config)
                        self.wv_webView.clipsToBounds = true
                        self.wv_webView.navigationDelegate = self
                    self.containerOfWebView.addSubview(self.wv_webView)
                        self.wv_webView.navigationDelegate = self
                        let request = URLRequest(url: url)
                        self.wv_webView.load(request)
                        print("Yeah trying to load file in webview")
                    }
                  self.wv_webView.addGestureRecognizer(tapRecognizer)
//                }
            }
            
            if self.tf_caption != nil {
                //  self.tf_caption.tag = vm.index ?? 0
                
                self.tf_caption.isEnabled = vm.picture?.pic == nil && vm.picture?.imageUrl != nil ? false : true
                self.tf_caption.text = vm.picture?.picCaption
                self.tf_caption.addTarget(self, action: #selector(self.fieldStateChanged), for: .editingChanged)
            }
        }
    }
    
    @objc func webViewTapped() {
        self.cellDelegate?.didTapVieFile(at: self.containerOfWebView.tag)
    }
    
    @objc func fieldStateChanged() {
        guard let caption = self.tf_caption.text else {
            return
        }
        App.bookingAttachedImages[self.tf_caption.tag].picture?.picCaption = caption
    }
    
    @IBAction func removePicTapped(_ sender:UIButton) {
        self.cellDelegate?.didTapRemoveImge(at: sender.tag)
    }
    
    @IBAction func viewFileTapped(_ sender: UIButton) {
         self.cellDelegate?.didTapVieFile(at: sender.tag)
    }
}

extension ImagesCollectionViewCell : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         let newLength = (self.tf_caption.text?.count)! + string.count - range.length
        return newLength <= 30
    }
}

extension ImagesCollectionViewCell : WKNavigationDelegate {
    func webView(_ webView: WKWebView,
                 didFinish navigation: WKNavigation!) {
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
}

extension ImagesCollectionViewCell : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
         self.activityIndicator.isHidden = true
         self.activityIndicator.stopAnimating()
    }

}
