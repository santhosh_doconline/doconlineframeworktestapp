//
//  AddOnsListTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 02/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class AddOnsListTableViewCell: UITableViewCell {

    @IBOutlet var iv_icon: UIImageView!
    @IBOutlet var lb_title: UILabel!
    @IBOutlet var lb_subContent: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
