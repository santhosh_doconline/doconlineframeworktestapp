//
//  ChatHistoryTableViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 23/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class ChatHistoryTableViewCell: UITableViewCell {

    
    @IBOutlet weak var iv_doctor_pic: UIImageView!
    @IBOutlet weak var lb_doctor_name: UILabel!
    @IBOutlet weak var lb_last_message: UILabel!
    @IBOutlet weak var lb_message_time: UILabel!
    @IBOutlet weak var lb_qualification: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
