//
//  HealthProfileTableViewCell.swift
//  DocOnline
//
//  Created by dev-3 on 14/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

class HealthProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var lb_name: UILabel!
    
    @IBOutlet weak var bt_edit_tapped: UIButton!
    @IBOutlet weak var bt_deleteTapped: UIButton!
    
    @IBOutlet weak var lb_fromDate: UILabel!
    @IBOutlet weak var lb_toDate: UILabel!
    
    @IBOutlet weak var lb_intake: UILabel!
    @IBOutlet weak var lb_notes: UILabel!
    
    @IBOutlet weak var lb_no_days: UILabel!
    @IBOutlet weak var img_no_days: UIImageView!
    
    @IBOutlet weak var lb_status: UILabel!
    @IBOutlet weak var img_status: UIImageView!

    @IBOutlet var vw_buttonsView: UIView!
    @IBOutlet var vw_back: UIView!
    
    @IBOutlet weak var img_arrow: UIImageView!
    @IBOutlet weak var img_from: UIImageView!
    @IBOutlet weak var img_to: UIImageView!
    @IBOutlet weak var img_intake: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
