//
//  GlobalConstants.swift
//  DoconlineTestFramework
//
//  Created by Santosh Kumar on 15/07/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import Foundation

///stores Opentok session id
var callSessionID = ""

///DoctorInfo from push
var callDoctorModal : Doctor!

///stores Opentok token id
var callTokenid = ""

///This variable stores integer to check call type **Audio** or **Video**
var incomingCallType = 0

///to check notification type
var incomingNotificationType = ""

///Boolean to check if already calling view is presenting or not
var isCallingViewPresenting = false
