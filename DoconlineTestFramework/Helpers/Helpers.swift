//
//  Helpers.swift
//  DoconlineTestFramework
//
//  Created by Santosh Kumar on 22/07/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages
import Photos
import MobileCoreServices

extension UIButton {
    ///animates the button with blinking
    func startBlink() {
        UIView.animate(withDuration: 0.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0.40 },
                       completion: nil)
    }
    
    ///Stops the animation of blinking
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}



extension UIImage{
    
    
    func tintedWithLinearGradientColors(colorsArr: [CGColor]) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        guard let context = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1, y: -1)

        context.setBlendMode(.normal)
        let rect = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)

        // Create gradient
        let colors = colorsArr as CFArray
        let space = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: space, colors: colors, locations: nil)

        // Apply gradient
        context.clip(to: rect, mask: self.cgImage!)
        context.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: 0), end: CGPoint(x: 0, y: self.size.height), options: .drawsAfterEndLocation)
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return gradientImage!
    }
    
    
}



extension UIView {
    
    func setGradientBackgroundForBanner(colorTop: UIColor, colorBottom: UIColor) {
        if (self.layer.sublayers) != nil{
            for layer in self.layer.sublayers! {
                if layer.name == "banner" {
                     layer.removeFromSuperlayer()
                }
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.name = "banner"
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
       layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
       layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setGradBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = colors
        self.layer.addSublayer(gradientLayer)
    }
    
    func addShadowToView() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity =  0.5
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1
        self.layer.masksToBounds = false
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
        // OUTPUT 1
        func dropShadow(scale: Bool = true) {
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.layer.shadowRadius = 1
            
            self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        }
        
        // OUTPUT 2
        func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
            self.layer.masksToBounds = false
            self.layer.shadowColor = color.cgColor
            self.layer.shadowOpacity = opacity
            self.layer.shadowOffset = offSet
            self.layer.shadowRadius = radius
            
            self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        }
    
    func addShadowLayer(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float, radius_shadow: CGFloat = 1.5) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius_shadow
        layer.shadowOpacity = opacity
        layer.cornerRadius = radius
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.5
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    

    
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
}



//extension UIImage {
//func imageWithColor(tintColor: UIColor) -> UIImage {
//    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
//
//    let context = UIGraphicsGetCurrentContext()!
//    context.translateBy(x: 0, y: self.size.height)
//    context.scaleBy(x: 1.0, y: -1.0);
//    context.setBlendMode(.normal)
//
//    let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
//    context.clip(to: rect, mask: self.cgImage!)
//    tintColor.setFill()
//    context.fill(rect)
//
//    let newImage = UIGraphicsGetImageFromCurrentImageContext()!
//    UIGraphicsEndImageContext()
//
//    return newImage
//}
//}


extension UIViewController{
    
    
    func getDayOfWeek(_ today:String) -> Int {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let todayDate = formatter.date(from: today)
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate!)
        return weekDay
    }
    
    func openSettings(alert: UIAlertAction!) {
        if let url = URL.init(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func ShowSettings(){
        let alert = UIAlertController(title: "Alert!!",
                                  message: "Please enable location for better service..Settings->DocOnline->Location",
                                  preferredStyle: UIAlertController.Style.alert)
    
        alert.addAction(UIAlertAction(title: "Open Settings",
                                      style: UIAlertAction.Style.default,
                                      handler: openSettings))
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: UIAlertAction.Style.cancel,
                                      handler: {
                                        alert in
                                        self.navigationController?.popViewController(animated: true)
        }))
    
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDateStringFromTimeStamp(timeStamp:Double) -> (Date,String){
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strDate = dateFormatter.string(from: date)
        return (date,strDate)
    }
    
    func handleIfAnyErrorResponse(statusCode:Int ,data:NSDictionary?) -> Bool{
        func showErrorTypeMessage(title:String, message:String) {
            DispatchQueue.main.async(execute: {
                print("Error code \(statusCode)=> \(message)")
                self.didShowAlert(title: title, message: message)
            })
        }
        
        switch statusCode {
        case 200, 201 , 202 ,204 :
            return true
        case 400 , 402 , 412 ,423 , 429:
            return false
        case  410 :
            print("Response status code is 410")
            if let datajson = data {
                if let message = datajson.object(forKey: Keys.KEY_MESSAGE) as? String {
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: message)
                    })
                }
            }
            return false
            
        case  401 :
            DispatchQueue.main.async(execute: {
                print("Error code 401=> Unauthorized request")
                //print("==>Login response : \(data)")
                if let dataToCheck = data {
                    if let jsonKeys : [String] = dataToCheck.allKeys as? [String] {
                        
                        if jsonKeys.contains(Keys.KEY_ERROR)
                        {
                            print("Error while login")
                            if let errorKey = data?.object(forKey: Keys.KEY_ERROR) as? String
                            {
                                var message = ""
                                if let mesg = data?.object(forKey: Keys.KEY_MESSAGE) as? String
                                {
                                    message = mesg
                                }
                                
                                if errorKey == "invalid_credentials"
                                {
                                    self.didShowAlert(title: "", message: AlertMessages.INCORRECT_CRED)
                                    //  AlertView.sharedInsance.showFailureAlert(title: "Incorrect Credentials", message: AlertMessages.INCORRECT_CRED)
                                }
                                else{
                                    self.didShowAlerWithAction(title: "Authentication Fail", message: "Unauthorized User. \(message)", data: data!, statusCode: 401)
                                }
                            }
                        }else  if let jsonData = data
                        {
                            if let errorData = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary
                            {
                                let errorDataKeys = errorData.allKeys as! [String]
                                if errorDataKeys.contains(Keys.KEY_ERRORS)
                                {
                                    if let typeOfError = errorData.object(forKey: Keys.KEY_ERRORS) as? NSDictionary
                                    {
                                        let keysArray = typeOfError.allKeys as! [String]
                                        for error in keysArray
                                        {
                                            if let value = typeOfError.object(forKey: error) as? [String],let valueString = value.first {
                                                self.didShowAlert(title: "", message: valueString)
                                                break
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
            return false
            
        case  500 :
            showErrorTypeMessage(title: "SORRY!!", message: "Service not available. Please try again later.")
            return false
            
        case  503 :
            showErrorTypeMessage(title: "SORRY!!", message: "Application is under maintenance. Please try again later.")
            return false
            
        default:
            showErrorTypeMessage(title: "Oops...", message: "Something went wrong. Please try again later")
            return false
        }
    }
    
    func UTCToLocalDate(date:String) -> Date {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat =  "yyyy-MM-dd HH:mm:ss"
        
        return dt!//dateFormator.string(from: dt!)
    }
    
    func setAttributeString(_ str: String,strLength:Int,fontSize1:CGFloat,fontSize2:CGFloat,isToStrike:Bool,strikeLocation:Int,strikeLength:Int) -> NSMutableAttributedString {
        let myMutableString = NSMutableAttributedString(string: str)
        myMutableString.addAttribute(
            NSAttributedString.Key.font,
            value: UIFont.boldSystemFont(ofSize: fontSize1),
            range: NSRange(
                location: 0,
                length: strLength))
        myMutableString.addAttribute(
            NSAttributedString.Key.font,
            value: UIFont.boldSystemFont(ofSize: fontSize2),
            range: NSRange(
                location: strLength,
                length: str.count-strLength))
        if isToStrike {
            myMutableString.addAttribute(
                NSAttributedString.Key.strikethroughStyle,
                value: 2,
                range: NSRange(
                    location: strikeLocation,
                    length: strikeLength))
        }
        
        return myMutableString
    }
    
    /**
     This returns random number within  range
     */
    func randomIntFrom(start: Int, to end: Int) -> Int {
        var a = start
        var b = end
        // swap to prevent negative integer crashes
        if a > b {
            swap(&a, &b)
        }
        return Int(arc4random_uniform(UInt32(b - a + 1))) + a
    }
    
    func addDropShadowToButtons(button:UIButton) {
        button.layer.cornerRadius = 0.0
        button.layer.shadowRadius = 2
        button.layer.shadowOpacity = 0.8
        button.layer.shadowOffset = CGSize(width: 0, height: 0)
        button.clipsToBounds = false
    }
    
    /**
     function converts UTC time string to Local time string HH:mm
     
     - Parameter date: takes date string
     - Returns: UTC time string ex: 09:20:00
     */
    func UTCToLocalHHMM(date:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat =  "HH:mm"
        
        return dateFormator.string(from: dt!)
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        let email = emailPredicate.evaluate(with: enteredEmail)
        
        return email
    }
    
    func validatePhoneNumber(enteredPhoneNumber:String) -> Bool {
        let phoneFormat = "\\A[0-9]{10}\\z"
        let phonePredicate = NSPredicate(format:"SELF MATCHES %@", phoneFormat)
        let phone = phonePredicate.evaluate(with: enteredPhoneNumber)
        
        return phone
    }
    
    /**
     This method returns color from hex string
     - Parameter hex: TakesdStringKey.font.rawVhlux string
     - Returns : UIColor
     */
    func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /**
     function takes email string and returns boolean value
     
     - Parameter enteredEmail: takes email string
     - Returns: boolean value
     */
    func validateOnlyEmail(enteredEmail:String) -> Bool {
        let emailFormat = "^[A-Z0-9a-z._%+-]{2,}@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}$"  //"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        let email = emailPredicate.evaluate(with: enteredEmail)
        return email ? email : enteredEmail.count == 10
    }
    
    
    /**
     This method returns string formated date from Date
     * NOTE :: Throws error if date format doesn't matches
     - Parameter date: Takes Date
     - Returns : Date string
     */
    func dateString(date:Date, format:String) -> String {  //Not used
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    /**
     Another type format of string date converter
     
     - Parameter string: takes date string
     - Returns: Date with format is returned.
     */
    func dateStringToDate(string:String, format:String) -> Date {  //not used
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: string)
        return date!
    }
    
    /**
     method used adds corner radius to buttons.
     - Parameter buttons: it's an array, takes array of button outlets at a time.
     */
    func buttonCornerRadius(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = button.frame.size.height / 2
            button.clipsToBounds = true
            button.layer.shadowOpacity = 0.5
            let shadowPath = UIBezierPath(roundedRect: button.bounds, cornerRadius: 2)
            button.layer.shadowOffset = CGSize(width: 0, height:3)
            button.layer.shadowRadius = 2
            button.layer.shadowColor = UIColor.darkGray.cgColor
            button.layer.shadowPath = shadowPath.cgPath
           // button.layer.cornerRadius = 20
           // button.clipsToBounds = true
        }
    }
    
    /**
     method used adds shadow effect for views
     - Parameter views: it's an array, takes array of view outlets at a time.
     */
    public func shadowEffect(views:[UIView]) {
        for view in views {
            view.layer.shadowOpacity = 0.3
            view.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
            view.layer.shadowRadius = 2.0
            view.layer.shadowColor = UIColor.darkGray.cgColor
            view.layer.cornerRadius = 20
            view.clipsToBounds = true
        }
    }
    
    /**
     Method used to calculate age by taking date of birth as parameter and returns age
     - Parameter date: Date of birth in form of date should be passed.
     - Returns : the calculated age
     */
    func calculate_age(date:Date) -> Int {
        let now = Date()
        let birthday: Date = date
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year!
        return age
    }
    
    /**
     This method is used to convert string date
     * NOTE :: Throws error if date format doesn't matches
     - Parameter date: Takes string date
     - Returns : Date
     */
    func stringToDateConverterWithFormat(date:String,format:String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: date)
        
        dateFormatter.timeZone = NSTimeZone.local   //not needed
        
        return date == nil ? Date() : date!
    }
    
    func stringToDateConverter(date:String) -> Date {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: date)
        
        dateFormatter.timeZone = NSTimeZone.local //not needed
        
        return date == nil ? Date() : date!
    }
    
        /**
            This method is used to show alert message if user mobile is not verifield
            #####Note#####
            * Alert contains a button `Verify now`
            * if user taps on the button it takes to mobile verification screen
         */
        func showVerifyMobileMessage(title:String,message:String,segue:String,tag:Int,fromView : Int) {
            
            var mobileNumer = ""
              if let mobile = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
                  mobileNumer = mobile
              }
            
                print("user mobile number stored")
            let messageView: MessageView = MessageView.viewFromNib(layout: MessageView.Layout.centeredView)
                messageView.configureBackgroundView(width: 250)
    //             var messageBody = ""
    //             if message.isEmpty {
    //                messageBody = "Your mobile number \(mobileNumer) is not verified."
    //             }else {
    //                messageBody = message
    //             }
            
                messageView.configureContent(title: title, body: message, iconImage: nil , iconText: nil, buttonImage: nil, buttonTitle: "Verify now") { _ in
                    //App.isFromAnotherView = true
                    //self.instantiate_to_view(withIdentifier: "MobileVerificationView")
                    if tag == 1 {
                        App.isFromView = fromView
                    }
                    self.performSegue(withIdentifier: segue, sender: self)
                    SwiftMessages.hide()
                }
                messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
                messageView.backgroundView.layer.cornerRadius = 12
                var config = SwiftMessages.defaultConfig
                config.presentationStyle = .center
                config.duration = .forever
                config.dimMode = .blur(style: .dark, alpha: 0.85, interactive: true)
                config.presentationContext  = .window(windowLevel: .statusBar)
                SwiftMessages.show(config: config, view: messageView)
            
        }
    
    /**
     Method used to perform view transition with storyboard viewcontroller identifier
     - Parameter withIdentifier:  here we have to pass view controller identifier.
     */
    func instantiate_to_view(withIdentifier:String) {
        let storyBoard = self.storyboard?.instantiateViewController(withIdentifier: withIdentifier)
        self.present(storyBoard!, animated: true, completion: nil)
    }
    
    
    /**
     Method checks status code and is used in catch bloack of every request in all classess.
     Shows alert according to the error status code
      - Parameter statusCode: takes *HTTPResponse* status code of Int type
     */
    func checkOnlyStatus(statusCode:Int) {
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
            switch statusCode {
            case  204 :
                print("delete or read success")
                break
                
            case  500 :
                print("Error code 500=> Internal Server Error")
                self.didShowAlert(title: "SORRY!", message: "Service not available. Please try again later.")
               // AlertView.sharedInsance.showFailureAlert(title: "SORRY!", message: "Service not available. Please try again later.")
                break
            case  502 :
                print("Error code 502=> Bad Gateway")
                self.didShowAlert(title: "Bad Gateway", message: "Please try again.")
                break
                
            case  503 :
                print("Error code 503=> Application is under maintenance")
                self.didShowAlert(title: "SORRY!", message: "Application is under maintenance. Please try again later.")
               // AlertView.sharedInsance.showFailureAlert(title: "SORRY!", message: "Application is under maintenance. Please try again later.")
                
                break
                
            default:
                print("Something went wrong. Please try again later")
                self.didShowAlert(title: "Oops...", message: "Something went wrong. Please try again later")
               
               //  AlertView.sharedInsance.showFailureAlert(title: "Oops!", message: "Something went wrong. Please try again later")
                break
            }
        })
    }

        /**
         Method takes **HTTP Response** Status code and the data from server response and returns boolean.
         
           ## Note ##
           * Used to check the response status code and show alert if error status code
           * alert message is shown according to the response
         
         - Parameter statusCode: takes *HTTPResponse* status code of Int type.
          - Parameter data: pass the response from server. It's an optional field pass nil if no response
         - Returns : Boolean value true if no error status code else false
         
         */
        func check_Status_Code(statusCode:Int ,data:NSDictionary?)  -> Bool{
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
            switch statusCode {
            case  204 :
                print("delete or read success")
                return true
                
            case  400 :
                return false
                
            case  429 :
                return false
                
    //        case  405 :
    //            return false
                
            case  401 :
                DispatchQueue.main.async(execute: {
                    print("Error code 401=> Unauthorized request")
                    //print("==>Login response : \(data)")
                    if let dataToCheck = data {
                        if let jsonKeys : [String] = dataToCheck.allKeys as? [String] {
                            
                            if jsonKeys.contains(Keys.KEY_ERROR)
                            {
                                print("Error while login")
                                if let errorKey = data?.object(forKey: Keys.KEY_ERROR) as? String
                                {
                                    var message = ""
                                    if let mesg = data?.object(forKey: Keys.KEY_MESSAGE) as? String
                                    {
                                        message = mesg
                                    }
                                    
                                    if errorKey == "invalid_credentials"
                                    {
                                        self.didShowAlert(title: "", message: AlertMessages.INCORRECT_CRED)
                                        //  AlertView.sharedInsance.showFailureAlert(title: "Incorrect Credentials", message: AlertMessages.INCORRECT_CRED)
                                    }
                                    else{
                                        self.didShowAlerWithAction(title: "Authentication Fail", message: "Unauthorized User. \(message)", data: data!, statusCode: 401)
                                    }
                                }
                            }else  if let jsonData = data
                            {
                                if let errorData = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary
                                {
                                    let errorDataKeys = errorData.allKeys as! [String]
                                    if errorDataKeys.contains(Keys.KEY_ERRORS)
                                    {
                                        if let typeOfError = errorData.object(forKey: Keys.KEY_ERRORS) as? NSDictionary
                                        {
                                            let keysArray = typeOfError.allKeys as! [String]
                                            for error in keysArray
                                            {
                                                if let value = typeOfError.object(forKey: error) as? [String],let valueString = value.first {
                                                    self.didShowAlert(title: "", message: valueString)
                                                    break
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
                return false
            case  402 :
                print("Response status code is 402")
                return false
                
           case  410 :
                print("Response status code is 410")
                if let datajson = data {
                    if let message = datajson.object(forKey: Keys.KEY_MESSAGE) as? String {
                       DispatchQueue.main.async(execute: {
                          self.didShowAlert(title: "", message: message)
                         // AlertView.sharedInsance.showFailureAlert(title: "", message: message)
                       })
                    }
                 }
                return false
                
            case  412 :
                print("Response status code is 412")
                
                return false
                
            case  500 :
                DispatchQueue.main.async(execute: {
                    print("Error code 500=> Internal Server Error")
                    self.didShowAlert(title: "SORRY!!", message: "Service not available. Please try again later.")
                    //  AlertView.sharedInsance.showFailureAlert(title: "SORRY!", message: "Service not available. Please try again later.")
                })
                return false
                
            case  502 :
                DispatchQueue.main.async(execute: {
                    print("Error code 502=> Bad Gateway")
                    self.didShowAlert(title: "Bad Gateway", message: "Please try again.")
                    //  AlertView.sharedInsance.showFailureAlert(title: "SORRY!", message: "Service not available. Please try again later.")
                })
                return false
                
            case  503 :
                DispatchQueue.main.async(execute: {
                    print("Error code 503=> Application is under maintenance")
                    if let datajson = data ,let message = datajson.object(forKey: Keys.KEY_MESSAGE) as? String{
                        self.didShowAlert(title: "SORRY!!", message: message)
                    }else {
                        self.didShowAlert(title: "SORRY!!", message: "Application is under maintenance. Please try again later.")
                    }
                })
                return false
                
            case  200 :
                DispatchQueue.main.async(execute: {
                    print("error code 200=> Success fully fetched")
                })
                return true
                
            case  202 :
                DispatchQueue.main.async(execute: {
                    print("Booking status 200=> Success fully fetched")
                })
                return true
                
            case  201 :
                print("error code 201=> Success posted")
                return true
                
            case  422 :
                print("error code 422=> unprocessable entity")
                DispatchQueue.main.async(execute: {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                    print("error code 422=> unprocessable entity")
                    if let jsonData = data
                    {
                        if let errorData = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary
                        {
                            let errorDataKeys = errorData.allKeys as! [String]
                            if errorDataKeys.contains(Keys.KEY_ERRORS) {
                                if let typeOfError = errorData.object(forKey: Keys.KEY_ERRORS) as? NSDictionary
                                {
                                    if let keysArray = typeOfError.allKeys as? [String] {
                                        for error in keysArray
                                        {
                                            if let value = typeOfError.object(forKey: error) as? [String],
                                                let valueString = value.first  {
                                                self.didShowAlert(title: "", message: valueString)
                                                break
                                            }else {
                                                if let value = typeOfError.object(forKey: error) as? String {
                                                    self.didShowAlert(title: "", message: value)
                                                    break
                                                }
                                            }
                                        }
                                    }else {
                                        if let value = jsonData.object(forKey: Keys.KEY_MESSAGE) as? String {
                                            self.didShowAlert(title: "", message: value)
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                })
                return false
                
            case 423 :
                
                return false
                
            default:
                DispatchQueue.main.async(execute: {
                    print("Something went wrong. Please try again later")
                    self.didShowAlert(title: "Oops...", message: "Something went wrong. Please try again later")
                   // AlertView.sharedInsance.showFailureAlert(title: "Oops!", message: "Something went wrong. Please try again later")
                })
                return false
            }
        }
    
    /**
        Method performs request to check appoitnment is rated or not
        */
       func getRatingStatus(urlString:String,httpMethod:String,jsonData:Data?,type:Int,completionHandler: @escaping (_ success:Bool,_ response:NSDictionary?) -> Void) {
           
           let url = URL(string: urlString)
           var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
           request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
           request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
           request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
           request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
           request.httpMethod = httpMethod
           
           if type == 2 {
               if let data = jsonData {
                   request.httpBody = data
               }
           }
    
           URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
               if let error = error
               {
                    completionHandler(false,nil)
                   DispatchQueue.main.async(execute: {
                       print("Error==> : \(error.localizedDescription)")
                       self.didShowAlert(title: "", message: error.localizedDescription)
                   })
               }
               if let data = data
               {
                   print("data =\(data)")
               }
               if let response = response
               {
                   print("url = \(response.url!)")
                   print("response = \(response)")
                   let httpResponse = response as! HTTPURLResponse
                   print("response code = \(httpResponse.statusCode)")
                   print("httpresponse:\(response as! HTTPURLResponse)")
                   
                   if httpResponse.statusCode == 204 && type == 2 {
                       completionHandler(true,nil)
                   }else
                   {
                       do {
                           if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                               
                               let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                               if !errorStatus {
                                   print("performing error handling update user profile:\(resultJSON)")
                                   completionHandler(false,resultJSON)
                                   if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String ,  type == 2{
                                       DispatchQueue.main.async {
                                           self.didShowAlert(title: "Info", message: message)
                                       }
                                   }
                               }else {
                                   completionHandler(true,resultJSON)
                                   print("Checking rating response:\(resultJSON)")
                               }
    
                               print("Rating response:\(resultJSON)")
                           }
                           
                       }catch let error {
                           completionHandler(false,nil)
                           print("Recieved a well formatted json:\(error.localizedDescription)")
                       }
                   }
               }
               
           }).resume()
       }
    
    /**
     method Shows alert and perfoms action according to status code
      *Logouts the user from application if status code is 401*
     
     - Parameter title: pass the title to show in alert title.
     - Parameter message: pass the message to show in alert message.
     - Parameter data: pass the response from server to perform some action
     - statusCode data: pass the response status code to perfrom logout action if status code is 401
    */
//
    func didShowAlerWithAction(title:String,message:String,data:NSDictionary,statusCode:Int) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in

            if statusCode == 401 { //logut user
                //UserDefaults.standard.set(App.apvoip_token, forKey: UserDefaltsKeys.PREVIOUS_APVOIP_TOKEN)
                UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_NAME)
                UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_HAS_LOGIN_KEY)
                UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_HEALTH_STATUS)
                UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.TOKENTYPE)
                UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.ACCESS_TOKEN)
                UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
                UserDefaults.standard.set(false,forKey: UserDefaltsKeys.KEY_USER_STATUS)
                UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.MOBILE_NUMBER)
                UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_LOGGED_USER_EMAIL)
                UserDefaults.standard.set(false, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
                UserDefaults.standard.set("", forKey: UserDefaltsKeys.KEY_USER_EMAIL)

                AppSettings.removeEncodedAppSettingsDataFromUserDefaults()
                App.appSettings = nil
                App.user_full_name = ""
                App.imagesURLString.removeAll()
                App.ImagesArray.removeAll()
                App.bookingAttachedImages.removeAll()
                App.user_full_name = ""
                App.userEmail = ""
                App.avatarUrl = ""
                App.isEmailVerified = false
                App.isMobileVerified = false
                App.didUserSubscribed = false
                App.userSubscriptionType = ""
                App.b2bUserType = ""
                App.subscription_details = nil
                App.pending_subscription = nil
                App.canUpgradeSubscription = false
                App.notificationReadCount = 0
                UserDefaults.standard.set(App.notificationReadCount, forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT)
                App.appointmentsList.removeAll()
                App.tempAppointments.removeAll()
                App.lang_preferences_values.removeAll()
                App.lang_preferences_values.append("English")
                RefreshTimer.sharedInstance.stopGlobalTimer()
//                GIDSignIn.sharedInstance().signOut()
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()

//                if !App.isFromLoginView {
//                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                    let vc = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                    self.present(vc, animated: true, completion: nil)
//                }
            }
            else if statusCode == 422 { //perform error action

            }

        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    

        /**
         method Shows only alert with custom title and message
         - Parameter title: pass the title to show in alert title.
         - Parameter message: pass the message to show in alert message.
         */
        func didShowAlert(title: String, message:String) {
            
          //  let addingNewLineMessage = message.inserting(separator: "\n", every: 20)

    //        if let replaceAtIndex = convertedMessage[convertedMessage.index(convertedMessage.startIndex, offsetBy: bar)..<convertedMessage.endIndex].range(of: " ", options: .regularExpression) {
    //            convertedMessage = convertedMessage.replacingCharacters(in: replaceAtIndex, with: "\n")
    //        }

            let alert = UIAlertController(title: title, message: message , preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    
    ///Asks permission for camera
    func didAskForCameraPermission() {
        //Camera
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                print("Access granted for camera")
            } else {
                print("Sorry Access denied for camera")
            }
        }
    }
    
    ///Asks permission for library
    func didAskForPhotoLibraryPermission() {
        PHPhotoLibrary.requestAuthorization({status in
            if status == .authorized{
                print("Access granted for photos")
            } else {
                print("Sorry Access denied for photo")
            }
        })
    }
    
    /**
      Method returns formatted date and time from string and returns string
     - Parameter dateString: pass date string here
     - Returns: The date and time in string format ex: **Aug,9th 2017 08:15 AM**
     */
    func getFormattedDateAndTime(dateString:String) -> String{
        let time = dateString.components(separatedBy: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeZone = .current
        var formattedDate = ""
        if let getDate = date {
           formattedDate = dateFormatter.string(from: getDate)
        }
        return formattedDate + " \(getTimeInAMPM(date:time[1]))"
    }
    
    /**
     function returns time in am/pm
     
     - Parameter date: takes date string
     - Returns: UTC time string ex: 09:20:00
     */
    func getTimeInAMPM(date:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat =  "hh:mm a"
        return dt == nil ? "" : dateFormator.string(from: dt!)
        
    }
    
    /**
     Method returns formatted date string from string
     - Parameter dateString: takes date string
     - Returns: The date and time in string format ex: **Aug,9th 2017 08:15 AM** of medium style
     */
    func getFormattedDate(dateString:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateStyle = .medium
        let formattedDate = date ==  nil ? "" : dateFormatter.string(from: date!)
        return formattedDate
    }
    
    /**
        function converts UTC time string to Local time string used in collection view
        
        - Parameter date: takes date string
        - Returns: UTC time string ex: 09:20:00
        */
       func UTCToLocalForCV(date:String) -> String {
           let dateFormator = DateFormatter()
           dateFormator.dateFormat = "HH:mm:ss"
           dateFormator.timeZone = TimeZone(abbreviation: "UTC")
           
           let dt = dateFormator.date(from: date)
           dateFormator.timeZone = TimeZone.current
           dateFormator.dateFormat = "hh:mm a"
           
           return dateFormator.string(from: dt!)
       }
    
    
    ///Shows alert for asking permission if denied
    func didShowAlertForDeniedPermissions(message:String) {
        if let settingsUrl = URL(string: UIApplication.openSettingsURLString)  {
            let alert = UIAlertController(title: "Access Denied!", message: message, preferredStyle: UIAlertController.Style.alert)
            let okay = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                // If camera or library settings are disabled then open general settings
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(okay)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    ///returns the mime string type
    func mimeTypeForPath(path: String) -> String {
        let pathUrl = NSURL(string: path)
        let pathExtension = pathUrl!.pathExtension
        var stringMimeType = "application/octet-stream"
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as! CFString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                stringMimeType = mimetype as NSString as String
            }
        }
        return stringMimeType;
    }
    
    /**
     Method genereates boundary string
     */
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
        
    }
    
    func checkLibraryPermission() -> Bool
    {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
            
        case .authorized:
            // Access is granted by user.
            return true
            
        case .notDetermined:
            // It is not determined until now.
            return true
            
        case .restricted:
            // User do not have access to photo album.
            return false
            
        case .denied:
            // User has denied the permission.
            return false
        }
    }
    
    
    
    /**
     This method returns string formated date from Date
     * NOTE :: Throws error if date format doesn't matches
     - Parameter date: Takes Date
     - Returns : Date string
     */
    func getDateString(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    
    func sizePerMB(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return size.doubleValue / 1000000.0
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
    }
    
    /**
     function returns current day date
     - Returns: Date string ex:2017-08-31
     */
    func getCurrentDate() -> String{
        let dateForm = DateFormatter()
        dateForm.dateFormat = "yyyy-MM-dd"
        dateForm.timeZone = TimeZone(abbreviation: "UTC")
        let date = Date()
        let todaysDate = dateForm.string(from: date)
        print("Current date and time:=\(todaysDate)")
        return todaysDate
    }
    
    /**
       Method returns current time
     - Returns: The time is returned ex: **08:15 AM**
     */
    func getCurrentTime() -> Date{
        let dateForm = DateFormatter()
        dateForm.dateFormat = "hh:mm a"
        let date = Date()
        let todaysDate = dateForm.string(from: date)
        print("Current date and time:=\(todaysDate)")
        return dateForm.date(from: todaysDate)!
    }
    
    /**
     function returns current time
     - Returns: time string ex:04:04 PM
     */
    func getCurrentTimeInString() -> String{
        let dateForm = DateFormatter()
        dateForm.dateFormat = "hh:mm a"
        let date = Date()
        let todaysDate = dateForm.string(from: date)
        print("Current date and time:=\(todaysDate)")
        return todaysDate
    }
    
    
    /**
     function converts UTC time string to Local time string
     
     - Parameter date: takes date string
     - Returns: UTC time string ex: 09:20:00
     */
    func UTCToLocal(date:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat =  "HH:mm:ss"
        
        return dateFormator.string(from: dt!)
    }
    
    
    /**
      Method returns formatted date string from string
     - Parameter dateString: takes date string
     - Returns: The date and time in string format ex: **Aug,9th 2017** of medium style
     */
    func getFormattedDateOnly(dateString:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateStyle = .medium
        return date == nil ? "" : dateFormatter.string(from: date!)
    }
    
    /**
     Method returns formatted date string
     - Parameter date: takes **Date** here
     - Returns: The date and time in string format ex: **Aug,9th 2017 08:15 AM**
     */
    func getDateFormat(date:Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date = dateFormatter.string(from: date)
        dateFormatter.dateStyle = .medium
        return date
    }
    
    /**
      method performs **GET** request to server and saves the name prefixes.
     */
    func getNamePrefixes() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message:  AlertMessages.NETWORK_ERROR)
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            ///if didn't have name prefixes perform request
            if App.namePrefixes == nil {
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)
                print("URL==>\(AppURLS.URL_NamePrefixes)")
                var request = URLRequest(url: URL(string:AppURLS.URL_NamePrefixes)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
                request.httpMethod = HTTPMethods.GET
                session.dataTask(with: request) { (data, response, error) in
                    if error != nil
                    {
                        print("Error while fetching name prefixes data\(String(describing: error?.localizedDescription))")
                    }
                    else
                    {
                        if let response = response
                        {
                            print("url = \(response.url!)")
                            print("response = \(response)")
                            let httpResponse = response as! HTTPURLResponse
                            print("response code = \(httpResponse.statusCode)")
                            do
                            {
                                if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                                    if !errorStatus {
                                        print("performing error handling get name prefixes:\(jsonData)")
                                    }
                                    else {
                                        let code = jsonData.object(forKey: Keys.KEY_CODE) as? Int
                                        let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as? String
                                       // print("Status=\(responseStatus) code:\(code)")
                                        if let data = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                            if code == 200 && responseStatus == "success"
                                            {
                                                print("prefix response:\(jsonData)")
                                                App.namePrefixes = data
                                                UserDefaults.standard.set(App.namePrefixes, forKey: UserDefaltsKeys.KEY_NAME_PREFIXES)
                                                
                                            }else {
                                                print("Error while fetching appointment details:\(responseStatus) ")
                                            }
                                        }
                                    }
                                }
                            }
                            catch _
                            {
                                self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                                print("Received not-well-formatted JSON")
                            }
                        }
                    }
                    
                    }.resume()
            }
            
            DispatchQueue.main.async {
                
            }
        }
    }
    
    
    /**
     Method returns formatted date and time from string and returns string
     - Parameter dateString: pass date string here
     - Returns: The date and time in string format ex: **Aug,9th 2017 08:15 AM**
     */
    func getISTDateAndTime(dateString:String) -> String{
        let time = dateString.components(separatedBy: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "IST")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeZone = .current
        var formattedDate = ""
        if let getDate = date {
            formattedDate = dateFormatter.string(from: getDate)
        }
        
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "IST")
        
        let dt = dateFormator.date(from: time[1])
        dateFormator.timeZone = TimeZone(identifier: TimeZone.current.identifier)
        dateFormator.dateFormat =  "hh:mm a"
        
        return formattedDate + " \(dt == nil ? "" : dateFormator.string(from: dt!))"
    }
}



class ImagesCollectionViewCellModal {
    var picture:Picture?
    var index:Int?
    var cellIdentifier = TV_CV_CELL_IDENTIFIERS.BookingImage
    var cellFileIdentifier = TV_CV_CELL_IDENTIFIERS.BookingFile
    var isFileSizeGreaterThan25MB = false
    
    init(picture:Picture,index:Int) {
        self.picture = picture
        self.index = index
    }
    
    func cellInstance(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> ImagesCollectionViewCell {
        if self.picture?.type?.rawValue  == FileType.file.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellFileIdentifier, for: indexPath) as! ImagesCollectionViewCell
            
            cell.webViewCellSetup(vm: self)
            print("Loaded file cell")
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImagesCollectionViewCell
            cell.cellSetup(vm: self)
            print("Loaded image cell")
            return cell
        }
        
    }
}



extension UserDefaults {
    struct Keys {
        static let encodedUserDetails = "EncodedUserDetails"
        static let encodedCurrentPlanDetails = "EncodedCurrentPlanDetails"
        static let encodedPendingPlanDetails = "EncodedPendingPlanDetails"
        static let onlySubscriptionInfo = "OnlySubscriptionInfo"
        static let familyPackPlanDetails = "FamilyPlanDetails"
        static let oneTimePlanDetails = "OneTimePlanDetails"
    }
}



///Setting up the navigation bar background color
extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        if #available(iOS 11, *) {//for ios 10 devices
        }else {
            updatedFrame.size.height += 20
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = colors
        setBackgroundImage(gradientLayer.creatGradientImage(), for: UIBarMetrics.default)
    }
}


extension UINavigationItem {
    /// Sets the title and subtitle for navigation bar
    ///
    /// - Parameters:
    ///   - title: the title text goes here
    ///   - subtitle: the subtitle text goes here
    func setNewTitle(_ title: String, subtitle: String) {
//        let appearance = UINavigationBar.appearance()
       // let textColor = appearance.titleTextAttributes?[NSAttributedString.Key.foregroundColor] as? UIColor ?? .black
        
        let titleLabel = MarqueeLabel()
        titleLabel.text = title
        titleLabel.font = .preferredFont(forTextStyle: UIFont.TextStyle.headline)
        titleLabel.textColor = .white
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel()
        subtitleLabel.text = subtitle
        subtitleLabel.font = .preferredFont(forTextStyle: UIFont.TextStyle.subheadline)
        subtitleLabel.textColor = UIColor.white.withAlphaComponent(0.75)
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
       // stackView.frame = CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width),height:40)
        stackView.distribution = .equalCentering
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.clipsToBounds = true
        
        self.titleView = stackView
        
        DispatchQueue.main.async {
            if titleLabel.isTruncated {
                titleLabel.type = .continuous
                titleLabel.speed = .duration(15)
                titleLabel.animationCurve = .easeInOut
                titleLabel.fadeLength = 6.0
                titleLabel.trailingBuffer = 13.0
            }else {
                titleLabel.holdScrolling = true
            }
        }
    }
    
    
    /// Sets the title and subtitle for navigation bar
    ///
    /// - Parameters:
    ///   - title: the title text goes here
    ///   - subtitle: the subtitle text goes here
    @available(*, deprecated)
    func setChatTitleView(with title:String, subtitle:String) {
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -2, width: 0, height: 0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        titleLabel.text = title
        titleLabel.textAlignment = .left
        titleLabel.clipsToBounds = true
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x:0, y:17, width:0, height:0))
        subtitleLabel.backgroundColor = .clear
        subtitleLabel.textColor = .white
        subtitleLabel.font = UIFont.systemFont(ofSize: 11)
        subtitleLabel.text = subtitle
        subtitleLabel.textAlignment = .left
        subtitleLabel.sizeToFit()
        
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        titleView.clipsToBounds = true
        
        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width
        
        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
//            subtitleLabel.frame = CGRect(x: 0, y: 18, width:titleView.frame.size.width , height: 12)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }
        
//        UIView.animate(withDuration: 12.0, delay: 2, options: ([.curveLinear, .repeat]), animations: {
//            titleLabel.center = CGPoint(x:0 - titleLabel.bounds.size.width / 2, y: titleLabel.center.y)
//        }, completion: nil)
        
        self.titleView = titleView
    }

}

///Adds gradient color image to navigation bar
extension CAGradientLayer {
    
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 0)
        endPoint = CGPoint(x: 0.5, y: 0)
    }
    
    func creatGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
}



extension String {
    
    
    func getTimeInAMPM() -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormator.date(from: self)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat =  "hh:mm a"
        return dt == nil ? "" : dateFormator.string(from: dt!)
        
    }
    
    var parseJSONString: AnyObject?
    {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if let jsonData = data
        {
            // Will return an object or nil if JSON decoding fails
            do
            {
                let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
                if let jsonResult = message as? NSMutableArray {
                    return jsonResult //Will return the json array output
                } else if let jsonResult = message as? NSMutableDictionary {
                    return jsonResult //Will return the json dictionary output
                } else {
                    return nil
                }
            }
            catch let error as NSError
            {
                print("An error occurred: \(error)")
                return nil
            }
        }
        else
        {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
    
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
    
    var ns: NSString {
        return self as NSString
    }
    ///The path extension, if any, of the string as interpreted as a path.
    var pathExtension: String? {
        return ns.pathExtension
    }
    ///The last path component of the receiver.
    var lastPathComponent: String? {
        return ns.lastPathComponent
    }
    
    
    func convertStringToNumberalIfDifferentLanguage() -> NSNumber? {
        let Formatter: NumberFormatter = NumberFormatter()
        Formatter.locale = Locale(identifier: "EN")
        let final = Formatter.number(from: self)
        return final
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
        
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}



@IBDesignable class UnitsLabel: UILabel {
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray
    @IBInspectable var topInset: CGFloat = 4.0
    @IBInspectable var bottomInset: CGFloat = 4.0
    @IBInspectable var leftInset: CGFloat = 4.0
    @IBInspectable var rightInset: CGFloat = 4.0
    @IBInspectable var radius: CGFloat {
        get {
            return self.layer.cornerRadius
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    
    func setup() {
        layer.borderWidth = 0.5
        layer.borderColor = borderColor.cgColor
        clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
}



extension UIColor {
    /**
     Extension of UIColor to get color from hex string
     */
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
  

}


extension NSMutableData {
    /**
     Used In image uploading method to append strings while uploading image
     */
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}



extension NSMutableAttributedString {
    /**
       used to make wanted text bold in a single string  *EX: Used in Consent form page view controller*
     */
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont(name: "AvenirNext-Medium", size: 16)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    /**
     used to make wanted text bold in a single string  *EX: Used in Consent form page view controller*
     */
    @discardableResult func boldWithSystem(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.boldSystemFont(ofSize: 16) ]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    /**
     used to make wanted text to be normal *EX: Used in Consent form page view controller*
     */
    @discardableResult func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
    
    /**
     used to make wanted text bold in a single string  *EX: Used in Consent form page view controller*
     */
    @discardableResult func normalWithSystem(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.systemFont(ofSize: 16) ]
        let normalString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normalString)
        return self
    }
}



extension Int {
    /**
     method takes the Int and returns the name of the month.
     
     - Parameter day: takes the month count
     - Returns : The name of the month
     - SeeAlso:  `getDayOfWeek(_ today:)`
     */
    func getMonthName() -> String {
        switch self {
        case 1:
            return "JAN"
        case 2:
            return "FEB"
        case 3:
            return "MAR"
        case 4:
            return "APR"
        case 5:
            return "MAY"
        case 6:
            return "JUN"
        case 7:
            return "JUL"
        case 8:
            return "AUG"
        case 9:
            return "SEP"
        case 10:
            return "OCT"
        case 11:
            return "NOV"
        case 12:
            return "DEC"
            
        default:
            return ""
        }
    }
}



extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}



extension Double {
    
    
    func getDateStringFromTimeStamp() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMMM yyyy hh:mm a"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



extension DateFormatter {
    /**
     Formats a date as the time since that date (e.g., “Last week, yesterday, etc.”).
     
     - Parameter from: The date to process.
     - Parameter numericDates: Determines if we should return a numeric variant, e.g. "1 month ago" vs. "Last month".
     
     - Returns: A string with formatted `date`.
     */
    
    func timeSince(from: NSDate,chatDate:String ,numericDates: Bool = false) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = now.earlierDate(from as Date)
        let latest = earliest == now as Date ? from : now
        let components = calendar.dateComponents([.year, .weekOfYear, .month, .day, .hour, .minute, .second], from: earliest, to: latest as Date)
        
        var result = ""
        
        if components.year! >= 2 {
            result = "\(components.year!) years ago"
        } else if components.year! >= 1 {
            if numericDates {
                result = "1 year ago"
            } else {
                result = chatDate.isEmpty ? "Last year" : chatDate
            }
        } else if components.month! >= 2 {
            result = "\(components.month!) months ago"
        } else if components.month! >= 1 {
            if numericDates {
                result = "1 month ago"
            } else {
                result = chatDate.isEmpty ? "Last month" : chatDate
            }
        } else if components.weekOfYear! >= 2 {
            result = chatDate.isEmpty ? "\(components.weekOfYear!) weeks ago" : chatDate
        } else if components.weekOfYear! >= 1 {
            if numericDates {
                result = "1 week ago"
            } else {
                result = chatDate.isEmpty ?  "Last week" : chatDate
            }
        } else if components.day! >= 2 {
            result = chatDate.isEmpty ? "\(components.day!) days ago" : chatDate
        } else if components.day! >= 1 {
            if numericDates {
                result = "1 day ago"
            } else {
                result = "Yesterday"
            }
        } else if components.hour! >= 2 {
            result = "\(components.hour!) hours ago"
        } else if components.hour! >= 1 {
            if numericDates {
                result = "1 hour ago"
            } else {
                result = "An hour ago"
            }
        } else if components.minute! >= 2 {
            result = "\(components.minute!) minutes ago"
        } else if components.minute! >= 1 {
            if numericDates {
                result = "1 minute ago"
            } else {
                result = "A minute ago"
            }
        } else if components.second! >= 3 {
            result = "\(components.second!) seconds ago"
        } else {
            result = "Just now"
        }
        
        return result
    }
    
}

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func loadImage(workoutID: Int,indicator: UIActivityIndicatorView){
        let imgURL = AppURLS.URL_WELLNESS_WORKOUT_IMAGE+"\(workoutID)"
        if let imageFromCache = imageCache.object(forKey: imgURL as AnyObject) as? UIImage{
            DispatchQueue.main.async {
                self.image = imageFromCache
            }
            
        }else{
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        DispatchQueue.main.async {
            indicator.startAnimating()
        }
        
        print("img url \(imgURL)")
        let url = URL(string: imgURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.GET
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            
//            print("res wellness \((response as! HTTPURLResponse).statusCode)")
            
            
            if let response = response, let statusCode = response as? HTTPURLResponse, statusCode.statusCode == 200{
                do{
                    DispatchQueue.main.async {
                        let imageToCache = UIImage(data: data!)
                        imageCache.setObject(imageToCache!, forKey: imgURL as AnyObject)
                        self.image = imageToCache
                        indicator.stopAnimating()
                    }
                    
                }
            }else{
                DispatchQueue.main.async {
                    indicator.stopAnimating()
                }
                
//                self.checkOnlyStatus(statusCode: statusCode)
            }
        }).resume()
        }
    }
    
}



var vSpinner : UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.white
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.color = Theme.buttonBackgroundColor
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}


extension UILabel
{
    var isTruncated: Bool {
        
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
}



extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 3) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
