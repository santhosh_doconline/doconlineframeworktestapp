//
//  AppointmentSlotBookingViewController.swift
//  DocOnline
//
//  Created by dev-3 on 10/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class TimeSlots {
    var dateKey : String!
    var timesArray : [String]!
    
    init(dateKey:String,times:[String]) {
        self.dateKey = dateKey
        self.timesArray = times
    }
}

var selectedConvertedDateFeomslot = ""

class AppointmentSlotBookingViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var slotCollectionView: UICollectionView!
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var lb_slot_status: UILabel!

    ///index to load slots of date
    var index : Int?
    ///stores only date ex_format: 2017-08-31
    var serverDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //to call loadjobslist method in post job controller
        NotificationCenter.default.addObserver(self, selector: #selector(AppointmentSlotBookingViewController.reloadCollectionView(notification:)),name:NSNotification.Name(rawValue: "ReloadSlots"), object: nil)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //viewSetup()
        if index != nil {
            self.lb_slot_status.isHidden = true
            loadSlots(index: index!)
        }else {
            print("index is nil")
            let currentDate = "\(Date())"
            let dateString = currentDate.components(separatedBy: " ")
            let formatedDate = getFormattedDate(dateString: "\(dateString[0]) \(dateString[1])")
            print("formatedDate:\(formatedDate)")
            self.lb_date.text = formatedDate
            self.lb_slot_status.isHidden = false
        }
    }
    
    /**
     Reloads the slots in collection view
     */
    @objc func reloadCollectionView(notification:Notification) {
//        print("Notification observer performed :  slots array:\(App.slots)")
        self.slotCollectionView.reloadData()
    }
    
    
    /**
      collection view setup
     */
    func viewSetup() {
         self.slotCollectionView.layer.shadowOpacity = 0.3
         self.slotCollectionView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
         self.slotCollectionView.layer.shadowRadius = 2.0
         self.slotCollectionView.layer.shadowColor = UIColor.darkGray.cgColor
         self.slotCollectionView.layer.cornerRadius = 20
         self.slotCollectionView.clipsToBounds = true
    }
    
    
    /**
      loads slots with date index
     */
    func loadSlots(index:Int) {
      
            startAnimating()
            print("Modal Array count:\( App.timeSlots.count)")
            let values =  App.timeSlots[index]
            let separatedDate = values.dateKey!.components(separatedBy: " ")
            serverDate = separatedDate[0]
            print("dates:\(values.dateKey!) Index:=\(index)")
            print("slots array:\(values.timesArray)")
            let formatedDate = getFormattedDate(dateString: values.dateKey!)
            print("formatedDate:\(formatedDate)")
            self.lb_date.text = formatedDate
            App.slots = values.timesArray!
            print("App slots:\( App.slots)")
            self.slotCollectionView.reloadData()
            self.stopAnimating()

    }
    
    
    
    @IBAction func nextDateTapped(_ sender: UIButton) {
        if index != nil {
            if index! < App.timeSlots.count - 1  {
                index! += 1
                print("Index=\(index!) count:\(App.timeSlots.count)")
                if  App.timeSlots.count > 1 {
                    loadSlots(index: index!)
                }else {
                    //index = App.timeSlots.count - 1
                    print("Sorry Next dates or not available index:\(index!)")
                }
            }else {
                self.didShowAlert(title: "Sorry", message: "No more dates available")
               // AlertView.sharedInsance.showInfo(title: "Sorry", message: "No more dates available")
            }
            print("Index=\(index!) sdsds")
        }
    }
    
    @IBAction func previousButtonTapped(_ sender: UIButton) {
        //index -= 1
        if index != nil {
            if  App.timeSlots.count > 1 {
                if index! == 0 {
                    loadSlots(index: index!)
                    print("Sorry previous dates or not available")
                }else {
                    index! -= 1
                    loadSlots(index: index!)
                }
            }else {
                self.didShowAlert(title: "Sorry", message: "No more dates available")
               // AlertView.sharedInsance.showInfo(title: "Sorry", message: "No more dates available")
            }
            print("Index=\(index!)")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AppointmentSlotBookingViewController : UICollectionViewDelegate ,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  App.slots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TV_CV_CELL_IDENTIFIERS.CELL, for: indexPath) as! AppointmentSlotBookingCollectionViewCell
        let time =  App.slots[indexPath.row]
        cell.lb_time.text = UTCToLocalForCV(date: time)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let time =  App.slots[indexPath.row]
        let mainDate = serverDate + " \(time)"
        selectedConvertedDateFeomslot = serverDate + " \(UTCToLocal(date: time))"
        App.selectedSlotTime = mainDate
        print("Selected Date:\(mainDate) convertedDate:\(selectedConvertedDateFeomslot)")
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension AppointmentSlotBookingViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 25
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/3, height: 50)
    }
}

