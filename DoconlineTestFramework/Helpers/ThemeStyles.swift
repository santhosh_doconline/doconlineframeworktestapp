//
//  Styles.swift
//  FirstApp
//
//  Created by SanthoshKumar.bangalore on 06/01/20.
//  Copyright © 2020 SanthoshKumar.bangalore. All rights reserved.
//

import Foundation
import UIKit

enum CompanyType{
    case defaultTheme
    case betterplace
    case tataMotors
    
    init() {
        self = .defaultTheme
    }
}

struct Theme {
    
    static var backgroundColor:UIColor?
    static var buttonTextColor:UIColor?
    static var buttonBackgroundColor:UIColor?
    static var navigationGradientColor:[UIColor?]?
    static var switchOnTintColor:UIColor?
    static var switchThumbTintColor:UIColor?
    static var litebuttonBackgroundColor: UIColor?
    static var titleImg: UIImage?
    static var titleBackgroundImg: UIImage?
    static var companyType: CompanyType?
    static var minorBackgroungImg: UIImage?

   static public func defaultTheme() {
    self.navigationGradientColor = [hexStringToUIColor(hex: StandardColorCodes.GREEN),hexStringToUIColor(hex: StandardColorCodes.BLUE)]
       self.backgroundColor = UIColor.red
       self.buttonTextColor = UIColor.white
    self.buttonBackgroundColor = hexStringToUIColor(hex: StandardColorCodes.GREEN)
    self.switchOnTintColor = hexStringToUIColor(hex: StandardColorCodes.GREEN)
    self.switchThumbTintColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)
//    self.switchThumbTintColor = UIColor.gray

    self.litebuttonBackgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
    self.titleImg = UIImage(named: "DoctorLogo")
    self.titleBackgroundImg = UIImage(named: "background")
//    self.iconSet = .defaultTheme
    self.minorBackgroungImg = UIImage(named: "behind_bg")
    self.companyType = .defaultTheme
   }

   static public func betterplaceTheme() {
    self.navigationGradientColor = [hexStringToUIColor(hex: StandardColorCodes.BetterPlace_Gradient_Color1), hexStringToUIColor(hex: StandardColorCodes.BetterPlace_Gradient_Color2)]
       self.backgroundColor = UIColor.darkGray
       self.buttonTextColor = UIColor.white
       self.buttonBackgroundColor = hexStringToUIColor(hex: StandardColorCodes.BetterPlace_Gradient_Color1)
    self.switchOnTintColor = hexStringToUIColor(hex: StandardColorCodes.BetterPlace_Gradient_Color1)
    self.switchThumbTintColor = hexStringToUIColor(hex: StandardColorCodes.BetterPlace_Gradient_Color2)
    self.litebuttonBackgroundColor = hexStringToUIColor(hex: StandardColorCodes.BetterplaceHeader_Lite_Btn_Color)
    self.titleImg = UIImage(named: "betterplace_logo")
    self.titleBackgroundImg = UIImage(named: "b_background")
//    self.iconSet = .betterplace
    self.minorBackgroungImg = UIImage(named: "b_background")
    self.companyType = .betterplace
   }
    
    
    static public func tatamotorsTheme() {
        self.navigationGradientColor = [hexStringToUIColor(hex: StandardColorCodes.Tata_Gradient_Color1), hexStringToUIColor(hex: StandardColorCodes.Tata_Gradient_Color2)]
           self.backgroundColor = UIColor.darkGray
           self.buttonTextColor = UIColor.white
           self.buttonBackgroundColor = hexStringToUIColor(hex: StandardColorCodes.Tata_Gradient_Color1)
        self.switchOnTintColor = hexStringToUIColor(hex: StandardColorCodes.Tata_Gradient_Color1)
        self.switchThumbTintColor = UIColor.gray
        self.litebuttonBackgroundColor = hexStringToUIColor(hex: StandardColorCodes.Tata_Gradient_Color2)
        self.titleImg = UIImage(named: "betterplace_logo")
        self.titleBackgroundImg = UIImage(named: "b_background")
    //    self.iconSet = .betterplace
        self.companyType = .tataMotors
        self.minorBackgroungImg = UIImage(named: "b_background")
       }
    
    
   static public func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}



/**
   Statically declared notification types
 */
struct NotificationType {
    ///Incoming call notification type *Note : * Used to check in if condition
    static let UserAppointmentIncomingCallNotification = "UserAppointmentIncomingCallNotification"
    ///Booking notification of type success
    static  let  KEY_NOTIFICATION_TYPE_BOOKING_SUCCESS = "BookAppointmentSuccess"
    ///Booking notification of type failure
    static  let  KEY_NOTIFICATION_TYPE_BOOKING_FAILED = "BookAppointmentFailed"
    ///Appointment reassigned notification
    static  let  KEY_NOTIFICATION_TYPE_APPOINTMENT_REASSIGNED = "AppointmentReassignedNotification"
}
