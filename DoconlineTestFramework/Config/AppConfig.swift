//
//  AppConfig.swift
//  DoconlineTestFramework
//
//  Created by Santosh Kumar on 16/07/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import UIKit
import PushKit
import CallKit
//import Firebase
//import IQKeyboardManagerSwift

public class AppConfig {
    
    public init() {
        AppEnvironment.shared().environment = .demo
        Theme.defaultTheme()
        IQKeyboardManager.shared.enable = true
    }
    
//    public static var shared: AppConfig {
//        return appConfigRef
//    }
    
    public static var shared: AppConfig? = nil
    var appConfigRef: AppConfig? = nil
    var providerDelegate: ProviderDelegate? = nil
    var callRefreshTimer: Timer? = nil
    var currentCallUUID: UUID? = nil
    var callManager: SpeakerboxCallManager? = nil
    ///background task identifier for ending the voip call ringing screen
    var callBackgroundHandlerIdentifier : UIBackgroundTaskIdentifier?
    var fcmToken = ""
    var bearerType = ""
    var accesstoken = ""
    
    public var fcm_Token: String = ""{
        didSet{
            fcmToken = fcm_Token
        }
    }
    
    public var bearer_type: String = ""{
        didSet{
            bearerType = bearer_type
            UserDefaults.standard.set(bearer_type, forKey: UserDefaltsKeys.TOKENTYPE)
        }
    }
    
    public var access_token: String = ""{
        didSet{
            accesstoken = access_token
            UserDefaults.standard.set(accesstoken, forKey: UserDefaltsKeys.ACCESS_TOKEN)
        }
    }
        
     var proDelegate: ProviderDelegate? = nil{
        didSet{
            providerDelegate = proDelegate
        }
    }
     var refreshTimer : Timer? = nil{
        didSet{
            callRefreshTimer = refreshTimer
        }
    }
    public var currentUUID : UUID? = nil{
        didSet{
            currentCallUUID = currentUUID
        }
    }
    
     var callManagerr: SpeakerboxCallManager? = nil{
        didSet{
            callManager = callManagerr
        }
    }
    
    public var callSession_Id: String = ""{
        didSet{
            callSessionID = callSession_Id
        }
    }
    
    public var call_DoctorModel: Doctor = Doctor(){
        didSet{
            callDoctorModal = call_DoctorModel
        }
    }
    
    public var tokenId: String = ""{
        didSet{
            callTokenid = tokenId
        }
    }
    
    public var incomingCall_Type: Int = 0{
        didSet{
            incomingCallType = incomingCall_Type
        }
    }
    
    public var incomingNotification_Type: String = ""{
        didSet{
            incomingNotificationType = incomingNotification_Type
        }
    }
    
    public var isCallingView_Presenting: Bool = false{
        didSet{
            isCallingViewPresenting = isCallingView_Presenting
        }
    }
    
    public func didUpdatePushRegistry(pushCredentials: PKPushCredentials){
        let deviceToken = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1) })
        App.apvoip_token = deviceToken
        print("\(#function) VOIP token is: \(deviceToken)")
        NotificationCenter.default.post(name: Notification.Name("voip_token"), object: nil)
        NSLog("didupdate push registry")
    }
    
    public func didReceiveIncomingPush(didReceiveIncomingPushWith payload: PKPushPayload, voipPushPayload voipPayload: [AnyHashable:Any]){
//        print("\(#function) incoming voip notfication: \(payload.dictionaryPayload)")

        NSLog("did receive incoming push")

        if let calls  = self.proDelegate?.callManager.calls {
            for call in calls {
                self.proDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
            }
        }else if self.providerDelegate != nil {
            self.proDelegate?.provider.reportCall(with: self.currentUUID! , endedAt: nil, reason: CXCallEndedReason.remoteEnded)
        }

        if self.callRefreshTimer != nil {
            self.callRefreshTimer?.invalidate()
            self.callRefreshTimer = nil
        }


        self.callManagerr = nil
        self.proDelegate = nil
        self.currentUUID = nil


//        self.voipPushPayload = payload.dictionaryPayload
        self.currentCallUUID = UUID()
        self.voipCallNotificationFetcher(userInfo: payload.dictionaryPayload) { (success, hasVideo, doctor) in
            if success
            {
                NSLog("voipcallfetcher callback")
                self.callManagerr = SpeakerboxCallManager()
                self.proDelegate = ProviderDelegate(callManager: self.callManager!)
                print("Current call UUID=-=->\(self.currentCallUUID)")
                OTDefaultAudioDevice.sharedInstance().typeOfCall = incomingCallType == 1 ? "audio" : "video"
                OTAudioDeviceManager.setAudioDevice(OTDefaultAudioDevice.sharedInstance())
                //   display incoming call UI when receiving incoming voip notification
                let backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
                self.displayIncomingCall(uuid:  self.currentCallUUID! , handle: doctor, hasVideo: hasVideo) { _ in
                    UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                }
            }
        }
    }
    //gud
    /// Display the incoming call to the user
    func displayIncomingCall(uuid: UUID, handle: String, hasVideo: Bool = false, completion: ((NSError?) -> Void)? = nil) {
        providerDelegate?.reportIncomingCall(uuid: uuid, handle: handle, hasVideo: hasVideo, completion: completion)
        NSLog("display incoming call")
        callBackTimer()
    }
    

    fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int {
        return input.rawValue
    }
    
    func callBackTimer() {
         self.callBackgroundHandlerIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            print("expired!")
            UIApplication.shared.endBackgroundTask( self.callBackgroundHandlerIdentifier!)
            self.callBackgroundHandlerIdentifier = UIBackgroundTaskIdentifier(rawValue: self.convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid))
        })

//        let ringingSeconds = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.RINGING_DURATION).stringValue ?? "30"
        let ringingSeconds = "30"


        self.callRefreshTimer = Timer.scheduledTimer(timeInterval: Double(ringingSeconds) ?? 30 , target: self, selector: #selector(self.hangUpOnCallTimeOut), userInfo: nil, repeats: false)
        print("Ringing Seconds::\(ringingSeconds)  Double:\(Double(ringingSeconds) ?? 30)")
    }
    

        @objc func hangUpOnCallTimeOut(){


            if let calls  = self.proDelegate?.callManager.calls {
                for call in calls {
                    self.proDelegate?.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                    isCallingViewPresenting = false
    //                schedulteAppointmentRemainder(timeInterVal: 1 , time: "")
                }
            }else {
                print("calls array is emtpy")
            }

            if self.callRefreshTimer != nil {
                self.callRefreshTimer?.invalidate()
                self.callRefreshTimer = nil
            }

            if self.callBackgroundHandlerIdentifier?.rawValue != convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid) {
                UIApplication.shared.endBackgroundTask(self.callBackgroundHandlerIdentifier!)
                self.callBackgroundHandlerIdentifier = UIBackgroundTaskIdentifier(rawValue: convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid))
            }
        }
    
    
    func stringToDateConverter(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: date)
        
        dateFormatter.timeZone = NSTimeZone.local
        return date!
    }

    
            @objc func voipCallNotificationFetcher(userInfo : [AnyHashable:Any],completionHandler : @escaping (_ success:Bool,_ hasVideo:Bool,_ doctorName:String) -> Void) {
        //       / let userInfo = userInfo
    //            print("**Tokend is empty: \(callTokenid.isEmpty) SessionID is empty:\(callSessionID.isEmpty)**")
                App.timeDuration = ""
                if isCallingViewPresenting
                {
                    print("Already in another call")
                }
                else
                {
                    ///This variable is used to store time in seconds of current time to Appointment call time
                    var calculatedSeconds  = 0
                    var scheduledDate = ""
                    var handle = ""
                    // print("$4 appoid = \(userInfo["appointment_id"] as! String)")
                    
                    if let notificationType = userInfo["notification_type"] as? String {
                        print("Notification type:\(notificationType)")
                        self.incomingNotification_Type = notificationType
                    }
                    
                    if let scheduledAt = userInfo["scheduled_at"] as? String {
                        print("Scheduled At:\(scheduledAt)")
                        scheduledDate = scheduledAt
                        let toTime = stringToDateConverter(date:scheduledAt)
                        //if Calendar.current.isDateInToday(toTime) {
                        print("The call is on current date")
                        let currentTime =  Date()
                        calculatedSeconds = Int(toTime.timeIntervalSince(currentTime))
                        //}else {
                        print("Notification call seconds::>> \(calculatedSeconds)")
                        // }
                    }
                    
                    if let appID = userInfo["appointment_id"] as? String {
                        print("++> appointid:\(appID) string")
                        App.call_appointment_id = appID
                    }else {
                        if let appID = userInfo["appointment_id"] as? Int {
                            print("++> appointid:\(appID) INT")
                            App.call_appointment_id = "\(appID)"
                        }
                    }
                    
                    print("\(#function) called==")
                    var doctorDetails : NSDictionary?
                    //  print("doctor modal:==# \(userInfo["doctor"] as! String)")
                    if let doctorString = userInfo["doctor"] as? String {
                        let data = doctorString.data(using: String.Encoding.utf8)
                        do
                        {
                            if let doctor = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary
                            {
                                doctorDetails = doctor
                            }
                            
                        }catch let error {
                            print("Error while getting notification schedule==>\(error.localizedDescription)")
                        }
                    }else  {
                        if let doctorD =  userInfo["doctor"] as? NSDictionary {
                            doctorDetails = doctorD
                        }
                    }
                    
                    if let doc = doctorDetails {
                        if let id = doc.object(forKey: Keys.KEY_ID) as? Int
                        {
                            if let fristName = doc.object(forKey: Keys.KEY_FIRST_NAME) as? String
                            {
                                if let middle = doc.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                {
                                    if let last = doc.object(forKey: Keys.KEY_LAST_NAME) as? String
                                    {
                                        if let practnum = doc.object(forKey: Keys.KEY_PRACTITIONER_NUMBER) as? Int {
                                            if let avatar = doc.object(forKey: Keys.KEY_AVATAR_URL) as? String
                                            {
                                                if let prefix = doc.object(forKey: Keys.KEY_PREFIX) as? String
                                                {
                                                    if let specialisation = doc.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                        if let fullName = doc.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                            let micCode = doc.object(forKey: Keys.KEY_MCI_CODE) as? String
                                                            if let ratings = doc.object(forKey: Keys.KEY_RATINGS) as? Double {
                                                    ///Global variable declare in appdelegate class to access in calling view
                                                    handle = "\(prefix) \(fristName) \(last)"
                                                                self.call_DoctorModel = Doctor(prefix:prefix,id:id,first_name:fristName,middle_name:middle,last_name:last,practitioner_number:practnum,avatar_url:avatar,fullname:fullName,specialization:specialisation,rating:ratings)
                                                                self.call_DoctorModel.appointment_id =  App.call_appointment_id
                                                                self.call_DoctorModel.mciCode = micCode ?? ""
                                                                print("prefix==>>\(fristName) id:\(id) appointment id:\(  self.call_DoctorModel.appointment_id)")
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
         
                    if let tokenID = userInfo["tokenId"] as? String {
                        print("++> tokenID:\(tokenID)")
                        callTokenid = tokenID
                    }
                    
                    if let callType = userInfo["call_type"] as? String {
                        print("++> $calltype:\(callType) string")
                        incomingCallType = Int(callType)!
                    }else {
                        if let callType = userInfo["call_type"] as? Int {
                            print("++> $calltype:\(callType) Int")
                            incomingCallType = callType
                        }
                    }
                    
                    if let sessID = userInfo["sessionId"] as? String {
                        print("++> sessionid:\(sessID)")
                        callSessionID = sessID
                    }
                    
                    if let aps = userInfo["aps"] as? NSDictionary {
                        if let alert = aps["alert"] as? NSDictionary {
                            if let message = alert["body"] as? NSString {
                                if let title = alert["title"] as? NSString {
                                    print("Message:\(message) title:\(title)")
                                }
                            }
                        }
                    }
                    
                     let queue1 = DispatchQueue(label: "com.doconline.doconline.callqueue1", qos: DispatchQoS.background)
                     queue1.async {
                         CallStatus.sharedInstance.perfromCallRecievedRequest(callStatus: CallingStatus.CALL_STATUS_PATIENT_ACKNOWLEDGED)
                     }
                   
                    
                    
                    ///Check if notification is call type handle navigation to calling view
                    if incomingNotificationType.lowercased() == "call" && !App.getUserAccessToken().isEmpty
                    {
                        
                        let hasVideo = incomingCallType == 1 ? false : true
    //                    FloatingButtonWindow.sharedInstance.removeTimer()
                        App.isIncomingCallRecieved = true
                        completionHandler(true, hasVideo, handle)
         
                    }else {
                        print("User logged out previously")
                         completionHandler(false, false, "")
                    }
                }
            }
    
//    public func showDashboard()->UIViewController{
//        let storybaord = UIStoryboard(name: "MainSto", bundle: nil)
//        let dashboard = storybaord.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//
//        return dashboard
//    }
}
