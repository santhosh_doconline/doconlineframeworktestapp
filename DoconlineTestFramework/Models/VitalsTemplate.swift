//
//  VitalsTemplate.swift
//  DocOnline
//
//  Created by Kiran Kumar on 24/08/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import Foundation

enum ValueUnion: Codable {
    case double(Double)
    case bloodPressure(BloodPressure)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Double.self) {
            self = .double(x)
            return
        }
        if let x = try? container.decode(BloodPressure.self) {
            self = .bloodPressure(x)
            return
        }
        throw DecodingError.typeMismatch(ValueUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ValueUnion"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .double(let x):
            try container.encode(x)
        case .bloodPressure(let x):
            try container.encode(x)
        }
    }
}


class VitalPages: Codable {
    var currentPage: Int?
    var nextPage: Int?
    var perPage: Int?
    var prevPage: Int?
    var total: Int?
    var recordData: [VitalRecord]?
    
    private enum CodingKeys: String, CodingKey {
        case currentPage
        case nextPage
        case perPage
        case prevPage
        case total
        case recordData = "data"
    }
    
    
    class func getVitalRecodsData(url:String,onCompletion: @escaping (_ success:Bool,_ records:VitalPages?,_ statusCode:Int,_ response:NSDictionary?) -> Void ) {
        NetworkCall.performGet(url: url) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting template:\(err.localizedDescription)")
                onCompletion(false,nil, statusCode ?? 0, response)
            }
            
            if success {
                if let resp = response ,
                    let data = resp.object(forKey: Keys.KEY_DATA) as? [String: Any] {
                    do  {
                        print("Record values Response:\(resp)")
                        let dicData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        onCompletion(true, try decoder.decode(VitalPages.self, from: dicData) , statusCode ?? 0, response)
                        
                    }catch let decodeErr {
                        print("Error while decoding records:\(decodeErr.localizedDescription)")
                        onCompletion(false,nil, statusCode ?? 0, response)
                    }
                }else {
                     onCompletion(false,nil, statusCode ?? 0, response)
                }
            }else {
                onCompletion(false,nil, statusCode ?? 0, response)
            }
        }
    }
}

class VitalRecord : Codable {
    var _id:String?
    var _meta:RecordMetaInfo?
    var createdAt: String?
    var profileType: String?
    var recordedAt: Double?
    var recordValues: [VitalTemplateFields]?
    var userId: Int?
    var isEditMode = false
    
    private enum CodingKeys: String, CodingKey {
        case _id
        case _meta
        case createdAt = "created_at"
        case profileType = "profile_type"
        case recordedAt = "recorded_at"
        case recordValues = "records"
        case userId = "user_id"
    }
    
    
    /// Deletes the selected record at index with id
    ///
    /// - Parameter recordId: provide the record id
    func deleteRecord(completion: @escaping (_ success:Bool) -> Void) {
        NetworkCall.performDELETE(url: AppURLS.URL_GET_VITAL_RECORDS + "/\(self._id ?? "")") { (success, response, statusCode, error) in
            if let err = error {
                print("Error while deleting record:\(err.localizedDescription)")
                completion(false)
            }
            completion(success)
        }
    }
}

struct RecordMetaInfo : Codable{
    var dataType: String?
    private enum CodingKeys: String, CodingKey {
        case dataType = "data_type"
    }
}

class VitalsTemplate: Codable{
    var profileType: String?
    var rootFileds: [VitalTemplateFields]?
    var recordFields: [VitalTemplateFields]?
    var userVitalInfo: UserVitalInfo? = nil
    var totalFields = [VitalTemplateFields]()
    
    private enum CodingKeys: String, CodingKey {
        case profileType = "profile_type"
        case rootFileds = "root_fields"
        case recordFields = "record_fields"
    }
    
    func getDicRepresentation() -> [String:Any] {
        var recordDate: TimeInterval = 0
        var dic = [String:Any]()
        var fieldsDic = [String:Any]()
    
        for field in self.totalFields {
            if (field.unit ?? "").lowercased().isEqual("date") {
                recordDate = TimeInterval( String(format: "%.0f", field.fieldValue ?? 0) ) ?? 0
            }else if !(field.fields ?? []).isEmpty {
                var pressureDic = [String:Any]()
                field.fields?.forEach{ pressureDic[$0.key ?? ""] = $0.fieldValue ?? nil }
                fieldsDic[field.key ?? ""] = pressureDic
            }else {
                fieldsDic[field.key ?? ""] = field.fieldValue ?? nil
            }
        }
        dic[UserVitalInfo.SERILIZATION_KEY.RECORDS] = fieldsDic
        dic[UserVitalInfo.SERILIZATION_KEY.RECORDED_AT] = recordDate
        return dic
    }
    
    /// Updates the existing record with new values
    ///
    /// - Parameters:
    ///   - recordId: takes record id
    ///   - onCompletion: do needful on completion
    func updateRecord(with recordId:String,method:String, onCompletion: @escaping (_ success:Bool, _ message:String,_ statusCode: Int,_ response:NSDictionary?) -> Void) {
        let updatedValues = self.getDicRepresentation()
        print("Updated data:\(updatedValues)")
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: updatedValues, options: .prettyPrinted)
            NetworkCall.performCustomRequest(url: AppURLS.URL_GET_VITAL_RECORDS + "/" + recordId, method: method, data: jsonData, completionHandler: { (success, response, statusCode, error) in
                if let err = error {
                    print("Error while updating vital values:\(err.localizedDescription)")
                    onCompletion(false,err.localizedDescription,statusCode ?? 0, response)
                }
                
                if success ,let resp = response , let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String{
                    onCompletion(success,message,statusCode ?? 0,resp)
                }
            })
        }catch let err {
            print("Error while converting updated values to data:\(err.localizedDescription)")
            onCompletion(false,"Unable to", 0,nil)
        }
    }
    

    /// Gets the record values and record dates
    ///
    /// - Parameters:
    ///   - withDate: used to fetch perticular record with recordId
    ///   - onCompletion: do need full after completion
    class func getVitalValues(withDate:String, onCompletion: @escaping (_ success:Bool,_ userVitalInfo:UserVitalInfo?,_ latestVitalinfo:LatestVitalsInfo?) -> Void) {
        NetworkCall.performGet(url: AppURLS.URL_GET_VITAL_RECORDS + withDate) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting template:\(err.localizedDescription)")
                onCompletion(false,nil, nil)
            }
            
            if success {
                if let resp = response ,
                    let data = resp.object(forKey: Keys.KEY_DATA) as? [String: Any] {
                    do  {
                         print("Record values Response:\(resp)")
                        let dicData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        if withDate.isEmpty {
                            onCompletion(true,try decoder.decode(UserVitalInfo.self , from: dicData), nil)
                        }else {
                           onCompletion(true, nil, try decoder.decode(LatestVitalsInfo.self , from: dicData))
                        }
                        
                    }catch let decodeErr {
                        print("Error while decoding values:\(decodeErr.localizedDescription)")
                        onCompletion(false,nil, nil)
                    }
                }
            }else {
                onCompletion(false,nil,nil)
            }
        }
    }
    
    /// Gets the template keys and labels to display in ui
    ///
    /// - Parameter onCompletion: do need full after completion
   class func getVitalTemplateKeys(onCompletion: @escaping (_ success:Bool,_ statusCode:Int,_ response:NSDictionary?,_ vitalTemplate:VitalsTemplate?) -> Void) {
        NetworkCall.performGet(url: AppURLS.URL_VITALS_TEMPLATE) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting template:\(err.localizedDescription)")
                onCompletion(false, statusCode ?? 0, response, nil)
            }
            
            if success {
                if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String: Any] {
                    do  {
                        print("template Response:\(resp)")
                        let dicData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        onCompletion(true,statusCode ?? 0,response, try decoder.decode(VitalsTemplate.self, from: dicData))
                    }catch let decodeErr {
                        print("Error while decoding template:\(decodeErr.localizedDescription)")
                        onCompletion(success,statusCode ?? 0,response, nil)
                    }
                }
            }else {
                onCompletion(success,statusCode ?? 0,response, nil)
            }
        }
    }
}

class UserVitalInfo : Codable{
    
    struct SERILIZATION_KEY {
        static let RECORDS = "records"
        static let HEIGHT = "height"
        static let WEIGHT = "weight"
        static let PULSE_RATE = "pulse_rate"
        static let RESPIRATION_RATE = "respiration_rate"
        static let BLOOD_PRESSURE = "blood_pressure"
        static let BLOOD_PRESSURE_HG = "blood_pressure_hg"
        static let BLOOD_PRESSURE_MM  = "blood_pressure_mm"
        static let TEMPARATURE = "temperature"
        static let SPO2 = "spo2"
        static let BMI = "bmi"
        static let HEAD_CIRCUMFERRENCe = "head_circumference_kids"
        static let URINE_OUPUT = "urine_output"
        static let RANDOM_BLOOD_SUGAR = "random_blood_sugar"
        static let RECORDED_AT = "recorded_at"
        static let ENTRIES = "entries"
        static let LATEST = "latest"
        static let RANGES = "ranges"
        static let MIN = "min"
        static let MAX = "max"
        static let NEONATE = "neonate"
        static let ADULT = "adult"
        static let CHILDREN_13 = "children_13"
        static let INFANT = "infant"
        static let TODLER = "toddler"
        static let SCHOOL_AGE = "school_age"
        static let CHILDREN = "children"
    }
    
    var latestVital: LatestVitalsInfo?
    var recordDates: [VitalRecordDateEntries]?
    
    private enum CodingKeys: String, CodingKey {
        case recordDates = "entries"
        case latestVital = "latest"
    }
}

class LatestVitalsInfo: Codable {
    var createdAt: String?
    var recordedAt: Double?
    var vitals: [VitalTemplateFields]?
    
    private enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case recordedAt = "recorded_at"
        case vitals
    }
}

class VitalRecordDateEntries: Codable {
    var id: String?
    var createdAt: String?
    var recordedAt: Double?
    var isSelected = false
    
    private enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case recordedAt = "recorded_at"
        case id = "_id"
    }
}

enum Units : String , Codable{
    case date
    case cm
    case kg
    case bpm
    case mmHg
    case celcius
    case percent
    case kg_m2
}

enum VitalType : String , Codable{
    case numeric
    case epoh
    case text
}

class VitalTemplateFields: Codable {
    var key: String?
    var label: String?
    var type: String?
    var unit: String?
    var fieldValue: Double?
    var valueObj: BloodPressure?
    var ranges: VitalRanges?
    var fields: [VitalTemplateFields]?
    
    private enum CodingKeys: String, CodingKey {
        case key
        case label
        case type
        case unit
        case fieldValue = "value"
        case valueObj = "value_obj"
        case ranges
        case fields
    }
    
    func dicRepresentation() -> [String:Any]{
        var dic = [String:Any]()
        if let key = self.key { dic["Key"] = key }
        if let label = self.label { dic["label"] = label }
        if let type = self.type { dic["type"] = type }
        if let unit = self.unit { dic["unit"] = unit }
        if let value = self.fieldValue { dic["value"] = value }
        if let value = self.valueObj { dic["value_obj"] = value }
        if let ranges = self.ranges { dic["ranges"] = ranges.dicRepresentation()}
        if let fields = self.fields {  var fieldValue = [[String:Any]]()
            fields.forEach{
                fieldValue.append($0.dicRepresentation())
            }
            dic["fields"] = fieldValue
        }
        return dic
    }
}

class VitalRanges: Codable {
    var neonate: FieldRange?
    var adult: FieldRange?
    var children13: FieldRange?
    var children: FieldRange?
    var infant: FieldRange?
    var toddler: FieldRange?
    var schoolAge: FieldRange?
    
    private enum CodingKeys: String, CodingKey {
        case neonate
        case adult
        case children13 = "children_13"
        case children
        case infant
        case toddler
        case schoolAge = "school_age"
    }
    
    func dicRepresentation() -> [String:Any]{
        var dic = [String:Any]()
        if let neonoteDic = self.neonate?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.NEONATE] = neonoteDic }
        if let adultDic = self.adult?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.ADULT] = adultDic }
        if let children13Dic = self.children13?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.CHILDREN_13] = children13Dic }
        if let children = self.children?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.CHILDREN] = children }
        if let infant = self.infant?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.INFANT] = infant }
        if let toddler = self.toddler?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.TODLER] = toddler }
        if let schoolAge = self.schoolAge?.dicRepresentation() { dic[UserVitalInfo.SERILIZATION_KEY.SCHOOL_AGE] = schoolAge }
        return dic
    }
}

struct FieldRange: Codable{
    var min: Double?
    var max: Double?
    
    private enum CodingKeys: String, CodingKey {
        case min
        case max
    }
    
    func dicRepresentation() -> [String:Any]{
        var dic = [String:Any]()
        if let min = self.min { dic[UserVitalInfo.SERILIZATION_KEY.MIN] = min  }
        if let max = self.min { dic[UserVitalInfo.SERILIZATION_KEY.MAX] = max  }
        return dic
    }
}

struct BloodPressure: Codable{
    var hg: Double?
    var mm: Double?
    
    private enum CodingKeys: String, CodingKey {
        case hg = "blood_pressure_hg"
        case mm = "blood_pressure_mm"
    }
    
    func dicRepresentation() -> [String:Any]{
        var dic = [String:Any]()
        if let hg = self.hg { dic[UserVitalInfo.SERILIZATION_KEY.BLOOD_PRESSURE_HG] = hg  }
        if let mm = self.mm { dic[UserVitalInfo.SERILIZATION_KEY.BLOOD_PRESSURE_MM] = mm  }
        return dic
    }
}

