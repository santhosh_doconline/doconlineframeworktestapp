//
//  MyBillings.swift
//  DocOnline
//
//  Created by Kiran Kumar on 07/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation


///My Billings Model class
class MyBillings {
    
    var id : String!  //
    var entity : String! //
    var invoice_number : Int!
    var customerId : String! //
    var customerName : String!
    var customerEmail : String!
    var customerContact : String!
    var customerBillingAddress : String!
    
    var orderId : String!  //
    var subscriptionId : String! //
    var subscribedPlan : [SubscriptionPlan]! //
    var paymentId : String! //
    var paymentStatus : String! //
    var issuedAt : Int32!  //
    var paidAt : Int32!  //
    var cancelledAt : Int32!  //
    var expiredAt : Int32! //
    var grossAmount : Int16!  //
    var taxAmount : Int!  //
    var amount : Int32! //
    var amountPaid : Int32!  //
    var amountDue : Int32! //
    var currency : String! //
    var notes : String!
    var comment : String!
    var bilingStartedAt : Double!
    var billingEndAt : Double!
    var type: String!
    var createdAt : Int32!
    
    var current_page : Int!
    var from : Int!
    var path : String!
    var last_page : Int!
    var next_page_url : String!
    var prev_page_url : String!
    var to : Int!
    var appliedCouponDiscount = ""
    
    init(id:String,entity:String,customerId:String,orderId:String,subscriptionId:String,subscriptionPlan:[SubscriptionPlan],paymentId:String,paymentStatus:String,issuedAt:Int32,paidAt:Int32,cancelledAt:Int32,expiredAt:Int32,grossAmount:Int16,taxAmount:Int,amount:Int32,amountPaid:Int32,amountDue:Int32,currency:String,billingStart:Double,billingEnd:Double,customerName:String,customerEmail:String) {
        self.id = id
        self.entity = entity
        self.customerId = customerId
        self.orderId = orderId
        self.subscriptionId = subscriptionId
        self.subscribedPlan = subscriptionPlan
        self.paymentId = paymentId
        self.paymentStatus = paymentStatus
        self.issuedAt = issuedAt
        self.paidAt = paidAt
        self.cancelledAt = cancelledAt
        self.expiredAt = expiredAt
        self.grossAmount = grossAmount
        self.taxAmount = taxAmount
        self.amount = amount
        self.amountPaid = amountPaid
        self.amountDue = amountDue
        self.currency = currency
        self.bilingStartedAt = billingStart
        self.billingEndAt = billingEnd
        self.customerName = customerName
        self.customerEmail = customerEmail
    }
    
    
}
