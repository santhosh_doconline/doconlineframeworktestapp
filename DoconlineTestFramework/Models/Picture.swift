//
//  Picture.swift
//  DocOnline
//
//  Created by Kiran Kumar on 22/05/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import Foundation

enum FileType : Int {
    case image = 1
    case file = 2
}

class Picture {
    
    var pic:UIImage?
    var picCaption: String?
    var type: FileType?
    var isNewUpload = true
    
   
    init(pic:UIImage,picCaption:String,type:FileType) {
        self.pic = pic
        self.picCaption = picCaption
        self.type = type
    }
    
    ///used for attachments new
    var imageUrl: String?
    var fileURl: String?
    var attachmentId:Int?
    var appointmentId:Int?
    
     ///used for new or unuploaded attachments new
    init(fileURL:String,picCaption:String,type:FileType) {
        self.fileURl = fileURL
        self.picCaption = picCaption
        self.type = type
    }
    
    
     ///used for uploaded attachments new
    init(imageUrl:String,picCaption:String,attachmentId:Int,appointmentId:Int,type:FileType) {
        self.imageUrl = imageUrl
        self.picCaption = picCaption
        self.attachmentId = attachmentId
        self.appointmentId = appointmentId
        self.type = type
    }
    
    ///used for uploaded attachments new
    init(fileURl:String,picCaption:String,attachmentId:Int,appointmentId:Int,type:FileType) {
        self.fileURl = fileURl
        self.picCaption = picCaption
        self.attachmentId = attachmentId
        self.appointmentId = appointmentId
        self.type = type
    }
}


