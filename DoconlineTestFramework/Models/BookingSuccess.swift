//
//  BookingSuccess.swift
//  DocOnline
//
//  Created by dev-3 on 12/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation


/** 
   This is modal class for Booking appointment success page
 */

class BookingSuccess {
    
    var booked_for : Int!
    var call_type : Int!
    var id : Int!
    var notes : String!
    var scheduled_at : String!
    var user_id : Int!
    var patientName: String!
    
    var doctorName: String!
    var doctorAvatar: String!
    var doctorSpecialisation: String!
    var doctorRating:Double!
    var status: Int!
    
    var docUserId:Int!
    var docUserFullName:String!
    
    init() {
        
    }
    
    init(id:Int,userid:Int,notes:String,bookedfor:Int,calltype:Int,scheduled:String,patientName:String) {
        self.booked_for = bookedfor
        self.call_type = calltype
        self.id = id
        self.notes = notes
        self.scheduled_at = scheduled
        self.user_id = userid
        self.patientName = patientName
    }
}
