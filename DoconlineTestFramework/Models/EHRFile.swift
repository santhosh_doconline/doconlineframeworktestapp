//
//  EHRFiles.swift
//  DocOnline
//
//  Created by Kiran Kumar on 24/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import Foundation

class EHRFile: Codable{
    var currentPage: Int?
    var nextPage: Int?
    var perPage: Int?
    var prevPage: Int?
    var total: Int?
    var batches: [Batch]?
    
    private enum CodingKeys: String, CodingKey {
        case currentPage
        case nextPage
        case perPage
        case prevPage
        case total
        case batches = "data"
    }
    
    struct Batch: Codable {
        var _id: String?
        var categoryId: Int?
        var createdAt: String?
        var recordedAt: String?
        var userId: Int?
        var files: [File]?
        
        private enum CodingKeys: String, CodingKey {
            case _id
            case categoryId = "category_id"
            case createdAt = "created_at"
            case recordedAt = "recorded_at"
            case userId = "user_id"
            case files
        }
    }
    
    struct File: Codable {
        var index: Int?
        var title: String?
        var url: String?
        
        private enum CodingKeys: String, CodingKey {
            case index
            case title
            case url
        }
    }
    
    class func getEHRFiles(page:String,isSearchEnabled: Bool ,onCompletion: @escaping (_ success:Bool,_ fileData: EHRFile?,_ response: NSDictionary?,_ statusCode:Int) -> Void) {
        print("Files URL:\(AppURLS.URL_EHR_FILE_UPLOAD + page )")
        NetworkCall.performGet(url: AppURLS.URL_EHR_FILE_UPLOAD + page ) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                onCompletion(false,nil,nil,statusCode ?? 0)
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    let ehrFile = try decoder.decode(EHRFile.self, from: jsonData)
                    onCompletion(true,ehrFile,resp,statusCode ?? 0)
                }catch let err {
                    print("Error while converting the ehrfile data:\(err)")
                     onCompletion(false,nil,resp,statusCode ?? 0)
                }
            }else {
                 onCompletion(false,nil, nil,statusCode ?? 0)
            }
        }
    }
}
