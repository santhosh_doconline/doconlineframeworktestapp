//
//  Appointments.swift
//  DocOnline
//
//  Created by dev-3 on 13/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation


/** 
  This is modal class for Appointments list
 */

class Appointments {
    
    var id : Int!
    var current_page : Int!
    var scheduled_at : String!
    var call_type : Int!
    var booked_for:Int!
    var notes : String!
    var status : Int!
    var from : Int!
    var last_page : Int!
    var next_page_url : String!
    var to : Int!
    var total : Int!
    var started_at : String!
    var finished_at:String!
    var attachments = [String]()
    
    var doctorName: String!
    var doctorAvatar: String!
    var doctorSpecialisation: String!
    var patientName: String!
    var patientGender: String!
    var patientAvatar: String!
    var age: String!
    
    init() {
        
    }
    
    ///Before new UI
    init(id:Int,current_page:Int,scheduled_at: String,calltype:Int,booked_for:Int,notes:String,from:Int,last_page: Int,nextpageurl:String,to:Int,total:Int,status:Int) {
        self.id = id
        self.current_page = current_page
        self.scheduled_at = scheduled_at
        self.call_type = calltype
        self.booked_for = booked_for
        self.notes = notes
        self.from = from
        self.last_page = last_page
        self.next_page_url = nextpageurl
        self.to = to
        self.total = total
        self.status = status
    }
    
    ///After New UI
    init(id:Int,current_page:Int,scheduled_at: String,calltype:Int,booked_for:Int,notes:String,from:Int,last_page: Int,nextpageurl:String,to:Int,total:Int,status:Int,doctorName:String,doctorAvatar:String,doctorSpecialisation:String,patientName:String,patientGender:String,patientAvatar:String,age:String) {
        self.id = id
        self.current_page = current_page
        self.scheduled_at = scheduled_at
        self.call_type = calltype
        self.booked_for = booked_for
        self.notes = notes
        self.from = from
        self.last_page = last_page
        self.next_page_url = nextpageurl
        self.to = to
        self.total = total
        self.status = status
        self.doctorName = doctorName
        self.doctorAvatar = doctorAvatar
        self.doctorSpecialisation = doctorSpecialisation
        self.patientName = patientName
        self.patientGender = patientGender
        self.patientAvatar = patientAvatar
        self.age = age
    }
}
