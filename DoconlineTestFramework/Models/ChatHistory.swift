//
//  ChatHistory.swift
//  DocOnline
//
//  Created by Kiran Kumar on 23/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

/**
 This is modal class for Chat history list
 */
class ChatHistory: Codable {
    
    //list information
    var current_page : Int?
    var first_page_url: String?
    var last_page_url: String?
    var from : Int?
    var last_page : Int?
    var next_page_url : String?
    var path : String?
    var prev_page_url : String?
    var to : Int?
    var total : Int?
    var perPage : Int?
    var data: [ChatInfo]?
    
    //chat list data
    var id : Int?
    var doctor_name:String?
    var last_message : String?
    var created_at : String?
    var updated_at : String?
    var status : Int?
    
    
    private enum CodingKeys: String,CodingKey {
        case current_page
        case from
        case last_page
        case next_page_url
        case last_page_url
        case first_page_url
        case path
        case prev_page_url
        case to
        case total
        case perPage = "per_page"
        case data
    }
    
    init() {
        
    }
    
    //page info
    init(currentPage : Int,from:Int,last_page:Int,nextPageUrl:String,path:String,perPage:Int,prevPageUrl: String,to:Int,total:Int) {
        
        self.current_page = currentPage
        self.from = from
        self.last_page = last_page
        self.next_page_url = nextPageUrl
        self.path = path
        self.prev_page_url = prevPageUrl
        self.perPage = perPage
        self.to = to
        self.total = total
    }
    
   //Array info
    init(id:Int,doctorName:String,lastMessage:String,createdAt:String,updateAt:String,status:Int) {
        self.id = id
        self.doctor_name = doctorName
        self.last_message = lastMessage
        self.created_at = createdAt
        self.updated_at = updateAt
        self.status = status
    }
    
}



struct ChatInfo: Codable {
    var id: Int?
    var user: User?
    var doctor: Doctor?
    var latestMessage: String?
    var createdAt: String?
    var updatedAt: String?
    var status: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case user
        case doctor
        case latestMessage
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case status
    }
}
