//
//  UserSession.swift
//  DocOnline
//
//  Created by Kiran Kumar on 31/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation
//import Firebase


/**
 This is a shared instance class for loging out user
 */
class UserSession {
    
    ///Shared instance for class
    static let sharedInstace = UserSession()
    let defaults = UserDefaults.standard
    
    /**
       Method returns previous user access token token type 
       
        ##Note##
        *if user not logged out successfully
       - Returns: token type and access token
     */
    func getPreviousUserAccessToken() -> String {
        if defaults.object(forKey: UserDefaltsKeys.PREVIOUS_ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.PREVIOUS_TOKEN_TYPE) != nil{
            let access_token = defaults.object(forKey:  UserDefaltsKeys.PREVIOUS_ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.PREVIOUS_TOKEN_TYPE) as! String
            return "\(token_type) \(access_token)"
        }else {
            print("No stored previous access token")
            return ""
        }
    }
    
    /**
     Method used to store logging out user access token and token type before logout tapped
     */
    func makePresentAccessTokenToPrevious() {
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil{
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            defaults.set(access_token, forKey: UserDefaltsKeys.PREVIOUS_ACCESS_TOKEN)
            defaults.set(token_type, forKey: UserDefaltsKeys.PREVIOUS_TOKEN_TYPE)
            
        }else {
            print("No stored access token")
        }
    }
    
    /**
     Method performs user logout operation
      - Parameter accessToken: takes the access token string of user
     */
    func logourUser(accessToken:String,apVoipToken:String) {

        if !NetworkUtilities.isConnectedToNetwork()
        {
           makePresentAccessTokenToPrevious()
        }
        
        var token = ""
       
        if let fcmToken = AppConfig.shared?.fcmToken {
            token = fcmToken
        }
        
    
        if token.isEmpty && App.apvoip_token.isEmpty {
            print("Tokens are eempty")
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let bookURL = AppURLS.URL_Logout
        let url = URL(string: bookURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(accessToken, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        let postData = "\(Keys.KEY_DEVICE_TOKEN)=\(token)&\(Keys.KEY_APVOIP_TOKEN)=\(apVoipToken)"
        print("Logout data==>\(postData)")
        print("LogoutURL==>\(bookURL)")
        request.httpBody = postData.data(using: String.Encoding.utf8)
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
               self.makePresentAccessTokenToPrevious()
               print("Error while logging out::\(error.localizedDescription)")
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                
                if httpResponse.statusCode == 204 {
                    print("Logout success user session is expired")
                    
                    self.defaults.set(true, forKey: UserDefaltsKeys.DID_USER_LOGED_OUT)
                    self.defaults.removeObject(forKey: UserDefaltsKeys.PREVIOUS_ACCESS_TOKEN)
                    self.defaults.removeObject(forKey: UserDefaltsKeys.PREVIOUS_TOKEN_TYPE)
                    self.defaults.removeObject(forKey: UserDefaltsKeys.PREVIOUS_APVOIP_TOKEN)
     
                }else {
                    self.defaults.set(true, forKey: UserDefaltsKeys.DID_USER_LOGED_OUT)
                }
            }
            
        }).resume()
    }

    
    
}
