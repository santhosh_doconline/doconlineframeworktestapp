//
//  MyOrders.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

///Orders model class
class MyOrders {
    var id : Int?
    var appointment_id : Int?
    var user_id :Int?
    var medplus_id  : String?
    var order_id : String?
    var order_amount : String?
    var pincode : String?
    var address1 : String?
    var address2 : String?
    var status : Int?
    var deliverd_on : String?
    var created_at : String?
    var updated_at : String?
    var items : [OrderItems]?
    
    var from : Int!
    var last_page : Int!
    var next_page_url : String!
    var to : Int!
    var total : Int!
    
    init() {
        
    }
    
    init(id:Int,appointment_id : Int,user_id :Int,medplus_id  : String,order_id : String,order_amount : String,pincode : String,address1 : String,address2 : String,status : Int,deliverd_on : String,created_at : String,updated_at : String,from:Int,last_page: Int,nextpageurl:String,to:Int,total:Int) {
        self.id = id
        self.appointment_id = appointment_id
        self.user_id = user_id
        self.medplus_id = medplus_id
        self.order_id = order_id
        self.order_amount = order_amount
        self.pincode = pincode
        self.address1 = address1
        self.address2 = address2
        self.status = status
        self.deliverd_on = deliverd_on
        self.created_at = created_at
        self.updated_at = updated_at
        self.from = from
        self.last_page = last_page
        self.next_page_url = nextpageurl
        self.to = to
        self.total = total
    }
}

class OrderItems {
    var item_id : Int?
    var prescription_order_id : Int?
    var product_id : String?
    var name : String?
    var qty : String?
    var price : String?
    var manufaturer : String?
    var packsize : Int?
    var discount : Int?
    var productForm : String?
    var mrp : Double?
    var available_qty : String?
    
    init(itemid: Int,pre_orderid:Int,productid:String,name:String,qty:String,price:String,manufacture:String,packsize:Int,discount:Int,producform:String,mrp:Double,availbleqty:String) {
        self.item_id = itemid
        self.prescription_order_id = pre_orderid
        self.product_id = productid
        self.name = name
        self.qty = qty
        self.price = price
        self.manufaturer = manufacture
        self.packsize = packsize
        self.discount = discount
        self.productForm = producform
        self.mrp = mrp
        self.available_qty = availbleqty
    }
}






