//
//  Languages.swift
//  DocOnline
//
//  Created by Kiran Kumar on 02/11/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

///Languages Model class
class Languages   {
    var lang_id : Int?
    var lang_name : String?
    var readOnly : Bool?
    var isSelected : Bool?
    var preferedCount : Int?
    
    init(id:Int,name:String,readOnly: Bool,isSelected: Bool ,preferred:Int) {
        self.lang_id = id
        self.lang_name = name
        self.readOnly = readOnly
        self.isSelected = isSelected
        self.preferedCount = preferred
    }
    
    /**
     Method performs request to server for family members
     */
    static func getLanguages(completionHandler: @escaping (_ success:Bool) -> Void) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let slotURL = AppURLS.URL_Languages
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                completionHandler(false)
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if response is json do the following
                do
                {
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        print("Languages response:\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        if code == 200 {
                            if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary]{
                                App.totalLanguages.removeAll()
                                App.selectedLanguages.removeAll()
                                for lang in data {
                                    if let id = lang.object(forKey:Keys.KEY_ID) as? Int {
                                        if let englishName = lang.object(forKey:Keys.KEY_ENGLISH_NAME) as? String {
                                            if let readOnly = lang.object(forKey:Keys.KEY_READ_ONLY) as? Bool {
                                                let language = Languages(id: id  , name: englishName, readOnly: readOnly, isSelected: readOnly, preferred: 0)
                                                
                                                App.totalLanguages.append(language)
                                                
                                            }
                                        }
                                    }
                                }
                                print("Step one finished")
                            }
                            completionHandler(true)
                        }else {
                            completionHandler(false)
                        }
                    }else {
                        completionHandler(false)
                    }
                }catch let error{
                     completionHandler(false)
                    print("Received not a well-formatted JSON languages: \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    class func changeValueAtIndex(atIndes:Int,language:Languages) {
        App.totalLanguages[atIndes] = language
    }
    
    class func test(_: ((Int, Int) -> ())) {
        
    }
    
    class func changePreferedStatus(completionHandler: @escaping (_ success:Bool) -> Void) {
        Languages.getLanguages { (success) in
            if success {
                var countof = 0
                for (lngIndex,lang) in App.totalLanguages.enumerated() {
                    var isPreferedLang = false
                     for (index,preferedID) in App.lang_preferences.enumerated() {
                        if preferedID == Int(lang.lang_id!) {
                            isPreferedLang = true
                            countof = index + 1
                            break
                        }else {
                            isPreferedLang = false
                        }
                    }
                    
                    if isPreferedLang {
                        let language = Languages(id: lang.lang_id!, name: lang.lang_name!, readOnly: lang.readOnly!, isSelected: true, preferred: countof)
                        App.selectedLanguages.append(language)
                        App.totalLanguages[lngIndex] = language
                    }else {
                        let language = Languages(id: lang.lang_id!, name: lang.lang_name!, readOnly: lang.readOnly!, isSelected: false, preferred: 0)
                        App.totalLanguages[lngIndex] = language
                    }
                }
                
             
                
//                for (index,preferedID) in App.lang_preferences.enumerated() {
//                    for (lngIndex,lang) in App.totalLanguages.enumerated() {
//                        if preferedID == Int(lang.lang_id!) {
//
//                            lang.isSelected! = true
//                            lang.preferedCount! = index + 1
//                            App.selectedLanguages.append(lang)
//                             let language = Languages(id: lang.lang_id!  , name: lang.lang_name!, readOnly: lang.readOnly!, isSelected: true, preferred: index + 1)
//                            changeValueAtIndex(atIndes: lngIndex, language: language)
//                            print("Lang id:\(lang.lang_id!) is prefered languages:\(lang.lang_name!) prefered count:\(lang.preferedCount!)")
//                        }else {
//                            lang.isSelected! = false
//                            lang.preferedCount! = 0
//                             let language = Languages(id: lang.lang_id!  , name: lang.lang_name!, readOnly: lang.readOnly!, isSelected: false, preferred: 0)
//                             changeValueAtIndex(atIndes: lngIndex, language: language)
//                            print("Lang id:\(lang.lang_id!) is not a prefered languages:\(lang.lang_name!) prefered count:\(lang.preferedCount!)")
//                        }
//                    }
//                }
//
                  print("Step two finished")
                completionHandler(true)
            }else {
                 completionHandler(false)
                print("########## Languages not fetched $####**")
            }
        }
    }
    
}
