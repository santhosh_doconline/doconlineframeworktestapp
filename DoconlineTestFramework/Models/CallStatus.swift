//
//  CallStatus.swift
//  DocOnline
//
//  Created by Kiran Kumar on 02/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

 class CallStatus {
    
    static var sharedInstance = CallStatus()
    
    /**
     Performs network call notifi server that call recieved
     */
     func perfromCallRecievedRequest(callStatus:Int) {
        let date = "\(Date())"
        let timeStamp = date.components(separatedBy: " ")
        let urlString = AppURLS.URL_Call_recieved + "\(App.call_appointment_id)/status"
        NSLog("callstatus-url \(urlString)")
        let postData = "\(Keys.KEY_DOCTOR_ID)=\(callDoctorModal.id!)&\(Keys.KEY_STATUS)=\(callStatus)&\(Keys.KEY_TIME_STAMP)=\(timeStamp[0]) \(timeStamp[1]))"
        print("call recieved Data ==>\(postData)")
        let url = URL(string: urlString)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
       // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = postData.data(using: String.Encoding.utf8)
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    print("Error==> : \(error.localizedDescription)")
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("httpresponse:\(response as! HTTPURLResponse)")
                
                if httpResponse.statusCode == 204
                {
                    if callStatus == CallingStatus.CALL_STATUS_PATIENT_ACKNOWLEDGED
                    {
                        print("123Status of call acknoledged sent to server successfully")
                    }
                    else if callStatus == CallingStatus.CALL_STATUS_PATIENT_ACCEPTED
                    {
                        print("456Status of call accepted sent to server successfully")
                    }
                    else if callStatus == CallingStatus.CALL_STATUS_PATIENT_REJECTED
                    {
                        print("789Status of call rejected sent to server successfully")
                    }
                }
            
//                do
//                {
//                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
//                    
//                    print("Call recieved response:\(resultJSON)")
//                    
//                }catch let error{
//                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
//                }
            }
        }).resume()
    }
    
}
