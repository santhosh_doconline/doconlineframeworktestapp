//
//  ChatSession.swift
//  DocOnline
//
//  Created by Kiran Kumar on 21/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

/**
 This is modal class for family members
 */
class ChatSession {
    
    var thread_id : Int?
    var doctor_id : Int?
    var created_at : String?
    var user_avatar : String?
    var user_name : String?
    var user_id : Int?
    
    
    init(threadid:Int,doctor_id:Int,created_at:String,user_avatar:String,username:String,userid:Int) {
        self.thread_id = threadid
        self.doctor_id = doctor_id
        self.created_at = created_at
        self.user_avatar = user_avatar
        self.user_name = username
        self.user_id = userid
    }
    
}
