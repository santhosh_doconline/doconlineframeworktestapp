//
//  Doctor.swift
//  DocOnline
//
//  Created by dev-3 on 13/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation



/** 
  This is modal class for Doctor list
 */

public class Doctor : Codable {
    
    public var id : Int!
    public var first_name: String!
    public var prefix: String!
    public var middle_name : String!
    public var last_name : String!
    public var practitionerNumber:Int! = nil
    public var avatar_url : String!
    public var appointment_id : String!
    public var full_name: String!
    public var specialisation: String! = ""
    public var ratings:Double!
    
    public var email: String?
    public var userName: String?
    public var mobileNo: String?
    public var uidNo: String?
    public var specializationId: Int?
    public var internalRating: Int?
    public var isVerified: Int?
    public var isActive: Int?
    public var activationToken: String?
    public var createdAt: String?
    public var updatedAt: String?
    var doctorSpecialization: Specialization?
    public var doctorPractitionerNumber: String?
    public var mciCode: String?
    public var qualification: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case first_name
        case middle_name
        case last_name
        case prefix
        case email
        case full_name
        case avatar_url
        case doctorSpecialization = "specialization"
        case doctorPractitionerNumber = "practitioner_number"
        case appointment_id
        case userName = "username"
        case mobileNo = "mobile_no"
        case uidNo = "uid_no"
        case specializationId = "specialization_id"
        case internalRating = "internal_rating"
        case isVerified = "is_verified"
        case isActive = "is_active"
        case activationToken = "activation_token"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case ratings
        case mciCode = "mci_code"
        case qualification
    }
    
    
    
    public init() {
        
    }
    
    ///Before New UI
    public init(prefix:String,id:Int,first_name:String,middle_name:String,last_name:String,practitioner_number:Int,avatar_url:String) {
        self.id = id
        self.first_name = first_name
        self.avatar_url = avatar_url
        self.middle_name = middle_name
        self.practitionerNumber = practitioner_number
        self.last_name = last_name
        self.prefix = prefix
    }
    
    ///After new UI
    public init(prefix:String,id:Int,first_name:String,middle_name:String,last_name:String,practitioner_number:Int,avatar_url:String,fullname:String,specialization:String,rating:Double) {
        self.id = id
        self.first_name = first_name
        self.avatar_url = avatar_url
        self.middle_name = middle_name
        self.practitionerNumber = practitioner_number
        self.last_name = last_name
        self.prefix = prefix
        self.full_name = fullname
        self.specialisation = specialization
        self.ratings = rating
    }
    
    
    struct Specialization: Codable {
        public var id: Int?
        public var name: String?
        public var description: String?
        
        private enum CodingKeys: String,CodingKey {
            case id
            case name
            case description
        }
    }
    
}
