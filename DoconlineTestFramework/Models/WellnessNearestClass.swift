//
//  WellnessNearestClass.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 25/04/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit

class WellnessNearestClass: Codable {
    var data: [WellnessInnerData]
    func sortByDistance() {
        data.sort{ Float($0.dist) < Float($1.dist) }
    }
}

class WellnessInnerData: Codable{
    var len: String
    var a2: String
    var c: String
    var catn: String
    var dist: Float
    var ft: String
    var tt: String
    var id: String
    var WorkoutID: Int
    var cn: String
}

class WellnessClassDetailsWrapper : Codable{
    var data: WellnessClassDetails
}

class WellnessClassDetails: Codable{
    var OutletName: String
    var WorkoutID: Int
    var WorkoutName: String
    var FromTime: String
    var ToTime: String
    var Description: String?
    var GoogleMapsCoordiates: String
    var Duration: String
    var WorkoutDetailsID: Int
    var StreetAddress: String
    var StreetAddress2: String
    var City: String
    var FitmeSeat: Int
}


class WellnessWorkoutList: Codable{
    var data: WellnessInfo
    
}

class WellnessInfo: Codable{
//    var page_no: String
//    var records_per_page: String
//    var total_count: Int
    var records: [WellnessTrackerInner]
}

class WellnessTrackerInner: Codable{
    var outlet_name: String?
    var workout_name: String?
    var from_time: String?
    var to_time: String?
    var address: String?
    var city: String?
    var lat_long: String?
    var booked_for_date: String?
    var expected_time: String?
    var is_cancelled: Int?
    var cancel_reason: String?
    var workout_id: String?
    var booking_id: String?
    var distance: String?
    var is_attended: Int?
    var description: String
}

class WellnessCancelClass: Codable{
    var message: String
}

class WellnessBookClass: Codable{
    var message: String
}



class WellnessAttendance: Codable{
    var code: Int
    var status: String
    var message: String
}


class WellnessStudioList: Codable{
    var data:[StudioInnerData]
    
    func sortByDistance() {
        data.sort{ Float($0.Distance) < Float($1.Distance) }
    }
}

class StudioInnerData: Codable{
    var OutletId: Int
    var OutletName: String
    var PartnerName: String
    var Longitude: String
    var Latitude: String
    var Distance: Float
    var StreetAddress1: String
    var StreetAddress2: String
    var CityName: String
    var WorkoutCount: Int
    var workouts: String
}

struct WorkoutList{
    var data : [String:[WorkoutInnerList]]
}

struct WorkoutInnerList{
    var cn : String
    var ft : String
    var tt : String
    var cb : String
}


