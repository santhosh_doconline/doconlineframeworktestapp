//
//  ShippinAddress.swift
//  DocOnline
//
//  Created by Kiran Kumar on 26/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

///Shipping address model class
class ShippinAddress : Codable {
    
    var addressLine1 : String?
    var addressLine2 : String?
    var city : String?
    var state : String?
    var country : String?
    var pincode : String?
    var phone : String?
    
    init() {
    }
    
    init(address1:String,address2:String,city:String,state:String,country:String,pincode:String,phone:String) {
        self.addressLine1 = address1
        self.addressLine2 = address2
        self.city = city
        self.state = state
        self.country = country
        self.pincode = pincode
        self.phone = phone
    }
    
}
