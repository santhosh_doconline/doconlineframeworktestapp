//
//  DashboardTiles.swift
//  DocOnline
//
//  Created by Kiran Kumar on 01/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import Foundation


class DashboardTiles {
    
    var tileTitle: String!
    var tileContent:String!
    var tileIcon: UIImage!
    
    var appointmentTime: String!
    var doctorName: String!
    var doctorSpecialization: String!
    
    init(tileTitle:String,tileContent:String,tileIcon:UIImage) {
        self.tileTitle = tileTitle
        self.tileContent = tileContent
        self.tileIcon = tileIcon
    }
    
    ///For My appointments
    init(tileTitle:String,tileContent:String,tileIcon:UIImage,appTime:String,doctorName:String,specialization:String) {
        self.tileTitle = tileTitle
        self.tileContent = tileContent
        self.tileIcon = tileIcon
        self.appointmentTime = appTime
        self.doctorName = doctorName
        self.doctorSpecialization = specialization
    }
    
    /**
     function returns time in am/pm
     
     - Parameter date: takes date string
     - Returns: UTC time string ex: 09:20:00
     */
    static func getTimeInAMPM(date:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat =  "d MMM EE hh:mm a"
        return dt == nil ? "" : dateFormator.string(from: dt!)
    }
    
    ///Method returns the dashboard items list with content and title array used to update dynamic content update in tiles
    static func addDashboardContet() -> [DashboardTiles]{
        var items = [DashboardTiles]()
        
        
        
        
        let appointmentContent = "View all your appointment details in one place!"   ///
        var appointmentTime = ""
        var doctorName = ""
        var doctorSpecialisation  = ""
        
        if App.tempAppointments.count != 0 && App.tempAppointments.count > 0{
            let firstApp = App.tempAppointments[0]
            if let date = firstApp.scheduled_at {
                appointmentTime = getTimeInAMPM(date: date)
                print("Converted")
            }
            
            if let docName = firstApp.doctorName {
                doctorName = docName
            }
            
            if let docSpeci = firstApp.doctorSpecialisation {  //
                doctorSpecialisation = docSpeci
            }
            
        }
//        var iconsHolder = [UIImage]()
//        switch Theme.iconSet {
//        case .defaultTheme:
//            iconsHolder = [UIImage(named:"book_consult")!, UIImage(named:"my_appointment")!, UIImage(named:"order_med")!, UIImage(named:"ask_q")!, UIImage(named:"book_diagno")!, UIImage(named:"MyDocs")!, UIImage(named:"mobile_bell_icon")!, UIImage(named:"add_ons")!, UIImage(named:"wellness_v1")!, UIImage(named:"HRA")!]
//        case .betterplace:
//            iconsHolder = [UIImage(named:"b_bookconsultation")!, UIImage(named:"b_myappintments")!, UIImage(named:"b_ordermedicine")!, UIImage(named:"b_chat")!, UIImage(named:"b_bookdiagnostics")!, UIImage(named:"b_ehr")!, UIImage(named:"b_medicalreminders")!, UIImage(named:"b_addons")!, UIImage(named:"wellness_v1")!, UIImage(named:"HRA")!]
//        default:
//            print("")
//        }
        
        
        
        let iconsHolder = [UIImage(named:"book_consult")!, UIImage(named:"my_appointment")!, UIImage(named:"order_med")!, UIImage(named:"ask_q")!, UIImage(named:"book_diagno")!, UIImage(named:"MyDocs")!, UIImage(named:"mobile_bell_icon")!, UIImage(named:"add_ons")!, UIImage(named:"wellness_v1")!, UIImage(named:"HRA")!].map{$0.tintedWithLinearGradientColors(colorsArr: [Theme.navigationGradientColor![0]!.cgColor, Theme.navigationGradientColor![1]!.cgColor])}
        
        
        let bookConsultation = DashboardTiles(tileTitle: "Book a Consultation", tileContent: "Book a Consultation as per your convenience!", tileIcon: iconsHolder[0])
        
        let myPpointments = DashboardTiles(tileTitle: "My Appointments", tileContent: appointmentContent, tileIcon: iconsHolder[1], appTime: appointmentTime, doctorName: doctorName, specialization: doctorSpecialisation)
        
        
        let orderMedicines = DashboardTiles(tileTitle: "Order Medicines", tileContent: "How about the pharmacy coming to your doorstep?\nMin Order Rs.200/-", tileIcon: iconsHolder[2])
        
        
        let askAQuestion = DashboardTiles(tileTitle: "Chat with Doctor", tileContent: "Put your questions to our panel of experts!", tileIcon: iconsHolder[3])
        
          //"Need diagnosis at your convenience?"
        let bookDiagnostics = DashboardTiles(tileTitle: "Book Diagnostics", tileContent: "Book Diagnostic  tests/packages at your doorstep.", tileIcon: iconsHolder[4])
        
        
        let addOns = DashboardTiles(tileTitle: "Add Ons", tileContent: "Are you monitoring your health regularly?", tileIcon: iconsHolder[7])
        
        
        let myDocs = DashboardTiles(tileTitle: "Medical Records(EHR)", tileContent: "You can save all your Medical Records here.", tileIcon: iconsHolder[5])
        
        
        let reminder = DashboardTiles(tileTitle: "Medicine Reminders", tileContent: "You can set reminders for your medicines here.", tileIcon: iconsHolder[6])
        
        
        let wellness = DashboardTiles(tileTitle: "Wellness", tileContent: "Book activities from the top fitness studios around you.", tileIcon: iconsHolder[8])
        
        
        let hra = DashboardTiles(tileTitle: "HRA", tileContent: "You can calculate Health Risk Assesment here.", tileIcon: iconsHolder[9])
       /*
        if App.user_type.isEmpty || App.user_type == "b2c" || (!App.fitmeinType && !App.hraType){
            items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,addOns]
        }else if App.user_type == "b2b" && App.fitmeinType && App.hraType{
            items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,wellness,hra,addOns]
        }else if App.user_type == "b2b" && App.fitmeinType && !App.hraType{
            items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,wellness,addOns]
        }
        else if App.user_type == "b2b" && !App.fitmeinType && App.hraType{
            items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,hra,addOns]
        }*/
//        items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,addOns]
 
        
        
        if App.user_type.isEmpty || App.user_type == "b2c"{
            items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,addOns]
        }else if App.user_type == "b2b" || App.user_type == "b2bpaid"{
            items = [bookConsultation,myPpointments,orderMedicines,askAQuestion,bookDiagnostics,myDocs,reminder,wellness,hra,addOns]
        }
        
        return items
    }
    
}

