//
//  FamilyMembers.swift
//  DocOnline
//
//  Created by Kiran Kumar on 11/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

/**
 This is modal class for family members
 */

class FamilyMembers {
    
    var id : Int?
    var email : String?
    var mobile_no : String?
    var mrn_no : String?
    var isVerified: Bool?
    var mobile_verified : Bool?
    var isActive: Bool?
    var avatar_url : String?
    var fb_id : String?
    var g_id : String?
    var full_name : String?
    var first_name : String?
    var prefix : String?
    var last_name : String?
    var middle_name : String?
    var dob : String?
    var gender : String?
    var user_id : Int?
    var family_id : Int?
    var created_at : String?
    var statusDisplay: String?
    var userDetails: User?
    
    init() {
        
    }
    
    ///Used for listing
    init(id:Int,email:String,avatar_url:String,full_name:String,first_name:String,dob:String,gender:String,prefix:String) {
        self.id = id
        self.email = email
        self.avatar_url = avatar_url
        self.full_name = full_name
        self.first_name = first_name
        self.dob = dob
        self.gender = gender
        self.prefix = prefix
    }
   
    ///used for data
    init(id:Int,prefix:String,email:String,avatar_url:String,mrn_no:String,mobile_no:String,mobile_verified:Bool,full_name:String,first_name:String,dob:String,gender:String,familyid:Int,userid:Int,createdat:String,isVerified:Bool,isActive:Bool,statusDisplay:String) {
        self.id = id
        self.prefix = prefix
        self.email = email
        self.mobile_no = mobile_no
        self.mrn_no = mrn_no
        self.mobile_verified = mobile_verified
        self.avatar_url = avatar_url
        self.full_name = full_name
        self.first_name = first_name
        self.dob = dob
        self.gender = gender
        self.user_id = userid
        self.family_id = familyid
        self.created_at = createdat
        self.isActive = isActive
        self.isVerified = isVerified
        self.statusDisplay = statusDisplay
    }
    
}
