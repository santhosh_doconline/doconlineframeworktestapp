//
//  UserFamily.swift
//  DocOnline
//
//  Created by Kiran Kumar on 10/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import Foundation
import UIKit


class AppSettings: Codable{
    var hotline: Hotline?
    var userFamilyLimitInfo: FamilyLimitInfo?
    var documentCategories: [DocumentCategories]?
    var ehrDocumentConsent: Int?
    var maxUploadFileSize: Int?
    var isPasswordBlank: Bool?
    var vitalImageInfoUrl: String?
    //var bookingConsent: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case hotline
        case userFamilyLimitInfo = "family_member"
        case documentCategories = "document_categories"
        case ehrDocumentConsent = "ehr_document_consent"
        case maxUploadFileSize = "max_file_size"
        case isPasswordBlank = "password_blank"
        case vitalImageInfoUrl = "vital_info_url"
    }
    
    class DocumentCategories: Codable{
        var id: Int?
        var title: String?
        var description: String?
        var iconUrl: String?
        var createdAt: String?
        var updatedAt: String?
        var deletedAt: String?
        
        var imageView: UIImageView! =  nil
        
        private enum CodingKeys: String, CodingKey {
            case id
            case title
            case description
            case iconUrl = "icon_url"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case deletedAt = "deleted_at"
        }
        
        class func getSettings(at index:Int) -> DocumentCategories?
        {
            if App.appSettings?.documentCategories?.indices.contains(index) ?? false {
                return App.appSettings?.documentCategories?[index]
            }
            return nil
        }
    }

    /// Encodes the model object with data
    ///
    /// - Parameter user: User object
    static func encodAppSettingsDataAndSaveInUserDefaults(appSettings:AppSettings) {
        if let encodedData = try? JSONEncoder().encode(appSettings) {
            UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.APP_SETTINGS_DTAIILS)
        }
    }
    
    /// removes Encoded model object
    static func removeEncodedAppSettingsDataFromUserDefaults() {
        UserDefaults.standard.removeObject(forKey:  UserDefaltsKeys.APP_SETTINGS_DTAIILS)
    }
    
    
    /// gets decoded the model object
    ///
    /// - Returns: returns decoded user object
    static func getDecodedAppSettingsDetailsFromUserDefaults() -> AppSettings? {
        if let decodedData = UserDefaults.standard.data(forKey:  UserDefaltsKeys.APP_SETTINGS_DTAIILS),
            let userData = try? JSONDecoder().decode(AppSettings.self, from: decodedData) {
            return userData
        }
        return nil
    }
    
    class func getAppSettings(completion:@escaping(() -> Void)) {
        NetworkCall.performGet(url: App.getUserAccessToken().isEmpty ? AppURLS.URL_PUBLIC_APP_SETTINGS  : AppURLS.URL_APP_SETTINGS) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting app settings:\(err.localizedDescription)")
                App.appSettings = AppSettings.getDecodedAppSettingsDetailsFromUserDefaults()
                completion()
            }
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                print("App settings response:\(resp)")
                do {
                    let josndata = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    let appSettings = try decoder.decode(AppSettings.self, from: josndata)
                    App.appSettings = appSettings
                    App.FileSize = Double((appSettings.maxUploadFileSize ?? 25000) / 1000)
                   // print("before count:\(App.appSettings?.documentCategories?.count ?? 0)")
                    App.appSettings?.documentCategories = App.appSettings?.documentCategories?.filter{ $0.deletedAt == nil }
                   // print("after count:\(App.appSettings?.documentCategories?.count ?? 0)")
                    AppSettings.encodAppSettingsDataAndSaveInUserDefaults(appSettings: appSettings)
                    UserDefaults.standard.set(appSettings.isPasswordBlank ?? false, forKey: UserDefaltsKeys.KEY_SET_PASSWORD)
                    App.isPasswordCreated = appSettings.isPasswordBlank ?? false
                    App.didAskForSetPassword = appSettings.isPasswordBlank ?? false
                    completion()
                }catch let err {
                    print("Error while decoding hotline data:\(err.localizedDescription)")
                }
            }
            App.appSettings = AppSettings.getDecodedAppSettingsDetailsFromUserDefaults()
            completion()
        }
    }
}


class FamilyLimitInfo : Codable{
    var familyMemberCount: Int?
    var familyMembersAllowed: Int?
    var isMemberEnabled: Bool?
    var message: String?
    
    private enum CodingKeys: String, CodingKey {
        case familyMemberCount = "family_members_count"
        case familyMembersAllowed = "family_members_allowed"
        case isMemberEnabled = "family_members_config"
        case message
    }
}

class Hotline : Codable{
    var hotlineNumber: String?
    var hotlineName: String?
    var isHotline: Int?
    
    private enum CodingKeys: String, CodingKey {
        case hotlineNumber = "hotline_number"
        case hotlineName = "number_type"
        case isHotline = "is_hotline"
    }
}


