//
//  Medicines.swift
//  DocOnline
//
//  Created by Kiran Kumar on 25/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation
///Medicines model class
class Medicines : Codable {
    var availableQty : String?
    var discountPercentage : Int?
    var manf : String?
    var mrp : Double?
    var name : String?
    var packsize : Int?
    var price : String?
    var product_id :String?
    var qty : Int?
    var doctorPrescribedQty : Double?
    var appointment_id : String?
    var maxPacks : Double?
    
    init() {
        
    }
    
    init(app_id:String,available_qty:String,discount:Int,manufaturer:String,mrp:Double,medicine_name:String,pack_size:Int,price:String,product_id:String,quantity:Int,docPreQty:Double,max_packs:Double)
       {
        self.appointment_id = app_id
         self.availableQty = available_qty
        self.discountPercentage = discount
        self.manf = manufaturer
        self.mrp = mrp
        self.name = medicine_name
        self.packsize = pack_size
        self.price = price
        self.product_id = product_id
        self.qty = quantity
        self.doctorPrescribedQty = docPreQty
        self.maxPacks = max_packs
       }
}
