//
//  NetworkUtils.swift
//  AcceleratorSampleApp-Swift
//
//  Created by dev-3 on 27/06/17.
//  Copyright © 2017 Tokbox, Inc. All rights reserved.
//

import Foundation
import SystemConfiguration


/** 
   This class checks Internet connection
 */

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

public class NetworkUtilities
{

    /**
       Method returns false if no internet connection
     */
    class func isConnectedToNetwork() -> Bool {
        let reachability: Reachability = Reachability.init()!
        if reachability.isReachable || reachability.isReachableViaWiFi || reachability.isReachableViaWWAN {
            return true
        }
        print("no internet**")
        return  false
    }
    
    
    /**
      Checks the update for the application in app store
     */
   class func isUpdateAvailable(completion: @escaping (Bool?, String? ,Error?) -> Void) throws -> URLSessionDataTask {
    
    var task : URLSessionDataTask?
        if let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)")  {
          
            print("App identifier:\(identifier)")
            
             task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                do {
                    if let error = error {
                        print("Error while checking for update\(error.localizedDescription)")
                    }
                    
                    guard let data = data else {
                        return  print("No data")
                    }
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                    guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String else {
                        return print("invalid version response")
                    }
                    completion(version != currentVersion, version , nil)
                } catch {
                    completion(nil, nil, error)
                }
            }
            task?.resume()
            
        }
    
      return task!
    }
    
   
    class func showUpdateAlertIfAvailable(_ this:UIViewController) {
        do {
            _ = try NetworkUtilities.isUpdateAvailable(completion: { (success, appStoreVersion, error) in
                if let err = error {
                    print("Error while checking update available:\(err.localizedDescription)")
                }
                
                if let check = success  {
                    if check {
                        if let storeVersion = appStoreVersion {
                            if let info = Bundle.main.infoDictionary,
                                let currentVersion = info["CFBundleShortVersionString"] as? String,
                                let identifier = info["CFBundleIdentifier"] as? String{
                                print("appStoreVersion number:\(storeVersion) identifier:\(identifier)")
                                if currentVersion != storeVersion {
                                    let alert = UIAlertController(title: "Update Available", message: "A new version of DocOnline is available. Please update to version \(storeVersion) now", preferredStyle: UIAlertController.Style.alert)
                                    let updateAction = UIAlertAction(title: "Update", style: .default, handler: { (action) in
                                        let url = URL(string:"https://itunes.apple.com/app/id1234520016")
                                        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (success) in
                                            print("open update url:\(success)")
                                        })
                                    })
                                    
                                    let nextTime = UIAlertAction(title: "Next time", style: .default, handler: { (action) in
                                        
                                    })
                                    
                                    alert.addAction(nextTime)
                                    alert.addAction(updateAction)
                                    this.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
            })
        }catch let err {
            print("Error while checking update available:\(err.localizedDescription)")
        }
    }
  
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
