//
//  NetworkCall.swift
//  DocOnline
//
//  Created by Kiran Kumar on 06/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation
import MobileCoreServices
//import Alamofire

///Singleton class for performing `GET` operations
class NetworkCall {
    

    ///GET request 
    static func performGet(url:String,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
        
                                print("NetworkCall Response:\(jsonData)")
                                completionHandler(true,jsonData,httpResponse.statusCode, nil)
                            
                        }else {
                             completionHandler(false,nil,httpResponse.statusCode, nil)
                        }
                    }
                    catch let error
                    {
                        completionHandler(false,nil,httpResponse.statusCode, error)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
        }.resume()
    }
 
    
    
    ///POST request
    static func performPOST(url:String,data:Data,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = data
        session.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("NetworkCall Response:\(jsonData)")
                            completionHandler(true,jsonData,httpResponse.statusCode, nil)
                        }
                    }
                    catch let error
                    {
                        completionHandler(false,nil,httpResponse.statusCode, error)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
            }.resume()
    }
    
    ///POST request
    static func performPATCH(url:String,data:Data,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.PATCH
        request.httpBody = data
        session.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("NetworkCall Response:\(jsonData)")
                            completionHandler(true,jsonData,httpResponse.statusCode, nil)
                        }
                    }
                    catch let error
                    {
                        completionHandler(false,nil,httpResponse.statusCode, error)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
            }.resume()
    }
    
    ///PUT request
    static func performPUT(url:String,data:Data,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.PUT
        request.httpBody = data
        session.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("NetworkCall Response:\(jsonData)")
                            completionHandler(true,jsonData,httpResponse.statusCode, nil)
                        }
                    }
                    catch let error
                    {
                        completionHandler(false,nil,httpResponse.statusCode, error)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
            }.resume()
    }
    

    static func performCustomRequest(url:String,method:String,data:Data,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = method
        request.httpBody = data
        session.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("NetworkCall Response:\(jsonData)")
                            completionHandler(true,jsonData,httpResponse.statusCode, nil)
                        }
                    }
                    catch let error
                    {
                        completionHandler(false,nil,httpResponse.statusCode, error)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
            }.resume()
    }
    
    static func performDELETE(url:String,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {
    
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.DELETE
 
        session.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    if httpResponse.statusCode == 204 {
                        completionHandler(true,nil,httpResponse.statusCode,err)
                    }else {
                        completionHandler(false,nil,httpResponse.statusCode,err)
                    }
                }
            }
            }.resume()
    }
    
    
    static func performPOSTRequestWihtCookies(url:String,dataToPost:Data,cookie:String,completionHandler : @escaping (_ success:Bool,_ response:NSDictionary?,_ statusCode:Int?,_ error:Error?) -> Void) {

        var request = URLRequest(url: URL(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_ONE_TIME_VERSION_VALUE , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API)
        request.setValue(cookie, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_SESSION_ID)
       // request.httpShouldHandleCookies = true
        request.httpMethod = HTTPMethods.POST
        request.httpBody = dataToPost
        URLSession.shared.dataTask(with: request) { (data, response, err) in
            if err != nil {
                completionHandler(false,nil,nil, err)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("NetworkCall Response:\(jsonData)")
                            completionHandler(true,jsonData,httpResponse.statusCode, nil)
                        }
                    }
                    catch let error
                    {
                        completionHandler(false,nil,httpResponse.statusCode, error)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
        }.resume()
    }
    
    /**
     Method creates body with parameters of uploading image
     */
//   static func createBodyWithParameters(parameters: [UIImage],boundary: String) -> NSData {
//        let body = NSMutableData()
//
//        if parameters.count != 0 {
//            var i = 0;
//            for image in parameters {
//                let filename = "Appointment_attachments_\(i).jpg"   //should change id to app id
//                let data = image.jpegData(compressionQuality: 0.4);
//                let mimetype = mimeTypeForPath(path: filename)
//                let key = "attachments[\(i)]"
//
//                body.appendString(string: "--\(boundary)\r\n")
//                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
//                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
//                body.append(data!)
//                body.appendString(string: "\r\n")
//                i += 1;
//            }
//
//        }
//        body.appendString(string: "--\(boundary)--\r\n")
//        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
//        return body
//    }
    
    /**
     Method creates body with parameters of uploading image
     */
//    static func createBodyWithParametersForCapetionedImage(parameters: [ImagesCollectionViewCellModal],boundary: String) -> NSData {
//        let body = NSMutableData()
//
//        if parameters.count != 0 {
//            for (index,image) in parameters.enumerated() {
//                if let picture = image.picture {
//                    if let picCaption = picture.picCaption {
//                        let titleKey = "titles[\(index)]"
//                        body.appendString(string: "--\(boundary)\r\n")
//                        body.appendString(string: "Content-Disposition: form-data; name=\"\(titleKey)\"\r\n\r\n")
//                        body.appendString(string: "\(picCaption)\r\n")
//                    }
//
//                    if let pic = picture.pic {
//                        let filename = "Appointment_attachments_\(index).jpg"   //should change id to app id
//                        let data = pic.jpegData(compressionQuality: 0.4);
//                        let mimetype = NetworkCall.mimeTypeForPath(path: filename)
//                        let key = "attachments[\(index)]"
//                        body.appendString(string: "--\(boundary)\r\n")
//                        body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
//                        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
//                        body.append(data!)
//                        body.appendString(string: "\r\n")
//                    }
//                }
//            }
//        }
//        body.appendString(string: "--\(boundary)--\r\n")
//        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
//        return body
//    }
    
    
    /**
     Method genereates boundary string
     */
    static func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
        
    }
    
    ///returns the mime string type
   static func mimeTypeForPath(path: String) -> String {
        let pathUrl = NSURL(string: path)
        let pathExtension = pathUrl!.pathExtension
        var stringMimeType = "application/octet-stream"
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as! CFString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                stringMimeType = mimetype as NSString as String
            }
        }
        return stringMimeType;
    }
    
    /**
     Method uploads images to server with parameters
     */
//    static func uploadSelectedImagesWithCookie(this:UIViewController,cookie:String,completionHandler: @escaping (_ success:Bool,_ statusCode:Int,_ imagesURLS:[[String:Any]]?) -> Void) {
//
//        if App.tempbookingAttachedImages.count != 0 {
//
//            if !NetworkUtilities.isConnectedToNetwork()
//            {
//                this.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//                // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
//                return
//            }
//
//            //            let config = URLSessionConfiguration.default
//            //            let session = URLSession(configuration: config)
//
//            let boundary = generateBoundaryString()
//            let urlString = AppURLS.URUSER_ATTACHEMTENTS
//            print("ImageUrl = \(urlString)")
//            let url = URL(string: urlString)
//            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
//            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
//            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
//            request.setValue(cookie, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_SESSION_ID)
//            request.setValue(NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API)
//            request.httpMethod = HTTPMethods.POST
//          //  request.httpShouldHandleCookies = true
//
//            request.httpBody = createBodyWithParametersForCapetionedImage(parameters: App.tempbookingAttachedImages, boundary: boundary) as Data // createBodyWithParameters(parameters: App.tempImageArray, boundary: boundary) as Data
//            URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
//                if let error = error
//                {
//                    print("Error==> : \(error.localizedDescription)")
//                    DispatchQueue.main.async {
//                        this.didShowAlert(title: "", message: error.localizedDescription)
//                        let httpResponse = response as! HTTPURLResponse
//                        completionHandler(false, httpResponse.statusCode, nil)
//                    }
//                }
//                if let data = data
//                {
//                    print("data =\(data)")
//                }
//                if let response = response
//                {
//                    print("url = \(response.url!)")
//                    print("response = \(response)")
//                    let httpResponse = response as! HTTPURLResponse
//                    print("response code = \(httpResponse.statusCode)")
//                    //if you response is json do the following
//                    do
//                    {
//                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
//
//                        if httpResponse.statusCode == 400 {
//                            if let message = resultJSON.object(forKey:Keys.KEY_MESSAGE) as? String {
//                                DispatchQueue.main.async {
//                                    completionHandler(false,httpResponse.statusCode,nil)
//                                    this.didShowAlert(title:"",message:message)
//                                }
//                            }
//                        }
//
//                        //clear images array after successfull upload **
//                        if httpResponse.statusCode == 201 || httpResponse.statusCode == 200{
//                            print("Images uploaded successfully :\(resultJSON)")
//                            if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? [[String:Any]] {
//                                completionHandler(true,httpResponse.statusCode, data)
//                            }else {
//                                completionHandler(true,httpResponse.statusCode, nil)
//                            }
//                        }else {
//                            DispatchQueue.main.async {
//                                completionHandler(false,httpResponse.statusCode,nil)
//                                this.didShowAlert(title: "", message: "Images are not uploaded. Please try again attaching images in MyAppointments")
//                            }
//                        }
//                        print("Images response:\(resultJSON)  ")
//
//                    }catch let error{
//                        completionHandler(false,httpResponse.statusCode,nil)
//                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
//                    }
//                }
//
//            }).resume()
//
//        }else {
//            print("User not logged in")
//            completionHandler(true, 0, nil)
//        }
//    }
    
//    static func uploadViaAlamofire(this:UIViewController,cookie:String,completionHandler: @escaping (_ success:Bool,_ statusCode:Int,_ imagesURLS:[[String:Any]]?,_ response:[String:Any]?) -> Void) {
//
//        let urlString = AppURLS.URUSER_ATTACHEMTENTS
//        let headers = [
//            NETWORK_REQUEST_KEYS.KEY_SESSION_ID : cookie,
//            NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API : NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE,
//            NETWORK_REQUEST_KEYS.KEY_ACCEPT : NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON
//        ]
//
//        if !App.bookingAttachedImages.isEmpty {
//            Alamofire.upload(multipartFormData: { (multipartFormData) in
//                for (index,file) in App.bookingAttachedImages.enumerated() {
//                    if file.picture?.type?.rawValue == FileType.file.rawValue {
//                        if let url = URL(string: file.picture?.fileURl ?? "") {
//                            multipartFormData.append(url, withName: "attachments[\(index)]" )
//                        }
//                    }else if file.picture?.type?.rawValue == FileType.image.rawValue {
//                        if let image = file.picture?.pic , let imageData = image.jpegData(compressionQuality: 0.4){
//                            multipartFormData.append(imageData, withName: "attachments[\(index)]" ,fileName: "image\(index).jpg", mimeType: "image/jpg")
//                        }
//                    }
//
//                    multipartFormData.append((file.picture?.picCaption ?? "").data(using: String.Encoding.utf8)!, withName: "titles[\(index)]")
//                }
//            }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
//                switch result {
//                case .success(let upload, _, _):
//
//                    upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
//                    })
//
//                    upload.responseJSON { response in
//                        print("Alamofire response : \(response.result.value)")
//                        if let resp = response.result.value as? [String:Any], let data = resp[Keys.KEY_DATA] as? [[String:Any]]  {
//                            completionHandler(true,response.response?.statusCode ?? 200, data, resp)
//                        }else {
//                            if let resp = response.result.value as? [String:Any] {
//                                completionHandler(false,response.response?.statusCode ?? 500, nil, resp)
//                            }
//                            completionHandler(false,response.response?.statusCode ?? 500, nil, nil)
//                        }
//                    }
//
//                case .failure(let encodingError):
//                    print("Upload failure:\(encodingError)")
//                    DispatchQueue.main.async {
//                        this.didShowAlert(title: "", message: "Failed to upload files. Please try again")
//                        completionHandler(false,409, nil, nil)
//                    }
//                }
//            }
//        }else {
//            completionHandler(false, 0, nil, nil)
//        }
//    }
    
}
