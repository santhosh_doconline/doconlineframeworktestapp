//
//  Patient.swift
//  DocOnline
//
//  Created by dev-3 on 13/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

/** 
  This is modal class for patient
 */
class Patient: Codable {
    
    var id : Int!
    var email: String!
    var avatar_url : String!
    var full_name : String!
    var firstName: String!
    var dob:String!
    var gender : String!
    var age: String!
    
    var mobileNo: String?
    var timeZone: String?
    var mrnNo: String?
    var isVerified: Int?
    var mobileVerified: Int?
    var isActive: Int?
    var idProofType: Int?
    var idProofValue: String?
    var newUser: Int?
    var medplusId: String?
    var razorpayId: String?
    var leadsquaredId: String?
    var statusDisplay: String?
    var fullDetails: User?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case email
        case avatar_url
        case full_name
        case firstName = "first_name"
        case dob
        case gender
        case age
        case mobileNo = "mobile_no"
        case timeZone = "timezone"
        case mrnNo = "mrn_no"
        case isVerified = "is_verified"
        case mobileVerified = "mobile_verified"
        case isActive = "is_active"
        case idProofType = "id_proof_type"
        case idProofValue = "id_proof_value"
        case newUser = "new_user"
        case medplusId = "medplus_id"
        case razorpayId = "razorpay_id"
        case leadsquaredId = "leadsquaredId"
        case statusDisplay = "status_display"
        case fullDetails = "details"
    }
   
    
    
    init() {
        
    }
    
    ///Before New UI
    init(id:Int,email:String,avatar:String,fullname:String,dob:String,gender:String) {
        self.id = id
        self.email = email
        self.avatar_url = avatar
        self.full_name = fullname
        self.dob = dob
        self.gender = gender
    }
    
    ///after new UI
    init(id:Int,email:String,avatar:String,firstName:String,fullname:String,dob:String,gender:String,age:String) {
        self.id = id
        self.email = email
        self.avatar_url = avatar
        self.full_name = fullname
        self.dob = dob
        self.gender = gender
        self.age = age
    }
    
}
