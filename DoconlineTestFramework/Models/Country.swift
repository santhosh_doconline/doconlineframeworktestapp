//
//  Country.swift
//  DocOnline
//
//  Created by dev-3 on 09/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

/**
  This is modal class for countries list
*/

class Country : NSObject , Codable {
    
    var id : Int?
    var name : String?
    
    init(id:Int,name:String) {
        self.id = id
        self.name = name
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "countryId"
        case name = "countryName"
    }
    
    /**
       Method used to get countries list
       - Returns : Countries list 
     */
    static func getCountries() -> [Country]{
        if UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_COUNTRIES_LIST) != nil ,
            let countriesdata = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_COUNTRIES_LIST) as? Data {
            do {
                App.countriesList = try JSONDecoder().decode([Country].self, from: countriesdata)
                print("countries count=\( App.countriesList.count)")
                return App.countriesList
            }catch let err {
                print("Error while decoding countries:\(err)")
                return App.countriesList
            }
        }
        return App.countriesList
    }

    
    
//    required init(coder decoder: NSCoder) {
//        self.id = decoder.decodeObject(forKey: "countryId") as? Int
//        self.name = decoder.decodeObject(forKey: "countryName") as? String
//    }
//
//    func encode(with coder: NSCoder) {
//        coder.encode(name, forKey: "countryName")
//        coder.encode(id, forKey: "countryId")
//    }
}
