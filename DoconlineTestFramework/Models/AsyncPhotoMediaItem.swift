//
//  AsyncPhotoMediaItem.swift
//  DocOnline
//
//  Created by Kiran Kumar on 05/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//



import Foundation
import JSQMessagesViewController
import Kingfisher
import SDWebImage

/**
 AsybcPhotoMediaItem class is used to fetches the image asynchronously in chatting view
 */
class AsyncPhotoMediaItem: JSQPhotoMediaItem {
    var asyncImageView: UIImageView!
    
    override init!(maskAsOutgoing: Bool) {
        super.init(maskAsOutgoing: maskAsOutgoing)
    }
    
    init(withURL url: NSURL  ) {        //, imageSize: CGSize, isOperator: Bool) {
        super.init()
 
        let size = super.mediaViewDisplaySize()

        asyncImageView = UIImageView()
        asyncImageView.frame = CGRect(x:0, y:0,width: size.width,height: size.height)
        asyncImageView.contentMode = .scaleAspectFill
        asyncImageView.clipsToBounds = true
        asyncImageView.layer.cornerRadius = 20
        asyncImageView.backgroundColor = UIColor.jsq_messageBubbleLightGray()
        
        let activityIndicator = JSQMessagesMediaPlaceholderView.withActivityIndicator()
        activityIndicator?.frame = asyncImageView.frame
        asyncImageView.addSubview(activityIndicator!)
        
        
        let image = SDImageCache.shared().imageFromDiskCache(forKey: url.absoluteString)
        
        if image == nil {
            self.asyncImageView.sd_setImage(with: url as URL, completed: { (image, error, cacheTypr, url) in
                if error == nil {
                    self.asyncImageView.image = image
                    activityIndicator?.removeFromSuperview()
                }else {
                    print("Image downlading error: %@",error?.localizedDescription ?? "")
                }
            })
        }else {
            self.asyncImageView.image = image
            activityIndicator?.removeFromSuperview()
        }
        
        
        
//       KingfisherManager.shared.cache.retrieveImage(forKey: url.absoluteString!, options: nil) { (image, cacheType) in
//            if let image = image {
//                self.asyncImageView.image = image
//                activityIndicator?.removeFromSuperview()
//            } else {
//                
//                KingfisherManager.shared.downloader.downloadImage(with: url as URL, retrieveImageTask: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, url, data) in
//                   
//                    if let image = image {
//                        self.asyncImageView.image = image
//                        activityIndicator?.removeFromSuperview()
//
//                        KingfisherManager.shared.cache.store(image, original: data, forKey: url!.absoluteString, processorIdentifier: url!.absoluteString, cacheSerializer: DefaultCacheSerializer.default , toDisk: true, completionHandler: nil)
//                    }
//                })
//            }
//        }
    }
    
    override func mediaView() -> UIView! {
        return asyncImageView
    }
    
    func getImage() -> UIImage! {
        return self.asyncImageView.image
    }
    
    override func mediaViewDisplaySize() -> CGSize {
        return asyncImageView.frame.size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class KTBubbleImageFactor : JSQMessagesBubbleImageFactory {
    
}



