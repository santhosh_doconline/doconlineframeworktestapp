//
//  Prescription.swift
//  DocOnline
//
//  Created by Kiran Kumar on 28/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import Foundation

class PrescriptionsList: Codable {
    var currentPage: Int?
    var from: Int?
    var lastPage: Int?
    var nextPageUrl: String?
    var path: String?
    var perPage: Int?
    var prevPageUrl: String?
    var to: Int?
    var total: Int?
    var consultations: [Consultation]?
    
    private enum CodingKeys: String,CodingKey {
        case currentPage = "current_page"
        case from
        case lastPage = "last_page"
        case nextPageUrl = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageUrl = "prev_page_url"
        case to
        case total
        case consultations = "data"
    }
}

class Consultation: Codable{
    var id: Int?
    var userId: Int?
    var bookedForUserId: Int?
    var isSelf: Int?
    var bookedBy: Int?
    var callType: Int?
    var callChannel: Int?
    //var langPref: Bool?
    var bookingType: Int?
    var scheduledAt: String?
    var startedAt: String?
    var finishedAt: String?
    var status: Int?
    var callStatus: Int?
    var callDuration: String?
    var cancelledBy: Int?
    var doctorNotified: Int?
    var userNotified: Int?
   // var isFullfilled: Int?
    var notes: String?
    var doctorNotes: String?
    var createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    var publicAppointmentID: String?
    var schedule: Schedule?
    var patient: Patient?
    var medications: [Medication]?
    var prescriptionHTMLString: String = ""
    var isPrescrptionLoaded = false
    var snapshot: UIImage? = nil
    
    //For booked appointment success response
    var appointmentId: Int?
    var user: User?
    var doctor: Doctor?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case appointmentId = "appointment_id"
        case userId = "user_id"
        case bookedForUserId = "booked_for_user_id"
        case isSelf = "is_self"
        case bookedBy = "booked_by"
        case callType = "call_type"
        case callChannel = "call_channel"
      //  case langPref = "lang_pref"
        case bookingType = "booking_type"
        case scheduledAt = "scheduled_at"
        case startedAt = "started_at"
        case finishedAt = "finished_at"
        case status
        case callStatus = "call_status"
        case callDuration = "call_duration"
        case cancelledBy = "cancelled_by"
        case doctorNotified = "doctor_notified"
        case userNotified = "user_notified"
      //  case isFullfilled = "is_fulfilled"
        case notes
        case doctorNotes = "doctor_notes"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case publicAppointmentID = "public_appointment_id"
        case schedule
        case patient
        case medications
        case user
        case doctor
    }
    
    struct Schedule : Codable{
        var id: Int?
        var doctorId: Int?
        var appointmentId: Int?
        var status: Int?
        var createdAt: String?
        var updatedAt: String?
        var doctor: Doctor?
        
        private enum CodingKeys: String, CodingKey {
            case id
            case doctorId = "doctor_id"
            case appointmentId = "appointment_id"
            case status
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case doctor
        }
    }
    
}

extension Consultation {
    func loadPrescription() {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + .milliseconds(4), execute: {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: "\(AppURLS.URL_Prescription)\(self.publicAppointmentID ?? "")")
            print("PrescriptionURL==>\(AppURLS.URL_Prescription)\(self.publicAppointmentID ?? "")")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request) { (data, response, error) in
                if error != nil
                {
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    let htmlString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    self.prescriptionHTMLString = String(htmlString ?? "")
                }
            }.resume()
        })
    }
}
