//
//  HealthProfile.swift
//  DocOnline
//
//  Created by dev-3 on 14/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation


/** 
  This is modal class for Health profile
 */
class HealthProfile {
    
    var weight : Double!
    var height : Double!
    var does_smoke : String!
    var medical_history : String!
    var allergies : [Allergy]!
    var medications : [Medication]!
    var life_style : Int!
    var sleep_duration : Int!
    var sleep_pattern : Int!
    var exercise_per_week : Int!
    var marital_status : Int!
    var switch_in_medicine : Int!
    var medical_history_new : Int!
    var medical_insurance : Int!
    var switch_in_medicine_reason : String!
    
    init() {
        
    }
    
    init(weight:Double,height:Double,does_smoke:String,medical_history:String,allergies:[Allergy],medications:[Medication]) {
        self.weight = weight
        self.height = height
        self.does_smoke = does_smoke
        self.medical_history = medical_history
        self.allergies = allergies
        self.medications = medications
    }
    
    init(weight:Double,height:Double,does_smoke:String,medical_history:String,allergies:[Allergy],medications:[Medication],life_style:Int,sleep_duration:Int,sleep_pattern:Int,exercise_per_week:Int,marital_status:Int,switch_in_medicine:Int,medical_history_new:Int,medical_insurance:Int,switch_in_medicine_reason:String) {
        self.weight = weight
        self.height = height
        self.does_smoke = does_smoke
        self.medical_history = medical_history
        self.allergies = allergies
        self.medications = medications
        self.life_style = life_style
        self.sleep_duration = sleep_duration
        self.sleep_pattern = sleep_pattern
        self.exercise_per_week = exercise_per_week
        self.marital_status = marital_status
        self.switch_in_medicine = switch_in_medicine
        self.medical_history_new = medical_history_new
        self.medical_insurance = medical_insurance
        self.switch_in_medicine_reason = switch_in_medicine_reason
    }
    
    /**
      Stores health profile locally
      - Parameter dataModel: Pass the health profile object to store details
     */
    class func store_health_profile_default(dataModel: HealthProfile)
    {
        
        let defaults = UserDefaults.standard
      
        defaults.setValue(dataModel.height, forKey: UserDefaltsKeys.KEY_HEIGHT)
        defaults.setValue(dataModel.weight, forKey: UserDefaltsKeys.KEY_WEIGHT)
        defaults.setValue(dataModel.does_smoke, forKey: UserDefaltsKeys.KEY_DOES_SMOKE)
        defaults.setValue(dataModel.medical_history, forKey: UserDefaltsKeys.KEY_MEDICAL_HISTORY)
        defaults.set(true, forKey: UserDefaltsKeys.KEY_HEALTH_STATUS)
        defaults.setValue(dataModel.life_style, forKey: UserDefaltsKeys.KEY_LIFE_STYLE)
        defaults.setValue(dataModel.sleep_duration, forKey: UserDefaltsKeys.KEY_SLEEP_DURATION)
        defaults.setValue(dataModel.sleep_pattern, forKey: UserDefaltsKeys.KEY_SLEEP_PATTERN)
        defaults.setValue(dataModel.exercise_per_week, forKey: UserDefaltsKeys.KEY_EXERCISE_PER_WEEK)
        defaults.setValue(dataModel.marital_status, forKey: UserDefaltsKeys.KEY_MARITAL_STATUS)
        defaults.setValue(dataModel.switch_in_medicine, forKey: UserDefaltsKeys.KEY_SWITCH_MEDICINE)
        defaults.setValue(dataModel.medical_history_new, forKey: UserDefaltsKeys.KEY_MEDICAL_HISTORY_NEW)
        defaults.setValue(dataModel.medical_insurance, forKey: UserDefaltsKeys.KEY_MEDICAL_INSURANCE)
        defaults.setValue(dataModel.switch_in_medicine_reason, forKey: UserDefaltsKeys.KEY_SWITCH_MEDICINE_OTHERS)
        defaults.synchronize()
      //  print("Storing health information:\(dataModel.does_smoke) heaight:\(dataModel.height)")
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateHealthProfile"), object: nil)
        })
    }
    
    
    /**
      Method used to access health profile details
     - Returns : User health profile details
     */
    class func get_health_profile_default() -> HealthProfile
    {
        let defaults = UserDefaults.standard
        let userHealth = HealthProfile()
        
        if let user_height = defaults.string(forKey: UserDefaltsKeys.KEY_HEIGHT)
        {
            userHealth.height = Double(user_height)
        }
        
        if let user_weight = defaults.string(forKey: UserDefaltsKeys.KEY_WEIGHT)
        {
            userHealth.weight =  Double(user_weight)
        }
        
        if let user_does_smoke = defaults.string(forKey: UserDefaltsKeys.KEY_DOES_SMOKE)
        {
            userHealth.does_smoke = user_does_smoke
        }
        
        if let user_medical_history = defaults.string(forKey: UserDefaltsKeys.KEY_MEDICAL_HISTORY)
        {
            userHealth.medical_history = user_medical_history
        }
        
        if let user_switch_in_medicine_reason = defaults.string(forKey: UserDefaltsKeys.KEY_SWITCH_MEDICINE_OTHERS)
        {
            userHealth.switch_in_medicine_reason = user_switch_in_medicine_reason
        }
        
        userHealth.life_style = defaults.integer(forKey: UserDefaltsKeys.KEY_LIFE_STYLE)
        
        
        userHealth.sleep_duration = defaults.integer(forKey: UserDefaltsKeys.KEY_SLEEP_DURATION)
        
        userHealth.sleep_pattern = defaults.integer(forKey: UserDefaltsKeys.KEY_SLEEP_PATTERN)
        
        
        userHealth.exercise_per_week = defaults.integer(forKey: UserDefaltsKeys.KEY_EXERCISE_PER_WEEK)
        
        
        userHealth.marital_status = defaults.integer(forKey: UserDefaltsKeys.KEY_MARITAL_STATUS)
        
        
        userHealth.switch_in_medicine = defaults.integer(forKey: UserDefaltsKeys.KEY_SWITCH_MEDICINE)
        
        
        userHealth.medical_history_new = defaults.integer(forKey: UserDefaltsKeys.KEY_MEDICAL_HISTORY_NEW)
        
        
        userHealth.medical_insurance = defaults.integer(forKey: UserDefaltsKeys.KEY_MEDICAL_INSURANCE)
        
        
        return userHealth
    }
    
    /**
     Method used to update health profile details
     */
    class func update_user_default(dataModel: HealthProfile)
    {
        let defaults = UserDefaults.standard
        defaults.setValue(dataModel.height, forKey: UserDefaltsKeys.KEY_HEIGHT)
        defaults.setValue(dataModel.weight, forKey: UserDefaltsKeys.KEY_WEIGHT)
        defaults.setValue(dataModel.does_smoke, forKey: UserDefaltsKeys.KEY_DOES_SMOKE)
        defaults.setValue(dataModel.medical_history, forKey: UserDefaltsKeys.KEY_MEDICAL_HISTORY)
        defaults.synchronize()
    }
    
    /**
     Method used to get medications
      - Returns : Medication array
     */
    static func getMedicaions() -> [Medication]{
        if UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_MEDICATIONS) != nil ,
            let medicationsData = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_MEDICATIONS) as? Data {
            do {
                 App.medications = try JSONDecoder().decode([Medication].self, from: medicationsData)
                 print("Medications=\( App.medications.count)")
                return App.medications
            }catch let err {
                print("Error while decoding medications:\(err)")
                return App.medications
            }
        }
        return App.medications
    }
    
    /**
     Method used to get Allergies
     - Returns : Allergies array
     */
    static func getAllergies() -> [Allergy]{
        if UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_ALLERGIES) != nil ,
            let allergiesData = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_ALLERGIES) as? Data {
            do {
                App.allergies = try JSONDecoder().decode([Allergy].self, from: allergiesData)
                print("Allergies=\( App.allergies.count)")
                return App.allergies
            }catch let err {
                print("Error while decoding allergies:\(err)")
                return App.allergies
            }
        }
        return App.allergies
    }
    
    /**
     Method used to get Drug Allergies
     - Returns : Drug Allergies array
     */
    static func getDrugAllergies() -> [Allergy]{
        if UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_DRUG_ALLERGIES) != nil ,
            let allergiesData = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_DRUG_ALLERGIES) as? Data {
            do {
                App.drug_allergies = try JSONDecoder().decode([Allergy].self, from: allergiesData)
                print("Drug Allergies=\( App.drug_allergies.count)")
                return App.drug_allergies
            }catch let err {
                print("Error while decoding drug allergies:\(err)")
                return App.drug_allergies
            }
        }
        return App.drug_allergies
    }
    
    /**
     Method used to get Drug Allergies
     - Returns : Drug Allergies array
     */
    static func getMasterData() -> MasterData{
        if UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_MASTER_DATA) != nil ,
            let allergiesData = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_MASTER_DATA) as? Data {
            do {
                App.drug_allergies = try JSONDecoder().decode([Allergy].self, from: allergiesData)
                print("masterData=\( App.masterData)")
                return App.masterData
            }catch let err {
                print("Error while decoding master data:\(err)")
                return App.masterData
            }
        }
        return App.masterData
    }

    /**
     Method used to check whether user health profile is stored or not
     - Returns : Boolean value
     */
    class func check_Health_defaults() ->  Bool {
        let defaults = UserDefaults.standard
        let value = defaults.bool(forKey:  UserDefaltsKeys.KEY_HEALTH_STATUS)
        print("Health status:\(value)")
        return value
    }
    
    
}

/*
 * This is modal class for Medications
 */

class Medication : NSObject , Codable{
    var id : Int?
    var user_id : Int?
    var name : String?
    var created_at : String?
    var updated_at : String?
    var dateFrom : String?
    var toDate : String?
    var intakeTime : String?
    var notes : String?
    var numberofdays : Int?
    var status : Int?
    
    var appointmentId: Int?
    var productId: String?
    var quantity: Int?
    var courseDuration: String?
    var courseDurationType: String?
    var drugType: String?
    var repeatMode: String?
    var dosageMorning: Int?
    var dosageAfternoon: Int?
    var dosageEvening: Int?
    var dosageNight: Int?
    var dosageTime: String?
    var doctorComments: String?
    
    private enum CodingKeys: String,CodingKey {
        case id
        case user_id
        case name
        case created_at
        case updated_at
        case dateFrom
        case toDate
        case intakeTime
        case notes
        case numberofdays = "medicine_intake_days"
        case status = "medication_state"
        case appointmentId = "appointment_id"
        case productId = "product_id"
        case quantity = "qty"
        case courseDuration = "course_duration"
        case courseDurationType = "course_duration_type"
        case drugType = "drug_type"
        case repeatMode = "repeat_mode"
        case dosageMorning = "dosage_morning"
        case dosageAfternoon = "dosage_afternoon"
        case dosageEvening = "dosage_evening"
        case dosageNight = "dosage_night"
        case dosageTime = "dosage_time"
        case doctorComments = "doctor_comments"
    }
    
    
    init(id:Int,userid:Int,name:String,createdat:String,updatedat:String,dateFrom: String,toDate: String,intake:String,notes:String,numberofdays:Int,status:Int) {
        self.id = id
        self.user_id = userid
        self.name = name
        self.created_at = createdat
        self.updated_at = updatedat
        self.dateFrom = dateFrom
        self.toDate = toDate
        self.intakeTime = intake
        self.notes = notes
        self.numberofdays = numberofdays
        self.status = status
    }
    
//    required init(coder decoder: NSCoder) {
//        self.id = decoder.decodeObject(forKey: "id") as! Int
//        self.user_id = decoder.decodeObject(forKey: "user_id") as! Int
//        self.created_at = decoder.decodeObject(forKey: "created_at") as? String ?? ""
//        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
//        self.updated_at = decoder.decodeObject(forKey: "updated_at") as? String ?? ""
//        self.dateFrom = decoder.decodeObject(forKey: "dateFrom") as? String ?? ""
//        self.toDate = decoder.decodeObject(forKey: "toDate") as? String ?? ""
//        self.intakeTime = decoder.decodeObject(forKey: "intakeTime") as? String ?? ""
//        self.notes = decoder.decodeObject(forKey: "notes") as? String ?? ""
//    }
//
//    func encode(with coder: NSCoder) {
//        coder.encode(name, forKey: "name")
//        coder.encode(id, forKey: "id")
//        coder.encode(user_id, forKey: "user_id")
//        coder.encode(created_at, forKey: "created_at")
//        coder.encode(updated_at, forKey: "updated_at")
//        coder.encode(dateFrom, forKey: "dateFrom")
//        coder.encode(toDate, forKey: "toDate")
//        coder.encode(intakeTime, forKey: "intakeTime")
//        coder.encode(notes, forKey: "notes")
//    }
}


/*
 * This is modal class for Allergies list
 */

class Allergy : NSObject , Codable {
    
    var id : Int?
    var user_id : Int?
    var name : String?
    var created_at : String?
    var updated_at : String?
    
    init(id:Int,userid:Int,name:String,createdat:String,updatedat:String) {
        self.id = id
        self.user_id = userid
        self.name = name
        self.created_at = createdat
        self.updated_at = updatedat
    }
    
    private enum CodingKeys: String,CodingKey {
        case id
        case user_id
        case name
        case created_at
        case updated_at
    }
    
//    required init(coder decoder: NSCoder) {
//        self.id = decoder.decodeObject(forKey: "id") as! Int
//        self.user_id = decoder.decodeObject(forKey: "user_id") as! Int
//        self.created_at = decoder.decodeObject(forKey: "created_at") as? String ?? ""
//        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
//        self.updated_at = decoder.decodeObject(forKey: "updated_at") as? String ?? ""
//    }
//
//    func encode(with coder: NSCoder) {
//        coder.encode(name, forKey: "name")
//        coder.encode(id, forKey: "id")
//        coder.encode(user_id, forKey: "user_id")
//        coder.encode(created_at, forKey: "created_at")
//        coder.encode(updated_at, forKey: "updated_at")
//    }
}

class MasterData : NSObject , Codable {
    var exercise_per_week : [OptionsData]?
    var life_style_activity : [OptionsData]?
    var marital_status : [OptionsData]?
    var medical_history : [OptionsData]?
    var sleep_duration : [OptionsData]?
    var sleep_pattern : [OptionsData]?
    var switch_in_medicine : [OptionsData]?
    
    override init() {
        
    }
    
    init(exercise_per_week:[OptionsData],life_style_activity:[OptionsData],marital_status:[OptionsData],medical_history:[OptionsData],sleep_duration:[OptionsData],sleep_pattern:[OptionsData],switch_in_medicine:[OptionsData]) {
        self.exercise_per_week = exercise_per_week
        self.life_style_activity = life_style_activity
        self.marital_status = marital_status
        self.medical_history = medical_history
        self.sleep_duration = sleep_duration
        self.sleep_pattern = sleep_pattern
        self.switch_in_medicine = switch_in_medicine
    }
    private enum CodingKeys: String,CodingKey {
        case exercise_per_week
        case life_style_activity
        case marital_status
        case medical_history
        case sleep_duration
        case sleep_pattern
        case switch_in_medicine
    }
}

class OptionsData : NSObject , Codable {
    
    var id : Int?
    var first_level_value : String?
    
    init(id:Int,first_level_value:String) {
        self.id = id
        self.first_level_value = first_level_value
    }
    
    private enum CodingKeys: String,CodingKey {
        case id
        case first_level_value
    }
}

class Pregnancy : NSObject , Codable {
    
    var id : Int?
    var user_id : Int?
    var created_at : String?
    var updated_at : String?
    var abortions : String?
    var conceptions : String?
    var complications : String?
    var expecting_mother : String?
    
    override init() {
        
    }
    
    init(id:Int,userid:Int,createdat:String,updatedat:String,abortions:String,conceptions:String,complications:String,expecting_mother:String) {
        self.id = id
        self.user_id = userid
        self.created_at = createdat
        self.updated_at = updatedat
        self.abortions = abortions
        self.conceptions = conceptions
        self.complications = complications
        self.expecting_mother = expecting_mother
    }
    
    private enum CodingKeys: String,CodingKey {
        case id
        case user_id
        case created_at
        case updated_at
        case abortions = "no_of_abortions"
        case conceptions = "no_of_conceptions"
        case complications = "complications_if_any"
        case expecting_mother
    }
}
