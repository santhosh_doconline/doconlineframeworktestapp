//
//  User.swift
//  DocOnline
//
//  Created by dev-3 on 09/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation


/**  
   this is Modal class for user details
 */

class User : Codable{
    
    var avatar : String?
    var prefix : String?
    var name : String?
    var first_name : String?
    var last_name : String?
    var middle_name : String?
    var email : String?
    var gender : String?
    var dob : String?
    var phone : String?
    var mobileNo : String?
    var address1 : String?
    var address2 : String?
    var city : String?
    var state : String?
    var pincode : String?
    var country : Int?
    var full_name : String?
    var id : Int?
    var user_id : Int?
    var alternate_phone : String?
    
    var family_mem_1 : String? = nil
    var family_mem_2 : String? = nil
    var token_type : String? = nil
    var access_token : String? = nil
    var enable_coupon : Bool? = nil
    var coupon_code : String? = nil
    var password : String? = nil
    var password_confirmation:String? = nil
    
    var language_preferences : [Int]?
    var language_preferences_values : [String]?
    var age: String?
    
    //added newly for serialization in prescriptions list
    var weight: Int?
    var height: Int?
    var does_smoke : Int?
    var medical_history : String?
    var createdAt: String?
    var updatedAt: String?
    var isBookable: Int?
    var is_verified: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case user_id
        case prefix
        case full_name 
        case first_name
        case last_name
        case middle_name
        case dob = "date_of_birth"
        case mobileNo,phone = "mobile_no"
        case password
        case password_confirmation
        case gender
        case email
        case timezone
        case mrnNumber = "mrn_no"
        case is_verified
        case address1
        case address2
        case country = "country_id"
        case state
        case city
        case pincode = "pin_code"
        case alternate_phone = "alternate_contact_no"
        case weight
        case height
        case does_smoke
        case medical_history
        case language_preferences
        case language_preferences_values
        case isBookable = "bookable"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case age
        case avatar = "avatar_url"
        case name
        case otp_code
    }
    
    
    var userType:String! = ""
    
    var isMobileVerified:Bool! = false
    var isEmailVerified:Bool! = false
    
    var mrnNumber: String? = ""
    
    ///for freshly signuped user for otp verification
    var timezone : String?
    var otp_code:String?
   
    //Familiy count
    var familyMembersCount: Int? = nil
    var familyMemAllowedToAdd: Int? = nil
    var exceededMsgOfFamilyMem: String? = nil
    
    
    var media_source:String = ""
    
    init() {
        
    }
    
    init(avatar : String,user_id : Int,prefix : String,middle_name : String,phone : String,alternate_phone : String,address1 : String,address2 : String,city : String,state : String,pincode :String,id:Int,first_name:String,last_name:String,email:String,gender:String,dob:String,fullname:String,country:Int) {
        self.avatar = avatar
        self.user_id = user_id
        self.prefix = prefix
        self.middle_name = middle_name
        self.phone = phone
        self.alternate_phone = alternate_phone
        self.address1 = address1
        self.address2 = address2
        self.city = city
        self.state = state
        self.pincode = pincode
        self.first_name = first_name
        self.id = id
        self.last_name = last_name
        self.email = email
        self.gender = gender
        self.dob = dob
        self.full_name = fullname
        self.country = country
    }
    
    
    init(first_name : String,last_name : String,email : String,mobile_no:String,password : String,password_confirmation : String,enable_coupon:Bool,coupon_code:String,media_source:String) {
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = mobile_no
        self.mobileNo = mobile_no
        self.password = password
        self.password_confirmation = password_confirmation
        self.enable_coupon = enable_coupon
        self.coupon_code = coupon_code
        self.media_source = media_source
    }
    
    init(id:Int,first_name:String,last_name:String,email:String,gender:String,dob:String,fullname:String,country:Int) {
        self.first_name = first_name
        self.id = id
        self.last_name = last_name
        self.email = email
        self.gender = gender
        self.dob = dob
        self.full_name = fullname
        self.country = country
    }
    
    ///For booking consultation drop down
    init(id:Int,userName:String,type:String,userAvatar:String,age:String,gender: String? = "") {
        self.id = id
        self.name = userName
        self.userType = type
        self.avatar = userAvatar
        self.age = age
        self.gender = ""
        if let tempGender = gender {
            self.gender = tempGender
        }
    }
    
    ///For familymember count check
    init(familyMemCount:Int,familyMemAllowed:Int,exceedMsg:String) {
        self.familyMembersCount = familyMemCount
        self.familyMemAllowedToAdd = familyMemAllowed
        self.exceededMsgOfFamilyMem = exceedMsg
    }
    
    
    /**
       stores user profile details locally using *UserDefaults*
       - Parameter dataModel : pass user details to save details
     */
    class func store_user_profile_default(dataModel: User)
    {
        let defaults = UserDefaults.standard
        defaults.setValue(dataModel.first_name, forKey: UserDefaltsKeys.KEY_FIRSTNAME)
        defaults.setValue(dataModel.mrnNumber, forKey: UserDefaltsKeys.KEY_MRN_NO)
       // print("value:\(dataModel.mrnNumber) Stored MRN:\(defaults.value(forKey: UserDefaltsKeys.KEY_MRN_NO) as? String)")
        defaults.setValue(dataModel.full_name, forKey: UserDefaltsKeys.KEY_FULL_NAME)
        defaults.setValue(dataModel.prefix, forKey: UserDefaltsKeys.KEY_USER_PREFIX)
        defaults.setValue(dataModel.name, forKey: UserDefaltsKeys.KEY_USER_NAME)
        defaults.setValue(dataModel.middle_name, forKey: UserDefaltsKeys.KEY_USER_MIDDLE_NAME)
        defaults.setValue(dataModel.last_name, forKey: UserDefaltsKeys.KEY_USER_LAST_NAME)
        defaults.setValue(dataModel.dob, forKey: UserDefaltsKeys.KEY_USER_DATE_BIRTH)
        defaults.setValue(dataModel.gender, forKey: UserDefaltsKeys.KEY_USER_GENDER)
        defaults.setValue(dataModel.email, forKey: UserDefaltsKeys.KEY_USER_EMAIL)
        defaults.setValue(dataModel.phone, forKey: UserDefaltsKeys.KEY_USER_PHONE)
        defaults.setValue(dataModel.alternate_phone, forKey: UserDefaltsKeys.KEY_USER_ALTERNATE_PHONE)
        defaults.setValue(dataModel.address1, forKey: UserDefaltsKeys.KEY_USER_ADDRESS1)
        defaults.setValue(dataModel.address2, forKey: UserDefaltsKeys.KEY_USER_ADDRESS2)
        defaults.setValue(dataModel.city, forKey: UserDefaltsKeys.KEY_USER_CITY)
        defaults.setValue(dataModel.state, forKey: UserDefaltsKeys.KEY_USER_STATE)
        defaults.setValue(dataModel.pincode, forKey: UserDefaltsKeys.KEY_USER_PINCODE)
        defaults.setValue(dataModel.country, forKey: UserDefaltsKeys.KEY_USER_COUNTRY_CODE)
        defaults.setValue(dataModel.language_preferences , forKey: UserDefaltsKeys.LANGUAGE_PREFERENCES)
        defaults.setValue(dataModel.language_preferences_values , forKey: UserDefaltsKeys.LANGUAGE_PREFERENCES_VALUES)
        defaults.set(dataModel.isMobileVerified, forKey: UserDefaltsKeys.IS_MOBILE_VERIFIED)
        defaults.set(dataModel.isEmailVerified, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
        
        defaults.set(true, forKey: UserDefaltsKeys.KEY_USER_STATUS)
        defaults.synchronize()
        ///new method to store details
        dataModel.saveSerializedUserDetails()
    }
    
    func storeFamilyCount() {
        let defaults = UserDefaults.standard
        defaults.set(self.familyMembersCount, forKey: UserDefaltsKeys.FAMILY_MEMBERS_COUNT)
        defaults.set(self.familyMemAllowedToAdd, forKey: UserDefaltsKeys.FAMILY_ALLOWED_COUNT)
        defaults.set(self.exceededMsgOfFamilyMem, forKey: UserDefaltsKeys.FAMILY_EXCEED_MESSAGE)
    }
    
    /**
      removes user profile details locally using *UserDefaults*
     */
    class func remove_user_profile_default()
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_FIRSTNAME)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_MRN_NO)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_PREFIX)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_NAME)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_MIDDLE_NAME)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_LAST_NAME)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_DATE_BIRTH)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_GENDER)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_EMAIL)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_PHONE)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_ALTERNATE_PHONE)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_ADDRESS1)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_ADDRESS2)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_CITY)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_STATE)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_PINCODE)
        defaults.removeObject(forKey:  UserDefaltsKeys.KEY_USER_COUNTRY_CODE)
        defaults.removeObject(forKey:  UserDefaltsKeys.LANGUAGE_PREFERENCES)
        defaults.removeObject(forKey:  UserDefaltsKeys.LANGUAGE_PREFERENCES_VALUES)
        defaults.removeObject(forKey: UserDefaltsKeys.KEY_FULL_NAME)
        
        defaults.removeObject(forKey:  UserDefaltsKeys.FAMILY_ALLOWED_COUNT)
        defaults.removeObject(forKey:  UserDefaltsKeys.FAMILY_MEMBERS_COUNT)
        defaults.removeObject(forKey: UserDefaltsKeys.FAMILY_EXCEED_MESSAGE)
        defaults.set(false, forKey: UserDefaltsKeys.IS_MOBILE_VERIFIED)
        defaults.set(false, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
        
        defaults.set(false, forKey: UserDefaltsKeys.KEY_USER_STATUS)
        User.removeSavedUserDetails()
        SubscriptionPlan.removeSavedPlanDetails()
        print("user profile Defaults removed preferences are:")
    }
    
    
    /**
     Method returns stored user details from *UserDefaults*
     */
    class func get_user_profile_default() -> User
    {
        
        let defaults = UserDefaults.standard
        let user = User()
        
        if let user_first_name = defaults.string(forKey: UserDefaltsKeys.KEY_FIRSTNAME)
        {
            user.first_name = user_first_name
        }
        
        if let user_mrn = defaults.string(forKey: UserDefaltsKeys.KEY_MRN_NO)
        {
            user.mrnNumber = user_mrn
             print("Stored MRN getting:\(user_mrn)")
        }
        
        if let user_full_name = defaults.string(forKey: UserDefaltsKeys.KEY_FULL_NAME)
        {
            user.full_name = user_full_name
        }
        
        if let user_prefix = defaults.string(forKey: UserDefaltsKeys.KEY_USER_PREFIX)
        {
            user.prefix = user_prefix
        }
        
        if let user_name = defaults.string(forKey: UserDefaltsKeys.KEY_USER_NAME)
        {
            user.name =  user_name
        }
        
        if let user_middle = defaults.string(forKey: UserDefaltsKeys.KEY_USER_MIDDLE_NAME)
        {
            user.middle_name = user_middle
        }
        
        if let user_last = defaults.string(forKey: UserDefaltsKeys.KEY_USER_LAST_NAME)
        {
            user.last_name = user_last
        }
        
        if let user_dob = defaults.string(forKey: UserDefaltsKeys.KEY_USER_DATE_BIRTH)
        {
            user.dob = user_dob
        }
        else
        {
            user.dob = ""
        }
        
        if let user_gender = defaults.string(forKey: UserDefaltsKeys.KEY_USER_GENDER)
        {
            user.gender = user_gender
        }
        else
        {
            user.gender = ""
        }
        
        if let user_email = defaults.string(forKey: UserDefaltsKeys.KEY_USER_EMAIL)
        {
            user.email = user_email
        }else {
            user.email = ""
        }
        
        if let user_phone = defaults.string(forKey: UserDefaltsKeys.KEY_USER_PHONE)
        {
            user.phone = user_phone
        }else {
            user.phone = ""
        }
        
        if let user_alter_phone = defaults.string(forKey: UserDefaltsKeys.KEY_USER_ALTERNATE_PHONE)
        {
            user.alternate_phone = user_alter_phone
        }
        
        if let user_add1 = defaults.string(forKey: UserDefaltsKeys.KEY_USER_ADDRESS1)
        {
            user.address1 = user_add1
        }
        
        if let user_add2 = defaults.string(forKey: UserDefaltsKeys.KEY_USER_ADDRESS2)
        {
            user.address2 = user_add2
        }
        
        if let user_state = defaults.string(forKey: UserDefaltsKeys.KEY_USER_STATE)
        {
            user.state = user_state
        }
        
        if let user_city = defaults.string(forKey: UserDefaltsKeys.KEY_USER_CITY)
        {
            user.city = user_city
        }
        
        if let user_pincode = defaults.string(forKey: UserDefaltsKeys.KEY_USER_PINCODE)
        {
            user.pincode = user_pincode
        }
        
        if let user_country = defaults.value(forKey: UserDefaltsKeys.KEY_USER_COUNTRY_CODE) as? Int
        {
            user.country = user_country
        }
        
        if let isMobileVerified = defaults.value(forKey: UserDefaltsKeys.IS_MOBILE_VERIFIED) as? Bool
        {
            user.isMobileVerified = isMobileVerified
        }
        
        if let isEmailVerified = defaults.value(forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED) as? Bool
        {
            user.isEmailVerified = isEmailVerified
        }
        
        if let userFamilyCount = defaults.value(forKey: UserDefaltsKeys.FAMILY_MEMBERS_COUNT) as? Int
        {
            user.familyMembersCount = userFamilyCount
        }
        
        if let familyMemAllowed = defaults.value(forKey: UserDefaltsKeys.FAMILY_ALLOWED_COUNT) as? Int
        {
            user.familyMemAllowedToAdd = familyMemAllowed
        }
        
        if let exceedMsg = defaults.value(forKey: UserDefaltsKeys.FAMILY_EXCEED_MESSAGE) as? String
        {
            user.exceededMsgOfFamilyMem = exceedMsg
        }
        
        return user
    }
}


extension User {
    
    /// Encodes the user model with info and stores in user defaults
    func saveSerializedUserDetails() {
        if let encodedData = try? JSONEncoder().encode(self) {
            UserDefaults.standard.set(encodedData, forKey: UserDefaults.Keys.encodedUserDetails)
            print("Saved user details using encoding")
        }else {
            print("Unable to save user details using encoding")
        }
    }
    
    /// removes the saved encoded data of user details
    static func removeSavedUserDetails() {
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.encodedUserDetails)
    }
    
    /// Used to get store user details
    ///
    /// **NOTE**
    ///      This is newly written method to get details from UserDefaults which are stored using after serializing and ecoding the user model
    /// - Returns: User model object with stored details
    static func getUserProfileDetails() -> User? {
        if let encodedData = UserDefaults.standard.data(forKey: UserDefaults.Keys.encodedUserDetails),
            let user = try? JSONDecoder().decode(User.self, from: encodedData) {
            return user
        }
        return nil
    }
}
