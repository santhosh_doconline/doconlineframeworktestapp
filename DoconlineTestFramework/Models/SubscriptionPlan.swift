//
//  Subscription.swift
//  DocOnline
//
//  Created by Kiran Kumar on 05/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation


class SubscriptionPlan: Codable {
    
    var plan_id : String?
    var plan_amount : Int?
    var period : String?
    var interval : Int?
    var plan_name : String?
    var description : String?
    var currency : String?
    var total_count : Int?
    var cross_price : Int?
    var discount_text : String?
    var packages : String?
    var unit_amount : Int?
    var display_name : String?
    var display_period : String?
    var display_amount : Int?
    var display_color : String?
    var internal_order : Int?
    var planType: String?
    
    ///Subscribed Plan
    var subscribed : Bool?
    var type : String?
    var id : Int?
    var user_id : Int?
    var razorpay_sub_id : String?
    var name : String?
    var quantity : Int?
    var status : String?
    var paid_count : Int?
    var next_charge_date : String?
    var trail_ends_at : String?
    var ends_at : String?
    var created_at : String?
    var updated_at : String?
    var active:Bool?
    var featured : Bool?
    var iconURL : String!
    
    //invoice details
    var item_id : String?
    var gross_amount : Int?
    var tax_amount : Int?
    var net_amount : Int?
    var tax_inclusive : Bool?
    var commit:String? //dummy variable
    
    //subscription type details
    var subscribedType: String?
    var doUserCanUpgrate: Bool?
    var subscribedUserType: String?
    
    private enum CodingKeys: String, CodingKey {
        case plan_id
        case plan_amount
        case period
        case interval
        case plan_name
        case description
        case currency
        case total_count
        case cross_price
        case discount_text
        case packages
        case unit_amount
        case display_name
        case display_period
        case display_amount
        case display_color
        case internal_order
        case planType
        
        ///Subscribed Plan
        case subscribed
        case type
        case id
        case user_id
        case razorpay_sub_id
        case name
        case quantity
        case status
        case paid_count
        case next_charge_date
        case trail_ends_at
        case ends_at
        case created_at
        case updated_at
        case active
        case featured
        case iconURL
        
        //invoice details
        case item_id
        case gross_amount
        case tax_amount
        case net_amount
        case tax_inclusive
        case commit
        
        //only sub info
        case subscribedType
        case doUserCanUpgrate
        case subscribedUserType
    }
    
   ///
    ///Subscription Plans List
    init(id:String,amount:Int,period:String,interval:Int,plan_name:String,description:String,currency:String,total_count:Int,cross_price:Int,discount_text:String,packages:String,featured:Bool,displayName:String,displayPeriod:String,displayAmount:Int,displayColor:String,internal_order:Int) {
        self.plan_id = id
        self.plan_amount = amount
        self.period = period
        self.interval = interval
        self.plan_name = plan_name
        self.description = description
        self.currency = currency
        self.total_count = total_count
        self.cross_price = cross_price
        self.discount_text = discount_text
        self.packages = packages
        self.featured = featured
        self.display_name = displayName
        self.display_period = displayPeriod
        self.display_amount = displayAmount
        self.display_color = displayColor
        self.internal_order = internal_order
    }
    
    ///Invoice details
    init(id:String,item_id:String,name:String,description:String,amount:Int,unit_amount:Int,gross_amount:Int,tax_amount:Int,net_amount:Int,currency:String,type:String,tax_inclusive:Bool) {
         self.plan_id = id
         self.item_id = item_id
        self.name = name
         self.plan_amount = amount
        self.description = description
        self.unit_amount = unit_amount
        self.gross_amount = gross_amount
        self.tax_amount = tax_amount
        self.net_amount = net_amount
        self.currency = currency
        self.type = type
        self.tax_inclusive = tax_inclusive
    }
    
     ///Subscribed status
    init(id: Int,subscribed : Bool,type:String,user_id:Int,subscription_id:String,name:String,plan_id:String,quantity:Int,total_count:Int,interval:Int,status:String,paid_count:Int,charge_at:String,trail_ends_at:String,ends_at:String,created_at:String,updated_at:String,active:Bool,plan_name:String,description:String,currency:String,amount:Int,unit_amount:Int,internal_order:Int) {
        
        self.id = id
        self.subscribed = subscribed
        self.type = type
        self.user_id = user_id
        self.razorpay_sub_id = subscription_id
        self.name = name
        self.plan_id = plan_id
        self.quantity = quantity
        self.total_count = total_count
        self.interval = interval
        self.status = status
        self.paid_count = paid_count
        self.next_charge_date = charge_at
        self.trail_ends_at = trail_ends_at
        self.ends_at = ends_at
        self.created_at = created_at
        self.updated_at = updated_at
        self.active = active
        self.plan_name = plan_name
        self.description = description
        self.currency = currency
        self.plan_amount = amount
        self.unit_amount = unit_amount
        self.internal_order = internal_order
    }
    
    ///Pending Subscribed status
    init(id: Int,subscribed : Bool,type:String,user_id:Int,subscription_id:String,name:String,plan_id:String,quantity:Int,total_count:Int,interval:Int,status:String,paid_count:Int,charge_at:String,trail_ends_at:String,ends_at:String,created_at:String,updated_at:String,cross_price:Int,discount_text:String,packages:String,featured:Bool,displayName:String,displayPeriod:String,displayAmount:Int,displayColor:String,internal_order:Int) {
        
        self.id = id
        self.subscribed = subscribed
        self.type = type
        self.user_id = user_id
        self.razorpay_sub_id = subscription_id
        self.name = name
        self.plan_id = plan_id
        self.quantity = quantity
        self.total_count = total_count
        self.interval = interval
        self.status = status
        self.paid_count = paid_count
        self.next_charge_date = charge_at
        self.trail_ends_at = trail_ends_at
        self.ends_at = ends_at
        self.created_at = created_at
        self.updated_at = updated_at
        self.cross_price = cross_price
        self.discount_text = discount_text
        self.packages = packages
        self.featured = featured
        self.display_name = displayName
        self.display_period = displayPeriod
        self.display_amount = displayAmount
        self.display_color = displayColor
        self.internal_order = internal_order
        
    }
    
    ///Family pack plan details
    init(charge_at: String,ends_at:String,planName:String) {
         self.next_charge_date = charge_at
         self.ends_at = ends_at
         self.plan_name = planName
    }
    
    ///Only subscride info details
    init(isSubscribed: Bool,subscribedType:String,canUpgrade:Bool,userType:String) {
        self.subscribed = isSubscribed
        self.subscribedType = subscribedType
        self.doUserCanUpgrate = canUpgrade
        self.subscribedUserType = userType
    }
}


extension SubscriptionPlan {
    
    
    /// removes the saved encoded data of subcription details
    static func removeSavedPlanDetails() {
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.encodedCurrentPlanDetails)
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.encodedPendingPlanDetails)
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.familyPackPlanDetails)
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.onlySubscriptionInfo)
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.oneTimePlanDetails)
    }
    
    /// Encodes the subscription model with info and stores in user defaults
    func saveSubscriptionDetails(key:String) {
        if let encodedData = try? JSONEncoder().encode(self) {
            UserDefaults.standard.set(encodedData, forKey: key)
        }else {
            print("Cannot store subscription details")
        }
    }
    
    /// Used to get stored subscription details
    ///
    /// - Returns: Subscription model object with stored details
    static func getSavedSubscriptionPlanDetails(key:String) -> SubscriptionPlan?{
        if let encodedData = UserDefaults.standard.data(forKey: key),
            let plan = try? JSONDecoder().decode(SubscriptionPlan.self, from: encodedData) {
            return plan
        }
        return nil
    }
}
