//
//  KKMessage.swift
//  DocOnline
//
//  Created by Kiran Kumar on 05/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation

/**
 Custom JSQMessage class for chat message
 */
class KKMessage {
    
    var type : Int?
    var body : String?
    var senderId : String?
    var senderDisplayName : String?
    var date: Date!
    var image : UIImage?
    
    init() {
        
    }
    
    init(type:Int,senderId:String,senderDisplayName:String,date:Date,text:String) {
       self.body = text
       self.senderId = senderId
       self.senderDisplayName = senderDisplayName
       self.date = date
       self.type = type
    }
    
    init(type:Int,body:String,senderid:String,senderDisplayName:String) {
        self.type = type
        self.body = body
        self.senderId = senderid
        self.senderDisplayName = senderDisplayName
    }
    
    init(type:Int,body:String,senderid:String,senderDisplayName:String,image:UIImage) {
        self.type = type
        self.body = body
        self.senderId = senderid
        self.senderDisplayName = senderDisplayName
        self.image = image
    }
    
}
