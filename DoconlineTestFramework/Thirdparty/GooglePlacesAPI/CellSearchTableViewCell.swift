//
//  CellSearchTableViewCell.swift
//  DocOnline
//
//  Created by Mac on 30/08/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit

class CellSearchTableViewCell: UITableViewCell {
    @IBOutlet var lblsearch: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
