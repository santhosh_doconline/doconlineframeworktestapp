//
//  RefreshTimer.swift
//  DocOnline
//
//  Created by Kiran Kumar on 17/10/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

protocol RefreshTimerDelegate {
    ///Used to reload the dashboard tiles
    func refreshTheStatusForAppointmentTileIfAppointmentIsInFiveMin()
}

///Singleton class for Appointment count down timer
class RefreshTimer {
    
    var timer : Timer?
    var totalSeconds = 0
    var totalAppoinmtens = [Appointments]()
    var globalCountDownTimer : GlobalTimerViewController?
    var delegate : RefreshTimerDelegate?

    var askForPassword = false
    var askPasswordTimer : Timer?

   static var sharedInstance = RefreshTimer()


    ///Timer to check the appointments every 60 sec
    func runTimer() {
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            print("Started Global minute timer")
        }else {
            print("Timer is already running")
        }
    }

    ///timer func calls every 1 min
    @objc func updateTimer() {
        print("Global minute timer :\(totalSeconds + 1)")
//        RefreshTimer.sharedInstance.globalCountDownTimer?.timer?.invalidate()
//        RefreshTimer.sharedInstance.globalCountDownTimer?.timer = nil
//        RefreshTimer.sharedInstance.globalCountDownTimer?.dismiss(animated: true, completion: nil)
//        RefreshTimer.sharedInstance.globalCountDownTimer = nil
//        FloatingButtonWindow.sharedInstance.removeTimer()
//        getAppointments()
        updateTimerValues()
    }

    func stopTimer() {
        App.totalSecondsForAppointment = 0
        App.timerAppWithinTime = nil
        RefreshTimer.sharedInstance.globalCountDownTimer?.timer?.invalidate()
        RefreshTimer.sharedInstance.globalCountDownTimer?.timer = nil
        RefreshTimer.sharedInstance.globalCountDownTimer?.dismiss(animated: true, completion: nil)
        RefreshTimer.sharedInstance.globalCountDownTimer = nil
    }

    ///Checks the appointments within the 5 min and starts the timer
    func updateTimerValues() {
        delegate?.refreshTheStatusForAppointmentTileIfAppointmentIsInFiveMin()  //
        for appointment in App.tempAppointments {
            let dateOFAppointment = self.stringToDateConverter(date: appointment.scheduled_at!)
            let currentTime = Date()
            let totalSeconds = dateOFAppointment.timeIntervalSince(currentTime)

            //let minutes = Int(totalSeconds) / 60 % 60
            print("\n*****More \(totalSeconds ) to start appointment timer**********\n")
            if totalSeconds <= 300.0  &&  totalSeconds > 0.0 && appointment.status! == 1 && !isCallingViewPresenting {
                print("Appointment is in \(totalSeconds)")

                if App.timerAppWithinTime != nil && App.timerAppWithinTime!.id == appointment.id!  {
                    App.totalSecondsForAppointment = 0
                    App.totalSecondsForAppointment = Int(totalSeconds)
                }else
                {
                    App.totalSecondsForAppointment = 0
                    App.totalSecondsForAppointment = Int(totalSeconds)
                    App.timerAppWithinTime = appointment
                    print("More \(totalSeconds) to start global timer appointment id:\(App.timerAppWithinTime?.id)")
                }

                if RefreshTimer.sharedInstance.globalCountDownTimer == nil && !isCallingViewPresenting {
                    App.isGlobalTimerVisible = false
                    self.globalCountDownTimer = GlobalTimerViewController()
                }else
                {
                    print("Already timer instance is running")
                }

                return
            }
        }
    }


    ///stops the currently running appointment count down timer
    func stopGlobalTimer() {

        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }

        RefreshTimer.sharedInstance.globalCountDownTimer?.timer?.invalidate()
        RefreshTimer.sharedInstance.globalCountDownTimer?.timer = nil
        RefreshTimer.sharedInstance.globalCountDownTimer?.dismiss(animated: true, completion: nil)
        RefreshTimer.sharedInstance.globalCountDownTimer = nil
        FloatingButtonWindow.sharedInstance.removeTimer()
    }

    /**
     Method performs request with url for appointments fetch
     */
    func getAppointments() {
        if UserDefaults.standard.value(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && UserDefaults.standard.value(forKey: UserDefaltsKeys.TOKENTYPE) != nil  {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)

            let url = URL(string: AppURLS.URL_BookAppointment + "/upcoming")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken() , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        // AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }else {
                    if let response = response {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")

                        if httpResponse.statusCode == 200 {
                            do
                            {
                                if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {

                                    _ = jsonData.object(forKey: Keys.KEY_CODE) as? Int
                                    _ = jsonData.object(forKey: Keys.KEY_STATUS) as? String

                                    if let data = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {

                                        if let appData = data.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                            App.appointmentsList.removeAll()
                                            App.tempAppointments.removeAll()
                                            for singleData in appData {
                                                var doctorName = ""
                                                var doctorSpzation = ""

                                                if let id = singleData.object(forKey: Keys.KEY_ID) as? Int {
                                                    if let bookedFor = singleData.object(forKey: Keys.KEY_BOOKED_FOR) as? Int {
                                                        if let callType = singleData.object(forKey: Keys.KEY_CALL_TYPE) as? Int {
                                                            if let scheduled_at = singleData.object(forKey: Keys.KEY_SCHEDULED_AT) as? String {
                                                                if let status = singleData.object(forKey: Keys.KEY_STATUS) as? Int {
                                                                    if let userNotes = singleData.object(forKey: Keys.KEY_NOTES) as? String {

                                                                        if let doctor = singleData.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary {
                                                                            if let name = doctor.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                                                doctorName = name
                                                                            }

                                                                            if let specialization = doctor.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                                                doctorSpzation = specialization
                                                                            }
                                                                        }

                                                                        let dateOFAppointment = self.stringToDateConverter(date: scheduled_at)
                                                                        let currentTime = Date()
                                                                        let totalSeconds = dateOFAppointment.timeIntervalSince(currentTime)

                                                                        let minutes = Int(totalSeconds) / 60 % 60
                                                                        print("Appopintment Time is in min:\(minutes) total seconds:\(totalSeconds)")

                                                                          let appointment = Appointments(id: id, current_page: 0, scheduled_at: scheduled_at, calltype: callType, booked_for: bookedFor, notes: userNotes, from: 0, last_page: 0, nextpageurl: "", to: 0, total: 0, status: status, doctorName: doctorName, doctorAvatar: "", doctorSpecialisation: doctorSpzation, patientName: "", patientGender: "", patientAvatar: "",age:"")

//                                                                        let appointment = Appointments(id: id, current_page: 0, scheduled_at: scheduled_at, calltype: callType, booked_for: bookedFor, notes: userNotes, from: 0, last_page: 0, nextpageurl: "", to: 0, total: 0, status: status)

                                                                        if  status == 1 && totalSeconds > 0  {
                                                                            App.tempAppointments.append(appointment) //
                                                                            App.appointmentsList.append(appointment)
                                                                            print("Appointments for timer count:=>\(App.appointmentsList.count)")

                                                                            if totalSeconds > 300 {
                                                                                self.calculateTimeToShowRemainder(appointment: appointment)
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            DispatchQueue.main.async {
                                                self.delegate?.refreshTheStatusForAppointmentTileIfAppointmentIsInFiveMin()
                                            }
                                        }
                                    }
                                }
                            }catch let error
                            {
                                print("Received not-well-formatted JSON Appointments=>\(error.localizedDescription)")
                            }
                        }else {
                            print("Un authorized user")
                        }
                    }
                }
                }.resume()
        }else {
            print("User not logged in to get appointments")
        }
    }

    /**
     This method retunrns date from string
     - Returns: Date is returned with mentioned date format
     */
    func stringToDateConverter(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: date)

        dateFormatter.timeZone = NSTimeZone.local
        return date!
    }

    /**
     This method calculates the time in seconds of all appointments and shows the timer button if any appointment is under 5min
     - Parameter list : list takes the array of appointments
     */
    func getTotalSecondsFromAppointments(list:[Appointments]) {
        for appointment in list {
            if appointment.status! == 1 {
                let dateOFAppointment = stringToDateConverter(date: appointment.scheduled_at!)
                let currentTime = Date()
                let totalSeconds = dateOFAppointment.timeIntervalSince(currentTime)
                if totalSeconds > 0 {
                    App.listOfAppointmentSeconds.append(Int(totalSeconds))
                }
            }
        }

        App.listOfAppointmentSeconds.sort()

        for second in  App.listOfAppointmentSeconds {
            if second > 10 && second < 900 {
                App.totalSecondsForAppointment = second

                if FloatingButtonWindow.sharedInstance.rootViewController == nil {
                    self.globalCountDownTimer = GlobalTimerViewController()
                    // self.globalCountDownTimer?.button.addTarget(self, action: #selector(ViewController.floatingButtonWasTapped), for: .touchUpInside)
                }else {
                    FloatingButtonWindow.sharedInstance.removeTimer()
                    App.totalSecondsForAppointment = Int(second)
                    self.globalCountDownTimer = GlobalTimerViewController()
                }
            }
        }

    }

    /**
     Converts UTC date string to local string and date
     - Parameter dateString: takes string date format
     - Returns: date string and Date from given string
     */
    func convertDate(dateString :String) ->(String,Date?) {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormator.date(from: dateString)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let localDateString = dt == nil ? "" : dateFormator.string(from: dt!)
        let localDate = dateFormator.date(from: localDateString)
        return (localDateString,localDate)
    }

    /**
     Method calculates time and removes 5 min from the appointment time to show the remainder before appointment
     */
    func calculateTimeToShowRemainder(appointment:Appointments) {
        print("\(#function) called")
        let currentTime =  Date()
        let toTime = stringToDateConverter(date: appointment.scheduled_at)
        let seconds = toTime.timeIntervalSince(currentTime)
        let timeToshow = seconds - 300
        print("date should show loacl notification local:\(convertDate(dateString: appointment.scheduled_at).0) utc:\(appointment.scheduled_at)")
        let calendar = Calendar(identifier: .indian)
        if let toDate = convertDate(dateString: appointment.scheduled_at).1 , let reducedDate = calendar.date(byAdding: .minute, value: -5, to: toDate) ,let scheduledAt = appointment.scheduled_at{
            print("Reduced date:\(reducedDate)")
            let components = calendar.dateComponents(in: .current, from: reducedDate)
            let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)

            print("Time to show notification:\(timeToshow)")
            let selectedDate = scheduledAt
            let selectedTime = selectedDate.components(separatedBy: " ")

            let time = "\(Date().getFormattedDate(dateString: selectedDate)) at \(Date().getTimeInAMPM(date:selectedTime[1]))"
            schedulteAppointmentRemainder(timeInterVal: newComponents, time: time,appointmentID:appointment.id)
        }
    }

    /**
     Method setups actions for notification remainder
     */
    func setupActionsForNotification() {
        let dismissAction = UNNotificationAction(identifier: "DismissAction", title: "Dismiss", options: UNNotificationActionOptions.destructive)
        let viewAction = UNNotificationAction(identifier: "ViewAction", title: "View", options: UNNotificationActionOptions.foreground)
        let notificationCategory = UNNotificationCategory(identifier: "AlertCategory", actions: [dismissAction,viewAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([notificationCategory])
    }

    /**
     Method setups local notification remainder with time interval
     - Parameter timeInterVal: pass time or date components to schedule remainder
     - Parameter time: to show time in notification
     */
    func schedulteAppointmentRemainder(timeInterVal : DateComponents , time:String,appointmentID:Int) {
         let requestIdentifier = "AppointmentAlert\(appointmentID)"
         let alertCategory = "AlertCategory"

         var isScheduledAlready = false

         UNUserNotificationCenter.current().getPendingNotificationRequests { (notifications) in
            for notfication in notifications {
                if notfication.identifier == requestIdentifier && notfication.content.categoryIdentifier == alertCategory {
                    isScheduledAlready = true
                    print("##### Notification is already scheduled #######")
                    break
                }else {
                     isScheduledAlready = false
                     print("##### Notification is Not scheduled #######")
                }
            }
         }

        if !isScheduledAlready {
            setupActionsForNotification()
            print("\(#function) called--")
            let content = UNMutableNotificationContent()
            content.title = "Appointment remainder"
            content.body = "You have an appointment with DocOnline at \(time). Make sure you are logged in and internet connection is working properly. Thank You"
            content.categoryIdentifier = alertCategory
            content.userInfo = ["local_appointment_id":"\(appointmentID)"]
            content.sound = UNNotificationSound.default

            // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterVal, repeats: false)
            let trigger = UNCalendarNotificationTrigger(dateMatching: timeInterVal, repeats: false)


            let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)

            UNUserNotificationCenter.current().add(request) { (error) in
                if error != nil {
                    print("Notification scheduling error:\(error?.localizedDescription)")
                }else{
                    print("Appointment notification scheduled successfully")
                }
            }
        }
        //App.selectedSlotTime = ""
    }

}
