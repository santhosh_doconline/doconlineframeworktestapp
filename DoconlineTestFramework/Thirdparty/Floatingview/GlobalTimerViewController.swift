//
//  GlobalTimerViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 16/10/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit


protocol GlobalTimerTriggerDelegate {
    func timerTapped(_ sender: UIGestureRecognizer,app_id:Int)
}

///Appointment timer viewcontroller
class GlobalTimerViewController: UIViewController {

    ///Instance of UIButton
    private(set) var button: UIButton!

    ///total seconds of timer
    var totalSeconds = 100
    ///Instance for timer
    var timer : Timer?

    ///Timer to remove Appointment timer on long tap
    var timerToRemoveButton: Timer?
    ///total seconds to remove button
    var secToRemove = 0

    ///Instance fore delegate method
    var delegate : GlobalTimerTriggerDelegate?


    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    init() {
        super.init(nibName: nil, bundle: nil)
        FloatingButtonWindow.sharedInstance.windowLevel = UIWindow.Level(rawValue: CGFloat.greatestFiniteMagnitude)
        FloatingButtonWindow.sharedInstance.isHidden = false
        FloatingButtonWindow.sharedInstance.rootViewController = self
        NotificationCenter.default.addObserver(self, selector: #selector(GlobalTimerViewController.keyboardDidShow(note:)), name: UIResponder.keyboardDidShowNotification, object: nil)
    }

    // private let window = FloatingButtonWindow()

    /**
      Method runs the appointment timer with decreasing count down
     */
    func runTimer() {
        if timer == nil {
             self.timer =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GlobalTimerViewController.updateTimer), userInfo: nil, repeats: true)
        }
    }


    /**
       Method calls every second of timer called in `runTimer()`
     */
    @objc func updateTimer() {
        App.totalSecondsForAppointment -= 1

        timeString(time: TimeInterval( App.totalSecondsForAppointment))
    }

    /**
     Method calls every second. called in `updateTimer()` Method to update the timer values in UI
     */
    func timeString(time:TimeInterval)  {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        if time == 0 || time < 0 {
            self.button.stopBlink()
            self.button.setTitle(String(format:"%02i:%02i", minutes,seconds), for: UIControl.State.normal)
            App.totalSecondsForAppointment = 0
            App.timerAppWithinTime = nil
            RefreshTimer.sharedInstance.delegate?.refreshTheStatusForAppointmentTileIfAppointmentIsInFiveMin()
            RefreshTimer.sharedInstance.globalCountDownTimer?.timer?.invalidate()
            RefreshTimer.sharedInstance.globalCountDownTimer?.timer = nil
            RefreshTimer.sharedInstance.globalCountDownTimer?.dismiss(animated: true, completion: nil)
            RefreshTimer.sharedInstance.globalCountDownTimer = nil
            FloatingButtonWindow.sharedInstance.removeTimer()

        }else if time <= 30 {
           self.button.startBlink()
           self.button.setTitle(String(format:"%02i:%02i", minutes,seconds), for: UIControl.State.normal)
        }else {
          self.button.setTitle(String(format:"%02i:%02i", minutes,seconds), for: UIControl.State.normal)
        }
    }

    override func loadView() {
        print("Total seconds to run timer:\(App.totalSecondsForAppointment)")
        if App.totalSecondsForAppointment > 0  && App.totalSecondsForAppointment <= 300{


            let view = UIView()
            let button = UIButton(type: .custom)
            button.setTitle("00:00", for: UIControl.State.normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 11)
            button.backgroundColor = UIColor.red
            button.layer.borderColor = UIColor.white.cgColor
            button.layer.borderWidth = 1
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowRadius = 3
            button.layer.shadowOpacity = 0.8
            button.layer.shadowOffset = CGSize.zero
            button.sizeToFit()
            button.frame = CGRect(origin:  CGPoint(x: 10, y: 10), size: CGSize(width: 60, height: 60))
            button.layer.cornerRadius = button.layer.frame.size.width / 2

            let vwGrad = GradientView() //(*)
            vwGrad.frame = CGRect(x: 0.0, y: 0.0, width: button.frame.size.width, height: button.frame.size.height)
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            button.insertSubview(vwGrad, at: 0)

            button.clipsToBounds = true
            button.autoresizingMask = []
            view.addSubview(button)
            self.view = view
            self.button = button
            FloatingButtonWindow.sharedInstance.button = button

            let panner = UIPanGestureRecognizer(target: self, action: #selector(GlobalTimerViewController.panDidFire(panner:)))
            button.addGestureRecognizer(panner)
            let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(GlobalTimerViewController.longPressTapped(_:)))
            button.addGestureRecognizer(longGesture)

            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(GlobalTimerViewController.timerTapped(_:)))
            button.addGestureRecognizer(tapGesture)

            runTimer()
        }


    }

    /**
     Method called when the displaying timer tapped to show appointment details
     */
   @objc func timerTapped(_ sender: UIGestureRecognizer) {
        print("Apppointment id:\(App.timerAppWithinTime!.id!)")
      delegate?.timerTapped(sender, app_id: App.timerAppWithinTime!.id!)
    }

    override func viewDidAppear(_ animated: Bool) {
        print("Global timer \(#function) called")
           App.isGlobalTimerVisible = true
//        let dateOFAppointment = self.stringToDateConverter(date: (App.timerAppWithinTime?.scheduled_at!)!)
//        let currentTime = Date()
//        let totalSeconds = dateOFAppointment.timeIntervalSince(currentTime)
//
//        let minutes = Int(totalSeconds) / 60 % 60
//        App.totalSecondsForAppointment = Int(totalSeconds)
//        print("Global timer \(#function) called total seconds\(totalSeconds)")
//        if minutes <= 15 && minutes > 0 {
//            if timer != nil {
//                timer?.invalidate()
//                timer = nil
//            }
//            runTimer()
//        }
    }

    /**
     Method calls for `UIGestureRecognizer` Long press to remove the timer from UI
     */
   @objc func longPressTapped(_ sender: UIGestureRecognizer) {
        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")

            if self.timerToRemoveButton != nil {
                self.secToRemove = 0
                self.timerToRemoveButton?.invalidate()
                self.timerToRemoveButton = nil
            }
            UIView.animate(withDuration: 0.2, animations: {
                self.button.alpha = 1
            })
        }
        else if sender.state == .began {
            self.secToRemove = 0
            print("UIGestureRecognizerStateBegan.")
            UIView.animate(withDuration: 0.2, animations: {
                self.button.alpha = 0.80
            })
            runEndTimer()
        }
    }

    ///Not used
    func closeTimerAlert() {
//        let alert = UIAlertController(title:  "Dismiss Timer", message: "do you want to remove timer", preferredStyle: .alert)
//
//        let yes = UIAlertAction(title: "Dismiss Timer", style: .default) { (action) in
//            print("Remove Tapped")
//            FloatingButtonWindow.sharedInstance.removeTimer()
//            self.view.removeFromSuperview()
//            self.dismiss(animated: true, completion: nil)
//            if self.timer != nil {
//                self.timer?.invalidate()
//                self.timer = nil
//            }
//
//            App.totalSecondsForAppointment = 0
//            App.timerAppWithinTime = nil
//
//            RefreshTimer.sharedInstance.globalCountDownTimer?.timer?.invalidate()
//            RefreshTimer.sharedInstance.globalCountDownTimer?.timer = nil
//            RefreshTimer.sharedInstance.globalCountDownTimer?.dismiss(animated: true, completion: nil)
//            RefreshTimer.sharedInstance.globalCountDownTimer = nil
//            FloatingButtonWindow.sharedInstance.removeTimer()
//            print("Global instance : \(RefreshTimer.sharedInstance.globalCountDownTimer == nil ? true : false)")
//        }
//        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
//            print("cancel tapped")
//        }
//
//       alert.addAction(yes)
//        alert.addAction(cancel)
//        self.present(alert, animated: true, completion: nil)
    }


    /**
     Method calls from `longPressTapped(_ sender: UIGestureRecognizer)` to run timer to close the Timer displaying
     */
    func runEndTimer() {
        self.timerToRemoveButton =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GlobalTimerViewController.countTimer), userInfo: nil, repeats: true)
    }

    /**
     Method calls from `runEndTimer()` to calculate the seconds
     */
   @objc func countTimer() {
        secToRemove += 1
        closeButton(time: TimeInterval(secToRemove))
    }

    /**
     Method calls from `countTimer()`  to close the Timer displaying
     */
    func closeButton(time:TimeInterval)  {
        if time == 1 {
            UIView.animate(withDuration: 0.2, animations: {
                self.button.alpha = 1
            })
            self.button.stopBlink()
            closeTimerAlert()
        }else {
           print("Hold it")
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        snapButtonToSocket()
    }

    /**
       Pan gesture method to get the point where user taps and moves the timer button dynamically
     */
   @objc func panDidFire(panner: UIPanGestureRecognizer) {
        let offset = panner.translation(in: view)
        panner.setTranslation(CGPoint.zero, in: view)
        var center = button.center
        center.x += offset.x
        center.y += offset.y
        button.center = center

        if panner.state == .ended || panner.state == .cancelled {
            UIView.animate(withDuration: 0.3) {
                self.snapButtonToSocket()
            }
        }
    }

    /**
      Shows the timer on the top of keyboard
     */
   @objc func keyboardDidShow(note: NSNotification) {
        FloatingButtonWindow.sharedInstance.windowLevel = UIWindow.Level(rawValue: 0)
        FloatingButtonWindow.sharedInstance.windowLevel = UIWindow.Level(rawValue: CGFloat.greatestFiniteMagnitude)
    }

    /**
       Adds the sockets to the corners to move the timer button to nearest socket
     */
    private func snapButtonToSocket() {
        var bestSocket = CGPoint.zero
        var distanceToBestSocket = CGFloat.infinity
        let center = button.center
        for socket in sockets {
            let distance = hypot(center.x - socket.x, center.y - socket.y)
            if distance < distanceToBestSocket {
                distanceToBestSocket = distance
                bestSocket = socket
            }
        }
        button.center = bestSocket
    }

    /**
     Adds the sockets to the corners of the screen
     */
    private var sockets: [CGPoint] {
        let buttonSize = button.bounds.size
        let rect = view.bounds.insetBy(dx: 4 + buttonSize.width / 2, dy: 4 + buttonSize.height / 2)
        let sockets: [CGPoint] = [
            CGPoint(x: rect.minX, y: rect.minY),
            CGPoint(x: rect.minX, y: rect.maxY),
            CGPoint(x: rect.maxX, y: rect.minY),
            CGPoint(x: rect.maxX, y: rect.maxY)
        ]
        return sockets
    }

    deinit {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
           App.isGlobalTimerVisible = false
    }

}

///New window to show timer button on top of the views
class FloatingButtonWindow: UIWindow {

    ///Button instance
    var button: UIButton?

    ///Shared instance to access the class
    static var sharedInstance = FloatingButtonWindow()

    ///Not used
    func showAlert() {

    }

    /**
      removes the timer , window and clears the window object
     */
    func removeTimer() {
         FloatingButtonWindow.sharedInstance.isHidden = true
        if self.rootViewController != nil {
            self.button?.removeFromSuperview()
            self.removeFromSuperview()
             self.isHidden = true
            RefreshTimer.sharedInstance.globalCountDownTimer?.timer?.invalidate()
            RefreshTimer.sharedInstance.globalCountDownTimer?.timer = nil
            RefreshTimer.sharedInstance.globalCountDownTimer?.dismiss(animated: true, completion: nil)
            RefreshTimer.sharedInstance.globalCountDownTimer = nil
        }
    }

    init() {
        super.init(frame: UIScreen.main.bounds)
        backgroundColor = nil
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        guard let button = button else { return false }
        let buttonPoint = convert(point, to: button)
        return button.point(inside: buttonPoint, with: event)
    }


}
