////
////  MenuTableViewController.swift
////  AcceleratorSampleApp-Swift
////
////  Created by dev-3 on 10/07/17.
////
////
//
//import UIKit
//
//struct Section {
//    var name: String!
//    var items: [String]!
//    var image : UIImage!
//    var collapsed: Bool!
//    
//    init(name: String, items: [String],image:UIImage ,collapsed: Bool = false) {
//        self.name = name
//        self.items = items
//        self.collapsed = collapsed
//        self.image = image
//    }
//}
//
/////Not used
//class MenuTableViewController: UITableViewController {
//
//    
//    var images = [ UIImage(named:"home")!,
//                   UIImage(named:"payment_drawer")!,
//                   UIImage(named:"ask_question_drawer")!,
//                   UIImage(named:"pricing_drawer")!,
//                   UIImage(named:"profile_drawer")!,
//                   UIImage(named:"faq_drawer")!,
//                   UIImage(named:"about_us_drawer")!,
//                   UIImage(named:"contact_us_drawer")!,
//                   UIImage(named:"logout_drawer")!]
//    
//   // var names = ["HOME","MEDICATIONS","ALLERGIES","PAYMENTS","ASK A QUESTION","PRICING","PROFILE","FAQ","ABOUT US","CONTACT US","LOGOUT"]
//    var names = ["HOME","MEDICATIONS","ALLERGIES","PROFILE","LOGOUT"]
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
//        self.tableView.estimatedRowHeight = 45
//        self.tableView.backgroundColor = UIColor.black
//        self.tableView.rowHeight = UITableViewAutomaticDimension
//         tableView.selectRow(at:  IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.middle)
//        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
//    }
//    
//    
//    override func viewWillAppear(_ animated: Bool) {
//        self.tableView.reloadData()
//    }
//    
//
//    // MARK: - Table view data source
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//          return names.count
//    }
//
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
//        cell.textLabel?.text = names[indexPath.row]
//        cell.contentView.backgroundColor = UIColor.clear
//        cell.backgroundColor = UIColor.clear
//        cell.textLabel?.textColor = UIColor.white
//        cell.selectionStyle = .none
//       // cell.imageView?.image = images[indexPath.row]
//        return cell
//    }
//    
//    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
//        var destViewController : UIViewController!
//    
//        switch indexPath.row {
//        case 0:
//            destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryBoardIds.ID_HOME_VIEW_CONTROLLER)
//             sideMenuController()?.setContentViewController(destViewController)
//            break
//
//        case 1:
//            destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryBoardIds.ID_MEDICATIONS_VIEW)
//              sideMenuController()?.setContentViewController(destViewController)
//            break
//        case 2:
//            destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryBoardIds.ID_ALLERGIES_VIEW)
//             sideMenuController()?.setContentViewController(destViewController)
//            break
//           
//        case 3:
//             print("index 3 tapped")
//             destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryBoardIds.ID_PROFILE_MAIN_VIEW)
//             sideMenuController()?.setContentViewController(destViewController)
//            break
//            
//        case 4:
//                print("index 4 tapped")
//                let alert = UIAlertController(title: "Are you sure?", message: "you want to logout", preferredStyle: UIAlertControllerStyle.alert)
//                let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
//                    UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_NAME)
//                    UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_HAS_LOGIN_KEY)
//                    UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_HEALTH_STATUS)
//                    UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.TOKENTYPE)
//                    UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.ACCESS_TOKEN)
//                    UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
//                    UserDefaults.standard.set(false,forKey: UserDefaltsKeys.KEY_USER_STATUS)
//                    UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.MOBILE_NUMBER)
//                    
//                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                    let vc = storyBoard.instantiateViewController(withIdentifier: StoryBoardIds.ID_LOGIN_VIEW) as! LoginViewController
//                    self.present(vc, animated: true, completion: nil)
//                })
//                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
//                alert.addAction(yesAction)
//                alert.addAction(cancel)
//                self.present(alert, animated: true, completion: nil)
//            break
//
//        case 5:
//                print("index 5 tapped")
//            
//        case 6:
//            print("index 6 tapped")
//            destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryBoardIds.ID_PROFILE_MAIN_VIEW)
//            sideMenuController()?.setContentViewController(destViewController)
//            break
//            
//        case 7:
//             print("index 7 tapped")
//           
//        case 8:
//             print("index 8 tapped")
// 
//        case 9:
//             print("index 9 tapped")
//            
//        case 10:
//             print("index 10 tapped")
//           
//        default:
//            print("no index")
//        }
//        
//         hideSideMenuView()
//    }
//    
//    
//    
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
//
//protocol CollapsibleTableViewHeaderDelegate {
//    func toggleSection(header: CollapsibleTableViewHeader, section: Int)
//}
//
//class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
//    
//    var delegate: CollapsibleTableViewHeaderDelegate?
//    var section: Int = 0
//    
//    let titleLabel = UILabel()
//    let imageLabel = UIImageView()
//    
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//        contentView.addSubview(titleLabel)
//        contentView.addSubview(imageLabel)
//         addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(gestureRecognizer:))))
//        
//        
//        imageLabel.widthAnchor.constraint(equalToConstant: 25).isActive = true
//        imageLabel.heightAnchor.constraint(equalToConstant: self.contentView.bounds.size.height).isActive = true
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        imageLabel.translatesAutoresizingMaskIntoConstraints = false
//    }
//    
//    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        let views = [
//            "titleLabel" : titleLabel,
//            "arrowLabel" : imageLabel,
//            ]
//        contentView.addConstraints(NSLayoutConstraint.constraints(
//            withVisualFormat: "H:|-20-[arrowLabel]-[titleLabel]-20-|",
//            options: [],
//            metrics: nil,
//            views: views
//        ))
//        contentView.addConstraints(NSLayoutConstraint.constraints(
//            withVisualFormat: "V:|-[arrowLabel]-|",
//            options: [],
//            metrics: nil,
//            views: views
//        ))
//        contentView.addConstraints(NSLayoutConstraint.constraints(
//            withVisualFormat: "V:|-[titleLabel]-|",
//            options: [],
//            metrics: nil,
//            views: views
//        ))
//    }
//    
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    @objc func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
//        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
//            return
//        }
//        delegate?.toggleSection(header: self, section: cell.section)
//    }
//    
//    func setCollapsed(collapsed: Bool) {
//        // Animate the arrow rotation (see Extensions.swf)
//    }
//}

