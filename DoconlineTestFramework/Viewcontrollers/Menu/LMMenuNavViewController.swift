//
//  LMMenuNavViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import Firebase
//import LGSideMenuController


protocol MenuNavgationDelegate {
    func dashboardSelected()
    func appointmentsSelected()
    func profileSelected()
    func questionsSelected()
    func ordersSelected()
    func vitalsSelected()
    func myRecordsSelected()
    func familySelected()
    func settingsSelected()
    func billingsSelected()
    func subscriptionPlansSelected()
    func diagnosisHistorySelected()
    func blogsSelected()
    func checkForPassword()
}

protocol HomeVCDelegate {
    func updateAppsettings()
}

class LMMenuNavViewController: UIViewController {

    
    var listItems = ["Appointments","Chat History", "Vitals","Medical Records","Orders","Select a Plan","Billing History","Family","Profile","Diagnosis History","Blogs","Settings"]
    var listItemIcons = ["appointment","questions","Vitals","MyDocsBlack","orders","subscription_plan","billing","family","profile","diagnos","Blog","sett_icon"]
    
    @IBOutlet var lb_contactNumber: UILabel!
    @IBOutlet weak var docImg: UIImageView!
    @IBOutlet weak var bg_img: UIImageView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var customercare_logo: UIImageView!
    
    @IBOutlet weak var customerBtn: UIButton!
    static var menuDelegate: MenuNavgationDelegate?
    
    @IBOutlet var tableView: UITableView!
    var isFamilyCountReachedAlertShown = false
    
//    let contactNumber = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.CUSTOMER_CARE_NUMBER).stringValue ?? "+918822126126"
    let contactNumber = "+918822126126"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeViewController.homeVCDelegate = self
//        lb_contactNumber.isHidden = true
//        customercare_logo.isHidden = true
//        customerBtn.isHidden = true
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView(frame:CGRect.zero)
//        switch Theme.companyType{
//        case .betterplace:
//            self.docImg.image = Theme.titleImg
//        case .tataMotors:
//            let url = URL(string: App.titleLogo)
//            self.docImg.kf.setImage(with: url, placeholder:  UIImage(named: "DoctorLogo"), options: nil, progressBlock: nil, completionHandler: nil)
//            self.lb_contactNumber.textColor = UIColor.blue
//            print("inside viewdidload")
//        default:
//            self.docImg.image = Theme.titleImg
//            self.lb_contactNumber.textColor = UIColor.white
//        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeLogo), name: NSNotification.Name(rawValue: "TitleLogo"), object: nil)

    }
    
    @objc func changeLogo(){
        DispatchQueue.main.async {
//        print("logo path \(App.titleLogo)")
            
                
            switch  Theme.companyType{
            case .tataMotors:
                self.bannerView.setGradientBackgroundForBanner(colorTop: UIColor.white, colorBottom: UIColor.white)
                let url = URL(string: App.titleLogo)
                self.docImg.kf.setImage(with: url, placeholder:  UIImage(named: "DoctorLogo"), options: nil, progressBlock: nil, completionHandler: nil)
                self.lb_contactNumber.textColor = UIColor.black
                self.customercare_logo.image = UIImage(named: "CustomerCare")!.imageWithColor(tintColor: .black)
                print("inside change logo")
                break
            default:
                self.bannerView.setGradientBackgroundForBanner(colorTop: Theme.navigationGradientColor![0]!, colorBottom: Theme.navigationGradientColor![1]!)
                self.docImg.image = Theme.titleImg
                self.lb_contactNumber.textColor = UIColor.white
                self.customercare_logo.image = UIImage(named: "CustomerCare")!.imageWithColor(tintColor: .white)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        bg_img.image = Theme.minorBackgroungImg
//        bannerView.setGradBackground(colors: [Theme.navigationGradientColor![0]!, Theme.navigationGradientColor![1]!])
         if let appSettings = App.appSettings , let hotline = appSettings.hotline{
            self.lb_contactNumber.text = "\(hotline.hotlineName ?? "")\n\(hotline.hotlineNumber ?? "")"
        }else {
            self.lb_contactNumber.text = "Customer Care Number\n\(contactNumber)"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
        switch  Theme.companyType{
        case .tataMotors:
            self.bannerView.setGradientBackgroundForBanner(colorTop: UIColor.white, colorBottom: UIColor.white)
            let url = URL(string: App.titleLogo)
            self.docImg.kf.setImage(with: url, placeholder:  UIImage(named: "DoctorLogo"), options: nil, progressBlock: nil, completionHandler: nil)
            self.lb_contactNumber.textColor = UIColor.black
            self.customercare_logo.image = UIImage(named: "CustomerCare")!.imageWithColor(tintColor: .black)
            print("inside didappear")
        default:
            self.bannerView.setGradientBackgroundForBanner(colorTop: Theme.navigationGradientColor![0]!, colorBottom: Theme.navigationGradientColor![1]!)
            self.docImg.image = Theme.titleImg
            self.lb_contactNumber.textColor = UIColor.white
            self.customercare_logo.image = UIImage(named: "CustomerCare")!.imageWithColor(tintColor: .white)
        }
//        bannerView.setGradientBackground(colorTop: Theme.navigationGradientColor![0]!, colorBottom: Theme.navigationGradientColor![1]!)
        print("menu \(#function) called")
        }
    }

    
    /**
     Method used to check family members limit and show alert if exceeded
     */
    func checkFamilyMembersLimit() {
        AppSettings.getAppSettings {
            DispatchQueue.main.async {
                guard let appSettings = App.appSettings else { return }
                LMMenuNavViewController.menuDelegate?.checkForPassword()
                
                if let hotline = appSettings.hotline{
                    (appSettings.documentCategories ?? []).forEach{
                        $0.imageView = UIImageView()
                        if let url = URL(string:$0.iconUrl ?? "") {
                            $0.imageView.kf.setImage(with: url , placeholder: UIImage(named:"AddFamily"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                    }
                        
                    self.lb_contactNumber.text = "\(hotline.hotlineName ?? "")\n\(hotline.hotlineNumber ?? "")"
                }else {
                    self.lb_contactNumber.text = "Customer Care Number\n\(self.contactNumber)"
                }
                
                if let famliyInfo = appSettings.userFamilyLimitInfo , let isEnabled = famliyInfo.isMemberEnabled {
                    if isEnabled , (famliyInfo.familyMemberCount ?? 0) > (famliyInfo.familyMembersAllowed ?? 0) {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: famliyInfo.message ?? "", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                self.isFamilyCountReachedAlertShown = true
                                LMMenuNavViewController.menuDelegate?.familySelected()
                            })
                            alert.addAction(ok)
                            if !self.isFamilyCountReachedAlertShown {
                              self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }

  
    @IBAction func backTapped(_ sender: UIButton) {
       
    }
    
    @IBAction func customerCareTapped(_ sender: UIButton) {
        var numberToCall = ""
        if let appSettings = App.appSettings , let hotline = appSettings.hotline{
            numberToCall = "\(hotline.hotlineNumber ?? "")"
        }else {
            numberToCall = contactNumber
        }
        let removedHyphen = numberToCall.replacingOccurrences(of: "-", with: "")
        let trimmedSpaces = removedHyphen.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://\(trimmedSpaces)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (success) in
                print("Call action success:\(success)")
            })
        }else {
            print("Canot open:\(trimmedSpaces)")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LMMenuNavViewController: HomeVCDelegate {
    func updateAppsettings() {
        if !App.isIncomingCallRecieved {
            DispatchQueue.global(qos: .background).async {
                self.checkFamilyMembersLimit()
            }
        }
    }
}

extension LMMenuNavViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  self.listItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.Menu, for: indexPath) as! MenuNavifationTableViewCell
        
        cell.lb_menuName.text = self.listItems[indexPath.row]
        cell.iv_menuIcon.image = UIImage(named:self.listItemIcons[indexPath.row])
        
        cell.selectionStyle = .none
        
     
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("\(self.listItems[indexPath.row]) Menu selected")
            
        if App.getUserAccessToken().isEmpty {
            
        }
       
        let mainViewController = sideMenuController!
        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        

//        let navigationController = mainViewController.rootViewController as! MenuNavifationViewController
//        let viewController: UIViewController!
//
//        LMMenuNavViewController.menuDelegate?.dashboardSelected()
//        if navigationController.viewControllers.first is HomeViewController {
//            print("Already presenting dashboard")
//        }
//        else {
//            viewController = self.storyboard!.instantiateViewController(withIdentifier: StoryBoardIds.ID_HOME_VIEW_CONTROLLER)
//            navigationController.setViewControllers([viewController], animated: false)
//        }
        
        
        if indexPath.row == 0 {
            LMMenuNavViewController.menuDelegate?.appointmentsSelected()
        }else if indexPath.row == 1 {
            LMMenuNavViewController.menuDelegate?.questionsSelected()
        }else if indexPath.row == 2 {
            LMMenuNavViewController.menuDelegate?.vitalsSelected()
        }else if indexPath.row == 3 {
            LMMenuNavViewController.menuDelegate?.myRecordsSelected()
        }else if indexPath.row == 4 {
            LMMenuNavViewController.menuDelegate?.ordersSelected()
        }else if indexPath.row == 5 {
             LMMenuNavViewController.menuDelegate?.subscriptionPlansSelected()
        }else if indexPath.row == 6 {
              LMMenuNavViewController.menuDelegate?.billingsSelected()
        }else if indexPath.row == 7 {
             LMMenuNavViewController.menuDelegate?.familySelected()
        }else if indexPath.row == 8 {
            LMMenuNavViewController.menuDelegate?.profileSelected()
        }else if indexPath.row == 9 {
            LMMenuNavViewController.menuDelegate?.diagnosisHistorySelected()
        }else if indexPath.row == 10 {
            LMMenuNavViewController.menuDelegate?.blogsSelected()
        }else if indexPath.row == 11 {
            LMMenuNavViewController.menuDelegate?.settingsSelected()
        }
       

//            if navigationController.viewControllers.first is SettingsViewController {
//                print("Already presenting dashboard")
//            }
//            else {
//                viewController = self.storyboard!.instantiateViewController(withIdentifier: StoryBoardIds.ID_SETTINGS_VIEW)
//                navigationController.setViewControllers([viewController], animated: false)
//            }
            
          
        
    }
}






// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
