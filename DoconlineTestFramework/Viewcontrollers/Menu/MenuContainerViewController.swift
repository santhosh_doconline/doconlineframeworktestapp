//
//  MenuContainerViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import LGSideMenuController

public class MenuContainerViewController: LGSideMenuController {

    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        setup(type: 2)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
   
    
    private var type: UInt?
    
    func setup(type: UInt) {
        self.type = type

        if #available(iOS 10.0, *) {
            let _: UIBlurEffect.Style = .regular
        }
        else {
            let _: UIBlurEffect.Style = .light
        }
        
        leftViewPresentationStyle = .slideAbove
        leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(0.0, 0.0)
        rootViewCoverColorForLeftView = UIColor.clear
        let screenWidth = UIScreen.main.bounds.width * 0.7
        leftViewWidth = screenWidth
        isLeftViewStatusBarHidden = true
        
    }
    
    override public func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
     //   if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
       // }
    }
    
//    override var isLeftViewStatusBarHidden: Bool {
//        get {
//            return super.isLeftViewStatusBarHidden
//        }
//
//        set {
//            super.isLeftViewStatusBarHidden = newValue
//        }
//    }

   
    @IBAction func menuTapped(_ sender: UIBarButtonItem) {
      
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
