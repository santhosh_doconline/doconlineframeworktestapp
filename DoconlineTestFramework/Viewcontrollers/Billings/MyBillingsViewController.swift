//
//  MyBillingsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 07/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

///Subscribed payments billings
class MyBillingsViewController: UIViewController ,NVActivityIndicatorViewable{

    ///instance for on veiwload loads upcoming url
    private var firstPageUrl = ""
    
    ///if billings has next list url uses this instance
    private var nextPageUrl = ""
    
    ///called when scrolling of list ends and set bool value
    private var isScrollingFinished :Bool!
    
    ///Bool value to check list ended stop network request
    private var isLoadingListItems:Bool = true
    
    var myBillingsList = [MyBillings]()
    
    ///linear bar instance reference
    private let linearBar: LinearProgressBar = LinearProgressBar()
    
    /**
     linear bar configuration
     */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedBilling : MyBillings?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ///setting scrolling false to call to set firsPageUrl instance url
        isScrollingFinished = false
        
        //used to load first list of billing list
        firstPageUrl = AppURLS.URL_BILLINGS 
        tableView.tableFooterView = UIView()
        self.tableView.isHidden = true
        configureLinearProgressBar()
        getBillingsList()
        
    }

    /**
     Method performs request with url for Billing list fetching
     */
    func getBillingsList() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error!", message:AlertMessages.NETWORK_ERROR)
            return
        }
        
        if isLoadingListItems == false
        {
            //self.didShowAlert(title: "Sorry", message: "List ended total count is\(self.appointmentsList.count)")
            print("AppointmentList ended total count is\(self.myBillingsList.count)")
        }
        else
        {
            print("commitng")
            var productListUrl = ""
            
            if isScrollingFinished == false
            {
                productListUrl = firstPageUrl
                print("billings list url-->:\(productListUrl)")
            }
            else
            {
                productListUrl =   nextPageUrl //appointmentsList[0].next_page_url!
                print("billings list url-->:\(productListUrl)")
                
                if productListUrl.isEmpty == true {
                    print("No next page url")
                    print("Total billings count:==> \(self.myBillingsList.count)")
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.tableView.reloadData()
                    })
                    return
                }
            }
            
            //startAnimating()
            linearBar.startAnimation()
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let url = URL(string: productListUrl)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                       // self.stopAnimating()
                        self.linearBar.stopAnimation()
                        self.tableView.reloadData()
                        self.tableView.isHidden = true
                        self.didShowAlert(title: "", message: (error?.localizedDescription)!)
                        // AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                        
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling get appointments list:\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                if code == 200 && responseStatus == "success"
                                {
                                    var nxtPage = ""
                                    var from = 0
                                    var to = 0
                                    var path = ""
                                    
                                    let currentPage = data.object(forKey: Keys.KEY_CURRENT_PAGE) as? Int
                                   // let total = data.object(forKey: Keys.KEY_TOTAL) as! Int
                                 //   let last_page = data.object(forKey: Keys.KEY_LAST_PAGE) as! Int
                                    if let nepageurl = data.object(forKey: Keys.KEY_NEXT_PAGE_URL) as? String {
                                        nxtPage = nepageurl
                                        self.nextPageUrl = nepageurl
                                        print("Next page url ==> \(nepageurl)")
                                        self.isLoadingListItems = true
                                    }else {
                                        self.nextPageUrl = ""
                                        self.isLoadingListItems = false
                                    }
                                    
                                    
                                    if let frm = data.object(forKey: Keys.KEY_FROM) as? Int {
                                        from = frm
                                    }
                                    if let too = data.object(forKey: Keys.KEY_TO) as? Int {
                                        to = too
                                    }
                                    if let pth = data.object(forKey: Keys.KEY_PATH) as? String {
                                        path = pth
                                    }
                                    print("**  from:\(from) to:\(to) path :\(path)")
                                    
                                    if let appData = data.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                      
                                        for singleData in appData {
                                            var id = ""
                                            var entity = ""
                                            var invoice_number = ""
                                            var customer_id = ""
                                            var customer_name = ""
                                            var customer_email = ""
                                            var customer_contact = ""
                                            var order_id = ""
                                            var subscription_id = ""
                                            var payment_id = ""
                                            var status = ""
                                            var expiredby : Int32 = 0
                                            var issuedAt : Int32 = 0
                                            var paidAt : Int32 = 0
                                            var cancelledAt : Int32 = 0
                                            var expiredAt : Int32 = 0
                                            var grossAmount : Int16 = 0
                                            var taxAmount : Int = 0
                                            var amount : Int32 = 0
                                            var amountPaid : Int32 = 0
                                            var amountDue : Int32 = 0
                                            var currency = ""
                                            var subscribedPlans = [SubscriptionPlan]()
                                            var billingStart : Double = 0
                                            var billingEnd : Double = 0
                                            
                                            if let lid = singleData.object(forKey: Keys.KEY_ID) as? String {
                                                id = lid
                                            }
                                          
                                            if let lEntity = singleData.object(forKey: Keys.KEY_ENTITY) as? String {
                                                entity = lEntity
                                            }
                                            
                                            if let lInoviceNum = singleData.object(forKey: Keys.KEY_INVOICE_NUMBER) as? Int {
                                                invoice_number = "\(lInoviceNum)"
                                            }
                                            
                                            if let customerID = singleData.object(forKey: Keys.KEY_CUSTOMER_ID) as? String {
                                                customer_id = customerID
                                            }
                                            
                                            if let customerDetails = singleData.object(forKey: Keys.KEY_CUSTOMER_DETAILS) as? NSDictionary {
                                                if let customerName = customerDetails.object(forKey: Keys.KEY_NAME) as? String {
                                                    customer_name = customerName
                                                     print("Cust Name :\(customerName)")
                                                }
                                                
                                                if let customerEmail = customerDetails.object(forKey: Keys.KEY_EMAIL) as? String {
                                                    customer_email = customerEmail
                                                     print("Cust Email :\(customerEmail)")
                                                }
                                                
                                                if let customerContact = customerDetails.object(forKey: Keys.KEY_CONTACT) as? String {
                                                    customer_contact = customerContact
                                                }
                                                
                                            }
                                            
                                            if let lOrderID = singleData.object(forKey: Keys.KEY_ORDER_ID_U) as? String {
                                                order_id = lOrderID
                                                print("Order_id :\(order_id)")
                                            }
                                            
                                            if let lineItems =  singleData.object(forKey: Keys.KEY_LINE_ITEMS) as? [NSDictionary] {
                                                subscribedPlans = self.getPlans(line_tems: lineItems)
                                                print("Plans count:\(subscribedPlans.count)")
                                            }
                                            
                                            if let lSubscriptionID = singleData.object(forKey: Keys.KEY_SUBSCRIPTION_ID) as? String {
                                                subscription_id = lSubscriptionID
                                                print("subscription_id :\(subscription_id)")
                                            }
                                            
                                            if let lPaymentId = singleData.object(forKey: Keys.KEY_PAYMENT_ID) as? String {
                                                payment_id = lPaymentId
                                            }
                                            
                                            if let lPaystatus = singleData.object(forKey: Keys.KEY_STATUS) as? String {
                                                status = lPaystatus
                                            }
                                            
                                            if let lExpireBy = singleData.object(forKey: Keys.KEY_EXPIRED_BY) as? Int32 {
                                                expiredby = lExpireBy
                                            }
                                            
                                            if let lIssuedAt = singleData.object(forKey: Keys.KEY_ISSUED_AT) as? Int32 {
                                                issuedAt = lIssuedAt
                                            }
                                            
                                            if let lPaidAt = singleData.object(forKey: Keys.KEY_PAID_AT) as? Int32 {
                                                paidAt = lPaidAt
                                            }
                                            
                                            if let lCancelledAt = singleData.object(forKey: Keys.KEY_CANCELLED_AT) as? Int32 {
                                                cancelledAt = lCancelledAt
                                            }
                                            
                                            if let lExpiredAt = singleData.object(forKey: Keys.KEY_EXPIRED_AT) as? Int32 {
                                                expiredAt = lExpiredAt
                                            }
                                            
                                            if let lGrossAmount = singleData.object(forKey: Keys.KEY_GROSS_AMOUNT) as? Int16 {
                                                grossAmount = lGrossAmount
                                            }
                                            
                                            if let lTaxAmount = singleData.object(forKey: Keys.KEY_TAX_AMOUNT) as? Int {
                                                taxAmount = lTaxAmount
                                            }
                                            
                                            if let lAmount = singleData.object(forKey: Keys.KEY_AMOUNT) as? Int32 {
                                                amount = lAmount
                                            }
                                            
                                            if let lAmountPaid = singleData.object(forKey: Keys.KEY_AMOUNT_PAID) as? Int32 {
                                                amountPaid = lAmountPaid
                                            }
                                            
                                            if let lAmountDue = singleData.object(forKey: Keys.KEY_AMOUNT_DUE) as? Int32 {
                                                amountDue = lAmountDue
                                            }
                                            
                                            if let lCurrency = singleData.object(forKey: Keys.KEY_CURRENCY) as? String {
                                                currency = lCurrency
                                            }
                                            
                                            if let lBillingStart = singleData.object(forKey: Keys.KEY_BILLING_START) as? Double {
                                                billingStart = lBillingStart
                                                print("Billing start date:\(lBillingStart)")
                                            }
                                            
                                            if let lBillingEnd = singleData.object(forKey: Keys.KEY_BILLING_END) as? Double {
                                                billingEnd = lBillingEnd
                                                 print("Billing end date:\(lBillingEnd)")
                                            }
                                            
                                            var appliedCouponDiscountOTP = ""
                                            if let notes = singleData.object(forKey: Keys.KEY_NOTES) as? NSDictionary {
                                                if let planamount = notes.object(forKey: Keys.KEY_PLAN_AMOUNT) as? String {
                                                    amount = Int32(planamount) ?? amount
                                                }
                                                if let plandiscount = notes.object(forKey: Keys.KEY_DISCOUNT) as? String {
                                                    appliedCouponDiscountOTP = plandiscount
                                                }
                                            }
                                            
                                            let billing = MyBillings(id: id, entity: entity, customerId: customer_id, orderId: order_id, subscriptionId: subscription_id, subscriptionPlan: subscribedPlans, paymentId: payment_id, paymentStatus: status, issuedAt: issuedAt, paidAt: paidAt, cancelledAt: cancelledAt, expiredAt: expiredAt, grossAmount: grossAmount, taxAmount: taxAmount, amount: amount,amountPaid: amountPaid,amountDue: amountDue, currency: currency,billingStart:billingStart,billingEnd:billingEnd,customerName:customer_name,customerEmail:customer_email)
                                            print("Is appending url:\(nxtPage)")
                                            billing.appliedCouponDiscount = appliedCouponDiscountOTP
                                            self.myBillingsList.append(billing)
                                        }
                                        
                                    }
                                    print("Appointments count:\(self.myBillingsList.count)")
                                }else {
                                    print("Error while fetching data")
                                }
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                               // self.stopAnimating()
                                if self.myBillingsList.count != 0 {
                                 self.tableView.isHidden = false
                                }
                                self.linearBar.stopAnimation()
                            })
                        }
                        catch let error
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                                self.linearBar.stopAnimation()
                                if self.myBillingsList.count == 0 {
                                    self.tableView.isHidden = true
                                }
                            })
                            print("Received not-well-formatted JSON=>\(error.localizedDescription)")
                        }
                        
                    }
                }
                
            }).resume()
        }
    }
    
    ///used to parse the json response to get plans list
    /// - Paramter line_tems : takes the json array to parse
    /// - returns : Subscription plans array
    func getPlans(line_tems:[NSDictionary]) -> [SubscriptionPlan]{
        var plans = [SubscriptionPlan]()
        for item in line_tems {
              let planId = item.object(forKey: Keys.KEY_ID) as? String
              let amount = item.object(forKey: Keys.KEY_AMOUNT) as? Int
              let name = item.object(forKey: Keys.KEY_NAME) as? String
              let planDescription = item.object(forKey: Keys.KEY_DESCRIPTION) as? String
              let currency = item.object(forKey: Keys.KEY_CURRENCY) as? String
              let item_id = item.object(forKey: Keys.KEY_ITEM_ID) as? String
              let unitAmount = item.object(forKey: Keys.KEY_UNIT_AMOUNT) as? Int
              let grossAmount = item.object(forKey: Keys.KEY_GROSS_AMOUNT) as? Int
              let taxAmount = item.object(forKey: Keys.KEY_TAX_AMOUNT) as? Int
              let netAmount = item.object(forKey: Keys.KEY_NET_AMOUNT) as? Int
              let type = item.object(forKey: Keys.KEY_TYPE) as? String
              let taxInclusive = item.object(forKey: Keys.TAX_INCLUSIVE) as? Bool
            //  print("item Details : \(amount) plan Name:\(name) planDescrp:\(planDescription) ")
              let plan = SubscriptionPlan(id: planId ?? "" , item_id: item_id ?? "", name: name ?? "", description: planDescription ?? "", amount: amount ?? 0, unit_amount: unitAmount ?? 0, gross_amount: grossAmount ?? 0, tax_amount: taxAmount ?? 0, net_amount: netAmount ?? 0, currency: currency ?? "" , type: type ?? "", tax_inclusive: taxInclusive ?? false)
                plans.append(plan)
            
        }
        return plans
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        self.linearBar.stopAnimation()
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == StoryboardSegueIDS.ID_INVOICE_BILLING_DETAILS {
            if let destVC = segue.destination as? InvoiceDetailsViewController {
                destVC.billingDetails = selectedBilling
            }
        }
        
    }
    

}

extension MyBillingsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.myBillingsList.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.Billing, for: indexPath) as! MyBillingTableViewCell
        
        let billing = self.myBillingsList[indexPath.row]
        
        cell.lb_order_id.text = billing.orderId
        cell.lb_payment_status.text = billing.paymentStatus.uppercased()
        if billing.paymentStatus.lowercased().isEqual("paid") {
            cell.lb_payment_status.textColor = UIColor.init(hexString: "#32CD32")
        }else if billing.paymentStatus.lowercased().isEqual("issued"){
            cell.lb_payment_status.textColor = UIColor.init(hexString: "#fda64c")
        }else {
            cell.lb_payment_status.textColor = UIColor.init(hexString: "#b1d489")
        }
        
        cell.lb_issued_on.text = "Issued On : " + DateFormatter().timeSince(from:  self.getDateStringFromTimeStamp(timeStamp: Double(billing.issuedAt) ).0 as NSDate, chatDate: "" )
        var amount = String(billing.amount)
        amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
        
        var discount = billing.appliedCouponDiscount
        if !discount.isEmpty && !discount.isEqual("0") {
          discount.insert(contentsOf: ".", at: discount.index(discount.endIndex, offsetBy: -2))
        }
        let doubleAmount = Double(amount) ?? 0
        let doublediscount = Double(discount) ?? 0
        let removeedDiscountAmount = Double(doubleAmount - doublediscount)
        
        cell.lb_amount.text = String(format: "₹%.2f", removeedDiscountAmount)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBilling = self.myBillingsList[indexPath.row]
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_INVOICE_BILLING_DETAILS, sender: self)
    }
    
}

extension MyBillingsViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        if !decelerate {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= scrollView.contentSize.height {
                isScrollingFinished = true
                getBillingsList()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Scrolling did end decelerating method called")
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            isScrollingFinished = true
            getBillingsList()
        }
    }
}


