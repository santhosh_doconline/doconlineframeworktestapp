//
//  DoctorInfoViewController.swift
//  DocOnline
//
//  Created by kiran kumar Gajula on 06/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class DoctorInfoViewController: UIViewController {

    
    var doctorInfo: Doctor?
    var doctorRating = 0.0
    
    @IBOutlet weak var iv_doctorPic: UIImageView!
    @IBOutlet weak var qualification: UILabel!
    @IBOutlet weak var mciCode: UILabel!
    @IBOutlet weak var practitionerNumber: UILabel!
    @IBOutlet weak var ratings: UILabel!
    @IBOutlet weak var vw_rating: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        self.iv_doctorPic.layer.cornerRadius = self.iv_doctorPic.frame.size .width / 2
        self.iv_doctorPic.layer.borderColor = UIColor(hexString: StandardColorCodes.GREEN).cgColor
        self.iv_doctorPic.layer.borderWidth = 1
        self.iv_doctorPic.clipsToBounds = true
        
        self.iv_doctorPic.kf.setImage(with: URL(string:self.doctorInfo?.avatar_url ?? ""), placeholder: UIImage(named:"doctor_placeholder_nocircle"), options: nil, progressBlock: nil, completionHandler: nil)
        self.qualification.text = self.doctorInfo?.qualification
        self.practitionerNumber.text = self.doctorInfo?.doctorPractitionerNumber
        self.mciCode.text = self.doctorInfo?.mciCode
        self.ratings.text = "\(doctorRating)"
        self.vw_rating.rating = doctorRating
        print("Doctor rating:\(doctorRating)")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
