////
////  ChatHistoryListViewController.swift
////  DocOnline
////
////  Created by Kiran Kumar on 23/08/17.
////  Copyright © 2017 ConversionBug. All rights reserved.
////
//
//import UIKit
////import NVActivityIndicatorView
//
//class ChatHistoryListViewController: UIViewController ,NVActivityIndicatorViewable {
//
//
//    let linearBar: LinearProgressBar = LinearProgressBar()
//
//    @IBOutlet weak var tv_chatHistory: UITableView!
//    @IBOutlet weak var vw_chat_with_doctor: UIView!
//    @IBOutlet weak var lb_empty_status: UILabel!
//    @IBOutlet weak var startChatBtn: UIButton!
//
//    var pageListInfoModal = ChatHistory()
//    //var chats = [ChatHistory]()
//
//    ///Boolean to check the scrollig ended
//    var isScrollingFinished :Bool!
//    ///Boolean to check the more items to load in array
//    var isLoadingListItems:Bool = true
//
//    ///Search controller to search chat
//    let searchController = UISearchController(searchResultsController: nil)
//    ///filtered chats array
//    var filteredChats = [ChatHistory]()
//
//    ///selected doctor chat history
//    var selectedDoctor : ChatInfo?
//
//    var firstPageUrl = ""
//    var nextPageUrl = ""
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        configureLinearProgressBar()
//
//        isScrollingFinished = false
//        firstPageUrl = AppURLS.URL_Chat_connect
//
//        self.tv_chatHistory.tableFooterView = UIView(frame: CGRect.zero)
//      //  self.shadowEffect(views: [vw_chat_with_doctor])
//        //configureSearchController()
//
//        App.chats?.data?.removeAll()
//        self.getChatHistory(at: AppURLS.URL_Chat_connect)
//
//     //   NotificationCenter.default.addObserver(self, selector: #selector(ChatHistoryListViewController.getChatHistory), name: NSNotification.Name(rawValue:"ChatHistoryLoad"), object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatHistoryListViewController.connectToChatSession), name: NSNotification.Name(rawValue:"ConnectToChat"), object: nil)
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        startChatBtn.backgroundColor = Theme.buttonBackgroundColor
//        self.getUserState { (success,newuser) in
//            print("Finish fetching userstate : \(success)")
//        }
//    }
//
//    ///To configure linear loader
//    fileprivate func configureLinearProgressBar(){
//        linearBar.backgroundColor = UIColor.white
//        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
//        //linearBar.heightForLinearBar = 2
//    }
//
//
//    ///MARK:- search bar configuration methods
//    func configureSearchController() {
//        // Initialize and perform a minimum configuration to the search controller.
//        searchController.searchResultsUpdater = self
//        searchController.dimsBackgroundDuringPresentation = false
//        definesPresentationContext = true
//        searchController.hidesNavigationBarDuringPresentation = false
//        tv_chatHistory.tableHeaderView = searchController.searchBar
//
//        tv_chatHistory.tableFooterView =  UIView(frame: CGRect.zero)
//    }
//
//    ///Filter content for searching text in list
//    func filterContentForSearchText(searchText: String) {
////        filteredChats =  App.chats?.data.filter { chatInfo in
////            return (doctor.doctor_name!.lowercased().contains(searchText.lowercased()))
////        }
//        tv_chatHistory.reloadData()
//    }
//
//
//    ///Methods performs request to server and fetches the Chat history
//    @objc func getChatHistory(at page: String) {
//        if !NetworkUtilities.isConnectedToNetwork()
//        {
//            self.didShowAlert(title: "Network Error", message: "Internet Connection Failure")
//            //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: "Internet Connection Failure")
//            return
//        }
//
//        linearBar.startAnimation()
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//        let url = URL(string: page)
//        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
//        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
//        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
//        request.httpMethod = HTTPMethods.GET
//
//        session.dataTask(with: request, completionHandler: { (data, response, error) in
//            if error != nil
//            {
//                print("Error while fetching data\(String(describing: error?.localizedDescription))")
//                DispatchQueue.main.async {
//                    self.didShowAlert(title: "", message:  error!.localizedDescription)
//                    // AlertView.sharedInsance.showFailureAlert(title: "", message:  error!.localizedDescription)
//                    self.linearBar.stopAnimation()
//                    self.tv_chatHistory.isHidden = (App.chats?.data ?? []).isEmpty
//                    self.lb_empty_status.isHidden = !(App.chats?.data ?? []).isEmpty
//                }
//            }
//            else
//            {
//                if let data = data
//                {
//                    print("data =\(data)")
//                }
//                if let response = response
//                {
//                    print("url = \(response.url!)")
//                    print("response = \(response)")
//                    let httpResponse = response as! HTTPURLResponse
//                    print("response code = \(httpResponse.statusCode)")
//                    do
//                    {
//                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
//                            let statusCheck = self.check_Status_Code(statusCode: httpResponse.statusCode, data: jsonData)
//                            if statusCheck , let results = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary ,
//                                let chatData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted),
//                                let historyData = try? JSONDecoder().decode(ChatHistory.self, from: chatData){
//                                let chatDataList = (App.chats?.data ?? []) + (historyData.data ?? [])
//                                App.chats = historyData
//                                App.chats?.data = chatDataList
//                            }
//                        }
//
//                        //Main thread to update ui after fetching data
//                        DispatchQueue.main.async(execute: {
//                            self.linearBar.stopAnimation()
//                            //print("Total chats count:==> \(App.chats.count)")
//                            //App.chats.sort(by: { $0.id! > $1.id! })
//                            self.tv_chatHistory.isHidden = (App.chats?.data ?? []).isEmpty
//                            self.lb_empty_status.isHidden = !(App.chats?.data ?? []).isEmpty
//                            self.tv_chatHistory.reloadData()
//                        })
//                    }
//                    catch let error {
//                        print("Error while getting chat history:\(error)")
//                        DispatchQueue.main.async(execute:
//                            {
//                                self.tv_chatHistory.isHidden = (App.chats?.data ?? []).isEmpty
//                                self.lb_empty_status.isHidden = !(App.chats?.data ?? []).isEmpty
//                                print("Error:\(error.localizedDescription)")
//                                self.didShowAlert(title: "Sorry", message: "We couldn't process now. Please try again later")
//                                self.linearBar.stopAnimation()
//                        })
//                    }
//                }
//            }
//        }).resume()
//    }
//
//    ///Method performs request to the server for chat connection with doctor
//   @objc func connectToChatSession() {
//        if !NetworkUtilities.isConnectedToNetwork()
//        {
//            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//            return
//        }
//
//        startAnimating()
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//        let chatSession = AppURLS.URL_Chat_connect
//        print("Chat url:=> \(chatSession)")
//
//        let url = URL(string: chatSession)
//        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
//        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
//        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
//        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
//        request.httpMethod = HTTPMethods.POST
//        session.dataTask(with: request, completionHandler: { (data, response, error) in
//            if let error = error
//            {
//                DispatchQueue.main.async(execute: {
//                    self.stopAnimating()
//                    print("Error==> : \(error.localizedDescription)")
//                    //self.didShowAlert(title: "", message: error.localizedDescription)
//                })
//            }
//            if let data = data
//            {
//                print("data =\(data)")
//            }
//            if let response = response
//            {
//                print("url = \(response.url!)")
//                print("response = \(response)")
//                let httpResponse = response as! HTTPURLResponse
//                print("response code = \(httpResponse.statusCode)")
//                //if you response is json do the following
//                do
//                {
//                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
//
//                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
//
//                    if !errorStatus
//                    {
//                        // UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
//                        print("performing error handling chat session connect::\(resultJSON)")
//                        if httpResponse.statusCode == 402
//                        {
//                            if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
//                                if let allKeys = data.allKeys as? [String] {
//                                    if allKeys.contains(Keys.KEY_SUBSCRIPTION_ERROR) {
//                                        if let message = data.object(forKey: Keys.KEY_SUBSCRIPTION_ERROR) as? String {
//                                            DispatchQueue.main.async(execute: {
//                                                if App.didUserSubscribed && App.userSubscriptionType.lowercased() == PlanType.B2BPAID {
//                                                    self.didShowAlert(title: "" , message: message)
//                                                }else {
//                                                    let alert = UIAlertController(title: "Membership", message: message, preferredStyle: UIAlertController.Style.alert)
//                                                    let viewPlans = UIAlertAction(title: "Choose", style: .default, handler: { (action) in
//                                                        App.isFromView = FromView.ChatHistoryView
//                                                        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
//                                                    })
//                                                    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                                                    alert.addAction(cancel)
//                                                    alert.addAction(viewPlans)
//                                                    self.present(alert, animated: true, completion: nil)
//                                                }
//                                            })
//                                        }
//                                    }
//                                }
//                            }
//                        }else {
//                          if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
//                            if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
//                                DispatchQueue.main.async(execute: {
//                                     self.stopAnimating()
//                                    //if httpResponse.statusCode == 412 {
//                                       // self.showVerifyMobileMessage(message: mobile, fromView: FromView.ChatHistoryView)
//                                     self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.ChatHistoryView)
//                                   // }
//                                })
//                            }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
//                                DispatchQueue.main.async(execute: {
//                                    self.stopAnimating()
//                                    self.showVerifyMobileMessage(title: "Email Verification", message: email, segue: StoryboardSegueIDS.ID_VERIFY_EMAIL, tag: 2, fromView: FromView.ChatHistoryView)
//                                })
//                            }
//                          }
//                       }
//                    }
//                    else {
//                        print("Chat session response::\(resultJSON)")
//
//                        if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
//                            if let id = data.object(forKey: Keys.KEY_ID) as? Int {
//                                App.threadID = "\(id)"
//                                if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int {
//                                    App.user_id = "\(userid)"
//                                    print("Thread id==> \(App.threadID) userid:\(App.user_id)")
//                                    DispatchQueue.main.async(execute: {
//                                        self.stopAnimating()
//                                        //self.instantiate_to_view(withIdentifier: StoryBoardIds.ID_CHAT_NAV_VIEW)
//                                        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_START_CHAT, sender: self)
//                                    })
//                                }
//                            }
//                        }
//                    }
//                }catch let error{
//                    DispatchQueue.main.async(execute: {
//                        self.stopAnimating()
//                    })
//                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
//                    print("Received not-well-formatted JSON chat session connect: \(error.localizedDescription)")
//                }
//
//            }
//
//        }).resume()
//    }
//
//    ///action to perform new chat connection
//    @IBAction func chatWithADoctorTapped(_ sender: UIButton) {
//        if App.userSubscriptionType.lowercased().isEqual(PlanType.ONE_TIME) ||  App.userSubscriptionType.lowercased().isEqual(PlanType.B2BPAID){
//            self.didShowAlert(title: "", message: "You cannot use chat feature in current plan")
//            return
//        }
//
////        if App.isMobileVerified {
//            let alert = UIAlertController(title: "Chat", message: "Do you want to start session?", preferredStyle: UIAlertController.Style.alert)
//
//            let connect = UIAlertAction(title: "Start Now", style: UIAlertAction.Style.default) { (UIAlertAction) in
//                self.connectToChatSession()
//            }
//
//            let cancel = UIAlertAction(title: "No Thanks", style: UIAlertAction.Style.cancel, handler: nil)
//
//            alert.addAction(connect)
//            alert.addAction(cancel)
//            self.present(alert, animated: true, completion: nil)
//
////        }else {
////            print("**Not verified")
////            App.isFromView = FromView.ChatHistoryView
////            showVerifyMobileMessage(message: "", fromView: FromView.ChatHistoryView)
////        }
//
//    }
//
//    /**
//     Unwind segue action
//     */
//    @IBAction func unwindToChatHistory(segue:UIStoryboardSegue) {
//
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        DispatchQueue.main.async {
//            self.linearBar.stopAnimation()
//        }
//    }
//
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if segue.identifier == StoryboardSegueIDS.ID_CONVERSATION_VIEW {
//            let destVC = segue.destination as! ChatConversationViewController
//            destVC.doctor = selectedDoctor
//        }else  if segue.identifier == StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS {
//            if let destVC = segue.destination as? SubscriptionPlansViewController {
//                destVC.previousActionDelegate = self
//            }
//        }else if segue.identifier ==  StoryboardSegueIDS.ID_START_CHAT {
//            if let destVC = segue.destination as? ChatViewController {
//                destVC.delegate = self
//            }
//        }else if segue.identifier == StoryboardSegueIDS.ID_VERIFY_EMAIL {
//            let destVC = segue.destination as! EmailVerificationViewController
//            destVC.delegate = self
//        }
//    }
//
//
//    deinit {
//       // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ChatHistoryLoad"), object: nil)
//    }
//
//
//}
//
//extension ChatHistoryListViewController : EmailVerificationDelegate {
//    func didVerifyEmail(_ email: String, _ success: Bool) {
//        if success {
//            self.navigationController?.popViewController(animated: true)
//        }
//    }
//}
//
//extension ChatHistoryListViewController : ChatViewControllerDelegate {
//    func didEndChat() {
//        self.navigationController?.popViewController(animated: true)
//        App.chats?.data?.removeAll()
//        self.getChatHistory(at: AppURLS.URL_Chat_connect)
//    }
//}
//
//extension ChatHistoryListViewController : ContinueUserPreviousActionsDelegate {
//    func doUserPreviousActionIfFinishedSubscription() {
//        self.navigationController?.popViewController(animated:true)
//        self.connectToChatSession()
//    }
//}
//
//extension ChatHistoryListViewController : UITableViewDelegate , UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        if searchController.isActive && searchController.searchBar.text != "" {
////            return filteredChats.count
////        }
//
//        return (App.chats?.data ?? []).count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.CHAT, for: indexPath) as! ChatHistoryTableViewCell
//
//        let chatInfo = App.chats?.data?[indexPath.row]
//        cell.iv_doctor_pic.layer.cornerRadius = cell.iv_doctor_pic.frame.size.width / 2
//        cell.iv_doctor_pic.clipsToBounds = true
//
//        cell.lb_doctor_name.text = "MCI: \(chatInfo?.doctor?.mciCode ?? "")"
//        cell.lb_last_message.text = chatInfo?.latestMessage
//        cell.lb_qualification.text = chatInfo?.doctor?.qualification
//
//        let currentTime =  Date()
//        let toTime = stringToDateConverter(date: chatInfo?.updatedAt ?? "")
//        let calculatedSeconds = toTime.timeIntervalSince(currentTime)
//
//        let day = Int(calculatedSeconds) / 86400
////        let hours = Int(calculatedSeconds) % 86400 / 3600
////        let minutes = Int(calculatedSeconds) / 60 % 60
////        let seconds = Int(calculatedSeconds) % 60
//        print("Day::\(day)")
//
//        let time = (chatInfo?.updatedAt ?? "").components(separatedBy: " ")
//       // let dateFormatter = DateFormatter()
//        let stringCurrentDate = "\(currentTime)"
//        let separatedDate = stringCurrentDate.components(separatedBy: " ")
//        let todaysDate = getDayOfWeek("\(separatedDate[0]) \(separatedDate[1])")
//        let chatDate = getDayOfWeek(chatInfo?.updatedAt ?? "")
//        let dateOfChatSession = getFormattedDateAndTime(dateString: chatInfo?.updatedAt ?? "")
//       // print("Todays day:\(todaysDate) chatDay:\(chatDate) Date:\(doctor.updated_at!) indexpath:\(indexPath.row)")
//
//         cell.lb_message_time.text =  DateFormatter().timeSince(from: toTime as NSDate,chatDate:dateOfChatSession , numericDates: false)
//        cell.selectionStyle = .none
//
//        return cell
//    }
//
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let chatInfo = App.chats?.data?[indexPath.row]
//        let currentTime =  Date()
//        let toTime = stringToDateConverter(date: chatInfo?.updatedAt ?? "")
//        let calculatedSeconds = toTime.timeIntervalSince(currentTime)
//        let day = Int(calculatedSeconds) / 86400
//        let time = (chatInfo?.updatedAt ?? "").components(separatedBy: " ")
//         let stringCurrentDate = "\(currentTime)"
//        let separatedDate = stringCurrentDate.components(separatedBy: " ")
//        let todaysDate = getDayOfWeek("\(separatedDate[0]) \(separatedDate[1])")
//        let chatDate = getDayOfWeek(chatInfo?.updatedAt ?? "")
//
//      //  print("selected Index:\(indexPath.row) days:\(day) Todays day:\(todaysDate) chatDay:\(chatDate) Date:\(doctor.updated_at!)")
//
//       // }
//       // let doctor = self.chats[indexPath.row]
//        self.selectedDoctor = chatInfo
//        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONVERSATION_VIEW, sender: self)
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 94
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//
//
//}
//
//extension ChatHistoryListViewController : UIScrollViewDelegate {
//
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
//                                  willDecelerate decelerate: Bool) {
//        if !decelerate {
//            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
//            if bottomEdge >= scrollView.contentSize.height {
//                isScrollingFinished = true
//                if let nextPage = App.chats?.next_page_url {
//                   getChatHistory(at: nextPage)
//                }
//            }
//        }
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        print("Scrolling did end decelerating method called")
//        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
//        if bottomEdge >= scrollView.contentSize.height {
//            isScrollingFinished = true
//            if let nextPage = App.chats?.next_page_url {
//                getChatHistory(at: nextPage)
//            }
//        }
//    }
//}
//
//extension ChatHistoryListViewController : UISearchResultsUpdating {
//
//    func updateSearchResults(for searchController: UISearchController) {
//        filterContentForSearchText(searchText: searchController.searchBar.text!)
//    }
//
//}
//
//
//
