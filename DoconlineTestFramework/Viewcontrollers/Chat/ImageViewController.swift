//
//  ImageViewController.swift
//  DocOnline
//
//  Created by dev-3 on 13/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
import ImageSlideshow


class ImageViewController: UIViewController ,UIScrollViewDelegate{

    @IBOutlet weak var imageView: UIImageView!
    
    var image : UIImage!
    var imageURl = ""
    var images = [ImageSource]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if imageURl.isEmpty != true {
           self.imageView.kf.setImage(with: URL(string: imageURl )!)
         
        }else {
            if image != nil {
                self.imageView.image = image
            }
        }
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    

//    func productImageLoaderSetupOnLoadView() {
//        self.imageSlideView.setImageInputs(images)
//        self.imageSlideView.contentScaleMode = .scaleAspectFit
//        self.imageSlideView.slideshowInterval = 10
//        self.imageSlideView.zoomEnabled = true
//        self.imageSlideView.pageControlPosition = .insideScrollView
//        self.imageSlideView.activityIndicator = DefaultActivityIndicator(style: .whiteLarge, color: .black)
//        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ImageViewController.didTap))
//        imageSlideView.addGestureRecognizer(recognizer)
//        
//    }
//    
//    func didTap() {
//        let fullScreenController = imageSlideView.presentFullScreenController(from: self)
//         fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
//    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
