////
////  ChatViewController.swift
////  DocOnline
////
////  Created by Kiran Kumar on 21/08/17.
////  Copyright © 2017 ConversionBug. All rights reserved.
////
//
//import UIKit
//import JSQMessagesViewController
//import FirebaseDatabase
//import Firebase
////import FirebaseAuth
//import UserNotifications
//import SwiftMessages
//import SDWebImage
//import ImageSlideshow
////import BSImagePicker
//import Photos
//
//protocol ChatViewControllerDelegate {
//    func didEndChat()
//}
//
//class ChatViewController: JSQMessagesViewController  ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
//
//    ///linear top animation bar
//    let linearBar: LinearProgressBar = LinearProgressBar()
//
//    ///Image picker controller instance variable
//    let picker = UIImagePickerController()
//
//    ///reference for picked image
//    var pickedImage : UIImage!
//
//    //var messages = [JSQMessage]()
//   // var tempMessages = [JSQMessage]()
//
//    ///Outgoing message bubble color
//    lazy var outgoingBubble: JSQMessagesBubbleImage = {
//        let bubbleFactory = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleRegularTailless(), capInsets: .zero)
//        return  bubbleFactory!.outgoingMessagesBubbleImage(with: UIColor(hexString: StandardColorCodes.OUTGOING_CHAT_BACKGROUND))
//    }()
//
//    ///message bubble color of incoming e8e8e8
//    lazy var incomingBubble: JSQMessagesBubbleImage = {
//        return  JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor(hexString: StandardColorCodes.INCOMING_CHAT_BACKGROUND))
//    }()
//
//    ///user id instance declaration
//    var userid = "user"
//    ///user name instance declaration
//    var userName = ""
//
//    ///doctor name instance declaration
//    var doctorName = ""
//    ///user avatar instance declaration
//    var user_avatar = ""
//    ///doctor avatar instance declaration
//    var doctor_avatar = ""
//    var doctorRating = 0.0
//    ///doctor id instance declaration
//    var doctor_id = ""
//
//    var doconlineDoctor: Doctor?
//    var connectedUser: User?
//
//    ///chat connection status message
//    var chatConnectionStatus = "Please wait! we're connecting you to doctor"
//
//    ///instance audio player declaration
//    var audioPlayer = AVAudioPlayer()
//
//    ///timer declared
//    var timer : Timer?
//
//    ///total seconds declaration for idle time out
//    var totalSeconds = 300
//    ///attached images url array
//    var chatImages = [String]()
//    ///messages array for only message
//    var chatMessages = [String]()
//    ///instance to refer the selected image in chat messages
//    var selectedImage : UIImage!
//
//    ///attached images to show in chat
//    var selectedImages = [ImageSource]()
//
//    ///custom message array for chat
//    var defaultMessages = [KKMessage]()
//    ///instance array for asyncronous image loading
//    var kfSource = [KingfisherSource]()
//    ///instance to show selected image in full size
//   // var imageView : ImageSlideshow = ImageSlideshow()
//
//  //  let refrenece =  Database.database().reference().child(App.ChatType).child("threads").child(App.threadID)
//  //  var messagesReference = Database.database().reference().child(App.ChatType).child("threads").child(App.threadID).child("messages")
//
//    ///Delegate instance
//    var delegate : ChatViewControllerDelegate?
//
//    //Network Stats
//    var lbl_stats:UILabel?
//    var btnRefresh: UIButton?
//    var imgRefresh:UIImageView?
//
//    @objc var measurer : NSTMeasurer!
//
//    let selectorMax : String = "maxDownloadSpeed"
//    let selectorAvg : String = "averageDownloadSpeed"
//    let selectorCurrent : String = "currentDownloadSpeed"
//
//    var currentSpeed : Double = 0.0
//    var maxSpeed : Double = 0.0
//
//    var isCurrentSpeed = true
//    var canStartPinging = false
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        //Ask permissions
//        didAskForCameraPermission()
//        didAskForPhotoLibraryPermission()
//
//        ///instantiation of database reference
//        App.mainReference = App.refrenece.child(App.ChatType).child("threads").child(App.threadID)
//
//       // setupBackgroundImage()
//        configureLinearProgressBar()
//
//        print("User_id_is==>\(userid) name:\(userName)")
//        senderId = "user"
//        senderDisplayName = userName
//
//        ///chat view  setup
//       // inputToolbar.contentView.leftBarButtonItem = nil
//        inputToolbar.contentView.textView.delegate = self
//        inputToolbar.contentView.textView.layer.borderColor = UIColor.clear.cgColor
//        inputToolbar.contentView.textView.placeHolder = "Your message"
//        inputToolbar.contentView.textView.backgroundColor = UIColor.clear
//        inputToolbar.contentView.textView.isUserInteractionEnabled = false
//        inputToolbar.backgroundColor = UIColor.white
//        inputToolbar.contentView.backgroundColor = UIColor.white
//
//        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
//        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
//        automaticallyScrollsToMostRecentMessage = true
//
//        let rightButton = UIButton(frame: CGRect.zero)
//        let sendImage = UIImage(named: "SendMessage")
//        rightButton.setImage(sendImage, for: .normal)
//        rightButton.setTitle("", for: .normal)
//        inputToolbar.contentView.rightBarButtonItemWidth = CGFloat(25)
//        inputToolbar.contentView.rightContentPadding = 2
//        inputToolbar.contentView.rightBarButtonItem = rightButton
//
//        let leftButton = UIButton(frame: CGRect.zero)
//        let attachImage = UIImage(named: "Attach")
//        leftButton.setImage(attachImage, for: .normal)
//        leftButton.setTitle("", for: .normal)
//        self.inputToolbar.contentView.leftBarButtonItemWidth = CGFloat(22)
//        inputToolbar.contentView.leftContentPadding = 10
//        inputToolbar.contentView.leftBarButtonItem = leftButton
//        inputToolbar.contentView.leftBarButtonItem.isEnabled = false
//
//        ///Adding observer for push notification
//       // NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.notificatonArrived(notification:)), name: NSNotification.Name(rawValue: "ChatThreadClosed"), object: nil)
//
//      //  NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.notificatonArrived(notification:)), name: .UIApplicationDidBecomeActive, object: nil)
//
//        readMessagesFromDatabase()
//        addGestureRecogniser()
//
////        AppDelegate.shared.chatEndDelegate = self
//
//        if lbl_stats == nil
//        {
//            topContentAdditionalInset = 50.0
//            let screenWidth = UIScreen.main.bounds.size.width
//            lbl_stats = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: (screenWidth - 55.0), height: 50))
//            lbl_stats!.text = ""
//            lbl_stats!.textColor = self.hexStringToUIColor(hex: "FF0000")
//            lbl_stats!.font = UIFont.systemFont(ofSize: 14)
//            lbl_stats!.textAlignment = .right
//            lbl_stats!.backgroundColor = UIColor.white
//            self.view.addSubview(lbl_stats!)
//
//            imgRefresh = UIImageView.init(frame: CGRect.init(x: (lbl_stats!.frame.size.width) + (lbl_stats!.frame.origin.x) + 5, y: 10, width: 30, height: 30))
//            imgRefresh!.image = UIImage.init(named: "Refresh")
//            imgRefresh?.isUserInteractionEnabled = false
//            self.view.addSubview(imgRefresh!)
//
//            btnRefresh = UIButton.init(frame: CGRect.init(x: (lbl_stats!.frame.size.width) + (lbl_stats!.frame.origin.x) - 50.0, y: 0, width: (screenWidth - ((lbl_stats!.frame.size.width) + (lbl_stats!.frame.origin.x) - 50.0 - 10.0)), height: 50))
//            btnRefresh!.setTitle("", for: .normal)
//            btnRefresh!.addTarget(self, action:#selector(self.testSpeed), for: .touchUpInside)
//            btnRefresh!.isEnabled = true
//            self.view.addSubview(btnRefresh!)
//            DispatchQueue.main.async {
//                self.testSpeed()
//            }
//            /*
//            let pinger = SimplePing(hostName: "www.apple.com")
//            pinger.delegate = self
//            pinger.start()
//
//            repeat {
//                if (canStartPinging) {
//                    pinger.send(with: nil)
//                }
//                RunLoop.current.run(mode: RunLoop.Mode.default, before: (NSDate.distantFuture as NSDate) as Date)
//            } while(pinger != nil)*/
//        }
//
//    }
//
//    /**
//       Setups the button of info to veiw doctor details on tap
//     */
//    func setUpUI(doctorAvatar:String) {
//        let doctorImageView = UIImageView()
//        doctorImageView.kf.setImage(with:  URL(string: doctorAvatar), placeholder: UIImage(named:"Doctor-placeholder"), options: [], progressBlock: nil) { (image, error, cache, url) in
//
//            doctorImageView.frame = CGRect(x:0,y: 0,width :30,height: 30)
//            doctorImageView.contentMode = .scaleAspectFit
//            doctorImageView.image = image
//            doctorImageView.layer.cornerRadius = doctorImageView.bounds.size.width / 2.0
//            doctorImageView.layer.masksToBounds = true
//
//            let button = UIButton.init(type: UIButton.ButtonType.infoLight)
//           // button.setBackgroundImage(doctorImageView.image, for: UIControlState.normal)
//            button.setTitle("", for: UIControl.State.normal)
//            button.addTarget(self, action: #selector(ChatViewController.doctorNameTapped), for: UIControl.Event.touchUpInside)
//           // button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//            //button.layer.cornerRadius = button.bounds.size.width / 2.0
//           // button.clipsToBounds = true
//            let barButton = UIBarButtonItem(customView: button)
//           // barButton.setBackgroundImage(doctorImageView.image, for: UIControlState.normal, barMetrics: UIBarMetrics.default)
//
//            self.navigationItem.rightBarButtonItems = [barButton]
//        }
//    }
//
//    ///Not used
//    func initUI(doctorAvatar:String,doctorName:String) {
//
//        let rect:CGRect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 64, height: 64))
//
//        let titleView:UIView = UIView.init(frame: rect)
//        /* image */
//        let image_view:UIImageView = UIImageView()
//        image_view.kf.setImage(with: URL(string: doctorAvatar))
//        image_view.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
//        image_view.center = CGPoint.init(x: titleView.center.x, y: titleView.center.y - 10)
//        image_view.layer.cornerRadius = image_view.bounds.size.width / 2.0
//        image_view.layer.masksToBounds = true
//        titleView.addSubview(image_view)
//
//        /* label */
//        let label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 31, width: 64, height: 24))
//        label.text = doctorName
//        label.textColor = UIColor.white
//        label.font = UIFont.systemFont(ofSize: 11)
//        label.textAlignment = .center
//        titleView.addSubview(label)
//
//        let height: CGFloat = 16
//        let bounds = self.navigationController!.navigationBar.bounds
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)
//
//        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.doctorNameTapped))
//
//        titleView.isUserInteractionEnabled = true
//        titleView.addGestureRecognizer(recognizer)
//
//        self.navigationItem.titleView = titleView
//
//    }
//
//
//    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.count
//        stopTimer()
//        print("Charecters count:\(numberOfChars)")
//        return numberOfChars < 500
//    }
//
//    /**
//      Method used to open camera
//     */
//    func openCamera()
//    {
//
//        if !self.checkCameraPermission()
//        {
//            //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
//            self.didShowAlertForDeniedPermissions( message: "Please provide access for the camera to upload pictures")
//            return
//        }
//
//
//        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
//        {
//            picker.sourceType = UIImagePickerController.SourceType.camera
//            self .present(picker, animated: true, completion: nil)
//        }
//        else
//        {
//            openGallary()
//        }
//    }
//
//    /**
//       Method used to open gallery
//     */
//    func openGallary()
//    {
//        if !self.checkLibraryPermission()
//        {
//            // self.didShowAlert(title: "Access denied!", message: "Please provide access to upload pictures from your Gallery")
//            self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
//            return
//        }
//
//        openBSImagePicker()
//
////        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
////        if UIDevice.current.userInterfaceIdiom == .phone
////        {
////            self.present(picker, animated: true, completion: nil)
////        }
//    }
//
//    func openBSImagePicker() {
//        let imgPickerView = BSImagePickerViewController()
//            imgPickerView.maxNumberOfSelections = 1
//        self.bs_presentImagePickerController(imgPickerView, animated: true, select: nil, deselect: nil, cancel: nil, finish: { (assets) in
//            for asset in assets {
//                let options = PHImageRequestOptions()
//                options.deliveryMode = .opportunistic
//                options.isSynchronous = true
//                options.resizeMode = .fast
//
//                let imageManager = PHImageManager.default()
//
//                imageManager.requestImage(for: asset, targetSize: CGSize(width:CGFloat(640), height:CGFloat(480)), contentMode: .aspectFill, options: options, resultHandler: { (resultThumbnail, resultDic) in
//                    if let thumbNail = resultThumbnail {
//                        self.pickedImage = thumbNail
//                    }
//                })
//            }
//
//            let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/message"
//            DispatchQueue.main.async {
//               self.linearBar.startAnimation()
//            }
//
//            DispatchQueue.global(qos: .userInitiated).async {
//                self.postMessageToServer(body: "", urlString: urlString, httpMethod: HTTPMethods.POST, type: 4)
//            }
//
//        }, completion: nil) { (count) in
//            print("Max count reached:\(count)")
//            imgPickerView.didShowAlert(title: "", message: "You can select a maximum of 1 image")
//        }
//    }
//
////Mark:- Image picker delegate methods
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
////        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
////        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((image), 1)!)
////        let imageSize: Int = imgData.length
////        let imageSizeInMB = Double(imageSize) / 1024.0 / 1024.0
////        print("size of image in MB: %f ", Double(imageSize) / 1024.0 / 1024.0)
////
////        if imageSizeInMB > 8 {
////          self.didShowAlert(title: "", message: "Image size should be less than 8MB")
////         //  AlertView.sharedInsance.showInfo(title: "", message: "Image size should be less than 8MB")
////            return
////        }
//
////        let mediaImage  =  JSQPhotoMediaItem(image: self.pickedImage)
////        if let mediaMessage = JSQMessage(senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: Date(), media: mediaImage ) {
////            App.messages.append(mediaMessage)
////            self.finishSendingMessage()
////            self.stopTimer()
////        }
//
//        DispatchQueue.main.async {
//            self.dismiss(animated: true) {
//                // Local variable inserted by Swift 4.2 migrator.
//                let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
//
//                guard let pimage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else { return }
//                self.pickedImage = pimage
//                let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/message"
//                self.linearBar.startAnimation()
//                DispatchQueue.global(qos: .userInitiated).async {
//                     self.postMessageToServer(body: "", urlString: urlString, httpMethod: HTTPMethods.POST, type: 4)
//                }
//            }
//        }
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        print("Image picker canceled")
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    /**
//       Method sets background image for chatting view
//     */
//    func setupBackgroundImage() {
//        let imgBackground:UIImageView = UIImageView(frame: self.view.bounds)
//        imgBackground.image = UIImage(named: "chat_background")
//        imgBackground.contentMode = UIView.ContentMode.scaleAspectFill
//        imgBackground.clipsToBounds = true
//        self.collectionView?.backgroundView = imgBackground
//    }
//
//    /**
//     Method used to play sound file
//     */
//    func playSound(fileName:String) {
//        let soundURL = URL(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: "mp3")!)
//        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
//            audioPlayer.prepareToPlay()
//            audioPlayer.play()
//        }catch let error {
//            print("error while playing sound:\(error.localizedDescription)")
//        }
//        print("Sound url:\(soundURL)")
//    }
//
//
//    /**
//     Method used to add gesture recognizer for view
//     */
//    func addGestureRecogniser() {
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.stopTimer))
//        self.view.addGestureRecognizer(gesture)
//
//    }
//
//    /**
//     Method used to stop running timer and start again
//     */
//     @objc func stopTimer() {
//        print("Stoping timer")
//        if self.timer != nil {
//            self.timer?.invalidate()
//            self.totalSeconds = 300
//            runTimer()
//        }
//    }
//
//    /**
//       Method used to instatiate timer with 1 sec repeat
//     */
//    func runTimer() {
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ChatViewController.updateTimer), userInfo: nil, repeats: true)
//    }
//
//    /**
//      Method used to update timer with decreasing seconds
//     */
//   @objc func updateTimer()
//    {
//        totalSeconds -= 1
//        timeString(time: TimeInterval(totalSeconds))
//    }
//
//    /**
//     Method checks if seconds becomes zero an @objcd performs network call to close chat
//     */
//    func timeString(time:TimeInterval)  {
//
//        if totalSeconds <= 0 {
//            let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/close"
//            self.postMessageToServer(body: "", urlString: urlString, httpMethod: HTTPMethods.PATCH, type: 3)
//            print("** Times up exit chat session **")
//        }else {
//            //print("Performing timer \(totalSeconds)")
//        }
//    }
//
//    /**
//      Method calls when push notification arrives
//     */
//   @objc func notificatonArrived(notification: NSNotification) {
//    print("Thread id::=>\(App.threadID) closig id:\( App.closingThreadId) func:=\(#function)")
//        if App.closingThreadId == App.threadID {
//            var message = ""
//
//            if App.chatClosedBy == "0" {
//                message = " due to no doctor is available"
//            }else {
//                message = " by doctor"
//            }
//
//            print("Message::=>\(message)")
//
//            DispatchQueue.main.async {
//                self.closing_alert(message: message)
//            }
////           let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/close"
////           self.postMessageToServer(body: "", urlString: urlString, httpMethod: HTTPMethods.PATCH, type: 3)
//        }else {
//             print("Thread is not equal")
//        }
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//         super.viewDidAppear(animated)
//        print("\(#function) called")
//        if self.doctorName.isEmpty == true {
//            self.showStatusLine(color: UIColor.orange)
//        }
//
//        if let doctor = self.doconlineDoctor {
//            self.navigationItem.setNewTitle("MCI: \(self.doconlineDoctor?.mciCode ?? "")", subtitle: self.doconlineDoctor?.qualification ?? "")
//           // self.navigationItem.setChatTitleView(with: "MCI: \(doctor.mciCode ?? "")", subtitle: doctor.qualification ?? "")
//        }
//
//        if self.timer != nil {
//            self.timer?.invalidate()
//            self.totalSeconds = 300
//         }
//         runTimer()
//    }
//
//    /**
//     Method configures the lineBar view
//     */
//    fileprivate func configureLinearProgressBar(){
//        linearBar.backgroundColor = UIColor.white
//        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
//        linearBar.heightForLinearBar = 3
//    }
//
//
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print("scroll started")
//        stopTimer()
//        resignFirstResponder()
//    }
//
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        print("Touche started**")
//        stopTimer()
//    }
//
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("Touche stopeed**")
//    }
//
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("Touch started moving")
//        stopTimer()
//    }
//
//
//    func addPromptToNavigation() {
//
//        self.navigationItem.prompt = nil
//
//        self.navigationItem.prompt = chatConnectionStatus
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
//            self.navigationItem.prompt = nil
//        }
//    }
//
//    /**
//     Method shows status line message when view loads
//     */
//    func showStatusLine(color:UIColor) {
//        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
//        status.backgroundView.backgroundColor = color
//        status.bodyLabel?.textColor = UIColor.white
//        status.configureContent(body: chatConnectionStatus)
//        var statusConfig = SwiftMessages.defaultConfig
//        statusConfig.presentationContext = .window(windowLevel: .statusBar)
//        SwiftMessages.show(config: statusConfig, view: status)
//    }
//
//    func getLiveChatMessages() {
//        print("Live chat url:\(AppURLS.URL_Chat_connect + "\(App.threadID)")")
//        NetworkCall.performGet(url: AppURLS.URL_Chat_connect + "\(App.threadID)") { (success, response, statusCode, error) in
//            if let err = error {
//                print("Error while fetching messages:\(err.localizedDescription)")
//            }else if let resp = response{
//               print("Chat messages resp:\(response)")
////                if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary ,
////                    let doctor = data.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary {
////                    if let avatar = doctor.object(forKey: Keys.KEY_AVATAR_URL) as? String {
////                        self.doctor_avatar = avatar
////                    }
////                    print("Doctor response:\(doctor)")
////                    self.doctorRating = (doctor.object(forKey: Keys.KEY_RATINGS) as? Double) ?? 0
////
////                    if let doctorData = try? JSONSerialization.data(withJSONObject: doctor, options: .prettyPrinted) {
////                        self.doconlineDoctor = try? JSONDecoder().decode(Doctor.self, from: doctorData)
////                        self.doctorName = self.doconlineDoctor?.mciCode ?? ""
////                        self.doctor_avatar = self.doconlineDoctor?.avatar_url ?? ""
////                    }
////                }
//
//
//                if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary {
//
//                    if let doctor = data.object(forKey: Keys.KEY_DOCTOR) as? [String:Any],
//                        let doctorAvatar = doctor[Keys.KEY_AVATAR_URL] as? String {
//                        self.doctor_avatar = doctorAvatar
//                    }
//
//                    if let conversations = data.object(forKey: Keys.KEY_MESSAGES) as? [NSDictionary] {
//
//                        App.tempMessages.removeAll()
//                        self.defaultMessages.removeAll()
//                        self.chatImages.removeAll()
//
//                        for (index,conversation) in conversations.enumerated() {
//
//                            var sender_type = ""
//                            var sender_display_name = ""
//                            var senderMessage = ""
//                            var senderImage = ""
//
//                            if let messageBody = conversation.object(forKey: Keys.KEY_BODY) as? String {
//                                senderMessage = messageBody
//                            }else{
//                                print("Message body is null")
//                            }
//
//                            if let stype = conversation.object(forKey: "s_type") as? String {
//                                sender_type = stype
//                                if stype ==  self.senderId {
//                                    sender_display_name = self.userName
//                                }else if stype == self.doctor_id {
//                                    sender_display_name = self.doctorName
//                                }
//                            }
//
//                            if let imageURL = conversation.object(forKey: Keys.KEY_CDN_PHOTO_URL) as? String {
//                                senderImage = imageURL
//                            }else{
//                                print("Message Image url is null")
//                            }
//
//
//                            DispatchQueue.main.async(execute: {
//
//                                if !senderMessage.isEmpty
//                                {
//                                    if let message =  JSQMessage(senderId: sender_type, displayName: sender_display_name, text: senderMessage)
//                                    {
//                                        if !self.checkMessageContainsInList(at: index, toCheckMessage: message) {
//                                            App.tempMessages.append(message)
//                                            self.chatMessages.append(senderMessage)
//                                            let kmessage = KKMessage(type: 1, senderId: sender_type, senderDisplayName: sender_display_name, date: Date(), text: senderMessage)
//                                            self.defaultMessages.append(kmessage)
//                                        }
//                                    }
//                                    print("Message Body:\(senderMessage)")
//                                    // }
//
//                                }else if !senderImage.isEmpty {
//
//                                    if self.chatImages.contains(senderImage)
//                                    {
//                                        print("Image is already there")
//                                    }
//                                    else
//                                    {
//                                        let photoItem = AsyncPhotoMediaItem(withURL: NSURL(string: senderImage )! )
//                                        if let mediaMessage = JSQMessage(senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: Date(), media: photoItem) {
//                                            if !self.checkMessageContainsInList(at: index, toCheckMessage: mediaMessage) {
//                                                App.tempMessages.append(mediaMessage)
//                                                self.chatImages.append(senderImage)
//                                                let kmessage = KKMessage(type: 2, senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: Date(), text: senderImage)
//                                                self.defaultMessages.append(kmessage)
//                                            }
//                                        }
//                                    }
//                                    print("Message image:\(senderImage)")
//                                }
//                            })
//
//                        }
//
//                    }
//                }
//
//                DispatchQueue.main.async(execute: {
//                    App.messages += App.tempMessages
//                    print("Temp Array count:\(App.tempMessages.count)  Messages count:\(App.messages.count)")
//                    self.finishReceivingMessage()
//                    self.playSound(fileName: "newmessage")
//                    self.stopTimer()
//                })
//
//            }
//        }
//    }
//
//    func checkMessageContainsInList(at index:Int,toCheckMessage:JSQMessage) -> Bool {
//        if App.messages.indices.contains(index) {
//            let message = App.messages[index]
//            if message.isMediaMessage && toCheckMessage.isMediaMessage{
//                return message.media.mediaHash() == toCheckMessage.media.mediaHash()
//            }
//            if let text = message.text ,let checkText = toCheckMessage.text {
//                return text == checkText
//            }
//        }
//        return false
//    }
//
//    /**
//     Method used to fetch details from firebase
//     */
//    func readMessages() {
//        print("Reading messages:: method called")
//        self.linearBar.startAnimation()
//        App.mainReference?.observeSingleEvent(of: .value, with: { (snapshot) in
//            if let item = snapshot.value as? NSDictionary {
//                print("Chat Data:\(item)")
//                if let user_id = item.object(forKey: Keys.KEY_USER_ID) as? Int {
//                     print("Usr id::=>\(user_id)")
//                   // self.userid = "\(user_id)"
//                    self.senderId = self.userid
//                }
//
//                if let doctorid = item.object(forKey: Keys.KEY_DOCTOR_ID) as? Int {
//                    print("Doctor id::=>\(doctorid)")
//                    self.doctor_id = "doctor"
//                }
//
//                if let doctor = item.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary {
//                    let avatar = doctor.object(forKey: Keys.KEY_AVATAR_URL) as? String
//                  //  let doctorFullName = doctor.object(forKey: Keys.KEY_FULLNAME) as? String
//                   // print("Doctor details:avtar\(avatar) fullname:\(doctorFullName)")
//                    self.doctor_avatar = avatar ?? ""
//                    self.doctorRating = (doctor.object(forKey: Keys.KEY_RATINGS) as? Double) ?? 0
//                    if let doctorData = try? JSONSerialization.data(withJSONObject: doctor, options: .prettyPrinted) {
//                        self.doconlineDoctor = try? JSONDecoder().decode(Doctor.self, from: doctorData)
//                        self.doctorName = self.doconlineDoctor?.mciCode ?? ""
//                        self.doctor_avatar = self.doconlineDoctor?.avatar_url ?? ""
//                    }
//
//                    DispatchQueue.main.async(execute: {
//                        self.chatConnectionStatus = "DocOnline Doctor is online now"
//                        self.showStatusLine(color: UIColor(hexString: "4cbb17"))
//                        self.linearBar.stopAnimation()
//                        self.inputToolbar.contentView.textView.isUserInteractionEnabled = true
//                        self.inputToolbar.contentView.leftBarButtonItem.isEnabled = true
//                       // self.navigationItem.setChatTitleView(with: "MCI: \(self.doconlineDoctor?.mciCode ?? "")", subtitle: self.doconlineDoctor?.qualification ?? "")
//                        self.navigationItem.setNewTitle("MCI: \(self.doconlineDoctor?.mciCode ?? "")", subtitle: self.doconlineDoctor?.qualification ?? "")
//                        let titleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.doctorNameTapped))
//                        self.navigationItem.titleView?.addGestureRecognizer(titleTapGesture)
//                        self.setUpUI(doctorAvatar: self.doctor_avatar)
//
//                       // self.initUI(doctorAvatar: self.doctor_avatar, doctorName: self.doctorName)
//                    })
//                }else {
//                    print("no doctor details@@")
//                }
//
//                if let user = item.object(forKey: Keys.KEY_USER) as? NSDictionary {
//                    let avatar = user.object(forKey: Keys.KEY_AVATAR_URL) as! String
//                    let userFirstName = user.object(forKey: Keys.KEY_FIRST_NAME) as! String
//                    print("user details:avtar\(avatar) firstname:\(userFirstName)")
//                    self.user_avatar = avatar
//                    self.userName = userFirstName
//                }
//            }else {
//                print("Error while getting messages")
//                self.linearBar.stopAnimation()
//            }
//        })
//
//    }
//
//    ///action to perform on doctor image tapped
//    @objc func doctorNameTapped() {
//        self.view.resignFirstResponder()
//        guard let doctorInfoView = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_CHAT_DOCTOR_INFO) as? DoctorInfoViewController else { return }
//        doctorInfoView.doctorInfo = self.doconlineDoctor
//        doctorInfoView.doctorRating = self.doctorRating
//        doctorInfoView.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 260)
//        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: doctorInfoView)
//        segue.configure(layout: .centered)
//        segue.dimMode = .blur(style: .dark, alpha: 0.6, interactive: true)
//        segue.messageView.configureDropShadow()
//        segue.presentationStyle = .center
//        self.prepare(for: segue, sender: nil)
//        segue.perform()
//    }
//
//    /**
//     Method adds firebase listners for contineous message syncing
//     */
//    func readMessagesFromDatabase() {
//       App.messages.removeAll()
//       App.tempMessages.removeAll()
//       self.defaultMessages.removeAll()
//       self.chatImages.removeAll()
//       self.chatMessages.removeAll()
//       // self.linearBar.startAnimation()
//
//        App.mainReference?.child("messages").observe(.childAdded, with: { (messageSnapshot) in
//            if self.doctorName.isEmpty == true {
//                self.readMessages()
//            }
//
//            self.getLiveChatMessages()
//
//            if let messages = messageSnapshot.value as? NSDictionary, App.messages.count < 1 {
//                print("messages data:\(messages)")
//
//                var sender_type = ""
//                var sender_display_name = ""
//                var senderMessage = ""
//                var senderImage = ""
//
//                if let messageBody = messages.object(forKey: Keys.KEY_BODY) as? String {
//                    senderMessage = messageBody
//                }else{
//                    print("Message body is null")
//                }
//
//                if let stype = messages.object(forKey: "s_type") as? String {
//                    sender_type = stype
//                    if stype ==  self.senderId {
//                        sender_display_name = self.userName
//                    }else if stype == self.doctor_id {
//                        sender_display_name = self.doctorName
//                    }
//                }
//
//                if let imageURL = messages.object(forKey: Keys.KEY_CDN_PHOTO_URL) as? String {
//                    senderImage = imageURL
//                }else{
//                    print("Message Image url is null")
//                }
//
//
//                DispatchQueue.main.async(execute: {
//
//                    if !senderMessage.isEmpty
//                    {
//
////                        if self.chatMessages.contains(senderMessage)
////                        {
////                            print("message is already there")
////                        }
////                        else
////                        {
//
//                         if let message =  JSQMessage(senderId: sender_type, displayName: sender_display_name, text: senderMessage)
//                          {
//                             App.tempMessages.append(message)
//                             self.chatMessages.append(senderMessage)
//                            let kmessage = KKMessage(type: 1, senderId: sender_type, senderDisplayName: sender_display_name, date: Date(), text: senderMessage)
//                            self.defaultMessages.append(kmessage)
//                          }
//                          print("Message Body:\(senderMessage)")
//                      // }
//
//                    }else if !senderImage.isEmpty {
//
//                        if self.chatImages.contains(senderImage)
//                        {
//                            print("Image is already there")
//                        }
//                        else
//                        {
//                            let photoItem = AsyncPhotoMediaItem(withURL: NSURL(string: senderImage )! )
//                            if let mediaMessage = JSQMessage(senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: Date(), media: photoItem) {
//                                App.tempMessages.append(mediaMessage)
//                                self.chatImages.append(senderImage)
//                                let kmessage = KKMessage(type: 2, senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: Date(), text: senderImage)
//                                self.defaultMessages.append(kmessage)
//                            }
//                        }
//                        print("Message image:\(senderImage)")
//                    }
//
//                    App.messages = App.tempMessages
//                    print("Temp Array count:\(App.tempMessages.count)  Messages count:\(App.messages.count)")
//                    self.finishReceivingMessage()
//                    self.playSound(fileName: "newmessage")
//                    self.stopTimer()
//                })
//
//            }else {
//                print("No messages")
//                 //self.linearBar.stopAnimation()
//            }
//        })
//    }
//
//    ///Not used
//    func showMessageNotification(messageBody:String,doctorName:String ) {
//        print("\(#function) called--")
//        let content = UNMutableNotificationContent()
//        content.title = doctorName
//        content.body = messageBody
//        content.categoryIdentifier = "MessageAlert"
//      //  content.userInfo = ["local_appointment_id"]
//        content.sound = UNNotificationSound.default
//
//        var dateComponents = DateComponents()
//        dateComponents.second = 3
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
//
//        let requestIdentifier = "ChatMessage"
//        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
//
//        UNUserNotificationCenter.current().add(request) { (error) in
//            if error != nil {
//                print("Notification scheduling error:\(error?.localizedDescription)")
//            }else{
//                print("Notification fired for message")
//            }
//        }
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        self.linearBar.stopAnimation()
//        NotificationCenter.default.removeObserver(self)
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//
////        if App.mainReference != nil {
////            App.mainReference?.removeAllObservers()
////            App.mainReference = nil
////            print("Removing Chat listners")
////            App.messages.removeAll()
////            App.tempMessages.removeAll()
////        }
////
////        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"ChatThreadClosed"), object: nil)
////        NotificationCenter.default.removeObserver(self)
////
////        if self.timer != nil {
////            self.timer?.invalidate()
////            self.timer = nil
////        }
////        print("chat \(#function) is calling")
//
//    }
//
//
////Mark:- JSQMessageViewController Delegate & Datasource methods
//    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
//        if let message =  JSQMessage(senderId: "\(self.userid)", senderDisplayName: self.userName, date: Date(), text: text)
//        {
//            App.messages.append(message)
//            self.finishSendingMessage()
//            //self.playSound(fileName: "newmessage")
//        }
//
//        let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/message"
//        postMessageToServer(body: text, urlString: urlString, httpMethod: HTTPMethods.POST, type: 1)
//
//        self.stopTimer()
//
//    }
//
//    override func didPressAccessoryButton(_ sender: UIButton!) {
//        self.view.resignFirstResponder()
//        let alert:UIAlertController=UIAlertController(title: "Select", message: "You can select a maximum of 1 image", preferredStyle: UIAlertController.Style.actionSheet)
//
//        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
//        {
//            UIAlertAction in
//            self.openCamera()
//
//        }
//
////        let takePic = UIImage(named: "camera_icon")
////        cameraAction.setValue(takePic?.resizableImage(withCapInsets: .zero, resizingMode: .tile), forKey: "camera_icon")
//
//        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
//        {
//            UIAlertAction in
//            self.openGallary()
//        }
//
////        let attachImage = UIImage(named: "attachment")
////        gallaryAction.setValue(attachImage?.resizableImage(withCapInsets: .zero, resizingMode: .tile), forKey: "attachment")
//
////        let document = UIAlertAction(title: "Documents", style: UIAlertActionStyle.default)
////        {
////            UIAlertAction in
////            self.openDocumentPicker()
////        }
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
//        {
//            UIAlertAction in
//
//        }
//
//        // Add the actions
//        picker.delegate = self
//        alert.addAction(cameraAction)
//        alert.addAction(gallaryAction)
//        //alert.addAction(document)
//        alert.addAction(cancelAction)
//        self.present(alert, animated: true, completion: nil)
//    }
//
//
////    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
////        let message = messages[indexPath.row]
////        var messageUserName = ""
////        if self.userid == Int(message.senderId) {
////            messageUserName = message.senderDisplayName
////        }else {
////            messageUserName = "Doctor"
////        }
////        return NSAttributedString(string: messageUserName)
////    }
//
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
//    {
//        return App.messages[indexPath.item]
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
//    {
//        return App.messages.count
//    }
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
//    {
//        return App.messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
//    }
//
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
//    {
//        let message = App.messages[indexPath.item]
//        let doctorImageView = UIImageView()
//        let userImageView = UIImageView()
//
//        if message.senderId == self.userid {
//            let url = self.user_avatar
//
//            userImageView.kf.setImage(with: URL(string: url))
//            return nil //JSQMessagesAvatarImage(placeholder: imageView.image)
//
//        }
//        if message.senderId == self.doctor_id {
//            let url = self.doctor_avatar
//
//            let photoItem = AsyncPhotoMediaItem(withURL: NSURL(string: self.doctor_avatar )! )
//            doctorImageView.kf.setImage(with: URL(string: url))
//
//            return photoItem.getImage() == nil ? JSQMessagesAvatarImage(placeholder: UIImage(named: "Default-avatar"))  : JSQMessagesAvatarImage(placeholder: photoItem.getImage())
//        }
//
////        let url = URL(string: self.user_avatar)
////
////        DispatchQueue.global().async {
////            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
////            DispatchQueue.main.async {
////                imageView.image = UIImage(data: data!)
////            }
////        }
//
//        return nil
//    }
//
//
//
////    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
////    {
////        return App.messages[indexPath.item].senderId == senderId ? NSAttributedString(string: App.messages[indexPath.item].senderDisplayName) : NSAttributedString(string: App.messages[indexPath.item].senderDisplayName)
////    }
//
////    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
////    {
////        return messages[indexPath.item].senderId == senderId ? 0 : 15
////    }
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
//        let message: JSQMessage = App.messages[indexPath.item]
//        return  JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
//    }
//
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
//        return App.messages[indexPath.item].senderId == senderId ? 0 : 15
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
//
//        let message = App.messages[indexPath.row]
//
//        if message.isMediaMessage {
//
//            cell.mediaView = message.media.mediaView()
//
//        }else if message.senderId == senderId {
//                cell.textView!.textColor = UIColor.white
//        }else {
//             cell.textView!.textColor = UIColor.black
//        }
//
//        return cell
//    }
//
//
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
//
//       self.kfSource.removeAll()
//
//        for (index,image) in self.defaultMessages.enumerated() {
//            if image.type! == 2 {
//                let kfisher = KingfisherSource(urlString: image.body!)
//                if index == indexPath.row && self.kfSource.count != 0 {
//                    self.kfSource.insert(kfisher!, at: 0)
//                }else {
//                    self.kfSource.append(kfisher!)
//                }
//            }
//        }
//
//
//        let imageView : ImageSlideshow = ImageSlideshow()
//        imageView.contentScaleMode = .scaleAspectFit
//        imageView.zoomEnabled = true
//        imageView.activityIndicator = DefaultActivityIndicator(style: .whiteLarge, color: .black)
//        imageView.setImageInputs(self.kfSource)
//        imageView.presentFullScreenController(from: self)
//    }
//
//    ///Not used
//    func loadImageSource(imgView:UIImageView,url:URL,arrIndex:Int,IndexPath:Int) {
//        imgView.sd_setImage(with: url, completed: { (image, error, cache, url) in
//            if error != nil {
//                print("Error while loading image")
//            }else
//            {
//                if arrIndex == IndexPath && self.selectedImages.count != 0 {
//                    self.selectedImages.insert(ImageSource(image: image!), at: 0)
//                }else {
//                    self.selectedImages.append(ImageSource(image: image!))
//                }
//            }
//        })
//    }
//
//
//    /**
//       performs cancel of chat session
//     */
//    @IBAction func cancelTapped(_ sender: UIBarButtonItem) {
//        resignFirstResponder()
//        self.view.endEditing(true)
//       let alert = UIAlertController(title: "Are you sure?", message: "Do you want to exit chat session", preferredStyle: UIAlertController.Style.alert)
//
//        let yes = UIAlertAction(title: "Exit", style: UIAlertAction.Style.default) { (UIAlertAction) in
//            self.linearBar.stopAnimation()
//             let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/close"
//            self.postMessageToServer(body: "", urlString: urlString, httpMethod: HTTPMethods.PATCH, type: 2)
//
//        }
//        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
//
//        alert.addAction(yes)
//        alert.addAction(cancel)
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    /**
//       Shows alert of chat disconnected and dismiss chat view
//     */
//    func closing_alert(message:String) {
//        let alert = UIAlertController(title: "CHAT DISCONNECTED!", message: "Your chat session has been disconnected\(message)", preferredStyle: UIAlertController.Style.alert)
//
//        let yes = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
//            Database.database().reference().child(App.ChatType).child("threads").removeAllObservers()
//            App.refrenece.removeAllObservers()
//           // NotificationCenter.default.post(name:  NSNotification.Name(rawValue:"ChatHistoryLoad"), object: nil)
//            self.delegate?.didEndChat()
//          //  self.navigationController?.popViewController(animated: true)
//            //self.dismiss(animated: true, completion: nil)
//        }
//
//        alert.addAction(yes)
//        self.present(alert, animated: true, completion: nil)
//    }
//
//
//    /**
//        Prepares the image to upload with boundary string
//     - Parameter parameters : takes the array of images
//     - Parameter boundary : takes the boundary string generated
//     */
//    func createBodyWithImage(parameters: [UIImage],boundary: String) -> NSData {
//        let body = NSMutableData()
//
//        if parameters.count != 0 {
//            var i = 0;
//            for image in parameters {
//                let filename = "chatmessage\(App.messages.count).jpeg"   //should change id to app id
//                let data = image.jpegData(compressionQuality: 0.4);
//                let mimetype = mimeTypeForPath(path: filename)
//                let key = "photo"
//                body.appendString(string: "--\(boundary)\r\n")
//                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
//                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
//                body.append(data!)
//                body.appendString(string: "\r\n")
//                i += 1;
//            }
//
//        }
//        body.appendString(string: "--\(boundary)--\r\n")
//        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
//        return body
//    }
//
//
//    /**
//        Method performs request to server.
//        - Parameter body: the body of the message is passed here.
//        - Parameter urlString : the request of url string should pass here.
//        - Parameter httpMethod : pass the type of request should be done ex: *GET,POST* etc.
//        - Parameter type: to know what type of request and perform response operations
//     */
//    func postMessageToServer(body: String , urlString:String,httpMethod:String,type:Int) {
//        if !NetworkUtilities.isConnectedToNetwork()
//        {
//            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//            return
//        }
//
//        let bodyMessage = "\(Keys.KEY_BODY)=\(body)"
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//        //let urlString = AppURLS.URL_Chat_connect + "\(App.threadID)/message"
//        print("Message post url = \(urlString)")
//        let url = URL(string: urlString)
//        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
//        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
//        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
//        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
//        request.httpMethod = httpMethod
//
//        if type == 1 {
//            request.httpBody = bodyMessage.data(using: String.Encoding.utf8)
//        }else  if type == 4 {
//            let bound = generateBoundaryString()
//            request.setValue("multipart/form-data; boundary=\(bound)", forHTTPHeaderField: "Content-Type")
//            request.httpBody = createBodyWithImage(parameters: [self.pickedImage], boundary: bound) as Data
//        }
//
//
//        session.dataTask(with: request, completionHandler: { (data, response, error) in
//            if let error = error
//            {
//                DispatchQueue.main.async(execute: {
//                    print("Error==> : \(error.localizedDescription)")
//                    //self.didShowAlert(title: "", message: error.localizedDescription)
//                })
//            }
//            if let data = data
//            {
//                print("data =\(data)")
//            }
//            if let response = response
//            {
//                print("url = \(response.url!)")
//                print("response = \(response)")
//                let httpResponse = response as! HTTPURLResponse
//                print("response code = \(httpResponse.statusCode)")
//                //if you response is json do the following
//                do
//                {
//                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
//
//                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
//
//                    if !errorStatus
//                    {
//                        // UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
//                        print("performing error handling send message::\(resultJSON)")
//
//                    }
//                    else {
//                        print("Send message response\(resultJSON)")
//
//
//                        if type == 1 {
//                            DispatchQueue.main.async(execute: {
//                                //self.readMessages()
//                                self.finishSendingMessage(animated: true)
//                            })
//                        }else if type == 3 {
//                            DispatchQueue.main.async(execute: {
//                               self.closing_alert(message: "")
//                                if self.timer != nil {
//                                    self.timer?.invalidate()
//                                }
//                            })
//                        }else if type == 4 {
//                            DispatchQueue.main.async(execute: {
//                                print("Image upload response::\(resultJSON)")
//                               self.linearBar.stopAnimation()
//                            })
//                        } else if type == 2 {
//                            DispatchQueue.main.async(execute: {
//                                if App.mainReference != nil {
//                                    App.mainReference?.removeAllObservers()
//                                    App.mainReference = nil
//                                    print("Removing Chat listners")
//                                    App.messages.removeAll()
//                                    App.tempMessages.removeAll()
//                                }
//
//                                //NotificationCenter.default.post(name:  NSNotification.Name(rawValue:"ChatHistoryLoad"), object: nil)
//                                self.delegate?.didEndChat()
//
//                                if self.timer != nil {
//                                    self.timer?.invalidate()
//                                    self.timer = nil
//                                }
//
//                               // self.dismiss(animated: true, completion: nil)
//                                print("exit chat session:\(resultJSON)")
//                            })
//                        }
//
//
////                        DispatchQueue.main.async(execute: {
////                            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"ChatHistoryLoad"), object: nil)
////                        })
//
//                       //self.readMessages()
//                    }
//                }catch let error{
//                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
//                    print("Received not-well-formatted JSON chat session connect: \(error.localizedDescription)")
//                }
//
//            }
//
//        }).resume()
//    }
//
//
//
//
//
//    deinit {
//        if App.mainReference != nil {
//            App.mainReference?.removeAllObservers()
//            App.mainReference = nil
//            print("Removing Chat listners")
//            App.messages.removeAll()
//            App.tempMessages.removeAll()
//        }
//
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"ChatThreadClosed"), object: nil)
//        NotificationCenter.default.removeObserver(self)
//
//        if self.timer != nil {
//            self.timer?.invalidate()
//            self.timer = nil
//        }
//        print("chat \(#function) is calling")
//    }
//
//
//
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if segue.identifier == StoryboardSegueIDS.ID_IMAGE_VIEW {
//           let destVC = segue.destination as! ImageViewController
//           destVC.images = selectedImages
//        }
//    }
//}
//
////extension ChatViewController : ChatEndNotificationDelegate {
////    func didRecieveChatEndNotification(threadID: String, closedBy: String) {
////        if threadID == App.threadID {
////            var message = ""
////
////            if closedBy == "0" {
////                message = " due to no doctor is available"
////            }else {
////                message = " by doctor"
////            }
////
////            print("Message::=>\(message)")
////
////            DispatchQueue.main.async {
////                self.closing_alert(message: message)
////            }
////        }else {
////            print("Thread is not equal")
////        }
////    }
////}
//
//extension ChatViewController : SimplePingDelegate
//{
//    @objc func testSpeed() {
//        self.imgRefresh!.rotate360Degrees()
//        self.btnRefresh!.isEnabled = false
//        currentSpeed = 0.0
//        maxSpeed = 0.0
//        self.measurer = NSTMeasurer()
//        self.measurer.addObserver(self, forKeyPath: selectorMax, options: .new, context: nil)
//        self.measurer.addObserver(self, forKeyPath: selectorAvg, options: .new, context: nil)
//        self.measurer.addObserver(self, forKeyPath: selectorCurrent, options: .new, context: nil)
//
//        //https://www.office.xerox.com/latest/SFTBR-04U.PDF
//        self.downloadFile(url: URL(string: "https://corpstaging.doconline.com/DocOnline_Brochure_1mb.pdf")!) {
//            print("download successfull")
//            self.measurer.removeObserver(self, forKeyPath: self.selectorMax)
//            self.measurer.removeObserver(self, forKeyPath: self.selectorAvg)
//            self.measurer.removeObserver(self, forKeyPath: self.selectorCurrent)
//            DispatchQueue.main.async {
//                self.changeColor(value: self.maxSpeed)
//                self.imgRefresh!.layer.removeAllAnimations()
//                /*
//                self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",self.maxSpeed*1000) + " Kb/s"
//                if (self.maxSpeed*1000) > 1000
//                {
//                    self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",(self.maxSpeed*1000/1024.0)) + " Mb/s"
//                }*/
//            }
//        }
//
//    }
//
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//
//        if keyPath == "maxDownloadSpeed"//#keyPath(measurer.maxDownloadSpeed)
//        {
//            maxSpeed = change?[.newKey] as! Double
//            print("maxSpeed : ",maxSpeed)
//            /*
//            self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",maxSpeed*1000) + " Kb/s"
//            if (maxSpeed*1000) > 1000
//            {
//               self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",(maxSpeed*1000/1024.0)) + " Mb/s"
//            }*/
//            DispatchQueue.main.async {
//                self.changeColor(value: self.maxSpeed)
//            }
//            return
//        }
//        if keyPath == "averageDownloadSpeed"//#keyPath(measurer.averageDownloadSpeed)
//        {
//            let avgSpeed : Double = change?[.newKey] as! Double
//            print("avgSpeed : ",avgSpeed)
//            return
//        }
//        if keyPath == "currentDownloadSpeed"//#keyPath(measurer.currentDownloadSpeed)
//        {
//            currentSpeed = change?[.newKey] as! Double
//            print("currentSpeed : ",currentSpeed)
//            self.isCurrentSpeed = true
//            /*
//            self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",currentSpeed*1000) + " Kb/s"
//            if (currentSpeed*1000) > 1000
//            {
//                self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",(currentSpeed*1000/1024.0)) + " Mb/s"
//            }*/
//            DispatchQueue.main.async {
//                self.changeColor(value: self.currentSpeed)
//            }
//            return
//        }
//
//    }
//
//    func downloadFile(url: URL, completion: @escaping () -> ()) {
//        let sessionConfig = URLSessionConfiguration.default
//        let session = URLSession(configuration: sessionConfig)
//        var request = URLRequest(url: url)
//        request.httpMethod = "GET"
//        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
//
//            if error == nil {
//                // Success
//                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
//                    print("Success Downloading: \(statusCode)")
//                }
//                DispatchQueue.main.async {
//                    self.isCurrentSpeed = false
//                    self.btnRefresh!.isEnabled = true
//
//                }
//                completion()
//
//            } else {
//                DispatchQueue.main.async {
//                    print("Failure: %@", error?.localizedDescription);
//                }
//
//            }
//        }
//        task.resume()
//    }
//
//    func changeColor(value:Double) {
//        if value*1000 >= 200.0
//        {
//            //self.lbl_stats!.text = "Your internet bandwidth supports Audio/Video calls"
//            self.lbl_stats?.textColor = self.hexStringToUIColor(hex: "FFA500")
//        }
//        else
//        {
//            //self.lbl_stats!.text = "Your internet bandwidth is not suitable for making Video call"
//            self.lbl_stats?.textColor = self.hexStringToUIColor(hex: "FF0000")
//            if value*1000<50
//            {
//                //self.lbl_stats!.text = "Your internet bandwidth is not suitable for making Audio/Video calls "
//            }
//        }
//        if value*1000 >= 500.0
//        {
//            self.lbl_stats?.textColor = self.hexStringToUIColor(hex: "6DBE45")
//            //self.lbl_stats!.text = "Your internet bandwidth is ideal for Video/Audio internet calls"
//        }
//
//        self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",value*1000) + " Kb/s"
//        if (value*1000) > 1000
//        {
//            self.lbl_stats!.text = "Bandwidth : " + String(format:"%.2f",(value*1000/1024.0)) + " Mb/s"
//        }
//    }
//
//    func simplePing(pinger: SimplePing!, didFailToSendPacket packet: NSData!, error: NSError!) {
//        print("didFailToSendPacket")
//    }
//
//    func simplePing(_ pinger: SimplePing, didFailWithError error: Error) {
//        print("didFailWithError")
//    }
//
//    func simplePing(pinger: SimplePing!, didReceivePingResponsePacket packet: NSData!) {
//        print("didReceivePingResponsePacket")
//    }
//
//    func simplePing(_ pinger: SimplePing, didReceiveUnexpectedPacket packet: Data) {
//        print("didReceiveUnexpectedPacket")
//    }
//
//    func simplePing(pinger: SimplePing!, didSendPacket packet: NSData!) {
//        print("didSendPacket")
//    }
//
//    func simplePing(_ pinger: SimplePing, didStartWithAddress address: Data) {
//        print("didStartWithAddress")
//        canStartPinging = true
//    }
//}
//
//
//// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
//	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
//}
//
//// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
//	return input.rawValue
//}
