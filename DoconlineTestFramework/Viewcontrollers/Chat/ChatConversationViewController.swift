//
//  ChatHistoryConversationViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 23/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import ImageSlideshow
import Kingfisher
import SwiftMessages

class SDPhotoMediaItem: JSQPhotoMediaItem
{
    override func mediaView() -> UIView?
    {
        if let imageView = super.mediaView() as? UIImageView {
            let masker = JSQMessagesMediaViewBubbleImageMasker(bubbleImageFactory: JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero))
            masker?.applyIncomingBubbleImageMask(toMediaView: imageView)
            
            return imageView
        }
        
        return nil
    }
}

class ChatConversationViewController: JSQMessagesViewController {

    ///chat history instance variable
    var doctor : ChatInfo?
    var doctorRating = 0.0
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    
//    lazy var outgoingBubble: JSQMessagesBubbleImage = {
//        
//        let gradient:CAGradientLayer = CAGradientLayer(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 100)), colors: [hexStringToUIColor(hex: StandardColorCodes.GREEN),hexStringToUIColor(hex: StandardColorCodes.BLUE)])
//        
//        let bubbleFactoryOutline = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: .zero)
//        
//        return  bubbleFactoryOutline!.outgoingMessagesBubbleImage(with: UIColor(patternImage: UIImage(named: "right_convo")!.resizableImage(withCapInsets: .zero)))
//        
//    }()
    
    ///Outgoing message bubble color
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        let bubbleFactory = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleRegularTailless(), capInsets: .zero)
      //  UIColor(hexString: StandardColorCodes.OUTGOING_CHAT_BACKGROUND)
        return  bubbleFactory!.outgoingMessagesBubbleImage(with: UIColor(hexString: StandardColorCodes.OUTGOING_CHAT_BACKGROUND) )
    }()
    
    ///message bubble color of incoming e8e8e8
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return  JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor(hexString: StandardColorCodes.INCOMING_CHAT_BACKGROUND))
    }()
    
    ///Instance array for chat messages
    var messages = [JSQMessage]()
    ///Instance for chat message
    var kkMessaages = [KKMessage]()
    ///Instance for selected message images
    var selectedImages = [ImageSource]()
    ///Instance for selected image in message
    var selectedImage : UIImage!
    ///image source type used to display image on `imageView`
    var images = [ImageSource]()
    ///asyncronous image loading array
    var kfSource = [KingfisherSource]()
    ///Displays selected image in full size
//    var imageView : ImageSlideshow = ImageSlideshow()
    ///Doctor avatar url
    var docotorAvatar = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        senderId = "user"
        senderDisplayName = ""
        
       // setupBackgroundImage()
        configureLinearProgressBar()
        
        inputToolbar.contentView.leftBarButtonItem = nil
        inputToolbar.isHidden = true
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        automaticallyScrollsToMostRecentMessage = true
        
        if let doctorModal = self.doctor {
            self.navigationItem.setNewTitle("MCI: \(doctorModal.doctor?.mciCode ?? "")", subtitle: doctorModal.doctor?.qualification ?? "")
           // self.navigationItem.setChatTitleView(with: "MCI: \(doctorModal.doctor?.mciCode ?? "")", subtitle: doctorModal.doctor?.qualification ?? "")
        }
        getConversation()
        
        let titleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.titleTapped(_:)))
        self.navigationItem.titleView?.addGestureRecognizer(titleTapGesture)
        addInfoButton()
    }
    
    func addInfoButton() {
        let button = UIButton.init(type: UIButton.ButtonType.infoLight)
        button.setTitle("", for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.titleTapped(_:)), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButton]
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        self.viewDidAppear(animated)
//        if let doctor = self.doctor?.doctor {
//            self.navigationItem.setTitleView(with: "MCI: \(doctor.mciCode ?? ""), RegNo: \(doctor.doctorPractitionerNumber ?? "")", subtitle: doctor.qualification ?? "")
//        }
//    }
    
    @objc func titleTapped(_ sender:UITapGestureRecognizer) {
        guard let doctorInfoView = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_CHAT_DOCTOR_INFO) as? DoctorInfoViewController else { return }
        doctorInfoView.doctorInfo = self.doctor?.doctor
        doctorInfoView.doctorRating = self.doctorRating
        doctorInfoView.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 260)
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: doctorInfoView)
        segue.configure(layout: .centered)
        segue.dimMode = .blur(style: .dark, alpha: 0.6, interactive: true)
        segue.messageView.configureDropShadow()
        segue.presentationStyle = .center
        self.prepare(for: segue, sender: nil)
        segue.perform()
    }
    
    ///Configures the linear bar loader
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
    ///Setups the Background image to chat
    func setupBackgroundImage() {
        let imgBackground:UIImageView = UIImageView(frame: self.view.bounds)
        imgBackground.image = UIImage(named: "chat_background")
        imgBackground.contentMode = UIView.ContentMode.scaleAspectFill
        imgBackground.clipsToBounds = true
        self.collectionView?.backgroundView = imgBackground
    }
    

    ///Method performs request to the server and fetches the chat messages
    func getConversation() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        if let doctorModal = self.doctor {
            self.linearBar.startAnimation()
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: "\(AppURLS.URL_Chat_connect)\(doctorModal.id!)")
            print("URL==>\(AppURLS.URL_Chat_connect)/\(doctorModal.id!)")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request) { (data, response, error) in
                if error != nil
                {
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                    DispatchQueue.main.async(execute: {
                        self.linearBar.stopAnimation()
                    })
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            
                            if !errorStatus
                            {
                                print("performing error handling get appointment details :\(jsonData)")
                                DispatchQueue.main.async(execute: {
                                    self.linearBar.stopAnimation()
                                })
                            }
                            else
                            {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                
                                if code == 200 && responseStatus == "success"
                                {
                                    
                                    if let doctor = data.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary {
                                        if let avatar = doctor.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                            self.docotorAvatar = avatar
                                        }
                                        self.doctorRating = (doctor.object(forKey: Keys.KEY_RATINGS) as? Double) ?? 0
                                    }
                                    
                                    
                                    if let conversations = data.object(forKey: Keys.KEY_MESSAGES) as? [NSDictionary] {
                                        
                                        for conversation in conversations {
                                            //let thread_id = conversation.object(forKey: Keys.KEY_THREAD_ID) as! Int
                                           //let sender_id = conversation.object(forKey: Keys.KEY_SESSION_ID) as! Int
                                  
                                            let sender_type = conversation.object(forKey: Keys.KEY_S_TYPE) as! String
                            
                                            let updatedAt = conversation.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                                                
                                            let msgdate = self.stringToDateConverter(date: updatedAt) ?? Date()
                                                                
                                            var senderName = ""
                                            if sender_type == "doctor" {
                                                senderName = doctorModal.doctor?.mciCode ?? ""
                                            }else {
                                                senderName = ""
                                            }
                                            
                                            if let message = conversation.object(forKey: Keys.KEY_BODY) as? String {
                                                print("Sender_type:\(sender_type) Message body:\(message)")
                                                let kmessage = KKMessage(type: 1, senderId: sender_type, senderDisplayName: senderName, date: msgdate, text: message)
                                                self.kkMessaages.append(kmessage)
                                                
                                            }else {
                                                if let image = conversation.object(forKey: Keys.KEY_CDN_PHOTO_URL) as? String {
                                                     print("Message Image:\(image)")
                                                    
                                                   //  let photoItem = AsyncPhotoMediaItem(withURL: NSURL(string: image )! )
                                                    
                                                    let kmessage = KKMessage(type: 2, senderId: sender_type, senderDisplayName: senderName, date: msgdate, text: image)
                                                    self.kkMessaages.append(kmessage)
                                                    
//                                                    if let mediaMessage = JSQMessage(senderId: sender_type, senderDisplayName: senderName, date: msgdate, media: photoItem) {
//
//                                                        self.messages.append(mediaMessage)
//                                                        DispatchQueue.main.async(execute: {
//                                                            self.finishReceivingMessage()
//                                                        })
//                                                    }
                                                    
                                                }else {
                                                    print("Image url is null")
                                                }
                                            }
                                            
                                            
//                                            DispatchQueue.main.async(execute: {
//                                                self.linearBar.stopAnimation()
//                                                self.updateMessages()
//                                                self.finishReceivingMessage()
//                                            })
                                        }
                                       
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        self.linearBar.stopAnimation()
                                        self.updateMessages()
                                        self.finishReceivingMessage()
                                    })
                                    
                                }else {
                                    DispatchQueue.main.async(execute: {
                                        self.linearBar.stopAnimation()
                                    })
                                    print("Error while fetching appointment details:\(responseStatus) ")
                                }
                                
                            }
                        }
                        catch let error
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON appointment summary:\(error.localizedDescription)")
                            DispatchQueue.main.async(execute: {
                                self.linearBar.stopAnimation()
                            })
                        }
                    }
                }
            }.resume()
        }
        else
        {
          print("Data modal is empty")
        }
        
    }
    
    /**
     Used to get the string width
     - Parameter text: pass the string to get the width
     - Parameter font: use the font which you want
     - Returns CGFloat: width of the string
     */
    func textWidthAndHeight(text: String, font: UIFont?) -> (CGFloat,CGFloat) {
        let attributes = font != nil ? [NSAttributedString.Key.font: font!] : [:]
        return (text.size(withAttributes: attributes).width,text.size(withAttributes: attributes).height)
    }
    
    ///Appends the message to array
    func updateMessages() {
        for mesg in self.kkMessaages {
            if mesg.type! == 1 {
                if let msg = JSQMessage(senderId: mesg.senderId!, senderDisplayName: mesg.senderDisplayName!, date: mesg.date, text: mesg.body!) {
                    self.messages.append(msg)
                }
            }else if mesg.type! == 2 {
                
                 let photoItem = AsyncPhotoMediaItem(withURL: NSURL(string: mesg.body! )! )
                
                if let mediaMessage = JSQMessage(senderId: mesg.senderId!, senderDisplayName:  mesg.senderDisplayName!, date: mesg.date, media: photoItem) {
                    self.messages.append(mediaMessage)
                }
            }
        }
    }
    
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
//         let message =  messages[indexPath.item]
//        var textWidth : CGFloat = 0.0
//        var textHeight : CGFloat = 0.0
//
//        if message.isMediaMessage {
//             textWidth  = 200
//             textHeight  = 200
//        }else if message.senderId == senderId {
//            let text = message.text
//            let values = textWidthAndHeight(text: text!, font: nil)
//            textWidth = values.0
//            textHeight = values.1
//        }
//
//        print("Width:\(textWidth)    height:\(textHeight)")
//          let gradient:CAGradientLayer = CAGradientLayer(frame: CGRect(x: 0, y: 0, width: textWidth + 20, height: textHeight + 20), colors: [hexStringToUIColor(hex: StandardColorCodes.GREEN),hexStringToUIColor(hex: StandardColorCodes.BLUE)])
//            gradient.startPoint = CGPoint(x: 0, y: 0)
//            gradient.endPoint = CGPoint(x: 0.5, y: 0)
//
     //     let bubbleFactoryOutline = JSQMessagesBubbleImageFactory(bubble: UIImage(named:"right_convo")!, capInsets: .zero)

        //let bubbleImage = JSQMessagesBubbleImage(messageBubble: UIImage(named:"right_convo")!.resizableImage(withCapInsets: .zero, resizingMode: .tile), highlightedImage: UIImage(named:"right_convo")!)

      //  let outgoingBubble: JSQMessagesBubbleImage =  bubbleFactoryOutline!.outgoingMessagesBubbleImage(with: StandardColorCodes)
     
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        if !self.docotorAvatar.isEmpty {
            let photoItem = AsyncPhotoMediaItem(withURL: NSURL(string: self.docotorAvatar )! )
           
            if let item = photoItem.getImage() {
               return  JSQMessagesAvatarImage(placeholder: item)  
            }else {
               return JSQMessagesAvatarImage(placeholder:  UIImage(named: "Default-avatar"))
            }
        }
        
        return nil
    }
    
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
//    {
//        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string: messages[indexPath.item].senderDisplayName)
//    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message: JSQMessage = self.messages[indexPath.item]
        return  JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
       let message = self.messages[indexPath.row]
        
        
        if message.isMediaMessage {
            
            cell.mediaView = message.media.mediaView()
            
        }else if message.senderId == senderId {
            cell.textView!.textColor = UIColor.white
        }else {
            cell.textView!.textColor = UIColor.black
        }
        
        return cell
    }

   
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        self.kfSource.removeAll()
        

        for (index,image) in self.kkMessaages.enumerated() {
            if image.type! == 2 {
                let kfisher = KingfisherSource(urlString: image.body!)
                if index == indexPath.row && self.kfSource.count != 0 {
                    self.kfSource.insert(kfisher!, at: 0)
                }else {
                   self.kfSource.append(kfisher!)
                }
            }
        }
    
        let imageView : ImageSlideshow = ImageSlideshow()
        imageView.contentScaleMode = .scaleAspectFit
        imageView.zoomEnabled = true
        imageView.activityIndicator = DefaultActivityIndicator(style: .whiteLarge, color: .black)
        imageView.setImageInputs(self.kfSource)
        imageView.presentFullScreenController(from: self)
       // let fullScreenController =  self.imageView.presentFullScreenController(from: self)
      //  fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    ///Not used
    func loadImageSource(imgView:UIImageView,url:URL,arrIndex:Int,IndexPath:Int) {
       
        imgView.sd_setImage(with: url, completed: { (image, error, cache, url) in
            if error != nil {
                print("Error while loading image")
            }else
            {
                if arrIndex == IndexPath && self.images.count != 0 {
                    self.images.insert(ImageSource(image: image!), at: 0)
                }else {
                    self.images.append(ImageSource(image: image!))
                }
                
               // self.imageView.setImageInputs(self.images)
            }
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            self.linearBar.stopAnimation()
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_IMAGE_VIEW {
            let destVC = segue.destination as! ImageViewController
            destVC.images = selectedImages
        }
    }
    

}
