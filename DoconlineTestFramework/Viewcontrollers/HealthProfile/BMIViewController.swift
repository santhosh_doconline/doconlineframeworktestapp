//
//  BMIViewController.swift
//  DocOnline
//
//  Created by Doconline India on 28/11/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import WMGaugeView

class BMIViewController: UIViewController {
 
    @IBOutlet weak var vw_weight: UIView!
    @IBOutlet weak var vw_height: UIView!
    @IBOutlet weak var vw_BMI: UIView!
    @IBOutlet weak var lbl_weight: UILabel!
    @IBOutlet weak var lbl_height: UILabel!
    @IBOutlet weak var lbl_total: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var gaugeView: WMGaugeView!
    @IBOutlet var tf_weight: UITextField!
    @IBOutlet var tf_height: UITextField!
    @IBOutlet var bt_calculate: UIButton!
    
    @IBOutlet var LC_calculate_height: NSLayoutConstraint!
    
    
    var weight : String?
    var height : String?
    var isFromDashboard = false
    var doubleBMI: Double = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        shadowEffect(views: [vw_weight,vw_height,vw_BMI])

        if !isFromDashboard {
            self.bt_calculate.isHidden = true
            self.LC_calculate_height.constant = 0
            self.tf_height.isEnabled = false
            self.tf_weight.isEnabled = false
            self.calculate(weight: self.weight ?? "", height: self.height ?? "")
        }else
        {
          //  buttonCornerRadius(buttons: [self.bt_calculate])
            self.tf_height.isEnabled = true
            self.tf_weight.isEnabled = true
            self.bt_calculate.isHidden = false
            self.LC_calculate_height.constant = 40
            let dataModel = HealthProfile.get_health_profile_default()
            
            if dataModel.weight == 0 {
                self.tf_weight.text = ""
            }else {
                self.tf_weight.text = String(dataModel.weight)
            }
            
            if dataModel.height == 0 {
                self.tf_height.text = ""
            }else {
                self.tf_height.text =  String(dataModel.height)
            }
            
        }
        
        gaugeView.maxValue = 50.0
        //gaugeView.minValue = 10.0
        gaugeView.showRangeLabels = true
        gaugeView.scaleDivisions = 10
        gaugeView.scaleSubdivisions = 5
        //        gaugeView.scaleSubdivisionsWidth = 0.002
        //        gaugeView.scaleSubdivisionsLength = 0.04
        //        gaugeView.scaleDivisionsWidth = 0.007
        //        gaugeView.scaleDivisionsLength = 0.07
        gaugeView.rangeValues = [ 18.5,25,30,40,50]
        gaugeView.rangeColors = [ RGB(r: 135, g: 206, b: 235),RGB(r: 21, g: 145, b: 211),  RGB(r: 241, g: 165, b: 120),RGB(r: 239, g: 105, b: 47),RGB(r: 184, g: 33, b: 43)    ]
        gaugeView.rangeLabels = [ "Thinness","Normal","Ex.Weight","Obese","Morbidly Obese"        ]
        
        gaugeView.unitOfMeasurement = "Kg/m\u{00B2}"
        gaugeView.showUnitOfMeasurement = true
        
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_calculate.backgroundColor = Theme.buttonBackgroundColor
    }
    
    func calculate(weight:String,height:String) {
        // Do any additional setup after loading the view.
        tf_weight.text = weight
        tf_height.text = height
        let doubleWeight = weight.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0   //Double(weight) ?? 0.0
        let doubleHeight = height.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0   //Double(height) ?? 0.0
        
        let doubleBMI = self.bmi(hight: doubleHeight/100, wighte: doubleWeight)
        lbl_total.text = String(format:"%.2f", doubleBMI)
        
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(gaugeUpdateTimer), userInfo: nil, repeats: false)
    }
    
    
    
    @objc func gaugeUpdateTimer(timer:Timer) {
        gaugeView.setValue(Float(lbl_total.text ?? "0.0") ?? 0.0, animated: true, duration: 0.5) { (finish) in
            print(Float(self.lbl_total.text ?? "0.0") ?? 0.0)
        }
    }
    
    func RGB(r:Double,g:Double,b:Double) -> UIColor {
        return UIColor(red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(g/255.0), alpha: 1.0)
    }
    func RGBValue(r:Double,g:Double,b:Double) -> UIColor {
        return UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
    }
    
    /**
     BMI Function
     this function is to calculate the body mass index
     by passing the hight and wighte
     @param hight Double
     @param wighte Double
     @return Bmi :Double .
     */
    func bmi(hight:Double,wighte:Double)-> Double{
        let x = wighte /  ( hight * hight )
        return x
    }
    /**
     BMI Function
     this function is to calculate the typical wighte
     by passing the hight only
     @param hight Double
     @return wighte:Double .
     */
    func tBmi(hight:Double)-> Double{
        let wight = 22 * (hight*hight)
        return wight
    }
    
    func changeStatus(bmi: Double) {
        if (bmi < 16)
        {
            lbl_Status.text = "Severe Thinness"
        }
        else if (bmi >= 16 && bmi < 17)
        {
            lbl_Status.text = "Moderate Thinness"
        }
        else if (bmi >= 17 && bmi < 18.5)
        {
            lbl_Status.text = "Mild Thinness"
        }
        else if (bmi >= 18.5 && bmi < 25)
        {
            lbl_Status.text = "Normal"
        }
        else if (bmi >= 25 && bmi < 30)
        {
            lbl_Status.text = "Overweight"
        }
        else if (bmi >= 30 && bmi < 35)
        {
            lbl_Status.text = "Obese Class I"
        }
        else if (bmi >= 35 && bmi < 40)
        {
            lbl_Status.text = "Obese Class II"
        }
        else
        {
            lbl_Status.text = "Obese Class III"
        }
    }

   
    @IBAction func calculateTapped(_ sender: UIButton) {
        let enteredWeight = self.tf_weight.text!
        let enteredHeight = self.tf_height.text!
        
        
        if enteredWeight.isEmpty == true {
            self.didShowAlert(title: "Weight required!", message: "")
            return
        }else if enteredWeight.isEqual(".") {
            self.didShowAlert(title: "Weight required!", message: "Please enter correct weight")
            return
        }else if enteredHeight.isEmpty == true {
            self.didShowAlert(title: "Height required!", message: "")
            return
        }else if enteredHeight.isEqual(".") {
            self.didShowAlert(title: "Height required!", message: "Please enter correct height")
            return
        }
        
        if let height = enteredHeight.convertStringToNumberalIfDifferentLanguage() , let weight = enteredWeight.convertStringToNumberalIfDifferentLanguage() {
            let valHeight : Double = height.doubleValue
            let  valWeight : Double = weight.doubleValue
            
            self.tf_height.resignFirstResponder()
            self.tf_weight.resignFirstResponder()
            
            print("Height::=>\(valHeight) weight:\(valWeight)")
            if(valHeight <= 51.0 || valHeight >= 275.0)
            {
                self.didShowAlert(title: "Info", message: "Height must be greater than 51cm and less than 275 cm")
                return
            }
            
            if(valWeight <= 2.0 || valWeight >= 500.0)
            {
                self.didShowAlert(title: "Info", message: "Weight must be greater than 2kg's and less than 500 kg's")
                return
            }
            
           self.calculate(weight: weight.stringValue, height: height.stringValue)
            
        }else {
            self.didShowAlert(title: "Info", message: "Please enter a valid weight or height")
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BMIViewController : UITextFieldDelegate {
    
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxNoOfDigitsBeforeDecimal = 3;
        
        let maxNoOfDigitsAfterDecimal = 2;
        
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        let seperatedString = newString?.components(separatedBy: ".")
        if (seperatedString?.count)! == 1 && (newString?.count)! > maxNoOfDigitsBeforeDecimal {
            return false
        }
        
        if (seperatedString?.count)! > 2 {
            return false
        }
        
        if (seperatedString?.count)! >= maxNoOfDigitsAfterDecimal {
            let  sepStr = String(format: "%@", (seperatedString?[1])!)
            return !(sepStr.count > maxNoOfDigitsAfterDecimal);
        }
        return true
    }
}


