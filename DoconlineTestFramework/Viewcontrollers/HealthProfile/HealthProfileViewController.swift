//
//  HealthProfileViewController.swift
//  DocOnline
//
//  Created by dev-3 on 14/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

struct ButtonOptionsIndex {
    static let lifeStyle = 0
    static let sleepStyle = 1
    static let sleepPattern = 2
    static let exercise = 3
    static let marital = 4
    static let switchMedicine = 5
    static let MedicalHistory = 6
    static let MedicalInsurance = 7
}

class HealthProfileViewController: UIViewController ,UITextViewDelegate ,NVActivityIndicatorViewable{


//    @IBOutlet weak var vw_weight: UIView!
//    @IBOutlet weak var vw_height: UIView!
//    @IBOutlet weak var vw_smoking: UIView!
//    @IBOutlet weak var vw_medical_history: UIView!
    
    @IBOutlet weak var addDrugAllergy: UIButton!
    @IBOutlet var vw_back: UIView!
    @IBOutlet weak var tf_weight: UITextField!
    @IBOutlet weak var tf_height: UITextField!
    @IBOutlet weak var tf_smoking: UITextField!
    @IBOutlet var tf_medication: UITextField!
    @IBOutlet weak var tf_total_sleep: UITextField!
    @IBOutlet weak var tf_pattern_sleep: UITextField!
    @IBOutlet weak var tf_exercise: UITextField!
    @IBOutlet weak var tf_pregnancy: UITextField!
    @IBOutlet weak var tf_marital_status: UITextField!
    @IBOutlet weak var tf_switch_medicin: UITextField!
    @IBOutlet weak var tf_medical_insurance: UITextField!
    
    @IBOutlet var tf_medicalHistory: UITextView!
    @IBOutlet weak var iv_smoking_epand: UIImageView!
    @IBOutlet var iv_totalSleepExpand: UIImageView!
    @IBOutlet var iv_patternSleepExpand: UIImageView!
    @IBOutlet var iv_exerciseExpand: UIImageView!
    @IBOutlet var iv_pregnancyExpand: UIImageView!
    @IBOutlet weak var LC_pregnancy_height: NSLayoutConstraint!
    @IBOutlet weak var LC_preg_title_height: NSLayoutConstraint!
    @IBOutlet weak var LC_preg_sep_height: NSLayoutConstraint!
    @IBOutlet weak var LC_preg_bottom_height: NSLayoutConstraint!
    @IBOutlet var iv_maritalExpand: UIImageView!
    @IBOutlet var iv_switchMedicinExpand: UIImageView!
    @IBOutlet var iv_medicalInsurance: UIImageView!
 //   @IBOutlet weak var LC_medicationsHeigh: NSLayoutConstraint!
    
   // @IBOutlet weak var medicationsTableView: UITableView!
//    @IBOutlet weak var lb_noMedications: UILabel!
    @IBOutlet weak var lb_noAllergies: UILabel!
    
    @IBOutlet weak var LC_allergies_height: NSLayoutConstraint!
    @IBOutlet weak var allergiesCollectionView: UICollectionView!
  //  @IBOutlet weak var bt_add_medications: UIButton!
    
    @IBOutlet weak var bt_add_allergies: UIButton!
    
    @IBOutlet weak var lb_no_drugAllergies: UILabel!
    @IBOutlet weak var LC_drug_allergies_height: NSLayoutConstraint!
    @IBOutlet weak var drug_allergiesCollectionView: UICollectionView!
    
    @IBOutlet weak var bt_save_changes_out: UIButton!
    @IBOutlet weak var bt_bmi: UIButton!
  //  @IBOutlet weak var tv_medicalHistory: UITextView!
    @IBOutlet var btnOptions : [UIButton]!
    
  //  @IBOutlet weak var lb_no_medical_history: UILabel!
    
    @IBOutlet var iv_medicationExpand: UIImageView!
    @IBOutlet var iv_medicalHistoryExpand: UIImageView!
    ///datamodel object reference of health profile
    var dataModel : HealthProfile!
    ///Not used
    var healthProfile : HealthProfile!
    
    ///smoking status
    var smokingStatus : Int!
    
    ///medical insurance
    var medicalInsurance : Int!
    
    let config = URLSessionConfiguration.default
    var session : URLSession!
    
   static var delegate: ProfileViewAlignDelegate?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bt_add_allergies.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        addDrugAllergy.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        
        if !tf_medicalHistory.text.isEmpty{
            tf_medicalHistory.text = nil
            tf_medicalHistory.textColor = UIColor.black
        }
        else{
            tf_medicalHistory.text = "Medical History"
            tf_medicalHistory.textColor = UIColor.lightGray
        }
        
        session = URLSession(configuration: config)
    
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
             updateViewOnFetchFinish()
        }else {
             updateViewOnFetchFinish()
           //  getHealthProfile(session: session)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateViewOnFetchFinish), name: NSNotification.Name(rawValue: "UpdateHealthProfile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.toStopAnimating), name: NSNotification.Name(rawValue: "StopAnimating"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.toStartAnimating), name: NSNotification.Name(rawValue: "StartAnimating"), object: nil )
        
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_medicationExpand.transform = CGAffineTransform(rotationAngle: (3 * CGFloat.pi) / 2)
            self.iv_pregnancyExpand.transform = CGAffineTransform(rotationAngle: (3 * CGFloat.pi) / 2)
        })

        let swipedDown = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwipeDown(gesture:)))
        swipedDown.direction = .down
        self.vw_back.addGestureRecognizer(swipedDown)
    
        setOptionsIds()
        //print("Get Gender---->",self.getGender()+"<-----")
        let gender : String = self.getGender()
        if gender == "Female" {
            self.LC_pregnancy_height.constant = 42.0
            self.LC_preg_title_height.constant = 14.5
            self.LC_preg_sep_height.constant = 1.0
            iv_pregnancyExpand.image = UIImage(named: "EXPAND")
            LC_preg_bottom_height.constant = 15.0
        }
        else{
            self.LC_pregnancy_height.constant = 0.0
            self.LC_preg_title_height.constant = 0.0
            self.LC_preg_sep_height.constant = 0.0
            iv_pregnancyExpand.image = UIImage()
            LC_preg_bottom_height.constant = 0.0
        }
    }
    
    @objc func didSwipeDown(gesture:UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizer.Direction.right:
            print("Swiped right")
        case UISwipeGestureRecognizer.Direction.down:
         //   HealthProfileViewController.delegate?.didScrollDown()
            print("Swiped down>>>>>")
        case UISwipeGestureRecognizer.Direction.left:
            print("Swiped left")
        case UISwipeGestureRecognizer.Direction.up:
            print("Swiped up")
        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_bmi.backgroundColor = Theme.buttonBackgroundColor
        bt_save_changes_out.backgroundColor = Theme.buttonBackgroundColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.adjustViewHeight()
    }

    ///stops loading animation
   @objc func toStopAnimating() {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
    
    ///starts loading animation
   @objc func toStartAnimating() {
        DispatchQueue.main.async {
            self.startAnimating()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
//        if session != nil {
//            session.invalidateAndCancel()
//        }
    }
    
    
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text.isEmpty == true {
//            self.lb_no_medical_history.isHidden = false
//        }else {
//            self.lb_no_medical_history.isHidden = true
//        }
//    }
//    
//    
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text.isEmpty == true {
//            self.lb_no_medical_history.isHidden = false
//        }el @objcse {
//            self.lb_no_medical_history.isHidden = true
//        }
//    }
    
    func getGender() -> String {
        let userDataModel : User  = User.get_user_profile_default()
        var gender = ""
        if userDataModel.gender != nil {
            gender = userDataModel.gender!
        }
        return gender
    }
    
    func setOptionsIds() {
    
        if App.masterData.life_style_activity != nil{
            let result_life = App.masterData.life_style_activity!.filter { [self.tf_smoking.text].contains($0.first_level_value) }
            if result_life.count > 0 { btnOptions[ButtonOptionsIndex.lifeStyle].tag = result_life[0].id!}
        }
        if App.masterData.sleep_duration != nil{
            let result_sleep_style = App.masterData.sleep_duration!.filter { [self.tf_total_sleep.text].contains($0.first_level_value) }
            if result_sleep_style.count > 0 { btnOptions[ButtonOptionsIndex.sleepStyle].tag = result_sleep_style[0].id!}
        }
        if App.masterData.sleep_pattern != nil{
            let result_sleep_pattern = App.masterData.sleep_pattern!.filter { [self.tf_pattern_sleep.text].contains($0.first_level_value) }
            if result_sleep_pattern.count > 0 { btnOptions[ButtonOptionsIndex.sleepPattern].tag = result_sleep_pattern[0].id!}
        }
        if App.masterData.exercise_per_week != nil{
            let result_exercise = App.masterData.exercise_per_week!.filter { [self.tf_exercise.text].contains($0.first_level_value) }
            if result_exercise.count > 0 { btnOptions[ButtonOptionsIndex.exercise].tag = result_exercise[0].id!}
        }
        if App.masterData.marital_status != nil{
            let result_marital = App.masterData.marital_status!.filter { [self.tf_marital_status.text].contains($0.first_level_value) }
            if result_marital.count > 0 { btnOptions[ButtonOptionsIndex.marital].tag = result_marital[0].id!}
        }
        if App.masterData.switch_in_medicine != nil{
            let result_switch = App.masterData.switch_in_medicine!.filter { [self.tf_switch_medicin.text].contains($0.first_level_value) }
            if result_switch.count > 0 {
                btnOptions[ButtonOptionsIndex.switchMedicine].tag = result_switch[0].id!
            }
            else{
                var txt_switch = ""
                let res_switch = App.masterData.switch_in_medicine!.filter { [self.dataModel.switch_in_medicine].contains($0.id) }
                if res_switch.count > 0 { txt_switch = res_switch[0].first_level_value!}
                if txt_switch == "Others" {
                    btnOptions[ButtonOptionsIndex.switchMedicine].tag = self.dataModel.switch_in_medicine
                }
            }
            
        }
        if App.masterData.medical_history != nil{
            let result_history = App.masterData.medical_history!.filter { [self.tf_medicalHistory.text].contains($0.first_level_value) }
            if result_history.count > 0 { btnOptions[ButtonOptionsIndex.MedicalHistory].tag = result_history[0].id!}
        }
        
        
        if self.tf_medical_insurance.text == "Yes"{
            btnOptions[ButtonOptionsIndex.MedicalInsurance].tag = 1
        }
        else{
            btnOptions[ButtonOptionsIndex.MedicalInsurance].tag = 0
        }
        
        for obj in btnOptions {
            print("btn.tag : ",obj.tag)
        }
        
 
    }
    
    /**
     Update ui values on load
     */
   @objc func updateViewOnFetchFinish() {
        print("\(#function) called")
        if HealthProfile.check_Health_defaults() {
            print("already stored health profile")
            self.dataModel = HealthProfile.get_health_profile_default()
            if self.dataModel != nil {
                if self.dataModel.weight == 0 {
                    self.tf_weight.text = ""
                }else {
                    self.tf_weight.text = String(self.dataModel.weight)
                }
                
                if self.dataModel.height == 0 {
                    self.tf_height.text = ""
                }else {
                    self.tf_height.text =  String(self.dataModel.height)
                }
                
                //print("Does smoke==>\(self.dataModel.does_smoke)")
                
                /*if self.dataModel.does_smoke.isEmpty == true {
                    self.tf_smoking.text = "No"
                }else {
                    self.tf_smoking.text = self.dataModel.does_smoke
                }*/
                
                if App.masterData.life_style_activity != nil{
                    let result_life = App.masterData.life_style_activity!.filter { [self.dataModel.life_style].contains($0.id) }
                    if result_life.count > 0 { self.tf_smoking.text =  result_life[0].first_level_value!}
                }
                
                if App.masterData.medical_history != nil{
                    let result_history = App.masterData.medical_history!.filter { [self.dataModel.medical_history_new].contains($0.id) }
                    if result_history.count > 0 { self.tf_medicalHistory.text = result_history[0].first_level_value!}
                }
                if App.masterData.sleep_duration != nil{
                    let result_sleep_style = App.masterData.sleep_duration!.filter { [self.dataModel.sleep_duration].contains($0.id) }
                    if result_sleep_style.count > 0 { self.tf_total_sleep.text = result_sleep_style[0].first_level_value!}
                }
                if App.masterData.sleep_pattern != nil{
                    let result_sleep_pattern = App.masterData.sleep_pattern!.filter { [self.dataModel.sleep_pattern].contains($0.id) }
                    if result_sleep_pattern.count > 0 { self.tf_pattern_sleep.text = result_sleep_pattern[0].first_level_value!}
                }
                if App.masterData.exercise_per_week != nil{
                    let result_exercise = App.masterData.exercise_per_week!.filter { [self.dataModel.exercise_per_week].contains($0.id) }
                    if result_exercise.count > 0 { self.tf_exercise.text = result_exercise[0].first_level_value!}
                }
                if App.masterData.marital_status != nil{
                    let result_marital = App.masterData.marital_status!.filter { [self.dataModel.marital_status].contains($0.id) }
                    if result_marital.count > 0 { self.tf_marital_status.text = result_marital[0].first_level_value!}
                }
                if App.masterData.switch_in_medicine != nil{
                    let result_switch = App.masterData.switch_in_medicine!.filter { [self.dataModel.switch_in_medicine].contains($0.id) }
                    if result_switch.count > 0 { self.tf_switch_medicin.text = result_switch[0].first_level_value!}
                }
                
                if self.dataModel.medical_insurance == 0 {
                    self.tf_medical_insurance.text = "No"
                }else {
                    self.tf_medical_insurance.text = "Yes"
                }
                
                if self.tf_switch_medicin.text == "Others" {
                    self.tf_switch_medicin.text = self.dataModel.switch_in_medicine_reason
                }
                

                /*
                if self.dataModel.life_style.isEmpty {
                    self.tf_smoking.text = ""
                }else {
                    self.tf_smoking.text =  String(self.dataModel.life_style)
                }
                
                self.tf_medicalHistory.text = self.dataModel.medical_history_new//self.dataModel.medical_history
                self.tf_total_sleep.text = self.dataModel.sleep_duration
                self.tf_pattern_sleep.text = self.dataModel.sleep_pattern
                self.tf_exercise.text = self.dataModel.exercise_per_week
                self.tf_marital_status.text = self.dataModel.marital_status
                self.tf_switch_medicin.text = self.dataModel.switch_in_medicine
                
                if self.dataModel.medical_insurance.isEmpty == true {
                    self.tf_medical_insurance.text = "No"
                }else {
                    self.tf_medical_insurance.text = self.dataModel.medical_insurance
                }*/

                if App.allergies.count == 0{
                    App.allergies = HealthProfile.getAllergies()
                }
                
                if App.medications.count == 0{
                    App.medications = HealthProfile.getMedicaions() 
                }
                
            }
        }else {
             print("Not stored health profile")
        }
        adjustViewHeight()
    }
    
    /**
     text view delegate method
     */
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText = textView.text  as NSString?
        let updatedText = currentText?.replacingCharacters(in: range, with: text)
        //let updatedText = currentText?.replacingCharacters(in: range as Range, with: text)
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view

        if (updatedText?.isEmpty)! || updatedText! == "No history"{
         //   self.lb_no_medical_history.isHidden = false
            textView.text = ""
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
         //   self.lb_no_medical_history.isHidden = true
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        return true
    }
    
    /**
       the height of view adjusts according to medications and allergies list
     */
    func adjustViewHeight() {
        print("\(#function) called")

        if App.allergies.count == 0 {
            self.lb_noAllergies.isHidden = false
            self.allergiesCollectionView.reloadData()
            self.allergiesCollectionView.isHidden = true
            self.LC_allergies_height.constant = self.allergiesCollectionView.contentSize.height
        }else if App.allergies.count == 1 {
            self.lb_noAllergies.isHidden = true
            self.allergiesCollectionView.reloadData()
            self.allergiesCollectionView.isHidden = false
            self.LC_allergies_height.constant = self.allergiesCollectionView.contentSize.height + 22
        }else{
            self.lb_noAllergies.isHidden = true
            self.allergiesCollectionView.reloadData()
            self.allergiesCollectionView.isHidden = false
            self.LC_allergies_height.constant = self.allergiesCollectionView.contentSize.height + 22
        }
        
        if App.drug_allergies.count == 0 {
            self.lb_no_drugAllergies.isHidden = false
            self.drug_allergiesCollectionView.reloadData()
            self.drug_allergiesCollectionView.isHidden = true
            self.LC_drug_allergies_height.constant = self.drug_allergiesCollectionView.contentSize.height
        }else if App.drug_allergies.count == 1 {
            self.lb_no_drugAllergies.isHidden = true
            self.drug_allergiesCollectionView.reloadData()
            self.drug_allergiesCollectionView.isHidden = false
            self.LC_drug_allergies_height.constant = self.drug_allergiesCollectionView.contentSize.height + 22
        }else{
            self.lb_no_drugAllergies.isHidden = true
            self.drug_allergiesCollectionView.reloadData()
            self.drug_allergiesCollectionView.isHidden = false
            self.LC_drug_allergies_height.constant = self.drug_allergiesCollectionView.contentSize.height + 22
        }
        
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            self.stopAnimating()
        })
    }

    ///Deletes or edits the allergy
    @objc func allergyDeleteTapped(_ sender:UIButton) {
        App.isDrugAllergy = false
        let alert = UIAlertController(title: "Allergy", message: "", preferredStyle: .actionSheet)
        let deltet = UIAlertAction(title: "Delete", style: .default) { (action) in
           // App.deleteIndex = sender.tag
            let aler = App.allergies[sender.tag]
            let url = AppURLS.URL_Allergies + "\(aler.id!)"
            let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete \(aler.name!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: { (UIAlertAction) in
                self.startAnimating()
                
                NetworkCall.performDELETE(url: url,completionHandler: { (success, response, status, error) in
                    if success && status == 204 {
                        DispatchQueue.main.async {
                            App.allergies.remove(at: sender.tag)
                            self.adjustViewHeight()
                            self.allergiesCollectionView.reloadData()
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                })
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        let edit = UIAlertAction(title: "Edit", style: .default) { (action) in
          //  App.editIndex = sender.tag
            self.editAllergiesTapped(sender: sender)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(deltet)
        alert.addAction(edit)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    ///Deletes or edits the allergy
    @objc func drugAllergyDeleteTapped(_ sender:UIButton) {
        App.isDrugAllergy = true
        let alert = UIAlertController(title: "Drug Allergy", message: "", preferredStyle: .actionSheet)
        let deltet = UIAlertAction(title: "Delete", style: .default) { (action) in
            // App.deleteIndex = sender.tag
            let aler = App.drug_allergies[sender.tag]
            let url = AppURLS.URL_Drug_Allergies + "\(aler.id!)"
            let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete \(aler.name!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: { (UIAlertAction) in
                self.startAnimating()
                
                NetworkCall.performDELETE(url: url,completionHandler: { (success, response, status, error) in
                    if success && status == 204 {
                        DispatchQueue.main.async {
                            App.drug_allergies.remove(at: sender.tag)
                            self.adjustViewHeight()
                            self.drug_allergiesCollectionView.reloadData()
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                })
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        let edit = UIAlertAction(title: "Edit", style: .default) { (action) in
            //  App.editIndex = sender.tag
            self.editDrugAllergiesTapped(sender: sender)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(deltet)
        alert.addAction(edit)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    ///Performs add new option for Medications or Allergies
    @IBAction func addNewTapped(_ sender: UIButton) {
        
        if sender.tag == 1 {
//            UserDefaults.standard.set(false, forKey: "EditTapped")
//            addMediUrlString = ""
//            addMediHttpMethod = ""
//            addMediMedicationID = ""
//            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_MEDICATIONS_DETAILS, sender: self)
            App.editIndex = nil
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_MEDICATIONS_DETAILS, sender: self)
        }else if sender.tag == 2 {
//            let urlString = AppURLS.URL_Allergies
//            let httpMethod = HTTPMethods.POST
//            showTextFieldAlert(title: "ALLERGIE", message: "", URLString: urlString, httpmethod: httpMethod,tag: sender.tag, ActionTitle: "Add", textToEdit: "")
             App.editIndex = nil
            App.isDrugAllergy = false
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ALLERGY, sender: self)
        }
        else if sender.tag == 3 {
            App.editIndex = nil
            App.isDrugAllergy = true
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ALLERGY, sender: self)
        }
    }
    
    @IBAction func doesSmokeTapped(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        /*
        let actionView = UIAlertController(title: "Any Life style activities?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let selfAction = UIAlertAction(title: "Alcoholic + Smoker", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_smoking.text = "Alcoholic + Smoker"
            self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "Only Alcoholic", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_smoking.text = "Only Alcoholic"
            self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let onlySmoker = UIAlertAction(title: "Only Smoker", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_smoking.text = "Only Smoker"
            self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let none = UIAlertAction(title: "None of the two", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_smoking.text = "None of the two"
            self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        actionView.addAction(onlySmoker)
        actionView.addAction(none)
        self.present(actionView, animated: true, completion: nil)*/
        
        let actionView = UIAlertController(title: "Any Life style activities?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.life_style_activity != nil{
        for obj in App.masterData.life_style_activity! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.tf_smoking.text = obj.first_level_value
                self.btnOptions[ButtonOptionsIndex.lifeStyle].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_smoking_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
        
    }
    
    @IBAction func totalSleepTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Total Sleep duration?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        /*
        let selfAction = UIAlertAction(title: "< 3hrs", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_total_sleep.text = "< 3hrs"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "3hrs - 6hrs", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_total_sleep.text = "3hrs - 6hrs"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let onlySmoker = UIAlertAction(title: "6hrs - 8hrs", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_total_sleep.text = "6hrs - 8hrs"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let none = UIAlertAction(title: "> 8hrs", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_total_sleep.text = "> 8hrs"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        actionView.addAction(onlySmoker)
        actionView.addAction(none)
        self.present(actionView, animated: true, completion: nil)*/
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.sleep_duration != nil{
        for obj in App.masterData.sleep_duration! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.tf_total_sleep.text = obj.first_level_value
                self.btnOptions[ButtonOptionsIndex.sleepStyle].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func patternSleepTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_patternSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Sleep pattern?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        /*
        let selfAction = UIAlertAction(title: "Disturbed", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pattern_sleep.text = "Disturbed"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_patternSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "Sound Sleep", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pattern_sleep.text = "Sound Sleep"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_patternSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_patternSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        self.present(actionView, animated: true, completion: nil)*/
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_patternSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.sleep_pattern != nil{
        for obj in App.masterData.sleep_pattern! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.tf_pattern_sleep.text = obj.first_level_value
                self.btnOptions[ButtonOptionsIndex.sleepPattern].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_patternSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func exerciseTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_exerciseExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Exercise Per week?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        /*
        let selfAction = UIAlertAction(title: "<2 hours – Sedentary", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_exercise.text = "<2 hours – Sedentary"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_exerciseExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "> 2 hours – Active", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_exercise.text = "> 2 hours – Active"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_exerciseExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_exerciseExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        self.present(actionView, animated: true, completion: nil)*/
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_exerciseExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.exercise_per_week != nil{
        for obj in App.masterData.exercise_per_week! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.tf_exercise.text = obj.first_level_value
                self.btnOptions[ButtonOptionsIndex.exercise].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_exerciseExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func pregnancyTapped(_ sender: UIButton) {
        /*
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Pregnancy Status?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let selfAction = UIAlertAction(title: "Expecting mother", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pregnancy.text = "Expecting mother"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_pregnancyExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "No. of conceptions", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pregnancy.text = "No. of conceptions"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_pregnancyExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let onlySmoker = UIAlertAction(title: "Abortions", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pregnancy.text = "Abortions"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_pregnancyExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let pregnancy = UIAlertAction(title: "Pregnancy related complications if any", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pregnancy.text = "Pregnancy related complications if any"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_pregnancyExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let none = UIAlertAction(title: "None of the above/Prefer not to say", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_pregnancy.text = "None of the above/Prefer not to say"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_pregnancyExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_totalSleepExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        actionView.addAction(onlySmoker)
        actionView.addAction(pregnancy)
        actionView.addAction(none)
        self.present(actionView, animated: true, completion: nil)*/
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_PREGNANCY_STATUS, sender: self)
    }
    
    @IBAction func maritalStatusTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Marital Status?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        /*
        let selfAction = UIAlertAction(title: "Single", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_marital_status.text = "Single"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "Married", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_marital_status.text = "Married"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let onlySmoker = UIAlertAction(title: "Divorced/Widow/Widower", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_marital_status.text = "Divorced/Widow/Widower"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let pregnancy = UIAlertAction(title: "Prefer not to say", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_marital_status.text = "Prefer not to say"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        actionView.addAction(onlySmoker)
        actionView.addAction(pregnancy)
        self.present(actionView, animated: true, completion: nil)*/
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.marital_status != nil{
        for obj in App.masterData.marital_status! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.tf_marital_status.text = obj.first_level_value
                self.btnOptions[ButtonOptionsIndex.marital].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_maritalExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func switchMedicinTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Reasons for Switch in the medicines?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        /*
        let selfAction = UIAlertAction(title: "Change in doctor", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_switch_medicin.text = "Change in doctor"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "Medicine not effective", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_switch_medicin.text = "Medicine not effective"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let onlySmoker = UIAlertAction(title: "Diagnosed with new ailments", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_switch_medicin.text = "Diagnosed with new ailments"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let pregnancy = UIAlertAction(title: "Others", style: UIAlertAction.Style.default) { (UIAlertAction) in
            //self.tf_switch_medicin.text = "Others"
            self.showOtherTextFieldAlert(title: "Others", message: "Drug allergies", ActionTitle: "Ok")
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        actionView.addAction(onlySmoker)
        actionView.addAction(pregnancy)
        self.present(actionView, animated: true, completion: nil)*/
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.switch_in_medicine != nil{
        for obj in App.masterData.switch_in_medicine! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                if obj.first_level_value == "Others"{
                     self.showOtherTextFieldAlert(title: "Others", message: "", ActionTitle: "Ok")
                }
                else{
                    self.tf_switch_medicin.text = obj.first_level_value
                }
                self.btnOptions[ButtonOptionsIndex.switchMedicine].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func medicinInsuranceTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Do you have Medical Insurance?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let selfAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_medical_insurance.text = "Yes"
            self.btnOptions[ButtonOptionsIndex.MedicalInsurance].tag = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_medical_insurance.text = "No"
            self.btnOptions[ButtonOptionsIndex.MedicalInsurance].tag = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func medicinHistoryTapped(_ sender: UIButton) {
        
        if !tf_medicalHistory.text.isEmpty{
            tf_medicalHistory.text = nil
            tf_medicalHistory.textColor = UIColor.black
        }
        else{
            tf_medicalHistory.text = "Medical History"
            tf_medicalHistory.textColor = UIColor.lightGray
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Medical History?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        /*
        let selfAction = UIAlertAction(title: "Surgical history", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_medicalHistory.text = "Surgical history"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let onlyAlcoholic = UIAlertAction(title: "Any hospitalization done", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_medicalHistory.text = "Any hospitalization done"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let onlySmoker = UIAlertAction(title: "Past Medical History if any", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_medicalHistory.text = "Past Medical History if any"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let pregnancy = UIAlertAction(title: "None", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_medicalHistory.text = "None"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        actionView.addAction(onlySmoker)
        actionView.addAction(pregnancy)
        self.present(actionView, animated: true, completion: nil)*/
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if App.masterData.medical_history != nil{
        for obj in App.masterData.medical_history! {
            let selfAction = UIAlertAction(title: obj.first_level_value, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.tf_medicalHistory.text = obj.first_level_value
                self.btnOptions[ButtonOptionsIndex.MedicalHistory].tag = obj.id!
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_medicalHistoryExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            }
            actionView.addAction(selfAction)
        }
        }
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func cancelTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BMITapped(_ sender: UIButton) {
        let enteredWeight = self.tf_weight.text!
        let enteredHeight = self.tf_height.text!
        
        if enteredWeight.isEmpty == true {
            self.didShowAlert(title: "Weight required!", message: "")
            return
        }else if enteredWeight.isEqual(".") {
            self.didShowAlert(title: "Weight required!", message: "Please enter correct weight")
            return
        }else if enteredHeight.isEmpty == true {
            self.didShowAlert(title: "Height required!", message: "")
            return
        }else if enteredHeight.isEqual(".") {
            self.didShowAlert(title: "Height required!", message: "Please enter correct height")
            return
        }
        
        if let height = enteredHeight.convertStringToNumberalIfDifferentLanguage() , let weight = enteredWeight.convertStringToNumberalIfDifferentLanguage() {
              let valHeight : Double = height.doubleValue
              let  valWeight : Double = weight.doubleValue
              print("Height::=>\(valHeight) weight:\(valWeight)")
                if(valHeight <= 51.0 || valHeight >= 275.0)
                {
                    self.didShowAlert(title: "Info", message: "Height must be greater than 51cm and less than 275 cm")
                    return
                }
                
                if(valWeight <= 2.0 || valWeight >= 500.0)
                {
                    self.didShowAlert(title: "Info", message: "Weight must be greater than 2kg's and less than 500 kg's")
                    return
                }
                
                performSegue(withIdentifier: StoryboardSegueIDS.ID_BMI_SEGUE, sender: self)
            
        }else {
              self.didShowAlert(title: "Info", message: "Please enter a valid weight or height")
        }
    }

    
    @IBAction func saveChangesTapped(_ sender: UIButton) {
        
        let weight = self.tf_weight.text!
        let height = self.tf_height.text!
        //let smoking = self.tf_smoking.text!
        let insurance = self.tf_medical_insurance.text!
        //let medicalHistory = self.tf_medicalHistory.text!
        
        if weight.isEmpty == true {
            self.didShowAlert(title: "Weight required!", message: "")
          //  AlertView.sharedInsance.showInfo(title: "Weight required!", message: "")
        }else if height.isEmpty == true {
            self.didShowAlert(title: "Height required!", message: "")
          //  AlertView.sharedInsance.showInfo(title: "Height required!", message: "")
        }
            /*
        else if smoking.isEmpty == true {
            self.didShowAlert(title: "Do you smoke?", message: "Please mension")
           // AlertView.sharedInsance.showInfo(title: "Do you smoke?", message: "please mension")
        }*/
        else {
            view.resignFirstResponder()
            startAnimating()
            
            if insurance == "No" {
                medicalInsurance = 0
            }else {
                medicalInsurance = 1
            }
            /*
            if smoking == "No" {
                smokingStatus = 0
            }else {
                smokingStatus = 1
            }*/
            
            var isOthers = false
            let result_switch = App.masterData.switch_in_medicine!.filter { ["Others"].contains($0.first_level_value) }
            if result_switch.count > 0 {
                if btnOptions[ButtonOptionsIndex.switchMedicine].tag == result_switch[0].id!
                {
                    isOthers = true
                }
            }
            
            
//            if  medicalHistory ==  "No history" {
//                medicalHistory = ""
//            }
            
            //  let userData = "{\"\(Keys.KEY_WEIGHT)\": \(weight), \"\(Keys.KEY_HEIGHT)\": \(height) , \"\(Keys.KEY_DOES_SMOKE)\":\(self.smokingStatus!), \"\(Keys.KEY_MEDICAL_HISTORY)\":\"\(medicalHistory)\"}"
            //let stringData = "\(Keys.KEY_WEIGHT)=\(weight)&\(Keys.KEY_HEIGHT)=\(height)&\(Keys.KEY_DOES_SMOKE)=\(self.smokingStatus!)&\(Keys.KEY_MEDICAL_HISTORY)=\(medicalHistory)"//&medical_insurance=0&sleep_duration=3hrs - 6hrs
            let stringData = "\(Keys.KEY_WEIGHT)=\(weight)&\(Keys.KEY_HEIGHT)=\(height)&\(Keys.KEY_LIFE_STYLE_ID)=\(btnOptions[ButtonOptionsIndex.lifeStyle].tag)&\(Keys.KEY_SLEEP_DURATION_ID)=\(btnOptions[ButtonOptionsIndex.sleepStyle].tag)&\(Keys.KEY_EXERCISE_PER_WEEK_ID)=\(btnOptions[ButtonOptionsIndex.exercise].tag)&\(Keys.KEY_SLEEP_PATTERN_ID)=\(btnOptions[ButtonOptionsIndex.sleepPattern].tag)&\(Keys.KEY_MEDICAL_INSURANCE)=\(btnOptions[ButtonOptionsIndex.MedicalInsurance].tag)&\(Keys.KEY_MEDICAL_HISTORY_NEW_ID)=\(btnOptions[ButtonOptionsIndex.MedicalHistory].tag)&\(Keys.KEY_SWITCH_MEDICINE_ID)=\(btnOptions[ButtonOptionsIndex.switchMedicine].tag)&\(Keys.KEY_MARITAL_STATUS_ID)=\(btnOptions[ButtonOptionsIndex.marital].tag)&\(Keys.KEY_SWITCH_MEDICINE_OTHERS)=\(isOthers ? self.tf_switch_medicin.text! : "")"
            
            // let dataToPost = ["\(Keys.KEY_WEIGHT)=\(weight),\(Keys.KEY_HEIGHT)=\(height),\(Keys.KEY_DOES_SMOKE)=\(self.smokingStatus!),\(Keys.KEY_MEDICAL_HISTORY)=\(medicalHistory)"]
            print("Health profile data:\(stringData)")
            let urlString = AppURLS.URL_HealthProfile
            let url = URL(string: urlString)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.PATCH
            
            print("URL health==>\(urlString)")
            print("HTTPMethod==>\(HTTPMethods.PATCH)")
            request.httpBody = stringData.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let error = error {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                      //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data{
                    print("data =\(data)")
                }
                if let response = response {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do{
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling save helath profile:\(resultJSON)")
                        }
                        else {
                            print("==>Health response : \(resultJSON)")
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let responseStatus = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(responseStatus) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            if code == 200 && responseStatus == "success" || code == 201 && responseStatus == "success"
                            {
                                var doesSmoke = 0
                                var med_insurance = 0
                                
                                let height = data.object(forKey: Keys.KEY_HEIGHT) as! Double
                                let weight = data.object(forKey: Keys.KEY_WEIGHT) as! Double
                                let medicalHistory = data.object(forKey: Keys.KEY_MEDICAL_HISTORY) as! String
                                let switch_in_medicine_reason = data.object(forKey: Keys.KEY_SWITCH_MEDICINE_OTHERS) as! String
                                if let smoking = data.object(forKey: Keys.KEY_DOES_SMOKE) as? Int {
                                    doesSmoke = smoking
                                }
                                var smoke = "No"
                                if doesSmoke == 1 {
                                    smoke = "Yes"
                                }
                                let life_style = data.object(forKey: Keys.KEY_LIFE_STYLE_ID) as! Int
                                let sleep_duration = data.object(forKey: Keys.KEY_SLEEP_DURATION_ID) as! Int
                                let sleep_pattern = data.object(forKey: Keys.KEY_SLEEP_PATTERN_ID) as! Int
                                let exercise_per_week = data.object(forKey: Keys.KEY_EXERCISE_PER_WEEK_ID) as! Int
                                let marital_status = data.object(forKey: Keys.KEY_MARITAL_STATUS_ID) as! Int
                                let switch_in_medicine = data.object(forKey: Keys.KEY_SWITCH_MEDICINE_ID) as! Int
                                let medical_history_new = data.object(forKey: Keys.KEY_MEDICAL_HISTORY_NEW_ID) as! Int
                                if let med_ins = data.object(forKey: Keys.KEY_MEDICAL_INSURANCE) as? Int {
                                    med_insurance = med_ins
                                }
                                var medical_insurance = 0
                                if med_insurance == 1 {
                                    medical_insurance = 1
                                }
                                
                                if let mdictions = data.object(forKey: Keys.KEY_MASTER_DATA) as? NSDictionary {
                                    
                                    print("Master Data fetched :\(mdictions)")
                                    
                                    var medical_history_option = [OptionsData]()
                                    var life_style_option = [OptionsData]()
                                    var sleep_duration_option = [OptionsData]()
                                    var sleep_pattern_option = [OptionsData]()
                                    var exercise_option = [OptionsData]()
                                    var marital_option = [OptionsData]()
                                    var switch_in_medicine_option = [OptionsData]()
                                    App.masterData = MasterData()
                                    
                                    if let exercise_per = mdictions.object(forKey: Keys.KEY_EXERCISE_PER_WEEK) as? [NSDictionary]{
                                        for options in exercise_per {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_exercise = OptionsData(id: id, first_level_value: optional_name)
                                            exercise_option.append(temp_exercise)
                                            if exercise_per_week == temp_exercise.id
                                            {
                                                self.tf_exercise.text = temp_exercise.first_level_value
                                            }
                                        }
                                    }
                                    if let life_style_activity = mdictions.object(forKey: Keys.KEY_LIFE_STYLE) as? [NSDictionary]{
                                        for options in life_style_activity {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_life_style = OptionsData(id: id, first_level_value: optional_name)
                                            life_style_option.append(temp_life_style)
                                            if life_style == temp_life_style.id
                                            {
                                                self.tf_smoking.text  = temp_life_style.first_level_value
                                            }
                                        }
                                    }
                                    if let marital_stat = mdictions.object(forKey: Keys.KEY_MARITAL_STATUS) as? [NSDictionary]{
                                        for options in marital_stat {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_marital = OptionsData(id: id, first_level_value: optional_name)
                                            marital_option.append(temp_marital)
                                            if marital_status == temp_marital.id
                                            {
                                                self.tf_marital_status.text  = temp_marital.first_level_value
                                            }
                                        }
                                    }
                                    if let medical_history = mdictions.object(forKey: Keys.KEY_MEDICAL_HISTORY) as? [NSDictionary]{
                                        for options in medical_history {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_medical_history = OptionsData(id: id, first_level_value: optional_name)
                                            medical_history_option.append(temp_medical_history)
                                            if medical_history_new == temp_medical_history.id
                                            {
                                                self.tf_medicalHistory.text  = temp_medical_history.first_level_value
                                            }
                                        }
                                    }
                                    if let sleep_dur =  mdictions.object(forKey: Keys.KEY_SLEEP_DURATION) as? [NSDictionary]{
                                        for options in sleep_dur {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_sleep_duration = OptionsData(id: id, first_level_value: optional_name)
                                            sleep_duration_option.append(temp_sleep_duration)
                                            if sleep_duration == temp_sleep_duration.id
                                            {
                                                self.tf_total_sleep.text  = temp_sleep_duration.first_level_value
                                            }
                                        }
                                    }
                                    if let sleep_pat =  mdictions.object(forKey: Keys.KEY_SLEEP_PATTERN) as? [NSDictionary]{
                                        for options in sleep_pat {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_sleep_pattern = OptionsData(id: id, first_level_value: optional_name)
                                            sleep_pattern_option.append(temp_sleep_pattern)
                                            if sleep_pattern == temp_sleep_pattern.id
                                            {
                                                self.tf_pattern_sleep.text  = temp_sleep_pattern.first_level_value
                                            }
                                        }
                                    }
                                    if let switch_in =  mdictions.object(forKey: Keys.KEY_SWITCH_MEDICINE) as? [NSDictionary]{
                                        for options in switch_in {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_switch_in_medicine = OptionsData(id: id, first_level_value: optional_name)
                                            switch_in_medicine_option.append(temp_switch_in_medicine)
                                            if switch_in_medicine == temp_switch_in_medicine.id
                                            {
                                                self.tf_switch_medicin.text  = temp_switch_in_medicine.first_level_value
                                            }
                                        }
                                    }
                                    
                                    
                                    let master = MasterData(exercise_per_week: exercise_option, life_style_activity: life_style_option, marital_status: marital_option, medical_history: medical_history_option, sleep_duration: sleep_duration_option, sleep_pattern: sleep_pattern_option, switch_in_medicine: switch_in_medicine_option)
                                    App.masterData = master
                                    print("MasterData=\( App.masterData)")
                                    
                                    let jsonEncoder = JSONEncoder()
                                    do {
                                        let encodedData = try jsonEncoder.encode(App.masterData)
                                        UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MASTER_DATA)
                                    }catch let err{
                                        print("Error while encoding master data :\(err)")
                                    }
                                    
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    self.view.endEditing(true)
                                    if weight == 0 {
                                        self.tf_weight.text = ""
                                    }else {
                                        self.tf_weight.text = String(weight)
                                    }
                                    
                                    if height == 0 {
                                        self.tf_height.text = ""
                                    }else {
                                        self.tf_height.text =  String(height)
                                    }
                                    //self.tf_smoking.text = smoke

                                    if medicalHistory.isEmpty == true || medicalHistory == "No history"{
//                                        self.tv_medicalHistory.text = ""
//                                        self.tv_medicalHistory.textColor = UIColor.lightGray
                                    }else {
//                                        self.tv_medicalHistory.text =  medicalHistory
//                                        self.tv_medicalHistory.textColor = UIColor.black
                                    }
                                    

                                    self.didShowAlert(title: responseStatus.capitalizingFirstLetter() , message: "Health profile updated")
                                   // AlertView.sharedInsance.showSuccessAlert(title: responseStatus, message: "Health profile updated")
                                    self.view.endEditing(true)
                                })
                                
                                //let health = HealthProfile(weight: weight, height: height, does_smoke: smoke, medical_history: medicalHistory, allergies: App.allergies, medications: App.medications)
                                let health = HealthProfile(weight: weight, height: height, does_smoke: smoke, medical_history: medicalHistory, allergies: App.allergies, medications: App.medications, life_style: life_style, sleep_duration:sleep_duration,sleep_pattern: sleep_pattern,exercise_per_week:exercise_per_week,marital_status:marital_status,switch_in_medicine:switch_in_medicine,medical_history_new:medical_history_new,medical_insurance:medical_insurance,switch_in_medicine_reason:switch_in_medicine_reason)
                                let dataModel = health
                                HealthProfile.store_health_profile_default(dataModel: dataModel)
                                
                            }else {
                                print("Somethong wrong...")
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                            })
                        }
                    }catch let error
                    {
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON:\(error.localizedDescription)")
                    }
                }
            })
            task.resume()
            
            //            self.perfromRequest(body: userData, URLString: url, httpmethod: HTTPMethods.PATCH, tag: 7)
        }
    }

    
    

    /**
     Used to get the string width
     - Parameter text: pass the string to get the width
     - Parameter font: use the font which you want
     - Returns CGFloat: width of the string
     */
    func textWidthAndHeight(text: String, font: UIFont?) -> (CGFloat,CGFloat) {
        let attributes = font != nil ? [NSAttributedString.Key.font: font!] : [:]
        return (text.size(withAttributes: attributes).width,text.size(withAttributes: attributes).height)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_BMI_SEGUE {
            let destVC = segue.destination as! BMIViewController
            destVC.weight = self.tf_weight.text
            destVC.height = self.tf_height.text
            destVC.isFromDashboard = false
        }else if segue.identifier == StoryboardSegueIDS.ID_ADD_ALLERGY {
            if let destVC = segue.destination as? AddAllergyViewController {
                destVC.delegate = self
            }
        }
    }
    

    /**
     Method used to show alert with custom title and message the text to edit in text field
     - Parameter title: pass the title of the alert
     - Parameter message: pass the message for alert
     - Parameter ActionTitle: Pass the title for action to perform
     - Parameter URLString: takes the urlstring of action
     - Parameter httpmethod: Pass the httpmethod here
     - Parameter tag: to know what kind of request is going and perform action
     - Parameter textToEdit: Pass the text of allergy to edit
     */
    func showOtherTextFieldAlert(title:String,message:String,ActionTitle:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter here.."
            textField.font = UIFont(name: "Helvetica", size: 20)
            textField.text = ""
            if App.masterData.switch_in_medicine != nil{
                for obj in App.masterData.switch_in_medicine! {
                    if self.btnOptions[ButtonOptionsIndex.switchMedicine].tag == obj.id!
                    {
                        if obj.first_level_value == "Others"
                        {
                            textField.text = self.tf_switch_medicin.text
                            break
                        }
                    }
                }
                let result_switch = App.masterData.switch_in_medicine!.filter { [self.tf_switch_medicin.text].contains($0.first_level_value) }
                if result_switch.count > 0 {
                    textField.text = ""
                }
            }
            textField.addTarget(self, action: #selector(self.textOtherChanged(_:)), for: .editingChanged)
        }
        
        let action = UIAlertAction(title: ActionTitle, style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            
            print("Text field: \(textField?.text!)")
            if textField?.text?.isEmpty == true {
                print("Not entered any text")
            }/*else if !self.containsOnlyLetters(input: (textField?.text)!) {
                print("Not entered any text")
            }*/else {
                let textBody = textField?.text!
                print("drug allergy = \(String(describing: textBody))")
                self.tf_switch_medicin.text = textBody
            }
            
        })
        
        
        alert.addAction(action)
        alert.actions[0].isEnabled = false
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension HealthProfileViewController : AddAllergyDelegate {
    func didAddAllergy(_ success: Bool) {
        if success {
            self.adjustViewHeight()
            self.allergiesCollectionView.reloadData()
            self.navigationController?.popViewController(animated: true)
        }
    }
    func didEditAllergy(_ success: Bool) {
        if success {
            self.adjustViewHeight()
            self.drug_allergiesCollectionView.reloadData()
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension HealthProfileViewController : UIScrollViewDelegate {

    
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            print(" you reached end of the table")
            HealthProfileViewController.delegate?.didScrollUp()
        }
    }
}

extension HealthProfileViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.allergiesCollectionView
        {
            return App.allergies.count
        }
        else
        {
            return App.drug_allergies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.allergiesCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Allergys", for: indexPath) as! AllergiesCollectionViewCell
            cell.bt_allergyDelete.tag = indexPath.row
            let allergy = App.allergies[indexPath.row]
            
            if let name = allergy.name {
                cell.lb_allergyName.text = name
            }
            cell.bt_allergyDelete.addTarget(self, action: #selector(self.allergyDeleteTapped(_:)), for: .touchUpInside)
            
            cell.bt_allergyDelete.layer.cornerRadius =   cell.bt_allergyDelete.frame.width / 2
            cell.bt_allergyDelete.clipsToBounds = true
            
            cell.layer.cornerRadius =  4
            cell.clipsToBounds = true
        

            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DrugAllergys", for: indexPath) as! AllergiesCollectionViewCell
            cell.bt_allergyDelete.tag = indexPath.row
            let allergy = App.drug_allergies[indexPath.row]
            
            if let name = allergy.name {
                cell.lb_allergyName.text = name
            }
            cell.bt_allergyDelete.addTarget(self, action: #selector(self.drugAllergyDeleteTapped(_:)), for: .touchUpInside)
            
            cell.bt_allergyDelete.layer.cornerRadius =   cell.bt_allergyDelete.frame.width / 2
            cell.bt_allergyDelete.clipsToBounds = true
            
            cell.layer.cornerRadius =  4
            cell.clipsToBounds = true
            
            
            return cell
        }
    }
}

extension HealthProfileViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width : CGFloat = 0.0
        var height: CGFloat = 0.0
        if collectionView == self.allergiesCollectionView
        {
            if let name = App.allergies[indexPath.row].name {
                let textheightNWidth = textWidthAndHeight(text: name, font: nil)
                 width = textheightNWidth.0
                 height = textheightNWidth.1
            }else {
                width = 30
            }
        }
        else{
            if let name = App.drug_allergies[indexPath.row].name {
                let textheightNWidth = textWidthAndHeight(text: name, font: nil)
                width = textheightNWidth.0
                height = textheightNWidth.1
            }else {
                width = 30
            }
        }
        print("Allergy width :==>\(width)")
        return CGSize(width: width + 35, height: height + 10)
    }
}

extension HealthProfileViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxNoOfDigitsBeforeDecimal = 3;
        
        let maxNoOfDigitsAfterDecimal = 2;
        
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        let seperatedString = newString?.components(separatedBy: ".")
        if (seperatedString?.count)! == 1 && (newString?.count)! > maxNoOfDigitsBeforeDecimal {
            return false
        }
        
        if (seperatedString?.count)! > 2 {
            return false
        }
        
        if (seperatedString?.count)! >= maxNoOfDigitsAfterDecimal {
            let  sepStr = String(format: "%@", (seperatedString?[1])!)
            return !(sepStr.count > maxNoOfDigitsAfterDecimal);
        }
        
     
        
        return true
    }
}

extension UIViewController {
    
    /**
      Method used to edit of selected index in medications
     */
   @objc func editMedicationTapped(sender:UIButton) {
        UserDefaults.standard.set(true, forKey: "EditTapped")
        print("index:\(sender.tag) edit medication")
        let medi = App.medications[sender.tag]
        let url = AppURLS.URL_Medications + "\(medi.id!)"
        print("Edit url==>\(url)")
        App.editIndex = sender.tag
        addMediUrlString = url
        addMediHttpMethod = HTTPMethods.PATCH
        addMediMedicationID = String(medi.id!)
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_MEDICATIONS_DETAILS, sender: self)
        //  showTextFieldAlert(title: "Edit medication", message: "", URLString: url, httpmethod: HTTPMethods.PATCH, tag@objc : 5, ActionTitle: "Update", textToEdit: medi.name!)
    }
    
    /**
     Method used to edit of selected index in allergies
     */
   @objc func editAllergiesTapped(sender:UIButton) {
        print("index:\(sender.tag) edit allergy")
        App.editIndex = sender.tag
        let aler = App.allergies[sender.tag]
        let url = AppURLS.URL_Allergies + "\(aler.id!)"
        print("Edit url==>\(url)")
        showTextFieldAlert(title: "Edit allergy", message: "", URLString: url, httpmethod: HTTPMethods.PATCH, tag: 6, ActionTitle: "Update", textToEdit: aler.name!)
    }
    
    /**
     Method used to edit of selected index in allergies
     */
    @objc func editDrugAllergiesTapped(sender:UIButton) {
        print("index:\(sender.tag) edit allergy")
        App.editIndex = sender.tag
        let aler = App.drug_allergies[sender.tag]
        let url = AppURLS.URL_Drug_Allergies + "\(aler.id!)"
        print("Edit url==>\(url)")
        //showTextFieldAlert(title: "Edit drug allergy", message: "", URLString: url, httpmethod: HTTPMethods.PATCH, tag: 6, ActionTitle: "Update", textToEdit: aler.name!)
        App.isDrugAllergy = true
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ALLERGY, sender: self)
    }
    
    /**
     Method used to delete of selected index in medications
     */
   @objc func deleteMedicationTapped(sender:UIButton) {
        print("index:\(sender.tag) delete medication")
        let medi = App.medications[sender.tag]
        let url = AppURLS.URL_Medications + "\(medi.id!)"
        App.deleteIndex = sender.tag
        print("Delete url==>\(url)")
        showAlertWithRequest(title: "Are you sure?", message: "Do you want to delete \(medi.name!)", Action: "Delete", urlString: url, httpMethod: HTTPMethods.DELETE, tag: 3)
 
    }
    
    /**
     Method used to delete of selected index in allergies
     */
   @objc func deleteAllergiesTapped(sender:UIButton) {
        App.deleteIndex = sender.tag
        print("index:\(sender.tag) delete allergy")
        let aler = App.allergies[sender.tag]
        let url = AppURLS.URL_Allergies + "\(aler.id!)"
        print("Delete url==>\(url)")
        showAlertWithRequest(title: "Are you sure?", message: "Do you want to delete \(aler.name!)", Action: "Delete", urlString: url, httpMethod: HTTPMethods.DELETE, tag: 4)
    }
    
    
    
    /**
     Method used to perform request of deleting or editing medications or allergies
     - Parameter body: pass the body data or string
     - Parameter URLString: pass the url string of perticular task
     - Parameter httpmethod: Pass the httpmethod here
     - Parameter tag: to know what kind of request is going and perform action
     */
    func  perfromRequest(body:String ,URLString :String , httpmethod :String,tag:Int ,completionHandler : @escaping (_ success:Bool) -> Void)  {
        
        print("\(#function) called")
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let text = body
        let url = URL(string: URLString)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = httpmethod
        
        if body.isEmpty == true {
            print("Data==>empty")
            print("URL==>\(URLString)")
            print("HTTPMethod==>\(httpmethod)")
        }else {
            let postData = body
            print("Data==>\(postData) tag:\(tag)")
            print("URL==>\(URLString)")
            print("HTTPMethod==>\(httpmethod)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            if let error = error {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    completionHandler(false)
                   //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data{
                print("data =\(data)")
            }
            if let response = response {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                if httpResponse.statusCode == 204 {
                    print("Delte performed tag:\(tag)")
                    if tag == 3 {
                        App.medications.remove(at: App.deleteIndex)
                        let jsonEncoder = JSONEncoder()
                        do {
                            let encodedData = try jsonEncoder.encode(App.medications)
                            UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MEDICATIONS)
                        }catch let err{
                            print("Error while encoding medicaitons to data:\(err)")
                        }
                        
//                        let encodedData = NSKeyedArchiver.archivedData(withRootObject:  App.medications)
//                        UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MEDICATIONS)
                        completionHandler(true)
                    }else  if tag == 4{
                        App.allergies.remove(at: App.deleteIndex)
                        let jsonEncoder = JSONEncoder()
                        do {
                            let encodedData = try jsonEncoder.encode(App.allergies)
                            UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_ALLERGIES)
                        }catch let err{
                            print("Error while encoding allergies to data:\(err)")
                        }
                        
//                        let encodedData1 = NSKeyedArchiver.archivedData(withRootObject:  App.allergies)
//                        UserDefaults.standard.set(encodedData1, forKey: UserDefaltsKeys.KEY_ALLERGIES)
                        completionHandler(true)
                    }
                }else {
                    //if you response is json do the following
                    do{
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling health profile perfrom request\(resultJSON)")
                            completionHandler(true)
                        }
                        else {
                            print("==>response : \(resultJSON)")
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let responseStatus = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(responseStatus) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            if code == 200 && responseStatus == "success" || code == 201 && responseStatus == "success"
                            {
                                
                                if tag == 7 {
                                    print("Updated health profile:")
                                    let height = data.object(forKey: Keys.KEY_HEIGHT) as! Double
                                    let weight = data.object(forKey: Keys.KEY_WEIGHT) as! Double
                                    let medicalHistory = data.object(forKey: Keys.KEY_MEDICAL_HISTORY) as! String
                                    let doesSmoke = data.object(forKey: Keys.KEY_DOES_SMOKE) as! Int
                                    var smoke = "No"
                                    if doesSmoke == 1 {
                                        smoke = "Yes"
                                    }
                                    
                                    let health = HealthProfile(weight: weight, height: height, does_smoke: smoke, medical_history: medicalHistory, allergies: App.allergies, medications: App.medications)
                                    let dataModel = health
                                    HealthProfile.store_health_profile_default(dataModel: dataModel)
                                    let config = URLSessionConfiguration.default
                                    let session = URLSession(configuration: config)
                                    self.getHealthProfile(session: session)
                                    DispatchQueue.main.async(execute: {
                                        self.didShowAlert(title: responseStatus, message: "Health profile updated")
                                      //  AlertView.sharedInsance.showSuccessAlert(title: responseStatus, message: "Health profile updated")
                                    })
                                     completionHandler(true)
                                }else {
                                    let id = data.object(forKey: Keys.KEY_ID) as! Int
                                    let user_id = data.object(forKey: Keys.KEY_USER_ID) as! Int
                                    let name = data.object(forKey: Keys.KEY_NAME) as! String
                                    let created_at = data.object(forKey: Keys.KEY_CREATED_AT) as! String
                                    let updated =  data.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                    var dateFrom = ""
                                    var dateTo = ""
                                    var intake = ""
                                    var notes = ""
                                    var no_of_days = 0
                                    var medication_status = 0
                                    
                                    if let dateFrm = data.object(forKey: Keys.KEY_FROM_DATE) as? String {
                                        dateFrom = dateFrm
                                    }
                                    if let toDate = data.object(forKey: Keys.KEY_TO_DATE) as? String {
                                        dateTo = toDate
                                    }
                                    if let intakeT = data.object(forKey: Keys.KEY_INTAKE_TIME) as? String {
                                        intake = intakeT
                                    }
                                    if let noteStrng = data.object(forKey: Keys.KEY_NOTES) as? String {
                                        notes = noteStrng
                                    }
                                    if  let no_days = data.object(forKey: Keys.KEY_NO_OF_DAYS) as? Int {
                                        no_of_days = no_days
                                    }
                                    else if  let no_days = data.object(forKey: Keys.KEY_NO_OF_DAYS) as? String {
                                        no_of_days = Int(no_days)!
                                    }
                                    
                                    if  let med_status = data.object(forKey: Keys.KEY_MEDICATION_STATE) as? Int {
                                        medication_status = med_status
                                    }
                                    else if  let med_status = data.object(forKey: Keys.KEY_MEDICATION_STATE) as? String {
                                        medication_status = Int(med_status)!
                                    }
                                    
                                    if tag == 1 {
                                        print("data in update medication=\(data)")
                                        let medicationDetails = Medication(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated, dateFrom: dateFrom, toDate: dateTo, intake: intake, notes: notes,numberofdays:no_of_days,status:medication_status)
                                        App.medications.append(medicationDetails)
                                        DispatchQueue.main.async(execute: {
                                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "PopView"), object: nil)
                                        })
                                        print("Medications addedd")
                                    }else if tag == 2{
                                        
                                        let allergyDetails = Allergy(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated)
                                        App.allergies.append(allergyDetails)
                                        print("Allergy addedd")
                                    }else if tag == 5{
                                        let medicationDetails = Medication(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated, dateFrom: dateFrom, toDate: dateTo, intake: intake, notes: notes,numberofdays:no_of_days,status:medication_status)
                                        App.medications[App.editIndex] = medicationDetails  //PopView
                                        UserDefaults.standard.set(false, forKey: "EditTapped")
                                        App.editIndex = nil
                                        DispatchQueue.main.async(execute: {
                                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "PopView"), object: nil)
                                        })
                                        print("Medication edited")
                                    }else if tag == 6{
                                        
                                        let allergyDetails = Allergy(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated)
                                        App.allergies[App.editIndex] = allergyDetails
                                        App.editIndex = nil
                                        print("Allergy edited")
                                    }
                                    else if tag == 8{
                                        
                                        let allergyDetails = Allergy(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated)
                                        App.drug_allergies[App.editIndex] = allergyDetails
                                        App.editIndex = nil
                                        print("Drug Allergy edited")
                                    }
                                }
                                 completionHandler(true)
                            }else {
                                completionHandler(false)
                                DispatchQueue.main.async(execute: {
                                    self.didShowAlert(title: responseStatus, message: "")
                                  //  AlertView.sharedInsance.showFailureAlert(title: responseStatus, message: "")
                                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateHealthProfile"), object: nil)
                                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateAllergys"), object: nil)
                                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateMedications"), object: nil)
                                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                })
                            }
                        }
                    }catch let error
                    {
                        completionHandler(false)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON:\(error.localizedDescription)")
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateHealthProfile"), object: nil)
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateAllergys"), object: nil)
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateMedications"), object: nil)
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                })
            }
        })
        task.resume()
    }
    
    /**
     Method used to show alert with custom title and message and perform network call
     - Parameter title: pass the title of the alert
     - Parameter message: pass the message for alert
     - Parameter Action: Pass the title for action to perform
     - Parameter urlString: takes the urlstring of action
     - Parameter httpmethod: Pass the httpmethod here
     - Parameter tag: to know what kind of request is going and perform action
     */
    func showAlertWithRequest(title:String ,message:String,Action:String,urlString:String,httpMethod:String,tag:Int) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Action, style: UIAlertAction.Style.destructive, handler: { (UIAlertAction) in
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StartAnimating"), object: nil)
            self.perfromRequest(body: "", URLString: urlString, httpmethod: httpMethod, tag: tag, completionHandler:
                { (success) in
                    
             })
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
     Methods checks wether string contains only letters or not and retuns BOOL
      - Parameter input: pass the string to check
     */
    func containsOnlyLetters(input: String) -> Bool {
        let trimed =  input.components(separatedBy: .whitespaces).joined()
        for chr in trimed {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z" ) ) {
                return false
            }
        }
        return true
    }
    
    /**
     Method used to show alert with custom title and message the text to edit in text field
     - Parameter title: pass the title of the alert
     - Parameter message: pass the message for alert
     - Parameter ActionTitle: Pass the title for action to perform
     - Parameter URLString: takes the urlstring of action
     - Parameter httpmethod: Pass the httpmethod here
     - Parameter tag: to know what kind of request is going and perform action
     - Parameter textToEdit: Pass the text of allergy to edit
     */
    func showTextFieldAlert(title:String,message:String , URLString :String , httpmethod :String,tag:Int , ActionTitle:String ,textToEdit:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter here.."
            textField.font = UIFont(name: "Helvetica", size: 20)
            textField.text = textToEdit
            textField.addTarget(self, action: #selector(self.textChanged(_:)), for: .editingChanged)
        }
        
        let action = UIAlertAction(title: ActionTitle, style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            
            print("Text field: \(textField?.text!)")
            if textField?.text?.isEmpty == true {
                print("Not entered any text")
            }else if !self.containsOnlyLetters(input: (textField?.text)!) {
                print("Not entered any text")
            }else {
                let textBody = "name=\((textField?.text!)!)"
                print("body allergy = \(textBody)")
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StartAnimating"), object: nil)
                self.perfromRequest(body: textBody, URLString: URLString, httpmethod: httpmethod, tag: tag, completionHandler:
                    { (success) in
                })
            }
            
        })
        
        
        alert.addAction(action)
        alert.actions[0].isEnabled = false
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
      Checks the text in textfield of alert controller has only letters
      - Parameter sender: UIAlertController textFiled goes here
   */
   @objc func textChanged(_ sender: UITextField) {
        var resp : UIResponder! = sender
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        let textToCheck = self.containsOnlyLetters(input: sender.text!)
         if sender.text == "" {
            alert.actions[0].isEnabled = false
         }else
         {
            alert.actions[0].isEnabled = textToCheck
         }
    }
    
    /**
     Checks the text in textfield of alert controller has only letters
     - Parameter sender: UIAlertController textFiled goes here
     */
    @objc func textOtherChanged(_ sender: UITextField) {
        var resp : UIResponder! = sender
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        //let textToCheck = self.containsOnlyLetters(input: sender.text!)
        if sender.text == "" {
            alert.actions[0].isEnabled = false
        }else
        {
            alert.actions[0].isEnabled = true//textToCheck
        }
    }
    
    
    /**
     Method performs request to server for health profile details
     - Parameter session: provide the URLDataSession
     */
    func getHealthProfile(session : URLSession) {
        
        print("\(#function) called")
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        print("**")
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
        let url = URL(string: AppURLS.URL_HealthProfile)
        print("URL==>\(AppURLS.URL_HealthProfile)")
        
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil
            {
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                        if !errorStatus {
                            print("performing error handling get health profile:\(jsonData)")
                        }
                        else {
                            let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                            let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(responseStatus) code:\(code)")
                            let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            print("response:\(jsonData)")
                            if code == 200 && responseStatus == "success"
                            {
                                var weight : Double!
                                var height : Double!
                                var does_smoke = ""
                                var medical_history = ""
                                var switch_in_medicine_reason = ""
                                
                                if let wght = data.object(forKey: Keys.KEY_WEIGHT) as? Double {
                                    weight = wght
                                }
                                
                                if let hght = data.object(forKey: Keys.KEY_HEIGHT) as? Double {
                                    height = hght
                                }
                                
                                if let doesSmoke = data.object(forKey: Keys.KEY_DOES_SMOKE) as? Int {
                                    does_smoke = "No"
                                    if doesSmoke == 1 {
                                        does_smoke = "Yes"
                                    }
                                }
                                
                                if let mHistory = data.object(forKey: Keys.KEY_MEDICAL_HISTORY) as? String {
                                    medical_history = mHistory
                                }
                                
                                if let switch_in_reason = data.object(forKey: Keys.KEY_SWITCH_MEDICINE_OTHERS) as? String {
                                    switch_in_medicine_reason = switch_in_reason
                                }
                                
                                if let allgys = data.object(forKey: Keys.KEY_ALLERGIES) as? [NSDictionary] {
                                    App.allergies.removeAll()
                                    for allergy in allgys {
                                        let id = allergy.object(forKey: Keys.KEY_ID) as! Int
                                        let user_id = allergy.object(forKey: Keys.KEY_USER_ID) as! Int
                                        let allergyName = allergy.object(forKey: Keys.KEY_NAME) as! String
                                        let created_at = allergy.object(forKey: Keys.KEY_CREATED_AT) as! String
                                        let updated =  allergy.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                        
                                        let allergyDetails = Allergy(id: id, userid: user_id, name: allergyName, createdat: created_at, updatedat: updated)
                                        App.allergies.append(allergyDetails)
                                        print("Allergies=\( App.allergies)")
                                    }
                                    
//                                    let encodedData1 = NSKeyedArchiver.archivedData(withRootObject:  App.allergies)
//                                    UserDefaults.standard.set(encodedData1, forKey: UserDefaltsKeys.KEY_ALLERGIES)
                                    let jsonEncoder = JSONEncoder()
                                    do {
                                        let encodedData = try jsonEncoder.encode(App.allergies)
                                        UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_ALLERGIES)
                                    }catch let err{
                                        print("Error while encoding allergies to data:\(err)")
                                    }
                                }
                                
                                if let allgys = data.object(forKey: Keys.KEY_DRUG_ALLERGIES) as? [NSDictionary] {
                                    App.drug_allergies.removeAll()
                                    for allergy in allgys {
                                        let id = allergy.object(forKey: Keys.KEY_ID) as! Int
                                        let user_id = allergy.object(forKey: Keys.KEY_USER_ID) as! Int
                                        let allergyName = allergy.object(forKey: Keys.KEY_NAME) as! String
                                        let created_at = allergy.object(forKey: Keys.KEY_CREATED_AT) as! String
                                        let updated =  allergy.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                        
                                        let allergyDetails = Allergy(id: id, userid: user_id, name: allergyName, createdat: created_at, updatedat: updated)
                                        App.drug_allergies.append(allergyDetails)
                                        print("Allergies=\( App.drug_allergies)")
                                    }
                                    
                                    let jsonEncoder = JSONEncoder()
                                    do {
                                        let encodedData = try jsonEncoder.encode(App.drug_allergies)
                                        UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_DRUG_ALLERGIES)
                                    }catch let err{
                                        print("Error while encoding drug allergies to data:\(err)")
                                    }
                                }
                                
                                
                                if let mdictions = data.object(forKey: Keys.KEY_MEDICATIONS) as? [NSDictionary] {
                                    
                                    print("Medications fetched :\(mdictions)")
                                    App.medications.removeAll()
                                    for medication in mdictions {
                                        let id = medication.object(forKey: Keys.KEY_ID) as! Int
                                        let user_id = medication.object(forKey: Keys.KEY_USER_ID) as! Int
                                        let medname = medication.object(forKey: Keys.KEY_NAME) as! String
                                        let created_at = medication.object(forKey: Keys.KEY_CREATED_AT) as! String
                                        let updated =  medication.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                        
                                        var dateFrom = ""
                                        var dateTo = ""
                                        var intake = ""
                                        var notes = ""
                                        var no_of_days = 0
                                        var medication_status = 0
                                        
                                        if let datefrm = medication.object(forKey: Keys.KEY_FROM_DATE) as? String{
                                            dateFrom = datefrm
                                        }
                                        if  let dateToStr = medication.object(forKey: Keys.KEY_TO_DATE) as? String {
                                            dateTo = dateToStr
                                        }
                                        if  let intakeT = medication.object(forKey: Keys.KEY_INTAKE_TIME) as? String {
                                            intake = intakeT
                                        }
                                        if  let notesStr = medication.object(forKey: Keys.KEY_NOTES) as? String {
                                            notes = notesStr
                                            print("mediNotes=\(notesStr)")
                                        }
                                        if  let no_days = medication.object(forKey: Keys.KEY_NO_OF_DAYS) as? Int {
                                            no_of_days = no_days
                                        }
                                        
                                        if  let med_status = medication.object(forKey: Keys.KEY_MEDICATION_STATE) as? Int {
                                            medication_status = med_status
                                        }
                                        
                                        
                                        print("1\(medname) = 2 \(dateFrom) 3 \(dateTo) 4 \(notes) 5 \(user_id)")
                                        let medicationDetails = Medication(id: id, userid: user_id, name: medname, createdat: created_at, updatedat: updated, dateFrom: dateFrom, toDate: dateTo, intake: intake, notes: notes,numberofdays:no_of_days,status:medication_status)
                                        App.medications.append(medicationDetails)
                                        print("Medications=\( App.medications.count)")
                                    }
                                    
                                    let jsonEncoder = JSONEncoder()
                                    do {
                                        let encodedData = try jsonEncoder.encode(App.medications)
                                        UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MEDICATIONS)
                                    }catch let err{
                                        print("Error while encoding medicaitons to data:\(err)")
                                    }
                                    
//                                    let encodedData = NSKeyedArchiver.archivedData(withRootObject:  App.medications)
//                                    UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MEDICATIONS)
                                }
                                var med_insurance = 0
                                let life_style = data.object(forKey: Keys.KEY_LIFE_STYLE_ID) as! Int
                                let sleep_duration = data.object(forKey: Keys.KEY_SLEEP_DURATION_ID) as! Int
                                let sleep_pattern = data.object(forKey: Keys.KEY_SLEEP_PATTERN_ID) as! Int
                                let exercise_per_week = data.object(forKey: Keys.KEY_EXERCISE_PER_WEEK_ID) as! Int
                                let marital_status = data.object(forKey: Keys.KEY_MARITAL_STATUS_ID) as! Int
                                let switch_in_medicine = data.object(forKey: Keys.KEY_SWITCH_MEDICINE_ID) as! Int
                                let medical_history_new = data.object(forKey: Keys.KEY_MEDICAL_HISTORY_NEW_ID) as! Int
                                if let med_ins = data.object(forKey: Keys.KEY_MEDICAL_INSURANCE) as? Int {
                                    med_insurance = med_ins
                                }
                                var medical_insurance = 0
                                if med_insurance == 1 {
                                    medical_insurance = 1
                                }
                                
                                if let mdictions = data.object(forKey: Keys.KEY_MASTER_DATA) as? NSDictionary {
                                    
                                    print("Master Data fetched :\(mdictions)")
                                    
                                    var medical_history_option = [OptionsData]()
                                    var life_style_option = [OptionsData]()
                                    var sleep_duration_option = [OptionsData]()
                                    var sleep_pattern_option = [OptionsData]()
                                    var exercise_option = [OptionsData]()
                                    var marital_option = [OptionsData]()
                                    var switch_in_medicine_option = [OptionsData]()
                                    App.masterData = MasterData()
                                    
                                    if let exercise_per_week = mdictions.object(forKey: Keys.KEY_EXERCISE_PER_WEEK) as? [NSDictionary]{
                                        for options in exercise_per_week {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_exercise = OptionsData(id: id, first_level_value: optional_name)
                                            exercise_option.append(temp_exercise)
                                        }
                                    }
                                    if let life_style_activity = mdictions.object(forKey: Keys.KEY_LIFE_STYLE) as? [NSDictionary]{
                                        for options in life_style_activity {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_life_style = OptionsData(id: id, first_level_value: optional_name)
                                            life_style_option.append(temp_life_style)
                                        }
                                    }
                                    if let marital_status = mdictions.object(forKey: Keys.KEY_MARITAL_STATUS) as? [NSDictionary]{
                                        for options in marital_status {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_marital = OptionsData(id: id, first_level_value: optional_name)
                                            marital_option.append(temp_marital)
                                        }
                                    }
                                    if let medical_history = mdictions.object(forKey: Keys.KEY_MEDICAL_HISTORY) as? [NSDictionary]{
                                        for options in medical_history {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_medical_history = OptionsData(id: id, first_level_value: optional_name)
                                            medical_history_option.append(temp_medical_history)
                                        }
                                    }
                                    if let sleep_duration =  mdictions.object(forKey: Keys.KEY_SLEEP_DURATION) as? [NSDictionary]{
                                        for options in sleep_duration {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_sleep_duration = OptionsData(id: id, first_level_value: optional_name)
                                            sleep_duration_option.append(temp_sleep_duration)
                                        }
                                    }
                                    if let sleep_pattern =  mdictions.object(forKey: Keys.KEY_SLEEP_PATTERN) as? [NSDictionary]{
                                        for options in sleep_pattern {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_sleep_pattern = OptionsData(id: id, first_level_value: optional_name)
                                            sleep_pattern_option.append(temp_sleep_pattern)
                                        }
                                    }
                                    if let switch_in_medicine =  mdictions.object(forKey: Keys.KEY_SWITCH_MEDICINE) as? [NSDictionary]{
                                        for options in switch_in_medicine {
                                            let id = options.object(forKey: Keys.KEY_ID) as! Int
                                            let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                            let temp_switch_in_medicine = OptionsData(id: id, first_level_value: optional_name)
                                            switch_in_medicine_option.append(temp_switch_in_medicine)
                                        }
                                    }
                                    
                                    
                                    let master = MasterData(exercise_per_week: exercise_option, life_style_activity: life_style_option, marital_status: marital_option, medical_history: medical_history_option, sleep_duration: sleep_duration_option, sleep_pattern: sleep_pattern_option, switch_in_medicine: switch_in_medicine_option)
                                    App.masterData = master
                                    print("MasterData=\( App.masterData)")
                                    
                                    let jsonEncoder = JSONEncoder()
                                    do {
                                        let encodedData = try jsonEncoder.encode(App.masterData)
                                        UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MASTER_DATA)
                                    }catch let err{
                                        print("Error while encoding master data :\(err)")
                                    }
                                    
                                }
                                
                                //let dataModel = HealthProfile(weight: weight, height: height, does_smoke: does_smoke, medical_history: medical_history, allergies: App.allergies, medications:  App.medications)
                                let dataModel = HealthProfile(weight: weight, height: height, does_smoke: does_smoke, medical_history: medical_history, allergies: App.allergies, medications: App.medications, life_style: life_style, sleep_duration:sleep_duration,sleep_pattern: sleep_pattern,exercise_per_week:exercise_per_week,marital_status:marital_status,switch_in_medicine:switch_in_medicine,medical_history_new:medical_history_new,medical_insurance:medical_insurance,switch_in_medicine_reason:switch_in_medicine_reason)
                                HealthProfile.store_health_profile_default(dataModel: dataModel)
                                
                            }else {
                                print("Error while fetching appointment details:\(responseStatus) ")
                            }
                            
                            DispatchQueue.main.async(execute: {
                                
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                // NotificationCenter.default.post(name:NSNotification.Name(rawValue: "PopView"), object: nil)
                                
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateHealthProfile"), object: nil)
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateAllergys"), object: nil)
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "UpdateMedications"), object: nil)
                            })
                            
                        }
                    }
                    catch let error
                    {
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON:\(error.localizedDescription)")
                    }
                    
                }
            }
        }).resume()
        print("\(#function) ended")
    }
}
