//
//  PregnancyViewController.swift
//  DocOnline
//
//  Created by Mac on 19/03/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class PregnancyViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tf_expecting: UITextField!
    @IBOutlet weak var tf_conceptions : UITextField!
    @IBOutlet weak var tf_abortions : UITextField!
    @IBOutlet weak var tf_complications : UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updatePregnancyUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        saveBtn.backgroundColor = Theme.buttonBackgroundColor
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func expectingTapped(_ sender: UIButton) {
        /*
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })*/
        
        let actionView = UIAlertController(title: "Expecting mother?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let selfAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_expecting.text = "Yes"
            /*
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })*/
        }
        let onlyAlcoholic = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_expecting.text = "No"
            /*
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })*/
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            /*
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })*/
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func complicationsTapped(_ sender: UIButton) {
        /*
         UIView.animate(withDuration: 0.5, animations: {
         self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
         })*/
        
        let actionView = UIAlertController(title: "Pregnancy related complications if any?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let selfAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_complications.text = "Yes"
            /*
             UIView.animate(withDuration: 0.5, animations: {
             self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
             })*/
        }
        let onlyAlcoholic = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_complications.text = "No"
            /*
             UIView.animate(withDuration: 0.5, animations: {
             self.iv_medicalInsurance.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
             })*/
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            /*
             UIView.animate(withDuration: 0.5, animations: {
             self.iv_switchMedicinExpand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
             })*/
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        self.present(actionView, animated: true, completion: nil)
    }
    
    func getGender() -> String {
        let userDataModel : User  = User.get_user_profile_default()
        var gender = ""
        if userDataModel.gender != nil {
            gender = userDataModel.gender!
        }
        return gender
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        
        self.startAnimating()
        var expecting = ""
        if self.tf_expecting.text! == "Yes"
        {
            expecting = "1"
        }
        else if self.tf_expecting.text! == "No"
        {
            expecting = "0"
        }
        
        var complications = ""
        if self.tf_complications.text! == "Yes"
        {
            complications = "1"
        }
        else if self.tf_complications.text! == "No"
        {
            complications = "0"
        }
        
        let stringData = "\(Keys.KEY_EXPECTING_MOTHER)=\(expecting)&\(Keys.KEY_COMPLICATIONS)=\(complications)&\(Keys.KEY_CONCEPTIONS)=\(self.tf_conceptions.text!)&\(Keys.KEY_ABORTIONS)=\(self.tf_abortions.text!)&\(Keys.KEY_GENDER)=\(self.getGender())"
        
        
        print("Health profile data:\(stringData)")
        let urlString = AppURLS.URL_HealthProfile
        let url = URL(string: urlString)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.PATCH
        
        print("URL health==>\(urlString)")
        print("HTTPMethod==>\(HTTPMethods.PATCH)")
        request.httpBody = stringData.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            if let error = error {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data{
                print("data =\(data)")
            }
            if let response = response {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                //if you response is json do the following
                do{
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling save helath profile:\(resultJSON)")
                    }
                    else {
                        print("==>Health response : \(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let responseStatus = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(responseStatus) code:\(code)")
                        let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                        if code == 200 && responseStatus == "success" || code == 201 && responseStatus == "success"
                        {
                            if let pregnancy = data.object(forKey: Keys.KEY_PREGNANCY_DETAILS) as? NSDictionary {
                                App.pregnancyData = Pregnancy()
                                
                                var abortions = ""
                                var conceptions = ""
                                var complications = ""
                                var expecting_mother = ""
                                let id = pregnancy.object(forKey: Keys.KEY_ID) as! Int
                                let user_id = pregnancy.object(forKey: Keys.KEY_USER_ID) as! Int
                                let created_at = pregnancy.object(forKey: Keys.KEY_CREATED_AT) as! String
                                let updated =  pregnancy.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                if let abor =  pregnancy.object(forKey: Keys.KEY_ABORTIONS) as? String{
                                    abortions = abor
                                }
                                else if let abor =  pregnancy.object(forKey: Keys.KEY_ABORTIONS) as? Int{
                                    abortions = String(abor)
                                }
                                //let expecting_mother =  pregnancy.object(forKey: Keys.KEY_EXPECTING_MOTHER) as! Int
                                if let expecting =  pregnancy.object(forKey: Keys.KEY_EXPECTING_MOTHER) as? String{
                                    expecting_mother = expecting
                                }
                                else if let expecting =  pregnancy.object(forKey: Keys.KEY_EXPECTING_MOTHER) as? Int{
                                    expecting_mother = String(expecting)
                                }
                                if let concept =  pregnancy.object(forKey: Keys.KEY_CONCEPTIONS) as? String{
                                    conceptions = concept
                                }
                                else if let concept =  pregnancy.object(forKey: Keys.KEY_CONCEPTIONS) as? Int{
                                    conceptions = String(concept)
                                }
                                if let complic =  pregnancy.object(forKey: Keys.KEY_COMPLICATIONS) as? String{
                                    complications = complic
                                }
                                else if let complic =  pregnancy.object(forKey: Keys.KEY_COMPLICATIONS) as? Int{
                                    complications = String(complic)
                                }
                                
                                App.pregnancyData = Pregnancy(id: id, userid: user_id, createdat: created_at, updatedat: updated, abortions: abortions, conceptions: conceptions, complications: complications, expecting_mother: expecting_mother)
                                
                                //print("pregnancyData=\(App.pregnancyData.expecting_mother!)")
                                
                                let jsonEncoder = JSONEncoder()
                                do {
                                    let encodedData = try jsonEncoder.encode(App.pregnancyData)
                                    UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_PREGNANCY_DATA)
                                }catch let err{
                                    print("Error while encoding pregnancy data :\(err)")
                                }
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.view.endEditing(true)
                                
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                            
                        }else {
                            print("Somethong wrong...")
                        }
                        
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                    }
                }catch let error
                {
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON:\(error.localizedDescription)")
                }
            }
        })
        task.resume()
        
        
    }

    func updatePregnancyUI() {
        let pregnancyDetails = App.pregnancyData
        self.tf_expecting.text = ""
        if pregnancyDetails.expecting_mother! == "1" {
            self.tf_expecting.text = "Yes"
        }
        else if pregnancyDetails.expecting_mother! == "0"{
            self.tf_expecting.text = "No"
        }
    
        self.tf_complications.text = ""
        if pregnancyDetails.complications! == "1" {
            self.tf_complications.text = "Yes"
        }
        else if pregnancyDetails.complications! == "0"{
            self.tf_complications.text = "No"
        }
        
        self.tf_conceptions.text = pregnancyDetails.conceptions!
        self.tf_abortions.text = pregnancyDetails.abortions!
    }
}

extension PregnancyViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var characterSet = CharacterSet.letters
        characterSet.insert(charactersIn: " ")
        //let unwantedStr = string.trimmingCharacters(in: characterSet)
        let newLength = (textField.text?.count)! + string.count - range.length
        if textField == self.tf_abortions {
            return newLength <= 1
        }
        else if textField == self.tf_conceptions {
            return newLength <= 1
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
}
