//
//  AddAllergyViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 26/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

protocol AddAllergyDelegate {
    func didAddAllergy(_ success:Bool)
    func didEditAllergy(_ success:Bool)
}


class AddAllergyViewController: UIViewController ,NVActivityIndicatorViewable{

    
    @IBOutlet var tv_allergyText: UITextView!
    @IBOutlet var lb_placeholder: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var LC_tableViewHeight: NSLayoutConstraint!
    @IBOutlet var btn_addUpdate: UIButton!
    
    var delegate: AddAllergyDelegate?
    
    var tag = 0
    
    var arrSearch : NSArray?
    var dictSearch : NSDictionary?
    var dosage_name : String = ""
    var dosage_id : String = ""
    var keyboardHeight : CGFloat = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tv_allergyText.layer.borderWidth = 1
        self.tv_allergyText.layer.borderColor = UIColor.darkGray.cgColor
        self.tv_allergyText.layer.cornerRadius = 2
        self.title = "Add Allergy"
        
        if App.editIndex != nil {
            if App.isDrugAllergy
            {
                let alergy = App.drug_allergies[App.editIndex]
                self.tv_allergyText.text = alergy.name
                self.lb_placeholder.isHidden = true
                btn_addUpdate.setTitle("UPDATE", for: .normal)
            }
            else
            {
                let alergy = App.allergies[App.editIndex]
                self.tv_allergyText.text = alergy.name
                self.lb_placeholder.isHidden = true
                btn_addUpdate.setTitle("ADD", for: .normal)
            }
        }
        
        LC_tableViewHeight.constant = 0.0
        if App.isDrugAllergy
        {
            self.title = "Add Drug Allergy"
            self.lb_placeholder.text = "Minimum 3 characters required to search"
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(keyboardWillShow),
                name: UIResponder.keyboardWillShowNotification,
                object: nil
            )
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btn_addUpdate.backgroundColor = Theme.buttonBackgroundColor
    }

    @IBAction func addTapped(_ sender: UIButton) {
        let enteredAllergy = self.tv_allergyText.text! as String
        
        if App.editIndex != nil &&  App.isDrugAllergy{
            
            if enteredAllergy.isEmpty {
                self.didShowAlert(title: "", message: "Please enter drug allergy")
            }
            else{
                
                startAnimating()
                let allergyData = ["\(Keys.KEY_NAME)" : enteredAllergy]
                let textBody = "name=\(enteredAllergy)"
                print("textBody",textBody)
                //let data1 = textBody.data(using: String.Encoding.utf8)
                print("mobiledata:\(allergyData)")
                let aler = App.drug_allergies[App.editIndex]
                let url = AppURLS.URL_Drug_Allergies + "\(aler.id!)"
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: allergyData, options: .prettyPrinted) as Data
                    NetworkCall.performPATCH(url: url, data: jsonData, completionHandler: { (success, response, status, error) in
                        if success {
                            if let resp = response ,let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary{
                                let id = data.object(forKey: Keys.KEY_ID) as! Int
                                let user_id = data.object(forKey: Keys.KEY_USER_ID) as! Int
                                let name = data.object(forKey: Keys.KEY_NAME) as! String
                                let created_at = data.object(forKey: Keys.KEY_CREATED_AT) as! String
                                let updated =  data.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                
                                let allergyDetails = Allergy(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated)
                                App.drug_allergies[App.editIndex] = allergyDetails
                                App.editIndex = nil
                                //App.drug_allergies.append(allergyDetails)
                                
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    self.delegate?.didEditAllergy(success)
                                }
                                print("Drug Allergy Updated")
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                            }
                        }
                    })
                } catch {
                    print("Error while converting to jsondata of drug allergy:\(error.localizedDescription)")
                }
            }
            
        }
        else
        {
        if enteredAllergy.isEmpty {
            self.didShowAlert(title: "", message: "Please enter allergy")
        }else {
            let textBody = "name=\(enteredAllergy)"
            print("body allergy = \(textBody)")
            startAnimating()
            
            let allergyData = ["\(Keys.KEY_NAME)" : enteredAllergy]
            print("mobiledata:\(allergyData)")
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: allergyData, options: .prettyPrinted) as Data
                NetworkCall.performPOST(url: App.isDrugAllergy ? AppURLS.URL_Drug_Allergies : AppURLS.URL_Allergies, data: jsonData, completionHandler: { (success, response, status, error) in
                    if success {
                        if let resp = response ,let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary{
                            let id = data.object(forKey: Keys.KEY_ID) as! Int
                            let user_id = data.object(forKey: Keys.KEY_USER_ID) as! Int
                            let name = data.object(forKey: Keys.KEY_NAME) as! String
                            let created_at = data.object(forKey: Keys.KEY_CREATED_AT) as! String
                            let updated =  data.object(forKey: Keys.KEY_UPDATED_AT) as! String
                            
                            if App.isDrugAllergy
                            {
                                let allergyDetails = Allergy(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated)
                                App.drug_allergies.append(allergyDetails)
                            }
                            else
                            {
                                let allergyDetails = Allergy(id: id, userid: user_id, name: name, createdat: created_at, updatedat: updated)
                                App.allergies.append(allergyDetails)
                            }
                            
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.delegate?.didAddAllergy(success)
                            }
                            print("Allergy addedd")
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.stopAnimating()
                         }
                    }
                })
            } catch {
                print("Error while converting to jsondata of allergy:\(error.localizedDescription)")
            }
            
           
//            self.perfromRequest(body: textBody, URLString: AppURLS.URL_Allergies, httpmethod: HTTPMethods.POST, tag: 2, completionHandler:
//                { (success) in
//                    if success {
//                        DispatchQueue.main.async {
//                            self.delegate?.didAddAllergy(success)
//                        }
//                    }
//            })
        }
        }
    }
    
    func getSearchList(strSearch:String) {
        print("Files URL:\(strSearch)")
        self.dictSearch = NSDictionary()
        self.arrSearch = NSArray()
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        DispatchQueue.main.async {
            self.startAnimating()
        }
        let str = strSearch.withoutSpecialCharacters
        print("strSearch",str)
        let str1 = self.removeSpecialCharsFromString(text:str)
        print("strSearch1",str1)
        NetworkCall.performGet(url:"\(AppURLS.URL_MEDICATION_SEARCH)\(str1)") { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    //self.tableView.reloadData()
                }
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                DispatchQueue.main.async {
                    print("Search Data:\(data)")
                    self.dictSearch = data as NSDictionary
                    self.arrSearch = self.dictSearch?.allValues as NSArray?
                    print("arrSearch : ",self.arrSearch ?? "")
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
                self.tableView.reloadData()
                let tblHeight = self.view.frame.size.height - (self.keyboardHeight + 88.0)
                print("tblHeight .... :",tblHeight)
                if self.tableView.contentSize.height <= tblHeight
                {
                    self.LC_tableViewHeight.constant = self.tableView.contentSize.height
                }
                else
                {
                    self.LC_tableViewHeight.constant = tblHeight
                }
            }
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return text.filter {okayChars.contains($0) }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if App.editIndex != nil {
            App.editIndex = nil
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
    }

}

extension AddAllergyViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var characterSet = CharacterSet.letters
        characterSet.insert(charactersIn: " ")
        let unwantedStr = text.trimmingCharacters(in: characterSet)
        let newLength = (textView.text?.count)! + text.count - range.length
        if(newLength >= 3 && App.isDrugAllergy){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.getSearchList(strSearch: textView.text)
            }
        }
        return newLength <= 100 && unwantedStr.count == 0
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.lb_placeholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
           self.lb_placeholder.isHidden = false
        }
    }
}

extension AddAllergyViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dictSearch != nil
        {
            return (dictSearch?.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "drugSearch", for: indexPath)
        if dictSearch != nil
        {
            cell.textLabel?.text = arrSearch?.object(at: indexPath.row) as? String
        }
        cell.selectionStyle = .none
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if dictSearch != nil
        {
            let strName = arrSearch?.object(at: indexPath.row) as? String ?? ""
            if strName.isEmpty != true
            {
                dosage_name = strName
                tv_allergyText.text = dosage_name
                let keys = dictSearch?.allKeys(for: strName)
                dosage_id = keys?[0] as! String
                
                self.view.endEditing(true)
                LC_tableViewHeight.constant = 0.0
            }
        }
    }
    
}





