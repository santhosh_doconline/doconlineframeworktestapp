//
//  MedicationsViewController.swift
//  DocOnline
//
//  Created by dev-3 on 21/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView


class MedicationsViewController: UIViewController ,NVActivityIndicatorViewable{

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var lb_noMedicationsStatus: UILabel!
    @IBOutlet var bt_addMedication: UIButton!
    
    ///Not used
    var dataModel : HealthProfile!
    ///Not used
    let config = URLSessionConfiguration.default
    ///Not used
    var session : URLSession!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MedicationsViewController.adjustViewHeight), name: NSNotification.Name(rawValue: "UpdateMedications"), object: nil)
        
        if HealthProfile.check_Health_defaults()
        {
            print("HealthDefaults: \(HealthProfile.check_Health_defaults() )")
            
            if App.medications.count == 0{
                App.medications = HealthProfile.getMedicaions()
            }
        }
        else
        {

            session = URLSession(configuration: config)
            getHealthProfile(session: session)
            print("Getting health profile")
        }
        adjustViewHeight()
        
//        navigationController?.navigationBar.barTintColor = UIColor(red: 5, green: 66, blue: 76, alpha: 1.0)
//        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }

    override func viewWillAppear(_ animated: Bool) {
        bt_addMedication.backgroundColor = Theme.buttonBackgroundColor
        tableView.reloadData()
    }
    
    ///Reloads the tableView
    @objc func adjustViewHeight() {
        if App.medications.count == 0 {
            self.tableView.reloadData()
            self.tableView.isHidden = true
            self.lb_noMedicationsStatus.isHidden = false
            self.bt_addMedication.setTitle("+Add medication", for: .normal)
        }else{
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.lb_noMedicationsStatus.isHidden = true
            self.bt_addMedication.setTitle("+Add other medication", for: .normal)
        }
        
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            self.stopAnimating()
        })
    }
    

    @IBAction func menuTapped(_ sender: UIBarButtonItem) {
       //  toggleSideMenuView()
    }
    
    @IBAction func addMedicationsTapped(_ sender: UIButton) {
        App.editIndex = nil
        UserDefaults.standard.set(false, forKey: "EditTapped")
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_MEDICATIONS_DETAILS, sender: self)
    }

    override func viewWillDisappear(_ animated: Bool) {
      //  hideSideMenuView()
        super.viewWillDisappear(animated)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MedicationsViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return App.medications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.Medications, for: indexPath) as! HealthProfileTableViewCell
        let medicationValues = App.medications[indexPath.row]
       
        cell.lb_name.text = medicationValues.name
        cell.lb_notes.text = medicationValues.notes
        print("\(indexPath.row)==>\(medicationValues.notes)")
        //var dateFrom = "N/A"
        // var toDate = "N/A"
        
        //        if medicationValues.dateFrom.isEmpty != true {
        //            dateFrom = medicationValues.dateFrom
        //        }
        //
        //        if medicationValues.toDate.isEmpty != true {
        //            toDate = medicationValues.toDate
        //        }
        
//        if medicationValues.intakeTime.isEmpty != true {
//            cell.lb_intake.text = self.getTimeInAMPM(date: medicationValues.intakeTime)
//        }
          cell.lb_intake.text = "Intake time: \(medicationValues.intakeTime!)"
        
        cell.img_intake.image = UIImage(named: "clock")
        if medicationValues.intakeTime! == ""{
            cell.lb_intake.text = ""
            cell.img_intake.image = UIImage()
        }
        if medicationValues.numberofdays! != 0 {
            //cell.img_no_days.isHidden = false
            cell.img_no_days.image = UIImage(named: "event_gray")
            cell.lb_no_days.text = "Number of days: \(medicationValues.numberofdays!)"
        }
        else
        {
           //cell.img_no_days.isHidden = true
            cell.img_no_days.image = UIImage()
            cell.lb_no_days.text = ""
        }
        
        if medicationValues.status! != 0 {
            if medicationValues.status! == 1 {
                //cell.img_status.isHidden = false
                cell.img_status.image = UIImage(named: "history_gray")
                cell.lb_status.text = "Present Medication"
            }
            else
            {
                //cell.img_status.isHidden = false
                cell.img_status.image = UIImage(named: "history_gray")
                cell.lb_status.text = "Past Medication"
            }
        }
        else
        {
            //cell.img_status.isHidden = true
            cell.img_status.image = UIImage()
            cell.lb_status.text = ""
        }
        
        cell.img_arrow.image = UIImage(named: "arrow")
        cell.img_from.image = UIImage(named: "Medication")
        cell.img_to.image = UIImage(named: "Medication")
        if let dateFrom = medicationValues.dateFrom ,let toDate = medicationValues.toDate{
            cell.lb_fromDate.text = getFormattedDateOnly(dateString: dateFrom)
            cell.lb_toDate.text = getFormattedDateOnly(dateString: toDate)
            if cell.lb_toDate.text == ""{
                cell.img_arrow.image = UIImage()
                cell.img_to.image = UIImage()
                if cell.lb_fromDate.text == ""{
                   cell.img_from.image = UIImage()
                }
            }
        }
        /*
        cell.bt_deleteTapped.tag = indexPath.row
        cell.bt_edit_tapped.tag = indexPath.row
        cell.bt_deleteTapped.addTarget(self, action: #selector(MedicationsViewController.deleteMedicationTapped(sender:)), for: UIControl.Event.touchUpInside)
        cell.bt_edit_tapped.addTarget(self, action: #selector(MedicationsViewController.editMedicationTapped(sender:)), for: UIControl.Event.touchUpInside)
        

        cell.vw_buttonsView.cornerRadius = 2
        cell.vw_buttonsView.layer.borderColor = UIColor.lightGray.cgColor
        cell.vw_buttonsView.layer.borderWidth = 1*/

        cell.vw_back.cornerRadius = 2
        cell.vw_back.layer.borderColor = UIColor.lightGray.cgColor
        cell.vw_back.layer.borderWidth = 1
        
        
        return cell
    }
    
   
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.contentView.backgroundColor = UIColor.clear
//        let whiteRoundedView  = UIView(frame: CGRect(x:0, y:10, width: self.view.frame.size.width,height: self.view.frame.size.height - 8))
//        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
//        whiteRoundedView.layer.masksToBounds = false
//        whiteRoundedView.layer.cornerRadius = 2.0
//        whiteRoundedView.layer.borderWidth = 1
//        whiteRoundedView.layer.borderColor = UIColor.lightGray.cgColor
//        // whiteRoundedView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
//        // whiteRoundedView.layer.shadowOpacity = 0.3
//        cell.contentView.addSubview(whiteRoundedView)
//        cell.contentView.sendSubview(toBack: whiteRoundedView)
//
//    }

    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
