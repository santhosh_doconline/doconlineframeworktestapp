//
//  AllergiesCollectionViewCell.swift
//  DocOnline
//
//  Created by Kiran Kumar on 25/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class AllergiesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var bt_allergyDelete: UIButton!
    @IBOutlet var lb_allergyName: UILabel!
}
