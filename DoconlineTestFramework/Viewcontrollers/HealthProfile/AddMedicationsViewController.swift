//
//  AddMedicationsViewController.swift
//  AcceleratorSampleApp-Swift
//
//  Created by dev-3 on 24/06/17.
//  Copyright © 2017 Tokbox, Inc. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

var addMediUrlString = ""
var addMediHttpMethod = ""
var addMediMedicationID = ""




extension UIView {
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
 
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0.0, height: 0.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 2.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        
        
    }
}


protocol AddMedicationDelegate {
    func didAddMedication(_ success:Bool)
}

class AddMedicationsViewController: UIViewController ,NVActivityIndicatorViewable {

    @IBOutlet weak var vw_nameField: UIView!
    @IBOutlet weak var vw_dateFrom: UIView!
    @IBOutlet weak var vw_dateTo: UIView!
    @IBOutlet weak var vw_intake: UIView!
    
    @IBOutlet var lb_textieldPlaceholder: UILabel!
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var tf_intake: UITextField!
    @IBOutlet weak var tf_toDate: UITextField!
    @IBOutlet weak var tf_dateFrom: UITextField!
    @IBOutlet weak var tf_days: UITextField!
    @IBOutlet weak var tf_status: UITextField!
    
    @IBOutlet var bt_save: UIButton!
    @IBOutlet weak var tv_notes: UITextView!
    
    ///Takes the from date string
    var selectedDateFrom = ""
    ///Takes the selected to date string
    var selectedDateTo = ""
    ///takes the selected intake time
    var selectedIntakeTime = ""
    
    ///Instance for from date
    var fromDate : Date?
    ///Instance of to date
    var toDate : Date?
    
    var arrSearch : NSArray?
    var dictSearch : NSDictionary?
    var dosage_name : String = ""
    var dosage_id : String = ""
    var keyboardHeight : CGFloat = 0.0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var LC_tableViewHeight: NSLayoutConstraint!
    //medicineSearch
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tf_intake.isEnabled = false
        self.tf_dateFrom.isEnabled = false
        self.tf_toDate.isEnabled = false
        
    //    shadowEffect(views: [vw_nameField,vw_dateFrom,vw_dateTo,vw_intake])
        self.tv_notes.layer.cornerRadius = 12
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddMedicationsViewController.popView), name: NSNotification.Name(rawValue: "PopView"), object: nil)
        showMedicationDetailsIfEditTapped()
        
          print("Edit Urlstring:\(addMediUrlString) method:\(addMediHttpMethod) id:\(addMediMedicationID)")
        
        LC_tableViewHeight.constant = 0.0
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_save.backgroundColor = Theme.buttonBackgroundColor
    }

    ///pops the view controller
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///method shows the medication details which needs to be edited
    func showMedicationDetailsIfEditTapped() {
        if UserDefaults.standard.bool(forKey: "EditTapped") {
            let medicationDetails = App.medications[App.editIndex]
            self.tf_name.text = medicationDetails.name
            self.selectedDateFrom = medicationDetails.dateFrom ?? ""
            self.selectedDateTo = medicationDetails.toDate ?? ""
            self.selectedIntakeTime = medicationDetails.intakeTime ?? ""
            self.tf_dateFrom.text = self.getFormattedDateOnly(dateString:  self.selectedDateFrom)
            self.tf_toDate.text = self.getFormattedDateOnly(dateString:   self.selectedDateTo)
            self.tf_intake.text = medicationDetails.intakeTime
            self.tv_notes.text = medicationDetails.notes
            self.lb_textieldPlaceholder.isHidden = true
            
            if medicationDetails.numberofdays == 0
            {
                self.tf_days.text = ""
            }
            else
            {
                self.tf_days.text = String(medicationDetails.numberofdays!)
            }
            
            if medicationDetails.status == 0
            {
                self.tf_status.text = ""
            }
            else if medicationDetails.status == 1
            {
                self.tf_status.text = "Present Medication"
            }
            else if medicationDetails.status == 2
            {
                self.tf_status.text = "Past Medication"
            }
        }
    }
    
    @IBAction func statusTapped(_ sender: UIButton) {
        
        let actionView = UIAlertController(title: "Medication status?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let selfAction = UIAlertAction(title: "Present Medication", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_status.text = "Present Medication"
            
        }
        let onlyAlcoholic = UIAlertAction(title: "Past Medication", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_status.text = "Past Medication"
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in

        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(selfAction)
        actionView.addAction(onlyAlcoholic)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func dateTapped(_ sender: UIButton) {
        
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
                (date) -> Void in
            if let dateSelcted = date {
                let dateSt = self.getDateString(date: dateSelcted)
                let formStr = self.getDateFormat(date: date!)
                let seperatedDate = dateSt.components(separatedBy: " ")
               print("MainDate:\(date!) stringD:\(dateSt) formatted:\(self.getFormattedDate(dateString: formStr))")
                if sender.tag == 1 {
                    self.selectedDateFrom = seperatedDate[0]
                    self.fromDate = dateSelcted
                    self.tf_dateFrom.text =  self.getFormattedDate(dateString: formStr)
                }else{
                    self.selectedDateTo = seperatedDate[0]
                    self.toDate = dateSelcted
                    self.tf_toDate.text = self.getFormattedDate(dateString: formStr)
                }
                
                print("seperated date:\(seperatedDate[0])")
                
                if !(self.tf_dateFrom.text?.isEmpty)! && !(self.tf_toDate.text?.isEmpty)!{
                    let diff = self.toDate!.interval(ofComponent: .day, fromDate: self.fromDate!)
                    print("number of dates:\(diff)")
                }
            }
         }
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.tf_dateFrom.text = ""
            self.selectedDateFrom = ""
        case 2:
            self.tf_toDate.text = ""
             self.selectedDateTo = ""
        case 3:
            self.tf_intake.text = ""
        
        default:
            print("No tag")
        }
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        DatePickerDialog().show("Select Intake Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {
            (date) -> Void in
            if let dateSelcted = date {
                let dateString = dateSelcted.toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
                let separate = dateString.components(separatedBy: " ")
                print("Date string:\(self.getTimeInAMPM(date: separate[1]))   string:\(dateString)")
                self.selectedIntakeTime = separate[1]
                self.tf_intake.text = self.UTCToLocalHHMM(date: separate[1])
                print("Local Time : \(self.UTCToLocalHHMM(date: separate[1])) ")
            }
        }
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        let medicationName = self.tf_name.text!
        let dateFrom = self.tf_dateFrom.text!
        let dateto = self.tf_toDate.text!
        let intake = self.tf_intake.text!
        let notes = self.tv_notes.text!
        let no_of_days = self.tf_days.text!.isEmpty ? 0 : Int(self.tf_days.text!)
        var medication_status = 0
        
        if self.tf_status.text! == ""{
            medication_status = 0
        }
        else if self.tf_status.text! == "Present Medication"{
            medication_status = 1
        }
        else if self.tf_status.text! == "Past Medication"{
            medication_status = 2
        }
        
        if medicationName.isEmpty == true {
           self.didShowAlert(title: "Medication Name", message: "Required!")
           // AlertView.sharedInsance.showInfo(title: "Medication Name", message: "Required!")
        }else if medicationName.count < 3 {
              self.didShowAlert(title: "Medication Name", message: "Minimum 3 characters required")
           // AlertView.sharedInsance.showInfo(title: "Medication Name", message: "should be minimum of three characters")
        }else if dateFrom.isEmpty == true && dateto.isEmpty == false{
            self.didShowAlert(title: "Medication", message: "From Date is Required!")
         //   AlertView.sharedInsance.showInfo(title: "Date of starting medication", message: "Required!")
        }else if dateto.isEmpty == true && dateFrom.isEmpty == false{
            self.didShowAlert(title: "Medication", message: "To Date is Required!")
          //  AlertView.sharedInsance.showInfo(title: "Medication end date", message: "Required!")
        }/*else if intake.isEmpty == true {
            self.didShowAlert(title: "Medication intake time", message: "Required!")
          // AlertView.sharedInsance.showInfo(title: "Medication intake time", message: "Required!")
        }*/else {
            
            
//            if self.fromDate! > self.toDate!{
//                print("Start date of medication shouldn't be greater than end date")
//               self.didShowAlert(title: "", message: "Start date of medication shouldn't be greater than end date")
//               //  AlertView.sharedInsance.showInfo(title: "", message: "Start date of medication shouldn't be greater than end date")
//                return
//            }

            startAnimating()
            let mediBody = "\(Keys.KEY_NAME)=\(medicationName)&\(Keys.KEY_FROM_DATE)=\(selectedDateFrom)&\(Keys.KEY_TO_DATE)=\(selectedDateTo)&\(Keys.KEY_INTAKE_TIME)=\(intake)&\(Keys.KEY_NOTES)=\(notes)&\(Keys.KEY_NO_OF_DAYS)=\(no_of_days ?? 0)&\(Keys.KEY_MEDICATION_STATE)=\(medication_status)"
           
            if !UserDefaults.standard.bool(forKey: "EditTapped") {
                let urlString = AppURLS.URL_Medications
                let httpMethod = HTTPMethods.POST
                perfromRequest(body: mediBody, URLString: urlString, httpmethod: httpMethod, tag: 1 , completionHandler:
                    { (success) in
                        if success {
                            DispatchQueue.main.async {
                             
                            }
                        }
                })
            }else {
                perfromRequest(body: mediBody, URLString: addMediUrlString, httpmethod: addMediHttpMethod, tag: 5 ,completionHandler:
                    { (success) in
                        if success {
                            DispatchQueue.main.async {
                               
                            }
                        }
                })
            }
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if App.editIndex != nil {
            App.editIndex = nil
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getSearchList(strSearch:String) {
        print("Files URL:\(strSearch)")
        self.dictSearch = NSDictionary()
        self.arrSearch = NSArray()
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        DispatchQueue.main.async {
            self.startAnimating()
        }
        let str = strSearch.withoutSpecialCharacters
        print("strSearch",str)
        let str1 = self.removeSpecialCharsFromString(text:str)
        print("strSearch1",str1)
        NetworkCall.performGet(url:"\(AppURLS.URL_MEDICATION_SEARCH)\(str1)") { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    //self.tableView.reloadData()
                }
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                DispatchQueue.main.async {
                    print("Search Data:\(data)")
                    self.dictSearch = data as NSDictionary
                    self.arrSearch = self.dictSearch?.allValues as NSArray?
                    print("arrSearch : ",self.arrSearch ?? "")
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
                self.tableView.reloadData()
                let tblHeight = self.view.frame.size.height - (self.keyboardHeight + 60.0)
                print("tblHeight .... :",tblHeight)
                if self.tableView.contentSize.height <= tblHeight
                {
                    self.LC_tableViewHeight.constant = self.tableView.contentSize.height
                }
                else
                {
                    self.LC_tableViewHeight.constant = tblHeight
                }
            }
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return text.filter {okayChars.contains($0) }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
    }

}
extension AddMedicationsViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.lb_textieldPlaceholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.lb_textieldPlaceholder.isHidden = false
        }
    }
}

extension AddMedicationsViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var characterSet = CharacterSet.letters
        characterSet.insert(charactersIn: " ")
        //let unwantedStr = string.trimmingCharacters(in: characterSet)
        let newLength = (textField.text?.count)! + string.count - range.length
        if textField == self.tf_name  {
            if(newLength >= 3){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.getSearchList(strSearch: textField.text!)
                }
            }
        }
        else if textField == self.tf_days {
            return newLength <= 3
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
}

extension AddMedicationsViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dictSearch != nil
        {
            return (dictSearch?.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicineSearch", for: indexPath)
        if dictSearch != nil
        {
            cell.textLabel?.text = arrSearch?.object(at: indexPath.row) as? String
        }
        cell.selectionStyle = .none
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if dictSearch != nil
        {
            let strName = arrSearch?.object(at: indexPath.row) as? String ?? ""
            if strName.isEmpty != true
            {
                dosage_name = strName
                tf_name.text = dosage_name
                let keys = dictSearch?.allKeys(for: strName)
                dosage_id = keys?[0] as! String
                
                self.view.endEditing(true)
                LC_tableViewHeight.constant = 0.0
            }
        }
    }
    
}

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}


