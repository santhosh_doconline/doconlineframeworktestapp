//
//  AllergiesViewController.swift
//  DocOnline
//
//  Created by dev-3 on 21/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class AllergiesViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var tableView: UITableView!
    
    let config = URLSessionConfiguration.default
    var session : URLSession!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        NotificationCenter.default.addObserver(self, selector: #selector(AllergiesViewController.adjustViewHeight), name: NSNotification.Name(rawValue: "UpdateAllergys"), object: nil)
        
        if HealthProfile.check_Health_defaults()
        {
            print("HealthDefaults stored: \(HealthProfile.check_Health_defaults())")
            if App.medications.count == 0{
                App.allergies = HealthProfile.getAllergies()
            }
        }
        else
        {
            session = URLSession(configuration: config)
            getHealthProfile(session: session)
            print("Getting health profile")
        }
        
        adjustViewHeight()
        
//        navigationController?.navigationBar.barTintColor = UIColor(red: 5, green: 66, blue: 76, alpha: 1.0)
//        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    ///Reloads the tableview with allergies
    @objc func adjustViewHeight() {
        if App.allergies.count == 0 {
            self.tableView.reloadData()
            self.tableView.isHidden = true
        }else{
            self.tableView.reloadData()
            self.tableView.isHidden = false
        }
        
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            self.stopAnimating()
        })
    }

    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
      //  toggleSideMenuView()
    }
    
    @IBAction func addAllergyTapped(_ sender: UIButton) {
        //2
     //   hideSideMenuView()
        let urlString = AppURLS.URL_Allergies
        let httpMethod = HTTPMethods.POST
        showTextFieldAlert(title: "ALLERGY", message: "", URLString: urlString, httpmethod: httpMethod,tag: 2, ActionTitle: "Add", textToEdit: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     //   hideSideMenuView()
        super.viewWillDisappear(animated)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AllergiesViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return App.allergies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.Allergies, for: indexPath) as! HealthProfileTableViewCell
        let allergyValues = App.allergies[indexPath.row]
        cell.lb_name.text = allergyValues.name
        cell.bt_deleteTapped.tag = indexPath.row
        cell.bt_edit_tapped.tag = indexPath.row
        cell.bt_deleteTapped.addTarget(self, action: #selector(AllergiesViewController.deleteAllergiesTapped(sender:)), for: UIControl.Event.touchUpInside)
        cell.bt_edit_tapped.addTarget(self, action: #selector(AllergiesViewController.editAllergiesTapped(sender:)), for: UIControl.Event.touchUpInside)
        
//        cell.backgroundColor = UIColor.white
//        cell.layer.cornerRadius = 20
//        cell.layer.masksToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.clear
        let whiteRoundedView  = UIView(frame: CGRect(x:0, y:10, width: self.view.frame.size.width,height: self.view.frame.size.height - 8))
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 5.0
        // whiteRoundedView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        // whiteRoundedView.layer.shadowOpacity = 0.3
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
    }

    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

