//
//  HomeViewController.swift
//  DocOnline
//
//  Created by dev-3 on 08/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import Firebase
//import NVActivityIndicatorView
import SwiftMessages
import AVFoundation
import Kingfisher
//import BIZPopupView
//import FBSDKLoginKit
//import GoogleSignIn
import UserNotifications
//import AppsFlyerLib
//import JJFloatingActionButton
//import Sentry


class HomeViewController: UIViewController ,NVActivityIndicatorViewable ,GlobalTimerTriggerDelegate{

    
    ///outlets
    @IBOutlet weak var porfile_pic: UIImageView!
    @IBOutlet weak var user_profile_name: UILabel!
    @IBOutlet weak var bt_menu_item: UIBarButtonItem!
    
    @IBOutlet var lb_subscriptionStatus: UILabel!
    @IBOutlet var bt_upgrade: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var LC_collectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var bg_view: UIView!
    
    @IBOutlet var medipharmaPopup: UIView!
    @IBOutlet weak var checkerImg: UIImageView!
    @IBOutlet weak var medipharmaLbl: UILabel!
    @IBOutlet weak var apnt_id: UILabel!
    @IBOutlet weak var apnt_time: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var orderNowBtn: UIButton!
    @IBOutlet weak var notNowBtn: UIButton!
    
    ///Container Blur view
    var container = UIView()
    
    
//    var menu = ["Book a Consultation","My Appointments","Order Medicines","Ask a Question","Book Diagnostics","Add-Ons"]
//    var menuIcons = ["book_consult","my_appointment","order_med","ask_q","book_diagno","add_ons"]
    
   //instance variables
    ///instance to notification button for showing badge
    var btnBarBadge : MJBadgeBarButton!
    var btnBarBadgeCart : MJBadgeBarButton!
    

    ///checks if user created account through social network so to ask set password
    var isPasswordCreated = false
    
    ///background queue
    var queue : DispatchQueue!
    
    ///main queue instance
    var mainQueue: DispatchQueue!
    
    ///Appointment id for check of appointment details
    var timerAppointmentId = ""
    
    ///Appointment id for ordering medicines
    var medicineReadyAppointmentID = ""
    
    static var doctorRatingView : BIZPopupViewController?
 
     var userDataModel : User!
     var selectedTag = 0
    
    ///Dashobard tiles info array
    var dashboardItems = [DashboardTiles]()
    
    ///For child user in to show in consent form
    var familyGuardianName = ""
    var isFromHomeViewToConsentForm = false
    
    static var homeVCDelegate: HomeVCDelegate?
    
    var removeStatus = true
    
    override  func viewDidLoad() {
        super.viewDidLoad()
//        self.dashboardItems = DashboardTiles.addDashboardContet()
//        setupCollectionView()
        removeStatus = true
        // self.postDeviceToken()
//        queueOperations()
        self.bt_upgrade.isHidden = true
        self.lb_subscriptionStatus.isHidden = true
        Theme.defaultTheme()
         App.isFromLoginView = false
        ///Initialising main queue
        mainQueue = DispatchQueue.main
        LMMenuNavViewController.menuDelegate = self
        ///initialising background queue
        queue = DispatchQueue(label: "Fetching", qos: DispatchQoS.background, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit, target: mainQueue)
        
        ///adding observer to get notifications from coredata
//        NotificationCenter.default.addObserver(self, selector: #selector(self.getNotificationsList), name: NSNotification.Name(rawValue: "NotificationsList"), object: nil)
        
        ///adding observer to get cart list
        NotificationCenter.default.addObserver(self, selector: #selector(self.getCartList), name: NSNotification.Name(rawValue: "CartList"), object: nil)
        
        ///adding observer to navigate to Health profile
        NotificationCenter.default.addObserver(self, selector: #selector(self.showHealthProfile), name: NSNotification.Name(rawValue: "HealthProfile"), object: nil)
        
        ///adding observer to update user profile name
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateProfilePic), name: NSNotification.Name(rawValue: "UserProfileName"), object: nil)
        
        ///adding observer to redirect view to diagnostic appointments
        NotificationCenter.default.addObserver(self, selector: #selector(self.diagnosisHistorySelected), name: NSNotification.Name(rawValue: "AppointmentView"), object: nil)
        
        ///adding observer to Navigate to diagnostics
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateToDiagnostics), name: NSNotification.Name(rawValue: "NavigateDiagnostics"), object: nil)
        ///adding observer to Navigate to blogs
        NotificationCenter.default.addObserver(self, selector: #selector(self.blogsSelected), name: NSNotification.Name(rawValue: "NavigateBlogs"), object: nil)
        
        makeProfilePicRound()
        setupNotificationBarButton()
        
        medipharmaPopup.frame = self.view.bounds
        
        queue.async {
            if App.totalLanguages.count == 0 {
                Languages.getLanguages(completionHandler: { success in
                    if success {
                       print("Fetched Languages")
                    }
                })
            }
            
            if !App.isIncomingCallRecieved {
//                self.fetchNotifications()
               // self.check_password()
                self.getCountriesList()
                self.postDeviceToken()
                self.checkAndAlertUserIfPrescriptionReadyForAnyAppointments()
                RefreshTimer.sharedInstance.getAppointments()
                self.checkIsFamilyMem()
                if App.appSettings == nil {
                    AppSettings.getAppSettings {}
                }
            }
           // self.check_mobile_verifield()
            //self.getCartCollection()
        }
    
        RefreshTimer.sharedInstance.runTimer()
        RefreshTimer.sharedInstance.globalCountDownTimer?.delegate = self
        RefreshTimer.sharedInstance.delegate = self
        
        
        
        ///setting up custom navigation back button
        let backImage = UIImage(named: "BoldNavBackButton")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.gotoSubscriptions(_:)))
        self.lb_subscriptionStatus.addGestureRecognizer(tapGesture)
        
        if App.userInfo != nil{
            if let notificationType = App.userInfo!["notification_type"] as? String
            {
                if notificationType.contains("Blogs"){
                    if let blog_url = App.userInfo!["blog_url"] as? String{
                        App.blogUrl = blog_url
                        self.blogsSelected()
                    }
                }
            }
        }
    }
    
    @objc func gotoSubscriptions(_ tapGesture:UITapGestureRecognizer) {
        App.isFromView = FromView.SubscriptionPlans
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
        
    }
    
    func setupCollectionView() {
        self.collectionView.reloadData()
        DispatchQueue.main.async {
            self.LC_collectionViewHeight.constant = self.collectionView.contentSize.height  //UIScreen.main.bounds.height - 120viewHeight > 568 ? 130 : 155
            
            
            UIView.animate(withDuration: 0.5) {
                // let minus = UIScreen.main.bounds.height - self.collectionView.contentSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func disableCellsForUnRegisteredUser(cell:UIHomeCollectionViewCell,indexPath:IndexPath) {
       // if App.getUserAccessToken().isEmpty {
            switch indexPath.row {
            case 0:
                cell.isUserInteractionEnabled = true
                cell.alpha = 1.0
            case 1:
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.5
            case 2:
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.5
            case 3:
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.5
            case 4:
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.5
            case 5:
                cell.isUserInteractionEnabled = true
                cell.alpha = 1.0
            default:
                print("Nothing to do")
            }
       // }
    }
    
//    func checkNotificationRegistered() {
//        NotificationCenter.current().getNotificationSettings { (settings) in
//            if settings.authorizationStatus == .authorized {
//                  print("\n***Registered for remote notifications***\n")
//            }
//            else {
//                   print("\n*** Not Registered for remote notifications***\n")
//                // Either denied or notDetermined
//                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
//                    (granted, error) in
//                    // add your own
//                    UNUserNotificationCenter.current().delegate = self
//                    let alertController = UIAlertController(title: "Notification Alert", message: "please enable notifications", preferredStyle: .alert)
//                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
//                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
//                            return
//                        }
//                        if UIApplication.shared.canOpenURL(settingsUrl) {
//                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                            })
//                        }
//                    }
//                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
//                    alertController.addAction(cancelAction)
//                    alertController.addAction(settingsAction)
//                    DispatchQueue.main.async {
//                        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//
//                    }
//                }
//            }
//        }
//    }
    
    override  var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override  var shouldAutorotate: Bool {
        return false
    } 
    
    ///GlobalTimer delegate method to perform navigation to Appointment view
    func timerTapped(_ sender: UIGestureRecognizer, app_id: Int) {
        self.timerAppointmentId = "\(app_id)"
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APPOINTMENT_CHECK, sender: self)
    }
    
    
    /**
       Method performs view instantiation to health profile after password confirmation.
     */
    @objc func showHealthProfile() {
        if App.isPasswordConfirmed {
            instantiate_to_view(withIdentifier: StoryBoardIds.ID_HEALTH_PROFILE_NAV)
        }
    }
    

    /**12
       perform main queue operations method
     */
    func queueOperations() {
        mainQueue.async {
            self.check_password_show_alert()
            self.updateProfilePic()
        }
    }

    /**
       Instance notification button instantiation method
     */
    func setupNotificationBarButton() {
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onNotificationButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "bell_icon"), for: .normal)
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        self.btnBarBadge.badgeValue = ""
        //self.navigationItem.rightBarButtonItem = self.btnBarBadge
        
        let customButton1 = UIButton(type: UIButton.ButtonType.custom)
        customButton1.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton1.addTarget(self, action: #selector(self.onCartButtonClick), for: .touchUpInside)
        customButton1.setImage(UIImage(named: "cart_empty"), for: .normal)
        self.btnBarBadgeCart = MJBadgeBarButton()
        self.btnBarBadgeCart.setup(customButton: customButton1)
        self.btnBarBadgeCart.badgeOriginX = 20.0
        self.btnBarBadgeCart.badgeOriginY = -4
        self.btnBarBadgeCart.badgeValue = String(describing: App.cartCount)
        
        let fixedSpace = UIBarButtonItem(
            barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace,
            target: nil,
            action: nil
        )
        fixedSpace.width = 0
        
        self.navigationItem.rightBarButtonItems = [self.btnBarBadge,fixedSpace,self.btnBarBadgeCart]    }
    
    /**
      method fetches notifications from core data
     */
//    @objc func getNotificationsList() {
//        do {
//            App.notifications.removeAll()
//            App.notifications = try App.coreDatacontext.fetch(Notifications.fetchRequest())
//            let count = App.notifications.count
//            print("Notifications count:\(count) array:\(App.notifications)")
//            if count > 0 {
//                self.btnBarBadge.badgeValue = "\(getNotificationsReadCount())"
//            }else {
//                App.notificationReadCount = 0
//                UserDefaults.standard.set(App.notificationReadCount, forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT)
//            }
//
//            DispatchQueue.main.async(execute: {
//                self.stopAnimating()
//            })
//        } catch let error{
//            print("Fetching Failed  Notifications:\(error.localizedDescription)")
//        }
//    }
    
    /**
     method fetches notifications to update Cart
     */
    @objc func getCartList() {
        self.btnBarBadgeCart.badgeValue = String(describing: App.cartCount)
    }
    
    @objc func navigateToDiagnostics() {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_SEGUE, sender: self)
    }
    
    /**
      Method called when notification button tapped and Shows notifications view
     */
   @objc func onNotificationButtonClick() {
        print("Notification button Clicked ")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIds.ID_NOTIFICATIONS) as! UINavigationController
        self.present(vc, animated: true, completion: nil)
    }
    
    /**
     Method called when cart button tapped and Shows cart view
     */
    @objc func onCartButtonClick(){
        print("Cart button Clicked ")
        if(App.cartCount != 0)
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_CART_SEGUE, sender: self)
        }
        else
        {
            self.didShowAlert(title: "Cart", message: "There are no items in cart")
        }
    }

    /**
       method used to make profile pic round on view load
     */
    func makeProfilePicRound() {
        self.porfile_pic.layer.cornerRadius = self.porfile_pic.frame.size.width / 2
        self.porfile_pic.layer.borderColor = Theme.buttonBackgroundColor!.cgColor
        self.porfile_pic.layer.borderWidth = 1
        self.porfile_pic.clipsToBounds = true
        
         self.porfile_pic.layer.shadowOpacity = 0.5
         self.porfile_pic.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
         self.porfile_pic.layer.shadowRadius = 5.0
         self.porfile_pic.layer.shadowColor = UIColor.gray.cgColor
    }
    
  
    /**
      method used to check if social login and ask for set password for security
     */
    func check_password_show_alert() {
        //self.stopAnimating()
        print("password status: \(App.isPasswordCreated)  defaults\(UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_SET_PASSWORD))")
        
        if App.didAskForSetPassword {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_SET_PASSWORD){
                App.didAskForSetPassword = false
               let messageView : MessageView = MessageView.viewFromNib(layout: MessageView.Layout.centeredView)
                messageView.configureBackgroundView(width: 250)
                messageView.configureContent(title: "Account information", body: "set password for better security reason", iconImage: nil , iconText: nil, buttonImage: nil, buttonTitle: "Set now") { _ in
                    App.isFromPasswordSetOption = true
                    SwiftMessages.hide()
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SET_PASSWORD, sender: self)
                }
                messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
                messageView.backgroundView.layer.cornerRadius = 12
                var config = SwiftMessages.defaultConfig
                config.presentationStyle = .center
                config.duration = .forever
                config.dimMode = .blur(style: .dark, alpha: 0.8, interactive: true)
                config.presentationContext  = .window(windowLevel: .statusBar)
                SwiftMessages.show(config: config, view: messageView)
            }
        }
    }
    
    func checkIsFamilyMem() {
        let url = URL(string: AppURLS.CONSENT_STATUS_FAMILY_MEM)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.GET
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
            }
            do
            {
                if let respData = data ,let resultJSON = try JSONSerialization.jsonObject(with: respData, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                    print("Family mem consent status check:\(resultJSON)")
                    if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary , let name = data.object(forKey: Keys.KEY_NAME) as? String {
                        if (data.object(forKey: Keys.KEY_BOOKING_CONSENT) as? Int) == nil {
                            self.familyGuardianName = name
                            self.isFromHomeViewToConsentForm = true
                            DispatchQueue.main.async {
                                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_FAMILY_MEMBERS, sender: self)
                            }
                        }
                    }
                }
            }catch let err {
                print("Error while checking status:\(err.localizedDescription)")
            }
        }.resume()
     }
    
    /**
     method used to performing network action to check any appointment has prescription generated and alert user
     */
    func checkAndAlertUserIfPrescriptionReadyForAnyAppointments() {
        let medicineCheckUrl = AppURLS.URL_MEDICINES_READY_TO_ORDER_CHECK
        NetworkCall.performGet(url: medicineCheckUrl) { (success, response, statusCode, error) in
        //    print("Medicine ready for oreder :\(success) status :\(statusCode) Response :\(response)")
            
            if let err = error {
                print("Medicine ready for oreder Error:\(err.localizedDescription)")
            }
            
            print("medipharama res \(response)")
            print("status code \(statusCode) res \(response?.object(forKey: Keys.KEY_DATA) as? NSDictionary)")
            if statusCode == 200 && success{
                if let resp = response {
                    if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                        if let id = data.object(forKey: Keys.KEY_ID) as? Int {
                            let time = data.object(forKey: Keys.KEY_MEDIPHARMA_TIME) as? String
                            print("apnt medicine id \(id)")
                            self.medicineReadyAppointmentID = "\(id)"
                            let existingId = UserDefaults.standard.value(forKey: "apntId") as? Int
                            let existingStatus = UserDefaults.standard.value(forKey: "status") as? Bool
                            if  existingId == nil || existingStatus == nil || existingId != id {
//                            DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(2) , execute: {
//                                let alert = UIAlertController(title: "Your prescribed medicines are ready", message: "Click here to order your prescribed medicines", preferredStyle: UIAlertController.Style.alert)
//                                let orderNow = UIAlertAction(title: "Order Now", style: .default, handler: { (action) in
//
//                                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ENTER_PINCODE, sender: self)
//                                })
//
//                                let notNow = UIAlertAction(title: "Not now", style: .default, handler: nil)
//                                alert.addAction(orderNow)
//                                alert.addAction(notNow)
//                                self.present(alert, animated: true, completion: nil)
//                            })
                                UserDefaults.standard.removeObject(forKey: "apntId")
                                UserDefaults.standard.removeObject(forKey: "status")
                                
                                DispatchQueue.main.async {
                                    self.apnt_id.text = " \(id)"
                                    // create dateFormatter with UTC time format
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                                    
                                    let date = dateFormatter.date(from: time!)// create   date from string
                                    // change to a readable time format and change to local time zone
                                    dateFormatter.dateFormat = "dd-MMM-yyyy h:mm a"
                                    dateFormatter.timeZone = NSTimeZone.local
                                    let timeStamp = dateFormatter.string(from: date!)
                                    
                                    self.apnt_time.text = timeStamp
                                    self.container.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
                                    self.container.backgroundColor = .black
                                    self.container.alpha = 0.7
                                    self.view.addSubview(self.container)
                                    self.view.addSubview(self.medipharmaPopup)
                                }
                        }
                        }else {
                             self.medicineReadyAppointmentID = ""
                        }
                        
//                        if let pid = data.object(forKey: Keys.KEY_PUBLIC_APPOINTMENT_ID) as? Int {
//
//                        }
                    }
                }
            }else {
                UserDefaults.standard.removeObject(forKey: "apntId")
                UserDefaults.standard.removeObject(forKey: "status")
                self.medicineReadyAppointmentID = ""
            }
        }
    }
    
    var isSelected = false
    @IBAction func medipharmaToggle(_ sender: UIButton) {
        //uncomment below comments
        if isSelected{
            checkerImg.image = UIImage(named: "UncheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
//            UserDefaults.standard.set(Int(self.medicineReadyAppointmentID), forKey: "apntId")
//            UserDefaults.standard.set(true, forKey: "status")
            isSelected = false
        }else{
            checkerImg.image = UIImage(named: "CheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
//            UserDefaults.standard.removeObject(forKey: "apntId")
//            UserDefaults.standard.removeObject(forKey: "status")
            isSelected = true
        }
    }
    
    @IBAction func notNowAction(_ sender: UIButton) {
        //remove from here
        if isSelected{
//            checkerImg.image = UIImage(named: "unCheckbox")
            UserDefaults.standard.set(Int(self.medicineReadyAppointmentID), forKey: "apntId")
            UserDefaults.standard.set(true, forKey: "status")
//            isSelected = false
        }else{
//            checkerImg.image = UIImage(named: "checkbox")
            UserDefaults.standard.removeObject(forKey: "apntId")
            UserDefaults.standard.removeObject(forKey: "status")
//            isSelected = true
        }
        //till here
        container.removeFromSuperview()
        medipharmaPopup.removeFromSuperview()
    }
    
    @IBAction func orderNowAction(_ sender: UIButton) {
        container.removeFromSuperview()
        medipharmaPopup.removeFromSuperview()
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ENTER_PINCODE, sender: self)
    }
    

    /**
     method used to performing network action to check user has created password or not
     */
    func check_password() {
          print("==>4")
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: AppURLS.URL_ChangePassword)
            print("URL changepassword ==>\(AppURLS.URL_ChangePassword)")
            var request = URLRequest(url: url!)
            //request.timeoutInterval = 25
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                       /// self.stopAnimating()
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling password status check :\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! Bool
                                print("Check passsword response:\(jsonData)")
                                if code == 200 && responseStatus == "success"
                                {
                                    UserDefaults.standard.set(data, forKey: UserDefaltsKeys.KEY_SET_PASSWORD)
                                    if data {
                                       App.isPasswordCreated = data
                                       App.didAskForSetPassword = data
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        self.check_password_show_alert()
                                    })
                                    
                                }else {
                                    print("Error while fetching appointment details:\(responseStatus) ")
                                }
                            }
                            
                            self.queueOperations()
//                            DispatchQueue.main.async(execute: {
//                                self.stopAnimating()
//                            })
                        }
                        catch let error
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON:\(error.localizedDescription)")
                        }
                        
                    }
                }
            }).resume()
    }
    
    
    /**
       Method used to post device fcm token to server for push notifications services
     */
    func postDeviceToken() {
          print("==>3")
//        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_TOKEN_REGISTRATION) {
//            print("Token already registered")
//        }
//        else
//        {
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
              //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            var token = ""
        if let fcmToken = AppConfig.shared?.fcmToken {
                token = fcmToken
            }
            
            if App.apvoip_token.isEmpty || token.isEmpty {
                print("Voip token didnt get")
                UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
            }else {

                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)
                let bookURL = AppURLS.URL_UserDevices
                let url = URL(string: bookURL)
                var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
                request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
                request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
                // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
                //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
                request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
                request.httpMethod = HTTPMethods.POST
                
                let postData = "\(Keys.KEY_DEVICE_TOKEN)=\(token)&\(Keys.KEY_APVOIP_TOKEN)=\(App.apvoip_token)&type_of=2&\(Keys.KEY_EXTRA_DATA)=\(App.getDeviceInfo())" //\(Keys.KEY_APVOIP_TOKEN)=\(token)
                
                print("Device Data==>\(postData)")
                print("URL==>\(bookURL)")
                request.httpBody = postData.data(using: String.Encoding.utf8)
                session.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let error = error
                    {
                        DispatchQueue.main.async(execute: {
                            //self.stopAnimating()
                            print("Error==> : \(error.localizedDescription)")
                            //self.didShowAlert(title: "", message: error.localizedDescription)
                        })
                    }
                    if let data = data
                    {
                        print("data =\(data)")
                    }
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        //if you response is json do the following
                        do
                        {
                            let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                            
                            if !errorStatus
                            {
                                // UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
                                print("performing error handling device token:\(resultJSON)")
                            }
                            else {
                                
                                let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                //  let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                print("Device token result:\(resultJSON)")
                                
                                if code == 200 //&& responseStatus == "success"
                                {
                                    UserDefaults.standard.set(true, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
                                    
                                }else {
                                    UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
                                    print("Error while fetching data")
                                }
                                
                            }
                        }catch let error{
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                            })
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON : \(error.localizedDescription)")
                        }
                        
                    }
                    
                }).resume()
            }
       // }
        //self.check_password()
    }
    
    /**
     Method used to post LeadSource to server for LMS
     */
    func postLeadSource() {
        var media_source : String = ""
        if let key = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_MEDIA_SOURCE)
        {
            print("key media_source:\(key)")
            media_source = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_MEDIA_SOURCE) as! String
        }
        
        var utm_campaign : String = ""
        if let key = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_UTM_CAMPAIGN)
        {
            print("key utm_campaign:\(key)")
            utm_campaign = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_UTM_CAMPAIGN) as! String
        }
        
        var utm_medium : String = ""
        if let key = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_UTM_MEDIUM)
        {
            print("key utm_medium:\(key)")
            utm_medium = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_UTM_MEDIUM) as! String
        }
        
        if media_source.isEmpty {
            print("media_source empty")
        }else {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_LeadSource
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            
            let postData = "\(Keys.KEY_MEDIA_SOURCE)=\(media_source)&\(Keys.KEY_UTM_CAMPAIGN)=\(utm_campaign)&\(Keys.KEY_UTM_MEDIUM)=\(utm_medium)"
            
            print("post Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error while posting lead==> : \(error.localizedDescription)")
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("media_source result:\(resultJSON)")
                    }catch let error{
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
            }).resume()
        }
    }
    
  
    /**
       Method used get user profile picture
     */
    func getUserAvatar() {

        if !NetworkUtilities.isConnectedToNetwork()
        {
            didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
          // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: AppURLS.URL_UserAvatar)
        
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        
        request.httpMethod = HTTPMethods.GET
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if error != nil
            {
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    
                    do
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                        
                        if !errorStatus
                        {
                            print("performing error handling user avatar:\(jsonData)")
                        }
                        else {
                            
                           let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                            let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(responseStatus) code:\(code)")
                            let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            if code == 200 && responseStatus == "success"
                            {
                                App.avatarUrl = data.object(forKey: Keys.KEY_AVATAR_URL) as! String
                                print("avatar success=\(jsonData)")
                                
                            }else {
                                print("Error while fetching data")
                            }
                            
                            //Main thread to update ui after fetching data
                           DispatchQueue.main.async(execute: {
                              self.updateProfilePic()
                           })
                        }
                    }
                    catch let error
                    {
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                    }
                }
            }
        }).resume()
    }
    
  
    /**
      Method is used to update fetched profile pic called in main thread of  `getUserAvatar()`
     */
    @objc func updateProfilePic() {
        if App.avatarUrl.isEmpty != true {
            let url = URL(string: App.avatarUrl)
            self.porfile_pic.kf.setImage(with: url, placeholder: self.porfile_pic.image, options: nil, progressBlock: nil, completionHandler: nil)
            print("Avatar url:\(App.avatarUrl)")
            
            if App.user_full_name.isEmpty != true {
                self.user_profile_name.isHidden = false
                self.user_profile_name.text =  "Hello, " + App.user_full_name
            }else {
                self.user_profile_name.text =  "Hello"
            }
            
//            if let userName = User.get_user_profile_default().first_name , let lastName = User.get_user_profile_default().last_name {
//                self.user_profile_name.isHidden = false
//                self.user_profile_name.text = userName + " \(lastName)"
//            }
        }else {
             self.user_profile_name.isHidden = false
             self.porfile_pic.image = UIImage(named: "Default-avatar")
             self.user_profile_name.text =  "Hello"
        }
        
        
        DispatchQueue.main.async {
            self.bt_upgrade.layer.cornerRadius =  11.5
            self.bt_upgrade.layer.borderColor = UIColor.darkGray.cgColor
            self.bt_upgrade.layer.borderWidth = 1
            if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("b2b") {
                self.lb_subscriptionStatus.text = "You are under DOCONLINE ENTERPRISE membership"
                self.lb_subscriptionStatus.isHidden = false
                 self.bt_upgrade.isHidden = true
            }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("promo") {
                if App.promoCodeExpiration == "0" {
                    self.lb_subscriptionStatus.text = "Promo code will expire today"
                }else {
                    self.lb_subscriptionStatus.text = "Promo code will be expired in \(App.promoCodeExpiration.uppercased()) days"
                }
                self.lb_subscriptionStatus.isHidden = false
                self.bt_upgrade.isHidden = true
            }
            else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("b2c"){
                
                func setupSubscriptionInfo(subDetails:SubscriptionPlan) {
                    self.lb_subscriptionStatus.text = "Your are under \(subDetails.plan_name != nil ? subDetails.plan_name!.uppercased() : "")"
                    self.lb_subscriptionStatus.isHidden = false
                    if App.canUpgradeSubscription {
                        self.bt_upgrade.isHidden = false
                        self.bt_upgrade.setTitle("Upgrade", for: .normal)
                    }else {
                        self.bt_upgrade.isHidden = true
                    }
                }
                
                if let subDetails = App.subscription_details {
                    setupSubscriptionInfo(subDetails: subDetails)
                }else if let subDetails = SubscriptionPlan.getSavedSubscriptionPlanDetails(key: UserDefaults.Keys.encodedCurrentPlanDetails) {
                    setupSubscriptionInfo(subDetails: subDetails)
                }else {
                    self.lb_subscriptionStatus.text = "Pick a membership as per your convenience"//"Your are not subscribed to any plan"
                    self.lb_subscriptionStatus.isHidden = false
                    self.bt_upgrade.setTitle("Choose", for: .normal)
                    self.bt_upgrade.isHidden = false
                }
            }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.ONE_TIME) || App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.B2BPAID){
                
                self.lb_subscriptionStatus.text =  "You have 1 consultation credit"
                self.lb_subscriptionStatus.isHidden = false
                self.bt_upgrade.isHidden = true
                
            }else if App.didUserSubscribed  {
                self.lb_subscriptionStatus.text =  "You are under DOCONLINE \(App.userSubscriptionType.uppercased()) membership"
                self.lb_subscriptionStatus.isHidden = false
                self.bt_upgrade.isHidden = true
            }else {
                self.lb_subscriptionStatus.text = "Pick a membership as per your convenience" //"Your are not subscribed to any plan"
                self.lb_subscriptionStatus.isHidden = false
                self.bt_upgrade.setTitle("Choose", for: .normal)
                self.bt_upgrade.isHidden = false
            }
            //"Choose Membership to start using DocOnline's services"
            
            //if App.fitmeinType || App.hraType{
                self.setupCollectionView()
            //}
        }
    }
    

    /**
       Method is used to show Video or audio call duration alert
       * shows alert only when call attended by user and time duration is not nil.
     */
    func showCallDurationWithInfo() {
        if App.timeDuration.isEmpty != true {
            if callDoctorModal != nil {
                if callDoctorModal.avatar_url.isEmpty != true {
                    if let imageData = NSData(contentsOf:  URL(string: callDoctorModal.avatar_url! )!) {
                        let messageView : MessageView = MessageView.viewFromNib(layout: MessageView.Layout.centeredView)
                        messageView.configureBackgroundView(width: 250)
                        messageView.configureIcon(withSize: CGSize(width: 60, height: 60))
                        messageView.iconImageView?.layer.cornerRadius =  60 / 2
                        messageView.iconImageView?.layer.borderColor = UIColor.red.cgColor
                        messageView.iconImageView?.layer.borderWidth = 1
                        messageView.iconImageView?.clipsToBounds = true
                        messageView.iconImageView?.contentMode = .scaleAspectFill
                        
                        messageView.configureContent(title: "\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)", body: "Call duration\n\(App.timeDuration)", iconImage:  UIImage(data: imageData as Data) , iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { _ in
                            App.timeDuration = ""
                            SwiftMessages.hide()
                        }
                        
                        messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
                        messageView.backgroundView.layer.cornerRadius = 12
                        var config = SwiftMessages.defaultConfig
                        config.presentationStyle = .center
                        config.duration = .forever
                        config.dimMode = .blur(style: .dark, alpha: 0.85, interactive: true)
                        config.presentationContext  = .window(windowLevel: .statusBar)
                       // DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                            SwiftMessages.show(config: config, view: messageView)
                       // })
                    }
                }
            }
        }
    }
    
    
    func updateTheme(){
        DispatchQueue.main.async {
//            self.indicator.color = Theme.buttonBackgroundColor
            self.navigationController?.navigationBar.setGradientBackground(colors: Theme.navigationGradientColor as! [UIColor])
            self.orderNowBtn.backgroundColor = Theme.buttonBackgroundColor
            self.notNowBtn.backgroundColor = Theme.buttonBackgroundColor
            self.checkerImg.image = UIImage(named: "UncheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        }
    }

    override  func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)

//            self.bg_view.removeFromSuperview()

            if self.bg_view != nil{
                self.bg_view.removeFromSuperview()
            }
            
            self.removeStatus = false
            return
        }
         self.getUserState { (success,newuser) in
            print("Finish fetching userstate : \(success) \(newuser)")
            print("user type after \(App.user_type)")
            if self.removeStatus{
                self.dashboardItems = DashboardTiles.addDashboardContet()
                print("tiles count \(self.dashboardItems.count) \(self.dashboardItems)")
                
                DispatchQueue.main.async {
                    
                    self.updateTheme()
                    self.setupCollectionView()
                    self.bg_view.removeFromSuperview()
                    self.removeStatus = false
                }
            }
            if(newuser == 1)
            {
                self.postLeadSource()
            }
            self.queueOperations()
        }
      
       // self.postDeviceToken()
//        queueOperations()
        RefreshTimer.sharedInstance.updateTimerValues()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        if !self.didCheckForNotificationsAllowedOrNot() {
//            self.didShowAlertForDeniedPermissions(message: "Please enable permission for sending push notifications in settings")
//        }
        
//        DispatchQueue.main.async {
//            print("Dashbaord appointment refreshed")
//            self.dashboardItems = DashboardTiles.addDashboardContet()
//            self.collectionView.reloadData()
//        }

         //updateProfilePic()
       ///Changes notification badge count if changed
        
        ///Makes navigation if from payment success page
        if App.isFromPaymentSuccessPage {
            if App.goToViewFromPayment == 1 {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BOOK_CONSULTATION_SEGUE, sender: self)
            }else if App.goToViewFromPayment == 2 {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MYBILLINGS, sender: self)
            }
            App.isFromPaymentSuccessPage = false
            App.goToViewFromPayment = 0
        }
        
        
        if let noticount = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT) as? Int{
            print("88=\(noticount)")
            if noticount > 0 {
                self.btnBarBadge.badgeValue = "\(noticount)"
            }else {
                self.btnBarBadge.badgeValue = ""
            }
        }
        
//        HomeViewController.homeVCDelegate?.updateAppsettings()
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            if let subscription = SubscriptionPlan.getSavedSubscriptionPlanDetails(key: UserDefaults.Keys.onlySubscriptionInfo) {
                App.didUserSubscribed = subscription.subscribed ?? false
                App.canUpgradeSubscription = subscription.doUserCanUpgrate ?? false
                App.b2bUserType = subscription.subscribedUserType ?? ""
                App.userSubscriptionType = subscription.subscribedType ?? ""
                print("is b2b? \(subscription.subscribedUserType)")
                self.updateProfilePic()
            }
            
            if let user = User.getUserProfileDetails() {
                self.user_profile_name.text =  "Hello, " + (user.full_name ?? "")
            }
        }
        
//        self.showRatingAlertWithCallDuration()
    }
    
    ///Displays alert with call duration and rating option on call
    func showRatingAlertWithCallDuration() {
        if App.timeDuration.isEmpty != true ,callDoctorModal != nil, let appointmentId = callDoctorModal.appointment_id {
            let ratingURl = AppURLS.URL_BookAppointment + "/\(appointmentId)/rating"

            self.getRatingStatus(urlString: ratingURl, httpMethod: HTTPMethods.GET, jsonData: nil, type: 1) { (success, response) in
                DispatchQueue.main.async {
                    if success , let resp = response ,
                        let rating = resp.object(forKey: Keys.KEY_DATA) as? Int , rating == 0 {
                        guard let ratingView = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_DOCTOR_RATING_VIEW) as? RatingView else { return }
                        ratingView.isImmediateAfterCall = true
                        ratingView.callDuration = App.timeDuration
                        ratingView.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 300)
                        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: ratingView)
                        segue.configure(layout: .bottomCard)
                        segue.dimMode = .blur(style: .dark, alpha: 0.7, interactive: false)
                        segue.messageView.configureDropShadow()
                        segue.presentationStyle = .bottom
                        self.prepare(for: segue, sender: nil)
                        segue.perform()

                    }else {
                        self.showCallDurationWithInfo()
                    }
                }
            }
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
     //   self.navigationController?.defaultNavigation()
        
        ///Making time duration empty to not show call duration every time home screen appears
          if App.timeDuration.isEmpty != true {
            App.timeDuration = ""
          }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // hideSideMenuView()
        super.viewWillDisappear(animated)
    
    }
    
    /**
     Method performs request to server to validate password
     - Parameter data : takes the converted jsondata
     - Parameter tag : used to check which menu tapped
     */
    func checkPassword(data:Data , tag:Int) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        

        let urlString = AppURLS.URL_PASSWORD_CHECK
        print("Password Check url = \(urlString)")
        let passconfig = URLSessionConfiguration.default
        let passwordSession = URLSession(configuration: passconfig)

        let url = URL(string: urlString)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = data
        startAnimating()
        print("Password data:\(data)")
        passwordSession.dataTask(with: request) { (data, response, error) in

            if  error != nil {
                print("Error while checking passowrd:\(error!.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: error!.localizedDescription )
                }
            }else {
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    _ = self.check_Status_Code(statusCode: httpResponse.statusCode, data: nil)
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        if httpResponse.statusCode == 204 {
                           UserDefaults.standard.set(Date(),forKey:UserDefaltsKeys.DATE_OF_LOGIN)
                           self.performSegueAfterPasswordCheck(tag: tag)
                        }else if httpResponse.statusCode == 400 {
                            self.showAlertForPasswordCheck(email:  App.userEmail, tag: self.selectedTag, failure: "Please enter valid password")
                           // self.didShowAlert(title: "Access Denied", message: "Please enter valid password")
                        }
                    }
                }
            }
        }.resume()
    }
    
    func performSegueAfterPasswordCheck(tag:Int) {
        switch tag {
        case 2 :
            //  self.instantiate_to_view(withIdentifier: StoryBoardIds.ID_HEALTH_PROFILE_NAV)
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_APPOINTMENT_SEGUE, sender: self)
        case 3 :
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_APPOINTMENT_SEGUE, sender: self)
        case 5 :
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_PROFILE_SEGUE, sender: self)
        case 6 :
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_FAMILY_MEMBERS, sender: self)
            
        default :
            print("Different tag cant perform action")
        }
    }
    
    
    ///Checks the password field is empty and disable button in alert
    @objc func passwordEntered(_ sender: UITextField) {
        var resp : UIResponder! = sender
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        if sender.text == "" {
            alert.actions[0].isEnabled = false
        }else
        {
            alert.actions[0].isEnabled = true
        }
    }
    
    ///Method performs request to the server for chat connection with doctor
    @objc func connectToChatSession() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        startAnimating()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let chatSession = AppURLS.URL_Chat_connect
        print("Chat url:=> \(chatSession)")
        
        let url = URL(string: chatSession)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    //self.didShowAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                //if you response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    
                    if !errorStatus
                    {
                        // UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
                        print("performing error handling chat session connect::\(resultJSON)")
                        if httpResponse.statusCode == 402
                        {
                            if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                if let allKeys = data.allKeys as? [String] {
                                    if allKeys.contains(Keys.KEY_SUBSCRIPTION_ERROR) {
                                        if let message = data.object(forKey: Keys.KEY_SUBSCRIPTION_ERROR) as? String {
                                            DispatchQueue.main.async(execute: {
                                                if App.didUserSubscribed && App.userSubscriptionType.lowercased() == PlanType.B2BPAID {
                                                    self.didShowAlert(title: "" , message: message)
                                                }else {
                                                    let alert = UIAlertController(title: "Membership", message: message, preferredStyle: UIAlertController.Style.alert)
                                                    let viewPlans = UIAlertAction(title: "Choose", style: .default, handler: { (action) in
                                                        App.isFromView = FromView.HomeView
                                                        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
                                                    })
                                                    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                                    alert.addAction(cancel)
                                                    alert.addAction(viewPlans)
                                                    self.present(alert, animated: true, completion: nil)
                                                }
                                            })
                                        }
                                    }
                                }
                            }
                        }else {
                            if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                    DispatchQueue.main.async(execute: {
                                        //if httpResponse.statusCode == 412 {
                                        self.stopAnimating()
                                        self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.HomeView)
                                     //   self.showVerifyMobileMessage(title: <#String#>, message: mobile, fromView: FromView.HomeView)
                                        // }
                                    })
                                }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                       // self.didShowAlert(title: "", message: email)
                                        self.showVerifyMobileMessage(title: "Email Verification", message: email, segue: StoryboardSegueIDS.ID_VERIFY_EMAIL, tag: 2, fromView: FromView.HomeView)
                                    })
                                }
                            }
                        }
                    }
                    else {
                        print("Chat session response::\(resultJSON)")
                        
                        if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                            if let id = data.object(forKey: Keys.KEY_ID) as? Int {
                                App.threadID = "\(id)"
                                if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int {
                                    App.user_id = "\(userid)"
                                    print("Thread id==> \(App.threadID) userid:\(App.user_id)")
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        //self.instantiate_to_view(withIdentifier: StoryBoardIds.ID_CHAT_NAV_VIEW)
                                        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_START_CHAT, sender: self)
                                    })
                                }
                            }
                        }
                    }
                }catch let error{
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON chat session connect: \(error.localizedDescription)")
                }
            }
        }).resume()
    }
    
    ///Shows the alert for confrimation to connect to chat
    func showConnectChatAlert() {
//        if App.isMobileVerified {
            let alert = UIAlertController(title: "Chat", message: "Do you want to start session?", preferredStyle: UIAlertController.Style.alert)
            
            let connect = UIAlertAction(title: "Start now", style: .default) { (UIAlertAction) in
                self.connectToChatSession()
            }
            
            let cancel = UIAlertAction(title: "No thanks", style: .cancel, handler: nil)
            
            alert.addAction(connect)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
            
//        }else {
//            print("**Not verified")
//            App.isFromView = FromView.ChatHistoryView
//            showVerifyMobileMessage(message: "", fromView: FromView.HomeView)
//        }
    }
    
    ///shows the UIAlertController for password verification
    func showAlertForPasswordCheck(email:String,tag:Int,failure:String) {
        var msg = email.isEmpty ? "" : "Email : \(email)"
        if !failure.isEmpty {
            msg =  email.isEmpty ? "Please enter valid password" : "Email : \(email)\n" + "Please enter valid password"
        }
        
        let alert = UIAlertController(title: "Password Confirmation", message: msg, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Enter Password"
            textField.isSecureTextEntry = true
            textField.textAlignment = .center
            textField.addTarget(self, action: #selector(self.passwordEntered(_:)), for: .editingChanged)
        })
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            
            let textBody = (textField?.text!)!
            let passData : [String:String] = [Keys.KEY_PASSWORD : textBody]
            print("passworddata to pass:\(passData)")
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: passData, options: .prettyPrinted)
                self.checkPassword(data: jsonData, tag: tag)
            }catch let err{
                print("Conversion error:\(err.localizedDescription)")
            }
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(cancel)
        alert.actions[0].isEnabled = false
        
        DispatchQueue.main.async {
            if !App.checkIfUserLogginTime() {
                self.present(alert, animated: true, completion: nil)
            }else {
                self.performSegueAfterPasswordCheck(tag: tag)
            }
        }
    }
    
    ///Performs logout operation of user
//    func logoutTapped() {
//        let alert = UIAlertController(title: "Are you sure?", message: "you want to logout", preferredStyle: UIAlertController.Style.alert)
//        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
//
//            UserDefaults.standard.set(App.apvoip_token, forKey: UserDefaltsKeys.PREVIOUS_APVOIP_TOKEN)
//            UserSession.sharedInstace.makePresentAccessTokenToPrevious()
//            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.DID_USER_LOGED_OUT)
//            UserSession.sharedInstace.logourUser(accessToken: App.getUserAccessToken(), apVoipToken: App.apvoip_token)
//
//            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_NAME)
//            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_HAS_LOGIN_KEY)
//            UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_HEALTH_STATUS)
//            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.TOKENTYPE)
//            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.ACCESS_TOKEN)
//            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
//            UserDefaults.standard.set(false,forKey: UserDefaltsKeys.KEY_USER_STATUS)
//            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.MOBILE_NUMBER)
//            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_LOGGED_USER_EMAIL)
//
//            User.remove_user_profile_default()
//            AppSettings.removeEncodedAppSettingsDataFromUserDefaults()
//
//            //UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_PHONE)
//            //App.isMobileVerified = false
//            App.appSettings = nil
//            App.imagesURLString.removeAll()
//            App.ImagesArray.removeAll()
//            App.bookingAttachedImages.removeAll()
//            App.subscription_details = nil
//            App.pending_subscription = nil
//            App.appointmentsList.removeAll()
//            App.lang_preferences_values.removeAll()
//            App.lang_preferences_values.append("English")
//            RefreshTimer.sharedInstance.stopGlobalTimer()
//
//            App.user_full_name = ""
//            App.userEmail = ""
//            App.avatarUrl = ""
//
//            App.b2bUserType = ""
//            App.didUserSubscribed = false
//            App.userSubscriptionType = ""
//            App.subscription_details = nil
//            App.canUpgradeSubscription = false
//            App.notificationReadCount = 0
//            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
//            UserDefaults.standard.set(App.notificationReadCount, forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT)
//            GIDSignIn.sharedInstance().signOut()
//         //   UNUserNotificationCenter.current().remo
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyBoard.instantiateViewController(withIdentifier: StoryBoardIds.ID_LOGIN_VIEW) as! LoginViewController
//            self.present(vc, animated: true, completion: nil)
//        })
//        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
//        alert.addAction(yesAction)
//        alert.addAction(cancel)
//        self.present(alert, animated: true, completion: nil)
//    }
    
    ///Subscription upgrade tapped
    @IBAction func upgradeNowTapped(_ sender: UIButton) {
        App.isFromView = FromView.SubscriptionPlans
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
    }
    
    ///Displays alert to enter password when Dashboard menu tapped
    @IBAction func menuOptionsTapped(_ sender: UIButton) {
    
        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_SET_PASSWORD){
            App.didAskForSetPassword = true
            self.check_password_show_alert()
            return
        }
        
        if sender.tag == 1 {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BOOK_CONSULTATION_SEGUE, sender: sender)
            return
        }

        self.userDataModel  = User.get_user_profile_default()
        var email = ""
        if self.userDataModel == nil {
            email = App.userEmail
        }else {
            if self.userDataModel.email != nil {
                email = self.userDataModel.email!
            }else if self.userDataModel.phone != nil {
                  email = self.userDataModel.email!
            }
            
            showAlertForPasswordCheck(email: email, tag: sender.tag, failure: "")
            selectedTag = sender.tag
            App.userEmail = email
        }
    }
    
    
    ///Side menu item which performs action on menu item selected
    func menuItemTapped(at row:Int) {
        switch  row {
        case 0 :
            print("My orders tapped")
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_ORDERS, sender: self)
        case 1 :
             print("Family members tapped")
           // self.performSegue(withIdentifier: StoryboardSegueIDS.ID_FAMILY_MEMBERS, sender: self)
             
             if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_SET_PASSWORD){
                App.didAskForSetPassword = true
                self.check_password_show_alert()
                return
             }
             
             self.userDataModel  = User.get_user_profile_default()
             var email = ""
             if self.userDataModel == nil {
                email = App.userEmail
             }else {
                if self.userDataModel.email != nil {
                    email = self.userDataModel.email!
                }
                
                showAlertForPasswordCheck(email: email, tag: 6, failure: "")
                selectedTag = 6
                App.userEmail = email
            }
            
        case 2 :
            print("MyBillings tapped")
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MYBILLINGS, sender: self)
            
        case 3 :
            print("Subscription plans tapped")
            App.isFromView = FromView.SubscriptionPlans
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
           
//        case 4 :
//            print("Diagnostics")
//            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_SEGUE, sender: self)
            
        case 4 :
            print("Logout tapped")
//            self.logoutTapped()
            
        default:
            print("Nothing to perform")
        }
    }
    
    
    /**
      Unwind segue action
     */
    @IBAction func unwindToHome(segue:UIStoryboardSegue) {
        
    }
    
    /**
        side menu action *Not used*
     */
    @IBAction func menuTapped(_ sender: UIBarButtonItem , event: UIEvent) {
      //  toggleSideMenuView()
//        let configuration = FTConfiguration.shared
//        configuration.textAlignment = .center
//        configuration.textColor = UIColor.white
//        configuration.menuSeparatorColor = UIColor.lightGray
//        configuration.menuWidth = 155
//        configuration.menuRowHeight = 40
//
//        FTPopOverMenu.showForEvent(event: event, with: ["My Orders","Family Members","Billing History","Subscription Plans","Logout"], done: { (selectedRow) in
//            print("Seelected row:\(selectedRow)")
//            self.menuItemTapped(at: selectedRow)
//        }) {
//            print("cancelled")
//        }
    }
    
    /**
       performs Logout user action
     */
    @IBAction func logoutTapped(_ sender: UIBarButtonItem) {

//       logoutTapped()
    }
      
    
    /**
      performs segue action to chat history view
     */
    @IBAction func chatWithDoctorTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CHAT_HISTORY, sender: self)
    }
    
    
    /**
     performs segue action to chat Health profile view
     */
    @IBAction func healthProfileTapped(_ sender: UIButton) {
        
//        let storyBaord = UIStoryboard(name: "Main", bundle: nil)
//        let addNotesVC = storyBaord.instantiateViewController(withIdentifier: StoryBoardIds.ID_PASSWORD_CHECK)
//        App.popUP = BIZPopupViewController(contentViewController: addNotesVC, contentSize: CGSize(width: 280, height: 280))
//        self.present(App.popUP!, animated: true, completion: nil)
        
         self.instantiate_to_view(withIdentifier: StoryBoardIds.ID_HEALTH_PROFILE_NAV)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"HealthProfile"), object: nil)
    }
    
    
    // MARK: - Navigation

//     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_APPOINTMENT_CHECK {
            let destvc = segue.destination as! AppointmentSummaryViewController
            destvc.appointmetnID = self.timerAppointmentId
        }else if segue.identifier == StoryboardSegueIDS.ID_ENTER_PINCODE {
            if let destVC = segue.destination as? UIOrederProcessViewController {
                destVC.appointment_id = self.medicineReadyAppointmentID
            }
        }else  if segue.identifier == StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS {
            if let destVC = segue.destination as? SubscriptionPlansViewController {
                destVC.previousActionDelegate = self
            }
        }
//        else if segue.identifier ==  StoryboardSegueIDS.ID_START_CHAT {
//            if let destVC = segue.destination as? ChatViewController {
//                destVC.delegate = self
//            }
//        }
        else if segue.identifier == StoryboardSegueIDS.ID_VERIFY_EMAIL {
            let destVC = segue.destination as! EmailVerificationViewController
            destVC.delegate = self
        }else if segue.identifier == StoryboardSegueIDS.ID_FAMILY_MEMBERS {
            if let destVC = segue.destination as? FamilyViewController {
                destVC.familyGuardianName = self.familyGuardianName
                destVC.isFromHomeViewToConsentForm = self.isFromHomeViewToConsentForm
            }
        }
    }
}

extension HomeViewController : EmailVerificationDelegate {
    func didVerifyEmail(_ email: String, _ success: Bool) {
        if success {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//extension HomeViewController : ChatViewControllerDelegate {
//    func didEndChat() {
//        self.navigationController?.popViewController(animated: true)
//    }
//}

extension HomeViewController : ContinueUserPreviousActionsDelegate {
    func doUserPreviousActionIfFinishedSubscription() {
        self.navigationController?.popViewController(animated: true)
        self.connectToChatSession()
    }
}

extension HomeViewController : RefreshTimerDelegate {
    func refreshTheStatusForAppointmentTileIfAppointmentIsInFiveMin() {
        DispatchQueue.main.async {
            
            for (index,appointment) in App.tempAppointments.enumerated() {
                if let scheduleDate = appointment.scheduled_at {
                    let dateOFAppointment = self.stringToDateConverter(date: scheduleDate)
                    let currentTime = Date()
                    let totalSeconds = dateOFAppointment.timeIntervalSince(currentTime)
                    print("Total seconds to remove from tile:\(totalSeconds)")
                    if totalSeconds <= 0 {
                        App.tempAppointments.remove(at: index)
                    }
                }
            }
            
            print("Dashbaord appointment refreshed")
            self.dashboardItems = DashboardTiles.addDashboardContet()
            self.collectionView.reloadData()
        }
    }
}

extension HomeViewController : MenuNavgationDelegate {
    
    func billingsSelected() {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MYBILLINGS, sender: self)
    }
    
    func subscriptionPlansSelected() {
        App.isFromView = FromView.SubscriptionPlans
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
    }
    
    func dashboardSelected() {
        
    }
    
    func appointmentsSelected() {
        self.userDataModel  = User.get_user_profile_default()
        var email = ""
        if self.userDataModel == nil {
            email = App.userEmail
        }else {
            if self.userDataModel.email != nil {
                email = self.userDataModel.email!
            }
            
            showAlertForPasswordCheck(email: email, tag: 2, failure: "")
            selectedTag = 2
            App.userEmail = email
        }
    }
    
    func myRecordsSelected() {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_DOCS, sender: self)
    }
    
    func ordersSelected() {
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_ORDERS, sender: self)
    }
    
    func vitalsSelected() {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_VITALS , sender: self)
    }
    
    func familySelected() {
        self.userDataModel  = User.get_user_profile_default()
        var email = ""
        if self.userDataModel == nil {
            email = App.userEmail
        }else {
            if self.userDataModel.email != nil {
                email = self.userDataModel.email!
            }
            
            showAlertForPasswordCheck(email: email, tag: 6, failure: "")
            selectedTag = 6
            App.userEmail = email
        }
    }
    
    func checkForPassword() {
        DispatchQueue.main.async {
          self.check_password_show_alert()
        }
    }
    
    @objc func blogsSelected() {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BLOGS_VIEW, sender: self)
    }
    
    func settingsSelected() { 
          self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SETTINGS_VIEW, sender: self)
    }
    
    @objc func diagnosisHistorySelected(){
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_HISTORY_SEGUE, sender: self)
    }
    
    func questionsSelected() {
       
      self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CHAT_HISTORY, sender: self)
    }
    
    func profileSelected() {
        self.userDataModel  = User.get_user_profile_default()
        var email = ""
        if self.userDataModel == nil {
            email = App.userEmail
        }else {
            if self.userDataModel.email != nil {
                email = self.userDataModel.email!
            }
            
            showAlertForPasswordCheck(email: email, tag: 5, failure: "")
            selectedTag = 5
            App.userEmail = email
        }
    }
    
    func showSettingsAlert(){
        if self.userDataModel.email!.isEmpty || self.userDataModel.phone!.isEmpty
        {
            let alert = UIAlertController(title: "Profile", message: "Looks like you haven't updated your e-mail address and mobile number. Please update to continue.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SETTINGS_VIEW, sender: self)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_WELLNESS_PROGRAM, sender: self)
        }
    }
    
    func showProfileAlert(forVC:String){
        if self.userDataModel.gender == "" || self.userDataModel.dob == ""
        {
            let alert = UIAlertController(title: "Profile", message: "Looks like you haven't updated your profile with complete details. Please update DOB and Gender.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.profileSelected()
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.performSegue(withIdentifier: forVC, sender: self)
        }
    }
}

extension HomeViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("count dash \(self.dashboardItems.count)")
        return self.dashboardItems.count
    }
    
     func collectionView(_    collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home", for: indexPath) as! UIHomeCollectionViewCell
      //  self.disableCellsForUnRegisteredUser(cell: cell, indexPath: indexPath)
        
        let dashbaord = self.dashboardItems[indexPath.row]
        print("tile tile \(dashbaord.tileTitle)")
        cell.lb_title.text = dashbaord.tileTitle
        cell.tv_info.text = dashbaord.tileContent
        cell.iv_image.image = dashbaord.tileIcon
        
    
        
        if let appTime = dashbaord.appointmentTime ,let docName = dashbaord.doctorName , let docSpec = dashbaord.doctorSpecialization {
            
            if appTime.isEmpty && docName.isEmpty && docSpec.isEmpty {
                cell.tv_info.isHidden = false
                cell.iv_appIcon.isHidden = true
                cell.lb_appTime.isHidden = true
                cell.lb_doctorName.isHidden = true
                cell.lb_doctorSpecialisation.isHidden = true
            }else {
                cell.iv_appIcon.isHidden = false
                cell.lb_appTime.isHidden = false
                cell.lb_doctorName.isHidden = false
                cell.lb_doctorSpecialisation.isHidden = false
                cell.tv_info.isHidden = true
                
                cell.lb_appTime.text = appTime
                cell.lb_doctorName.text = docName
                cell.lb_doctorSpecialisation.text = docSpec
            }
        }else {
            cell.tv_info.isHidden = false
            cell.iv_appIcon.isHidden = true
            cell.lb_appTime.isHidden = true
            cell.lb_doctorName.isHidden = true
            cell.lb_doctorSpecialisation.isHidden = true
        }
        
        return cell
    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Home screen index path tapped:\(indexPath.row)")
        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_SET_PASSWORD){
            App.didAskForSetPassword = true
            self.check_password_show_alert()
            return
        }
        
        self.userDataModel  = User.get_user_profile_default()
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BOOK_CONSULTATION_SEGUE, sender: self)
            return
        }else if indexPath.row == 2 {
            if !self.medicineReadyAppointmentID.isEmpty {
               self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ENTER_PINCODE, sender: self)
            }else  {
                self.didShowAlert(title:"",message:"No medicines to order")
            }
            return
        }else if indexPath.row == 3 {
            if App.userSubscriptionType.lowercased().isEqual(PlanType.ONE_TIME) || App.userSubscriptionType.lowercased().isEqual(PlanType.B2BPAID) {
                self.didShowAlert(title: "", message: "You cannot use chat feature in current plan")
            }else {
                self.showConnectChatAlert()
            }
          //  self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CHAT_HISTORY, sender: self)
            return
        }else if indexPath.row == 4 {
            //self.didShowAlert(title:"",message:"Coming Soon")
            self.showProfileAlert(forVC: StoryboardSegueIDS.ID_DIAGNOSTICS_SEGUE)
            return
        }else if indexPath.row == 5 {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_DOCS, sender: self)
            return
        }else if indexPath.row == 6 {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_REMINDER_SEGUE, sender: self)
            return
        }else if App.user_type == "b2b" || App.user_type == "b2bpaid"{
            
            if indexPath.row == 7{
                if App.fitmeinType{
                    self.showSettingsAlert()
                    return
                }else{
                    self.didShowAlert(title: "Alert!!", message: "Please consult your company to enable this feature")
                }
            }
            
            if indexPath.row == 8{
                if App.hraType{
                    self.showProfileAlert(forVC: StoryboardSegueIDS.ID_HRA_SEGUE)
                    return
                }else{
                    self.didShowAlert(title: "Alert!!", message: "Please consult your company to enable this feature")
                }
            }
            
            if indexPath.row == 9 {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ONS, sender: self)
                return
            }
 
            /*
            if !App.fitmeinType && !App.hraType{
                if indexPath.row == 7 {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ONS, sender: self)
                    return
                }
            }else if App.fitmeinType && App.hraType{
                if indexPath.row == 7 {
                    self.showSettingsAlert()
                    return
                }else if indexPath.row == 8 {
                    self.showProfileAlert(forVC: StoryboardSegueIDS.ID_HRA_SEGUE)
                    return
                }else if indexPath.row == 9 {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ONS, sender: self)
                    return
                }
            }else if !App.fitmeinType && App.hraType{
                if indexPath.row == 7 {
                    self.showProfileAlert(forVC: StoryboardSegueIDS.ID_HRA_SEGUE)
                    return
                }else if indexPath.row == 8 {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ONS, sender: self)
                    return
                }
            }else if App.fitmeinType && !App.hraType{
                if indexPath.row == 7 {
                    self.showSettingsAlert()
                    return
                }else if indexPath.row == 8 {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ONS, sender: self)
                    return
                }
            }*/
        }else if App.user_type.isEmpty || App.user_type == "b2c"{
            if indexPath.row == 7 {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_ONS, sender: self)
                return
            }
        }
        
        
        var email = ""
        if self.userDataModel == nil {
            email = App.userEmail
        }else {
            if self.userDataModel.email != nil {
                email = self.userDataModel.email!
            }
            
            showAlertForPasswordCheck(email: email, tag: indexPath.row + 1, failure: "")
            selectedTag = indexPath.row + 1
            App.userEmail = email
        }
    }
    
    
    
    
}

extension HomeViewController : UICollectionViewDelegateFlowLayout {
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        let padding: CGFloat = 10
        let collectionCellSize = width  - padding
        let viewHeight = self.view.frame.size.height
        return CGSize(width: collectionCellSize / 2, height: viewHeight > 568 ? 130 : 155 )
    }
}

extension UIViewController {
    
    /**
      Method checks whether mobile is verified or not by performing request.
      * can be accessed in any class
     */
    func check_mobile_verifield() {
            let url = URL(string: AppURLS.URL_ChanegMobileNumber)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        print("Error==> : \(error.localizedDescription)")
                        // self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    print("httpresponse:\(response as! HTTPURLResponse)")
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("perform error handling splash verify number:\(resultJSON)")
                            DispatchQueue.main.async(execute: {
                                //  self.instantiate_to_view(withIdentifier: StoryBoardIds.ID_LOGIN_VIEW)
                            })
                        }
                        else {
                            print("Mobile view response:\(resultJSON)")
                            
                            if httpResponse.statusCode == 200 {
                                if let dataDic = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    let mobileVerified = dataDic.object(forKey: Keys.KEY_MOBILE_VERIFIED) as! Bool
                                    if let mobileNumber = dataDic.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                        print("Mobile num is string")
                                        UserDefaults.standard.set(mobileNumber, forKey: UserDefaltsKeys.MOBILE_NUMBER)
                                    }else {
                                        App.isSocialSignup = true
                                    }
                                    UserDefaults.standard.set(mobileVerified, forKey: UserDefaltsKeys.IS_MOBILE_VERIFIED)
                                    App.isMobileVerified = mobileVerified
                                }
                            }
                        }
                    }catch let error{
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
            }).resume()
    }

    
    /**
      Methods performs request to get countries list and can be accessed in every class.
       * can be accessed in any class
     */
    func getCountriesList() {
          print("==>5")
       if App.countriesList.count == 0 {
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
              //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
   
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: AppURLS.URL_Countries)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            print("CountryURL==>\(url!)")
            session.dataTask(with: request) { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                        print("Error while fetching data\(String(describing: error?.localizedDescription))")
                        self.didShowAlert(title: "Error", message: "\(error!.localizedDescription)")
                      //  AlertView.sharedInsance.showFailureAlert(title: "", message: "\(error!.localizedDescription)")
                    })
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                 print("performing error handling countires list:\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let status = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(status) code:\(code) jsondata=\(jsonData)")
                                if code == 200 {
                                    if let data = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                        
                                        UserDefaults.standard.set(data, forKey: "ListCountry")
                                        let keys = data.allKeys
                                        let values = data.allValues
                                        
                                        UserDefaults.standard.set(data, forKey: "CountriesDefaults")
                                        
                                        for (key,value) in zip(keys,values) {
                                            let id = key as! String
                                            let name = value
                                            let countryDetails = Country(id: Int(id)!, name: name as! String)
                                            App.countriesList.append(countryDetails)
                                        }
                                        
                                        do {
                                            let encoder = JSONEncoder()
                                            let countryList = try encoder.encode(App.countriesList)
                                            UserDefaults.standard.set(countryList, forKey: UserDefaltsKeys.KEY_COUNTRIES_LIST)
                                        }catch let err {
                                            print("Error while decoding countries list:\(err)")
                                        }
                                        
//                                        let encodedCountriesData = NSKeyedArchiver.archivedData(withRootObject:  App.countriesList)
//                                        UserDefaults.standard.set(encodedCountriesData, forKey: UserDefaltsKeys.KEY_COUNTRIES_LIST)
                                        
                                    }
                                }else {
                                    print("Error :Coudn't fetch countrys")
                                }
                            }
                            
                            // sorting fetched countires in alphabetical manner
                            self.sortCountryArray()
                            print("Countries count:=\(App.countriesList.count)")
                          }
                        }
                        catch let error
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON:==> \(error.localizedDescription)")
                        }
                    }
                }
                }.resume()
        }else {
            print("Countries already stored")
            //App.get_countries_list()
            print("CountriesCount:=>\(App.countriesList.count)")
        }
    }
    
    
    /**
       Method sorts countries in alphabetical manner called in  `getCountrieList()`
       * can be accessed in any class
     */
    func sortCountryArray() {
        let sortedList = App.countriesList.sorted(by: { $0.id! < $1.id! })
        App.countriesList = sortedList
    }

    
   
    /**
      method clears notification stored in core data
       * can be accessed in any class
     */
//    func removeItemsFromNotifications() {
//        DispatchQueue.main.async {
//            let coreDatacontext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext  //as!
//            do {
//                let result : [Notifications] = try coreDatacontext!.fetch(Notifications.fetchRequest()) //!
//
//                if result.count != 0
//                {
//                    for object in result
//                    {
//                        App.coreDatacontext.delete(object)
//                    }
//
//                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                    print("Items deleted from coredata successfully")
//                }
//
//            } catch {
//                print("Fetching Failed")
//            }
//        }
//    }
    
   
    /**
      Method returns notification count to show on badge by checking notification is read or not
      * can be accessed in any class
    */
//    func getNotificationsReadCount() -> Int{
//        App.notificationReadCount = 0
//        for notification in App.notifications {
//            let readAt = notification.read_at!
//            if readAt.isEmpty == true {
//                App.notificationReadCount += 1
//                //  print("unread count:\(App.notificationReadCount) total:\(App.notifications.count)")
//            }else {
//                print("Notification :\(notification.id!) already read")
//            }
//        }
//        UserDefaults.standard.set(App.notificationReadCount, forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT)
//        return App.notificationReadCount
//    }

    
   
    /**
       Method perform request to get user info and stores
       * can be accessed in any class
     */
    func getUserProfile() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            //  startAnimating()
            let url = URL(string: AppURLS.URL_UserProfile)
          //  print("Profile URl==>\(url)")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "Sorry", message: "We couldn't get your profile right now. please try again later")
                       // AlertView.sharedInsance.showFailureAlert(title: "Sorry!", message: "we couldn't get your profile right now. please try again later")
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling get user profile:\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("User profile Status=\(responseStatus) code:\(code) Response:==>\(jsonData)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                if code == 200 && responseStatus == "success"
                                {
                                    var prefix = ""
                                    var mrnNumber = ""
                                    var first_name = ""
                                    var middle_name = ""
                                    var last_name = ""
                                    var dob = ""
                                    var gender = ""
                                    var address1 = ""
                                    var address2 = ""
                                    var uCountryId = 0
                                    var state = ""
                                    var city = ""
                                    var pincode = ""
                                    var mobile = ""
                                    var alternateContact = ""
                                    var user_id :Int!
                                    var id :Int!
                                    var email = ""
                                    
                                    if let emailID = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_USER_NAME) {
                                        email = emailID as! String
                                    }
                                    
                                    if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int
                                    {
                                        user_id = userid
                                      //  UserDefaults.standard.set(user_id, forKey: <#T##String#>)
                                    }
                                    if let uid = data.object(forKey: Keys.KEY_ID) as? Int
                                    {
                                        id = uid
                                    }
                                    if let mrn = data.object(forKey: Keys.KEY_MRN_NO) as? String
                                    {
                                        mrnNumber = mrn
                                    }
                                    if let prefixType = data.object(forKey: Keys.KEY_PREFIX) as? String
                                    {
                                        prefix = prefixType
                                    }
                                    if let fName = data.object(forKey: Keys.KEY_FIRST_NAME) as? String
                                    {
                                        first_name = fName
                                    }
                                    if let lname = data.object(forKey: Keys.KEY_LAST_NAME) as? String
                                    {
                                        last_name = lname
                                    }
                                    if let mname = data.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                    {
                                        middle_name = mname
                                    }
                                    if let dateob = data.object(forKey: Keys.KEY_DATE_OF_BIRTH) as? String
                                    {
                                        dob = dateob
                                    }
                                    if let ugender = data.object(forKey: Keys.KEY_GENDER) as? String
                                    {
                                        gender = ugender
                                    }
                                    if let add1 = data.object(forKey: Keys.KEY_ADDRESS_1) as? String
                                    {
                                        address1 = add1
                                    }
                                    if let add2 = data.object(forKey: Keys.KEY_ADDRESS_2) as? String
                                    {
                                        address2 = add2
                                    }
                                    if let cID = data.object(forKey: Keys.KEY_COUNTRY_ID) as? Int
                                    {
                                        uCountryId = cID
                                    }
                                    if let ustate = data.object(forKey: Keys.KEY_STATE) as? String
                                    {
                                        state = ustate
                                    }
                                    if let ucity = data.object(forKey: Keys.KEY_CITY) as? String
                                    {
                                        city = ucity
                                    }
                                    if let upincode = data.object(forKey: Keys.KEY_PIN_CODE) as? String
                                    {
                                        pincode = upincode
                                    }
                                    
                                    if let emailID = data.object(forKey: Keys.KEY_EMAIL) as? String {
                                        email = emailID
                                    }else {
                                        email = User.get_user_profile_default().email!
                                    }
                                    
                                    if let umobile = data.object(forKey: Keys.KEY_MOBILE_NO) as? String
                                    {
                                        mobile = umobile
                                    }else {
                                        print("Server phone no is empty")
                                        if let mobileNum = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
                                             mobile = mobileNum
                                             print("stored phone is:\(mobileNum)")
                                        }
                                    }
                                    
                                    if let ualtMob = data.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                    {
                                        alternateContact = ualtMob
                                    }
                                   
                              //      App.user_full_name = first_name + " \(last_name)"
                                    
                                    let userDetails = User(avatar: App.avatarUrl, user_id: user_id, prefix: prefix, middle_name: middle_name, phone: mobile,alternate_phone:alternateContact, address1: address1, address2: address2, city: city, state: state, pincode: pincode, id: id, first_name: first_name, last_name: last_name, email: email, gender: gender, dob: dob, fullname: "", country: uCountryId)
                                    userDetails.mrnNumber = mrnNumber
                                    User.store_user_profile_default(dataModel: userDetails)
                                    
                                    DispatchQueue.main.async(execute: {
                                        //self.stopAnimating()
                                        print("##-Health profile updated-#")
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserProfileName"), object: nil)
                                    })
                                    
                                }else {
                                    print("Error while fetching data")
                                }
                            }
                        }
                        catch _
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON")
                        }
                    }
                }
                
            }).resume()
            
            // Bounce back to the main thread to update the UI
        }
    }
    
    /**
     Method used get user details
     */
   @objc func getUserState(completion: @escaping (_ success:Bool,_ newuser:Int) -> Void){
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "", message: AlertMessages.NETWORK_ERROR)
            //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: AppURLS.URL_User_state)
        
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            print("access token \(App.getUserAccessToken())")
            if error != nil
            {
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
                completion(false,0)
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        print("user state data \(jsonData)")
//                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)

//                        if !errorStatus
//                        {
//                            print("performing error handling user avatar:\(jsonData)")
//                             completion(false)
//                        }
//                        else {
                           
                        
                            let code = jsonData.object(forKey: Keys.KEY_CODE) as? Int
                            let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as? String
                         //   print("Status=\(responseStatus) code:\(code)avatar USER STATE=\(jsonData)")
                            if code ?? 0 == 200 && responseStatus ?? "" == "success"
                            {
                                if let data = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    print("user state : ",data)
                                    if let user = data.object(forKey: Keys.KEY_USER) as? NSDictionary {
                                        
                                        var mrn_no = ""
                                        var prefix = ""
                                        var first_name = ""
                                        var full_name = ""
                                        var avatar_url = ""
                                        var middle_name = ""
                                        var last_name = ""
                                        var dob = ""
                                        var gender = ""
                                        var address1 = ""
                                        var address2 = ""
                                        var uCountryId = 0
                                        var state = ""
                                        var city = ""
                                        var pincode = ""
                                        var mobile = ""
                                        var alternateContact = ""
                                        var user_id :Int!
                                        var id :Int!
                                        var email = ""
                                        var new_user = -1
                                        var lang_prefe = [Int]()
                                        var lang_prefe_values = [String]()
                                        
                                        if let uid = user.object(forKey: Keys.KEY_ID) as? Int
                                        {
                                            id = uid
                                        }
                                        
                                        if let mrn = user.object(forKey: Keys.KEY_MRN_NO) as? String
                                        {
                                            mrn_no = mrn
                                            print("MRN : \(mrn_no)")
                                        }
                                        
                                        if let userid = user.object(forKey: Keys.KEY_USER_ID) as? Int
                                        {
                                            user_id = userid
                                            UserDefaults.standard.set(user_id, forKey: UserDefaltsKeys.CURRENT_USER_ID)
                                        }else {
                                            user_id = id
                                            UserDefaults.standard.set(user_id, forKey: UserDefaltsKeys.CURRENT_USER_ID)
                                        }
                                        
                                        if let fullname = user.object(forKey: Keys.KEY_FULLNAME) as? String
                                        {
                                            full_name = fullname
                                            App.user_full_name = fullname
                                        }
                                        if let avatar = user.object(forKey: Keys.KEY_AVATAR_URL) as? String
                                        {
                                            avatar_url = avatar
                                            App.avatarUrl = avatar
                                        }
                                        
                                        //health profile
                                        self.getHealthProfileDetails(response: user)
                                        
                                        if let details = user.object(forKey: Keys.KEY_DETAILS) as? NSDictionary {
                                            if let prefixType = details.object(forKey: Keys.KEY_PREFIX) as? String
                                            {
                                                prefix = prefixType
                                            }
                                            if let fName = details.object(forKey: Keys.KEY_FIRST_NAME) as? String
                                            {
                                                first_name = fName
                                            }
                                            
                                            if let lname = details.object(forKey: Keys.KEY_LAST_NAME) as? String
                                            {
                                                last_name = lname
                                            }
                                            if let mname = details.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                            {
                                                middle_name = mname
                                            }
                                            if let dateob = details.object(forKey: Keys.KEY_DATE_OF_BIRTH) as? String
                                            {
                                                dob = dateob
                                            }
                                            if let ugender = details.object(forKey: Keys.KEY_GENDER) as? String
                                            {
                                                gender = ugender
                                            }
                                            
                                            if let add1 = details.object(forKey: Keys.KEY_ADDRESS_1) as? String
                                            {
                                                address1 = add1
                                            }
                                            if let add2 = details.object(forKey: Keys.KEY_ADDRESS_2) as? String
                                            {
                                                address2 = add2
                                            }
                                            if let cID = details.object(forKey: Keys.KEY_COUNTRY_ID) as? Int
                                            {
                                                uCountryId = cID
                                            }
                                            if let ustate = details.object(forKey: Keys.KEY_STATE) as? String
                                            {
                                                state = ustate
                                            }
                                            if let ucity = details.object(forKey: Keys.KEY_CITY) as? String
                                            {
                                                city = ucity
                                            }
                                            
                                            if let upincode = details.object(forKey: Keys.KEY_PIN_CODE) as? String
                                            {
                                                pincode = upincode
                                            }
                                            
                                            if let altContct = details.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                            {
                                                alternateContact = altContct
                                            }
                                            
                                            
                                            if let lngPreferences = details.object(forKey : Keys.KEY_LANGUAGE_PREFERENCES ) as? [Int] {
                                                lang_prefe = lngPreferences
                                                App.lang_preferences = lngPreferences
                                                print("Langauage Preferences:\(lngPreferences)")
                                            }
                                            
                                            if let lngPreferencesValues = details.object(forKey : Keys.KEY_LANGUAGE_PREFERENCES_VALUES ) as? [String] {
                                                lang_prefe_values = lngPreferencesValues
                                                print("Langauage Preferences values:\(lang_prefe_values)")
                                                App.lang_preferences_values = lngPreferencesValues
                                            }
                                        }
                                        
                                        if let emailID = user.object(forKey: Keys.KEY_EMAIL) as? String {
                                            email = emailID
                                            print("Email id =:\(emailID)")
                                            App.userEmail = email
                                        }else {
                                            email = ""  //User.get_user_profile_default().email!
                                            App.userEmail = email
                                        }
                                        
                                        if let umobile = user.object(forKey: Keys.KEY_MOBILE_NO) as? String
                                        {
                                            mobile = umobile
                                            UserDefaults.standard.set(umobile, forKey: UserDefaltsKeys.MOBILE_NUMBER)
                                            print("Mobile Number:=:\(mobile)")
                                        }else {
                                            print("Server phone no is empty")
                                            mobile = ""
//                                            if let mobileNum = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
//                                                mobile = mobileNum
//                                                print("stored phone is:\(mobileNum)")
//                                            }
                                        }
                                        
                                        if let ualtMob = user.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                        {
                                            alternateContact = ualtMob
                                        }
                                        
                                        if let mobileVerified = user.object(forKey: Keys.KEY_MOBILE_VERIFIED) as? Bool {
                                            App.isMobileVerified = mobileVerified
                                            UserDefaults.standard.set(mobileVerified, forKey: UserDefaltsKeys.IS_MOBILE_VERIFIED)
                                        }
                                        
                                        if let emailVerified = user.object(forKey: Keys.KEY_IS_VERIFIED) as? Bool {
                                            App.isEmailVerified = emailVerified
                                            UserDefaults.standard.set(emailVerified, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
                                        }
                                        
                                        let userDetails = User(avatar: avatar_url, user_id: user_id, prefix: prefix, middle_name: middle_name, phone: mobile,alternate_phone:alternateContact, address1: address1, address2: address2, city: city, state: state, pincode: pincode, id: id, first_name: first_name, last_name: last_name, email: email, gender: gender, dob: dob, fullname: full_name, country: uCountryId)
                                        userDetails.mrnNumber = mrn_no
                                        userDetails.language_preferences = lang_prefe
                                        userDetails.language_preferences_values = lang_prefe_values
                                        userDetails.isEmailVerified =  App.isEmailVerified
                                        userDetails.isMobileVerified =  App.isMobileVerified
                                       // print("User details saved:\(id) mrnsn:\(userDetails.mrnNumber)")
                                        App.userDetails = userDetails
                                        User.store_user_profile_default(dataModel: userDetails)
                                        
                                        if let newUser = user.object(forKey: Keys.KEY_NEW_USER) as? Int {
                                            new_user = newUser
                                            print("new_user =:\(newUser)")
                                            App.newUser = new_user
                                        }else {
                                            new_user = -1
                                            App.newUser = new_user
                                        }
                                        
                                        if(App.ChatType == MessageServer.PRODUCTION && App.newUser == 1)
                                        {
                                            let network : String = UserDefaults.standard.object(forKey: UserDefaltsKeys.KEY_NETWORK_TYPE) as! String
                                            print("network type:",network)
                                           
//                                            GATracker.shared().trackEvent(withCategory: GATRackerCategories.Registration.rawValue , action: GATRackerActions.RegistrationSuccessfull.rawValue , label: "ios", value: nil)
                                            
//                                            AppsFlyerTracker.shared().trackEvent(AFEventCompleteRegistration, withValues: [
//                                                AFEventParamRegistrationMethod: "Registration complete via \(network) in iOS"
//                                                ])
                                        }
                                        
                                        if let pregnancy = user.object(forKey: Keys.KEY_PREGNANCY_DETAILS) as? NSDictionary {
                                            App.pregnancyData = Pregnancy()
                                            
                                            var abortions = ""
                                            var conceptions = ""
                                            var complications = ""
                                            var expecting_mother = ""
                                            let id = pregnancy.object(forKey: Keys.KEY_ID) as! Int
                                            let user_id = pregnancy.object(forKey: Keys.KEY_USER_ID) as! Int
                                            let created_at = pregnancy.object(forKey: Keys.KEY_CREATED_AT) as! String
                                            let updated =  pregnancy.object(forKey: Keys.KEY_UPDATED_AT) as! String
                                            if let abor =  pregnancy.object(forKey: Keys.KEY_ABORTIONS) as? String{
                                                abortions = abor
                                            }
                                            else if let abor =  pregnancy.object(forKey: Keys.KEY_ABORTIONS) as? Int{
                                                abortions = String(abor)
                                            }
                                            //let expecting_mother =  pregnancy.object(forKey: Keys.KEY_EXPECTING_MOTHER) as! Int
                                            if let expecting =  pregnancy.object(forKey: Keys.KEY_EXPECTING_MOTHER) as? String{
                                                expecting_mother = expecting
                                            }
                                            else if let expecting =  pregnancy.object(forKey: Keys.KEY_EXPECTING_MOTHER) as? Int{
                                                expecting_mother = String(expecting)
                                            }
                                            
                                            if let concept =  pregnancy.object(forKey: Keys.KEY_CONCEPTIONS) as? String{
                                                conceptions = concept
                                            }
                                            else if let concept =  pregnancy.object(forKey: Keys.KEY_CONCEPTIONS) as? Int{
                                                conceptions = String(concept)
                                            }
                                            
                                            if let complic =  pregnancy.object(forKey: Keys.KEY_COMPLICATIONS) as? String{
                                                complications = complic
                                            }
                                            else if let complic =  pregnancy.object(forKey: Keys.KEY_COMPLICATIONS) as? Int{
                                                complications = String(complic)
                                            }
                                            
                                            App.pregnancyData = Pregnancy(id: id, userid: user_id, createdat: created_at, updatedat: updated, abortions: abortions, conceptions: conceptions, complications: complications, expecting_mother: expecting_mother)
                                            
                                            //print("pregnancyData=\(App.pregnancyData.expecting_mother!)")
                                            
                                            let jsonEncoder = JSONEncoder()
                                            do {
                                                let encodedData = try jsonEncoder.encode(App.pregnancyData)
                                                UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_PREGNANCY_DATA)
                                            }catch let err{
                                                print("Error while encoding pregnancy data :\(err)")
                                            }
                                        }else{
                                            App.pregnancyData = Pregnancy()
                                            let abortions = "0"
                                            let conceptions = "0"
                                            let complications = "0"
                                            let expecting_mother = "0"
                                            App.pregnancyData = Pregnancy(id: 0, userid: 0, createdat: "", updatedat: "", abortions: abortions, conceptions: conceptions, complications: complications, expecting_mother: expecting_mother)
                                        }
                                    }
                                    
                                    let user_type = (data.value(forKey: "subscription") as! NSDictionary)["user_type"]
                                    print("user type is \(user_type as! String)")
                                    App.user_type = user_type as! String
                                    
                                    let service = data.value(forKey: "services") as? NSDictionary
                                    if let serviceType = service, let fitmeinType = serviceType.value(forKey: "fitmein") as? Bool, fitmeinType{
                                        print("fitmein type \(fitmeinType)")
                                        App.fitmeinType = true
                                    }else{
                                        App.fitmeinType = false
                                    }
                                    //hra
                                    if let serviceType = service, let hraType = serviceType.value(forKey: "hra") as? Bool, hraType{
                                        print("hra type \(hraType)")
                                        App.hraType = true
                                    }else{
                                        App.hraType = false
                                    }
                                    
                                    
                                    
                                    if let mdictions = data.object(forKey: Keys.KEY_MASTER_DATA) as? NSDictionary {
                                        
                                        print("Master Data fetched :\(mdictions)")
                                        
                                        var medical_history_option = [OptionsData]()
                                        var life_style_option = [OptionsData]()
                                        var sleep_duration_option = [OptionsData]()
                                        var sleep_pattern_option = [OptionsData]()
                                        var exercise_option = [OptionsData]()
                                        var marital_option = [OptionsData]()
                                        var switch_in_medicine_option = [OptionsData]()
                                        App.masterData = MasterData()
                                        
                                        if let exercise_per_week = mdictions.object(forKey: Keys.KEY_EXERCISE_PER_WEEK) as? [NSDictionary]{
                                            for options in exercise_per_week {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_exercise = OptionsData(id: id, first_level_value: optional_name)
                                                exercise_option.append(temp_exercise)
                                            }
                                        }
                                        if let life_style_activity = mdictions.object(forKey: Keys.KEY_LIFE_STYLE) as? [NSDictionary]{
                                            for options in life_style_activity {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_life_style = OptionsData(id: id, first_level_value: optional_name)
                                                life_style_option.append(temp_life_style)
                                            }
                                        }
                                        if let marital_status = mdictions.object(forKey: Keys.KEY_MARITAL_STATUS) as? [NSDictionary]{
                                            for options in marital_status {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_marital = OptionsData(id: id, first_level_value: optional_name)
                                                marital_option.append(temp_marital)
                                            }
                                        }
                                        if let medical_history = mdictions.object(forKey: Keys.KEY_MEDICAL_HISTORY) as? [NSDictionary]{
                                            for options in medical_history {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_medical_history = OptionsData(id: id, first_level_value: optional_name)
                                                medical_history_option.append(temp_medical_history)
                                            }
                                        }
                                        if let sleep_duration =  mdictions.object(forKey: Keys.KEY_SLEEP_DURATION) as? [NSDictionary]{
                                            for options in sleep_duration {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_sleep_duration = OptionsData(id: id, first_level_value: optional_name)
                                                sleep_duration_option.append(temp_sleep_duration)
                                            }
                                        }
                                        if let sleep_pattern =  mdictions.object(forKey: Keys.KEY_SLEEP_PATTERN) as? [NSDictionary]{
                                            for options in sleep_pattern {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_sleep_pattern = OptionsData(id: id, first_level_value: optional_name)
                                                sleep_pattern_option.append(temp_sleep_pattern)
                                            }
                                        }
                                        if let switch_in_medicine =  mdictions.object(forKey: Keys.KEY_SWITCH_MEDICINE) as? [NSDictionary]{
                                            for options in switch_in_medicine {
                                                let id = options.object(forKey: Keys.KEY_ID) as! Int
                                                let optional_name = options.object(forKey: Keys.KEY_FIRST_LEVEL_VALUE) as! String
                                                let temp_switch_in_medicine = OptionsData(id: id, first_level_value: optional_name)
                                                switch_in_medicine_option.append(temp_switch_in_medicine)
                                            }
                                        }
                                        
                                        
                                        let master = MasterData(exercise_per_week: exercise_option, life_style_activity: life_style_option, marital_status: marital_option, medical_history: medical_history_option, sleep_duration: sleep_duration_option, sleep_pattern: sleep_pattern_option, switch_in_medicine: switch_in_medicine_option)
                                        App.masterData = master
                                        print("MasterData=\( App.masterData)")
                                        
                                        let jsonEncoder = JSONEncoder()
                                        do {
                                            let encodedData = try jsonEncoder.encode(App.masterData)
                                            UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MASTER_DATA)
                                        }catch let err{
                                            print("Error while encoding master data :\(err)")
                                        }
                                        
                                    }
                                    
                                    
                                    if Theme.companyType != CompanyType.betterplace{
                                    if let employer = data.object(forKey: Keys.KEY_WHITELABEL_EMPLOYER) as? NSDictionary {
                                        if let isWhiteLabelEnabled = employer.object(forKey: Keys.KEY_WHITELABEL_ENABLED) as? Bool, isWhiteLabelEnabled {
                                            print("inside whitelabeller")
                                            if let employer_info = employer.object(forKey: Keys.KEY_WHITELABEL_CONFIG) as? NSDictionary {
                                                if let color1 = employer_info.object(forKey: Keys.KEY_WHITELABEL_COLOR1) as? String{
                                                    if let color2 = employer_info.object(forKey: Keys.KEY_WHITELABEL_COLOR2) as? String{
                                                        StandardColorCodes.Tata_Gradient_Color1 = color1
                                                        StandardColorCodes.Tata_Gradient_Color2 = color2
                                                        
//                                                        Theme.defaultTheme()
                                                        print("its inside maga")
                                                    }
                                                }
                                                if let titleLogoo = employer_info.object(forKey: Keys.KEY_WHITELABEL_TITLELOGO) as? String{
                                                    print("inside logo url \(titleLogoo)")
                                                    App.titleLogo = titleLogoo
                                                    
                                                    print(App.titleLogo)
                                                }
                                            }
//                                            Theme.companyType = .tataMotors
                                            DispatchQueue.main.async(execute: {
                                            Theme.tatamotorsTheme()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TitleLogo"), object: nil)
                                            })
                                        }
                                        else{
                                            print("inside defaulter1")
                                            App.titleLogo  = ""
                                            DispatchQueue.main.async(execute: {
                                                Theme.defaultTheme()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TitleLogo"), object: nil)
                                            })
                                        }
                                    }else{
                                        print("inside defaulter2")
                                        App.titleLogo  = ""
                                        DispatchQueue.main.async(execute: {
                                            Theme.defaultTheme()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TitleLogo"), object: nil)
                                        })
                                    }
                                    }
                                    else{
                                        App.titleLogo  = ""
                                        DispatchQueue.main.async(execute: {
                                            Theme.betterplaceTheme()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TitleLogo"), object: nil)
                                        })
                                    }
                                    
                                    ///Subscription Details
                                    self.getSubscriptionDetails(data: data, completionHandler: {
                                        print("Completed")
                                        if App.newUser == 1{
                                            completion(true,1)
                                        }else{
                                         completion(true,0)
                                        }
                                    })
                                    
                                    ///cart count
                                    if let count = data.object(forKey: Keys.KEY_CART) as? Int {
                                        App.cartCount = count
                                        DispatchQueue.main.async(execute: {
                                            //updates cart list
                                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CartList"), object: nil)
                                        })
                                        
                                    }
                                }
                                
                                //App.avatarUrl = data.object(forKey: Keys.KEY_AVATAR_URL) as! String
                            }else {
                                 completion(false,0)
                                print("Error while fetching data")
                            }
                            //Main thread to update ui after fetching data
                            DispatchQueue.main.async(execute: {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserProfileName"), object: nil)
                              //  self.updateProfilePic()
                            })
//                        }
                       }
                    }
                    catch let error
                    {
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON \(error.localizedDescription)")
                         completion(false,0)
                    }
                }
            }
        }).resume()
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: String) -> UIImage? {
        print("Download Started")
        var img : UIImage?
        let urlPath = URL(string: url)
        getData(from: urlPath!) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
//                self.imageView.image = UIImage(data: data)
                Theme.titleImg = UIImage(data: data)
            }
            
        }
        return img
    }
    
    @objc func getCartCollection() {
        print("*****")
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_CART_COLLECTION
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    //self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("get cart = \(resultJSON)")
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                let arrTemp : NSArray =  data.object(forKey: "cart_data") as! NSArray
                                //let arrTemp1 : NSArray =  data.object(forKey: "general") as! NSArray
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    App.cartArray = arrTemp.mutableCopy() as! NSMutableArray
                                    App.cartCount = data.object(forKey: "cart_count") as! Int
                                    App.cartAmount = data.object(forKey: "cart_amount") as! Float
                                    
                                    print("*1*1*1*1")
                                })
                                
                            }
                            else
                            {
                                
                            }
                        }
                    }catch let error{
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    /**
     Method used to fetch health profile details from `getUserState()`
     Parameter response: Pass the response from server to parse
     */
    func getHealthProfileDetails(response:NSDictionary) {
        var weight : Double = 0
        var height : Double = 0
        var does_smoke = ""
        var medical_history = ""
        var life_style = 0
        var sleep_duration = 0
        var sleep_pattern = 0
        var exercise_per_week = 0
        var marital_status = 0
        var switch_in_medicine = 0
        var medical_history_new = 0
        var medical_insurance = 0
        var switch_in_medicine_reason = ""
        
        if let details = response.object(forKey: Keys.KEY_DETAILS) as? NSDictionary {
            if let wght = details.object(forKey: Keys.KEY_WEIGHT) as? Double {
                weight = wght
            }
            
            if let hght = details.object(forKey: Keys.KEY_HEIGHT) as? Double {
                height = hght
            }
            
            if let doesSmoke = details.object(forKey: Keys.KEY_DOES_SMOKE) as? Int {
                does_smoke = "No"
                if doesSmoke == 1 {
                    does_smoke = "Yes"
                }
            }
            
            if let mHistory = details.object(forKey: Keys.KEY_MEDICAL_HISTORY) as? String {
                medical_history = mHistory
            }
            
            if let switch_in_reason = details.object(forKey: Keys.KEY_SWITCH_MEDICINE_OTHERS) as? String {
                switch_in_medicine_reason = switch_in_reason
            }
            
            if let lS = details.object(forKey: Keys.KEY_LIFE_STYLE_ID) as? Int {
                life_style = lS
            }
            
            if let sleep_dur = details.object(forKey: Keys.KEY_SLEEP_DURATION_ID) as? Int {
                sleep_duration = sleep_dur
            }
            
            if let sleep_patt = details.object(forKey: Keys.KEY_SLEEP_PATTERN_ID) as? Int {
                sleep_pattern = sleep_patt
            }
            
            if let exercise = details.object(forKey: Keys.KEY_EXERCISE_PER_WEEK_ID) as? Int {
                exercise_per_week = exercise
            }
            
            if let marital = details.object(forKey: Keys.KEY_MARITAL_STATUS_ID) as? Int {
                marital_status = marital
            }
            
            if let switch_medicin = details.object(forKey: Keys.KEY_SWITCH_MEDICINE_ID) as? Int {
                switch_in_medicine = switch_medicin
            }
            
            if let med_new = details.object(forKey: Keys.KEY_MEDICAL_HISTORY_NEW_ID) as? Int {
                medical_history_new = med_new
            }
            
            if let med_insurance = details.object(forKey: Keys.KEY_MEDICAL_INSURANCE) as? Int {
                medical_insurance = 0
                if med_insurance == 1 {
                    medical_insurance = 1
                }
            }
            
        }
        
        
        if let allgys = response.object(forKey: Keys.KEY_ALLERGIES) as? [NSDictionary] {
            App.allergies.removeAll()
            for allergy in allgys {
                let id = allergy.object(forKey: Keys.KEY_ID) as! Int
                let user_id = allergy.object(forKey: Keys.KEY_USER_ID) as! Int
                let allergyName = allergy.object(forKey: Keys.KEY_NAME) as! String
                let created_at = allergy.object(forKey: Keys.KEY_CREATED_AT) as! String
                let updated =  allergy.object(forKey: Keys.KEY_UPDATED_AT) as! String
                
                let allergyDetails = Allergy(id: id, userid: user_id, name: allergyName, createdat: created_at, updatedat: updated)
                App.allergies.append(allergyDetails)
                print("Allergies=\( App.allergies)")
            }
            
            let jsonEncoder = JSONEncoder()
            do {
                let encodedData = try jsonEncoder.encode(App.allergies)
                UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_ALLERGIES)
            }catch let err{
                print("Error while encoding allergies to data:\(err)")
            }
            
//            let encodedData1 = NSKeyedArchiver.archivedData(withRootObject:  App.allergies)
//            UserDefaults.standard.set(encodedData1, forKey: UserDefaltsKeys.KEY_ALLERGIES)
        }
        
        if let allgys = response.object(forKey: Keys.KEY_DRUG_ALLERGIES) as? [NSDictionary] {
            App.drug_allergies.removeAll()
            for allergy in allgys {
                let id = allergy.object(forKey: Keys.KEY_ID) as! Int
                let user_id = allergy.object(forKey: Keys.KEY_USER_ID) as! Int
                let allergyName = allergy.object(forKey: Keys.KEY_NAME) as! String
                let created_at = allergy.object(forKey: Keys.KEY_CREATED_AT) as! String
                let updated =  allergy.object(forKey: Keys.KEY_UPDATED_AT) as! String
                
                let allergyDetails = Allergy(id: id, userid: user_id, name: allergyName, createdat: created_at, updatedat: updated)
                App.drug_allergies.append(allergyDetails)
                print("Drug Allergies=\( App.drug_allergies)")
            }
            
            let jsonEncoder = JSONEncoder()
            do {
                let encodedData = try jsonEncoder.encode(App.drug_allergies)
                UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_DRUG_ALLERGIES)
            }catch let err{
                print("Error while encoding allergies to data:\(err)")
            }
        }
        
        
        if let mdictions = response.object(forKey: Keys.KEY_MEDICATIONS) as? [NSDictionary] {
            
            print("Medications fetched :\(mdictions)")
            App.medications.removeAll()
            for medication in mdictions {
                let id = medication.object(forKey: Keys.KEY_ID) as! Int
                let user_id = medication.object(forKey: Keys.KEY_USER_ID) as! Int
                let medname = medication.object(forKey: Keys.KEY_NAME) as! String
                let created_at = medication.object(forKey: Keys.KEY_CREATED_AT) as! String
                let updated =  medication.object(forKey: Keys.KEY_UPDATED_AT) as! String
                
                var dateFrom = ""
                var dateTo = ""
                var intake = ""
                var notes = ""
                var no_of_days = 0
                var medication_status = 0
                
                if let datefrm = medication.object(forKey: Keys.KEY_FROM_DATE) as? String{
                    dateFrom = datefrm
                }
                if  let dateToStr = medication.object(forKey: Keys.KEY_TO_DATE) as? String {
                    dateTo = dateToStr
                }
                if  let intakeT = medication.object(forKey: Keys.KEY_INTAKE_TIME) as? String {
                    intake = intakeT
                }
                if  let notesStr = medication.object(forKey: Keys.KEY_NOTES) as? String {
                    notes = notesStr
                    print("mediNotes=\(notesStr)")
                }
                if  let no_days = medication.object(forKey: Keys.KEY_NO_OF_DAYS) as? Int {
                    no_of_days = no_days
                }
                
                if  let med_status = medication.object(forKey: Keys.KEY_MEDICATION_STATE) as? Int {
                    medication_status = med_status
                }
                
                print("1\(medname) = 2 \(dateFrom) 3 \(dateTo) 4 \(notes) 5 \(user_id)")
                let medicationDetails = Medication(id: id, userid: user_id, name: medname, createdat: created_at, updatedat: updated, dateFrom: dateFrom, toDate: dateTo, intake: intake, notes: notes,numberofdays:no_of_days,status:medication_status)
                App.medications.append(medicationDetails)
                print("Medications=\( App.medications.count)")
            }
            
            let jsonEncoder = JSONEncoder()
            do {
                let encodedData = try jsonEncoder.encode(App.medications)
                UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MEDICATIONS)
            }catch let err{
                print("Error while encoding medicaitons to data:\(err)")
            }
            
//            let encodedData = NSKeyedArchiver.archivedData(withRootObject:  App.medications)
//            UserDefaults.standard.set(encodedData, forKey: UserDefaltsKeys.KEY_MEDICATIONS)
        }
        
        print("Health profiles==>\(weight) height:\(height) medical : \(App.allergies) \(App.medications)")
        //let dataModel = HealthProfile(weight: weight , height: height, does_smoke: does_smoke, medical_history: medical_history, allergies: App.allergies, medications:  App.medications)
        let dataModel = HealthProfile(weight: weight, height: height, does_smoke: does_smoke, medical_history: medical_history, allergies: App.allergies, medications: App.medications, life_style: life_style,sleep_duration: sleep_duration,sleep_pattern: sleep_pattern,exercise_per_week:exercise_per_week,marital_status:marital_status,switch_in_medicine:switch_in_medicine,medical_history_new:medical_history_new,medical_insurance:medical_insurance,switch_in_medicine_reason:switch_in_medicine_reason)
        HealthProfile.store_health_profile_default(dataModel: dataModel)
    }
    
    
    /**
     Method used to subscription details from `getUserState()`
     Parameter response: Pass the response from server to parse
     */
    func getSubscriptionDetails(data:NSDictionary,completionHandler: @escaping (()->())) {
        if let subscription = data.object(forKey: Keys.KEY_SUBSCRIPTION) as? NSDictionary {
            
            if let subscribed = subscription.object(forKey: Keys.KEY_SUBSCRIBED) as? Bool
            {
                App.didUserSubscribed = subscribed
                
                if  let canUpgrade = subscription.object(forKey: Keys.KEY_CAN_UPGRADE) as? Bool {
                     App.canUpgradeSubscription = canUpgrade
                }else {
                     App.canUpgradeSubscription = false
                }
                
                if let userType = subscription.object(forKey: Keys.KEY_USER_TYPE) as? String {
                    App.b2bUserType = userType
                }else {
                    App.b2bUserType = ""
                }
 
//                if !App.didUserSubscribed {
//                    completionHandler()
//                }
                if let sub_type = subscription.object(forKey: Keys.KEY_TYPE) as? String {
                    print("123type:\(sub_type) subscribed:\(App.didUserSubscribed)")
                    App.userSubscriptionType = sub_type
                    
                    let onlySubInfo = SubscriptionPlan(isSubscribed: subscribed, subscribedType: sub_type, canUpgrade: App.canUpgradeSubscription, userType: App.b2bUserType)
                    onlySubInfo.saveSubscriptionDetails(key: UserDefaults.Keys.onlySubscriptionInfo)
                    
                    if let details = subscription.object(forKey: Keys.KEY_DETAILS) as? NSDictionary {
                        if subscribed && sub_type.lowercased() == "promo" {
                            if let endsat = details.object(forKey: Keys.KEY_ENDS_AT) as? String {
                                let endDate = self.stringToDateConverter(date: endsat)
                                let days = Calendar.current.dateComponents([.day], from: Date(), to: endDate).day
                                print("Days coupon code ends:\(days!)")
                                
                                DispatchQueue.main.async(execute: {
                                    App.promoCodeExpiration = "\(days!)"
                                    if days! == 0 {
                                       // self.didShowAlert(title: "", message: "Your promo code will expire today")
                                        // AlertView.sharedInsance.showInfo(title: "", message: "Your promo code will expire today")
                                        
                                    }else if days! == 1 {
                                      //  self.didShowAlert(title: "", message: "Your promo code will be expired in a day")
                                        // AlertView.sharedInsance.showInfo(title: "", message: "Your promo code will be expired in a day")
                                    }else {
                                       // self.didShowAlert(title: "", message: "Your promo code will be expired in \(days!) days")
                                        //AlertView.sharedInsance.showInfo(title: "", message: "Your promo code will be expired in \(days!) days")
                                    }
                                    completionHandler()
                                })
                            }
                        }else if subscribed && sub_type.lowercased() == "b2c" {
                            
                            let interval = details.object(forKey: Keys.KEY_INTERVAL) as? Int
                            let ends_at = details.object(forKey: Keys.KEY_ENDS_AT) as? String
                            let charge_at = details.object(forKey: Keys.KEY_CHARGE_AT) as? String
                            
                            let id = details.object(forKey: Keys.KEY_ID) as? Int
                            let user_id = details.object(forKey: Keys.KEY_USER_ID) as? Int
                            let razorpaySubId = details.object(forKey: Keys.KEY_RAZORPAY_ID) as? String
                            let name = details.object(forKey: Keys.KEY_NAME) as? String
                            let razorpay_plan = details.object(forKey: Keys.KEY_RAZORPAY_PLAN) as? String
                            let quantity = details.object(forKey: Keys.KEY_QUANTITY) as? Int
                            let total_count = details.object(forKey: Keys.KEY_TOTAL_COUNT) as? Int
                            let status = details.object(forKey: Keys.KEY_STATUS) as? String
                            let paid_count = details.object(forKey: Keys.KEY_PAID_COUNT) as? Int
                            let trail_ends_at = details.object(forKey: Keys.KEY_TRIAL_ENDS_AT) as? String
                            let created_at = details.object(forKey: Keys.KEY_CREATED_AT) as? String
                            let updated_at = details.object(forKey: Keys.KEY_UPDATED_AT) as? String
                            let razorpay_plan_item = details.object(forKey: Keys.KEY_RAZORPAY_PLAN_ITEM) as? NSDictionary
                            let active = razorpay_plan_item?.object(forKey: Keys.KEY_ACTIVE) as? Bool
                            let plan_name = razorpay_plan_item?.object(forKey: Keys.KEY_NAME) as? String
                            let plan_descr = razorpay_plan_item?.object(forKey: Keys.KEY_DESCRIPTION) as? String
                            let amount = razorpay_plan_item?.object(forKey: Keys.KEY_AMOUNT) as? Int
                            let unit_amount = razorpay_plan_item?.object(forKey: Keys.KEY_UNIT_AMOUNT) as? Int
                            let currency = razorpay_plan_item?.object(forKey: Keys.KEY_CURRENCY) as? String
                            

                            var tinternalOrder = 0
                            var tdisplayName = ""
                            var tfeatured = false
                            var tcrossPrice = 0
                            var tdiscountText = ""
                            var tdisplayPrice = 0
                            var tdisplayColor = ""
                            var tdisplayPeriod = ""
                            
                            if  let razorpay_plan_item_extra = razorpay_plan_item?.object(forKey: Keys.KEY_EXTRA) as? NSDictionary,
                                let internal_order = razorpay_plan_item_extra.object(forKey: Keys.KEY_INTERNAL_ORDER) as? Int,
                                let display_name = razorpay_plan_item_extra.object(forKey: Keys.KEY_DISPLAY_NAME) as? String,
                                let featured = razorpay_plan_item_extra.object(forKey: Keys.KEY_FEAUTURED) as? Bool,
                                let crossPrice = razorpay_plan_item_extra.object(forKey: Keys.KEY_CROSS_PRICE) as? Int,
                                let discountText = razorpay_plan_item_extra.object(forKey: Keys.KEY_DISCOUNT_TEXT) as? String,
                                let displayPrice = razorpay_plan_item_extra.object(forKey: Keys.KEY_DISPLAY_PRICE) as? Int,
                                let displayColor = razorpay_plan_item_extra.object(forKey: Keys.KEY_DISPLAY_COLOR) as? String,
                                let displayPeriod = razorpay_plan_item_extra.object(forKey: Keys.KEY_DISPLAY_PERIOD) as? String {
                                tinternalOrder = internal_order
                                tdisplayName = display_name
                                tfeatured = featured
                                tcrossPrice = crossPrice
                                tdiscountText = discountText
                                tdisplayPrice = displayPrice
                                tdisplayColor = displayColor
                                tdisplayPeriod = displayPeriod
                            }
                            
                            print("$$$$$$$=>Status Internal Order:=\(tinternalOrder)=$$$$$$$")
                            
                         let subcsriotionDetails = SubscriptionPlan(id: id ?? 0,
                                                                       subscribed: subscribed,
                                                                       type: sub_type,
                                                                       user_id: user_id ?? 0,
                                                                       subscription_id: razorpaySubId ?? "",
                                                                       name: name ?? "" ,
                                                                       plan_id: razorpay_plan ?? "",
                                                                       quantity: quantity ?? 0,
                                                                       total_count: total_count ?? 0,
                                                                       interval: interval == nil ? 1 : interval!,
                                                                       status: status ?? "",
                                                                       paid_count: paid_count ?? 0,
                                                                       charge_at: charge_at == nil ? "" : charge_at!,
                                                                       trail_ends_at: trail_ends_at ?? "",
                                                                       ends_at: ends_at == nil ? "" : ends_at!,
                                                                       created_at: created_at ?? "",
                                                                       updated_at: updated_at ?? "",
                                                                       active: active ?? false,
                                                                       plan_name: plan_name ?? "",
                                                                       description: plan_descr ?? "",
                                                                       currency: currency ?? "",
                                                                       amount: amount ?? 0,
                                                                       unit_amount: unit_amount ?? 0,
                                                                       internal_order: tinternalOrder)
                            
                            subcsriotionDetails.display_name = tdisplayName
                            subcsriotionDetails.featured = tfeatured
                            subcsriotionDetails.cross_price = tcrossPrice
                            subcsriotionDetails.discount_text = tdiscountText
                            subcsriotionDetails.display_amount = tdisplayPrice
                            subcsriotionDetails.display_color = tdisplayColor
                            subcsriotionDetails.display_period = tdisplayPeriod
                            
                            subcsriotionDetails.subscribed = subscribed
                            subcsriotionDetails.doUserCanUpgrate = App.canUpgradeSubscription
                            subcsriotionDetails.subscribedUserType = App.b2bUserType
                            subcsriotionDetails.subscribedType = App.userSubscriptionType
                            
                            App.subscription_details = subcsriotionDetails
                            subcsriotionDetails.saveSubscriptionDetails(key: UserDefaults.Keys.encodedCurrentPlanDetails)
                            print("IS data came")
                            
                            if let sub_keys = subscription.allKeys as? [String]{
                                if sub_keys.contains(Keys.KEY_PENDING_SUBSCRIPTION) {
                                    if let pending_subscription = subscription.object(forKey: Keys.KEY_PENDING_SUBSCRIPTION) as? NSDictionary {
                                        let pinterval = pending_subscription.object(forKey: Keys.KEY_INTERVAL) as? Int
                                        let pends_at = pending_subscription.object(forKey: Keys.KEY_ENDS_AT) as? String
                                        let pcharge_at = pending_subscription.object(forKey: Keys.KEY_CHARGE_AT) as? String
                                        
                                        guard let pid = pending_subscription.object(forKey: Keys.KEY_ID) as? Int,
                                        let puser_id = pending_subscription.object(forKey: Keys.KEY_USER_ID) as? Int,
                                        let prazorpaySubId = pending_subscription.object(forKey: Keys.KEY_RAZORPAY_ID) as? String,
                                        let pname = pending_subscription.object(forKey: Keys.KEY_NAME) as? String,
                                        let prazorpay_plan = pending_subscription.object(forKey: Keys.KEY_RAZORPAY_PLAN) as? String,
                                        let pquantity = pending_subscription.object(forKey: Keys.KEY_QUANTITY) as? Int,
                                        let ptotal_count = pending_subscription.object(forKey: Keys.KEY_TOTAL_COUNT) as? Int,
                                        let pstatus = pending_subscription.object(forKey: Keys.KEY_STATUS) as? String,
                                        let ppaid_count = pending_subscription.object(forKey: Keys.KEY_PAID_COUNT) as? Int,
                                        let ptrail_ends_at = pending_subscription.object(forKey: Keys.KEY_TRIAL_ENDS_AT) as? String,
                                        let pcreated_at = pending_subscription.object(forKey: Keys.KEY_CREATED_AT) as? String,
                                        let pupdated_at = pending_subscription.object(forKey: Keys.KEY_UPDATED_AT) as? String,
                                        let extra = pending_subscription.object(forKey: Keys.KEY_EXTRA) as? NSDictionary,
                                        let pdisplay_name = extra.object(forKey: Keys.KEY_DISPLAY_NAME) as? String,
                                        let pfeatured = extra.object(forKey: Keys.KEY_FEAUTURED) as? Bool,
                                        let pcrossPrice = extra.object(forKey: Keys.KEY_CROSS_PRICE) as? Int,
                                        let pdiscountText = extra.object(forKey: Keys.KEY_DISCOUNT_TEXT) as? String,
                                        let pdisplayPrice = extra.object(forKey: Keys.KEY_DISPLAY_PRICE) as? Int,
                                        let pdisplayColor = extra.object(forKey: Keys.KEY_DISPLAY_COLOR) as? String,
                                        let pdisplayPeriod = extra.object(forKey: Keys.KEY_DISPLAY_PERIOD) as? String,
                                        let pinternalOrder = extra.object(forKey: Keys.KEY_INTERNAL_ORDER) as? Int
                                        else {
                                            print("Something not correct while getting pending subscription details")
                                            completionHandler()
                                            return
                                        }
                                        
                                        var packageDiscrp = ""
                                        if let packages = extra.object(forKey: Keys.KEY_PACKAGES) as? [String] {
                                            for pack in packages {
                                                let bulletPoint: String = "\u{2022}"
                                                let formattedString: String = "\(bulletPoint) \(pack)\n"
                                                packageDiscrp += formattedString
                                                print("Formatte string:\(packageDiscrp)")
                                            }
                                        }
                                        
                                        let pendingSubsc = SubscriptionPlan(id: pid, subscribed: subscribed, type: sub_type, user_id: puser_id, subscription_id: prazorpaySubId, name: pname, plan_id: prazorpay_plan, quantity: pquantity, total_count: ptotal_count, interval: pinterval == nil ? 1 : pinterval!, status: pstatus, paid_count: ppaid_count, charge_at: pcharge_at == nil ? "" : pcharge_at!, trail_ends_at: ptrail_ends_at, ends_at: pends_at == nil ? "" : pends_at!, created_at: pcreated_at, updated_at: pupdated_at, cross_price: pcrossPrice, discount_text: pdiscountText, packages: packageDiscrp, featured: pfeatured, displayName: pdisplay_name, displayPeriod: pdisplayPeriod, displayAmount: pdisplayPrice, displayColor: pdisplayColor, internal_order: pinternalOrder)
                                          App.pending_subscription = pendingSubsc
                                        
                                        pendingSubsc.subscribed = subscribed
                                        pendingSubsc.doUserCanUpgrate = App.canUpgradeSubscription
                                        pendingSubsc.subscribedUserType = App.b2bUserType
                                        pendingSubsc.subscribedType = App.userSubscriptionType
                                        
                                          pendingSubsc.saveSubscriptionDetails(key: UserDefaults.Keys.encodedPendingPlanDetails)
                                        
                                         completionHandler()
                                    }else {
                                         App.pending_subscription = nil
                                        completionHandler()
                                    }
                                }else {
                                    completionHandler()
                                }
                            }
                        }else if subscribed && sub_type.lowercased() == "family" ,let razorpay_plan = details.object(forKey: Keys.KEY_RAZORPAY_PLAN) as? String {
                            App.subscribedAndActivePlanId = razorpay_plan
                            completionHandler()
                        }else if subscribed && sub_type.lowercased() == PlanType.FAMILY_PACK || subscribed && sub_type.lowercased() == PlanType.CORPARATE_PACKAGE{
                            let ends_at = details.object(forKey: Keys.KEY_ENDS_AT) as? String
                            let charge_at = details.object(forKey: Keys.KEY_CHARGE_AT) as? String
                            let planName = details.object(forKey: Keys.KEY_PLAN_NAME) as? String
                            let subcsriotionDetails = SubscriptionPlan(charge_at: charge_at ?? "", ends_at: ends_at ?? "", planName: planName ?? "")
                            App.subscription_details = subcsriotionDetails
                            
                            subcsriotionDetails.subscribed = subscribed
                            subcsriotionDetails.doUserCanUpgrate = App.canUpgradeSubscription
                            subcsriotionDetails.subscribedUserType = App.b2bUserType
                            subcsriotionDetails.subscribedType = App.userSubscriptionType
                            
                            subcsriotionDetails.saveSubscriptionDetails(key: UserDefaults.Keys.familyPackPlanDetails)
                            completionHandler()
                        }
                        else if subscribed && sub_type.lowercased() == "onetime"{
                            let planName = details.object(forKey: Keys.KEY_PLAN_NAME) as? String
                            let subcsriotionDetails = SubscriptionPlan(charge_at: "", ends_at: "", planName: planName ?? "")
                            App.subscription_details = subcsriotionDetails
                            
                            subcsriotionDetails.subscribed = subscribed
                            subcsriotionDetails.doUserCanUpgrate = App.canUpgradeSubscription
                            subcsriotionDetails.subscribedUserType = App.b2bUserType
                            subcsriotionDetails.subscribedType = App.userSubscriptionType
                            
                            subcsriotionDetails.saveSubscriptionDetails(key: UserDefaults.Keys.oneTimePlanDetails)
                            completionHandler()
                        }
                        else{
                            completionHandler()
                        }
                    }else {
                         App.subscription_details = nil
                         completionHandler()
                    }
                }else {
                   App.pending_subscription = nil
                   App.subscription_details = nil
                   completionHandler()
                }
            }
        }
    }
    
    /**
     method Performs request to get notification list and stores using coredata
      * can be accessed in any class
     */
//    func fetchNotifications() {
//          print("==>2")
//        if !NetworkUtilities.isConnectedToNetwork()
//        {
//            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
//            return
//        }
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//
//        let url = URL(string: AppURLS.URL_Notifications)
//        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
//        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
//        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
//        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
//        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
//        request.httpMethod = HTTPMethods.GET
//        session.dataTask(with: request, completionHandler: { (data, response, error) in
//            if error != nil
//            {
//                print("Error while fetching data\(String(describing: error?.localizedDescription))")
//            }
//            else
//            { if let response = response
//            {
//                print("url = \(response.url!)")
//                print("response = \(response)")
//                let httpResponse = response as! HTTPURLResponse
//                print("response code = \(httpResponse.statusCode)")
//
//                do
//                {
//                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
//                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
//                    if !errorStatus {
//                         print("performing error handling fetch notifications:\(jsonData)")
//                    }
//                    else {
//                        let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
//                        let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
//                        print("Status=\(responseStatus) code:\(code)")
//                        let data = jsonData.object(forKey: Keys.KEY_DATA) as! [NSDictionary]
//                        print("Notification response:\(jsonData)")
//                        if code == 200 && responseStatus == "success"
//                        {
//                            self.removeItemsFromNotifications()
//
//                            for notifData in data {
//                                var read_at = ""
//
//                                if let id = notifData.object(forKey: Keys.KEY_ID) as? String {
//                                    if let type = notifData.object(forKey: Keys.KEY_TYPE) as? String {
//                                        let typeStringArray = type.components(separatedBy: "\\")
//                                        if let notification_id = notifData.object(forKey: Keys.KEY_NOTIFICATION_ID) as? Int {
//                                            if let notification_type = notifData.object(forKey: Keys.KEY_NOTIFICATION_TYPE) as? String {
//                                                if let created_at = notifData.object(forKey: Keys.KEY_CREATED_AT) as? String {
//                                                    if let updated_at = notifData.object(forKey: Keys.KEY_UPDATED_AT) as? String {
//                                                        if let readat = notifData.object(forKey: Keys.KEY_READ_AT) as? String {
//                                                            read_at = readat
//                                                        }
//                                                        print("Created at in Notification data:\(created_at)")
//                                                        let notiData = notifData.object(forKey: Keys.KEY_DATA) as! NSDictionary
//                                                        let notificationData = try JSONSerialization.data(withJSONObject: notiData, options: JSONSerialization.WritingOptions.prettyPrinted)
//                                                        //converting nsdictionary string
//                                                        let notificationDataString = String(data: notificationData, encoding: String.Encoding.utf8)
//                                                        //print("converted json string:\(notificationDataString)")
//
//                                                        //Storing notifications
//                                                        DispatchQueue.main.async(execute: {
//                                                            let coreDatacontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//                                                            let coreNotifcation = Notifications(context: coreDatacontext)
//                                                            coreNotifcation.id = id
//                                                            coreNotifcation.notification_id = Int32(notification_id)
//                                                            coreNotifcation.notification_type = notification_type
//                                                            coreNotifcation.type = typeStringArray[2]
//                                                            coreNotifcation.read_at = read_at
//                                                            coreNotifcation.dataObject = notificationDataString
//                                                            coreNotifcation.created_at = created_at
//                                                            coreNotifcation.updatedate_at = updated_at
//
//                                                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                                                        })
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//
//                        }else {
//                            print("Error while fetching notificaitons data")
//                        }
//                    }
//
//                    //Main thread to update ui after fetching data
//                    DispatchQueue.main.async(execute: {
//                        //updates notification list
//                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "NotificationsList"), object: nil)
//                        //stops spinner animation
//                       // NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
//                    })
//
//                }
//                catch let error
//                {
//                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
//                    print("Received not-well-formatted JSON:\(error.localizedDescription)")
//                }
//
//                }
//            }
//        }).resume()
//    }
}


