//
//  RatingView.swift
//  DocOnline
//
//  Created by Kiran Kumar on 30/10/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import Foundation
import SwiftMessages
import Kingfisher
//import NVActivityIndicatorView


protocol RatingViewDelegate {
    func didTapeSubmit(_ success:Bool,rating:Double,jsonData:Data)
    func didTapNotNow()
    
}


class RatingView : UIViewController ,NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var vw_back: UIView!
    @IBOutlet weak var bt_backgorund: UIButton!
    @IBOutlet weak var lb_doctor_name: UILabel!
    @IBOutlet weak var iv_doctor_pic: UIImageView!
    @IBOutlet weak var lb_call_duration: UILabel!
    //@IBOutlet weak var vw_rating: CosmosView!
    @IBOutlet weak var bt_submit: UIButton!
    @IBOutlet weak var bt_next_time: UIButton!
    
    @IBOutlet weak var vw_call_duration: UIView!
    @IBOutlet weak var LC_call_duration_height: NSLayoutConstraint!
    
    @IBOutlet weak var lb_doctorDesignation: UILabel!
    @IBOutlet weak var lb_serprator: UILabel!
    
    @IBOutlet var ratings: [UIImageView]!
    @IBOutlet var ratingsTitle: [UILabel]!
    
    
    var emptyEmoji = [UIImage(named: "very_poor"),UIImage(named: "poor"),UIImage(named: "good"),UIImage(named: "satisfied"),UIImage(named: "awesome")]
    var filledEmoji = [UIImage(named: "very_poor_filled"),UIImage(named: "poor_filled"),UIImage(named: "good_filled"),UIImage(named: "satisfied_filled"),UIImage(named: "awesome_filled")]
    
    var appointment_id = ""
    var isImmediateAfterCall = false
    var callDuration = ""
    var docotorDetails: Doctor?
    var defaultRating: Double = 0
    var rating = 0
    
    var delegate : RatingViewDelegate?
    
    override func viewDidLoad() {
        self.iv_doctor_pic.layer.cornerRadius = self.iv_doctor_pic.frame.size.width / 2
        self.iv_doctor_pic.clipsToBounds = true
        
        self.bt_submit.layer.cornerRadius = 10
        self.bt_submit.clipsToBounds = true
        
        
        
        loadViewWithDetails()
        App.isRatingViewAlreadyPresented = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_submit.backgroundColor = Theme.buttonBackgroundColor
        bt_next_time.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
    }
    
    func adjustView(bool:Bool,constant:CGFloat) {
        self.vw_call_duration.isHidden = bool
        self.lb_serprator.isHidden = bool
        self.lb_call_duration.isHidden = bool
        self.LC_call_duration_height.constant = constant
    }
    
    func laodDetails() {
        if callDoctorModal != nil  {
            adjustView(bool:true,constant:0)
            self.lb_doctor_name.text = "\(callDoctorModal.prefix!) \(callDoctorModal.first_name!) \(callDoctorModal.last_name!)"
            let url = URL(string:callDoctorModal.avatar_url!)
            self.iv_doctor_pic.sd_setImage(with: url, completed: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.didTapNotNow()
    }
    
    func setupDoctorInfo(_ doctor:Doctor) {
        self.lb_doctor_name.text = "\(doctor.prefix ?? "") \(doctor.first_name ?? "") \(doctor.last_name ?? "")"
        self.lb_call_duration.text = App.timeDuration.isEmpty ? "" : "Call Duration: \(App.timeDuration)"
        self.lb_doctorDesignation.text = doctor.specialisation
        let url = URL(string:doctor.avatar_url ?? "")
        self.iv_doctor_pic.kf.setImage(with: url, placeholder: UIImage(named: "doctor_placeholder_nocircle"), options: nil, progressBlock: nil, completionHandler: nil)
        //self.vw_rating.rating = self.defaultRating
        rating = Int(self.defaultRating)
        for i in 0..<ratings.count{
            if i == Int((self.defaultRating-1)){
                ratings[i].image = filledEmoji[i]
            }else{
                ratings[i].image = emptyEmoji[i]
            }
        }
    
    }
    
    func loadViewWithDetails() {
        if let doctor = callDoctorModal  {
            setupDoctorInfo(doctor)
        }else if let doctor = docotorDetails {
            setupDoctorInfo(doctor)
        }
    }
    
    func storeDetails() {
        let jsonEncoder = JSONEncoder()
        do {
            let jsonEncoded = try jsonEncoder.encode(callDoctorModal)
            UserDefaults.standard.set(jsonEncoded, forKey: "DoctorDetails")
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.IS_RATING_GIVEN_TO_DOCTOR)
        }catch let err{
            print("Error while converting to data:\(err.localizedDescription)")
        }
    }
    
    ///displays success message after request
    func showSuccessMessage(message:String,duration:SwiftMessages.Duration) {
        let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
        view.configureTheme(.success)
        view.configureDropShadow()
        view.configureContent(title: "Rating submitted", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = duration
        SwiftMessages.show(config: config, view: view)
    }

    
    func showActionAlert() {
       // self.didShowAlert(title:"Sorry!",message:"Rating can't submitted now. Please try again later")
        let alert = UIAlertController(title: "Sorry!", message: "Rating can't submitted now. Please try again later", preferredStyle: .alert)
        let Okay = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            App.timeDuration = ""
            if callDoctorModal != nil {
                callDoctorModal = nil
            }
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(Okay)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func ratingCapture(_ sender: UIButton) {
        print("rating \(sender.tag)")
        rating = sender.tag
        for i in 0..<ratings.count{
            if i == (sender.tag-1){
                ratings[i].image = filledEmoji[i]
            }else{
                ratings[i].image = emptyEmoji[i]
            }
        }
    }
    
    
    @IBAction func submitTapped(_ sender: UIButton) {
        
        
        
        if rating == 0 {
            didShowAlert(title:"",message:"Please provide your rating")
            return
        }

        let ratingDic = [Keys.KEY_RATING : rating]
        
        do {
             let jsondata = try JSONSerialization.data(withJSONObject: ratingDic, options: .prettyPrinted)
            
            if self.isImmediateAfterCall {
                guard let appointmentId = callDoctorModal.appointment_id else {
                    return
                }
                let ratingURL = AppURLS.URL_BookAppointment + "/\(appointmentId)/rating"
                self.startAnimating()
                getRatingStatus(urlString: ratingURL, httpMethod: HTTPMethods.POST, jsonData: jsondata, type: 2, completionHandler: { (success, response) in
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        if success {
                            App.timeDuration = ""
                            if callDoctorModal != nil {
                                callDoctorModal = nil
                            }
                             self.showSuccessMessage(message: "Thank you for your feedback." ,duration: .seconds(seconds: 3))
                            self.dismiss(animated: true, completion: nil)
                        }else {
                            
                        }
                    }
                })
            }else {
              delegate?.didTapeSubmit(true, rating: Double(rating), jsonData: jsondata)
            }
        } catch {
            print(error.localizedDescription)
            showActionAlert()
        }
    }
    
    
    @IBAction func backgorundTapped(_ sender: UIButton) {
    }
    
    @IBAction func nextTimeTapped(_ sender: UIButton) {
     //   print("Next time Submit tapped ::: callerData is nil:\(callDoctorModal == nil ? true : false)")
        App.timeDuration = ""
        if callDoctorModal != nil {
            callDoctorModal = nil
        }
        self.delegate?.didTapNotNow()
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
