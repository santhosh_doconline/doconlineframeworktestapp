//
//  SettingsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 30/01/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import GoogleSignIn
import UserNotifications

class SettingsViewController: UIViewController {

    
    @IBOutlet var bt_emailVerification: UIButton!
    @IBOutlet var lb_email: UILabel!
    @IBOutlet var lb_phone: UILabel!
    @IBOutlet var lb_subscriptionPlanTitle: UILabel!
    @IBOutlet var lb_subscriptionStatus: UILabel!
    
    @IBOutlet var lb_mrnNumber: UILabel!
    @IBOutlet var lb_mobileNumberStatus: UILabel!
    @IBOutlet var bt_subscribe: UIButton!
    @IBOutlet var bt_phoneNumber: UIButton!
    @IBOutlet var bt_changePassword: UIButton!
    @IBOutlet var bt_changeAddress: UIButton!
    @IBOutlet var bt_logout: UIButton!
    
     var userDataModel : User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubscriptionDetails()
        
        bt_emailVerification.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        bt_phoneNumber.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        
        lb_mobileNumberStatus.textColor = Theme.buttonBackgroundColor
        
        bt_subscribe.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        
        bt_changePassword.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
      
        bt_changeAddress.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        
        bt_logout.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
    }
    
    func checkNotificationEnabled() {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
            if(settings.authorizationStatus == .authorized)
            {
                print("Push authorized")
            }
            else
            {
                print("Push not authorized")
            }
        }
    }
    
    func setupView() {
        
        if App.isMobileVerified {
            self.bt_phoneNumber.setTitle("Change", for: UIControl.State.normal)
            self.lb_mobileNumberStatus.text = "Verified & Active"
        }
        
        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
            
            self.userDataModel  = User.get_user_profile_default()
            self.lb_email.text =  App.userEmail
            self.bt_emailVerification.isHidden = false
            self.lb_mrnNumber.text = self.userDataModel.mrnNumber
            if self.userDataModel.email!.isEmpty {
                self.bt_emailVerification.setTitle("Add", for: .normal)
                self.bt_emailVerification.isEnabled = true
                self.lb_email.text = self.userDataModel.email!
            }else {
                self.lb_email.text = self.userDataModel.email!
                if App.isEmailVerified {
                    self.bt_emailVerification.setTitle("", for: .normal)
                    self.bt_emailVerification.isEnabled = false
                }else {
                    self.bt_emailVerification.setTitle("Verify", for: .normal)
                    self.bt_emailVerification.isEnabled = true
                }
            }
            
            
            if self.userDataModel.phone!.isEmpty == true {
                print("User phone is empty")
                 self.lb_phone.text = ""
                self.bt_phoneNumber.setTitle("Add", for: UIControl.State.normal)
                self.lb_mobileNumberStatus.text = ""
            }else {
                self.lb_phone.text = self.userDataModel.phone!
                if !App.isMobileVerified  {
                    self.bt_phoneNumber.setTitle("Verify", for: UIControl.State.normal)
                    self.lb_mobileNumberStatus.text = "Not Verified"
                }else {
                    self.bt_phoneNumber.setTitle("Change", for: UIControl.State.normal)
                    self.lb_mobileNumberStatus.text = "Verified & Active"
                }
                
            }
            
            
            
            if let mobileStored = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
                self.lb_phone.text = mobileStored
            }else {
                self.lb_phone.isEnabled = true
            }
        }

    }
    
    func setupSubscriptionDetails() {
        if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("b2b") {
            self.lb_subscriptionStatus.text = ""// "Enterprise Plan"
        }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("b2c"){
            if let subDetails = App.subscription_details {
                self.lb_subscriptionStatus.text = ""
            }
            
            if let pending = App.pending_subscription {
               self.lb_subscriptionStatus.text = "" //"Upgrade Pending"
            }
        }else if App.didUserSubscribed  {
          self.lb_subscriptionStatus.text = "" //"\(App.userSubscriptionType) Plan"
        }else {
            self.lb_subscriptionStatus.text = "" //"UnSubscribed"
        }
     }
    
    ///Performs logout operation of user
    func logoutTapped() {
        let alert = UIAlertController(title: "Are you sure?", message: "you want to logout", preferredStyle: UIAlertController.Style.alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            Theme.defaultTheme()
            UserDefaults.standard.set(false, forKey: "isNotDefaultTheme")

            UserDefaults.standard.set(App.apvoip_token, forKey: UserDefaltsKeys.PREVIOUS_APVOIP_TOKEN)
            UserSession.sharedInstace.makePresentAccessTokenToPrevious()
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.DID_USER_LOGED_OUT)
            UserSession.sharedInstace.logourUser(accessToken: App.getUserAccessToken(), apVoipToken: App.apvoip_token)
            
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_NAME)
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_HAS_LOGIN_KEY)
            UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_HEALTH_STATUS)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.TOKENTYPE)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.ACCESS_TOKEN)
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
            UserDefaults.standard.set(false,forKey: UserDefaltsKeys.KEY_USER_STATUS)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.MOBILE_NUMBER)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_LOGGED_USER_EMAIL)
            
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
            UserDefaults.standard.set("", forKey: UserDefaltsKeys.KEY_USER_EMAIL)
            
            User.remove_user_profile_default()
            AppSettings.removeEncodedAppSettingsDataFromUserDefaults()
            App.appSettings = nil
            
            //UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_PHONE)
            App.isMobileVerified = false
            App.isEmailVerified = false
            App.imagesURLString.removeAll()
            App.ImagesArray.removeAll()
            App.bookingAttachedImages.removeAll()
            App.subscription_details = nil
            App.pending_subscription = nil
            App.appointmentsList.removeAll()
            App.tempAppointments.removeAll()
            App.lang_preferences_values.removeAll()
            App.lang_preferences_values.append("English")
            RefreshTimer.sharedInstance.stopGlobalTimer()
            
            App.user_full_name = ""
            App.userEmail = ""
            App.avatarUrl = ""
            
            App.b2bUserType = ""
            App.didUserSubscribed = false
            App.userSubscriptionType = ""
            App.subscription_details = nil
            App.canUpgradeSubscription = false
            App.notificationReadCount = 0
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UserDefaults.standard.set(App.notificationReadCount, forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT)
//            GIDSignIn.sharedInstance().signOut()
            
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyBoard.instantiateViewController(withIdentifier: StoryBoardIds.ID_LOGIN_VIEW) as! LoginViewController
//            self.present(vc, animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    /**
     Unwind segue action
     */
    @IBAction func unwindToSettings(segue:UIStoryboardSegue) {
        setupView()
    }
    
    @IBAction func emailVerifyTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VERIFY_EMAIL, sender: sender)
    }
    
    @IBAction func changePhoneNumberTapped(_ sender: UIButton) {
         App.isFromView = FromView.SettingsView
         self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, sender: self)
    }
    
    @IBAction func subscribeTapped(_ sender: UIButton) {
        App.isFromView = FromView.SettingsView
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
    }
    
    @IBAction func changePasswordTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SET_PASSWORD, sender: self)
    }
    
    @IBAction func clickBtnChangeAddress(_ sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SET_ADDRESS, sender: self)
    }
    
    @IBAction func logoutTapped(_ sender: UIButton) {
        logoutTapped() 
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_VERIFY_EMAIL {
            let destVC = segue.destination as! EmailVerificationViewController
            destVC.delegate = self
        }
    }
}
extension SettingsViewController : EmailVerificationDelegate {
    func didVerifyEmail(_ email: String, _ success: Bool) {
        if success {
            self.navigationController?.popViewController(animated: true)
            self.setupView()
            self.bt_emailVerification.setTitle("", for: .normal)
            self.bt_emailVerification.isEnabled = false
            self.lb_email.text = email
        }
    }
}


