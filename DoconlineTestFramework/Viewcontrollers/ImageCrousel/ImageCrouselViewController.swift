//
//  ImageCrouselViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 23/05/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Alamofire
//import PDFReader

protocol ImageCrouselDelegate {
    func didCompleteAttachingImages(images:[ImagesCollectionViewCellModal])
}

class ImageCrouselViewController: UIViewController , NVActivityIndicatorViewable {

    private let reuseIdentifier = "Cell"
    let kRoomCellScaling: CGFloat = 0.6
    
    @IBOutlet var lb_shadow: UILabel!
    @IBOutlet var crouselCollectionView: UICollectionView!
    @IBOutlet var bottomCollectionView: UICollectionView!
    
    @IBOutlet var bt_viewFile: UIBarButtonItem!
    @IBOutlet var tv_caption: UITextField!
    
    var selectedImageIndex = 0
    var isFromSummaryPage = false
    var isNewUpload = false
    var appintmentId = ""
    
    var delegate: ImageCrouselDelegate?
    var doneBarButtonItem: UIBarButtonItem?
    
    var tempSelectedFiles = [ImagesCollectionViewCellModal]()
    var tempSelectedImages = [ImagesCollectionViewCellModal]()
    
    /// Creating UIDocumentInteractionController instance.
    let documentInteractionController = UIDocumentInteractionController()
    
    let layout = ImageCollectionViewLayout()
    
    var tempBookingAttachments = [ImagesCollectionViewCellModal]()
  
    var isFromEHRFileUploadVC = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /// Setting UIDocumentInteractionController delegate.
        documentInteractionController.delegate = self
        self.tv_caption.attributedPlaceholder = NSAttributedString(string: "Add a caption",
                                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        // This method sets up the collection view
        layout.itemSize = CGSize(width: self.view.frame.size.width - 20, height: self.view.frame.size.height - 64)
        layout.scrollDirection = .horizontal
        
        layout.sideItemAlpha = 1
        layout.sideItemScale = 0.8
        layout.spacingMode = ImageCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 60)
        
        crouselCollectionView?.setCollectionViewLayout(layout, animated: false)
      
        if App.bookingAttachedImages.indices.contains(self.selectedImageIndex) && !self.isNewUpload  && self.isFromSummaryPage  {
            self.tempBookingAttachments.append(App.bookingAttachedImages[self.selectedImageIndex])
            self.loadCaptionForItem(At: 0)
            self.crouselCollectionView.reloadData()
        }else if self.isFromSummaryPage && self.isNewUpload {
            self.doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.doneTapped(_:)))
            self.navigationItem.rightBarButtonItem =  self.doneBarButtonItem
            self.tempBookingAttachments = App.bookingAttachedImages
            self.crouselCollectionView.reloadData()
            self.loadCaptionForItem(At: self.selectedImageIndex)
        }else if !self.isFromSummaryPage && isNewUpload && App.bookingAttachedImages.indices.contains(self.selectedImageIndex){
             self.tempBookingAttachments.append(App.bookingAttachedImages[self.selectedImageIndex])
            self.crouselCollectionView.reloadData()
            self.loadCaptionForItem(At: 0)
        }
       
        self.tv_caption.addTarget(self, action: #selector(self.fieldStateChanged), for: .editingChanged)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    @objc func doneTapped(_ sender:UIBarButtonItem) {
        self.uploadViaAlamofire()
    }
    
    func uploadViaAlamofire() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        self.startAnimating()
        let urlString = "\(AppURLS.URL_BookAppointment)/\(self.appintmentId)/attachments"
        let headers = [
                  NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION : App.getUserAccessToken(),
                  NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API : NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE,
                  NETWORK_REQUEST_KEYS.KEY_ACCEPT : NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON
           ]
        
    //    App.bookingAttachedImages = self.tempBookingAttachments
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index,file) in App.bookingAttachedImages.enumerated() {
                if file.picture?.type?.rawValue == FileType.file.rawValue {
                    if let url = URL(string: file.picture?.fileURl ?? "") {
                        multipartFormData.append(url, withName: "attachments[\(index)]" )
                    }
                }else if file.picture?.type?.rawValue == FileType.image.rawValue {
                    if let image = file.picture?.pic , let imageData = image.jpegData(compressionQuality: 0.4){
                        multipartFormData.append(imageData, withName: "attachments[\(index)]" ,fileName: "image\(index).jpg", mimeType: "image/jpg")
                    }
                }
                
                multipartFormData.append((file.picture?.picCaption ?? "").data(using: String.Encoding.utf8)!, withName: "titles[\(index)]")
            }
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print("Alamofire response : \(response.result.value)")
                    if response.response?.statusCode ?? 0 == 422 ,let resp = response.result.value as? [String:Any] ,let data = resp[Keys.KEY_DATA] as? [String:Any] {
                        var errors : [String:Any] = [:]
                        if let singleError =  data[Keys.KEY_ERROR] as? [String:Any] {
                            errors = singleError
                        }
                        
                        if let multipleErrors = data[Keys.KEY_ERRORS] as? [String:Any] {
                            errors = multipleErrors
                        }
                        
                        var message = ""
                        for (_,value) in errors {
                            if let titleMsg = value as? [String] {
                                message = titleMsg.first ?? ""
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.stopAnimating()
                            self.didShowAlert(title: "Failed to upload file(s)." , message: message)
                        }
                    }else {
                        
                        let status = self.check_Status_Code(statusCode: response.response?.statusCode ?? 0, data: response.result.value as? NSDictionary)
                        
                        if status {
                            if let resp = response.result.value as? [String:Any] ,let attachments = resp[Keys.KEY_DATA] as? [[String:Any]] {
                                 App.bookingAttachedImages.removeAll()
                                for (index,newAttach) in attachments.enumerated() {
                                    let id = newAttach[Keys.KEY_ID] as? Int
                                    let appointmentID = newAttach[Keys.KEY_APPOINTMENT_ID] as? Int
                                    let title = newAttach[Keys.KEY_TITLE] as? String
                                    let fileURL = newAttach[Keys.FILE_URL] as? String
                                    
                                    var isFileExtension = false
                                    for fileExtension in App.fileExtentionTypes {
                                        if (fileURL ?? "").contains(fileExtension) {
                                            isFileExtension = true
                                            break
                                        }else {
                                            isFileExtension = false
                                        }
                                    }
                                    print("isFileExtension:\(isFileExtension)")
                                    if isFileExtension {
                                        var picture = Picture(fileURl: fileURL ?? "" ,picCaption: title ?? "", attachmentId: id ?? 0, appointmentId: appointmentID ?? 0, type: .file)
                                        picture.isNewUpload = false
                                        App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picture , index: index ) )
                                    }else {
                                        var picture = Picture(imageUrl: fileURL ?? "" ,picCaption: title ?? "", attachmentId: id ?? 0, appointmentId: appointmentID ?? 0, type: .image)
                                        picture.isNewUpload = false
                                        App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picture , index: index ) )
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    self.delegate?.didCompleteAttachingImages(images: App.bookingAttachedImages)
                                }
                            }
                        
                        }else {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.didShowAlert(title: "", message: "Failed to upload file(s). Please try again")
                            }
                        }
                    }
                }
    
                
            case .failure(let encodingError):
                print(encodingError)
                
                DispatchQueue.main.async {
                    self.stopAnimating()
                     self.didShowAlert(title: "", message: "Failed to upload file(s). Please try again")
                }
            }
        }
    }
    
    func loadCaptionForItem(At:Int) {
        if self.tempBookingAttachments.count != 0 , self.tempBookingAttachments.indices.contains(At){  //App.bookingAttachedImages.count != 0 {
            let pictureVM = self.tempBookingAttachments[At]  //[At]
            self.bt_viewFile.tag = At
            if pictureVM.picture?.isNewUpload ?? true {
                self.tv_caption.isEnabled = true
                self.tv_caption.isHidden = false
                self.lb_shadow.isHidden = false
                self.tv_caption.isEnabled = true
    
                if self.isFromSummaryPage && self.isNewUpload {
                   self.navigationItem.rightBarButtonItem =  self.doneBarButtonItem
                }else if !self.isFromSummaryPage && self.isNewUpload {
                    self.navigationItem.rightBarButtonItem =  nil
                }else {
                    self.navigationItem.rightBarButtonItem = nil
                }
            }else {
                self.navigationItem.rightBarButtonItem = bt_viewFile

                if (pictureVM.picture?.picCaption?.isEmpty ?? false) {
                    self.tv_caption.isHidden = true
                    self.lb_shadow.isHidden = true
                }else {
                    self.tv_caption.isHidden = false
                    self.lb_shadow.isHidden = false
                    self.tv_caption.isEnabled = false
                }
            }
            self.tv_caption.text = pictureVM.picture?.picCaption
        }
    }
    
    @objc func fieldStateChanged() {
        print("fbabfbf:\(self.selectedImageIndex)  fbnff:\(App.bookingAttachedImages.indices.contains(self.selectedImageIndex))")
        guard let caption = self.tv_caption.text ,App.bookingAttachedImages.indices.contains(self.selectedImageIndex) else  {
           return
        }
       
        App.bookingAttachedImages[self.selectedImageIndex].picture?.picCaption = caption
    }
    
    /**
     let frame: CGRect = CGRect(x: contentOffset, y: self.contentOffset.y , width: self.frame.width, height: self.frame.height)
     self.scrollRectToVisible(frame, animated: true)
     
     Method uploads images to server with parameters
     - Parameter id: appoitment id should be passed
     - Parameter tag: to know from which class it is called
     */
    func uploadSelectedImages(selectedFiles:[ImagesCollectionViewCellModal],completion: @escaping ((_ success:Bool) -> Void)) {
        if App.bookingAttachedImages.count != 0 {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
                completion(false)
                return
            }
            
            self.startAnimating()
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let boundary = generateBoundaryString()
            let urlString = "\(AppURLS.URL_BookAppointment)/\(self.appintmentId)/attachments"
            print("ImageUrl = \(urlString)")
            let url = URL(string: urlString)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API) //New header for identification
            // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethods.POST
            request.httpBody = createBodyWithParametersForCapetionedImage(parameters: selectedFiles, boundary: boundary) as Data
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.stopAnimating()
                        completion(false)
                    }
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                 completion(false)
                            }
                            print("performing error handling uploading consultation images:\(resultJSON)")
                        }
                        else {
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            //clear images array after successfull upload **
                            
                            if httpResponse.statusCode == 201 || httpResponse.statusCode == 200{
                                App.ImagesArray.removeAll()
                                App.imagesURLString.removeAll()
                                
                                DispatchQueue.main.async {
                                  //  self.delegate?.didCompleteAttachingImages(images: App.bookingAttachedImages)
                                    completion(false)
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                completion(false)
                            }
                            print("Images upload response:\(resultJSON)  status:\(status)")
                        }
                    }catch let error{
                        DispatchQueue.main.async {
                            self.stopAnimating()
                        }
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                         completion(false)
                    }
                }
                
            }).resume()
            
        }else {
            print("User not logged in")
        }
    }
    
    func openFile(atIndex:Int) {
        let picCellModal = self.tempBookingAttachments[atIndex] //App.bookingAttachedImages[atIndex]
        if picCellModal.picture?.isNewUpload ?? false {
            return
        }
        
        var fileURl = ""
        if picCellModal.picture?.type?.rawValue == FileType.file.rawValue {
            fileURl = picCellModal.picture?.fileURl ?? ""
        }else {
            fileURl = picCellModal.picture?.imageUrl ?? ""
        }
        
        if fileURl.isEmpty {
            self.didShowAlert(title: "", message: "File is not available to download.Please try again later")
            return
        }
        
        self.storeAndShare(withURLString: fileURl)
    }
    
    
    @IBAction func viewFileTapped(_ sender: UIBarButtonItem) {
         self.openFile(atIndex: sender.tag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ImageCrouselViewController {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        
        let tmpDirURL = FileManager.default.temporaryDirectory.appendingPathComponent(url.lastPathComponent)
        
        if FileManager.default.fileExists(atPath: tmpDirURL.path) {
            print("File existes at path")
            DispatchQueue.main.async {
                self.share(url: tmpDirURL)
            }
        }else {
            /// START YOUR ACTIVITY INDICATOR HERE
            self.startAnimating()
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                    return
                }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.pdf")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    self.stopAnimating()
                    self.share(url: tmpURL)
                }
            }.resume()
        }
    }

}

extension ImageCrouselViewController: UIDocumentInteractionControllerDelegate {
    
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension ImageCrouselViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tempBookingAttachments.count//App.bookingAttachedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInstance = self.tempBookingAttachments[indexPath.row] //App.bookingAttachedImages[indexPath.row]
        let cell = cellInstance.cellInstance(collectionView, cellForItemAt: indexPath)
        if cell.bt_viewFile != nil {
            cell.bt_viewFile.tag = indexPath.row
        }
        
        if cell.containerOfWebView != nil {
            cell.containerOfWebView.tag = indexPath.row
        }
        
        cell.cellDelegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.openFile(atIndex: indexPath.row)
    }
}

extension ImageCrouselViewController : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.crouselCollectionView.contentOffset, size: self.crouselCollectionView.bounds.size)
        let indexPath = self.crouselCollectionView.indexPathForItem(at: CGPoint(x: visibleRect.midX, y: visibleRect.midY))
        self.selectedImageIndex = indexPath?.row ?? 0
        self.loadCaptionForItem(At: self.selectedImageIndex)
    }
}

extension ImageCrouselViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (self.tv_caption.text?.count)! + string.count - range.length
        return newLength <= 30
    }
}

extension ImageCrouselViewController : ImagesCollectionViewDelegate {
    func didTapVieFile(at: Int) {
        self.openFile(atIndex: at)
    }
    
    func didTapRemoveImge(at: Int) {
    }
    
}


