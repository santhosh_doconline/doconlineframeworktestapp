
//
//  EHRUploadViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 18/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
//import NVActivityIndicatorView
//import Firebase

class EHRUploadViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var uploadsCollectionView: UICollectionView!
    
    @IBOutlet weak var uploadBtn: UIButton!
    
    var selectedImageIndex = 0
    var selectedCategoryIndex = 0
    
    var completionDelegate: ImageCrouselDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uploadBtn.backgroundColor = Theme.buttonBackgroundColor
        self.uploadsCollectionView.reloadData()
        
        let category = AppSettings.DocumentCategories.getSettings(at: self.selectedCategoryIndex)
        self.navigationItem.title = category?.title
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))//UIButton(type: .custom)
        backButton.setImage(UIImage(named: "BoldNavBackButton") , for: .normal)
        backButton.addTarget(self, action:  #selector(self.backTapped(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backTapped(_ sender:UIBarButtonItem) {
        if App.bookingAttachedImages.isEmpty {
            self.navigationController?.popViewController(animated: true)
        }else {
            let alert = UIAlertController(title: "Are you sure?", message: "Files are not uploaded. Do you want to discard", preferredStyle: .alert)
            let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                self.navigationController?.popViewController(animated: true)
                App.bookingAttachedImages.removeAll()
            }
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func uploadViaAlamofire() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        self.startAnimating()
        let urlString = AppURLS.URL_EHR_FILE_UPLOAD
        let headers = [
            NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION : App.getUserAccessToken(),
            NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API : NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE,
            NETWORK_REQUEST_KEYS.KEY_ACCEPT : NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON
        ]
        
        //    App.bookingAttachedImages = self.tempBookingAttachments
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index,file) in App.bookingAttachedImages.enumerated() {
                if file.picture?.type?.rawValue == FileType.file.rawValue {
                    if let url = URL(string: file.picture?.fileURl ?? "") {
                        multipartFormData.append(url, withName: "attachments[\(index)]" )
                    }
                }else if file.picture?.type?.rawValue == FileType.image.rawValue {
                    if let image = file.picture?.pic , let imageData = image.jpegData(compressionQuality: 0.4){
                        multipartFormData.append(imageData, withName: "attachments[\(index)]" ,fileName: "image\(index).jpg", mimeType: "image/jpg")
                    }
                }
                
                multipartFormData.append((file.picture?.picCaption ?? "").data(using: String.Encoding.utf8)!, withName: "titles[\(index)]")
            }
    
            multipartFormData.append("\(AppSettings.DocumentCategories.getSettings(at: self.selectedCategoryIndex)?.id ?? 0)".data(using: String.Encoding.utf8)!, withName: "category_id")
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print("Alamofire response : \(response.result.value)")
                    if response.response?.statusCode ?? 0 == 422 ,let resp = response.result.value as? [String:Any] ,let data = resp[Keys.KEY_DATA] as? [String:Any] {
                        var errors : [String:Any] = [:]
                        if let singleError =  data[Keys.KEY_ERROR] as? [String:Any] {
                            errors = singleError
                        }
                        
                        if let multipleErrors = data[Keys.KEY_ERRORS] as? [String:Any] {
                            errors = multipleErrors
                        }
                        
                        var message = ""
                        for (_,value) in errors {
                            if let titleMsg = value as? [String] {
                                message = titleMsg.first ?? ""
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.stopAnimating()
                            self.didShowAlert(title: "Failed to upload file(s)." , message: message)
                        }
                    }else {
                        
                        let status = self.check_Status_Code(statusCode: response.response?.statusCode ?? 0, data: response.result.value as? NSDictionary)
                        
                        if status {
                            if let resp = response.result.value as? [String:Any] ,
                                let message = resp[Keys.KEY_MESSAGE] as? String{
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                        self.completionDelegate?.didCompleteAttachingImages(images: [])
                                    })
                                    alert.addAction(action)
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.didShowAlert(title: "", message: "Failed to upload file(s). Please try again")
                            }
                        }
                    }
                }
                
                
            case .failure(let encodingError):
                print(encodingError)
                
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: "Failed to upload file(s). Please try again")
                }
            }
        }
    }


    @IBAction func uploadTapped(_ sender: UIButton) {
         let fileSize = App.FileSize
        if App.bookingAttachedImages.isEmpty {
            self.didShowAlert(title: "", message: "You don't have any files to upload.")
            return
        }
        
        for file in App.bookingAttachedImages {
            if file.isFileSizeGreaterThan25MB {
                self.didShowAlert(title: "", message: String(format: "Max %.0f MB / File is allowed to upload", fileSize) )
                return
            }
            
            if (file.picture?.picCaption ?? "").isEmpty {
                self.didShowAlert(title: "", message: "File title(s) required!.")
                return
            }
        }
        
        self.uploadViaAlamofire()
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_APP_IMAG_VIEW {
            if let destVC = segue.destination as? EHRPreviewViewController {
                destVC.selectedImageIndex = self.selectedImageIndex
                destVC.isNewUpload = true
            }
        }
    }
    

}

extension EHRUploadViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return App.bookingAttachedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let  cell = App.bookingAttachedImages[indexPath.row].cellInstance(collectionView, cellForItemAt: indexPath)
        cell.cellDelegate = self
        cell.bt_delete.tag = indexPath.row
        cell.tf_caption.tag = indexPath.row
       
        if let url = URL(string: App.bookingAttachedImages[indexPath.row].picture?.fileURl ?? "") {
            let size = self.sizePerMB(url:url)
            cell.lb_fileSize.text = String(format: "%.2fMB", size)
            if size > App.FileSize {
                cell.lb_fileSizeExceedMsg.isHidden = true
                App.bookingAttachedImages[indexPath.row].isFileSizeGreaterThan25MB = true
            }else {
                cell.lb_fileSizeExceedMsg.isHidden = true
                App.bookingAttachedImages[indexPath.row].isFileSizeGreaterThan25MB = false
            }
        }
        
        if let image = App.bookingAttachedImages[indexPath.row].picture?.pic , let imgData: Data =  image.jpegData(compressionQuality: 0.5) {
            let imageSize: Int = imgData.count
            let imageSizeInMB = Double(imageSize) / 1024.0 / 1024.0
            cell.lb_fileSize.text = String(format: "%.2fMB", imageSizeInMB)
            if imageSizeInMB > App.FileSize {
                cell.lb_fileSizeExceedMsg.isHidden = true
                App.bookingAttachedImages[indexPath.row].isFileSizeGreaterThan25MB = true
            }else {
                cell.lb_fileSizeExceedMsg.isHidden = true
                App.bookingAttachedImages[indexPath.row].isFileSizeGreaterThan25MB = false
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedImageIndex = indexPath.row
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APP_IMAG_VIEW, sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension EHRUploadViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 80)
    }
}

extension EHRUploadViewController: ImagesCollectionViewDelegate {
    func didTapRemoveImge(at: Int) {
        App.bookingAttachedImages.remove(at: at)
        self.uploadsCollectionView.reloadData()
    }
    
    func didTapVieFile(at: Int) {
        
    }
}
