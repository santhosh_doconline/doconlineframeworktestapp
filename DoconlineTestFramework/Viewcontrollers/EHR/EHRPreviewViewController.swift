//
//  EHRPreviewViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 24/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class EHRPreviewViewController: UIViewController , NVActivityIndicatorViewable {

    @IBOutlet var lb_shadow: UILabel!
    @IBOutlet var crouselCollectionView: UICollectionView!
    @IBOutlet var bt_viewFile: UIBarButtonItem!
    @IBOutlet var tv_caption: UITextField!
    
    var selectedImageIndex = 0
    var isNewUpload = false
    var appintmentId = ""
    var delegate: ImageCrouselDelegate?
    var doneBarButtonItem: UIBarButtonItem?
    
    /// Creating UIDocumentInteractionController instance.
    let documentInteractionController = UIDocumentInteractionController()
    let layout = ImageCollectionViewLayout()
    var tempBookingAttachments = [ImagesCollectionViewCellModal]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        /// Setting UIDocumentInteractionController delegate.
        documentInteractionController.delegate = self 
        self.tv_caption.attributedPlaceholder = NSAttributedString(string: "Add a caption",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        // This method sets up the collection view
        layout.itemSize = CGSize(width: self.view.frame.size.width - 20, height: self.view.frame.size.height - 64)
        layout.scrollDirection = .horizontal
        
        layout.sideItemAlpha = 1
        layout.sideItemScale = 0.8
        layout.spacingMode = ImageCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 60)
        
        crouselCollectionView?.setCollectionViewLayout(layout, animated: false)
        
        self.tv_caption.addTarget(self, action: #selector(self.fieldStateChanged), for: .editingChanged)

        
//        if App.bookingAttachedImages.indices.contains(self.selectedImageIndex) && !self.isNewUpload   {
//            self.tempBookingAttachments.append(App.bookingAttachedImages[self.selectedImageIndex])
//            self.loadCaptionForItem(At: 0)
//            self.crouselCollectionView.reloadData()
//        }else if self.isNewUpload {
//            self.doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneTapped(_:)))
//            self.navigationItem.rightBarButtonItem =  self.doneBarButtonItem
            self.tempBookingAttachments = App.bookingAttachedImages
            self.loadCaptionForItem(At: self.selectedImageIndex)
            self.crouselCollectionView.reloadData()
//        }
    }
    
    @objc func doneTapped(_ sender:UIBarButtonItem) {
       
    }

    func loadCaptionForItem(At:Int) {
        if App.bookingAttachedImages.count != 0 , App.bookingAttachedImages.indices.contains(At){  //App.bookingAttachedImages.count != 0 {
            let pictureVM = self.tempBookingAttachments[At]  //[At]
            self.bt_viewFile.tag = At
            if pictureVM.picture?.isNewUpload ?? true {
                self.tv_caption.isEnabled = true
                self.tv_caption.isHidden = false
                self.lb_shadow.isHidden = false
                self.tv_caption.isEnabled = true
                
                if self.isNewUpload {
                    self.navigationItem.rightBarButtonItem =  self.doneBarButtonItem
                }else {
                    self.navigationItem.rightBarButtonItem = nil
                }
            }else {
                self.navigationItem.rightBarButtonItem = bt_viewFile
                
                if (pictureVM.picture?.picCaption?.isEmpty ?? false) {
                    self.tv_caption.isHidden = true
                    self.lb_shadow.isHidden = true
                }else {
                    self.tv_caption.isHidden = false
                    self.lb_shadow.isHidden = false
                    self.tv_caption.isEnabled = false
                }
            }
            self.tv_caption.text = pictureVM.picture?.picCaption
        }
    }
    
    
    @objc func fieldStateChanged() {
        print("fbabfbf:\(self.selectedImageIndex)  fbnff:\(App.bookingAttachedImages.indices.contains(self.selectedImageIndex))")
        guard let caption = self.tv_caption.text ,App.bookingAttachedImages.indices.contains(self.selectedImageIndex) else  {
            return
        }
        
        App.bookingAttachedImages[self.selectedImageIndex].picture?.picCaption = caption
    }
    
    
    func openFile(atIndex:Int) {
        let picCellModal = self.tempBookingAttachments[atIndex] //App.bookingAttachedImages[atIndex]
        if picCellModal.picture?.isNewUpload ?? false {
            return
        }
        
        var fileURl = ""
        if picCellModal.picture?.type?.rawValue == FileType.file.rawValue {
            fileURl = picCellModal.picture?.fileURl ?? ""
        }else {
            fileURl = picCellModal.picture?.imageUrl ?? ""
        }
        
        if fileURl.isEmpty {
            self.didShowAlert(title: "", message: "File is not available to download.Please try again later")
            return
        }
        
        //self.storeAndShare(withURLString: fileURl)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EHRPreviewViewController {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        
        let tmpDirURL = FileManager.default.temporaryDirectory.appendingPathComponent(url.lastPathComponent)
        
        if FileManager.default.fileExists(atPath: tmpDirURL.path) {
            print("File existes at path")
            DispatchQueue.main.async {
                self.share(url: tmpDirURL)
            }
        }else {
            /// START YOUR ACTIVITY INDICATOR HERE
            self.startAnimating()
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                    return
                }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.pdf")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    self.stopAnimating()
                    self.share(url: tmpURL)
                }
                }.resume()
        }
    }
}

extension EHRPreviewViewController: UIDocumentInteractionControllerDelegate {
    
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension EHRPreviewViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tempBookingAttachments.count//App.bookingAttachedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInstance = self.tempBookingAttachments[indexPath.row] //App.bookingAttachedImages[indexPath.row]
        let cell = cellInstance.cellInstance(collectionView, cellForItemAt: indexPath)
        if cell.bt_viewFile != nil {
            cell.bt_viewFile.tag = indexPath.row
        }
        
        if cell.containerOfWebView != nil {
            cell.containerOfWebView.tag = indexPath.row
        }
        
        cell.cellDelegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.openFile(atIndex: indexPath.row)
    }
}

extension EHRPreviewViewController : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.crouselCollectionView.contentOffset, size: self.crouselCollectionView.bounds.size)
        let indexPath = self.crouselCollectionView.indexPathForItem(at: CGPoint(x: visibleRect.midX, y: visibleRect.midY))
        self.selectedImageIndex = indexPath?.row ?? 0
        self.loadCaptionForItem(At: self.selectedImageIndex)
    }
}

extension EHRPreviewViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (self.tv_caption.text?.count)! + string.count - range.length
        return newLength <= 30
    }
}

extension EHRPreviewViewController : ImagesCollectionViewDelegate {
    func didTapVieFile(at: Int) {
        self.openFile(atIndex: at)
    }
    
    func didTapRemoveImge(at: Int) {
    }
    
}

