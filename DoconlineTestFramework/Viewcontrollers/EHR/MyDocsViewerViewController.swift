//
//  MyDocsViewerViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import WebKit
//import AnimatedCollectionViewLayout
//import NVActivityIndicatorView
import SwiftMessages
import MobileCoreServices

protocol PrescriptionFetchDelegate {
    func didFetchHtmlString(_ string:String,with id:String)
    func didFinishPagination(_ prescriptionList:PrescriptionsList?,with prescriptions:[Consultation])
    func didTakeSnapshot(at index:Int,snapshot:UIImage?)
}

class MyDocsViewerViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet var collectionView: UICollectionView!
    
    var docType = 2
    
    let cellIdenitifierForWebView = "Document"
    let cellIdentifierForSnapshot = "DocumentImage"
    
    var prescriptions = [Consultation]()
    var prescriptionList: PrescriptionsList?
    
    var batches = [EHRFile.Batch]()
    var batchData: EHRFile?
    
    var shouldShowIndexAt = 0
    
    var fetchDelegate: PrescriptionFetchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.isPrefetchingEnabled = true
        self.collectionView.prefetchDataSource = self
        
        if let layout = collectionView?.collectionViewLayout as? AnimatedCollectionViewLayout {
            layout.scrollDirection = .horizontal
            layout.animator = CubeAttributesAnimator()
        }
        self.collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated:Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.collectionView.scrollToItem(at: IndexPath(row: self.shouldShowIndexAt, section: 0)  , at: .centeredHorizontally, animated: true)
           
            self.navigationItem.title = "BOOKING ID: \(self.prescriptions[self.shouldShowIndexAt].publicAppointmentID ?? "")"
            
        }
    }
    
    func getPrescriptions(pageUrl:String) {
        print("Files URL:\(pageUrl)")
        DispatchQueue.main.async {
            self.startAnimating()
        }
        NetworkCall.performGet(url:pageUrl) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.collectionView.reloadData()
                }
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    self.prescriptionList = try decoder.decode(PrescriptionsList.self, from: jsonData)
                    self.prescriptions += self.prescriptionList?.consultations ?? []
                    
                    self.fetchDelegate?.didFinishPagination(self.prescriptionList, with: self.prescriptionList?.consultations ?? [])
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                        print("prescriotnjvbdjbsbv:\(self.prescriptions.count)")
                        self.prescriptions.forEach{ print("Appointmntid:\($0.publicAppointmentID ?? "") user:\($0.patient?.full_name ?? "")") }
                    }
                }catch let err {
                    print("Error while converting the ehrfile data:\(err)")
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
                self.collectionView.reloadData()
            }
        }
    }

//    func loadPrescription(at index:Int,cell:MyDocViewerCollectionViewCell) {
//        cell.getPrescription(appId: self.prescriptions[index]) { (htmlString) in
//            if !htmlString.isEmpty {
//                self.prescriptions[index].prescriptionHTMLString = htmlString
//                self.fetchDelegate?.didFetchHtmlString(htmlString, with: self.prescriptions[index].publicAppointmentID ?? "")
//            }
//        }
//    }
    
//    private func indexOfMajorCell() -> Int {
//        if let layout = collectionView?.collectionViewLayout as? AnimatedCollectionViewLayout {
//            let itemWidth = layout.itemSize.width
//            let proportionalOffset = layout.collectionView!.contentOffset.x / itemWidth
//            let index = Int(round(proportionalOffset))
//            let safeIndex = max(0, min(dataSource.count - 1, index))
//            return safeIndex
//
//            let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
//            let index = Int(round(proportionalOffset))
//            let safeIndex = max(0, min(dataSource.count - 1, index))
//            return safeIndex
//        }
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension MyDocsViewerViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let visibleCell = self.collectionView.visibleCells.first as? MyDocViewerCollectionViewCell ,
            let index = self.collectionView.indexPath(for: visibleCell)?.row{
           // self.loadPrescription(at: index, cell: visibleCell)
            print("showing index:\(index)")
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= scrollView.contentSize.height {
                if let nxtPage = self.prescriptionList?.nextPageUrl , self.prescriptions.count - 1 == index  {
                    self.getPrescriptions(pageUrl: nxtPage)
                }
            }
            
            DispatchQueue.main.async {
                self.navigationItem.title = "BOOKING ID: \(self.prescriptions[index].publicAppointmentID ?? "")"
            }
        }
    }
}

extension MyDocsViewerViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        indexPaths.forEach{
            if self.prescriptions[$0.row].prescriptionHTMLString.isEmpty {
                self.prescriptions[$0.row].loadPrescription()
            }
        }
    }
}

extension MyDocsViewerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.prescriptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let prescription = self.prescriptions[indexPath.row]
        
//        if prescription.snapshot != nil {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifierForSnapshot, for: indexPath) as! MyDocViewerCollectionViewCell
//            cell.snapshotImage.image = prescription.snapshot
//            return cell
//        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Document", for: indexPath) as! MyDocViewerCollectionViewCell
        cell.cellDelegate = self
        cell.setupWebView(with: self.view.frame)
        if prescription.prescriptionHTMLString.isEmpty {
            cell.loadUrlInWebView(with: prescription, tag: indexPath.row)
        }else {
            cell.loadWebView(with: prescription.prescriptionHTMLString, tag: indexPath.row)
        }
        return cell
    }
}

extension MyDocsViewerViewController: PrescriptionWebViewDelegate {
    func didFinishLoadingContent(at: Int, snapshot: UIImage?) {
//        self.prescriptions[at].snapshot = snapshot
//        self.fetchDelegate?.didTakeSnapshot(at: at, snapshot: snapshot)
//        self.collectionView.reloadData()
//        print("taken snapshot:\(snapshot)")
    }
}

extension MyDocsViewerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width /  CGFloat(1), height: self.view.bounds.height / CGFloat(1))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

protocol PrescriptionWebViewDelegate {
    func didFinishLoadingContent(at:Int,snapshot:UIImage?)
}

class MyDocViewerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var snapshotImage: UIImageView!
    @IBOutlet var containerOfWebView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var webView: WKWebView!
    var isWebViewAlreadyLoaded = false
    var cellDelegate: PrescriptionWebViewDelegate?
    
    func setupWebView(with frame:CGRect) {
        if self.webView == nil {
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            
            let config = WKWebViewConfiguration()
            config.preferences = preferences
            
            self.webView = WKWebView(frame: frame, configuration: config)
            self.webView.clipsToBounds = true
            self.containerOfWebView.addSubview(self.webView)
            self.webView.uiDelegate = self
            self.webView.navigationDelegate = self
        }
    }
    
    func loadWebView(with htmlString:String, tag: Int) {
            self.webView.loadHTMLString(htmlString , baseURL: nil)
            //self.activityIndicator.stopAnimating()
    }
  
    func loadUrlInWebView(with prescription:Consultation, tag: Int) {
        var urlrequest = URLRequest(url: URL(string: "\(AppURLS.URL_Prescription)\(prescription.publicAppointmentID ?? "")" )! )
        urlrequest.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        self.webView.load(urlrequest)
        self.webView.tag = tag
    }
}

extension MyDocViewerCollectionViewCell: WKUIDelegate ,WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let scrollableSize = CGSize(width: UIScreen.main.bounds.size.width, height: webView.scrollView.contentSize.height)
        self.webView?.scrollView.contentSize = scrollableSize
        isWebViewAlreadyLoaded = true
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
//            if #available(iOS 11.0, *) {
//                let config = WKSnapshotConfiguration()
//                config.rect = webView.bounds
//                webView.takeSnapshot(with: config, completionHandler: { (image, error) in
//                    if error != nil {
//                        print("Error while taking webview snapshot")
//                    }else {
//                        self.cellDelegate?.didFinishLoadingContent(at: webView.tag, snapshot: image)
//                    }
//                })
//            } else {
//                self.cellDelegate?.didFinishLoadingContent(at: webView.tag, snapshot: webView.takeSnapshot())
//            }
            self.activityIndicator.stopAnimating()
        }
    }
}
