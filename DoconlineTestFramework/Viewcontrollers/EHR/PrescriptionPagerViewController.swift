//
//  PrescriptionPagerViewController.swift
//  DocOnline
//
//  Created by kiran kumar Gajula on 14/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class PrescriptionPagerViewController: UIPageViewController {

    var showIndex = 0
    var frame: CGRect!
    var ehrFilesVC: EHRFielsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        if let firstVC = self.getViewController(at: showIndex) {
            let viewControllers = [firstVC]
            self.setViewControllers(viewControllers, direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
    }
    
    func checForMorePrescriptions(at index:Int) {
        if App.prescriptions.count - 1 == index ,
            let ehrVC = self.ehrFilesVC,
            let prescriptionList = ehrVC.prescriptionList,
            let nextPage = prescriptionList.nextPageUrl{
            DispatchQueue.global(qos: .background).async {
              ehrVC.getPrescriptions(pageUrl: nextPage)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PrescriptionPagerViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentPageViewController = viewController as? PrescriptionDetailsViewController ,
            let prescription = currentPageViewController.prescription ,
            let currentIndex = App.prescriptions.index(where: {$0 === prescription}) {
            return getViewController(at: currentIndex + 1)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentPageViewController = viewController as? PrescriptionDetailsViewController ,
            let prescription = currentPageViewController.prescription ,
            let currentIndex = App.prescriptions.index(where: {$0 === prescription}) {
            return getViewController(at: currentIndex - 1)
        }
        return nil
    }
    
    func getViewController(at index: Int) -> UIViewController? {
        if App.prescriptions.indices.contains(index) {
            let prescription = App.prescriptions[index]
            let vc = storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_PRESCRIPTION_VC) as! PrescriptionDetailsViewController
            vc.prescription = prescription
            vc.index = index
            vc.frame = self.frame
            vc.navigationItemRef = self.navigationItem
            self.checForMorePrescriptions(at: index)
            return vc
        }
        return nil
    }
    
}
