//
//  EHRFielsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 18/09/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import JJFloatingActionButton
//import NVActivityIndicatorView
//import BSImagePicker
import SwiftMessages
//import Firebase
import MobileCoreServices
import Photos
import WebKit

struct EHR {
    var records:[String]
}

class EHRFielsViewController: UIViewController , NVActivityIndicatorViewable {

    var ehrsList = [EHR]()
    
    // This is the size of our header sections
    let SectionHeaderHeight: CGFloat = 30
    
    @IBOutlet var segmentToolBar: UIToolbar!
    
    @IBOutlet var bt_search: UIBarButtonItem!
    @IBOutlet var lb_emptyMsg: UILabel!
    @IBOutlet var ehrTableView: UITableView!
    @IBOutlet var vw_allowAccess: UIView!
    @IBOutlet var bt_removeSelectedFiles: UIButton!
    @IBOutlet var bt_switch: UISwitch!
    @IBOutlet var lb_allowAccessMsg: UILabel!
    @IBOutlet var lc_allowAccessHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var vw_picker: UIView!
    @IBOutlet var lc_pickerViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var prescriptionsNFiles: UISegmentedControl!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var vw_gradientForToolBar: UIView!
    @IBOutlet weak var noAppointmentsLabel: UILabel!
  
    
    var flotingActionButton: JJFloatingActionButton!
    
    var actionButton: JJFloatingActionButton!
    
    /// Creating UIDocumentInteractionController instance.
    let documentInteractionController = UIDocumentInteractionController()
    
    ///picker var instance declaration
    let picker = UIImagePickerController()
    
    var selectedCategoryIndex = 0
    var selectedPrescriptionIndex = 0
    
    var ehrFileData: EHRFile?
    var prescriptionList: PrescriptionsList?
    var batches = [EHRFile.Batch]() {
        didSet {
            DispatchQueue.main.async {
                self.stopAnimating()
                self.ehrTableView.reloadData()
                self.ehrTableView.isHidden = self.batches.isEmpty
            }
        }
    }
    
    var prescriptions = [Consultation]() {
        didSet {
            DispatchQueue.main.async {
                self.stopAnimating()
                self.ehrTableView.reloadData()
                self.ehrTableView.isHidden = self.prescriptions.isEmpty
            }
        }
    }
    
    var searchBar: DebounceSearchBar!
    
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        App.prescriptions.removeAll()
         /// Setting UIDocumentInteractionController delegate.
         documentInteractionController.delegate = self
         self.lb_emptyMsg.text = "At a Glance: View all your past medical records here!"
         self.showPickerView(show: false)
         self.addSearchbar(add: false)
         self.addShadowToPickerView()
        
         DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.toolBar.frame
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.toolBar.insertSubview(vwGrad, at: 0)
            self.navigationController?.navigationBar.shadowImage = UIImage()
         }
        
        self.ehrTableView.tag = 1
        self.ehrTableView.tableFooterView = UIView(frame: .zero)
        self.ehrTableView.estimatedRowHeight = 70
        self.ehrTableView.rowHeight = self.ehrTableView.tag == 1 ? UITableView.automaticDimension : 70
        self.ehrTableView.reloadData()
    
        self.addActionMenuButton()
        self.getFiles(atPage: "", isSearchEnabled: false)
    
        self.showAllowAccess(show: true )
    }
    
    func addActionMenuButton() {
        actionButton = JJFloatingActionButton()
        
        let categories = App.appSettings?.documentCategories ?? []
        
        for (index,item) in categories.enumerated() {
            if index <= 4 {
                actionButton.addItem(title: item.title, image: UIImage(named: "File")?.withRenderingMode(.alwaysTemplate)) { but in
                    self.openOptions()
                    self.selectedCategoryIndex = index
                    print("category tapped:\(item.title ?? "") dubf:\(but.titleLabel.text ?? "")  index:\(index)")
                }
            }else {
                actionButton.addItem(title: "More", image: UIImage(named:"More") ) { but in
                    self.showPickerView(show: true)
                }
                break
            }
        }
        
        actionButton.buttonColor = Theme.buttonBackgroundColor!
        actionButton.buttonImageColor = .white
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        } else {
            actionButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
        }
        actionButton.bottomAnchor.constraint(equalTo: self.vw_allowAccess.topAnchor, constant: -16).isActive = true
        actionButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    
    
    func getFiles(atPage:String,isSearchEnabled:Bool) {
        DispatchQueue.main.async {
            self.startAnimating()
            if self.searchBar != nil {
               self.searchBar.resignFirstResponder()
            }
        }
        
        EHRFile.getEHRFiles(page: atPage,isSearchEnabled: isSearchEnabled) { (success, ehrfile,response, statusCode) in
            let status = self.handleIfAnyErrorResponse(statusCode: statusCode, data: response)
            if status {
                if atPage.isEmpty || isSearchEnabled{
                    self.batches.removeAll()
                }
                
                if let filedata = ehrfile {
                    self.ehrFileData = filedata
                    self.batches += filedata.batches ?? []
                }
                print("file count \(self.batches.count)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.ehrTableView.reloadData()
                    self.ehrTableView.isHidden = self.batches.isEmpty
                    
                    if isSearchEnabled && self.batches.isEmpty {
                        self.lb_emptyMsg.text = "No records found!"
                    }
                }
                
            }else {
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.ehrTableView.reloadData()
                    self.ehrTableView.isHidden = self.batches.isEmpty
                    if isSearchEnabled && self.batches.isEmpty {
                        self.lb_emptyMsg.text = "No records found!!!!"
                    }
                }
            }
        }
    }
    
    func getPrescriptions(pageUrl:String) {
        print("Files URL:\(pageUrl)")
         DispatchQueue.main.async {
            self.startAnimating()
         }
        NetworkCall.performGet(url:pageUrl) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.ehrTableView.reloadData()
                    self.ehrTableView.isHidden = self.prescriptions.isEmpty
                }
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    self.prescriptionList = try decoder.decode(PrescriptionsList.self, from: jsonData)
                    self.prescriptions += self.prescriptionList?.consultations ?? []
                    App.prescriptions += self.prescriptionList?.consultations ?? []
                    DispatchQueue.main.async {
                        print("prescriotnjvbdjbsbv:\(self.prescriptions.count)")
                        self.prescriptions.forEach{ print("Appointmntid:\($0.publicAppointmentID ?? "") user:\($0.patient?.full_name ?? "")") }
                    }
                }catch let err {
                    print("Error while converting the ehrfile data:\(err)")
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
                self.ehrTableView.reloadData()
                self.ehrTableView.isHidden = self.prescriptions.isEmpty
                
            }
        }
    }
    
    func addSearchbar(add:Bool) {
        func initialiseSearchBar()  {
            if add {
                self.searchBar = DebounceSearchBar()
                self.searchBar.showsCancelButton = true
                self.searchBar.placeholder = "Search here..."
                
                self.searchBar.debounceInterval = 0.8
                self.searchBar.removeBackgroundImageView()
                self.searchBar.customRoundRect()
                self.searchBar.becomeFirstResponder()
                
                self.searchBar.onSearchTextUpdate = { [weak self] searchText in
                    print("search text:\(searchText) should call")
                    let replacedSearchText = searchText.replacingOccurrences(of: " ", with: "%20")
                    self?.searchText = replacedSearchText
                    DispatchQueue.global(qos: .background).async {
                        self?.getFiles(atPage: "search?q=\(replacedSearchText)", isSearchEnabled: true)
                    }
                }
                
                self.searchBar.onCancel = { [weak self]  in
                    self?.addSearchbar(add: false)
                    DispatchQueue.main.async {
                        self?.lb_emptyMsg.text = "At a Glance: View all your past medical records here!"
                    }
                    DispatchQueue.global(qos: .background).async {
                        self?.getFiles(atPage: "", isSearchEnabled: false)
                    }
                }
                
                //self.batches.removeAll()
                DispatchQueue.main.async {
                    self.ehrFileData = nil
                    self.ehrTableView.reloadData()
                }
                
            }else {
                self.searchBar = nil
            }
        }
        
        DispatchQueue.main.async {
            initialiseSearchBar()
            self.navigationItem.title = add ? "" : "Medical Records"
            self.navigationItem.titleView = add ? self.searchBar : nil
            self.navigationItem.rightBarButtonItem = add ? nil : self.bt_search
            self.navigationItem.hidesBackButton = add
        }
    }
    
   
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.ehrTableView.reloadData()
    }
    
    
    
    
    
    func openOptions() {
        if App.bookingAttachedImages.count >= 5 {
            self.didShowAlert(title: "Sorry", message: "Only 5 files are allowed to upload")
            return
        }
        
        let fileSize = App.FileSize
        let alert = UIAlertController(title: "Options", message: String(format: "Max %.0f MB / File", fileSize), preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (UIAlertAction) in
                
                if !self.checkCameraPermission()
                {
                    //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
                    self.didShowAlertForDeniedPermissions( message: "Please provide access for the camera to upload pictures")
                    return
                }
                
                if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                {
                    self.picker.sourceType = UIImagePickerController.SourceType.camera
                    self.present(self.picker, animated: true, completion: nil)
                }
            }
            
            let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { (UIAlertAction) in
                
                if !self.checkLibraryPermission()
                {
                    // self.didShowAlert(title: "Access denied!", message: "Please provide access to upload pictures from your Gallery")
                    self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
                    return
                }
                
                
                let imgPickerView = BSImagePickerViewController()
//                let mainCount = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.FILE_ATTACHMENT_LIMIT).numberValue?.intValue ?? 5
                let mainCount = 5

                if App.bookingAttachedImages.count > 0 && App.bookingAttachedImages.count < mainCount{
                    let imageCount = mainCount - App.bookingAttachedImages.count
                    imgPickerView.maxNumberOfSelections = imageCount
                }else {
                    imgPickerView.maxNumberOfSelections = mainCount
                }
                
                
                self.bs_presentImagePickerController(imgPickerView, animated: true,
                                                     select: { (asset: PHAsset) -> Void in
                                                        print("Selected: \(asset)")
                                                        
                }, deselect: { (asset: PHAsset) -> Void in
                    
                    print("Deselected: \(asset)")
                    
                }, cancel: { (assets: [PHAsset]) -> Void in
                    print("Cancel: \(assets)")
                    
                }, finish: { (assets: [PHAsset]) -> Void in
                    
                    print("Finish: \(assets)")
                    //App.ImagesArray.removeAll()
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .opportunistic
                    options.isSynchronous = true
                    options.resizeMode = .fast
                    let imageManager = PHCachingImageManager()
                    
                    self.startAnimating()
                    for asset in assets {
                        imageManager.requestImage(for: asset, targetSize: CGSize(width:CGFloat(640), height:CGFloat(480)), contentMode: .aspectFill, options: options, resultHandler: { (resultThumbnail , info) in
                            if let thumbNail = resultThumbnail {
                                let path =  info?["PHImageFileURLKey"] as? URL
                                let pathName: [String] = (path?.lastPathComponent ?? "").contains(".") ? (path?.lastPathComponent ?? "").components(separatedBy: ".") : []
                                let picData = Picture(pic: thumbNail, picCaption: pathName.isEmpty ? "" : pathName.first ?? "", type: .image)
                                picData.isNewUpload = true
                                App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                            }
                        })
                    }
                    
                    DispatchQueue.main.async {
                       self.stopAnimating()
                       self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VIEW_EDIT_RECORD, sender: self)
                    }
                    
                }, completion: nil)
            }
            
            let fileAction = UIAlertAction(title: "Pick a File", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.openDocumentPicker()
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            
            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(galleryAction)
            alert.addAction(fileAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
    }
    
    func showPickerView(show: Bool) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.lc_pickerViewBottomConstraint.constant = show ? 0 : 1000
                self.actionButton.isHidden = show
                self.pickerView.reloadAllComponents()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func showAllowAccess(show: Bool) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.bt_switch.isOn = (App.appSettings?.ehrDocumentConsent ?? 0) == 1 ? true : false
                self.bt_switch.set(width: 0.0, height: 0.0) // //changed as per ticket
                self.lc_allowAccessHeightConstraint.constant = 0.0//show ? 50 : 0 //
                self.vw_allowAccess.isHidden = true//!show  //changed as per ticket
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func addShadowToPickerView() {
        self.vw_picker.layer.shadowColor = UIColor.lightGray.cgColor
        self.vw_picker.layer.shadowOffset = CGSize(width:0,height: 2.0)
        self.vw_picker.layer.shadowRadius = 4.0
        self.vw_picker.layer.shadowOpacity = 0.5
        self.vw_picker.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.vw_picker.layer.masksToBounds = false
        self.vw_picker.layer.shadowPath = UIBezierPath(roundedRect:self.vw_picker.bounds, cornerRadius:self.vw_picker.layer.cornerRadius).cgPath
        self.vw_picker.layer.shouldRasterize = true
        self.vw_picker.layer.rasterizationScale =  UIScreen.main.scale
    }
    
    //MARK: - Document picker controller
    func openDocumentPicker(){ //doc, docx, xls, xlsx, pdf
        let docs = String(kUTTypeCompositeContent)
        let presentation = String(kUTTypePresentation)
        let spreadSheet = String(kUTTypeSpreadsheet)//kUTTypeSpreadsheet)
        let pdf = String(kUTTypePDF)
        let image = String(kUTTypeImage)
        let jpg = String(kUTTypeJPEG)
        let png = String(kUTTypePNG)
        let microsftDoc = "com.microsoft.word.doc"
        let microsftDocx = "org.openxmlformats.wordprocessingml.document"
        let microsftxls = "com.microsoft.excel.xls"
        let gsheet = "com.google.gsheet"
        //"public.item","public.data" ,"public.content"
        let importMenu = UIDocumentPickerViewController(documentTypes: [pdf,presentation,spreadSheet,docs,microsftDoc,microsftDocx,microsftxls,gsheet,png,jpg,image], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true) {
            if #available(iOS 11.0, *) {
                importMenu.allowsMultipleSelection = true
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func openFile(at section: Int ,at Index:Int) {
        if let fileData = self.batches[section].files?[Index] ,
            let url = fileData.url {
             self.storeAndShare(withURLString: url)//BaseUrl + "storage/" + url)
        }else {
            self.didShowAlert(title: "", message: "File is not available to preview.Please try again later")
        }
    }
    
    func postConsentChange(allow:Int) {
        let dic = ["document_consent": allow]
        if let data = try? JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted) {
            self.startAnimating()
            NetworkCall.performPATCH(url: AppURLS.URL_EHR_CONSENT, data: data) { (success, response, statusCode, error) in
                if let err = error {
                    print("Error while posting consent:\(err.localizedDescription)")
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        self.bt_switch.isOn = !self.bt_switch.isOn
                    }
                }else {
                    let check = self.check_Status_Code(statusCode: statusCode ?? 0, data: response)
                    
                    if check , let resp = response ,
                        let data =  resp.object(forKey: Keys.KEY_DATA ) as? [String:Any] ,
                        let consent =  data["ehr_document_consent"] as? Int {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.bt_switch.isOn =  consent == 1 ? true : false
                                App.appSettings?.ehrDocumentConsent = consent
                            }
                    }else {
                        DispatchQueue.main.async {
                            self.stopAnimating()
                            self.bt_switch.isOn = !self.bt_switch.isOn
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func allowAcccessTapped(_ sender:UISwitch) {
        self.postConsentChange(allow: sender.isOn ? 1 : 0 )
    }

    @IBAction func searchTapped(_ sender: UIBarButtonItem) {
        if self.batches.isEmpty {
            self.didShowAlert(title:"",message:"You don't have any added records to search. You can add by selecting the plus button below")
            return
        }
        self.addSearchbar(add: true)
    }
    
    @IBAction func prescriptionsNfilesSegmentTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.lb_emptyMsg.text = "At a Glance: View all your past medical records here!"
            self.ehrTableView.tag = 1
            self.prescriptions.removeAll()
            App.prescriptions.removeAll()
            self.ehrTableView.reloadData()
            self.showAllowAccess(show: true )
            
            if self.actionButton != nil {
                self.actionButton.isHidden = false
            }
            DispatchQueue.main.async {
               self.navigationItem.rightBarButtonItem = self.bt_search
            }
            self.getFiles(atPage: "", isSearchEnabled: false)
        }else if sender.selectedSegmentIndex == 1 {
            self.showAllowAccess(show: false )
            self.lb_emptyMsg.text = "At a Glance: View all your past prescriptions here!"
            self.ehrTableView.tag = 2
            self.batches.removeAll()
            self.ehrTableView.reloadData()
            if self.actionButton != nil {
                self.actionButton.isHidden = true
            }
            DispatchQueue.main.async {
                self.navigationItem.rightBarButtonItem = nil
            }
            self.getPrescriptions(pageUrl: AppURLS.URL_PRESCRIPTIONS)
        }
        print("tableview tag:\(self.ehrTableView.tag)")
    }

    @IBAction func cancelTapped(_ sender: UIButton) {
        self.showPickerView(show: false)
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        self.showPickerView(show: false)
        self.openOptions()
    }
    
    @IBAction func didTapViewSelectedFiles(_ sender: UIButton) {
          self.performSegue(withIdentifier: "Uploads", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_VIEW_EDIT_RECORD {
            if let destVC = segue.destination as? EHRUploadViewController {
                  destVC.selectedCategoryIndex = self.selectedCategoryIndex
                destVC.completionDelegate = self
            }
        }else if segue.identifier == StoryboardSegueIDS.ID_DOCS_VEIWER {
            if let destVC = segue.destination as? PrescriptionPagerViewController {
                destVC.showIndex = self.selectedPrescriptionIndex
                destVC.frame = self.view.frame
                destVC.ehrFilesVC = self
//                destVC.shouldShowIndexAt = self.selectedPrescriptionIndex
//                destVC.prescriptionList = self.prescriptionList
//                destVC.prescriptions = self.prescriptions
//                destVC.batchData = self.ehrFileData
//                destVC.batches = self.batches
//                destVC.docType = self.ehrTableView.tag
//                destVC.fetchDelegate = self
            }
        }
    }
}

extension EHRFielsViewController: UIDocumentInteractionControllerDelegate {
    
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension EHRFielsViewController: PrescriptionFetchDelegate {
    func didFinishPagination(_ prescriptionList: PrescriptionsList?, with prescriptions: [Consultation]) {
        self.prescriptionList = prescriptionList
        self.prescriptions += prescriptions
        DispatchQueue.main.async {
            self.ehrTableView.reloadData()
        }
    }
    
    func didFetchHtmlString(_ string: String, with id: String) {
        self.prescriptions.filter{ ($0.publicAppointmentID ?? "").isEqual(id) }.first?.prescriptionHTMLString = string
    }
    
    func didTakeSnapshot(at index: Int, snapshot: UIImage?) {
        if self.prescriptions.indices.contains(index) {
            self.prescriptions[index].snapshot = snapshot
        }
    }
}

extension EHRFielsViewController {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        
        let tmpDirURL = FileManager.default.temporaryDirectory.appendingPathComponent(url.lastPathComponent)
        
        if FileManager.default.fileExists(atPath: tmpDirURL.path) {
            print("File existes at path")
            DispatchQueue.main.async {
                self.share(url: tmpDirURL)
            }
        }else {
            /// START YOUR ACTIVITY INDICATOR HERE
            self.startAnimating()
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        self.didShowAlert(title: "", message: "File is not available to preview.Please try again later")
                    }
                    return
                }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? url.lastPathComponent)
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    self.stopAnimating()
                    self.share(url: tmpURL)
                }
                }.resume()
        }
    }
}

extension EHRFielsViewController: UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return App.appSettings?.documentCategories?.count ?? 0
    }
    
    // Return the title of each row in your picker ... In my case that will be the date or time slots
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return App.appSettings?.documentCategories?[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCategoryIndex = row
    }
}

extension EHRFielsViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ehrTableView.tag == 1 ? self.batches.count : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ehrTableView.tag == 1 ? (self.batches[section].files?.count ?? 0) : self.prescriptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.ehrTableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier:  "EHR" , for: indexPath) as! EHRFielsTableViewCell
            cell.setupTableViewCell(with:self.batches[indexPath.section].files?[indexPath.row])
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Prescription", for: indexPath) as! EHRFielsTableViewCell
        cell.prescriptionDetails(prescription: self.prescriptions[indexPath.row], vc: self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.ehrTableView.tag == 1 {
          self.openFile(at: indexPath.section, at: indexPath.row)
        }
        
        if self.ehrTableView.tag == 2 {
            self.selectedPrescriptionIndex = indexPath.row
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DOCS_VEIWER, sender: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.ehrTableView.tag == 1
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCell.EditingStyle.delete)
        {
            if let batchID = self.batches[indexPath.section]._id ,
                let index = self.batches[indexPath.section].files?[indexPath.row].index {
                self.startAnimating()
                NetworkCall.performDELETE(url: AppURLS.URL_EHR_FILE_UPLOAD + "\(batchID)/\(index)") { (success, response, statusCode, error) in
                    if let err = error {
                        print("Error while deleting record:\(err.localizedDescription)")
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                    }
                    
                    let check = self.check_Status_Code(statusCode: statusCode ?? 0, data: response)
                    
                    if check {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            self.batches[indexPath.section].files?.remove(at:indexPath.row)
                            if (self.batches[indexPath.section].files ?? []).isEmpty , self.batches.indices.contains(indexPath.section){
                                self.batches.remove(at: indexPath.section)
                            }
//                            let indexpath = IndexPath(item: indexPath.row, section: indexPath.section)
//                            self.ehrTableView.deleteRows(at: [indexpath], with: .left)
                            self.ehrTableView.reloadData()
                        })
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                }
            }
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.ehrTableView.tag == 1 ? UITableView.automaticDimension : 70
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  self.ehrTableView.tag == 1 ? SectionHeaderHeight : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 10, y: 0, width: tableView.bounds.width - 15, height: SectionHeaderHeight))
        view.backgroundColor = UIColor.white
        
        let stackView = UIStackView(frame: view.frame)
        stackView.axis = .horizontal
        stackView.spacing = 10
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textColor = UIColor.black
        titleLabel.adjustsFontSizeToFitWidth = true
        
        let dateLabel = UILabel()
        dateLabel.font = UIFont.boldSystemFont(ofSize: 18)
        dateLabel.textColor = UIColor.black
        dateLabel.adjustsFontSizeToFitWidth = true
        dateLabel.textAlignment = .right
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(dateLabel)
      
        if self.ehrTableView.tag == 1  {
            let category = App.appSettings?.documentCategories?.filter{ $0.id ?? 0 == self.batches[section].categoryId ?? 0 }.first
            titleLabel.text =  category?.title
            dateLabel.text = getFormattedDateAndTime(dateString: self.batches[section].createdAt ?? "")
            view.addSubview(stackView)
        }
        return self.ehrTableView.tag == 1 ? view : nil
    }
}

extension EHRFielsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DispatchQueue.main.async {
            if self.searchBar != nil && !self.batches.isEmpty{
                self.showPickerView(show: false)
                self.searchBar.resignFirstResponder()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Scrolling did end decelerating method called down")
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            if let nxtPage = self.ehrFileData?.nextPage , self.ehrTableView.tag == 1{
                self.getFiles(atPage: self.searchBar == nil ? "?page=\(nxtPage)" : "search?q=\(searchText)?page=\(nxtPage)", isSearchEnabled: false)
            }
            //self.searchBar == nil
            if let nxtPage = self.prescriptionList?.nextPageUrl , self.ehrTableView.tag == 2{
                self.getPrescriptions(pageUrl: nxtPage)
            }
        }
    }
}

extension EHRFielsViewController: ImageCrouselDelegate {
    func didCompleteAttachingImages(images: [ImagesCollectionViewCellModal]) {
        self.navigationController?.popViewController(animated: true)
        App.bookingAttachedImages.removeAll()
        self.getFiles(atPage: "", isSearchEnabled: false)
    }
}

extension EHRFielsViewController: UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker canceled")
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            DispatchQueue.main.async {
                let picData = Picture(pic: image, picCaption:  "", type: .image)
                 picData.isNewUpload = true
                App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                
                self.dismiss(animated: true, completion: {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VIEW_EDIT_RECORD, sender: self)
                    }
                })
            }
        }
    }
}

extension EHRFielsViewController : UIDocumentPickerDelegate {
//    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
//        documentPicker.delegate = self
//        present(documentPicker, animated: true, completion: nil)
//    }
//
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        let myURL = url as URL
//        print("import result1 : \(myURL.absoluteString)")
//
//    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        var arrFilesURL = [URL]()
        arrFilesURL = urls.filter {
            if let lastone = $0.absoluteString.last {
                 return !"\(lastone)".isEqual("/")
            }
            return true
        }
        
        for (index,myURL) in arrFilesURL.enumerated() {
            print("Caonverte file size:\(self.sizePerMB(url:myURL)) \n URL::\(myURL.absoluteString)")
            if index <= 4 {
                let pathComponent = myURL.lastPathComponent.components(separatedBy: ".")
                DispatchQueue.main.async {
                    let picData = Picture(fileURL:myURL.absoluteString,picCaption: pathComponent[0] ,type: .file )
                    picData.isNewUpload = true
                    App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                }
            }
        }
        
        DispatchQueue.main.async {
            if !App.bookingAttachedImages.isEmpty {
               self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VIEW_EDIT_RECORD, sender: self)
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
    }
}


class EHRFielsTableViewCell: UITableViewCell {
    @IBOutlet var iv_image: UIImageView!
    @IBOutlet var lb_name: UILabel!
    @IBOutlet var lb_time: UILabel!
    @IBOutlet var webView: WKWebView!
    @IBOutlet var containerOfWebView: UIView!
    
    //prescription
    @IBOutlet var lb_date: UILabel!
    @IBOutlet var lb_month: UILabel!
    @IBOutlet var lb_patientName: UILabel!
    @IBOutlet var lb_appointmentId: UILabel!
   
    
    func setupTableViewCell(with:EHRFile.File?) {
        self.lb_name.text = with?.title
        
        if let url = URL(string: with?.url ?? "" ) { //BaseUrl + "storage/" + (
            if url.lastPathComponent.contains("png") || url.lastPathComponent.contains("jpg") || url.lastPathComponent.contains("jpeg"){
                self.iv_image.kf.setImage(with:url , placeholder: UIImage(named:"File"), options: nil, progressBlock: nil, completionHandler: nil)
//                self.iv_image.isHidden = false
            }else{
                self.iv_image.image = UIImage(named:"File")
//                self.webView.navigationDelegate = self
//                let request = URLRequest(url: url)
//                self.webView.load(request)
            }
        }
    }
    
    func prescriptionDetails(prescription: Consultation,vc:UIViewController) {
        let dateString = prescription.updatedAt
        let convertedDate = vc.stringToDateConverter(date: dateString ?? "")
        
        let calendar = Calendar.current
        
        let month = calendar.component(.month, from: convertedDate)
        let day = calendar.component(.day, from: convertedDate)
        
        let monthName = month.getMonthName()
        
        //new UI
        self.lb_date.text = "\(day)"
        self.lb_month.text = monthName
        self.lb_patientName.text = prescription.patient?.full_name
        self.lb_appointmentId.text = "BOOKING ID: " + (prescription.publicAppointmentID ?? "")
        self.lb_time.text = dateString?.components(separatedBy: " ").last?.getTimeInAMPM()
    }
}

extension EHRFielsTableViewCell: WKNavigationDelegate {
    func webView(_ webView: WKWebView,
                 didFinish navigation: WKNavigation!) {
        self.webView.isHidden = true
        self.iv_image.isHidden = false
    }
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
