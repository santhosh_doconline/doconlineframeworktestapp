//
//  PrescriptionDetailsViewController.swift
//  DocOnline
//
//  Created by kiran kumar Gajula on 14/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import WebKit

class PrescriptionDetailsViewController: UIViewController {

    @IBOutlet weak var vw_item: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var prescription: Consultation?
    var frame: CGRect!
    var webView: WKWebView!
    var index = 0
    var navigationItemRef: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.activityIndicator.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupWebView()
        if App.prescriptions.indices.contains(index) {
            let html = App.prescriptions[index].prescriptionHTMLString
            if !html.isEmpty {
                self.activityIndicator.isHidden = true
                self.webView.loadHTMLString(html, baseURL: nil)
            }else {
                self.activityIndicator.isHidden = false
                self.loadWebViewWithUrl(prescription: App.prescriptions[index])
            }
            self.navigationItemRef.title = "BOOKING ID: \(App.prescriptions[index].publicAppointmentID ?? "")"
        }
    }
    
    func loadImage(image:UIImage) {
        let imageView = UIImageView(frame: self.vw_item.frame)
        imageView.image = image
        self.vw_item.addSubview(imageView)
        self.vw_item.bringSubviewToFront(imageView)
    }
    
    func loadWebViewWithUrl(prescription:Consultation) {
        var urlrequest = URLRequest(url: URL(string: "\(AppURLS.URL_Prescription)\(prescription.publicAppointmentID ?? "")" )! )
        urlrequest.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        self.webView.load(urlrequest)
    }
    
    func setupWebView() {
        if self.webView == nil {
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            
            let config = WKWebViewConfiguration()
            config.preferences = preferences
            self.webView = WKWebView(frame: self.view.frame, configuration: config)
            self.webView.clipsToBounds = true
            self.vw_item.addSubview(self.webView)
            self.webView.uiDelegate = self
            self.webView.navigationDelegate = self
             self.webView.translatesAutoresizingMaskIntoConstraints = false
            let topConstraint = NSLayoutConstraint(item: self.webView, attribute: .top, relatedBy: .equal, toItem: self.vw_item, attribute: .top, multiplier: 1.0, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: self.webView, attribute: .bottom, relatedBy: .equal, toItem: self.vw_item, attribute: .bottom, multiplier: 1.0, constant: 0)
            let leadingConstraint = NSLayoutConstraint(item: self.webView, attribute: .leading, relatedBy: .equal, toItem: self.vw_item, attribute: .leading, multiplier: 1.0, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: self.webView, attribute: .trailing, relatedBy: .equal, toItem: self.vw_item, attribute: .trailing, multiplier: 1.0, constant: 0)
            self.view.addConstraints([topConstraint,bottomConstraint,leadingConstraint,trailingConstraint])

        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PrescriptionDetailsViewController: WKNavigationDelegate,WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let scrollableSize = CGSize(width: UIScreen.main.bounds.size.width, height: webView.scrollView.contentSize.height)
        self.webView?.scrollView.contentSize = scrollableSize
        
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()") { (html, error) in
            App.prescriptions[self.index].prescriptionHTMLString = (html as? String) ?? ""
        }
        self.activityIndicator.stopAnimating()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5)) {
//            if #available(iOS 11.0, *) {
//                let config = WKSnapshotConfiguration()
//                config.rect = webView.bounds
//                webView.takeSnapshot(with: config, completionHandler: { (image, error) in
//                    if error != nil {
//                        print("Error while taking webview snapshot")
//                    }else {
//                        App.prescriptions[self.index].snapshot = image
//                    }
//                })
//            } else {
//                App.prescriptions[self.index].snapshot = webView.takeSnapshot()
//            }
//            self.activityIndicator.stopAnimating()
//        }
    }
}
