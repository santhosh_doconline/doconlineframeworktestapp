//
//  ConsentFormViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 08/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import BIZPopupView
import WebKit

protocol ConsentFormDelegate {
    func didUserAcceptPolicy(accept:Bool)
    func didUserAcceptedPolicyOnFollowUp(accept: Bool,appointmentId: String,followUpReason: String)
}

class ConsentFormViewController: UIViewController {

    
    @IBOutlet weak var lb_patient_name: UILabel!
    @IBOutlet weak var lb_phone_number: UILabel!
    @IBOutlet weak var lb_date_of_consent: UILabel!
    @IBOutlet weak var lb_consultation_date: UILabel!
    
    @IBOutlet weak var containerOfWebView: UIView!
    var webView: WKWebView!
    
    @IBOutlet weak var bt_decline: UIButton!
    @IBOutlet weak var bt_accept: UIButton!
    
    @IBOutlet var vw_familyDetails: UIView!
    
    @IBOutlet var lb_fpatientName: UILabel!
    @IBOutlet var lb_fpatientAttendent: UILabel!
    @IBOutlet var lb_fageOfPatient: UILabel!
    @IBOutlet var lb_dateOfConsent: UILabel!
    @IBOutlet weak var accptDeclineContainer: UIView!
    @IBOutlet weak var accptDeclineHeight: NSLayoutConstraint!
    
    var userDataModel : User!
    var selectedUser:User!
    var delegate:ConsentFormDelegate?
    var isConsultationForFamily = false
    var userAge = 0
    var strDiagnoMobile = ""
    var isFromDiagnosis = false
    
    var appointment_id = ""
    var followup_reason = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vw_familyDetails.isHidden = true
       // self.vw_familyDetails.isHidden = !self.isConsultationForFamily
        //loadHtmlFile()
        if let familymem = self.selectedUser , let ageString = familymem.age {
            let onlyAge = ageString.components(separatedBy: " ")
            userAge = Int(onlyAge[0]) ?? 0
        }
        
        if !isFromDiagnosis{
            accptDeclineContainer.isHidden = true
            accptDeclineHeight.constant = 0
        }
        
        print("selected slot \(App.selectedSlotTime)")
        print("consent slot \(App.consultationDate)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         loadHtmlFile()
    }
    
    /**
     loads user info for booking consultation with info and terms and conditions
     */
    func setUpView() {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
                
                self.userDataModel  = User.get_user_profile_default()
                
                if self.isConsultationForFamily && self.userAge >= 16{
                    self.vw_familyDetails.isHidden = false
                    let formattedName = NSMutableAttributedString()
                    formattedName.normal("PATIENT NAME : ").bold("\(App.selectedUserForBooking)")
                    self.lb_fpatientName.attributedText = formattedName
                    
                    let formattedAttendentName = NSMutableAttributedString()
                    formattedAttendentName.normal("PATIENT ATTENDANT NAME : ").bold("\(App.user_full_name)")
                    self.lb_fpatientAttendent.attributedText = formattedAttendentName
                    
                    if let familymem = self.selectedUser , let ageString = familymem.age {
                        let formattedAge = NSMutableAttributedString()
                        formattedAge.normal("AGE OF THE PATIENT : ").bold(ageString)
                        self.lb_fageOfPatient.attributedText = formattedAge
                    }
                    
                    let formattedDate = NSMutableAttributedString()
                    if(App.selectedSlotTime == "")
                    {
                        formattedDate.normal("DATE OF CONSENT : ").bold(self.getFormattedDate(dateString: App.consultationDate))
                    }
                    else
                    {
                        var date = self.getFormattedDateAndTime(dateString: App.selectedSlotTime)
                        if App.isDiagnostics
                        {
                            date = self.getISTDateAndTime(dateString: App.selectedSlotTime)
                        }
                        formattedDate.normal("DATE OF CONSENT : ").bold(date)
                    }
                    self.lb_dateOfConsent.attributedText = formattedDate
                }
                else if self.isConsultationForFamily && self.userAge <= 16{
                    self.bookingForSelfInfo()
                }else {
                   self.bookingForSelfInfo()
                }
            }else{
                
                let formattedName = NSMutableAttributedString()
                formattedName.normal("PATIENT NAME : ").bold("Myself")
                self.lb_patient_name.attributedText = formattedName
                
                self.setDateOfConsent()
                
                if(!App.selectedSlotTime.isEmpty){
                    let formattedConDate = NSMutableAttributedString()
                    formattedConDate.normal("CONSULTATION DATE : ").bold(self.getFormattedDateAndTime(dateString: App.selectedSlotTime))
                    self.lb_date_of_consent.attributedText = formattedConDate
                }else{
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MMM-yyyy"
                    let result = formatter.string(from: date)
                    let formattedResult = NSMutableAttributedString()
                    formattedResult.normal("CONSULTATION DATE : ").bold(result)
                    self.lb_date_of_consent.attributedText = formattedResult
                }
            }
        }
    }
    
    func bookingForSelfInfo() {
        self.vw_familyDetails.isHidden = true
        let formattedName = NSMutableAttributedString()
        formattedName.normal("PATIENT NAME : ").bold("\(App.selectedUserForBooking)")
        self.lb_patient_name.attributedText = formattedName
        
        let formattedPhoneNumber = NSMutableAttributedString()
        if self.userDataModel.phone!.isEmpty == true {
            if App.isDiagnostics{
                formattedPhoneNumber.normal("PHONE NUMBER : ").normal(self.strDiagnoMobile)
                self.lb_phone_number.attributedText = formattedPhoneNumber
            }
            formattedPhoneNumber.normal("PHONE NUMBER : ").normal("Update phone number")
            // self.lb_phone_number.attributedText = formattedPhoneNumber
        }else {
            formattedPhoneNumber.normal("PHONE NUMBER : ").bold(self.userDataModel.phone!)
            self.lb_phone_number.attributedText = formattedPhoneNumber
        }
        
        let formattedDate = NSMutableAttributedString()
        if(App.selectedSlotTime == "")
        {
            formattedDate.normal("CONSULTATION DATE : ").bold(self.getFormattedDate(dateString: App.consultationDate))
        }
        else
        {
            var date = self.getFormattedDateAndTime(dateString: App.selectedSlotTime)
            if App.isDiagnostics
            {
                date = self.getISTDateAndTime(dateString: App.selectedSlotTime)
            }
            formattedDate.normal("CONSULTATION DATE : ").bold(date)
        }
        self.lb_date_of_consent.attributedText = formattedDate
        
        self.setDateOfConsent()
        
    }
    
    func setDateOfConsent(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        let formattedResult = NSMutableAttributedString()
        formattedResult.normal("DATE OF CONSENT : ").bold(self.getISTDateAndTime(dateString: result))
        self.lb_consultation_date.attributedText = formattedResult
    }

    /**
       Method loads html file from project of consent form
     */
    func loadHtmlFile() {
        setUpView()
        var consentForm = ""
        if App.isDiagnostics{
            consentForm = "test-cancellation-form"
        }
        else if self.isConsultationForFamily {
            if let familymem = self.selectedUser , let ageString = familymem.age {
                let onlyAge = ageString.components(separatedBy: " ")
                let age = Int(onlyAge[0])
                if age ?? 0 < 16 {
                    consentForm = "MinorConsentForm"
                }else {
                    consentForm = "MajorFamilyMemConsentForm"
                }
            }
        }
        else {
            consentForm = "MajorConsentForm"
        }
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        getImageFromDir("MajorConsentForm.html")
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        self.webView = WKWebView(frame: self.containerOfWebView.bounds, configuration: config)
        self.webView.clipsToBounds = true
        self.webView.navigationDelegate = self
        self.containerOfWebView.addSubview(self.webView)
        let url = Bundle.main.url(forResource: consentForm, withExtension: "html")
        
        let request = URLRequest(url: url!)
        self.containerOfWebView.clipsToBounds = true
        self.webView.clipsToBounds = true
        self.webView.load(request)
    }
    
    func getImageFromDir(_ imageName: String)  {

        if let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = documentsUrl.appendingPathComponent(imageName)
            print("consent url \(fileURL)")
        }
//        return nil
    }
   
    @IBAction func acceptTapped(_ sender: UIButton) {
     //   NotificationCenter.default.post(name: NSNotification.Name(rawValue:"BookConsultationNow"), object: nil)
     //   NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StartAnimating"), object: nil)
        if appointment_id.isEmpty{
            delegate?.didUserAcceptPolicy(accept: true)
        }else{
            delegate?.didUserAcceptedPolicyOnFollowUp(accept: true, appointmentId: appointment_id, followUpReason: followup_reason)
        }
        
      //  App.popUP?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func declineTapped(_ sender: UIButton) {
        if appointment_id.isEmpty{
          delegate?.didUserAcceptPolicy(accept: false)
        }else{
            delegate?.didUserAcceptedPolicyOnFollowUp(accept: false, appointmentId: appointment_id, followUpReason: followup_reason)
        }
//         App.popUP?.dismiss(animated: true, completion: {
//            App.popUP = nil
//         })
       // self.dismiss(animated: true, completion: nil)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        App.popUP = nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ConsentFormViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let urlToOpen = navigationAction.request.url {
                UIApplication.shared.open(urlToOpen, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                decisionHandler(.cancel)
            }else {
                decisionHandler(.allow)
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
        }
    }
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
