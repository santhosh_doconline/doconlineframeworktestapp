//
//  SpeedTestViewController.swift
//  DocOnline
//
//  Created by Mac on 09/01/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
import WMGaugeView
//import NVActivityIndicatorView

class SpeedTestViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var gaugeView: WMGaugeView!
    @IBOutlet weak var maxSpeedLabel: UILabel!
    @IBOutlet weak var averageSpeedLabel: UILabel!
    @IBOutlet weak var currentSpeedLabel: UILabel!
    @IBOutlet weak var lblShow: UILabel!
    @IBOutlet weak var btnTest: UIButton!
    @IBOutlet weak var imgShow: UIImageView!

    
    @objc var measurer : NSTMeasurer!
    
    let selectorMax : String = "maxDownloadSpeed"//NSStringFromSelector(#selector(getter: measurer.maxDownloadSpeed))
    let selectorAvg : String = "averageDownloadSpeed"//NSStringFromSelector(#selector(getter: measurer.averageDownloadSpeed))
    let selectorCurrent : String = "currentDownloadSpeed"//NSStringFromSelector(#selector(getter: measurer.currentDownloadSpeed))
    
    var currentSpeed : Double = 0.0
    var maxSpeed : Double = 0.0
    
    var isCurrentSpeed = true
    
    
    /*
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        measurer = NSTMeasurer()
        let selectorMax : String = NSStringFromSelector(#selector(getter: measurer.maxDownloadSpeed))
        let selectorAvg : String = NSStringFromSelector(#selector(getter: measurer.averageDownloadSpeed))
        let selectorCurrent : String = NSStringFromSelector(#selector(getter: measurer.currentDownloadSpeed))
        
        self.measurer.addObserver(self, forKeyPath: selectorMax, options: .new, context: nil)
        self.measurer.addObserver(self, forKeyPath: selectorAvg, options: .new, context: nil)
        self.measurer.addObserver(self, forKeyPath: selectorCurrent, options: .new, context: nil)
    }*/

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        gaugeView.maxValue = 1000.0
        //gaugeView.showRangeLabels = true
        gaugeView.scaleDivisions = 10
        gaugeView.scaleSubdivisions = 5
        gaugeView.rangeValues = [200,500,1000]//[ 18.5,25,30,40,50]
        gaugeView.rangeColors = [ hexStringToUIColor(hex: "FF0000"),hexStringToUIColor(hex: "FFA500"),hexStringToUIColor(hex: "6DBE45")]
        gaugeView.rangeLabels = ["","",""]//[ "Thinness","Normal","Ex.Weight","Obese","Morbidly Obese"        ]
        
        gaugeView.unitOfMeasurement = "Kb/s"//"Kg/m\u{00B2}"
        gaugeView.unitOfMeasurementColor = hexStringToUIColor(hex: "6DBE45")
        gaugeView.showUnitOfMeasurement = true
        gaugeView.innerBackgroundStyle = WMGaugeViewInnerBackgroundStyleFlat
        gaugeView.needleStyle = WMGaugeViewNeedleStyleFlatThin
        gaugeView.showInnerBackground = false
        gaugeView.showScaleShadow = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnTest.backgroundColor = Theme.buttonBackgroundColor
        /*self.measurer.addObserver(self, forKeyPath: selectorMax, options: .new, context: nil)
        self.measurer.addObserver(self, forKeyPath: selectorAvg, options: .new, context: nil)
        self.measurer.addObserver(self, forKeyPath: selectorCurrent, options: .new, context: nil)*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /*
        self.measurer.removeObserver(self, forKeyPath: self.selectorMax)
        self.measurer.removeObserver(self, forKeyPath: self.selectorAvg)
        self.measurer.removeObserver(self, forKeyPath: self.selectorCurrent)*/
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func RGB(r:Double,g:Double,b:Double) -> UIColor {
        return UIColor(red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(g/255.0), alpha: 1.0)
    }
    func RGBValue(r:Double,g:Double,b:Double) -> UIColor {
        return UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
    }
    
    @IBAction func speedTapped(_ sender: UIButton) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        DispatchQueue.main.async {
            self.startAnimating()
        }
        self.btnTest.isEnabled = false
        currentSpeed = 0.0
        maxSpeed = 0.0
        self.measurer = NSTMeasurer()
        self.measurer.addObserver(self, forKeyPath: selectorMax, options: .new, context: nil)
        self.measurer.addObserver(self, forKeyPath: selectorAvg, options: .new, context: nil)
        self.measurer.addObserver(self, forKeyPath: selectorCurrent, options: .new, context: nil)
        
        //https://www.office.xerox.com/latest/SFTBR-04U.PDF
        //https://corpstaging.doconline.com/DocOnline_Brochure_1mb.pdf
        self.downloadFile(url: URL(string: "https://doconline-filestorage.s3.ap-south-1.amazonaws.com/WhiteLabelImages/DocOnline_Brochure_1mb.pdf")!) {
            print("download successfull")
            self.measurer.removeObserver(self, forKeyPath: self.selectorMax)
            self.measurer.removeObserver(self, forKeyPath: self.selectorAvg)
            self.measurer.removeObserver(self, forKeyPath: self.selectorCurrent)
            DispatchQueue.main.async {
                if self.maxSpeed*1000 >= 200.0
                {
                    self.lblShow.text = "Your internet bandwidth supports Audio/Video calls"
                    self.imgShow.image = UIImage(named: "video-camera")
                }
                else
                {
                    self.lblShow.text = "Your internet bandwidth is only suitable for making Audio calls"
                    self.imgShow.image = UIImage(named: "audio-speaker")
                    if self.maxSpeed*1000<50
                    {
                        self.lblShow.text = "Your internet bandwidth is not suitable for making Audio/Video calls "
                        self.imgShow.image = UIImage(named: "audio-speaker-no")
                    }
                }
                if self.maxSpeed*1000 >= 500.0
                {
                    self.lblShow.text = "Your internet bandwidth is ideal for Video/Audio internet calls"
                    self.imgShow.image = UIImage(named: "video-camera")
                }
                self.stopAnimating()
            }
        }
        //Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(gaugeUpdateTimer), userInfo: nil, repeats: false)
    }
    
    @objc func gaugeUpdateTimer() {
        print("isCurrentSpeed : ",isCurrentSpeed)
        gaugeView.setValue(isCurrentSpeed ? Float(currentSpeed*1000) : Float(maxSpeed*1000), animated: true, duration: 0.5) { (finish) in
            print("Speed : ",self.isCurrentSpeed ? Float(self.currentSpeed*1000) : Float(self.maxSpeed*1000))
            //self.currentSpeedLabel.text = "Bandwidth : " + String(format:"%.2f",self.isCurrentSpeed ? self.currentSpeed*1000 : self.maxSpeed*1000) + " Kb/s"
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "maxDownloadSpeed"//#keyPath(measurer.maxDownloadSpeed)
        {
            maxSpeed = change?[.newKey] as! Double
            print("maxSpeed : ",maxSpeed)
            //self.currentSpeedLabel.text = "Bandwidth : " + String(format:"%.2f",maxSpeed*1000) + " Kb/s"
            return
        }
        if keyPath == "averageDownloadSpeed"//#keyPath(measurer.averageDownloadSpeed)
        {
            let avgSpeed : Double = change?[.newKey] as! Double
            print("avgSpeed : ",avgSpeed)
            return
        }
        if keyPath == "currentDownloadSpeed"//#keyPath(measurer.currentDownloadSpeed)
        {
            currentSpeed = change?[.newKey] as! Double
            print("currentSpeed : ",currentSpeed)
            self.currentSpeedLabel.text = "Bandwidth : " + String(format:"%.2f",currentSpeed*1000) + " Kb/s"
            self.isCurrentSpeed = true
            Timer.scheduledTimer(timeInterval: 0.005, target: self, selector: #selector(gaugeUpdateTimer), userInfo: nil, repeats: false)
            return
        }
        
        //super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        
    }
    
    func downloadFile(url: URL, completion: @escaping () -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            
            if error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success Downloading: \(statusCode)")
                }
                DispatchQueue.main.async {
                self.isCurrentSpeed = false
                    //Timer.scheduledTimer(timeInterval: 0.005, target: self, selector: #selector(self.gaugeUpdateTimer), userInfo: nil, repeats: false)
                    self.gaugeUpdateTimer()
                    self.currentSpeedLabel.text = "Bandwidth : " + String(format:"%.2f",self.isCurrentSpeed ? self.currentSpeed*1000 : self.maxSpeed*1000) + " Kb/s"
                    self.btnTest.isEnabled = true
                }
                completion()
                
            } else {
                DispatchQueue.main.async {
                    print("Failure: %@", error?.localizedDescription)
                    self.stopAnimating()
                }
                
            }
        }
        task.resume()
    }

}

