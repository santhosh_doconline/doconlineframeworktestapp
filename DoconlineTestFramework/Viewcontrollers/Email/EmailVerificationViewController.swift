//
//  EmailVerificationViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 27/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView


protocol EmailVerificationDelegate {
    func didVerifyEmail(_ email:String,_ success:Bool)
}

class EmailVerificationViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var vw_email: UIView!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var bt_send: UIButton!
    @IBOutlet var lb_otpSentMessage: UILabel!
    @IBOutlet weak var bannerView: UIView!
    
    @IBOutlet var tf_first: UITextField!
    @IBOutlet var tf_second: UITextField!
    @IBOutlet var tf_third: UITextField!
    @IBOutlet var tf_fourth: UITextField!
    @IBOutlet var tf_five: UITextField!
    @IBOutlet var tf_six: UITextField!
  
    @IBOutlet var bt_emailVerify: UIButton!
    
    @IBOutlet var vw_enterEmail: UIView!
    @IBOutlet var vw_otpSent: UIView!
    
    @IBOutlet var LC_enterEamilCenterConstraint: NSLayoutConstraint!
    @IBOutlet var LC_otpSentCenterConstraint: NSLayoutConstraint!


    var delegate:EmailVerificationDelegate?
    var userDataModel : User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        self.activityIndicator.isHidden = true
        
        addDropShadowToButtons(button: self.bt_send)
        addDropShadowToButtons(button: self.bt_emailVerify)
        self.bt_emailVerify.setTitle("Verify OTP", for: .normal)
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        bannerView.setGradientBackground(colorTop: Theme.navigationGradientColor![0]!, colorBottom: Theme.navigationGradientColor![1]!)
//    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    ///starts activity indicator
    func startActiviy(bool:Bool) {
        if bool {
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }else {
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }
    
    ///Setting up the centered horizontal constraints on view load
    func setupView() {
        self.LC_otpSentCenterConstraint.constant = 500
        self.LC_enterEamilCenterConstraint.constant = 0
        
        if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
             self.userDataModel  = User.get_user_profile_default()
            if self.userDataModel.email != nil {
                self.tf_email.text = self.userDataModel.email!
            }
        }
    }
    
    @IBAction func sendVerificationLinkTapped(_ sender: UIButton) {
        
        let email = self.tf_email.text! as String
        
        if email.isEmpty {
            self.didShowAlert(title: "", message: "Email required!")
        }else if !validateEmail(enteredEmail: email) {
            self.didShowAlert(title: "", message: "Please enter a valid email")
        }else {
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "", message: AlertMessages.NETWORK_ERROR)
                //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            self.startAnimating()
            let data = [Keys.KEY_EMAIL : email]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
               // AppURLS.SEND_EMAIL_VERIFICATION_CODE
                NetworkCall.performPOST(url: AppURLS.EmailVerification, data: jsonData) { (success, response, statusCode, error) in
                    if let err = error {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            if let code = statusCode {
                                self.checkOnlyStatus(statusCode:code)
                            }
                        })
                    }
                    if success {
                        if let resp = response , let code = statusCode{
                            let errorStatus = self.check_Status_Code(statusCode: code , data: resp)
                            if !errorStatus {
                                print("performing error handling send Email verification code :\(resp)")
                                if let message = resp.object(forKey:Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async {
                                        self.stopAnimating()
                                        self.didShowAlert(title:"",message:message)
                                    }
                                }
                            }else
                            {
                                print("sent Email verification code :\(resp)")
                                if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary ,let otpSent = data.object(forKey: Keys.KEY_OTP_SENT) as? Bool,let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    if otpSent {
                                        DispatchQueue.main.async {
                                            self.stopAnimating()
                                            self.lb_otpSentMessage.text = message
                                            UIView.animate(withDuration: 0.5, animations: {
                                                self.LC_otpSentCenterConstraint.constant = 0
                                                self.LC_enterEamilCenterConstraint.constant = -1000
                                                self.view.layoutIfNeeded()
                                            })
                                            
                                            if sender.tag == 2 {
                                                self.didShowAlert(title: "", message: message)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }catch let err{
            print("Error converting to data error:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    self.didShowAlert(title: "Sorry!", message: "Could not process now please try again later")
                }
         }
    }
 }
    
    @IBAction func verifyOTPTapped(_ sender: UIButton) {
        let firstDigit = self.tf_first.text! as String
        let secondDigit = self.tf_second.text! as String
        let thirdDigit = self.tf_third.text! as String
        let fourthDigit = self.tf_fourth.text! as String
        let fifthDigit = self.tf_five.text! as String
        let sixthDigit = self.tf_six.text! as String
        let email = self.tf_email.text! as String
        
        if firstDigit.isEmpty || secondDigit.isEmpty || thirdDigit.isEmpty || fourthDigit.isEmpty || fifthDigit.isEmpty || sixthDigit.isEmpty{
            self.didShowAlert(title: "OTP", message: "required!")
        }else {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "", message: AlertMessages.NETWORK_ERROR)
                //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            let otpText = firstDigit + secondDigit + thirdDigit + fourthDigit + fifthDigit + sixthDigit
            var otpNumber = otpText
            if let otp = otpText.convertStringToNumberalIfDifferentLanguage() {
                otpNumber = otp.stringValue
                print("Entered OTP numer:\(otpNumber)")
            }
            
            let data = [Keys.KEY_EMAIL : email,Keys.KEY_OTP_CODE:otpNumber]
            do {
                self.startAnimating()
                
                let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                //AppURLS.VERIFY_MAIL
                NetworkCall.performPOST(url: AppURLS.EmailVerification, data: jsonData, completionHandler: { (success, response, statusCode, error) in
                    if let err = error {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            if let code = statusCode {
                                self.checkOnlyStatus(statusCode:code)
                            }
                        })
                    }
                    if success {
                        if let resp = response , let code = statusCode{
                            let errorStatus = self.check_Status_Code(statusCode: code , data: resp)
                            if !errorStatus {
                                print("performing error handling send verify email code :\(resp)")
                                if let message = resp.object(forKey:Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async {
                                        self.stopAnimating()
                                        self.didShowAlert(title:"",message:message)
                                    }
                                }
                            }else
                            {
                                print("Email verification :\(resp)")
                                if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary ,let email = data.object(forKey: Keys.KEY_EMAIL) as? String,let emailVerified = data.object(forKey: Keys.KEY_IS_VERIFIED) as? Bool,let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async {
                                        App.isEmailVerified = emailVerified
                                        UserDefaults.standard.set(emailVerified, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
                                        UserDefaults.standard.set(email, forKey: UserDefaltsKeys.KEY_USER_EMAIL)
                                        self.stopAnimating()
                                        self.delegate?.didVerifyEmail(email, true)
                                    }
                                }
                            }
                        }
                    }
                })
                
//                NetworkCall.performPATCH(url: AppURLS.EmailVerification, data: jsonData, completionHandler: { (success, response, statusCode, error) in
//                    if let err = error {
//                        DispatchQueue.main.async(execute: {
//                            self.stopAnimating()
//                            if let code = statusCode {
//                                self.checkOnlyStatus(statusCode:code)
//                            }
//                        })
//                    }
//                    if success {
//                        if let resp = response , let code = statusCode{
//                            let errorStatus = self.check_Status_Code(statusCode: code , data: resp)
//                            if !errorStatus {
//                                print("performing error handling send verify email code :\(resp)")
//                                if let message = resp.object(forKey:Keys.KEY_MESSAGE) as? String {
//                                    DispatchQueue.main.async {
//                                        self.stopAnimating()
//                                        self.didShowAlert(title:"",message:message)
//                                    }
//                                }
//                            }else
//                            {
//                                print("Email verification :\(resp)")
//                                if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary ,let email = data.object(forKey: Keys.KEY_EMAIL) as? String,let emailVerified = data.object(forKey: Keys.KEY_EMAIL_VERIFIED) as? Bool,let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String {
//                                    DispatchQueue.main.async {
//                                        App.isEmailVerified = emailVerified
//                                        UserDefaults.standard.set(emailVerified, forKey: UserDefaltsKeys.IS_EMAIL_VERIFIED)
//                                        UserDefaults.standard.set(email, forKey: UserDefaltsKeys.KEY_USER_EMAIL)
//                                        self.stopAnimating()
//                                        self.delegate?.didVerifyEmail(email, true)
//                                    }
//                                }
//                            }
//                        }
//                    }
//                })
            }catch let err{
                print("Error converting to data error:\(err.localizedDescription)")
                DispatchQueue.main.async {
                  self.stopAnimating()
                   self.didShowAlert(title: "Sorry!", message: "Could not process now please try again later")
                }
            }
        }
    }
    
    @IBAction func resendOTPTapped(_ sender: UIButton) {
        
    }
    
    
    func textFieldShouldReturnSingle(_ textField: UITextField , newString : String)
    {
        let nextTag: Int = textField.tag + 1
        
        let nextResponder: UIResponder? = textField.superview?.superview?.viewWithTag(nextTag)
        textField.text = newString
        if let nextR = nextResponder
        {
            // Found next responder, so set it.
            nextR.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension EmailVerificationViewController : UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
        
        let newLength = newString.count
        
        if newLength == 1 {
            textFieldShouldReturnSingle(textField , newString : newString)
            return false
        }
        
        
        
        return true
    }
}
