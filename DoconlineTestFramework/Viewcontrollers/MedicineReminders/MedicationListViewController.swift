//
//  MedicationListViewController.swift
//  DocOnline
//
//  Created by Doconline india on 03/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class MedicationListViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfVwGrad: NSLayoutConstraint!
    
    var arrSearch : NSArray?
    var dictSearch : NSDictionary?
    
    var searchActive : Bool = false
    var strOthers = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupNavigationBar()
        if #available(iOS 11, *) {
            //nothing to do
        }else {
            self.searchBar.scopeBarBackgroundImage = UIImage()
        }
        self.searchBar.removeBackgroundImageView()
        self.searchBar.customRoundRect()
        
        tableView.tableFooterView = UIView()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("view will disappear called")
        
        heightOfVwGrad.constant = 44.0
        
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func getSearchList(strSearch:String) {
        print("Files URL:\(strSearch)")
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        DispatchQueue.main.async {
            self.startAnimating()
        }
        let str = strSearch.withoutSpecialCharacters
        print("strSearch",str)
        let str1 = self.removeSpecialCharsFromString(text:str)
        print("strSearch1",str1)
        NetworkCall.performGet(url:"\(AppURLS.URL_MEDICATION_SEARCH)\(str1)") { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.stopAnimating()
                    //self.tableView.reloadData()
                }
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                DispatchQueue.main.async {
                    print("Search Data:\(data)")
                    self.dictSearch = data as NSDictionary
                    self.arrSearch = self.dictSearch?.allValues as NSArray?
                    print("arrSearch : ",self.arrSearch ?? "")
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
                self.tableView.reloadData()
            }
        }
    }
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return text.filter {okayChars.contains($0) }
    }
    
    func clickedOthersBtn(){
        let alertController = UIAlertController(title: "Add Medication", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Medication"
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            self.strOthers = textField.text!
            
            if self.strOthers.isEmpty != true
            {
                App.medicinReminder.med_name = self.strOthers
                App.medicinReminder.med_id = self.strOthers
                self.view.endEditing(true)
                _ = self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.didShowAlert(title: "Add Medication", message: "Please enter valid input")
            }
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension MedicationListViewController:UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        heightOfVwGrad.constant = 64.0
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        heightOfVwGrad.constant = 44.0
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        heightOfVwGrad.constant = 44.0
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.endEditing(true)
        heightOfVwGrad.constant = 44.0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        heightOfVwGrad.constant = 64.0
        let strSearch = searchText.trimmingCharacters(in: .whitespaces)
        
        if(strSearch.count >= 3){
            heightOfVwGrad.constant = 44.0
            searchActive = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.getSearchList(strSearch: strSearch)
            }
        } else if(searchText.count == 0) {
            searchActive = false
        }
        //self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard range.location == 0 else {
            return true
        }
        
        //let newString = (searchBar.text! as NSString).replacingCharacters(in: range, with: text) as NSString
        //return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
        if(text == " ")
        {
            return false
        }
        
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = text.components(separatedBy: cs).joined(separator: "")
        return (text == filtered)
    }
}

extension MedicationListViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dictSearch != nil
        {
            return (dictSearch?.count)! + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicationSearch", for: indexPath)
        if dictSearch != nil
        {
            if dictSearch?.count == indexPath.row
            {
                cell.textLabel?.text = "Others"
            }
            else
            {
                cell.textLabel?.text = arrSearch?.object(at: indexPath.row) as? String
            }
        }
        else
        {
            cell.textLabel?.text = "Others"
        }
        cell.selectionStyle = .none
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_SEARCH, sender: self)
        if dictSearch != nil
        {
            if dictSearch?.count == indexPath.row
            {
                DispatchQueue.main.async {
                    self.clickedOthersBtn()
                    self.tableView.deselectRow(at: indexPath, animated: false)
                }
            }
            else
            {
                let strName = arrSearch?.object(at: indexPath.row) as? String ?? ""
                if strName.isEmpty != true
                {
                    App.medicinReminder.med_name = strName
                    let keys = dictSearch?.allKeys(for: strName)
                    App.medicinReminder.med_id = keys?[0] as! String
                    
                    self.view.endEditing(true)
                    _ = navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.clickedOthersBtn()
        }
    }
    
}

//extension String {
//    var withoutSpecialCharacters: String {
//        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
//    }
//}
