//
//  AddDosageViewController.swift
//  DocOnline
//
//  Created by Doconline india on 03/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class AddDosageViewController: UIViewController {
    
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var btnDosage: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    ///Stores the gosage units to display in picker
    var dosageUnits = [String]()
    var selectedDosage = "dosage"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        btnDosage.layer.borderWidth = 1.0
        btnDosage.layer.borderColor = UIColor.black.cgColor
        btnDosage.layer.masksToBounds = true
        btnDosage.setTitle(selectedDosage, for: .normal)
        dosageUnits = ["Select Unit","cc","ml","gr","mg","mcg"]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.doneBtn.backgroundColor = Theme.buttonBackgroundColor
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func clickBtnDosage(_ sender: UIButton) {
        self.view.endEditing(true)
        self.showPickerInActionSheet()
    }
    
    func showPickerInActionSheet() {
        
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 250)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        pickerView.delegate = self
        pickerView.dataSource = self
        vc.view.addSubview(pickerView)
        
        let title = "Select Dosage"
        let editRadiusAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        
        let selectDosage = UIAlertAction(title: "Ok", style: .default) { (action) in
            
        }
        
        
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        editRadiusAlert.addAction(selectDosage)
        
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(editRadiusAlert, animated: true)
        
    }
    
    @IBAction func clickBtnDone(_ sender: UIButton) {
        //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_ADD_REMINDER, sender: self)
        if txtValue.text?.isEmpty != true && selectedDosage.isEmpty != true && selectedDosage != "dosage" && selectedDosage != "Select Unit"
        {
            App.medicinReminder.med_dosage = txtValue.text! + " " + selectedDosage
            
            self.view.endEditing(true)
            _ = navigationController?.popViewController(animated: true)
        }
        else
        {
            if txtValue.text?.isEmpty == true
            {
                self.didShowAlert(title: "Dosage", message: "Please Enter Value")
            }
            else
            {
                self.didShowAlert(title: "Dosage", message: "Please Select Dosage")
            }
        }
    }
    
}
extension AddDosageViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: - Picker Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.dosageUnits.count
    }
    
    // Return the title of each row in your picker ... In my case that will be the profile name or the username string
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //print("Picker view : \(pickerView.tag) ,dosageUnits :\(dosageUnits[row])")
        self.selectedDosage = self.dosageUnits[row]
        return self.selectedDosage
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //  print("Picker view : \(pickerView.tag) ,dosageUnits:\(dosageUnits[row])")
        self.selectedDosage = self.dosageUnits[row]
        btnDosage.setTitle(selectedDosage, for: .normal)
    }
}

extension AddDosageViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtValue {
            let newLength = (self.txtValue.text?.count)! + string.count - range.length
            return newLength <= 4
        }
        
        return true
    }
}
