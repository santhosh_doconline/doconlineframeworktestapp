//
//  AddMedicationReminderViewController.swift
//  DocOnline
//
//  Created by Doconline india on 03/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import SQLite
import UserNotifications

struct Category {
    let name : String
    var items : [[String:Any]]
}

struct taken_time_status {
    let time : String
    let status : Int64
}

class AddMedicationReminderViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneConstraint: NSLayoutConstraint!
    @IBOutlet weak var doneBtn: UIButton!
    
    ///picker var instance declaration
    let picker = UIImagePickerController()
    
    var sections = [Category]()
    
    var itemsA = [["Item": "Select Medication","ItemId" : "1"],["Item": "Select Dosage","ItemId" : "2"],["Item": "Select Image","ItemId" : "3"]]
    var itemsB = [["Item": "Add Time","ItemId" : "1"]]
    var itemsC = [["Item": "Select Date","ItemId" : "1"]]
    
    let reminders = Table("medicinesremainder")
    let med_name = Expression<String?>("tablet_name")
    let med_dosage = Expression<String?>("tablet_dosage")
    let med_startDate = Expression<String?>("startdate")
    let med_endDate = Expression<String?>("enddate")
    let med_addedOn = Expression<String?>("added_on")
    let med_id = Expression<String?>("tablet_id")
    
    let sun = Expression<Int64?>("sun")
    let mon = Expression<Int64?>("mon")
    let tue = Expression<Int64?>("tue")
    let wed = Expression<Int64?>("wed")
    let thu = Expression<Int64?>("thu")
    let fri = Expression<Int64?>("fri")
    let sat = Expression<Int64?>("sat")
    let med_img = Expression<String>("tablet_img")
    let id = Expression<Int64>("id")
    let med_end_enable = Expression<String?>("end_date_enabled")
    
    let tempReminder = Reminders()
    
    var imgDD : UIImage!
    
    var currentRowId : Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //,Category(name:"C", items:itemsC)]
        imgDD = UIImage(named: "order_med")!.tintedWithLinearGradientColors(colorsArr: [Theme.navigationGradientColor![0]!.cgColor, Theme.navigationGradientColor![1]!.cgColor])
        tableView.tableFooterView = UIView()
        doneBtn.backgroundColor = Theme.buttonBackgroundColor
        setTempReminder()
        
        if !App.medicinReminder.med_img.isEmpty && App.isFromEditMed {
            imgDD = loadImageFromDocumentDirectory(nameOfImage:App.medicinReminder.med_img)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var set_med_name = "Select Medication"
        var set_med_dosage = "Select Dosage"
        var set_med_startDate = ""
        var set_med_endDate = ""
        var set_med_addOn = ""
        
        if !App.medicinReminder.med_name.isEmpty {
            set_med_name = App.medicinReminder.med_name
        }
        
        if !App.medicinReminder.med_dosage.isEmpty {
            set_med_dosage = App.medicinReminder.med_dosage
        }
        
        
        if !App.medicinReminder.med_startDate.isEmpty {
            set_med_startDate = App.medicinReminder.med_startDate
            var arrStr : NSArray = set_med_startDate.components(separatedBy: " ") as NSArray
            set_med_startDate = arrStr[0] as! String
            
            if !App.medicinReminder.med_endDate.isEmpty {
                set_med_endDate = App.medicinReminder.med_endDate
                arrStr = set_med_endDate.components(separatedBy: " ") as NSArray
                set_med_endDate = arrStr[0] as! String
            }
            else
            {
                set_med_endDate = set_med_startDate
            }
            
            itemsC = [["Item": set_med_startDate + " to " + set_med_endDate,"ItemId" : "2"]]
        }
        
        if !App.medicinReminder.med_addOn.isEmpty {
            set_med_addOn = App.medicinReminder.med_addOn
            let arrStr : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
            itemsB.removeAll()
            for (index,obj) in arrStr.enumerated(){
                itemsB.append(["Item": obj as! String,"ItemId" : String(index+1)])
            }
            
            itemsB.append(["Item": "Add Time","ItemId" : String(arrStr.count+1)])
            
        }
        
        itemsA = [["Item": set_med_name,"ItemId" : "1"],["Item": set_med_dosage,"ItemId" : "2"],["Item": "Select Image","ItemId" : "3"]]
        
        sections = [Category(name:"Medication Information", items:itemsA),Category(name:"Add Reminders", items:itemsB)]//,Category(name:"Date Reminder", items:itemsC)]
        
        tableView.reloadData()
        
        var count = 0
        for obj in App.medicinReminder.med_week
        {
            if obj == 1
            {
                count = 1
            }
        }
        
        if set_med_name == "Select Medication" || set_med_startDate == "" || set_med_endDate == "" || set_med_addOn == "" || count == 0 //|| set_med_dosage == "Select Dosage"
        {
            doneConstraint.constant = 0.0
        }
        else
        {
            doneConstraint.constant = 40.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.linearBar.stopAnimation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func clickBtnDone(_ sender: UIButton) {
        //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_ADD_REMINDER, sender: self)
        if App.isFromEditMed {
            if self.updatetDB()
            {
                print("Updated record")
                removePreviousNotifications()
                self.saveImageToDocumentDirectory(image: imgDD!, imgName: App.medicinReminder.med_img)
                self.addReminderNotification()
                
                let successAlert = UIAlertController(title: "Medication", message: "Reminder Updated Successfully", preferredStyle: UIAlertController.Style.alert)
                
                let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
                    
                    self.view.endEditing(true)
                    _ = self.navigationController?.popViewController(animated: true)
                    
                }
                successAlert.addAction(successOk)
                self.present(successAlert, animated: true)
            }
        }
        else
        {
            
            if self.createDB() {
                if self.insertDB()
                {
                    print("Record inserted")
                    self.addReminderNotification()
                    self.saveImageToDocumentDirectory(image: imgDD!, imgName: App.medicinReminder.med_img)
                    //self.didShowAlert(title: "Medication", message: "Reminder Saved Successfully")
                    
                    let successAlert = UIAlertController(title: "Medication", message: "Reminder Saved Successfully", preferredStyle: UIAlertController.Style.alert)
                    
                    let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
                        
                        self.view.endEditing(true)
                        _ = self.navigationController?.popViewController(animated: true)
                        
                    }
                    successAlert.addAction(successOk)
                    self.present(successAlert, animated: true)
                }
            }
        }
        
        
    }
    
    // MARK: - DB
    
    func createDB() -> Bool {
        
        //let dbFileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("doconline.db")
        print("fileURL : ",App.dbFileURL.path)
        
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        /*
         let create_stmt = try? db.prepare("CREATE TABLE IF NOT EXISTS medicinesremainder ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `tablet_id` TEXT ,`tablet_name` TEXT, `tablet_dosage` TEXT, `sun` INTEGER DEFAULT 0, `mon` INTEGER DEFAULT 0, `tue` INTEGER DEFAULT 0, `wed` INTEGER DEFAULT 0, `thu` INTEGER DEFAULT 0, `fri` INTEGER DEFAULT 0, `sat` INTEGER DEFAULT 0, `sun_t` INTEGER DEFAULT 0, `mon_t` INTEGER DEFAULT 0, `tue_t` INTEGER DEFAULT 0, `wed_t` INTEGER DEFAULT 0, `thu_t` INTEGER DEFAULT 0, `fri_t` INTEGER DEFAULT 0, `sat_t` INTEGER DEFAULT 0, `startdate` TEXT, `enddate` TEXT, `added_on` TEXT, `sun_t_a` TEXT, `mon_t_a` TEXT, `tue_t_a` TEXT, `wed_t_a` TEXT, `thu_t_a` TEXT, `fri_t_a` TEXT, `sat_t_a` TEXT, `tablet_img` TEXT )")*/
        let create_stmt = try? db.prepare("CREATE TABLE IF NOT EXISTS medicinesremainder ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `tablet_id` TEXT ,`tablet_name` TEXT, `tablet_dosage` TEXT, `startdate` TEXT, `enddate` TEXT, `added_on` TEXT, `tablet_img` TEXT, `sun` INTEGER DEFAULT 0, `mon` INTEGER DEFAULT 0, `tue` INTEGER DEFAULT 0, `wed` INTEGER DEFAULT 0, `thu` INTEGER DEFAULT 0, `fri` INTEGER DEFAULT 0, `sat` INTEGER DEFAULT 0,`end_date_enabled` TEXT)")
        print("create : ",create_stmt ?? "")
        
        guard let table_create = try? create_stmt?.run() else{
            print("table medicin not created")
            return false
        }
        
        print("table_create_medicin : ",table_create ?? "")
        
        let create_stmt_taken = try? db.prepare("CREATE TABLE IF NOT EXISTS taken_skip ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `medicineremainderid` TEXT, `status` INTEGER DEFAULT 0, `date` TEXT, `time` TEXT )")
        print("create : ",create_stmt ?? "")
        
        guard let table_create_taken = try? create_stmt_taken?.run() else{
            print("table taken not created")
            return false
        }
        
        print("table_create_taken : ",table_create_taken ?? "")
        return true
        
    }
    
    func insertDB() -> Bool {
        
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        
        var set_med_name = ""
        var set_med_dosage = ""
        var set_med_startDate = ""
        var set_med_endDate = ""
        var set_med_addedOn = ""
        var set_med_id = ""
        var set_med_img = ""
        var set_med_end_enable = ""
        
        if !App.medicinReminder.med_name.isEmpty {
            set_med_name = App.medicinReminder.med_name
        }
        else
        {
            return false
        }
        
        if !App.medicinReminder.med_dosage.isEmpty {
            set_med_dosage = App.medicinReminder.med_dosage
        }
        /*
         else
         {
         return false
         }*/
        
        if !App.medicinReminder.med_startDate.isEmpty {
            set_med_startDate = App.medicinReminder.med_startDate
        }
        else
        {
            return false
        }
        
        if !App.medicinReminder.med_endDate.isEmpty {
            set_med_endDate = App.medicinReminder.med_endDate
        }
        else
        {
            set_med_endDate = App.medicinReminder.med_startDate
        }
        
        if !App.medicinReminder.med_addOn.isEmpty {
            set_med_addedOn = App.medicinReminder.med_addOn
        }
        else
        {
            return false
        }
        
        if !App.medicinReminder.med_id.isEmpty {
            set_med_id = App.medicinReminder.med_id
        }
        else
        {
            return false
        }
        
        if !App.medicinReminder.med_img.isEmpty {
            set_med_img = App.medicinReminder.med_img
        }
        /*
         else
         {
         return false
         }*/
        
        if !App.medicinReminder.med_end_enable.isEmpty {
            set_med_end_enable = App.medicinReminder.med_end_enable
        }
        /*
         else
         {
         return false
         }*/
        
        let med_week = App.medicinReminder.med_week
        
        let insert = reminders.insert(med_name <- set_med_name,med_dosage <- set_med_dosage,med_startDate <- set_med_startDate,med_endDate <- set_med_endDate,med_addedOn <- set_med_addedOn,med_id <- set_med_id, med_img <- set_med_img,sun <- med_week[0],mon <- med_week[1],tue <- med_week[2],wed <- med_week[3],thu <- med_week[4],fri <- med_week[5],sat <- med_week[6],med_end_enable <- set_med_end_enable)
        guard let rowid = try? db.run(insert) else{
            print("Not inserted")
            return false
        }
        
        print("row inserted",rowid)
        currentRowId = rowid
        
        return true
    }
    
    func updatetDB() -> Bool {
        
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        
        let med_week = App.medicinReminder.med_week
        /*
         let med_taken = App.medicinReminder.med_taken
         let med_taken_at = App.medicinReminder.med_taken_at
         let temp_taken_at = tempReminder.med_taken_at*/
        
        print("tempReminder - : ",tempReminder.obj_description)
        print("App.medicinReminder - : ",App.medicinReminder.obj_description)
        let reminder_record = reminders.filter(id == tempReminder.id)
        
        let update = reminder_record.update(
            [med_name <- med_name.replace(tempReminder.med_name, with: App.medicinReminder.med_name),
             med_id <- med_id.replace(tempReminder.med_id, with: App.medicinReminder.med_id),
             med_dosage <- App.medicinReminder.med_dosage,
             med_startDate <- med_startDate.replace(tempReminder.med_startDate, with: App.medicinReminder.med_startDate),
             med_endDate <- med_endDate.replace(tempReminder.med_endDate, with: App.medicinReminder.med_endDate),
             med_addedOn <- med_addedOn.replace(tempReminder.med_addOn, with: App.medicinReminder.med_addOn),
             sun <- med_week[0],mon <- med_week[1],tue <- med_week[2],
             wed <- med_week[3],thu <- med_week[4],fri <- med_week[5],
             sat <- med_week[6],
             med_img <- App.medicinReminder.med_img,
             med_end_enable <- med_end_enable.replace(tempReminder.med_end_enable, with: App.medicinReminder.med_end_enable)
            ])
        
        print("update query",update)
        
        guard let rowid2 = try? db.run(update) else{
            print("Not updated 2")
            return false
        }
        
        print("updated 2",rowid2)
        
        return true
    }
    
    func setTempReminder() {
        tempReminder.id = App.medicinReminder.id
        tempReminder.med_id = App.medicinReminder.med_id
        tempReminder.med_name = App.medicinReminder.med_name
        tempReminder.med_dosage = App.medicinReminder.med_dosage
        tempReminder.med_startDate = App.medicinReminder.med_startDate
        tempReminder.med_endDate = App.medicinReminder.med_endDate
        tempReminder.med_addOn = App.medicinReminder.med_addOn
        tempReminder.med_week = App.medicinReminder.med_week
        tempReminder.med_taken = App.medicinReminder.med_taken
        tempReminder.med_taken_at = App.medicinReminder.med_taken_at
        tempReminder.med_img = App.medicinReminder.med_img
        tempReminder.med_end_enable = App.medicinReminder.med_end_enable
    }
    
    // MARK: - Add Notification
    
    func scheduleMedicationRemainder(timeInterVal : DateComponents, weekInt : Int, addTime:String, startTime:String, endTime:String, endEnable:String) {
        
        if !App.isFromEditMed
        {
            App.medicinReminder.id = currentRowId
        }
        let med_week = App.medicinReminder.med_week
        setupActionsForNotification()
        print("\(#function) called--")
        let content = UNMutableNotificationContent()
        content.title = "Medicine reminder"
        content.body = "It's time to take your medicine \(App.medicinReminder.med_name). Thank You"
        content.categoryIdentifier = "Remainder"
        //content.userInfo = ["medicin_id":"MedicationAlert\(App.medicinReminder.id)\(App.medicinReminder.med_id)\(weekInt)"]
        
        content.userInfo = ["medicine_id":App.medicinReminder.id,"medicine_time":addTime,"medicine_start":startTime,"medicine_end":endTime,"medicine_img":App.medicinReminder.med_img,"medicine_dosage":App.medicinReminder.med_dosage,"medicine_tab_id":App.medicinReminder.med_id,"medicine_name":App.medicinReminder.med_name,"medicine_sun":med_week[0],"medicine_mon":med_week[1],"medicine_tue":med_week[2],"medicine_wed":med_week[3],"medicine_thu":med_week[4],"medicine_fri":med_week[5],"medicine_sat":med_week[6],"medicine_end_enable":endEnable]
        content.sound = UNNotificationSound.default
        
        // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterVal, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: timeInterVal, repeats: (endEnable == "0") ? true : false)
        
        //let requestIdentifier = "MedicationAlert\(App.medicinReminder.id)\(App.medicinReminder.med_id)\(weekInt)"
        print("added notification : ","MedicationAlert \(App.medicinReminder.id) \(weekInt) \(addTime)")
        let requestIdentifier = "MedicationAlert \(App.medicinReminder.id) \(weekInt) \(addTime)"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
                print("Notification scheduling error:\(String(describing: error?.localizedDescription))")
            }else{
                print("medicin notification scheduled successfully")
            }
        }
        //App.selectedSlotTime = ""
    }
    
    func setupActionsForNotification() {
        let dismissAction = UNNotificationAction(identifier: NotificationActionIdentifiers.DismissAction , title: "Dismiss", options: UNNotificationActionOptions.destructive)
        let viewAction = UNNotificationAction(identifier: NotificationActionIdentifiers.ViewAction , title: "View", options: UNNotificationActionOptions.foreground)
        let notificationCategory = UNNotificationCategory(identifier: NotificationCategoryIdentifiers.AlertCategory , actions: [dismissAction,viewAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([notificationCategory])
    }
    
    func addReminderNotification()
    {
        let reminder : Reminders = App.medicinReminder
        if reminder.med_startDate.isEmpty != true
        {
            if App.medicinReminder.med_endDate.isEmpty {
                App.medicinReminder.med_endDate = App.medicinReminder.med_startDate
            }
            let set_med_start = App.medicinReminder.med_startDate
            let set_med_end = App.medicinReminder.med_endDate
            let set_med_end_enable = App.medicinReminder.med_end_enable
            let arrStrStart : NSArray = set_med_start.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_end.components(separatedBy: ",") as NSArray
            let arrStrEndEnable : NSArray = set_med_end_enable.components(separatedBy: ",") as NSArray
            
            for (index,objStart) in arrStrStart.enumerated(){
                //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
                let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate: self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss"))
                
                print("arrDates : ",arrDates.debugDescription)
                
                for (index1,dateTemp) in arrDates.enumerated(){
                    let week = Calendar.current.component(.weekday, from: dateTemp)
                    
                    let set_med_addOn = App.medicinReminder.med_addOn
                    var set_addOn_time = ""
                    let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
                    //for obj in arrStr1{
                    let strObj = (arrStr1.count == 1) ? arrStr1[0] as! String : arrStr1[index] as! String
                    set_addOn_time = strObj
                    let trimmedString = set_addOn_time.trimmingCharacters(in: .whitespaces)
                    
                    let strObjEnd = (arrStrEnd.count == 1) ? arrStrEnd[0] as! String : arrStrEnd[index] as! String
                    let strObjEndEnable = (arrStrEndEnable.count == 1) ? arrStrEndEnable[0] as! String : arrStrEndEnable[index] as! String
                    
                    if reminder.med_week[week-1] == 1 {
                        print("dateTemp : ",dateTemp)
                        print("timeTemp : ",self.getDateFromTime(time: set_addOn_time))
                        let calendar = Calendar(identifier: .indian)
                        let components = calendar.dateComponents(in: .current, from: dateTemp)
                        let components1 = calendar.dateComponents(in: .current, from: self.getDateFromTime(time: set_addOn_time))
                        
                        print("Day : ",components.day!,"Month : ",components.month!,"hour : ",components1.hour!,"Minutes : ",components1.minute!)
                        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components1.hour, minute: components1.minute)
                        //let newComponents1 = DateComponents(calendar: calendar, timeZone: .current, hour: components1.hour, minute: components1.minute)
                        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: combineDate(currentDate: dateTemp, currentTime: set_addOn_time))
                        
                        self.scheduleMedicationRemainder(timeInterVal: (strObjEndEnable == "0") ? triggerDaily : newComponents, weekInt: index1+1, addTime: trimmedString, startTime: objStart as! String, endTime: strObjEnd, endEnable: strObjEndEnable)
                    }
                    
                    
                    //}
                    /*
                     if arrStr1.count == 0
                     {
                     let trimmedString = set_med_addOn.trimmingCharacters(in: .whitespaces)
                     if reminder.med_week[week-1] == 1 {
                     print("dateTemp : ",dateTemp)
                     print("timeTemp : ",self.getDateFromTime(time: reminder.med_addOn))
                     let calendar = Calendar(identifier: .indian)
                     let components = calendar.dateComponents(in: .current, from: dateTemp)
                     let components1 = calendar.dateComponents(in: .current, from: self.getDateFromTime(time: reminder.med_addOn))
                     print("Day : ",components.day!,"Month : ",components.month!,"hour : ",components1.hour!,"Minutes : ",components1.minute!)
                     let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components1.hour, minute: components1.minute)
                     
                     self.scheduleMedicationRemainder(timeInterVal: newComponents, weekInt: index+1, addTime: trimmedString)
                     
                     }
                     }*/
                    
                }
            }
            
            
            
        }
    }
    
    func combineDate(currentDate:Date,currentTime:String) -> Date
    {
        let arrStr : NSArray = currentTime.components(separatedBy: " ") as NSArray
        let time = arrStr[0] as! String
        let time_a = arrStr[1] as! String
        
        let strDate = self.dateString(date:currentDate, format:"yyyy-MM-dd HH:mm:ss")
        let date1 = self.UTCToLocalDate(date: strDate)
        let set_taken_date : String! = self.dateString(date:date1.dateOnly(), format:"yyyy-MM-dd HH:mm:ss")
        let arrSep = set_taken_date.components(separatedBy: " ") as NSArray
        
        let dateTemp = self.dateStringToDate(string: "\(arrSep[0]) \(time):00 \(time_a)", format: "yyyy-MM-dd hh:mm:ss a")
        let strDate1 = self.dateString(date:dateTemp, format:"yyyy-MM-dd HH:mm:ss")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: strDate1)
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss a"
        let str = dateFormatter.string(from: date!)
        print("12 hour formatted Date:",str)
        return dateFormatter.date(from: str)!
    }
    
    func compareDate(dateInitial:Date, dateFinal:Date) -> Bool {
        let order = Calendar.current.compare(dateInitial, to: dateFinal, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    
    func removePreviousNotifications()
    {
        let reminder : Reminders = tempReminder
        if reminder.med_startDate.isEmpty != true
        {
            if tempReminder.med_endDate.isEmpty {
                tempReminder.med_endDate = tempReminder.med_startDate
            }
            
            let set_med_start = reminder.med_startDate
            let set_med_end = reminder.med_endDate
            let arrStrStart : NSArray = set_med_start.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_end.components(separatedBy: ",") as NSArray
            
            for (index,objStart) in arrStrStart.enumerated(){
                
                //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
                let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate: self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss"))
                
                print("arrDates to remove: ",arrDates.debugDescription)
                
                for (index1,dateTemp) in arrDates.enumerated(){
                    let week = Calendar.current.component(.weekday, from: dateTemp)
                    if reminder.med_week[week-1] == 1 {
                        let center = UNUserNotificationCenter.current()
                        center.removeAllDeliveredNotifications()
                        //center.removePendingNotificationRequests(withIdentifiers: ["MedicationAlert\(reminder.id)\(reminder.med_id)\(index+1)"])
                        let set_med_addOn = tempReminder.med_addOn
                        var set_addOn_time = ""
                        let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
                        //for obj in arrStr1{
                        let strObj = (arrStr1.count == 1) ? arrStr1[0] as! String : arrStr1[index] as! String
                        set_addOn_time = strObj
                        let trimmedString = set_addOn_time.trimmingCharacters(in: .whitespaces)
                        print("remove previous notification : ","MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)")
                        center.removePendingNotificationRequests(withIdentifiers: ["MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)"])
                        //}
                        /*
                         if arrStr1.count == 0
                         {
                         let trimmedString = set_med_addOn.trimmingCharacters(in: .whitespaces)
                         center.removePendingNotificationRequests(withIdentifiers: ["MedicationAlert\(reminder.id)\(index+1)\(trimmedString)"])
                         }*/
                        
                        
                    }
                    
                }
            }
            
        }
    }
    
    func getDateFromTime(time:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone.current
        
        return dateFormatter.date(from: time)!
    }
    
    // MARK: - Add Image
    
    func attachPhotos() {
        
        let alert = UIAlertController(title: "Options", message:"Please Upload File" , preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (UIAlertAction) in
            
            if !self.checkCameraPermission()
            {
                //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
                self.didShowAlertForDeniedPermissions( message: "Please provide access for the camera to upload pictures")
                return
            }
            
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
            {
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.picker, animated: true, completion: nil)
            }
        }
        
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { (UIAlertAction) in
            
            if !self.checkLibraryPermission()
            {
                self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
                return
            }
            
            
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary))
            {
                self.picker.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.present(self.picker, animated: true, completion: nil)
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func saveImageToDocumentDirectory(image: UIImage,imgName:String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = imgName // name of the image to be saved
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        if let data = image.jpegData(compressionQuality: 1.0){//,!FileManager.default.fileExists(atPath: fileURL.path){
            do {
                try data.write(to: fileURL)
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        }
    }
    
    func loadImageFromDocumentDirectory(nameOfImage : String) -> UIImage {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(nameOfImage)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if(image == nil)
            {
                return UIImage.init(named: "order_med.png")!
            }
            return image!
        }
        return UIImage.init(named: "order_med.png")!
    }
    
}

extension AddMedicationReminderViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section].name
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let items = self.sections[section].items
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let items = self.sections[indexPath.section].items
        let item = items[indexPath.row]
        print(item["ItemId"] as? String ?? "")
        
        if indexPath.row == 2 && indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "addMedicationImage", for: indexPath) as! AddReminderTableCell
            cell.lblName.text = item["Item"] as? String ?? ""
            cell.imgTaken.image = imgDD//decodedimage
            return cell
        }
        //.tintedWithLinearGradientColors(colorsArr: [Theme.navigationGradientColor![0]!.cgColor, Theme.navigationGradientColor![1]!.cgColor]
        let cell = tableView.dequeueReusableCell(withIdentifier: "addMedication", for: indexPath)
        cell.textLabel?.text = item["Item"] as? String ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && indexPath.section == 0 {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_SEARCH, sender: self)
        }
        else if indexPath.row == 1 && indexPath.section == 0{
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_DOSAGE, sender: self)
        }
        else if indexPath.row == 2 && indexPath.section == 0{
            DispatchQueue.main.async {
                self.attachPhotos()
                // Deselect the row
                self.tableView.deselectRow(at: indexPath, animated: false)
            }
        }
        else if indexPath.row == 0 && indexPath.section == 2{
            App.isTime = false
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_MEDICATION_TIME, sender: self)
        }
            //else if indexPath.row == 0 && indexPath.section == 1{
        else if indexPath.section == 1{
            
            App.timeIndex = indexPath.row
            
            let items = self.sections[indexPath.section].items
            if indexPath.row == items.count - 1
            {
                App.timeIndex = -1
            }
            
            App.isTime = true
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_MEDICATION_TIME, sender: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

class AddReminderTableCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgTaken: UIImageView!
    
}

extension AddMedicationReminderViewController : UNUserNotificationCenterDelegate {
    /*
     func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
     completionHandler([.alert,.sound])
     let userInfo = notification.request.content.userInfo as NSDictionary
     print("\(userInfo)")
     }*/
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        print("User : \(userInfo)")
        let identifier:String = userInfo.value(forKey: "medicin_id") as! String
        //let reminder:Reminders = userInfo.value(forKey: "reminder_obj") as! Reminders
        
        switch response.actionIdentifier {
        case "DismissAction":
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identifier])
            
        case "ViewAction":
            // UIApplication.shared.cancelAllLocalNotifications()
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identifier])
            //App.medicinReminder = reminder
            //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_DETAILS, sender: self)
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_REMINDER_SEGUE, sender: self)
            
        default:
            print("Something wrong in notification action")
        }
        print("LocalNotifiResponse:\(response)")
    }
    
    
}

extension AddMedicationReminderViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        let imgData: NSData = NSData(data: ( info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage).jpegData(compressionQuality: 0.4)!)
        let imageSize: Int = imgData.length
        _ = Double(imageSize) / 1024.0 / 1024.0
        print("size of image in MB: %f ", Double(imageSize) / 1024.0 / 1024.0)
        
        let img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        imgDD = img
        
        //let imageData:Data = img.pngData()!
        //let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if !App.isFromEditMed
        {
            App.medicinReminder.med_img = "Image\(App.currentDBId+1)" //strBase64
        }
        else{
            App.medicinReminder.med_img = "Image\(App.selectedDBId)"
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker canceled")
        self.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
