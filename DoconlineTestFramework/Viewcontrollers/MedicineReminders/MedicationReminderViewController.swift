//
//  MedicationReminderViewController.swift
//  DocOnline
//
//  Created by Doconline india on 29/11/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import SQLite
import UserNotifications

class MedicationReminderViewController: UIViewController {
    
    
    let dayHeaderView = DayHeaderView()
    var state = DayViewState()
    
    @IBOutlet weak var tableView: UITableView!
    
    let reminders = Table("medicinesremainder")
    let med_name = Expression<String?>("tablet_name")
    let med_dosage = Expression<String?>("tablet_dosage")
    let med_startDate = Expression<String?>("startdate")
    let med_endDate = Expression<String?>("enddate")
    let med_addedOn = Expression<String?>("added_on")
    let med_id = Expression<String?>("tablet_id")
    let sun = Expression<Int64?>("sun")
    let mon = Expression<Int64?>("mon")
    let tue = Expression<Int64?>("tue")
    let wed = Expression<Int64?>("wed")
    let thu = Expression<Int64?>("thu")
    let fri = Expression<Int64?>("fri")
    let sat = Expression<Int64?>("sat")
    let med_end_enable = Expression<String?>("end_date_enabled")
    
    let taken = Table("taken_skip")
    let taken_date = Expression<String?>("date")
    let taken_time = Expression<String?>("time")
    let taken_state = Expression<Int64?>("status")
    let taken_reminder_id = Expression<Int64?>("medicineremainderid")
    let reminder_time = Expression<String?>("reminder_time")
    
    let med_img = Expression<String>("tablet_img")
    let id = Expression<Int64>("id")
    
    var arrReminders = [Reminders]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        dayHeaderView.delegate = self
        self.view.addSubview(dayHeaderView)
        dayHeaderView.anchorAndFillEdge(.top, xPad: 0, yPad: 0, otherSize: 88.0)
        dayHeaderView.state = state
        let style = CalendarStyle()
        style.header.backgroundColor = UIColor.clear
        style.header.daySymbols.weekDayColor = UIColor.white
        style.header.daySymbols.weekendColor = UIColor.white
        style.header.swipeLabel.textColor = UIColor.white
        style.header.daySelector.weekendTextColor = UIColor.black
        
        dayHeaderView.updateStyle(style.header)
        
        setupNavigationBar()
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.dayHeaderView.frame
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.dayHeaderView.insertSubview(vwGrad, at: 0)
        }
        
        tableView.tableFooterView = UIView()
        
        /*
         for subviews in dayHeaderView.subviews
         {
         print(subviews.description)
         }*/
        
        App.selectedDate = currentDate().dateOnly()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        App.reminderList.removeAll()
        App.weekReminder.removeAll()
        App.takenReminder.removeAll()
        App.medicinReminder = Reminders()
        arrReminders = [Reminders]()
        if self.createDB() {
            self.fetchDB(date: App.selectedDate)
        }
        
        getPendingNotifications()
        
        //self.removePastDateandTimeNotifications()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.linearBar.stopAnimation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    func getPendingNotifications() {
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        center.getPendingNotificationRequests(completionHandler: { requests in
            for request in requests {
                print("PendingNotification : ",request)
            }
        })
    }
    
    @IBAction func clickAddReminder(_ sender: UIButton) {
        App.isFromEditMed = false
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_ADD_REMINDER, sender: self)
    }
    
    func currentDate() -> Date {
        print("current date :",Date().debugDescription)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        let strDate = dateFormatter.string(from: Date())
        print("current date in string :",strDate)
        let yourDate = dateFormatter.date(from: strDate)
        return yourDate!
    }
    
    func createDB() -> Bool {
        
        //let dbFileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("doconline.db")
        print("fileURL : ",App.dbFileURL.path)
        
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        /*
         let create_stmt = try? db.prepare("CREATE TABLE IF NOT EXISTS medicinesremainder ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `tablet_id` TEXT ,`tablet_name` TEXT, `tablet_dosage` TEXT, `sun` INTEGER DEFAULT 0, `mon` INTEGER DEFAULT 0, `tue` INTEGER DEFAULT 0, `wed` INTEGER DEFAULT 0, `thu` INTEGER DEFAULT 0, `fri` INTEGER DEFAULT 0, `sat` INTEGER DEFAULT 0, `sun_t` INTEGER DEFAULT 0, `mon_t` INTEGER DEFAULT 0, `tue_t` INTEGER DEFAULT 0, `wed_t` INTEGER DEFAULT 0, `thu_t` INTEGER DEFAULT 0, `fri_t` INTEGER DEFAULT 0, `sat_t` INTEGER DEFAULT 0, `startdate` TEXT, `enddate` TEXT, `added_on` TEXT, `sun_t_a` TEXT, `mon_t_a` TEXT, `tue_t_a` TEXT, `wed_t_a` TEXT, `thu_t_a` TEXT, `fri_t_a` TEXT, `sat_t_a` TEXT, `tablet_img` TEXT )")*/
        let create_stmt = try? db.prepare("CREATE TABLE IF NOT EXISTS medicinesremainder ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `tablet_id` TEXT ,`tablet_name` TEXT, `tablet_dosage` TEXT, `startdate` TEXT, `enddate` TEXT, `added_on` TEXT, `tablet_img` TEXT, `sun` INTEGER DEFAULT 0, `mon` INTEGER DEFAULT 0, `tue` INTEGER DEFAULT 0, `wed` INTEGER DEFAULT 0, `thu` INTEGER DEFAULT 0, `fri` INTEGER DEFAULT 0, `sat` INTEGER DEFAULT 0, `end_date_enabled` TEXT)")
        print("create : ",create_stmt ?? "")
        
        guard let table_create = try? create_stmt?.run() else{
            print("table medicin not created")
            return false
        }
        
        print("table_create_medicin : ",table_create ?? "")
        
        let create_stmt_taken = try? db.prepare("CREATE TABLE IF NOT EXISTS taken_skip ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `medicineremainderid` TEXT, `status` INTEGER DEFAULT 0, `date` TEXT, `time` TEXT, `reminder_time` TEXT )")
        print("create : ",create_stmt ?? "")
        
        guard let table_create_taken = try? create_stmt_taken?.run() else{
            print("table taken not created")
            return false
        }
        
        print("table_create_taken : ",table_create_taken ?? "")
        return true
        
    }
    
    func fetchDB(date: Date)
    {
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return
        }
        App.reminderList.removeAll()
        
        let strDate = self.dateString(date:date, format:"yyyy-MM-dd HH:mm:ss")
        let date1 = self.UTCToLocalDate(date: strDate)
        let set_taken_date : String! = self.dateString(date:date1.dateOnly(), format:"yyyy-MM-dd HH:mm:ss")
        let arrSep = set_taken_date.components(separatedBy: " ") as NSArray
        print("date1.dateOnly() while fetching: ","\(set_taken_date ?? "")")
        
        let reminders = Table("medicinesremainder")
        for med_reminder in try! db.prepare(reminders) {
            print("id: \(med_reminder[id]), name: ", med_reminder[med_name] ?? "","enable: ",med_reminder[med_end_enable] ?? "")
            
            let med_week = [med_reminder[sun]!,med_reminder[mon]!,med_reminder[tue]!,med_reminder[wed]!,med_reminder[thu]!,med_reminder[fri]!,med_reminder[sat]!]
            var med_taken : Int64 = 0
            
            var med_taken_at = [taken_time_status]()
            
            let count : Int64 = try! db.scalar("SELECT count(*) FROM taken_skip") as! Int64
            if count != 0
            {
                print("select take_Skip : ","SELECT status FROM taken_skip WHERE medicineremainderid = \(med_reminder[id]) AND date = '\(arrSep[0])'")
                for row in try! db.prepare("SELECT status,reminder_time FROM taken_skip WHERE medicineremainderid = \(med_reminder[id]) AND date = '\(arrSep[0])'")
                {
                    print("row : ",row)
                    let status = row[0] as! Int64
                    let time = row[1] as! String
                    if status == 1
                    {
                        med_taken = 1
                        med_taken_at.append(taken_time_status(time:time,status:status))
                    }
                    else if status == 2
                    {
                        med_taken = 2
                        med_taken_at.append(taken_time_status(time:time,status:status))
                    }
                }
            }
            let reminder = Reminders(id:med_reminder[id], med_id: med_reminder[med_id]!, med_name: med_reminder[med_name]!, med_dosage: med_reminder[med_dosage]!, med_startDate: med_reminder[med_startDate]!, med_endDate: med_reminder[med_endDate]!, med_addOn: med_reminder[med_addedOn]!, med_week:med_week, med_taken: med_taken, med_taken_at: med_taken_at, med_img:med_reminder[med_img],timeIndex:-1,med_end_enable:med_reminder[med_end_enable]!)
            App.reminderList.append(reminder)
        }
        
        if App.reminderList.count != 0 {
            for (index,obj) in App.reminderList.enumerated()
            {
                print("reminderList - \(index) : ",obj.obj_description)
            }
            App.currentDBId = App.reminderList.count
            self.checkWithStartEndDates(date: date)
        }
        tableView.reloadData()
    }
    
    func checkWithStartEndDates(date: Date)
    {
        //EEEE, d MMMM yyyy
        arrReminders.removeAll()
        for remi in App.reminderList {
            
            let reminder : Reminders = remi
            if reminder.med_startDate.isEmpty != true
            {
                let set_med_startDate = reminder.med_startDate
                var set_med_endDate = reminder.med_endDate
                let set_med_end_enable = reminder.med_end_enable
                
                if reminder.med_endDate.isEmpty {
                    set_med_endDate = set_med_startDate
                }
                
                let arrStrStart : NSArray = set_med_startDate.components(separatedBy: ",") as NSArray
                let arrStrEnd : NSArray = set_med_endDate.components(separatedBy: ",") as NSArray
                let arrStrEndEnable : NSArray = set_med_end_enable.components(separatedBy: ",") as NSArray
                
                for (index,objStart) in arrStrStart.enumerated(){
                    let set_end_date = arrStrEnd[index] as! String
                    let set_start_date = arrStrStart[index] as! String
                    let arrStrDates : NSArray = set_end_date.components(separatedBy: " ") as NSArray
                    let arrStrDates1 : NSArray = set_start_date.components(separatedBy: " ") as NSArray
                    let dateEnd = self.dateStringToDate(string: "\(arrStrDates[0]) \(arrStrDates1[1])", format: "yyyy-MM-dd HH:mm:ss")
                    let strDate_end = self.dateString(date:dateEnd, format:"yyyy-MM-dd HH:mm:ss")
                    let date_end = self.UTCToLocalDate(date: strDate_end)
                    
                    //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
                    let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate:date_end)
                    //self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss")
                    
                    print("arrDates : ",arrDates.debugDescription)
                    
                    var strDate = ""
                    if dayHeaderView.swipeLabelView.labels.first!.text?.isEmpty != true {
                        //strDate = self.dateString(date:self.dateStringToDate(string: dayHeaderView.swipeLabelView.labels.first!.text ?? "", format: "EEEE, d MMMM yyyy"), format:"yyyy-MM-dd HH:mm:ss")
                        strDate = self.dateString(date:date, format:"yyyy-MM-dd HH:mm:ss")
                        var firstDate : Date = Date()
                        for (count,dateTemp) in arrDates.enumerated(){
                            let date1 = self.UTCToLocalDate(date: strDate)
                            print("date1 : ",date1)
                            print("date2 : ",dateTemp)
                            if count == 0 { firstDate = dateTemp }
                            print("date3 : ",firstDate)
                            
                            let components = Calendar.current.dateComponents([.year, .month, .day], from: date1)
                            let components1 = Calendar.current.dateComponents([.year, .month, .day], from: dateTemp)
                            let components2 = Calendar.current.dateComponents([.year, .month, .day], from: firstDate)
                            
                            let day = components.day
                            let month = components.month
                            let year = components.year
                            
                            let day1 = components1.day
                            let month1 = components1.month
                            let year1 = components1.year
                            
                            let day2 = components2.day
                            let month2 = components2.month
                            let year2 = components2.year
                            
                            let strObjEndEnable = (arrStrEndEnable.count == 1) ? arrStrEndEnable[0] as! String : arrStrEndEnable[index] as! String
                            
                            if (day == day1 && month == month1 && year == year1) || (strObjEndEnable == "0" &&
                                (Int(day!) >= Int(day2!) || (Int(month!) > Int(month2!) || Int(year!) > Int(year2!))) &&
                                (Int(month!) >= Int(month2!) || Int(year!) > Int(year2!)) &&
                                Int(year!) >= Int(year2!))
                            {
                                //let week = Calendar.current.component(.weekday, from: dateTemp)
                                if remi.med_week[dayHeaderView.currentWeekdayIndex] == 1 {
                                    //arrReminders.append(reminder)
                                    
                                    let set_med_addOn = reminder.med_addOn
                                    let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
                                    //for (index,obj) in arrStr1.enumerated(){
                                    let strObj = (arrStr1.count == 1) ? arrStr1[0] as! String : arrStr1[index] as! String
                                    let strObjStart = (arrStrStart.count == 1) ? arrStrStart[0] as! String : arrStrStart[index] as! String
                                    let strObjEnd = (arrStrEnd.count == 1) ? arrStrEnd[0] as! String : arrStrEnd[index] as! String
                                    
                                    print("index : ",index,"Obj : ",strObj)
                                    let tempRem : Reminders = self.setTempReminder(reminder: reminder)
                                    tempRem.med_addOn = strObj
                                    tempRem.med_startDate = strObjStart
                                    tempRem.med_startDate = strObjEnd
                                    tempRem.timeIndex = index
                                    tempRem.med_end_enable = strObjEndEnable
                                    arrReminders.append(tempRem)
                                    //}
                                    /*if arrStr1.count == 0
                                     {
                                     arrReminders.append(reminder)
                                     }*/
                                }
                            }
                        }
                    }
                }
                
            }
        }
    }
    
    func checkCurrentDateWithPast(date:Date,remi:Reminders) -> Bool
    {
        //for remi in App.reminderList {
        
        let reminder : Reminders = remi
        if reminder.med_startDate.isEmpty != true
        {
            let set_med_startDate = reminder.med_startDate
            var set_med_endDate = reminder.med_endDate
            
            if reminder.med_endDate.isEmpty {
                set_med_endDate = set_med_startDate
            }
            
            let arrStrStart : NSArray = set_med_startDate.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_endDate.components(separatedBy: ",") as NSArray
            let arrStrAddOn : NSArray = reminder.med_addOn.components(separatedBy: ",") as NSArray
            
            for (index,objStart) in arrStrStart.enumerated(){
                let set_end_date = arrStrEnd[index] as! String
                let set_start_date = arrStrStart[index] as! String
                let arrStrDates : NSArray = set_end_date.components(separatedBy: " ") as NSArray
                let arrStrDates1 : NSArray = set_start_date.components(separatedBy: " ") as NSArray
                let dateEnd = self.dateStringToDate(string: "\(arrStrDates[0]) \(arrStrDates1[1])", format: "yyyy-MM-dd HH:mm:ss")
                let strDate_end = self.dateString(date:dateEnd, format:"yyyy-MM-dd HH:mm:ss")
                let date_end = self.UTCToLocalDate(date: strDate_end)
                
                //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
                let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate:date_end)
                //self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss")
                
                print("arrDates : ",arrDates.debugDescription)
                
                var strDate = ""
                if dayHeaderView.swipeLabelView.labels.first!.text?.isEmpty != true {
                    //strDate = self.dateString(date:self.dateStringToDate(string: dayHeaderView.swipeLabelView.labels.first!.text ?? "", format: "EEEE, d MMMM yyyy"), format:"yyyy-MM-dd HH:mm:ss")
                    strDate = self.dateString(date:date, format:"yyyy-MM-dd HH:mm:ss a")
                    
                    for dateTemp in arrDates{
                        
                        let dateFormator = DateFormatter()
                        dateFormator.dateFormat = "yyyy-MM-dd h:mm:ss a"
                        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
                        
                        let dt = dateFormator.date(from: strDate)
                        dateFormator.timeZone = TimeZone.current
                        dateFormator.dateFormat =  "yyyy-MM-dd h:mm:ss a"
                        
                        print("date1 notification : ",dt!)
                        print("date2 notification : ",dateTemp)
                        let strDate = self.dateString(date:dateTemp, format:"yyyy-MM-dd HH:mm:ss")
                        let dateTemp1 = self.UTCToLocalDate(date: strDate)
                        let set_taken_date : String! = self.dateString(date:dateTemp1.dateOnly(), format:"yyyy-MM-dd HH:mm:ss")
                        let arrSep = set_taken_date.components(separatedBy: " ") as NSArray
                        
                        for obj in arrStrAddOn{
                            let strObj = obj as! String
                            let arrAddSep = strObj.components(separatedBy: " ") as NSArray
                            let dateTemp2 = self.dateStringToDate(string: "\(arrSep[0]) \(arrAddSep[0]):00 \(arrAddSep[1])" , format: "yyyy-MM-dd hh:mm:ss a")
                            let strDate1 = self.dateString(date:dateTemp2, format:"yyyy-MM-dd h:mm:ss a")
                            
                            let dateFormator1 = DateFormatter()
                            dateFormator1.dateFormat = "yyyy-MM-dd h:mm:ss a"
                            dateFormator1.timeZone = TimeZone(abbreviation: "UTC")
                            
                            let dt3 = dateFormator1.date(from: strDate1)
                            dateFormator1.timeZone = TimeZone.current
                            dateFormator1.dateFormat =  "yyyy-MM-dd h:mm:ss a"
                            
                            print("date1 notification : ",dt!)
                            print("date3 notification : ",dt3!)
                            
                            if dt3! < dt!
                            {
                                print("notification is less than current date")
                                return true
                            }
                        }
                    }
                }
            }
            
        }
        return false
        // }
    }
    
    func loadImageFromDocumentDirectory(nameOfImage : String) -> UIImage {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(nameOfImage)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if(image == nil)
            {
                return UIImage.init(named: "order_med.png")!
            }
            return image!
        }
        return UIImage.init(named: "order_med.png")!
    }
    
    func compareDates(firstDate:Date,second:Date)->Bool
    {
        let strSelectedDate = self.dateString(date:firstDate.dateOnly(), format:"yyyy-MM-dd")// HH:mm:ss")
        let strCurrentDate = self.dateString(date:second.dateOnly(), format:"yyyy-MM-dd")// HH:mm:ss")
        
        let date1 = self.UTCToLocalDate(date: strSelectedDate + " 18:30:00")
        let date2 = self.UTCToLocalDate(date: strCurrentDate + " 18:30:00")
        print("selected date : ",date1)
        print("current date : ",date2)
        
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date1)
        let components1 = Calendar.current.dateComponents([.year, .month, .day], from: date2)
        
        let day = components.day
        let month = components.month
        let year = components.year
        
        let day1 = components1.day
        let month1 = components1.month
        let year1 = components1.year
        
        if (Int(day!) == Int(day1!) && Int(month!) == Int(month1!) && Int(year!) == Int(year1!))
        {
            return true
        }
        else
        {
            return false
            
        }
        
    }
    
    func setTempReminder(reminder : Reminders) -> Reminders {
        let tempReminder : Reminders = Reminders()
        tempReminder.id = reminder.id
        tempReminder.med_id = reminder.med_id
        tempReminder.med_name = reminder.med_name
        tempReminder.med_dosage = reminder.med_dosage
        tempReminder.med_startDate = reminder.med_startDate
        tempReminder.med_endDate = reminder.med_endDate
        tempReminder.med_addOn = reminder.med_addOn
        tempReminder.med_week = reminder.med_week
        tempReminder.med_taken = reminder.med_taken
        tempReminder.med_taken_at = reminder.med_taken_at
        tempReminder.med_img = reminder.med_img
        return tempReminder
    }
    
    func updatetAddOn(tempReminder:Reminders,withAddOn:String,withStart:String,withEnd:String,withEndEnable:String) -> Bool {
        
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        
        print("tempReminder.med_id : ",tempReminder.med_id)
        let reminder_record = reminders.filter(id == tempReminder.id)
        
        let update = reminder_record.update(
            [med_addedOn <- med_addedOn.replace(tempReminder.med_addOn, with: withAddOn),
             med_startDate <- med_startDate.replace(tempReminder.med_startDate, with: withStart),
             med_endDate <- med_endDate.replace(tempReminder.med_endDate, with: withEnd),
             med_end_enable <- med_end_enable.replace(tempReminder.med_end_enable, with: withEndEnable)])
        
        print("update query",update)
        
        guard let rowid2 = try? db.run(update) else{
            print("Not updated 2")
            return false
        }
        
        print("updated 2",rowid2)
        
        return true
    }
    
    func removeNotification(reminder:Reminders, removeAddOn:String)
    {
        
        let set_med_start = reminder.med_startDate
        let set_med_end = reminder.med_endDate
        let arrStrStart : NSArray = set_med_start.components(separatedBy: ",") as NSArray
        let arrStrEnd : NSArray = set_med_end.components(separatedBy: ",") as NSArray
        
        for (index,objStart) in arrStrStart.enumerated(){
            
            //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
            let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate: self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss"))
            
            print("arrDates to remove: ",arrDates.debugDescription)
            
            for (index1,dateTemp) in arrDates.enumerated(){
                let week = Calendar.current.component(.weekday, from: dateTemp)
                if reminder.med_week[week-1] == 1 {
                    //if compareDates(firstDate: dateTemp, second: App.selectedDate)
                    //{
                    let center = UNUserNotificationCenter.current()
                    //center.removeAllDeliveredNotifications()
                    let trimmedString = removeAddOn.trimmingCharacters(in: .whitespaces)
                    print("remove previous notification : ","MedicationAlert \(reminder.id) \(index1+1)  \(trimmedString)")
                    center.removePendingNotificationRequests(withIdentifiers: ["MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)"])
                    //}
                }
            }
        }
    }
    
    func removeNotification(reminder:Reminders)
    {
        if reminder.med_startDate.isEmpty != true
        {
            if App.medicinReminder.med_endDate.isEmpty {
                App.medicinReminder.med_endDate = App.medicinReminder.med_startDate
            }
            let set_med_start = reminder.med_startDate
            let set_med_end = reminder.med_endDate
            let arrStrStart : NSArray = set_med_start.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_end.components(separatedBy: ",") as NSArray
            
            for (index,objStart) in arrStrStart.enumerated(){
                
                //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
                let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate: self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss"))
                
                print("arrDates to remove: ",arrDates.debugDescription)
                
                for (index1,dateTemp) in arrDates.enumerated(){
                    let week = Calendar.current.component(.weekday, from: dateTemp)
                    if reminder.med_week[week-1] == 1 {
                        
                        let set_med_addOn = App.medicinReminder.med_addOn
                        var set_addOn_time = ""
                        let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
                        //for obj in arrStr1{
                        let strObj = (arrStr1.count == 1) ? arrStr1[0] as! String : arrStr1[index] as! String
                        set_addOn_time = strObj
                        let trimmedString = set_addOn_time.trimmingCharacters(in: .whitespaces)
                        print("remove notification : ","MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)")
                        cancelPendingNotifications(identifier: "MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)")
                        //}
                        /*
                         if arrStr1.count == 0
                         {
                         let trimmedString = set_med_addOn.trimmingCharacters(in: .whitespaces)
                         print("remove notification : ","MedicationAlert\(reminder.id)\(index+1)\(trimmedString)")
                         cancelPendingNotifications(identifier: "MedicationAlert\(reminder.id)\(index+1)\(trimmedString)")
                         }*/
                        //cancelPendingNotifications(identifier: "MedicationAlert\(reminder.id)\(index+1)")
                    }
                }
            }
        }
    }
    
    func removePastDateandTimeNotifications()
    {
        if App.reminderList.count != 0 {
            for (index,obj) in App.reminderList.enumerated()
            {
                print("reminderID - \(index) : ",obj.id)
                let center = UNUserNotificationCenter.current()
                center.removeAllDeliveredNotifications()
                center.getPendingNotificationRequests(completionHandler: { requests in
                    for request in requests {
                        print("Pending request Id : ",request.identifier)
                        let strRequestID = request.identifier
                        let arrStr : NSArray = strRequestID.components(separatedBy: " ") as NSArray
                        let strId :Int64 = arrStr[1] as! Int64
                        if obj.id == strId
                        {
                            if self.checkCurrentDateWithPast(date: Date(), remi: obj)
                            {
                                center.removePendingNotificationRequests(withIdentifiers: [request.identifier])
                            }
                        }
                    }
                })
            }
        }
    }
    
    func deleteFromDB(tempReminder:Reminders) -> Bool {
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        
        let reminder_record = reminders.filter(id == tempReminder.id)
        guard let rowid = try? db.run(reminder_record.delete()) else
        {
            print("Not deleated")
            return false
        }
        print("row deleted",rowid)
        return true
    }
    
    func cancelPendingNotifications(identifier:String)
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
            var identifiers: [String] = []
            for notification:UNNotificationRequest in notificationRequests {
                if notification.identifier == identifier {
                    identifiers.append(notification.identifier)
                }
            }
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
        }
    }
    
}

extension MedicationReminderViewController : DayHeaderViewDelegate
{
    func didSelectDate(_ date: Date) {
        print("didSelectDate : ",date)
        
        //let date1 = Calendar.current.date(byAdding: .day, value: 1, to: date)
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        //formatter.timeZone = NSTimeZone(name: "GMT")! as TimeZone
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        
        /*
         //then again set the date format whhich type of output you need
         formatter.dateFormat = "dd-MMM-yyyy"
         // again convert your date to string
         let myStringafd = formatter.string(from: yourDate!)*/
        
        App.selectedDate = yourDate!
        
        fetchDB(date: yourDate!)
    }
    
    func didMoveToDate(_ date: Date) {
        print("didMoveToDate : ",date)
        //let date1 = Calendar.current.date(byAdding: .day, value: 1, to: date)
        let formatter = DateFormatter()
        
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date)
        
        let yourDate = formatter.date(from: myString)
        
        App.selectedDate = yourDate!
        
        fetchDB(date: yourDate!)
    }
    
}

extension MedicationReminderViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReminders.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < arrReminders.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.MedicationReminderCell, for: indexPath) as! MedicationReminderTableCell
            let reminder : Reminders = arrReminders[indexPath.row]
            cell.lblName.text = reminder.med_name
            cell.lblDosage.text = reminder.med_dosage + " " + reminder.med_addOn
            cell.imgTaken.image = UIImage(named: "Unchecked_icon")
            
            if reminder.med_taken_at.count != 0
            {
                let med_taken_at = reminder.med_taken_at
                for obj in med_taken_at{
                    if obj.status == 1 && reminder.med_addOn.contains(obj.time)
                    {
                        cell.imgTaken.image = UIImage(named: "Checked_icon")
                        App.isTaken = true
                        
                    }
                    else if obj.status == 2 && reminder.med_addOn.contains(obj.time)
                    {
                        App.isSkipped = true
                        cell.imgTaken.image = UIImage(named: "skip")
                        
                    }
                    
                }
                
            }
            if !reminder.med_img.isEmpty
            {
                //let dataDecoded:Data = Data(base64Encoded: reminder.med_img, options: .ignoreUnknownCharacters)!
                //let decodedimage:UIImage = UIImage(data: dataDecoded)!
                cell.imgMed.image = loadImageFromDocumentDirectory(nameOfImage: reminder.med_img)//decodedimage
            }
            else
            {
                cell.imgMed.image = UIImage(named: "order_med")!.tintedWithLinearGradientColors(colorsArr: [Theme.navigationGradientColor![0]!.cgColor, Theme.navigationGradientColor![1]!.cgColor])
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "medReminderAdd", for: indexPath) as! MedicationReminderAdd
        /*
         cell.lblAdd.layer.cornerRadius = 25.0
         cell.lblAdd.layer.borderColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN).cgColor
         cell.lblAdd.layer.borderWidth = 2.0
         cell.lblAdd.layer.masksToBounds = true*/
        cell.addImg.image = UIImage(named: "add_reminder")!.imageWithColor(tintColor: Theme.buttonBackgroundColor!)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < arrReminders.count {
            App.medicinReminder = arrReminders[indexPath.row]
            //App.isFromEditMed = true
            App.isTaken = false
            App.isSkipped = false
            App.currentWeekdayIndex = dayHeaderView.currentWeekdayIndex
            
            if App.medicinReminder.med_taken_at.count != 0
            {
                let med_taken_at = App.medicinReminder.med_taken_at
                for obj in med_taken_at{
                    if obj.status == 1 && App.medicinReminder.med_addOn.contains(obj.time)
                    {
                        App.isTaken = true
                        
                    }
                    else if obj.status == 2 && App.medicinReminder.med_addOn.contains(obj.time)
                    {
                        App.isSkipped = true
                        
                    }
                    
                }
            }
            App.timeIndex = App.medicinReminder.timeIndex
            App.selectedDBId = App.medicinReminder.id
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_DETAILS, sender: self)
        }
        else
        {
            App.isFromEditMed = false
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_ADD_REMINDER, sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let reminder : Reminders = arrReminders[indexPath.row]
        var reminderTemp : Reminders = Reminders()
        for remi in App.reminderList {
            if remi.id == reminder.id
            {
                reminderTemp = remi
                break
            }
        }
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let set_med_addOn = reminderTemp.med_addOn
            let set_med_startDate = reminderTemp.med_startDate
            let set_med_endDate = reminderTemp.med_endDate
            let set_med_end_enable = reminderTemp.med_end_enable
            let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
            let arrStrStart : NSArray = set_med_startDate.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_endDate.components(separatedBy: ",") as NSArray
            let arrEnableEnd : NSArray = set_med_end_enable.components(separatedBy: ",") as NSArray
            var new_addOn = ""
            var removed_addOn = ""
            //var removed_start = ""
            //var removed_end = ""
            var new_start = ""
            var new_end = ""
            var new_end_enable = ""
            for (index,obj) in arrStr1.enumerated(){
                print(index,obj)
                let strObj = obj as! String
                let strObjStart = (arrStrStart.count == 1) ? arrStrStart[0] as! String : arrStrStart[index] as! String
                let strObjEnd = (arrStrEnd.count == 1) ? arrStrEnd[0] as! String : arrStrEnd[index] as! String
                let strObjEndEnable = (arrEnableEnd.count == 1) ? arrEnableEnd[0] as! String : arrEnableEnd[index] as! String
                if strObj == reminder.med_addOn
                {
                    removed_addOn = strObj
                    //removed_start = strObjStart
                    //removed_end = strObjEnd
                }
                else
                {
                    if new_addOn.isEmpty || new_addOn == ""
                    {
                        new_addOn = strObj
                    }
                    else{
                        new_addOn = new_addOn + "," + strObj
                    }
                    
                    if new_start.isEmpty || new_start == ""
                    {
                        new_start = strObjStart
                    }
                    else{
                        new_start = new_start + "," + strObjStart
                    }
                    
                    if new_end.isEmpty || new_end == ""
                    {
                        new_end = strObjEnd
                    }
                    else{
                        new_end = new_end + "," + strObjEnd
                    }
                    
                    if new_end_enable.isEmpty || new_end_enable == ""
                    {
                        new_end_enable = strObjEndEnable
                    }
                    else{
                        new_end_enable = new_end_enable + "," + strObjEndEnable
                    }
                    print("new_end_enable : ",new_end_enable)
                }
            }
            if arrStr1.count == 0
            {
                new_addOn = ""
                new_start = ""
                new_end = ""
                new_end_enable = ""
            }
            
            /*if new_addOn.isEmpty || new_addOn == ""
             {
             if self.updatetAddOn(tempReminder: reminderTemp, withAddOn: new_addOn, withStart:new_start, withEnd:new_end)
             {
             if self.deleteFromDB(tempReminder: reminderTemp)
             {
             DispatchQueue.main.async {
             self.removeNotification(reminder: reminderTemp)
             let successAlert = UIAlertController(title: "Medication", message: "Reminder Deleted Successfully", preferredStyle: UIAlertController.Style.alert)
             
             let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
             
             self.fetchDB(date: App.selectedDate)
             
             }
             successAlert.addAction(successOk)
             self.present(successAlert, animated: true)
             }
             
             }
             }
             else{*/
            if self.updatetAddOn(tempReminder: reminderTemp, withAddOn: new_addOn, withStart:new_start, withEnd:new_end, withEndEnable: new_end_enable)
            {
                print("Updated record")
                DispatchQueue.main.async {
                    self.removeNotification(reminder: reminderTemp,removeAddOn: removed_addOn)
                    let successAlert = UIAlertController(title: "Medication", message: "Reminder Deleted Successfully", preferredStyle: UIAlertController.Style.alert)
                    
                    let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
                        
                        self.fetchDB(date: App.selectedDate)
                        
                    }
                    successAlert.addAction(successOk)
                    self.present(successAlert, animated: true)
                }
            }
            //}
            
            
        }
    }
    /*
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
     let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
     // delete item at indexPath
     }
     
     let share = UITableViewRowAction(style: .normal, title: "Disable") { (action, indexPath) in
     // share item at indexPath
     }
     
     share.backgroundColor = UIColor.blue
     
     return [delete, share]
     }*/
}

class MedicationReminderTableCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDosage: UILabel!
    @IBOutlet var imgTaken: UIImageView!
    @IBOutlet var imgMed: UIImageView!
    
}

class MedicationReminderAdd: UITableViewCell
{
    @IBOutlet var lblAdd:InsetLabel!
    @IBOutlet weak var addImg: UIImageView!
    
}

class Reminders {
    private var _id: Int64 = 0
    private var _med_id: String = ""
    private var _med_name: String = ""
    private var _med_dosage: String = ""
    private var _med_startDate:String = ""
    private var _med_endDate:String = ""
    private var _med_addOn:String = ""
    private var _med_week:[Int64] = [0,0,0,0,0,0,0]
    private var _med_taken:Int64 = 0
    private var _med_taken_at = [taken_time_status]()
    private var _med_img:String = ""
    private var _timeIndex: Int = 0
    private var _med_end_enable:String = ""
    
    init() {
    }
    
    init(id:Int64, med_id: String, med_name: String, med_dosage: String, med_startDate:String, med_endDate:String, med_addOn:String, med_week:[Int64], med_taken:Int64, med_taken_at:[taken_time_status], med_img:String, timeIndex:Int, med_end_enable:String){
        self.id = id
        self.med_id = med_id
        self.med_name = med_name
        self.med_dosage = med_dosage
        self.med_startDate = med_startDate
        self.med_endDate = med_endDate
        self.med_addOn = med_addOn
        self.med_week = med_week
        self.med_taken = med_taken
        self.med_taken_at = med_taken_at
        self.med_img = med_img
        self.timeIndex = timeIndex
        self.med_end_enable = med_end_enable
    }
    
    var med_id: String {
        set { _med_id = newValue }
        get { return _med_id }
    }
    
    var id: Int64 {
        set { _id = newValue }
        get { return _id }
    }
    
    var med_name: String {
        set { _med_name = newValue }
        get { return _med_name }
    }
    
    var med_dosage: String {
        set { _med_dosage = newValue }
        get { return _med_dosage }
    }
    
    var med_startDate: String {
        set { _med_startDate = newValue }
        get { return _med_startDate }
    }
    
    var med_endDate: String {
        set { _med_endDate = newValue }
        get { return _med_endDate }
    }
    
    var med_addOn: String {
        set { _med_addOn = newValue }
        get { return _med_addOn }
    }
    
    var med_week: [Int64] {
        set { _med_week = newValue }
        get { return _med_week }
    }
    
    var med_taken: Int64 {
        set { _med_taken = newValue }
        get { return _med_taken }
    }
    
    var med_taken_at: [taken_time_status] {
        set { _med_taken_at = newValue }
        get { return _med_taken_at }
    }
    
    var med_img: String {
        set { _med_img = newValue }
        get { return _med_img }
    }
    
    var timeIndex: Int {
        set { _timeIndex = newValue }
        get { return _timeIndex }
    }
    
    var med_end_enable: String {
        set { _med_end_enable = newValue }
        get { return _med_end_enable }
    }
    
    var obj_description:String {
        return "db_id :\(self.id) \n med_id :\(self.med_id) \n med_name \(self.med_name) \n med_dosage \(self.med_dosage) \n med_startDate \(self.med_startDate) \n med_endDate \(self.med_endDate) \n med_addOn \(self.med_addOn) \n med_week \(self.med_week) \n med_taken \(self.med_taken) \n med_taken_at \(self.med_taken_at) \n timeIndex \(self.timeIndex) \n med_img \(self.med_img)"
    }
}

extension Date{
    
    func arrayBetweenTwoDates(startDate: Date , endDate:Date) ->[Date]
    {
        var datesArray: [Date] =  [Date]()
        var startDate = startDate
        let calendar = Calendar.current
        
        //let fmt = DateFormatter()
        //fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //fmt.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        //fmt.timeZone = NSTimeZone.local
        
        while startDate <= endDate {
            datesArray.append(startDate)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return datesArray
    }
}

@IBDesignable class InsetLabel: UILabel {
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    
    var insets: UIEdgeInsets {
        get {
            //return UIEdgeInsetsMake(topInset, leftInset, bottomInset, rightInset)
            return UIEdgeInsets(top: topInset,left: leftInset,bottom: bottomInset,right: rightInset)
        }
        set {
            topInset = newValue.top
            leftInset = newValue.left
            bottomInset = newValue.bottom
            rightInset = newValue.right
        }
    }
    
    override func drawText(in rect: CGRect) {
        //super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        super.drawText(in: rect.inset(by: UIEdgeInsets(top: insets.top, left: insets.left, bottom: insets.bottom, right: insets.right)))
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var adjSize = super.sizeThatFits(size)
        adjSize.width += leftInset + rightInset
        adjSize.height += topInset + bottomInset
        
        return adjSize
    }
    
    override var intrinsicContentSize: CGSize {
        var contentSize = super.intrinsicContentSize
        contentSize.width += leftInset + rightInset
        contentSize.height += topInset + bottomInset
        
        return contentSize
    }
}

extension UIImage {
func imageWithColor(tintColor: UIColor) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)

    let context = UIGraphicsGetCurrentContext()!
    context.translateBy(x: 0, y: self.size.height)
    context.scaleBy(x: 1.0, y: -1.0);
    context.setBlendMode(.normal)

    let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
    context.clip(to: rect, mask: self.cgImage!)
    tintColor.setFill()
    context.fill(rect)

    let newImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    return newImage
}
}
