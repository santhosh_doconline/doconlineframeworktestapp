//
//  AddMedicationTimeViewController.swift
//  DocOnline
//
//  Created by Doconline india on 04/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class AddMedicationTimeViewController: UIViewController {
    
    @IBOutlet var btnWeeks : [UIButton]!
    @IBOutlet var scrVW: UIScrollView!
    @IBOutlet var vwTimePicker: UIView!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var vwStartDatePicker: UIView!
    @IBOutlet var lblStartDate: UILabel!
    @IBOutlet var vwEndDatePicker: UIView!
    @IBOutlet var lblEndDate: UILabel!
    @IBOutlet var enableSwitch: UISwitch!
    
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var heightOfEndDate: NSLayoutConstraint!
    @IBOutlet weak var heightOfTime: NSLayoutConstraint!
    @IBOutlet weak var heightOfTimeLbl: NSLayoutConstraint!
    @IBOutlet weak var heightOfDaysLbl: NSLayoutConstraint!
    @IBOutlet weak var heightOfDays: NSLayoutConstraint!
    @IBOutlet weak var heightOfStartLbl: NSLayoutConstraint!
    @IBOutlet weak var heightOfStart: NSLayoutConstraint!
    @IBOutlet weak var heightOfEndLbl: NSLayoutConstraint!
    @IBOutlet weak var heightOfViewEnd: NSLayoutConstraint!
    
    var timePicker: UIDatePicker!
    var startDatePicker: UIDatePicker!
    var endDatePicker: UIDatePicker!
    
    var med_week:[Int64] = [0,0,0,0,0,0,0]
    var weekDay = 0
    
    var strEndDate = ""
    var strStartDate = ""
    
    var strEnableEnd = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        heightOfEndDate.constant = 0.0
        scrVW.contentSize.height = scrVW.contentSize.height - 160.0
        
        /*
         if !App.isTime
         {
         heightOfTime.constant = 0.0
         heightOfTimeLbl.constant = 0.0
         
         heightOfDaysLbl.constant = 20.0
         heightOfDays.constant = 56.0
         heightOfStartLbl.constant = 20.0
         heightOfStart.constant = 160.0
         heightOfEndLbl.constant = 20.0
         heightOfViewEnd.constant = 41.0
         enableSwitch.isHidden = false
         }
         else
         {
         heightOfTime.constant = 160.0
         heightOfTimeLbl.constant = 20.0
         
         heightOfDaysLbl.constant = 0.0
         heightOfDays.constant = 0.0
         heightOfStartLbl.constant = 0.0
         heightOfStart.constant = 0.0
         heightOfEndLbl.constant = 0.0
         heightOfViewEnd.constant = 0.0
         enableSwitch.isHidden = true
         }*/
        
        for obj in btnWeeks
        {
            obj.layer.borderColor = Theme.buttonBackgroundColor!.cgColor
            obj.layer.borderWidth = 2.0
            obj.layer.masksToBounds = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.doneBtn.backgroundColor = Theme.buttonBackgroundColor
        self.enableSwitch.onTintColor = Theme.switchOnTintColor!
        self.enableSwitch.thumbTintColor = Theme.switchThumbTintColor!
        
        var set_med_addOn = ""
        var dateEdit = Date()
        var set_med_start = ""
        //var dateStart = Date()
        var set_med_end = ""
        //var dateEnd = Date()
        var set_start_date = ""
        var set_end_date = ""
        var set_enable_end_date = ""
        var set_enable_end = ""
        if App.isFromEditMed && App.timeIndex != -1{
            //if !App.isTime
            //{
            med_week = App.medicinReminder.med_week
            for (index,obj) in med_week.enumerated()
            {
                btnWeeks[index].isSelected = (obj == 1) ? true : false
                
                btnWeeks[index].backgroundColor = btnWeeks[index].isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
                btnWeeks[index].setTitleColor(btnWeeks[index].isSelected ? UIColor.white : Theme.buttonBackgroundColor! , for: .normal)
                
            }
            
            var set_med_startDate = ""
            var set_med_endDate = ""
            if !App.medicinReminder.med_startDate.isEmpty {
                set_med_startDate = App.medicinReminder.med_startDate
                strStartDate = App.medicinReminder.med_startDate
                
                var arrStr : NSArray = set_med_startDate.components(separatedBy: " ") as NSArray
                set_med_startDate = arrStr[0] as! String
                
                if !App.medicinReminder.med_endDate.isEmpty {
                    set_med_endDate = App.medicinReminder.med_endDate
                    strEndDate = App.medicinReminder.med_endDate
                    
                    arrStr = set_med_endDate.components(separatedBy: " ") as NSArray
                    set_med_endDate = arrStr[0] as! String
                }
                else
                {
                    set_med_endDate = set_med_startDate
                    strStartDate = strEndDate
                }
            }
            
            lblStartDate.text = set_med_startDate
            lblEndDate.text = set_med_endDate
            
            if !App.medicinReminder.med_end_enable.isEmpty {
                strEnableEnd = App.medicinReminder.med_end_enable
            }
            //}
        }
        else
        {
            //if !App.isTime{
            lblStartDate.text = self.dateString(date: Date().dateOnly(), format: "yyyy-MM-dd")
            //App.medicinReminder.med_startDate = self.dateString(date: Date(), format: "yyyy-MM-dd HH:mm:ss")
            strStartDate = self.dateString(date: Date(), format: "yyyy-MM-dd HH:mm:ss")
            
            //App.medicinReminder.med_endDate = self.dateString(date: Date(), format: "yyyy-MM-dd HH:mm:ss")
            strEndDate = self.dateString(date: Date(), format: "yyyy-MM-dd HH:mm:ss")
            for (index,obj) in btnWeeks.enumerated()
            {
                obj.isSelected = true
                
                obj.backgroundColor = obj.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
                if obj.isSelected
                {
                    med_week[index] = 1
                }
                else
                {
                    med_week[index] = 0
                }
                obj.setTitleColor(obj.isSelected ? UIColor.white : Theme.buttonBackgroundColor! , for: .normal)
            }
            strEnableEnd = "0"
            //}
        }
        
        //if App.isTime{
        lblTime.text = Date().getCurrentTimeInString()
        //App.medicinReminder.med_addOn = Date().getCurrentTimeInString()
        
        if !App.medicinReminder.med_addOn.isEmpty &&  App.timeIndex != -1{
            set_med_addOn = App.medicinReminder.med_addOn
            set_med_start = strStartDate//App.medicinReminder.med_startDate
            set_med_end = strEndDate//App.medicinReminder.med_endDate
            set_enable_end_date = strEnableEnd
            var set_addOn_time = ""
            let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
            let arrStrStart : NSArray = set_med_start.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_end.components(separatedBy: ",") as NSArray
            let arrStrEnable : NSArray = set_enable_end_date.components(separatedBy: ",") as NSArray
            for (index,obj) in arrStr1.enumerated(){
                if index == App.timeIndex
                {
                    let strObj = obj as! String
                    set_addOn_time = strObj
                    set_start_date = (arrStrStart.count == 1) ? arrStrStart[0] as! String : arrStrStart[index] as! String
                    set_end_date = (arrStrEnd.count == 1) ? arrStrEnd[0] as! String : arrStrEnd[index] as! String
                    set_enable_end = (arrStrEnable.count == 1) ? arrStrEnable[0] as! String : arrStrEnable[index] as! String
                    break
                }
            }
            if arrStr1.count == 0
            {
                lblTime.text = App.medicinReminder.med_addOn
                lblStartDate.text = App.medicinReminder.med_startDate
                lblEndDate.text = App.medicinReminder.med_endDate
            }
            else
            {
                var set_med_startDate = ""
                var set_med_endDate = ""
                if !set_start_date.isEmpty {
                    set_med_startDate = set_start_date
                    
                    var arrStr : NSArray = set_med_startDate.components(separatedBy: " ") as NSArray
                    set_med_startDate = arrStr[0] as! String
                    
                    if !set_end_date.isEmpty {
                        set_med_endDate = set_end_date
                        
                        arrStr = set_med_endDate.components(separatedBy: " ") as NSArray
                        set_med_endDate = arrStr[0] as! String
                    }
                    else
                    {
                        set_med_endDate = set_med_startDate
                    }
                }
                
                lblTime.text = set_addOn_time
                lblStartDate.text = set_med_startDate
                strStartDate = set_start_date
                lblEndDate.text = set_med_endDate
                strEndDate = set_end_date
            }
            
            //set_med_addOn = App.medicinReminder.med_addOn
            let arrStr : NSArray = set_addOn_time.components(separatedBy: " ") as NSArray
            let time = arrStr[0] as! String
            let time_a = arrStr[1] as! String
            
            // Show Edit Time .......
            let currentDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
            let strDate = self.dateString(date:currentDate!, format:"yyyy-MM-dd HH:mm:ss")
            let date1 = self.UTCToLocalDate(date: strDate)
            let set_taken_date : String! = self.dateString(date:date1.dateOnly(), format:"yyyy-MM-dd HH:mm:ss")
            let arrSep = set_taken_date.components(separatedBy: " ") as NSArray
            
            let dateTemp = self.dateStringToDate(string: "\(arrSep[0]) \(time):00 \(time_a)", format: "yyyy-MM-dd hh:mm:ss a")
            let strDate1 = self.dateString(date:dateTemp, format:"yyyy-MM-dd HH:mm:ss")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let date = dateFormatter.date(from: strDate1)
            dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss a"
            let str = dateFormatter.string(from: date!)
            print("12 hour formatted Date:",str)
            dateEdit = dateFormatter.date(from: str)!
            // Show Edit Time
            
        }
        timePicker = self.addDatePicker(toView: vwTimePicker, pickerMode: "time")
        timePicker.addTarget(self, action: #selector(self.selectedTime(_:)), for: .valueChanged)
        timePicker.setDate(dateEdit, animated: true)
        //}
        //else{
        
        startDatePicker = self.addDatePicker(toView: vwStartDatePicker, pickerMode: "date")
        startDatePicker.addTarget(self, action: #selector(self.selectedStartTime(_:)), for: .valueChanged)
        startDatePicker.setDate((App.isFromEditMed && App.timeIndex != -1) ? self.dateStringToDate(string: set_start_date, format: "yyyy-MM-dd HH:mm:ss") : Date(), animated: true)
        
        endDatePicker = self.addDatePicker(toView: vwEndDatePicker, pickerMode: "date")
        endDatePicker.addTarget(self, action: #selector(self.selectedEndTime(_:)), for: .valueChanged)
        endDatePicker.setDate((App.isFromEditMed && App.timeIndex != -1) ? self.dateStringToDate(string: set_end_date, format: "yyyy-MM-dd HH:mm:ss") : Date(), animated: true)
        if set_enable_end == "1"
        {
            enableSwitch.isOn = true
        }
        else
        {
            enableSwitch.isOn = false
        }
        self.checkState(enableSwitch)
        //}
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func addDatePicker(toView : UIView, pickerMode:String) -> UIDatePicker {
        // DatePicker
        let datePicker = UIDatePicker(frame:CGRect(x: (toView.frame.size.width-300)/2, y: 0, width: 300, height: 160))
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = (pickerMode == "time") ? UIDatePicker.Mode.time : UIDatePicker.Mode.date
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: (pickerMode == "time") ? -1 : 0, to: Date())
        datePicker.setDate(Date(), animated: true)
        toView.layer.borderColor = UIColor.black.cgColor
        toView.layer.borderWidth = 0.5
        toView.layer.masksToBounds = true
        toView.addSubview(datePicker)
        
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: datePicker, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: datePicker, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        toView.addConstraints([horizontalConstraint, verticalConstraint])
        
        return datePicker
        
    }
    
    @IBAction func clickBtnWeeks(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        // handle the cases
        case 1:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[0] = 1
            }
            else
            {
                med_week[0] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        case 2:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[1] = 1
            }
            else
            {
                med_week[1] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        case 3:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[2] = 1
            }
            else
            {
                med_week[2] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        case 4:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[3] = 1
            }
            else
            {
                med_week[3] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        case 5:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[4] = 1
            }
            else
            {
                med_week[4] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        case 6:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[5] = 1
            }
            else
            {
                med_week[5] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        case 7:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            if sender.isSelected
            {
                med_week[6] = 1
            }
            else
            {
                med_week[6] = 0
            }
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
            break
        default:
            sender.backgroundColor = sender.isSelected ? Theme.buttonBackgroundColor! : UIColor.clear
            sender.setTitleColor(sender.isSelected ? UIColor.white : Theme.litebuttonBackgroundColor! , for: .normal)
        }
    }
    
    @IBAction func checkState(_ sender: UISwitch) {
        if (sender.isOn == true)
        {
            if !App.isFromEditMed
            {
                lblEndDate.text = self.dateString(date: Date().dateOnly(), format: "yyyy-MM-dd")
                //App.medicinReminder.med_endDate = self.dateString(date: Date(), format: "yyyy-MM-dd HH:mm:ss")
            }
            
            scrVW.contentSize.height = scrVW.contentSize.height
            heightOfEndDate.constant = 160.0
            strEnableEnd = "1"
        }
        else
        {
            lblEndDate.text = "Select End Date"
            //App.medicinReminder.med_endDate = ""
            
            scrVW.contentSize.height = scrVW.contentSize.height - 160.0
            heightOfEndDate.constant = 0.0
            strEnableEnd = "0"
        }
    }
    
    @objc func selectedTime(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.hour, .minute, .second], from: sender.date)
        if let hour = components.hour, let minute = components.minute, let second = components.second {
            print("\(hour) \(minute) \(second)")
            //let strTime = String(hour) + ":" + String(minute) + ":" + String(second)
            lblTime.text = sender.date.getCurrentTimeInString()
            //App.medicinReminder.med_addOn = sender.date.getCurrentTimeInString()//String(hour) + ":" + String(minute)
        }
    }
    
    @objc func selectedStartTime(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {//,let week = components.weekday{
            print("\(day) \(month) \(year)")
            //lblStartDate.text = String(year) + "-" + String(month) + "-" + String(day)
            let week = Calendar.current.component(.weekday, from: sender.date)
            self.weekDay = week
            //App.medicinReminder.med_startDate = String(year) + "-" + String(month) + "-" + String(day)
            //App.medicinReminder.med_startDate = self.dateString(date: sender.date, format: "yyyy-MM-dd HH:mm:ss")
            lblStartDate.text = self.dateString(date: sender.date.dateOnly(), format: "yyyy-MM-dd")
            strStartDate = self.dateString(date: sender.date, format: "yyyy-MM-dd HH:mm:ss")
            
        }
    }
    
    @objc func selectedEndTime(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
            //lblEndDate.text = String(year) + "-" + String(month) + "-" + String(day)
            //App.medicinReminder.med_endDate = String(year) + "-" + String(month) + "-" + String(day)
            lblEndDate.text = self.dateString(date: sender.date.dateOnly(), format: "yyyy-MM-dd")
            //App.medicinReminder.med_endDate = self.dateString(date: sender.date, format: "yyyy-MM-dd HH:mm:ss")
            strEndDate = self.dateString(date: sender.date, format: "yyyy-MM-dd HH:mm:ss")
        }
    }
    
    @IBAction func clickBtnDone(_ sender: UIButton) {
        //if !App.isTime || App.isFromEditMed
        //{
        var count = 0
        for obj in med_week
        {
            if obj == 1
            {
                count = 1
            }
        }
        if lblStartDate.text?.isEmpty != true && lblStartDate.text != "Select Start Date" && count == 1 //&& lblTime.text?.isEmpty != true && lblTime.text != "Select Time"
        {
            if ((enableSwitch.isOn == true && lblEndDate.text?.isEmpty != true && lblEndDate.text != "Select End Date") || enableSwitch.isOn == false)
            {
                var checkWeek = 0
                for item in med_week {
                    if item == 0
                    {
                        
                    }
                    else
                    {
                        checkWeek = 1
                    }
                }
                
                if checkWeek == 0 {
                    med_week[weekDay-1] = 1
                }
                
                App.medicinReminder.med_week = med_week
                
                
                if !App.isTime
                {
                    _ = navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                self.didShowAlert(title: "Medication", message: "Please Select End Date")
            }
        }
        else
        {
            /*if lblTime.text?.isEmpty == true && lblTime.text == "Select Time"
             {
             self.didShowAlert(title: "Medication", message: "Please Select Time")
             }else*/
            if count == 0
            {
                self.didShowAlert(title: "Medication", message: "Please Select Week day")
            }
            else
            {
                self.didShowAlert(title: "Medication", message: "Please Select Start Date")
            }
        }
        //}
        //if App.isTime{
        
        if lblTime.text?.isEmpty == true && lblTime.text == "Select Time"
        {
            self.didShowAlert(title: "Medication", message: "Please Select Time")
        }
        else
        {
            if App.medicinReminder.med_addOn.isEmpty
            {
                App.medicinReminder.med_addOn = lblTime.text!
            }
            else
            {
                if App.timeIndex != -1
                {
                    var edited_addOn = ""
                    let set_med_addOn = App.medicinReminder.med_addOn
                    let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
                    for (index,obj) in arrStr1.enumerated(){
                        var strObj = obj as! String
                        if index == App.timeIndex
                        {
                            strObj = lblTime.text!
                        }
                        
                        if edited_addOn.isEmpty || edited_addOn == ""
                        {
                            edited_addOn = strObj
                        }
                        else
                        {
                            edited_addOn = edited_addOn + "," + strObj
                        }
                    }
                    print("edited_addOn : ",edited_addOn)
                    App.medicinReminder.med_addOn = edited_addOn
                }
                else if App.medicinReminder.med_addOn.contains(lblTime.text!)
                {
                    let successAlert = UIAlertController(title: "Medication", message: "Time already added", preferredStyle: UIAlertController.Style.alert)
                    
                    let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
                        
                    }
                    successAlert.addAction(successOk)
                    self.present(successAlert, animated: true)
                }
                else
                {
                    App.medicinReminder.med_addOn = App.medicinReminder.med_addOn + "," + lblTime.text!
                }
                
            }
            
            _ = navigationController?.popViewController(animated: true)
        }
        //}
        
        if App.medicinReminder.med_startDate.isEmpty
        {
            App.medicinReminder.med_startDate = strStartDate
        }
        else
        {
            if App.timeIndex != -1
            {
                var edited_start = ""
                let set_med_start = App.medicinReminder.med_startDate
                let arrStr1 : NSArray = set_med_start.components(separatedBy: ",") as NSArray
                for (index,obj) in arrStr1.enumerated(){
                    var strObj = obj as! String
                    if index == App.timeIndex
                    {
                        strObj = strStartDate//lblStartDate.text!
                    }
                    
                    if edited_start.isEmpty || edited_start == ""
                    {
                        edited_start = strObj
                    }
                    else
                    {
                        edited_start = edited_start + "," + strObj
                    }
                }
                print("edited_start: ",edited_start)
                App.medicinReminder.med_startDate = edited_start
            }
            else
            {
                App.medicinReminder.med_startDate = App.medicinReminder.med_startDate + "," + strStartDate
            }
            
        }
        
        
        if App.medicinReminder.med_end_enable.isEmpty
        {
            App.medicinReminder.med_end_enable = strEnableEnd
        }
        else
        {
            if App.timeIndex != -1
            {
                var edited_start = ""
                let set_med_start = App.medicinReminder.med_end_enable
                let arrStr1 : NSArray = set_med_start.components(separatedBy: ",") as NSArray
                for (index,obj) in arrStr1.enumerated(){
                    var strObj = obj as! String
                    if index == App.timeIndex
                    {
                        strObj = strEnableEnd//lblStartDate.text!
                    }
                    
                    if edited_start.isEmpty || edited_start == ""
                    {
                        edited_start = strObj
                    }
                    else
                    {
                        edited_start = edited_start + "," + strObj
                    }
                }
                print("edited_enabled : ",edited_start)
                App.medicinReminder.med_end_enable = edited_start
            }
            else
            {
                App.medicinReminder.med_end_enable = App.medicinReminder.med_end_enable + "," + strEnableEnd
            }
            
        }
        
        App.medicinReminder.med_week = med_week
        
        if App.medicinReminder.med_endDate.isEmpty
        {
            App.medicinReminder.med_endDate = strEndDate
        }
        else
        {
            if App.timeIndex != -1
            {
                var edited_end = ""
                let set_med_end = App.medicinReminder.med_endDate
                let arrStr1 : NSArray = set_med_end.components(separatedBy: ",") as NSArray
                for (index,obj) in arrStr1.enumerated(){
                    var strObj = obj as! String
                    if index == App.timeIndex
                    {
                        strObj = strEndDate
                    }
                    
                    if edited_end.isEmpty || edited_end == ""
                    {
                        edited_end = strObj
                    }
                    else
                    {
                        edited_end = edited_end + "," + strObj
                    }
                }
                print("edited_end : ",edited_end)
                App.medicinReminder.med_endDate = edited_end
            }
            else
            {
                App.medicinReminder.med_endDate = App.medicinReminder.med_endDate + "," + strEndDate
            }
            
        }
        
        if strEnableEnd == "0"
        {
            App.medicinReminder.med_endDate = App.medicinReminder.med_startDate
        }
        
    }
    
}
