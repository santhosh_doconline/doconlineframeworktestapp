//
//  MedicationDetailsViewController.swift
//  DocOnline
//
//  Created by Doconline india on 07/12/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import SQLite
import UserNotifications

class MedicationDetailsViewController: UIViewController {
    
    @IBOutlet weak var lblMedicinName: UILabel!
    @IBOutlet weak var lblMedicinTime: UILabel!
    @IBOutlet weak var lblMedicinTake: UILabel!
    @IBOutlet weak var imgMedicin: UIImageView!
    @IBOutlet weak var takenHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var takeBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    
    let reminders = Table("medicinesremainder")
    let id = Expression<Int64>("id")
    let med_id = Expression<String?>("tablet_id")
    
    let taken = Table("taken_skip")
    let taken_date = Expression<String?>("date")
    let taken_time = Expression<String?>("time")
    let taken_state = Expression<Int64?>("status")
    let taken_reminder_id = Expression<Int64?>("medicineremainderid")
    let reminder_time = Expression<String?>("reminder_time")
    var timeIndex = -1
    
    //let tempReminder = Reminders()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgMedicin.image = UIImage(named: "order_med")!.tintedWithLinearGradientColors(colorsArr: [Theme.navigationGradientColor![0]!.cgColor, Theme.navigationGradientColor![1]!.cgColor])
        self.lblMedicinTake.textColor = Theme.buttonBackgroundColor
        // Do any additional setup after loading the view.
        timeIndex = App.timeIndex
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        takeBtn.backgroundColor = Theme.buttonBackgroundColor
        editBtn.backgroundColor = Theme.buttonBackgroundColor
        
        if !App.medicinReminder.med_name.isEmpty {
            lblMedicinName.text = App.medicinReminder.med_name
        }
        
        
        if !App.medicinReminder.med_dosage.isEmpty ||  !App.medicinReminder.med_addOn.isEmpty{
            //lblMedicinTime.text = App.medicinReminder.med_dosage  + " " + App.medicinReminder.med_addOn
            
            let set_med_addOn = App.medicinReminder.med_addOn
            let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
            if arrStr1.count == 0
            {
                if !App.medicinReminder.med_dosage.isEmpty
                {
                    lblMedicinTime.text = App.medicinReminder.med_dosage  + " " + App.medicinReminder.med_addOn
                }
                else
                {
                    lblMedicinTime.text = App.medicinReminder.med_addOn
                }
            }
            else{
                let tempRem : Reminders = self.setTempReminder(reminder: App.medicinReminder)
                tempRem.med_addOn = set_med_addOn//arrStr1[App.timeIndex] as! String
                if arrStr1.count > 1
                {
                    tempRem.med_addOn = arrStr1[timeIndex] as! String
                }
                if !App.medicinReminder.med_dosage.isEmpty
                {
                    lblMedicinTime.text = App.medicinReminder.med_dosage  + " " + tempRem.med_addOn
                }
                else
                {
                    lblMedicinTime.text = tempRem.med_addOn
                }
            }
        }
        
        lblMedicinTake.text = ""
        
        let strSelectedDate = self.dateString(date:App.selectedDate, format:"yyyy-MM-dd HH:mm:ss")
        let strCurrentDate = self.dateString(date:currentDate().dateOnly(), format:"yyyy-MM-dd HH:mm:ss")
        
        let date1 = self.UTCToLocalDate(date: strSelectedDate)
        let date2 = self.UTCToLocalDate(date: strCurrentDate)
        print("selected date : ",date1)
        print("current date : ",date2)
        
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date1)
        let components1 = Calendar.current.dateComponents([.year, .month, .day], from: date2)
        
        let day = components.day
        let month = components.month
        let year = components.year
        
        let day1 = components1.day
        let month1 = components1.month
        let year1 = components1.year
        
        if (day == day1 && month == month1 && year == year1)
        {
            takenHeightConstraint.constant = 40
            if App.isTaken
            {
                lblMedicinTake.text = "Taken"
                takenHeightConstraint.constant = 0
            }
            else
            {
                takenHeightConstraint.constant = 40
                lblMedicinTake.text = ""
                if App.isSkipped
                {
                    lblMedicinTake.text = "Skipped"
                    takenHeightConstraint.constant = 0
                }
                else
                {
                    takenHeightConstraint.constant = 40
                    lblMedicinTake.text = ""
                }
            }
        }
        else
        {
            takenHeightConstraint.constant = 0
            
        }
        
        if !App.medicinReminder.med_img.isEmpty
        {
            //let dataDecoded:Data = Data(base64Encoded: App.medicinReminder.med_img, options: .ignoreUnknownCharacters)!
            //let decodedimage:UIImage = UIImage(data: dataDecoded)!
            imgMedicin.image = loadImageFromDocumentDirectory(nameOfImage: App.medicinReminder.med_img)//decodedimage
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.linearBar.stopAnimation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setTempReminder(reminder : Reminders) -> Reminders {
        let tempReminder : Reminders = Reminders()
        tempReminder.id = reminder.id
        tempReminder.med_id = reminder.med_id
        tempReminder.med_name = reminder.med_name
        tempReminder.med_dosage = reminder.med_dosage
        tempReminder.med_startDate = reminder.med_startDate
        tempReminder.med_endDate = reminder.med_endDate
        tempReminder.med_addOn = reminder.med_addOn
        tempReminder.med_week = reminder.med_week
        tempReminder.med_taken = reminder.med_taken
        tempReminder.med_taken_at = reminder.med_taken_at
        tempReminder.med_img = reminder.med_img
        return tempReminder
    }
    
    func currentDate() -> Date {
        print("current date :",Date().debugDescription)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        let strDate = dateFormatter.string(from: Date())
        print("current date in string :",strDate)
        let yourDate = dateFormatter.date(from: strDate)
        return yourDate!
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func clickBtnSkip(_ sender: UIButton) {
        if self.insertTakenDB(isTaken: false)
        {
            print("Updated Skip record")
            takenHeightConstraint.constant = 0
            //self.didShowAlert(title: "Skipped", message: "Skipped successfully")
            let successAlert = UIAlertController(title: "Skipped", message: "Medicine skipped", preferredStyle: UIAlertController.Style.alert)
            
            let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
                
                self.view.endEditing(true)
                _ = self.navigationController?.popViewController(animated: true)
                
            }
            successAlert.addAction(successOk)
            self.present(successAlert, animated: true)
        }
    }
    
    @IBAction func clickBtnTake(_ sender: UIButton) {
        if self.insertTakenDB(isTaken: true)
        {
            print("Updated Taken record")
            takenHeightConstraint.constant = 0
            //self.didShowAlert(title: "Taken", message: "Taken successfully")
            let successAlert = UIAlertController(title: "Taken", message: "Medicine Taken", preferredStyle: UIAlertController.Style.alert)
            
            let successOk = UIAlertAction(title: "Ok", style: .default) { (action) in
                
                self.view.endEditing(true)
                _ = self.navigationController?.popViewController(animated: true)
                
            }
            successAlert.addAction(successOk)
            self.present(successAlert, animated: true)
        }
    }
    
    @IBAction func clickBtnDelete(_ sender: UIButton) {
        let title = "Delete Medication"
        let deleteAlert = UIAlertController(title: title, message: "Are you sure. You want to delete all reminders for this Medication ?", preferredStyle: UIAlertController.Style.alert)
        
        let selectDosage = UIAlertAction(title: "Ok", style: .default) { (action) in
            if self.deleteFromDB()
            {
                self.removeNotifications()
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        deleteAlert.addAction(selectDosage)
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(deleteAlert, animated: true)
        
        
    }
    
    @IBAction func clickBtnEdit(_ sender: UIButton) {
        App.isFromEditMed = true
        
        for remi in App.reminderList {
            if remi.id == App.medicinReminder.id
            {
                App.medicinReminder = remi
                break
            }
        }
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MEDICATION_ADD_REMINDER, sender: self)
    }
    
    func deleteFromDB() -> Bool {
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        
        let reminder_record = reminders.filter(id == App.medicinReminder.id)
        guard let rowid = try? db.run(reminder_record.delete()) else
        {
            print("Not deleated")
            return false
        }
        print("row deleted",rowid)
        return true
    }
    
    
    func removeNotifications()
    {
        let reminder : Reminders = App.medicinReminder
        if reminder.med_startDate.isEmpty != true
        {
            if App.medicinReminder.med_endDate.isEmpty {
                App.medicinReminder.med_endDate = App.medicinReminder.med_startDate
            }
            let set_med_start = reminder.med_startDate
            let set_med_end = reminder.med_endDate
            let arrStrStart : NSArray = set_med_start.components(separatedBy: ",") as NSArray
            let arrStrEnd : NSArray = set_med_end.components(separatedBy: ",") as NSArray
            
            for (index,objStart) in arrStrStart.enumerated(){
                
                //let arrDates = Date().arrayBetweenTwoDates(startDate: self.UTCToLocalDate(date: reminder.med_startDate), endDate: self.UTCToLocalDate(date: reminder.med_endDate))
                let arrDates = Date().arrayBetweenTwoDates(startDate: self.dateStringToDate(string: objStart as! String, format: "yyyy-MM-dd HH:mm:ss"), endDate: self.dateStringToDate(string: arrStrEnd[index] as! String, format: "yyyy-MM-dd HH:mm:ss"))
                
                print("arrDates to remove: ",arrDates.debugDescription)
                
                for (index1,dateTemp) in arrDates.enumerated(){
                    let week = Calendar.current.component(.weekday, from: dateTemp)
                    if reminder.med_week[week-1] == 1 {
                        /*
                         let center = UNUserNotificationCenter.current()
                         //center.removeAllDeliveredNotifications()
                         //center.removePendingNotificationRequests(withIdentifiers: ["MedicationAlert\(reminder.id)\(reminder.med_id)\(index+1)"])
                         print("remove notification : ","MedicationAlert\(reminder.id)\(index+1)")
                         center.removePendingNotificationRequests(withIdentifiers: ["MedicationAlert\(reminder.id)\(index+1)"])*/
                        
                        let set_med_addOn = App.medicinReminder.med_addOn
                        var set_addOn_time = ""
                        let arrStr1 : NSArray = set_med_addOn.components(separatedBy: ",") as NSArray
                        //for obj in arrStr1{
                        let strObj = (arrStr1.count == 1) ? arrStr1[0] as! String : arrStr1[index] as! String
                        set_addOn_time = strObj
                        let trimmedString = set_addOn_time.trimmingCharacters(in: .whitespaces)
                        print("remove notification : ","MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)")
                        cancelPendingNotifications(identifier: "MedicationAlert \(reminder.id) \(index1+1) \(trimmedString)")
                        //}
                        /*
                         if arrStr1.count == 0
                         {
                         let trimmedString = set_med_addOn.trimmingCharacters(in: .whitespaces)
                         print("remove notification : ","MedicationAlert\(reminder.id)\(index+1)\(trimmedString)")
                         cancelPendingNotifications(identifier: "MedicationAlert\(reminder.id)\(index+1)\(trimmedString)")
                         }*/
                        //cancelPendingNotifications(identifier: "MedicationAlert\(reminder.id)\(index+1)")
                    }
                }
            }
        }
    }
    
    func cancelPendingNotifications(identifier:String)
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
            var identifiers: [String] = []
            for notification:UNNotificationRequest in notificationRequests {
                if notification.identifier == identifier {
                    identifiers.append(notification.identifier)
                }
            }
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
        }
    }
    
    func loadImageFromDocumentDirectory(nameOfImage : String) -> UIImage {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(nameOfImage)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if(image == nil)
            {
                return UIImage.init(named: "order_med.png")!
            }
            return image!
        }
        return UIImage.init(named: "order_med.png")!
    }
    
    func insertTakenDB(isTaken:Bool) -> Bool {
        
        guard let db = try? Connection(App.dbFileURL.path) else {
            print("Not connected to db")
            return false
        }
        print("connected to db")
        
        var set_taken_date : String!
        var set_taken_time : String!
        set_taken_date  = self.dateString(date:currentDate().dateOnly(), format:"yyyy-MM-dd HH:mm:ss")
        set_taken_time  = self.getCurrentTimeInString()
        let arrSep = set_taken_date.components(separatedBy: " ") as NSArray
        
        
        let insert = taken.insert(taken_date <- "\(arrSep[0])",taken_time <- set_taken_time,taken_state <- isTaken ? 1 : 2,taken_reminder_id <- App.medicinReminder.id,reminder_time <- App.medicinReminder.med_addOn)
        guard let rowid = try? db.run(insert) else{
            print("Not inserted")
            return false
        }
        
        print("row inserted",rowid)
        
        return true
    }
    
}
