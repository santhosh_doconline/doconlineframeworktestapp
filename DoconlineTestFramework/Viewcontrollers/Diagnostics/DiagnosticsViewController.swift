//
//  DiagnosticsViewController.swift
//  DocOnline
//
//  Created by Doconline India on 13/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import SwiftMessages
//import SDWebImage

extension UISearchBar {
    func removeBackgroundImageView(){
        if let view:UIView = self.subviews.first {
            for curr in view.subviews {
                guard let searchBarBackgroundClass = NSClassFromString("UISearchBarBackground") else {
                    return
                }
                if curr.isKind(of:searchBarBackgroundClass){
                    if let imageView = curr as? UIImageView{
                        if #available(iOS 13.0, *) {
                            imageView.alpha = 0.0
                            self.searchTextField.clearButtonMode = UITextField.ViewMode.never
                            self.searchTextField.background = nil
                            self.searchTextField.backgroundColor = UIColor.white
                        }
                        else{
                            imageView.removeFromSuperview()
                        }

                        break
                    }
                }
            }
        }
    }
    
    func customRoundRect() {
        if let textfield = self.value(forKey: "searchField") as? UITextField {
            //textfield.textColor = UIColor.blue
            if let backgroundview = textfield.subviews.first {
                // Background color
                backgroundview.backgroundColor = UIColor.white
                // Rounded corner
                backgroundview.layer.cornerRadius = 5;
                backgroundview.clipsToBounds = true;
            }
        }
    }
}

extension UIButton {
    private struct AssociatedKeys {
        static var DescriptiveName = "nsh_DescriptiveName"
        static var IndexID = "nsh_IndexID"
    }
    
    @IBInspectable var descriptiveDictionary: NSDictionary? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.DescriptiveName) as? NSDictionary
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.DescriptiveName,
                    newValue as NSDictionary?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC
                )
            }
        }
    }
    
    var indexTag: Int? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.IndexID) as? Int
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.IndexID,
                    newValue as Int?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC
                )
            }
        }
    }
}

class DiagnosticsViewController: UIViewController, UITabBarDelegate,DiagnosticsPlanDelegate,DiagnosticListCellDelegate,NVActivityIndicatorViewable {
    
    var storyboardIDs:[String] = ["DiagnosticsPlanViewController","DiagnosticsHistoryViewController"]
    var viewControllers:[UIViewController] = []
    var activeController:UIViewController? = nil
    
    
    @IBOutlet weak var vwGrad: UIView!
    @IBOutlet weak var vwInfo: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var imgWorks: UIImageView!
    @IBOutlet weak var heightOfVwGrad: NSLayoutConstraint!
    //@IBOutlet weak var tabbar: UITabBar!
    //@IBOutlet weak var childView: UIView!
    
    //instance variables
    ///instance to notification button for showing badge
    var btnBarBadge : MJBadgeBarButton!
    var packageID : Int!
    var expectedContentLength = 0
    ///linear bar instance reference
    //let linearBar: LinearProgressBar = LinearProgressBar()
    //let linearBar1: LinearProgressBar = LinearProgressBar()
    var arrB2B : NSMutableArray!
    var arrGeneral : NSMutableArray!
    var dictDetails : NSDictionary!
    var pageNo : Int = 1
    var totalPages : Int = 0
    var searchActive : Bool = false
    var b2b_company = ""
    ///flip timer
    var timer : Timer?
    var timeCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imgWorks.layer.cornerRadius = 8
        imgWorks.layer.masksToBounds = true
        //vwInfo.backgroundColor = UIColor.init(hexString: "#e5e5e5")
        vwInfo.isHidden = true
        setupNavigationBar()
        if #available(iOS 11, *) {
            //nothing to do
        }else {
            self.searchBar.scopeBarBackgroundImage = UIImage()
        }
        self.searchBar.removeBackgroundImageView()
        self.searchBar.customRoundRect()
        
        arrB2B = NSMutableArray()
        arrGeneral = NSMutableArray()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNotificationBarButton()
        searchBar.text = ""
        //pageNo = 1
        if arrB2B.count+arrGeneral.count == 0 {
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            getPlans(pageNumber: pageNo)
        }
        else
        {
            checkCartWithDiagnosticList()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("view will disappear called")
        
        heightOfVwGrad.constant = 44.0
        
        if self.timer != nil {
            self.timer?.invalidate()
            timer = nil
        }
        
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func checkCartWithDiagnosticList() {
        let arrList : NSMutableArray =  arrGeneral.mutableCopy() as! NSMutableArray
        let arrCart : NSMutableArray =  App.cartArray.mutableCopy() as! NSMutableArray
        arrGeneral.removeAllObjects()
        for objList in arrList {
            let dictList :NSDictionary = objList as! NSDictionary
            let dictList1 :NSMutableDictionary = dictList.mutableCopy() as! NSMutableDictionary
            let strIDList = String(describing: dictList1.object(forKey: "package_id") ?? "0")
            var itemInCart = false
            for objCart in arrCart {
                let dictCart :NSDictionary = objCart as! NSDictionary
                let strIDCart = String(describing: dictCart.object(forKey: "package_id") ?? "0")
                if(Int(strIDList)! == Int(strIDCart)!)
                {
                    itemInCart = true
                    break
                }
            }
            if itemInCart == true
            {
                dictList1.setValue(true, forKey: "in_cart")
            }
            else
            {
                dictList1.setValue(false, forKey: "in_cart")
            }
            arrGeneral.add(dictList1)
        }
        self.tableView.reloadData()
    }
    
    /**
     Instance notification button instantiation method
     */
    func setupNotificationBarButton() {
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onNotificationButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "cart_empty"), for: .normal)
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        self.btnBarBadge.badgeValue = String(describing: App.cartCount)
        self.navigationItem.rightBarButtonItem = self.btnBarBadge
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_TEST_SEGUE {
            let destVC = segue.destination as! DiagnosticsTestViewController
            //destVC.packageID = self.packageID
            destVC.dictPlans = dictDetails
        }
        else if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_PLAN_SEGUE {
            let destVC = segue.destination as! DiagnosticsPlanViewController
            //destVC.packageID = self.packageID
            destVC.dictPlans = dictDetails
        }
    }
    
    
    
    func diagnosticPlanDetails(packageID:Int) {
        self.packageID = packageID
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE, sender: self)
    }
    
    // MARK: - UIButton Action
    
    @IBAction func clickInfoButton(_ sender: UIButton) {
        //searchBar.text = ""
        searchBar.resignFirstResponder()
        //self.view.endEditing(true)
        vwInfo.isHidden = false
    }
    
    @IBAction func clickCloseInfoButton(_ sender: UIButton) {
        vwInfo.isHidden = true
    }
    
    @objc func onNotificationButtonClick() {
        print("Notification button Clicked ")
        searchBar.resignFirstResponder()
        //self.view.endEditing(true)
        vwInfo.isHidden = true
        if(App.cartCount != 0)
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_CART_SEGUE, sender: self)
        }
        else
        {
            self.didShowAlert(title: "Cart", message: "There are no items in cart")
        }
    }
    
    
    func clickCartBtn(sender: UIButton) {
        
        if sender.isSelected
        {
            self.didShowAlert(title: "Cart", message: "Item already added to cart")
        }
        else{
            print("sender.tag :",sender.tag)
            print("sender.descriptiveDictionary :",sender.descriptiveDictionary ?? [:])
            let packageID : String = String(describing: sender.descriptiveDictionary!.object(forKey: "package_id") ?? "0")
            let packagePrice: String = String(describing: sender.descriptiveDictionary!.object(forKey: "price") ?? "0.0")
            let partnerID: String = String(describing: sender.descriptiveDictionary!.object(forKey: "partner_id") ?? "0.0")
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            self.addToCart(packageName: sender.descriptiveDictionary!.object(forKey: "package_name") as! String,packageID: packageID,packagePrice: packagePrice,partnerId:partnerID,completion: {(done) in
                if(done)
                {
                    let indexPath = IndexPath(row: sender.indexTag!, section: 0)
                    let cell = self.tableView.cellForRow(at: indexPath) as! DiagnosticsListCell
                    cell.btnCart.isSelected = true
                    let arrTemp : NSMutableArray =  self.arrGeneral.mutableCopy() as! NSMutableArray
                    self.arrGeneral.removeAllObjects()
                    for obj in arrTemp {
                        let dict :NSDictionary = obj as! NSDictionary
                        let dictAdd :NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                        let strID = String(describing: dictAdd.object(forKey: "package_id") ?? "0")
                        if(cell.btnCart.tag == Int(strID)!)
                        {
                            dictAdd.setValue(true, forKey: "in_cart")
                        }
                        self.arrGeneral.add(dictAdd)
                    }
                    self.tableView.reloadData()
                    
                    //sender.isSelected = true
                    self.btnBarBadge.badgeValue = String(describing: App.cartCount )
                    self.didShowAlert(title: "Cart", message: "Added to cart")
                    
                }
            })
        }
        
        
        //let position: CGPoint = sender.convert(.zero, to: self.tableView)
        //let indexPath = self.tableView.indexPathForRow(at: position)
        //let cell = tableView.cellForRow(at: indexPath!) as! DiagnosticsListCell
        
    }
    
    // MARK: - Web services
    
    @objc func getPlans(pageNumber:Int) {
        
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            DispatchQueue.main.async(execute: {
                self.stopAnimating()
            })
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = "\(AppURLS.URL_Diagnostics_plans)/\(pageNumber)"
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.stopAnimating()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("resultJSON in list = \(resultJSON)")
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.didShowAlert(title: "", message: message)
                                        self.stopAnimating()
                                    })
                                }
                            }
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                let arrTemp : NSArray =  data.object(forKey: "b2b") as! NSArray
                                let arrTemp1 : NSArray =  data.object(forKey: "general") as! NSArray
                                self.totalPages = data.object(forKey: "total_pages") as! Int
                                self.arrB2B = arrTemp.mutableCopy() as! NSMutableArray
                                //self.arrGeneral = arrTemp1.mutableCopy() as! NSMutableArray
                                if(arrTemp.count != 0)
                                {
                                    self.b2b_company = data.object(forKey: "b2b_company") as! String
                                }
                                for obj in arrTemp1
                                {
                                    let dict :NSDictionary = obj as! NSDictionary
                                    self.arrGeneral.add(dict)
                                }
                                DispatchQueue.main.async(execute: {
                                    
                                    
                                    self.tableView.reloadData()
                                    self.stopAnimating()
                                    
                                    print("*1*1*1*1")
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    
                                    self.stopAnimating()
                                    
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    @objc func addToCart(packageName:String,packageID:String,packagePrice:String,partnerId:String,completion: @escaping (_ done:Bool) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            
            
            //self.startAnimating()
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            dataToPost = "\(Keys.KEY_PACKAGE_ID)=\(packageID)&\(Keys.KEY_PACKAGE_NAME)=\(packageName)&\(Keys.KEY_PACKAGE_PRICE)=\(packagePrice)&\(Keys.KEY_PARTNER_ID)=\(partnerId)"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_CART_ADD
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        
                        print("Error==> : \(error.localizedDescription)")
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        completion(false)
                                        self.stopAnimating()
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    App.cartCount = data.object(forKey: "updated_cart_count") as! Int
                                    completion(true)
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false)
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false)
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    @objc func searchPlans(strSearch:String, pageNumber:Int) {
        print("*****")
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async(execute: {
                self.stopAnimating()
            })
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = "\(AppURLS.URL_DIAGNOSTICS_SEARCH)\(strSearch)/\(pageNumber)"
            let escapedString = slotURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: escapedString!)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        
                        self.stopAnimating()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("resultJSON in list = \(resultJSON)")
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.didShowAlert(title: "", message: message)
                                        self.stopAnimating()
                                    })
                                }
                            }
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                let arrTemp : NSArray =  data.object(forKey: "b2b") as! NSArray
                                let arrTemp1 : NSArray =  data.object(forKey: "general") as! NSArray
                                self.totalPages = data.object(forKey: "total_pages") as! Int
                                self.arrB2B = arrTemp.mutableCopy() as! NSMutableArray
                                //self.arrGeneral = arrTemp1.mutableCopy() as! NSMutableArray
                                if self.pageNo == 1{self.arrGeneral.removeAllObjects()}
                                for obj in arrTemp1
                                {
                                    let dict :NSDictionary = obj as! NSDictionary
                                    self.arrGeneral.add(dict)
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    self.tableView.reloadData()
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    @objc func flip(sender: Timer)//(firstView:UIView,secondView:UIView)
    {
        timeCounter = timeCounter + 1
        //print("timeCounter : ",timeCounter)
        let dictObj = sender.userInfo as! Dictionary<String,AnyObject>
        let firstView : UIView = dictObj["firstView"] as! UIView
        let secondView : UIView = dictObj["secondView"] as! UIView
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .showHideTransitionViews]
        
        UIView.transition(with: firstView, duration: 1.0, options: transitionOptions, animations: {
            if(self.timeCounter == 1 || self.timeCounter % 2 != 0)
            {
                firstView.isHidden = true
            }
            else{
                firstView.isHidden = false
            }
        })
        
        UIView.transition(with: secondView, duration: 1.0, options: transitionOptions, animations: {
            if(self.timeCounter == 1 || self.timeCounter % 2 != 0)
            {
                secondView.isHidden = false
            }
            else
            {
                secondView.isHidden = true
            }
        })
    }
    
}
extension DiagnosticsViewController:UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if arrB2B.count+arrGeneral.count > 0 {
            self.view.endEditing(true)
        }
        print("pageNo :",pageNo," self.totalPages",self.totalPages)
        if(tableView.contentOffset.y==self.tableView.contentSize.height - self.tableView.frame.size.height) && self.totalPages > pageNo
        {
            print("table reached to bottom")
            pageNo = pageNo + 1
            print("pageNo :",pageNo)
            print("search",(searchBar.text)!)
            if searchActive || (searchBar.text)!.count >= 3{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                    self.searchPlans(strSearch: (self.searchBar.text)!, pageNumber: self.pageNo)
                }
            }
            else{
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                getPlans(pageNumber: pageNo)
            }
            
        }
    }
}

extension DiagnosticsViewController: UITableViewDelegate ,UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiagnosticsListCell", for: indexPath) as! DiagnosticsListCell
        cell.delegate = self
        cell.btnCheck.backgroundColor = Theme.buttonBackgroundColor
        if(arrB2B.count == 0)
        {
            if indexPath.row == 0
            {
                cell.widthOfImgPopConstraint.constant = 0.0//76.0
                cell.widthOfLblPopConstraint.constant = 0.0//76.0
                
                cell.mainBackground.isHidden = true
                cell.shadowLayer.isHidden = true
                
                cell.vwPackageList.isHidden = true
                cell.vwListHeader.isHidden = false
                cell.vwList.isHidden = true
            }
            else
            {
                cell.widthOfImgPopConstraint.constant = 0.0//76.0
                cell.widthOfLblPopConstraint.constant = 0.0//76.0
                
                cell.mainBackground.isHidden = false
                cell.shadowLayer.isHidden = false
                cell.vwPackageList.isHidden = true
                cell.vwListHeader.isHidden = true
                cell.vwList.isHidden = false
                
                let dict = arrGeneral.object(at: indexPath.row-1) as! NSDictionary
                print("dict :",dict)
                cell.lblName.text = "\(String(describing: dict.object(forKey: "package_name") ?? ""))\n\n\u{20B9}\(String(describing: dict.object(forKey: "price") ?? ""))"
                cell.btnCart.tag = dict.object(forKey: "package_id") as! Int
                cell.btnCart.descriptiveDictionary = dict
                cell.btnCart.indexTag = indexPath.row
                cell.btnCart.isSelected = dict.object(forKey: "in_cart") as! Bool
            }
        }
        else{
            if indexPath.row <= arrB2B.count-1//indexPath.row == 0
            {
                cell.widthOfImgPopConstraint.constant = 0.0//76.0
                cell.widthOfLblPopConstraint.constant = 0.0//76.0
                
                cell.mainBackground.isHidden = false
                cell.shadowLayer.isHidden = false
                cell.vwList.isHidden = true
                cell.vwPackageList.isHidden = false
                cell.vwListHeader.isHidden = true
                
                let dictB2B = arrB2B.object(at: indexPath.row) as! NSDictionary
                print("dictB2B :",dictB2B)
                cell.lblB2BName.text = "\(String(describing: dictB2B.object(forKey: "package_name") ?? ""))"
                cell.lblB2BCompany.text = self.b2b_company
                cell.lblB2BExpires.text = "Valid till \(String(describing: dictB2B.object(forKey: "expires_on") ?? ""))"
                let url = URL(string: dictB2B.object(forKey: "partner_img_url") as! String)
                cell.imgPartner.sd_setImage(with: url, completed: { (image, error, cache, url) in
                    if error != nil
                    {
                        print("imgPartner error")
                    }
                    else
                    {
                        print("imgPartner ok")
                    }
                })
                
            }
            else if indexPath.row == arrB2B.count//indexPath.row == 1
            {
                cell.widthOfImgPopConstraint.constant = 0.0//76.0
                cell.widthOfLblPopConstraint.constant = 0.0//76.0
                
                cell.mainBackground.isHidden = true
                cell.shadowLayer.isHidden = true
                cell.vwListHeader.isHidden = false
                
            }
            else
            {
                cell.widthOfImgPopConstraint.constant = 0.0//76.0
                cell.widthOfLblPopConstraint.constant = 0.0//76.0
                
                cell.mainBackground.isHidden = false
                cell.shadowLayer.isHidden = false
                cell.vwPackageList.isHidden = true
                cell.vwListHeader.isHidden = true
                cell.vwList.isHidden = false
                
                //let dict = arrGeneral.object(at: indexPath.row-2) as! NSDictionary
                let dict = arrGeneral.object(at: indexPath.row-(arrB2B.count+1)) as! NSDictionary
                print("dict :",dict)
                cell.lblName.text = "\(String(describing: dict.object(forKey: "package_name") ?? ""))\n\n\u{20B9}\(String(describing: dict.object(forKey: "price") ?? ""))"
                cell.btnCart.tag = dict.object(forKey: "package_id") as! Int
                cell.btnCart.indexTag = indexPath.row
                cell.btnCart.isSelected = dict.object(forKey: "in_cart") as! Bool
                
            }
        }
        
        cell.mainBackground.layer.cornerRadius = 5
        cell.mainBackground.layer.masksToBounds = true
        
        cell.shadowLayer.setShadowView(opacity: 0.4, radius: 8)
        
        cell.vwVL.setShadowView(opacity: 0.4, radius: 1)
        
        
        cell.selectionStyle = .none
        
        return cell
    }
    /*
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     if(arrB2B.count > 0)
     {
     let cellVw = cell as! DiagnosticsListCell
     if indexPath.row == 0
     {
     //self.flip(firstView: cellVw.vwBottomBG, secondView: cellVw.vwBook)
     if self.timer != nil {
     self.timer?.invalidate()
     timer = nil
     timeCounter = 0
     }
     timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.flip(sender:)), userInfo: ["firstView":cellVw.vwBottomBG,"secondView":cellVw.vwBook], repeats: true)
     }
     }
     }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //let cell = tableView.cellForRow(at: indexPath as IndexPath) as! DiagnosticsListCell
        
        if(arrB2B.count == 0)
        {
            if indexPath.row == 0
            {
            }
            else
            {
                dictDetails = arrGeneral.object(at: indexPath.row-1) as! NSDictionary
                print("dict :",dictDetails)
                if(dictDetails.object(forKey: "package_tests") != nil)
                {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_PLAN_SEGUE, sender: self)
                    
                }
                else
                {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_PLAN_SEGUE, sender: self)
                    //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_TEST_SEGUE, sender: self)
                    
                }
                UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER)
            }
        }
        else{
            if indexPath.row <= arrB2B.count-1//indexPath.row == 0
            {
                dictDetails = arrB2B.object(at: indexPath.row) as! NSDictionary
                print("dict :",dictDetails)
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_PLAN_SEGUE, sender: self)
                UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER)
            }
            else if indexPath.row == arrB2B.count//indexPath.row == 1
            {
                
            }
            else
            {
                dictDetails = arrGeneral.object(at: indexPath.row-(arrB2B.count+1)) as! NSDictionary
                print("dict :",dictDetails)
                if(dictDetails.object(forKey: "package_tests ") != nil)
                {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_PLAN_SEGUE, sender: self)
                    
                }
                else
                {
                    //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_TEST_SEGUE, sender: self)
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_PLAN_SEGUE, sender: self)
                }
                UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(arrB2B.count == 0)
        {return arrGeneral.count + 1}
        else if(arrB2B.count == 0 && arrGeneral.count == 0)
        {return 0}
        else
        {return arrB2B.count + arrGeneral.count + 1}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt IndexPath:IndexPath) -> CGFloat
    {
        if(arrB2B.count == 0)
        {
            if IndexPath.row == 0
            {
                return 60
            }
            else
            {
                return 110
            }
        }
        else if(arrB2B.count == 0 && arrGeneral.count == 0)
        {
            return 0
        }
        else
        {
            if IndexPath.row <= arrB2B.count-1//indexPath.row == 0
            {
                return 230
            }
            else if IndexPath.row == arrB2B.count//indexPath.row == 1
            {
                return 60
            }
            else
            {
                return 110
            }
        }
    }
}

extension DiagnosticsViewController:UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        heightOfVwGrad.constant = 64.0
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        heightOfVwGrad.constant = 44.0
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        heightOfVwGrad.constant = 44.0
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.endEditing(true)
        heightOfVwGrad.constant = 44.0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        heightOfVwGrad.constant = 64.0
        
        let trimmed = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        arrGeneral.removeAllObjects()
        arrB2B.removeAllObjects()
        pageNo = 1
        totalPages = 1
        if(searchText.count >= 3){
            heightOfVwGrad.constant = 44.0
            searchActive = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                self.searchPlans(strSearch: (self.searchBar.text)!, pageNumber: self.pageNo)
            }
        } else if(searchText.count == 0) {
            searchActive = false
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            getPlans(pageNumber: pageNo)
        }
        self.tableView.reloadData()
    }
}

protocol DiagnosticListCellDelegate: class
{
    func clickCartBtn(sender:UIButton)
}

class DiagnosticsListCell: UITableViewCell {
    
    @IBOutlet weak var heightOfLblTxtConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwListBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwPackageListBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthOfImgPopConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthOfLblPopConstraint: NSLayoutConstraint!
    @IBOutlet var mainBackground: UIView!
    @IBOutlet var vwList: UIView!
    @IBOutlet var vwPackageList: UIView!
    @IBOutlet var vwListHeader: UIView!
    @IBOutlet var vwBottomBG: UIView!
    @IBOutlet var vwBook: UIView!
    @IBOutlet var vwHL: UIView!
    @IBOutlet var vwVL: UIView!
    @IBOutlet var vwOffered: UIView!
    @IBOutlet var imgPartner: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblB2BName: UILabel!
    @IBOutlet var lblB2BExpires: UILabel!
    @IBOutlet var lblB2BCompany: UILabel!
    @IBOutlet var btnCheck: UIButton!
    @IBOutlet var btnCart: UIButton!
    @IBOutlet var shadowLayer: UIView!
    @IBOutlet var imgPackage: UIImageView!
    weak var delegate: DiagnosticListCellDelegate? = nil
    
    
    @IBAction func clickCheckBtn(_ sender: UIButton) {
        print("selected")
        //delegate?.ClickBtnCheck(sender: sender)
    }
    
    @IBAction func clickCartBtn(_ sender: UIButton) {
        print("selected")
        delegate?.clickCartBtn(sender: sender)
        //sender.isSelected = true
        //print("sender.tag :",sender.tag)
        
    }
    
}

extension UIView
{
    func setShadowView(opacity : Float, radius : Float)
    {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 8
    }
}


