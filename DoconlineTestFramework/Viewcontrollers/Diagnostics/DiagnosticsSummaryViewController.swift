//
//  DiagnosticsSummaryViewController.swift
//  DocOnline
//
//  Created by Doconline india on 19/03/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
//import FlexibleSteppedProgressBar

class DiagnosticsSummaryViewController: UIViewController, FlexibleSteppedProgressBarDelegate, NVActivityIndicatorViewable, DiagnosticReportsCellDelegate {
    @IBOutlet var vwPackageList: UIView!
    @IBOutlet var vwPackageShadow: UIView!
    @IBOutlet var vwHL: UIView!
    @IBOutlet var vwVL: UIView!
    @IBOutlet var vwOffered: UIView!
    @IBOutlet var vwScr: UIView!
    @IBOutlet var imgPartner: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgPackage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableOrder: UITableView!
    @IBOutlet var vwtable: UIView!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblOrderHConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwTblConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwScrConstraint: NSLayoutConstraint!
    @IBOutlet var btnCall: UIButton!
    @IBOutlet var vwCustomerSupport: UIView!
    @IBOutlet var vwCustomerSubView: UIView!
    @IBOutlet var lblStatusHeader: UILabel!
    @IBOutlet var lblStatusTitle: UILabel!
    @IBOutlet var lblAgentNumber: UILabel!
    @IBOutlet var vwStatus: UIView!
    @IBOutlet var vwStatusSubView: UIView!
    @IBOutlet weak var vwAgentHeight: NSLayoutConstraint!
    
    var dictTest : NSDictionary!
    var progressBar: FlexibleSteppedProgressBar!
    
    /// Creating UIDocumentInteractionController instance.
    let documentInteractionController = UIDocumentInteractionController()
    
    var arrReports = NSMutableArray()
    var arrTracking = NSMutableArray()
    var dictTSP : NSDictionary!
    let arrImages = ["yet.png","assigned.png","started.png","serviced.png"]//["yet.png","assigned.png","started.png","arrived.png","serviced.png"]
    let arrImages_gray = ["yet.png","assigned_gray.png","started_gray.png","serviced_gray.png"]//["yet.png","assigned_gray.png","started_gray.png","arrived_gray.png","serviced_gray.png"]
    let arrStatusDesc = ["Your Order will be assigned to one of the pick up Agents shortly","Your Order has been assigned to our pick up Agent","Your Order has been serviced by the pick up Agent","The sample collected has been sent to the Laboratory for testing. Results will be available shortly"]
    
    var currentIndex = 0
    var currentReportIndex = 0
    var appointmentStatus = "Unknown"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.sectionHeaderHeight = 0
        self.tableOrder.sectionHeaderHeight = 50
        
        vwCustomerSupport.isHidden = true
        vwCustomerSubView.roundCorners(topLeft: 20, topRight: 20, bottomLeft: 10, bottomRight: 10)
        
        vwStatus.isHidden = true
        vwStatusSubView.roundCorners(topLeft: 20, topRight: 20, bottomLeft: 10, bottomRight: 10)
        
        /// Setting UIDocumentInteractionController delegate.
        documentInteractionController.delegate = self
        
        // Do any additional setup after loading the view.
        lblName.text = "\(String(describing: dictTest.object(forKey: "package_name") ?? ""))\n\n\(String(describing: dictTest.object(forKey: "apt_id") ?? ""))"
        
        vwPackageList.layer.cornerRadius = 5
        vwPackageList.layer.masksToBounds = true
        
        vwPackageShadow.setShadowView(opacity: 0.4, radius: 8)
        vwVL.setShadowView(opacity: 0.23, radius: 8)
        
        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        self.getAppointmentSummary { (done, dictDefault) in
            if(done)
            {
                
                print("dictDefault :",dictDefault)
            
                if let data = dictDefault.object(forKey: Keys.KEY_REPORTS)  {
                    // now val is not nil and the Optional has been unwrapped, so use it
                    let data1 = data as! NSArray
                    self.arrReports = data1.mutableCopy() as! NSMutableArray
                }
                
                //let dataTrack = dictDefault.object(forKey: Keys.KEY_TRACKING) as! NSArray
                
                let dataTrack1 = dictDefault.object(forKey: Keys.KEY_TRACKING) as! NSDictionary
                var sortedKeys = dataTrack1.allKeys
                var sortIntKeys = [Int]()
                
                for i in 0..<sortedKeys.count {
                    let intKey = Int(sortedKeys[i] as! String)!
                    sortIntKeys.append(intKey)
                }
                sortIntKeys = sortIntKeys.sorted(by: <)
                for i in 0..<sortIntKeys.count {
                    
                    self.arrTracking.add(dataTrack1.object(forKey: String(describing:sortIntKeys[i])) ?? "")
                    
                }
                print("self.arrTracking :",self.arrTracking)
                for i in 0..<sortIntKeys.count {
                    let dict = self.arrTracking.object(at: i) as! NSDictionary
                    if (dict.value(forKey: "date_time") as? String) != ""
                    {
                        self.currentIndex = i
                    }
                }
                
                
                //self.arrTracking = dataTrack.mutableCopy() as! NSMutableArray
                self.dictTSP = dictDefault.object(forKey: Keys.KEY_TSP) as? NSDictionary
                
                for i in 0..<self.arrReports.count {
                    let dict = self.arrReports.object(at: i) as! NSDictionary
                    let strUrl = dict.value(forKey: "report_url") as? String
                    if(strUrl?.isEmpty == true)
                    {
                        
                    }
                    else
                    {
                        self.currentReportIndex = self.currentReportIndex+1
                    }
                }
                
                let dataAppointment = dictDefault.object(forKey: Keys.KEY_APPOINTMENT_DETAILS) as! NSDictionary
                if (dataAppointment.value(forKey: "status") as? String) == "Cancelled"
                {
                    self.currentReportIndex = self.arrReports.count
                    self.appointmentStatus = "Cancelled"
                }
                else if (dataAppointment.value(forKey: "status") as? String) == "Done"
                {
                    self.appointmentStatus = "Done"
                }
                else
                {
                    self.appointmentStatus = "Unknown"
                }
                
                self.tableView.reloadData()
                if(self.currentReportIndex == self.arrReports.count)
                {
                    self.arrTracking.removeAllObjects()
                }
                else{
                }
                self.tableOrder.reloadData()
                
                if(self.arrReports.count == 0)
                {
                    self.tblHeightConstraint.constant = 0
                    self.vwTblConstraint.constant = 0
                    self.vwScrConstraint.constant = 300
                }
                /*
                self.progressBar = FlexibleSteppedProgressBar()
                self.progressBar.translatesAutoresizingMaskIntoConstraints = false
                self.vwScr.addSubview(self.progressBar)
                
                let horizontalConstraint = self.progressBar.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
                let verticalConstraint = self.progressBar.topAnchor.constraint(
                    equalTo: self.tableView.bottomAnchor,
                    constant: self.arrReports.count == 0 ? 100 : self.tblHeightConstraint.constant+60
                )
                
                let widthConstraint = self.progressBar.widthAnchor.constraint(equalToConstant: 300)
                let heightConstraint = self.progressBar.heightAnchor.constraint(equalToConstant: 100)
                NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
                
                
                // Customise the progress bar here
                self.progressBar.numberOfPoints = self.arrTracking.count
                self.progressBar.lineHeight = 9
                self.progressBar.radius = 10
                self.progressBar.progressRadius = 20
                self.progressBar.textDistance = 30
                self.progressBar.progressLineHeight = 3
                self.progressBar.currentSelectedCenterColor = UIColor.init(hexString: "#6dbe45")
                self.progressBar.selectedOuterCircleStrokeColor = UIColor.init(hexString: "#6dbe45")
                self.progressBar.lastStateOuterCircleStrokeColor  = UIColor.init(hexString: "#6dbe45")
                self.progressBar.lastStateCenterColor  = UIColor.init(hexString: "#6dbe45")
                self.progressBar.selectedBackgoundColor = UIColor.init(hexString: "#6dbe45")
                self.progressBar.backgroundShapeColor = UIColor.init(hexString: "#6dbe45")//UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 0.8)
                self.progressBar.viewBackgroundColor = UIColor.init(hexString: "#6dbe45")//UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 0.8)
                self.progressBar.currentIndex = 2
                self.progressBar.delegate = self
                
                self.progressBar.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi/2)
                //setAnchorPoint(anchorPoint: CGPoint(x: 0.5, y: -1), forView: progressBar)
                self.setAnchorePosition()*/
            }
            
        }
        
        //self.addShadow(toViews:[self.tableView])
        //self.tableView.setShadowView(opacity: 0.4, radius: 8)
        self.tableView.layer.cornerRadius = 5
        self.tableView.layer.masksToBounds = true
        
        self.vwtable.setShadowView(opacity: 0.4, radius: 8)
        
    }
    func addShadow(toViews:[UIView]) {
        for view in toViews {
            view.layer.shadowRadius = 1
            view.layer.shadowOpacity = 0.5
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
            view.clipsToBounds = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear....")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear....")
        /*
        var intCount = 0
        for layer: CALayer in progressBar.layer.sublayers! {
            print("CALayer :",layer.description)
            if layer.isKind(of: CATextLayer.self)
            {
                print("CATextLayer....")
                layer.transform = CATransform3DMakeRotation(-CGFloat.pi/2, 0.0, 0.0, 1.0)
                
                let oldFrame = layer.frame
                //layer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
                
                layer.anchorPoint = CGPoint(x: 1, y: 1)
                var yValue : CGFloat = 130.0
                if intCount == 0{
                    yValue = 140
                }
                else if intCount == 1{
                    yValue = 115
                }
                else if intCount == 2{
                    yValue = 150
                }
                else if intCount == 3{
                    yValue = 160
                }
                else if intCount == 4{
                    yValue = 140
                }
                
                layer.frame = CGRect(x: oldFrame.origin.x, y: oldFrame.origin.y-yValue, width: oldFrame.size.width, height: oldFrame.size.height)//oldFrame
                
                intCount = intCount + 1
            }
        }*/
    }
    
    func setAnchorePosition()
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                setAnchorPoint(anchorPoint: CGPoint(x: 0.5, y: -0.75), forView: progressBar)
            case 1334:
                print("iPhone 6/6S/7/8")
                setAnchorPoint(anchorPoint: CGPoint(x: 0.5, y: -1), forView: progressBar)
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                setAnchorPoint(anchorPoint: CGPoint(x: 0.5, y: -1), forView: progressBar)
            case 2436:
                print("iPhone X")
                setAnchorPoint(anchorPoint: CGPoint(x: 0.5, y: -1), forView: progressBar)
            default:
                print("unknown")
            }
        }
    }
    
    func setAnchorPoint(anchorPoint: CGPoint, forView view: UIView) {
        var newPoint = CGPoint(x: view.bounds.size.width * anchorPoint.x, y: view.bounds.size.height * anchorPoint.y)
        var oldPoint = CGPoint(x: view.bounds.size.width * view.layer.anchorPoint.x, y: view.bounds.size.height * view.layer.anchorPoint.y)
        
        newPoint = newPoint.applying(view.transform)
        oldPoint = oldPoint.applying(view.transform)
        
        var position = view.layer.position
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        view.layer.position = position
        view.layer.anchorPoint = anchorPoint
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIButton Action
    
    func ClickBtnReport(sender:UIButton) {
        let urlReport: String = String(describing: sender.descriptiveDictionary!.object(forKey: "report_url") ?? "")
        if(urlReport.isEmpty == true)
        {
            let alert = UIAlertController(title: "Report", message: "No Reports Available For This User", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
            }
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            //let urlReport = dict.value(forKey: "report_url") as? String
            self.storeAndShare(withURLString: urlReport)
        }
        
    }
    
    @IBAction func ClickBtnCall(_ sender: UIButton) {
        vwCustomerSupport.isHidden = false
    }
    
    @IBAction func ClickCallCustomer(_ sender: UIButton) {
        
        vwCustomerSupport.isHidden = true
        if let url = URL(string: "tel://02230900000"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (success) in
                print("Call action success:\(success)")
            })
        }else {
            print("Canot open:02230900000")
        }
        
    }
    @IBAction func ClickBtnCancel(_ sender: UIButton) {
        vwCustomerSupport.isHidden = true
    }
    @IBAction func ClickBtnStatusOK(_ sender: UIButton) {
        vwStatus.isHidden = true
    }
    @IBAction func ClickCallAgent(_ sender: UIButton) {
        vwStatus.isHidden = true
        let strUrl = dictTSP.value(forKey: "tsp_mobile") as? String ?? ""
        if strUrl != "" {
            let trimmedSpaces = strUrl.replacingOccurrences(of: " ", with: "")
            if let url = URL(string: "tel://\(trimmedSpaces)"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (success) in
                    print("Call action success:\(success)")
                })
            }else {
                print("Canot open:\(trimmedSpaces)")
            }
        }
        
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - FlexibleSteppedProgressBarDelegate
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,didSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,willSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,canSelectItemAtIndex index: Int) -> Bool {
        return false
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        if position == .top {
            print("Index : ",index)
            let dict = self.arrTracking.object(at: index) as! NSDictionary
            if (dict.value(forKey: "date_time") as? String) == ""
            {
                return "  \(dict.value(forKey: "status") as? String ?? "")"
            }
            return "  \(dict.value(forKey: "status") as? String ?? "")\n  \(dict.value(forKey: "date_time") as? String ?? "")"
            /*
            switch index {
                
            case 0: return "Order Received"
            case 1: return "Order Placed"
            case 2: return "Order Confirmed"
            case 3: return "Order Dispatched Order Dispatched"
            case 4: return "Order Delivered"
            case 5: return "Yet To Assign"
            default: return "Date"
                
            }*/
        }
        return ""
    }
    
    // MARK: - Web services
    
    @objc func getAppointmentSummary(completion: @escaping (_ done:Bool,_ dictDefault:NSDictionary) -> ()) {
        print("getAppointmentSummary")
        if !NetworkUtilities.isConnectedToNetwork()
        {
            DispatchQueue.main.async {
                self.stopAnimating()
                completion(false,[:])
            }
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_DIAGNOSTICS_APPOINTMENT_SUMMARY + String(describing: self.dictTest.object(forKey: "apt_id") ?? "")
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    //self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        completion(false,[:])
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.stopAnimating()
                    })
                    
                }
                if let data = data
                {
                    print("Appointment Data =\(data)")
                }
                if let response = response
                {
                    print("Appointment url = \(response.url!)")
                    print("Appointment response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("Appointment response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        
                                        completion(false,[:])
                                        let alert = UIAlertController(title: "Appointment Summary", message: message, preferredStyle: UIAlertController.Style.alert)
                                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                                        }
                                        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                        
                                        alert.addAction(okAction)
                                        alert.addAction(cancel)
                                        self.present(alert, animated: true, completion: nil)
                                        self.stopAnimating()
                                    })
                                }
                            }
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                
                                DispatchQueue.main.async(execute: {
                                    completion(true,data)
                                    self.stopAnimating()
                                    
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    completion(false,[:])
                                    self.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            completion(false,[:])
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
}

public struct ConsecutiveSequence<T: IteratorProtocol>: IteratorProtocol, Sequence {
    private var base: T
    private var index: Int
    private var previous: T.Element?
    
    init(_ base: T) {
        self.base = base
        self.index = 0
    }
    
    public typealias Element = (T.Element, T.Element)
    
    public mutating func next() -> Element? {
        guard let first = previous ?? base.next(), let second = base.next() else {
            return nil
        }
        
        previous = second
        
        return (first, second)
    }
}

extension Sequence {
    public func makeConsecutiveIterator() -> ConsecutiveSequence<Self.Iterator> {
        return ConsecutiveSequence(self.makeIterator())
    }
}

extension DiagnosticsSummaryViewController {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        
        let tmpDirURL = FileManager.default.temporaryDirectory.appendingPathComponent(url.lastPathComponent)
        
        if FileManager.default.fileExists(atPath: tmpDirURL.path) {
            print("File existes at path")
            DispatchQueue.main.async {
                self.share(url: tmpDirURL)
            }
        }else {
            /// START YOUR ACTIVITY INDICATOR HERE
            self.startAnimating()
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                    return
                }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.pdf")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    self.stopAnimating()
                    self.share(url: tmpURL)
                }
                }.resume()
        }
    }
}

extension DiagnosticsSummaryViewController: UIDocumentInteractionControllerDelegate {
    
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

protocol DiagnosticReportsCellDelegate: class
{
    func ClickBtnReport(sender:UIButton)
}

class DiagnosticReportsCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnReport: UIButton!
    weak var delegate: DiagnosticReportsCellDelegate? = nil
    
    @IBAction func ClickReportBtn(_ sender: UIButton) {
        print("selected")
        delegate?.ClickBtnReport(sender: sender)
    }
    
}

class DiagnosticOrderCell: UITableViewCell {
    
    @IBOutlet var lblOrder: UILabel!
    @IBOutlet var imgOrder: UIImageView!

}

extension DiagnosticsSummaryViewController: UITableViewDelegate ,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView == tableOrder)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderList", for: indexPath) as! DiagnosticOrderCell
            cell.imgOrder.image = (indexPath.row > currentIndex) ? (UIImage(named:arrImages_gray[indexPath.row])) : (UIImage(named:arrImages[indexPath.row]))
            
            let dict = self.arrTracking.object(at: indexPath.row) as! NSDictionary
            
            if (dict.value(forKey: "date_time") as? String) == ""
            {
                cell.lblOrder.text =  "\(dict.value(forKey: "status") as? String ?? "")"
                cell.lblOrder.textColor = UIColor.init(hexString: "#D8D8D8")
                if indexPath.row <= currentIndex {
                    cell.lblOrder.textColor = UIColor.init(hexString: "#000000")
                }
            }
            else
            {
                let strDate = self.dateString(date:self.dateStringToDate(string: dict.value(forKey: "date_time") as? String ?? "", format: "yyyy-MM-dd HH:mm:ss"), format:"dd MMM yy")
                cell.lblOrder.text = "\(dict.value(forKey: "status") as? String ?? "")\n\(strDate)"
                cell.lblOrder.textColor = UIColor.init(hexString: "#000000")
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reportsList", for: indexPath) as! DiagnosticReportsCell
            cell.delegate = self
            let dict = self.arrReports.object(at: indexPath.row) as! NSDictionary
            cell.lblName.text = dict.value(forKey: "user") as? String
            cell.btnReport.tag = indexPath.row
            cell.btnReport.descriptiveDictionary = dict
            let strUrl = dict.value(forKey: "report_url") as? String
            if(strUrl?.isEmpty == true)
            {
                cell.btnReport.isEnabled = true
                cell.btnReport.setTitleColor(UIColor.gray, for: .normal)
            }
            else
            {
                cell.btnReport.isEnabled = true
                cell.btnReport.setTitleColor(UIColor.init(hexString: "#6dbe45"), for: .normal)
            }
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == tableOrder)
        {
            let dict = self.arrTracking.object(at: indexPath.row) as! NSDictionary
            print("\(dict.value(forKey: "status") as? String ?? "") tapped")
            
            let dictTrack = self.arrTracking.object(at: indexPath.row) as! NSDictionary
            
            if (dictTrack.value(forKey: "date_time") as? String) == ""
            {}
            else
            {
                lblStatusHeader.text = dict.value(forKey: "status") as? String ?? ""
                lblStatusTitle.text = arrStatusDesc[indexPath.row]
                vwAgentHeight.constant = 0.0
                if(dict.value(forKey: "status") as? String == "Assigned")
                {
                   lblStatusTitle.text = "\(arrStatusDesc[indexPath.row]) \(dictTSP.value(forKey: "tsp_name") as? String ?? "")"
                    lblAgentNumber.text = dictTSP.value(forKey: "tsp_mobile") as? String ?? ""
                    vwAgentHeight.constant = 38.0
                }
            
                vwStatus.isHidden = false
            }
        }
        else{
            let dict = self.arrReports.object(at: indexPath.row) as! NSDictionary
            let strUrl = dict.value(forKey: "report_url") as? String
            if(strUrl?.isEmpty == true)
            {
                let alert = UIAlertController(title: "Report", message: "No Reports Available For This User", preferredStyle: UIAlertController.Style.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                }
            
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let urlReport = dict.value(forKey: "report_url") as? String
                self.storeAndShare(withURLString: urlReport ?? "")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == tableOrder)
        {
            return self.arrTracking.count
        }
        else{
            return self.arrReports.count//(self.arrReports.count != 0) ? 1 : 0
        }
            
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(tableView == tableOrder)
        {
            tblOrderHConstraint.constant = self.tableOrder.contentSize.height
            print("self.tableOrder.contentSize.height",self.tableOrder.contentSize.height)
            vwScrConstraint.constant = tblHeightConstraint.constant+tblOrderHConstraint.constant+40
        }
        else
        {
            tblHeightConstraint.constant = self.tableView.contentSize.height
            vwTblConstraint.constant = tblHeightConstraint.constant-6
            vwScrConstraint.constant = tblHeightConstraint.constant
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == tableOrder)
        {
            if indexPath.row == arrTracking.count-1 {
                return 58.0
            }
            return 80.0
        }
        else{
            return 45.0
        }
        //return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(tableView == tableOrder)
        {
            let headerView:UIView =  UIView(frame: CGRect(x: 0, y: 0, width: self.tableOrder.frame.width, height: 60))
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableOrder.frame.width, height: 60))
            if(arrReports.count != 0)
            {
                label.text = (arrTracking.count != 0) ? "Track your Order here" : "Your Reports have been generated. Please click on the \"Test Result\" button to view your report"
                if appointmentStatus == "Cancelled"
                {
                    label.text = "You appointment has been Cancelled"
                }
            }
            else
            {
                label.text = ""
            }
            label.font = UIFont.systemFont(ofSize: 16)
            label.textColor = UIColor.black
            label.numberOfLines = 3
            label.textAlignment = .center
            headerView.addSubview(label)
            return headerView
        }
        else{
            let headerView:UIView =  UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView == tableOrder)
        {
            return 60.0
        }
        else
        {
            return 0.0
        }
    }
}

extension UITableView
{
    /// Calls reloadsData() on self, and ensures that the given closure is
    /// called after reloadData() has been completed.
    ///
    /// Discussion: reloadData() appears to be asynchronous. i.e. the
    /// reloading actually happens during the next layout pass. So, doing
    /// things like scrolling the collectionView immediately after a
    /// call to reloadData() can cause trouble.
    ///
    /// This method uses CATransaction to schedule the closure.
    
    func reloadDataThenPerform(_ closure: @escaping (() -> Void))
    {
        CATransaction.begin()
        CATransaction.setCompletionBlock(closure)
        self.reloadData()
        CATransaction.commit()
    }
}

extension UIBezierPath {
    convenience init(shouldRoundRect rect: CGRect, topLeftRadius: CGSize = .zero, topRightRadius: CGSize = .zero, bottomLeftRadius: CGSize = .zero, bottomRightRadius: CGSize = .zero){
        
        self.init()
        
        let path = CGMutablePath()
        
        let topLeft = rect.origin
        let topRight = CGPoint(x: rect.maxX, y: rect.minY)
        let bottomRight = CGPoint(x: rect.maxX, y: rect.maxY)
        let bottomLeft = CGPoint(x: rect.minX, y: rect.maxY)
        
        if topLeftRadius != .zero{
            path.move(to: CGPoint(x: topLeft.x+topLeftRadius.width, y: topLeft.y))
        } else {
            path.move(to: CGPoint(x: topLeft.x, y: topLeft.y))
        }
        
        if topRightRadius != .zero{
            path.addLine(to: CGPoint(x: topRight.x-topRightRadius.width, y: topRight.y))
            path.addCurve(to:  CGPoint(x: topRight.x, y: topRight.y+topRightRadius.height), control1: CGPoint(x: topRight.x, y: topRight.y), control2:CGPoint(x: topRight.x, y: topRight.y+topRightRadius.height))
        } else {
            path.addLine(to: CGPoint(x: topRight.x, y: topRight.y))
        }
        
        if bottomRightRadius != .zero{
            path.addLine(to: CGPoint(x: bottomRight.x, y: bottomRight.y-bottomRightRadius.height))
            path.addCurve(to: CGPoint(x: bottomRight.x-bottomRightRadius.width, y: bottomRight.y), control1: CGPoint(x: bottomRight.x, y: bottomRight.y), control2: CGPoint(x: bottomRight.x-bottomRightRadius.width, y: bottomRight.y))
        } else {
            path.addLine(to: CGPoint(x: bottomRight.x, y: bottomRight.y))
        }
        
        if bottomLeftRadius != .zero{
            path.addLine(to: CGPoint(x: bottomLeft.x+bottomLeftRadius.width, y: bottomLeft.y))
            path.addCurve(to: CGPoint(x: bottomLeft.x, y: bottomLeft.y-bottomLeftRadius.height), control1: CGPoint(x: bottomLeft.x, y: bottomLeft.y), control2: CGPoint(x: bottomLeft.x, y: bottomLeft.y-bottomLeftRadius.height))
        } else {
            path.addLine(to: CGPoint(x: bottomLeft.x, y: bottomLeft.y))
        }
        
        if topLeftRadius != .zero{
            path.addLine(to: CGPoint(x: topLeft.x, y: topLeft.y+topLeftRadius.height))
            path.addCurve(to: CGPoint(x: topLeft.x+topLeftRadius.width, y: topLeft.y) , control1: CGPoint(x: topLeft.x, y: topLeft.y) , control2: CGPoint(x: topLeft.x+topLeftRadius.width, y: topLeft.y))
        } else {
            path.addLine(to: CGPoint(x: topLeft.x, y: topLeft.y))
        }
        
        path.closeSubpath()
        cgPath = path
    }
}

extension UIView{
    func roundCorners(topLeft: CGFloat = 0, topRight: CGFloat = 0, bottomLeft: CGFloat = 0, bottomRight: CGFloat = 0) {//(topLeft: CGFloat, topRight: CGFloat, bottomLeft: CGFloat, bottomRight: CGFloat) {
        let topLeftRadius = CGSize(width: topLeft, height: topLeft)
        let topRightRadius = CGSize(width: topRight, height: topRight)
        let bottomLeftRadius = CGSize(width: bottomLeft, height: bottomLeft)
        let bottomRightRadius = CGSize(width: bottomRight, height: bottomRight)
        let maskPath = UIBezierPath(shouldRoundRect: bounds, topLeftRadius: topLeftRadius, topRightRadius: topRightRadius, bottomLeftRadius: bottomLeftRadius, bottomRightRadius: bottomRightRadius)
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
}
