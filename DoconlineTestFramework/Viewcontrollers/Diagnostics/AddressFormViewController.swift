//
//  AddressFormViewController.swift
//  DocOnline
//
//  Created by Doconline india on 27/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class AddressFormViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var txtAddressTitle: UITextField!
    @IBOutlet weak var txtAddress1: UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtPinCode: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    var dictAddress : NSDictionary!
    var navTitle : String?
    var isDefaultAddress: Bool = false
    
    @IBAction func clickBtnSave(_ sender: UIButton) {
        
        if (isValidated() && dictAddress == nil )
        {
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(txtAddressTitle.text, forKey: "title")
            dictTemp.setValue(txtAddress1.text, forKey: "address1")
            dictTemp.setValue(txtAddress2.text, forKey: "address2")
            dictTemp.setValue(txtCity.text, forKey: "city")
            dictTemp.setValue(txtPinCode.text, forKey: "pincode")
            dictTemp.setValue(txtState.text, forKey: "state")
            dictTemp.setValue(App.addressArray.count, forKey: "addressID")
            //App.addressArray.add(dictTemp)
            addAddress(isToUpdate: false)
        }
        else{
            if (isValidated()){
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                addAddress(isToUpdate: true)
            }
        }
        
    }
    @IBAction func clickBtnAccept(_ sender: UIButton) {
        
//        sender.isSelected = !sender.isSelected
        if isDefaultAddress {
            let img = UIImage(named: "UncheckedBox")?.withRenderingMode(.alwaysTemplate)
            sender.setImage(img, for: .normal)
            sender.tintColor = Theme.buttonBackgroundColor
            isDefaultAddress = false
        }else{
            let img = UIImage(named: "CheckedBox")?.withRenderingMode(.alwaysTemplate)
            sender.setImage(img, for: .normal)
            sender.tintColor = Theme.buttonBackgroundColor
            isDefaultAddress = true
        }
        
        
        
    }
    @IBAction func clickBtnEdit(_ sender: UIButton) {
        
        txtAddressTitle.text = ""
        txtAddress1.text = ""
        txtAddress2.text = ""
        txtCity.text = ""
        txtPinCode.text = ""
        txtState.text = ""
//        btnAccept.isSelected = false
        let img = UIImage(named: "UncheckedBox")?.withRenderingMode(.alwaysTemplate)
        sender.setImage(img, for: .normal)
        sender.tintColor = Theme.buttonBackgroundColor
        isDefaultAddress = false

    }
    
    func isValidated() -> Bool {
        if txtAddressTitle.text?.isEmpty == true{
            self.didShowAlert(title: "Title required!", message: "")
            return false
        }else if txtAddress1.text?.isEmpty == true {
            self.didShowAlert(title: "Address 1 required!", message: "")
            return false
        }
        else if txtAddress2.text?.isEmpty == true {
            self.didShowAlert(title: "Address 2 required!", message: "")
            return false
        }else if txtCity.text?.isEmpty == true {
            self.didShowAlert(title: "City required!", message: "")
            return false
        }else if txtPinCode.text?.isEmpty == true {
            self.didShowAlert(title: "Pincode required", message: "")
            return false
        }else if (txtPinCode.text?.count)! < 6 {
            self.didShowAlert(title: "Pincode must contain atleast 6 digits", message: "")
            return false
        }
        else if txtState.text?.isEmpty == true {
            self.didShowAlert(title: "State required!", message: "")
            return false
        }
        /*else if (!btnAccept.isSelected)
        {
            self.didShowAlert(title: "Alert", message: "Please accept agreement")
            return false
        }*/
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtAddressTitle.setLeftPaddingPoints(10)
        btnSave.backgroundColor = Theme.buttonBackgroundColor
        let img = UIImage(named: "UncheckedBox")?.withRenderingMode(.alwaysTemplate)
        btnAccept.setImage(img, for: .normal)
        btnAccept.tintColor = Theme.buttonBackgroundColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isDefaultAddress = false

        if dictAddress != nil {
            if dictAddress.value(forKey: "title") != nil {
                txtAddressTitle.text = dictAddress.value(forKey: "title") as? String
            }
            if dictAddress.value(forKey: "address") != nil {
                txtAddress1.text = dictAddress.value(forKey: "address") as? String
            }
            if dictAddress.value(forKey: "landmark") != nil {
                txtAddress2.text = dictAddress.value(forKey: "landmark") as? String
            }
            if dictAddress.value(forKey: "city") != nil {
                txtCity.text = dictAddress.value(forKey: "city") as? String
            }
            if dictAddress.value(forKey: "pincode") != nil {
                txtPinCode.text = dictAddress.value(forKey: "pincode") as? String
            }
            if dictAddress.value(forKey: "state") != nil {
                txtState.text = dictAddress.value(forKey: "state") as? String
            }
            if dictAddress.value(forKey: "is_default") != nil {
                btnAccept.isSelected = dictAddress.value(forKey: "is_default") as! Bool
            }
            self.title = "Edit Address"
        }
        else
        {
            self.title = "New Address"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - WebService
    
    @objc func addAddress(isToUpdate : Bool) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                return
            }
            
            
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            //title, address, landmark, city, state, pincode,
            dataToPost = "\(Keys.KEY_ADDRESS_TITLE)=\(self.txtAddressTitle.text  ?? "")&\(Keys.KEY_PINCODE)=\(self.txtPinCode.text ?? "")&\(Keys.KEY_ADDRESS)=\(self.txtAddress1.text ?? "")&\(Keys.KEY_ADDRESS_LANDMARK)=\(self.txtAddress2.text ?? "")&\(Keys.KEY_CITY)=\(self.txtCity.text ?? "")&\(Keys.KEY_STATE)=\(self.txtState.text ?? "")&\(Keys.KEY_ADDRESS_DEFAULT)=\(self.isDefaultAddress)"
            var bookURL = AppURLS.URL_ADDRESS_ADD
            if (isToUpdate && dictAddress.value(forKey: "address_id") != nil)
            {
                dataToPost = "\(Keys.KEY_ADDRESS_TITLE)=\(self.txtAddressTitle.text  ?? "")&\(Keys.KEY_PINCODE)=\(self.txtPinCode.text ?? "")&\(Keys.KEY_ADDRESS)=\(self.txtAddress1.text ?? "")&\(Keys.KEY_ADDRESS_LANDMARK)=\(self.txtAddress2.text ?? "")&\(Keys.KEY_CITY)=\(self.txtCity.text ?? "")&\(Keys.KEY_STATE)=\(self.txtState.text ?? "")&\(Keys.KEY_ADDRESS_DEFAULT)=\(self.btnAccept.isSelected)&\(Keys.KEY_ADDRESS_ID)=\(dictAddress.value(forKey: "address_id") ?? "")"
                dataToPost = "{\"\(Keys.KEY_ADDRESS_TITLE)\":\"\(self.txtAddressTitle.text  ?? "")\", \"\(Keys.KEY_PINCODE)\":\"\(self.txtPinCode.text ?? "")\", \"\(Keys.KEY_ADDRESS)\":\"\(self.txtAddress1.text ?? "")\", \"\(Keys.KEY_ADDRESS_LANDMARK)\":\"\(self.txtAddress2.text ?? "")\", \"\(Keys.KEY_CITY)\":\"\(self.txtCity.text ?? "")\" ,\"\(Keys.KEY_STATE)\":\"\(self.txtState.text ?? "")\",\"\(Keys.KEY_ADDRESS_DEFAULT)\":\"\(self.btnAccept.isSelected)\",\"\(Keys.KEY_ADDRESS_ID)\":\"\(dictAddress.value(forKey: "address_id") ?? "")\"}"
                bookURL = AppURLS.URL_ADDRESS_UPDATE
            }
            let url = URL(string: bookURL)
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            
            if (isToUpdate)
            {
                request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
                request.httpMethod = HTTPMethods.PATCH
            }
            else
            {
                request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
                request.httpMethod = HTTPMethods.POST
            }
            
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.stopAnimating()
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.didShowAlert(title: "", message: message)
                                        self.stopAnimating()
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            print("data***=>\(data)")
                            
                            if code == 201 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    self.stopAnimating()
                                    let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as! String
                                    //self.didShowAlert(title: "", message: message)
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
                                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                                        _ = self.navigationController?.popViewController(animated: true)
                                    }
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                //let errors = data.object(forKey: "errors") as! NSDictionary
                                //print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }

}

extension AddressFormViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField != txtPinCode {
            textField.text?.capitalizeFirstLetter()
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
}

private var kAssociationKeyMaxLength: Int = 0

extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        
        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)
        
        selectedTextRange = selection
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
