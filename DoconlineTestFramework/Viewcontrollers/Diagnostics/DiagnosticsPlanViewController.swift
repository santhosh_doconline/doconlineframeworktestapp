//
//  DiagnosticsPlanViewController.swift
//  DocOnline
//
//  Created by Doconline India on 13/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

protocol DiagnosticsPlanDelegate: class
{
    func diagnosticPlanDetails(packageID:Int)
}

class DiagnosticsPlanViewController: UIViewController,NVActivityIndicatorViewable {
    
    ///linear bar instance reference
    let linearBar: LinearProgressBar = LinearProgressBar()
    var expectedContentLength = 0
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var vwPackageList: UIView!
    @IBOutlet var vwPackageShadow: UIView!
    @IBOutlet var vwHL: UIView!
    @IBOutlet var vwVL: UIView!
    @IBOutlet var vwOffered: UIView!
    @IBOutlet var imgPartner: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgPackage: UIImageView!
    @IBOutlet var txtVw: UITextView!
    @IBOutlet var btnAppointment: UIButton?
    @IBOutlet var btnCart: UIButton?
    @IBOutlet weak var heightOftxtVw: NSLayoutConstraint!
    @IBOutlet var shadowLayer: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var stackVw: UIStackView!
    
    
    //instance variables
    ///instance to notification button for showing badge
    var btnBarBadge : MJBadgeBarButton!
    var dictPlans : NSDictionary!
    var arrTests : NSArray = NSArray()
    weak var delegate: DiagnosticsPlanDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.getPlans()
        print("dictPlans :",dictPlans)
        lblName.text = "\(String(describing: dictPlans.object(forKey: "package_name") ?? ""))\n\n\u{20B9}\(String(describing: dictPlans.object(forKey: "price") ?? ""))"
        lblTitle.text = ""
        if dictPlans.object(forKey: "package_tests") != nil {
            arrTests = dictPlans.object(forKey: "package_tests") as! NSArray
            tableView.layer.cornerRadius = 5
            tableView.layer.masksToBounds = true
            self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width-30, height: 10))
            shadowLayer.setShadowView(opacity: 0.23, radius: 8)
            lblTitle.text = "List of tests"
        }
        
        print("arrTests :",arrTests)
        
        btnCart?.backgroundColor = Theme.buttonBackgroundColor
        btnAppointment?.backgroundColor = Theme.buttonBackgroundColor
        
        heightOftxtVw.constant = 0.0
        
        vwPackageList.layer.cornerRadius = 5
        vwPackageList.layer.masksToBounds = true
        
        vwPackageShadow.setShadowView(opacity: 0.4, radius: 8)
        vwVL.setShadowView(opacity: 0.23, radius: 8)
        
        //vwHL.backgroundColor = UIColor.init(hexString: "#71cb70")
        //btnAppointment?.backgroundColor = UIColor.init(hexString: "#6dbe45")
        
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         setupNotificationBarButton()
        if (UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER))
        {
            stackVw.removeArrangedSubview(btnCart!)
            btnCart?.isHidden = true
        }
        else
        {
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     */
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE {
            let destVC = segue.destination as! DiagnosticsPlanDetailsViewController
            destVC.isFromCart = false
            App.isFromView = FromView.DiagnosticPlanView
            destVC.packageID = dictPlans.object(forKey: "package_id") as? Int
            destVC.partnerID  = dictPlans.object(forKey: "partner_id") as? Int
            destVC.dictPlan  = dictPlans
        }
        else if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_PINCODE_SEGUE {
            if segue.destination is DiagnosticsPinCodeViewController {
                
            }
        }
    }
    
    
    /**
     linear bar configuration
     */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view did disappear called")
        
        self.linearBar.stopAnimation()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    // MARK: - Utilities
    
    /**
     Instance notification button instantiation method
     */
    func setupNotificationBarButton() {
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onNotificationButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "cart_empty"), for: .normal)
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        self.btnBarBadge.badgeValue = String(describing: App.cartCount )
        self.navigationItem.rightBarButtonItem = self.btnBarBadge
    }
    
    func RGB(r:Double,g:Double,b:Double) -> UIColor {
        return UIColor(red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(g/255.0), alpha: 1.0)
    }
    
    // MARK: - Web services
    
    @objc func getPlans() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        self.linearBar.startAnimation()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_Diagnostics_plans
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    //self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.linearBar.stopAnimation()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                for object in data {
                                    print("object=\((object as! NSDictionary).debugDescription)")
                                    var dictTemp = NSMutableDictionary()
                                    for (key, value) in object as! NSDictionary
                                    {
                                        print("Key: \(key) - Value: \(value)")
                                        
                                        dictTemp.setValue(value, forKey: key as! String)
                                        if(key as! String == "package_tests")
                                        {
                                            let arrTemp : NSArray = value as! NSArray
                                            var count : Int = 0
                                            var strTemp = ""
                                            for obj in arrTemp
                                            {
                                                if count == arrTemp.count
                                                {
                                                    strTemp = " \(strTemp)\(count+1) \(obj)"
                                                    break
                                                }
                                                
                                                strTemp = "\(strTemp) \(count+1) \(obj) \n"
                                                count = count + 1
                                                
                                            }
                                            dictTemp.setValue(strTemp, forKey: key as! String)
                                        }
                                        
                                    }
                                    //self.arrPlans.add(dictTemp)
                                }
                                
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    self.linearBar.stopAnimation()
                                    self.tableView.reloadData()
                                    
                                    print("*1*1*1*1")
                                })
                                
                            }
                            else
                            {
                                
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.linearBar.stopAnimation()
                        
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    @objc func addToCart(packageName:String,packageID:String,packagePrice:String,partnerId:String,completion: @escaping (_ done:Bool) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            //self.startAnimating()
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            dataToPost = "\(Keys.KEY_PACKAGE_ID)=\(packageID)&\(Keys.KEY_PACKAGE_NAME)=\(packageName)&\(Keys.KEY_PACKAGE_PRICE)=\(packagePrice)&\(Keys.KEY_PARTNER_ID)=\(partnerId)"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_CART_ADD
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        
                        print("Error==> : \(error.localizedDescription)")
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        completion(false)
                                        self.stopAnimating()
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    App.cartCount = data.object(forKey: "updated_cart_count") as! Int
                                    completion(true)
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false)
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false)
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }

    // MARK: - UIButton Action
    
    @objc func onNotificationButtonClick() {
        print("Notification button Clicked ")
        if(App.cartCount != 0)
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_CART_SEGUE, sender: self)
        }
        else
        {
            self.didShowAlert(title: "Cart", message: "There are no items in cart")
        }
    }
    
    @IBAction func clickBookAppointment(_ sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE, sender: self)
        //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_PINCODE_SEGUE, sender: sender)
    }
    
    @IBAction func clickAddToCart(_ sender: UIButton) {
        
        let checkInCart = dictPlans.object(forKey: "in_cart") as! Bool
        if checkInCart
        {
            self.didShowAlert(title: "Cart", message: "Item already added to cart")
        }
        else{
            print("sender.tag :",sender.tag)
            print("dictPlans :",dictPlans ?? [:])
            let packageID : String = String(describing: dictPlans!.object(forKey: "package_id") ?? "0")
            let packagePrice: String = String(describing: dictPlans!.object(forKey: "price") ?? "0.0")
            let partnerID: String = String(describing: dictPlans!.object(forKey: "partner_id") ?? "0.0")
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            self.addToCart(packageName: dictPlans!.object(forKey: "package_name") as! String,packageID: packageID,packagePrice: packagePrice,partnerId:partnerID, completion: {(done) in
                if(done)
                {
                    self.btnBarBadge.badgeValue = String(describing: App.cartCount)
                    self.didShowAlert(title: "Cart", message: "Added to cart")
                    
                    var dictTemp : NSMutableDictionary = NSMutableDictionary()
                    dictTemp = self.dictPlans.mutableCopy() as! NSMutableDictionary
                    dictTemp.setValue("1", forKey: "quantity")
                    print("dictTemp :",dictTemp)
                    App.cartArray.add(dictTemp)
                }
            })
        }
    }
}

extension DiagnosticsPlanViewController : UITableViewDelegate ,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return arrPlans.count
        switch (section) {
        case 0:
            return arrTests.count
        case 1:
            return 6
        default:
            return 6
        }
    }
    
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        switch (section) {
        case 0:
            return "No. of Tests"
        case 1:
            return "Reference Ranges"
        default:
            return "No. of Tests"
        }
    }
    func tableView ( _ tableView : UITableView , viewForHeaderInSection section: Int)->UIView?
    {
        let vwHeader : UIView = UIView()
        vwHeader.frame = CGRect(x: 0, y: 0, width: 300, height: 30)
        vwHeader.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        vwHeader.backgroundColor = UIColor.white
        
        let lbl : UILabel = UILabel()
        lbl.frame = CGRect(x: 0, y: 0, width: 300, height: 30)
        lbl.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        lbl.backgroundColor = UIColor.clear
        lbl.textColor = UIColor.blue
        lbl.textAlignment = .left
        
        switch (section) {
        case 0:
            lbl.text = "No. of Tests"
        case 1:
            lbl.text = "Reference Ranges"
        default:
            lbl.text = "No. of Tests"
        }
        vwHeader.addSubview(lbl)
        return vwHeader
    }
    
    func tableView ( _ tableView : UITableView , heightForHeaderInSection section: Int)->CGFloat
    {
        
        let title = self.tableView(tableView, titleForHeaderInSection: section)
        if (title == "") {
            return 0.0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DignosticsTest", for: indexPath) as! DiagnosticTestCell
        /*
         let dict = arrPlans.object(at: indexPath.row) as! NSMutableDictionary
         cell.lblPlan.text = dict["package_name"] as? String
         //cell.lblPlan.backgroundColor = RGB(r: 245, g: 90, b: 45)
         if dict["availed"] as? Int == 1
         {
         cell.lblStatus.text = "Already availed"
         //cell.lblStatus.backgroundColor = RGB(r: 30, g: 144, b: 255)
         }
         else
         {
         cell.lblStatus.text = "Book"
         cell.lblStatus.backgroundColor = RGB(r: 1, g: 153, b: 1)
         }
         
         cell.txtView.text = dict["package_tests"] as? String
         
         cell.lblExpires.text = "Valid till : " + self.dateString(date:self.dateStringToDate(string:(dict["expires_on"] as? String)!, format:"yyyy-MM-dd"), format:"dd-MMM-yyy")
         
         cell.selectionStyle = .none
         */
        
        //cell.lblPoint.backgroundColor = UIColor.init(hexString: "#71cb70")
        
        switch (indexPath.section) {
        case 0:
            print("section 0")
            cell.widthOflblTestValues.constant = 0.0
            cell.lblTest.text = arrTests.object(at: indexPath.row) as! String
        case 1:
            cell.widthOflblTestValues.constant = 120.0
            print("section 1")
        default:
            print("default")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
         let dict = arrPlans.object(at: indexPath.row) as! NSMutableDictionary
         if dict["availed"] as? Int == 1
         {
         
         }
         else
         {
         delegate?.diagnosticPlanDetails(packageID: dict["package_id"] as! Int)
         }*/
    }
    
}

class DiagnosticTestCell: UITableViewCell {
    
    @IBOutlet var lblTest: UILabel!
    @IBOutlet var lblValues: UILabel!
    @IBOutlet var lblPoint: UILabel!
    @IBOutlet var vwBG: UIView!
    @IBOutlet weak var widthOflblTestValues: NSLayoutConstraint!
    
}

class DiagnosticPlansViewCell: UITableViewCell {
    
    @IBOutlet var txtView: UITextView!
    @IBOutlet var lblPlan: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblExpires: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: 0, height: 0)
        self.clipsToBounds = false
    }
    
}

