//
//  AdddressViewController.swift
//  DocOnline
//
//  Created by Doconline india on 26/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class AdddressViewController: UIViewController,DiagnosticAddressCellDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnDefault: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var addAddress: UIButton!
    
    var expectedContentLength = 0
    ///linear bar instance reference
    //let linearBar: LinearProgressBar = LinearProgressBar()
    var dictAddress : NSDictionary!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.tableView.estimatedRowHeight = 240.0;
        //self.tableView.rowHeight = UITableViewAutomaticDimension;
        addAddress.backgroundColor = Theme.buttonBackgroundColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        App.isFromView = FromView.AddressView
        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
    
        self.getAddress(completion: {(done1) in
        })
        //tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    

    /*
    // MARK: - Navigation
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_ADDRESS_FORM_SEGUE {
            let destVC = segue.destination as! AddressFormViewController
            //destVC.packageID = self.packageID
            destVC.dictAddress = dictAddress
        }
    }
    
    @IBAction func clickBtnAddAddress(_ sender: UIButton) {
        print("selected")
        //delegate?.ClickBtnCheck(sender: sender)
        if App.addressArray.count == 10 {
            self.didShowAlert(title: "", message: "Cant add more than 10 addresses")
            return
        }
        dictAddress = nil
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_ADDRESS_FORM_SEGUE, sender: self)
    }
    
    // MARK: - Web services
    
    @objc func getAddress(completion: @escaping (_ done:Bool) -> ()) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            DispatchQueue.main.async(execute: {
                self.stopAnimating()
            })
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        //self.linearBar.startAnimation()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_ADDRESS_LIST
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        //self.linearBar.stopAnimation()
                        self.stopAnimating()
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("get address = \(resultJSON)")
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        completion(false)
                                        self.stopAnimating()
                                        //self.linearBar.stopAnimation()
                                        //self.didShowAlert(title: "", message: message)
                                        self.lblAddress.text = message
                                    })
                                }
                            }
                            print("performing error handling time slots:\(resultJSON)")
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                
                                
                                App.addressArray = data.mutableCopy() as! NSMutableArray
                                DispatchQueue.main.async(execute: {
                                    completion(true)
                                    self.tableView.reloadData()
                                    self.stopAnimating()
                                    //self.linearBar.stopAnimation()
                                    self.lblAddress.text = ""
                                    
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    completion(false)
                                    self.stopAnimating()
                                    //self.linearBar.stopAnimation()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            completion(false)
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    @objc func makeItAsDefaultAddress(addressID:Int,completion: @escaping (_ done:Bool) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            //dataToPost = "\(Keys.KEY_ADDRESS_ID)=\(addressID)"
            dataToPost = "{\"\(Keys.KEY_ADDRESS_ID)\":\"\(addressID)\"}"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_ADDRESS_DEFAULT
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.httpMethod = HTTPMethods.PATCH
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        print("Error==> : \(error.localizedDescription)")
                        self.stopAnimating()
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        completion(false)
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            print("data***=>\(data)")
                            
                            if code == 201 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    completion(true)
                                    self.stopAnimating()
                                    
                                })
                            }
                            else
                            {
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false)
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false)
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    
    @objc func deleteAddress(addressID:Int,completion: @escaping (_ done:Bool) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            //dataToPost = "\(Keys.KEY_ADDRESS_ID)=\(addressID)"
            dataToPost = "{\"\(Keys.KEY_ADDRESS_ID)\":\"\(addressID)\"}"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_ADDRESS_DELETE
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            
            request.httpMethod = HTTPMethods.DELETE
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        print("Error==> : \(error.localizedDescription)")
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        completion(false)
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            print("data***=>\(data)")
                            
                            if code == 201 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    
                                    //let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as! String
                                    //self.didShowAlert(title: "", message: message)
                                    completion(true)
                                    self.stopAnimating()
                                
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                //let errors = data.object(forKey: "errors") as! NSDictionary
                                //print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false)
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false)
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    // MARK: - DiagnosticAddressCellDelegate
    
    func clickBtnTitle(sender: UIButton){
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        if sender.isSelected
        {
            self.didShowAlert(title: "", message: "Already marked it as default")
        }
        else{
            print("sender.tag :",sender.tag)
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            
            self.makeItAsDefaultAddress(addressID: sender.tag, completion: { (done) in
                if(done)
                {
                    let indexPath = IndexPath(row: sender.indexTag!, section: 0)
                    
                    let cell = self.tableView.cellForRow(at: indexPath) as! AddressListCell
//                    cell.btnTitle.isSelected = true
                    let arrTemp : NSMutableArray =  App.addressArray.mutableCopy() as! NSMutableArray
                    App.addressArray.removeAllObjects()
                    for obj in arrTemp {
                        let dict :NSDictionary = obj as! NSDictionary
                        let dictAdd :NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                        let strID = String(describing: dictAdd.object(forKey: "address_id") ?? "0")
                        dictAdd.setValue(false, forKey: "is_default")
                        if(cell.btnTitle.tag == Int(strID)!)
                        {
                            dictAdd.setValue(true, forKey: "is_default")
                        }
                        
                        
                        App.addressArray.add(dictAdd)
                    }
                    self.tableView.reloadData()
                    self.didShowAlert(title: "", message: "Address is marked as default")
                }
            })
            
        }
    }
    func clickBtnEdit(sender: UIButton){
        dictAddress = sender.descriptiveDictionary
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_ADDRESS_FORM_SEGUE, sender: self)
    }
    func clickBtnDelete(sender: UIButton){
        print("sender.tag :",sender.tag)
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let alert = UIAlertController(title: "Address", message: "Are you sure you want to delete this address?", preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            
            self.deleteAddress(addressID: sender.tag, completion: { (done) in
                
                if(done)
                {
                    let indexPath = IndexPath(row: sender.indexTag!, section: 0)
                    
                    let cell = self.tableView.cellForRow(at: indexPath) as! AddressListCell
//                    cell.btnTitle.isSelected = true
                    let arrTemp : NSMutableArray =  App.addressArray.mutableCopy() as! NSMutableArray
                    App.addressArray.removeAllObjects()
                    for obj in arrTemp {
                        let dict :NSDictionary = obj as! NSDictionary
                        let strID = String(describing: dict.object(forKey: "address_id") ?? "0")
                        if(cell.btnTitle.tag != Int(strID)!)
                        {
                            App.addressArray.add(dict)
                        }
                        
                    }
                    self.tableView.reloadData()
                    self.didShowAlert(title: "", message: "Address is deleted")
                }
                
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getHeight(for string: String, font: UIFont, width: CGFloat) -> CGFloat {
        let textStorage = NSTextStorage(string: string)
        let textContainter = NSTextContainer(size: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainter)
        textStorage.addLayoutManager(layoutManager)
        textStorage.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, textStorage.length))
        textContainter.lineFragmentPadding = 0.0
        layoutManager.glyphRange(for: textContainter)
        return layoutManager.usedRect(for: textContainter).size.height
    }

}

extension AdddressViewController: UITableViewDelegate ,UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressListCell", for: indexPath) as! AddressListCell
        cell.delegate = self
        cell.mainBackground.layer.cornerRadius = 5
        cell.mainBackground.layer.masksToBounds = true
    
        cell.shadowLayer.setShadowView(opacity: 0.4, radius: 8)
        let dict = App.addressArray.object(at: indexPath.row) as! NSDictionary
        cell.btnTitle.setTitle(dict.value(forKey: "title") as? String, for: .normal)
        cell.lblAddress1.text = dict.value(forKey: "address") as? String
        cell.lblAddress2.text = "\(String(describing: dict.value(forKey: "landmark") ?? ""))\n\(String(describing: dict.value(forKey: "city") ?? ""))\n\(String(describing: dict.value(forKey: "pincode") ?? ""))\n\(String(describing: dict.value(forKey: "state") ?? ""))"
        let img = UIImage(named: "UncheckedBox")?.withRenderingMode(.alwaysTemplate)
        cell.btnTitle.setImage(img, for: .normal)
        cell.btnTitle.tintColor = Theme.buttonBackgroundColor
//        cell.btnTitle.isSelected = dict.object(forKey: "is_default") as! Bool
        if(dict.object(forKey: "is_default") as! Bool)
        {
            self.btnDefault.setTitle("\(String(describing: dict.value(forKey: "title") ?? "")) is a your default address", for: .normal)
            let img = UIImage(named: "CheckedBox")?.withRenderingMode(.alwaysTemplate)
            cell.btnTitle.setImage(img, for: .normal)
            cell.btnTitle.tintColor = Theme.buttonBackgroundColor

        }
        
        
        let strID = String(describing: dict.object(forKey: "address_id") ?? "0")
        cell.btnTitle.tag = Int(strID)!
        cell.btnTitle.indexTag = indexPath.row
        cell.btnDelete.tag = Int(strID)!
        cell.btnDelete.indexTag = indexPath.row
        cell.btnEdit.tag = Int(strID)!
        cell.btnEdit.descriptiveDictionary = dict
        cell.btnEdit.backgroundColor = Theme.buttonBackgroundColor
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //let cell = tableView.cellForRow(at: indexPath as IndexPath) as! DiagnosticsListCell
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return App.addressArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt IndexPath:IndexPath) -> CGFloat
    {
        //print("UITableViewAutomaticDimension : ",UITableViewAutomaticDimension)
        let dict = App.addressArray.object(at: IndexPath.row) as! NSDictionary
        let lblHeight = self.getHeight(for: "\(String(describing: dict.value(forKey: "landmark") ?? ""))\n\(String(describing: dict.value(forKey: "city") ?? ""))\n\(String(describing: dict.value(forKey: "pincode") ?? ""))\n\(String(describing: dict.value(forKey: "state") ?? ""))", font: UIFont.systemFont(ofSize: 16), width: self.view.frame.size.width-60)
        //print("lblHeight : ",lblHeight)
        var rowHeight = 0.0
        if(lblHeight > 89.0)
        {
            rowHeight = Double(lblHeight - 89.0)
            //print("rowHeight : ",rowHeight)
            return CGFloat(240.0 + rowHeight)
        }
        return 240.0
    }
    /*
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240.0
    }*/
}

protocol DiagnosticAddressCellDelegate: class
{
    func clickBtnTitle(sender: UIButton)
    func clickBtnEdit(sender: UIButton)
    func clickBtnDelete(sender: UIButton)
}

class AddressListCell: UITableViewCell {
    @IBOutlet var mainBackground: UIView!
    @IBOutlet var shadowLayer: UIView!
    @IBOutlet var lblAddress1: UILabel!
    @IBOutlet var lblAddress2: UILabel!
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
    weak var delegate: DiagnosticAddressCellDelegate? = nil
    
    @IBAction func clickBtnTitle(_ sender: UIButton) {
        print("clickBtnTitle")
        //sender.isSelected = !sender.isSelected
        delegate?.clickBtnTitle(sender: sender)
    }
    @IBAction func clickBtnEdit(_ sender: UIButton) {
        print("clickBtnEdit")
        delegate?.clickBtnEdit(sender: sender)
    }
    @IBAction func clickBtnDelete(_ sender: UIButton) {
        print("clickBtnDelete")
        delegate?.clickBtnDelete(sender: sender)
    }
}
