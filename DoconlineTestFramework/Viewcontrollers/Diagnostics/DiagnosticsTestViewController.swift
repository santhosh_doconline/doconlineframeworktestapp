//
//  DiagnosticsTestViewController.swift
//  DocOnline
//
//  Created by Doconline india on 24/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class DiagnosticsTestViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet var vwList: UIView!
    @IBOutlet var vwShadow: UIView!
    @IBOutlet var txtVw: UITextView!
    @IBOutlet var imgPackage: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnAppointment: UIButton?
     @IBOutlet var btnCart: UIButton?
    
    //instance variables
    ///instance to notification button for showing badge
    var btnBarBadge : MJBadgeBarButton!
    var dictPlans : NSDictionary!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
        vwList.layer.cornerRadius = 5
        vwList.layer.masksToBounds = true
        
        vwShadow.setShadowView(opacity: 0.4, radius: 8)
        
        print("dictPlans :",dictPlans)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNotificationBarButton()
        lblName.text = "\(String(describing: dictPlans.object(forKey: "package_name") ?? ""))\n\n\u{20B9}\(String(describing: dictPlans.object(forKey: "price") ?? ""))"
        btnCart?.isSelected = dictPlans.object(forKey: "in_cart") as! Bool
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    /**
    Instance notification button instantiation method
    */
    func setupNotificationBarButton() {
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onNotificationButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "cart_empty"), for: .normal)
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        self.btnBarBadge.badgeValue = String(describing: App.cartCount )
        self.navigationItem.rightBarButtonItem = self.btnBarBadge
    }
    
    @IBAction func clickBookAppointment(_ sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE, sender: self)
    }
    @IBAction func clickCartBtn(_ sender: UIButton) {
        print("selected")
        if sender.isSelected
        {
            self.didShowAlert(title: "Cart", message: "Item already added to cart")
        }
        else{
            print("sender.tag :",sender.tag)
            print("sender.descriptiveDictionary :",dictPlans ?? [:])
            let packageID : String = String(describing: dictPlans.object(forKey: "package_id") ?? "0")
            let packagePrice: String = String(describing: dictPlans.object(forKey: "price") ?? "0.0")
            let partnerID: String = String(describing: dictPlans!.object(forKey: "partner_id") ?? "0.0")
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            self.addToCart(packageName: dictPlans.object(forKey: "package_name") as! String,packageID: packageID,packagePrice: packagePrice,partnerId:partnerID,completion: {(done) in
                if(done)
                {
                    sender.isSelected = true
                    self.btnBarBadge.badgeValue = String(describing: App.cartCount )
                    self.didShowAlert(title: "Cart", message: "Added to cart")
                    
                }
            })
        }
    }
    @objc func onNotificationButtonClick() {
        if(App.cartCount != 0)
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_CART_SEGUE, sender: self)
        }
        else
        {
            self.didShowAlert(title: "Cart", message: "There are no items in cart")
        }
 }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
     */

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE {
            let destVC = segue.destination as! DiagnosticsPlanDetailsViewController
            destVC.isFromCart = false
            destVC.packageID = dictPlans.object(forKey: "package_id") as? Int
            destVC.partnerID  = dictPlans.object(forKey: "partner_id") as? Int
            destVC.dictPlan  = dictPlans
        }
    }
    
    // MARK: - Web services
    
    @objc func addToCart(packageName:String,packageID:String,packagePrice:String,partnerId:String,completion: @escaping (_ done:Bool) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.stopAnimating()
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            dataToPost = "\(Keys.KEY_PACKAGE_ID)=\(packageID)&\(Keys.KEY_PACKAGE_NAME)=\(packageName)&\(Keys.KEY_PACKAGE_PRICE)=\(packagePrice)&\(Keys.KEY_PARTNER_ID)=\(partnerId)"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_CART_ADD
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        print("Error==> : \(error.localizedDescription)")
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                       
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        completion(false)
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    App.cartCount = data.object(forKey: "updated_cart_count") as! Int
                                    completion(true)
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false)
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false)
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }

}
