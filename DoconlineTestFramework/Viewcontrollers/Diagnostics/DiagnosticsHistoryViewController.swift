//
//  DiagnosticsHistoryViewController.swift
//  DocOnline
//
//  Created by Doconline India on 13/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class DiagnosticsHistoryViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var bt_upcommingOutlet: UIButton!
    @IBOutlet weak var bt_previousOutlet: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noAppointmentsLabel: UILabel!
    @IBOutlet var upcomingNPrevSegmentControl: UISegmentedControl!
    @IBOutlet var toolBar: UIToolbar!
    
    //instance variables
    ///instance to notification button for showing badge
    var btnBarBadge : MJBadgeBarButton!
    
    var UP_COMING_BUTTON_TAG = 1
    var PREVIOUS_BUTTON_TAG = 2
    
    var selectedButtonTag = 1
    let selectedButtonBGColorCode = "#F6846A"
    var noAppointmetns = "Knock ! Knock!\nYour scheduled upcoming appointments await you here!"
    
    //var appointmentsList = NSArray()
    var appointmentsList : NSArray?
    var upcomingAppointments = NSMutableArray()
    var previousAppointments = NSMutableArray()
    var arrTemp = NSArray()
    var dictTest = NSDictionary()
    
    //let linearBar: LinearProgressBar = LinearProgressBar()//(frame:CGRect(origin: CGPoint(x: 0,y :108), size: CGSize(width: UIScreen.main.bounds.width, height: 0)))
    var expectedContentLength = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //buttonCornerRadius(buttons: [self.bt_upcommingOutlet,self.bt_previousOutlet])
        hideTableView()
        
        // Do any additional setup after loading the view.
        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        getHistory()
        
        //self.sortDates()
        self.tableView.reloadData()
        
        //setupNotificationBarButton()
        setupNavigationBar()
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.toolBar.frame
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.toolBar.insertSubview(vwGrad, at: 0)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        
        print("viewWillAppear")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.linearBar.stopAnimation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_SUMMARY {
            let destVC = segue.destination as! DiagnosticsSummaryViewController
            //destVC.packageID = self.packageID
            destVC.dictTest = dictTest
        }
        
    }
    
    
    /**
     Instance notification button instantiation method
     */
    func setupNotificationBarButton() {
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onNotificationButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "cart_empty"), for: .normal)
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        self.btnBarBadge.badgeValue = String(describing: App.cartCount )
        self.navigationItem.rightBarButtonItem = self.btnBarBadge
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @IBAction func previousNUpcomingSegmentTapped(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            selectedButtonTag = 1
            noAppointmetns = "Knock ! Knock!\nYour scheduled upcoming appointments await you here!"
            
        }else if sender.selectedSegmentIndex == 1 {
            selectedButtonTag = 2
            noAppointmetns = "At a Glance: View all your past appointment details here!"
            
        }
        
        self.tableView.reloadData()
        hideTableView()
    }
    
    @IBAction func prevNupCommingTapped(_ sender: UIButton) {
        selectedButtonTag = sender.tag
        if sender.tag == UP_COMING_BUTTON_TAG
        {
            noAppointmetns = "No Upcoming appointments"
            
            //setting background color
            sender.backgroundColor = hexStringToUIColor(hex: selectedButtonBGColorCode)
            self.bt_previousOutlet.backgroundColor = UIColor.darkGray
            
        }
        else if sender.tag == PREVIOUS_BUTTON_TAG
        {
            
            //setting background color
            sender.backgroundColor = hexStringToUIColor(hex: selectedButtonBGColorCode)
            self.bt_upcommingOutlet.backgroundColor = UIColor.darkGray
            noAppointmetns = "No Previous appointments"
        }
        
        
        self.tableView.reloadData()
        hideTableView()
    }
    
    @objc func onNotificationButtonClick() {
        print("Notification button Clicked ")
        if(App.cartCount != 0)
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_CART_SEGUE, sender: self)
        }
        else
        {
            self.didShowAlert(title: "Cart", message: "There are no items in cart")
        }
    }
    
    func hideTableView() {
        if arrTemp.count == 0 {
            self.tableView.isHidden = true
            self.noAppointmentsLabel.isHidden = false
            self.noAppointmentsLabel.text = noAppointmetns
        }else {
            self.noAppointmentsLabel.isHidden = true
            self.tableView.isHidden = false
        }
    }
    
    // MARK: - Web services
    @objc func getHistory() {
        print("*****")
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            self.stopAnimating()
            return
        }
        //self.linearBar.startAnimation()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_Diagnostics_History
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        //self.linearBar.stopAnimation()
                        self.stopAnimating()
                        self.hideTableView()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("get history = \(resultJSON)")
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        self.hideTableView()
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                DispatchQueue.main.async(execute: {
                                    self.appointmentsList = data
                                    self.sortDates()
                                    //self.linearBar.stopAnimation()
                                    self.tableView.reloadData()
                                    self.stopAnimating()
                                    self.hideTableView()
                                    
                                    print("*1*1*1*1")
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                    self.hideTableView()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                            self.hideTableView()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    func sortDates() {
        if appointmentsList?.count != 0
        {
            for obj in appointmentsList!
            {
                let dict : NSDictionary = obj as! NSDictionary
                let date1 = Date()
                print("date1 : ",date1,"date2 : ",dict.object(forKey: "apt_dt") as! String)
                let date2 = self.dateStringToDate(string:dict.object(forKey: "apt_dt") as! String, format:"yyyy-MM-dd HH:mm:ss +0000")
                
                if date1 >= date2
                {
                    previousAppointments.add(dict)
                }
                else
                {
                    upcomingAppointments.add(dict)
                }
            }
        }
        
        print("upcomingAppointments",upcomingAppointments)
        print("previousAppointments",previousAppointments)
    }
    
}

extension DiagnosticsHistoryViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if selectedButtonTag == UP_COMING_BUTTON_TAG
        {
            arrTemp = upcomingAppointments
            print("upcomingAppointments : ",upcomingAppointments)
        }
        else
        {
            arrTemp = previousAppointments
            print("previousAppointments : ",previousAppointments)
        }
        return arrTemp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.DiagnosticReportCell, for: indexPath) as! DiagnosticReportViewCell
        
        //cell.textLabel!.text = "Monday july 13 2016"
        
        let dict : NSDictionary = arrTemp[indexPath.row] as! NSDictionary
        
        let date = self.dateStringToDate(string:dict.object(forKey: "apt_dt") as! String, format:"yyyy-MM-dd HH:mm:ss +0000")
        let strDate = dateString(date:date, format:"yyyy dd MMM HH:mm")
        
        cell.lblAppID.text = "\(dict.object(forKey: "apt_id") ?? "")"
        cell.lblDate.text = strDate
        cell.lblName.text = dict.object(forKey: "package_name") as? String
        
        
        cell.selectionStyle = .none
        
        cell.mainBackground.layer.cornerRadius = 5
        cell.mainBackground.layer.masksToBounds = true
        
        cell.shadowLayer.setShadowView(opacity: 0.4, radius: 8)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedButtonTag == PREVIOUS_BUTTON_TAG
        {
            dictTest = arrTemp[indexPath.row] as! NSDictionary
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_SUMMARY, sender: self)
        } 
    }
    
}

class DiagnosticReportViewCell: UITableViewCell {
    
    
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblAppID: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgPartner: UIImageView!
    @IBOutlet var mainBackground: UIView!
    @IBOutlet var shadowLayer: UIView!
    
    
}

