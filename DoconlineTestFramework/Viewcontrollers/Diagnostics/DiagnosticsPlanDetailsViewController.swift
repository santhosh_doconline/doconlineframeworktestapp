//
//  DiagnosticsPlanDetailsViewController.swift
//  DocOnline
//
//  Created by Doconline India on 13/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import Razorpay
//import Firebase
import SwiftMessages
//import NVActivityIndicatorView

class DiagnosticsPlanDetailsViewController: UIViewController,DiagnosticFamilyCellDelegate, NVActivityIndicatorViewable,UIPickerViewDelegate,UIPickerViewDataSource
//RazorpayPaymentCompletionProtocol,ExternalWalletSelectionProtocol,PGTransactionDelegate
{
    
    @IBOutlet var txtAddress1: UITextField!
    @IBOutlet var txtAddress2: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtPinCode: UITextField!
    @IBOutlet var txtMobile: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtOrderBy: UITextField!
    @IBOutlet var txtDate: UITextField!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var scrVW: UIScrollView!
    @IBOutlet var vwScr: UIView!
    @IBOutlet var btnAcceptAgree: UIButton!
    @IBOutlet var btnNeedPrint: UIButton!
    @IBOutlet var btnFamily: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableFamilyView: UITableView!
    @IBOutlet var btnChooseAddress: UIButton!
    @IBOutlet var vwBG: UIView!
    @IBOutlet var vwAppointmentDetails: UIView!
    @IBOutlet weak var lbl_appointmentID: UILabel!
    @IBOutlet weak var lbl_appointmentDate: UILabel!
    @IBOutlet weak var lbl_PackageName: UILabel!
    @IBOutlet var btnViewHistory: UIButton!
    @IBOutlet weak var okAddressBtn: UIButton!
    
    @IBOutlet var vwPackageList: UIView!
    @IBOutlet var vwPackageShadow: UIView!
    @IBOutlet var vwHL: UIView!
    @IBOutlet var vwVL: UIView!
    @IBOutlet var vwOffered: UIView!
    @IBOutlet var imgPartner: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgPackage: UIImageView!
    @IBOutlet var imgExpand: UIImageView!
    
    @IBOutlet var vwChooseAddress: UIView!
    @IBOutlet var vwAddressList: UIView!
    @IBOutlet weak var heightOfAddressView: NSLayoutConstraint!
    @IBOutlet weak var lbl_PinCode: UILabel!
    @IBOutlet var btnPin: UIButton!
    
    @IBOutlet weak var heightOfBtnNeedPrint: NSLayoutConstraint!
    @IBOutlet weak var heightOfLblNeedPrint: NSLayoutConstraint!
    
    //instance variables
    ///instance to notification button for showing badge
    var btnBarBadge : MJBadgeBarButton!
    
    var lastSelectedIndexPath = NSIndexPath(row: -1, section: 0)
    var tempfamilyMemrs = [User]()
    var familyMem = [NSDictionary]()
    var familyMemrs = [User]()
    var editStatus = false
    var membersCount = 0
    var userDataModel : User!
    var minimumDate:Date!
    var maximumDate:Date!
    var jsonString : String!
    var packageID : Int!
    var partnerID : Int!
    //    var picker : DateTimePicker! = nil
    var defaultDict : NSDictionary!
    var bookingDict : NSDictionary!
    var strDefaultAdd : String = ""
    var dictPlan : NSDictionary!
    var dictConfirm : NSDictionary!
    var isFromCart = false
    
    var isNeedPrint = false
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var expectedContentLength = 0
    ///linear bar instance reference
    //let linearBar: LinearProgressBar = LinearProgressBar()
    
    
    ///razorpay key instance
    private let rz_key = RazorpayDetails.rzKey
    
//    private var razorpay : RazorpayCheckout!
    
    ///Sessions
    static let config = URLSessionConfiguration.default
    var session : URLSession = URLSession.shared
    static var cookieJar = HTTPCookieStorage.shared
    
    var cookieHeaders : String = ""
    var sessionID : String = ""
    
    ///Selected user index for consultation
    var selectedUserIndex = 0
    
    @IBOutlet var LC_tableFamilyViewHeight: NSLayoutConstraint!
    @IBOutlet var LC_btnExpandHeight: NSLayoutConstraint!
    @IBOutlet var dropDownShadowView: CardView!
    
    ///Stores the slot dates to display in picker
    var slotDates = [String]()
    ///Stores the slot times to display in picker
    var slotTimes = [String]()
    var slotsData : NSDictionary!
    var selectedDate = ""
    var selectedTimeSlot = ""
    ///instance var declaration for date
    var scheduled_at = ""
    var strOrderBy = ""
    
    //paytm
    var strCheckSum = ""
    private var paymentGateWay = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwPackageList.layer.cornerRadius = 5
        vwPackageList.layer.masksToBounds = true
        
        vwPackageShadow.setShadowView(opacity: 0.4, radius: 8)
        
        vwVL.setShadowView(opacity: 0.4, radius: 8)
        
        let checkImg = UIImage(named: "UncheckedBox")?.withRenderingMode(.alwaysTemplate)
        btnNeedPrint.setImage(checkImg, for: UIControl.State.normal)
        btnNeedPrint.tintColor = Theme.navigationGradientColor![0]!
        
        vwChooseAddress.isHidden = true
        btnConfirm.backgroundColor = Theme.buttonBackgroundColor
        btnChooseAddress.backgroundColor = Theme.buttonBackgroundColor
        okAddressBtn.backgroundColor = Theme.buttonBackgroundColor
        txtAddress1.delegate = self
        txtAddress1.text = "Address"
        lblName.text = dictPlan.object(forKey: "package_name") as? String
        
        packageID = dictPlan.object(forKey: "package_id") as? Int
        partnerID = dictPlan.object(forKey: "partner_id") as? Int
        print("dictPlan :",dictPlan,"packageID :",dictPlan.object(forKey: "package_id") ?? "","partnerID :",dictPlan.object(forKey: "partner_id") ?? "")
        
        
        UpdateUIFields()
        // Do any additional setup after loading the view.
        
        scrVW.delegate = self
        
        //setupNotificationBarButton()
        
        //txtDate.isEnabled = false
        lbl_PinCode.text = ""
        lbl_PinCode.textColor = UIColor.white
        
        LC_tableFamilyViewHeight.constant = 0.0
        
        for i in stride(from: 1, to: 31, by: 1){
            //let date : Date = Calendar.current.date(byAdding: .day, value: i, to: Date())!
            let date : Date = (Calendar.current as NSCalendar).date(byAdding: .day, value: i, to: Date(), options: [])!
            let strDate :  String = date.toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
            let strConvertedDate :  String = self.dateString(date: self.dateStringToDate(string: strDate, format: "yyyy-MM-dd HH:mm:ss"), format: "dd-MMM-yyyy")
            slotDates.append(strConvertedDate)
        }
        
        print("slotDates:",slotDates)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER))
        {
            heightOfBtnNeedPrint.constant = 0.0
            heightOfLblNeedPrint.constant = 0.0
        }
        else
        {
            heightOfBtnNeedPrint.constant = 50.0
            heightOfLblNeedPrint.constant = 50.0
        }
        if(App.isFromView != FromView.ConsentFormView)
        {
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            self.getAddress(completion: {(done,dictDefault) in
                if(done)
                {
                    print("dictDefault :",dictDefault)
                    self.setDefaultValues(dict: dictDefault)
                }
            })
        }
        
        if (UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER))
        {
            LC_btnExpandHeight.constant = 0.0
        }
        else
        {
            //LC_btnExpandHeight.constant = 0.0
            
            self.getFamilyMembers(completionHandler: {(success) in
                if success {
                    
                }
            })
            /*
             self.getTotalFamilyMembers(completionHandler: {(success) in
             if success {
             
             }
             })*/
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func showPickerInActionSheet(sentBy: String) {
        
        if !self.btnPin.isSelected {
            self.didShowAlert(title: "Sorry!", message: "Service not available in the selected area.")
            return
        }
        
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 250)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        pickerView.delegate = self
        pickerView.dataSource = self
        
        if sentBy == "dates"{
            /*
             if let dates = self.slotsData {
             if let allDateKeys = dates.allKeys as? [String] {
             //self.slotDates = allDateKeys
             self.slotDates.removeAll()
             for dateKey in allDateKeys {
             if let values = dates.object(forKey: dateKey) as? [String] {
             if values.count == 0 {
             print("Date has no slots")
             }else {
             self.slotDates.append(dateKey)
             //  print("DateKey::=>\(dateKey) at index:\(self.slotDates.count - 1)")
             }
             }
             }
             }
             }*/
            
            /*
             if !slotDates.isEmpty {
             self.slotDates.sort(by: { (dateString1, dateString2) -> Bool in
             let date1 = self.stringToDateConverter(date: dateString1)
             let date2 = self.stringToDateConverter(date: dateString2)
             return date1 < date2
             })
             }*/
            
            print("Sorted slot dates:\(self.slotDates)")
            
            pickerView.tag = 1
        } else if sentBy == "slots"{
            pickerView.tag = 2
        } else {
            pickerView.tag = 0
        }
        
        
        vc.view.addSubview(pickerView)
        let title = sentBy == "dates" ? "Select Date" : "Select Slot"
        let editRadiusAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        
        let selectSlot = UIAlertAction(title: "Show Slots", style: .default) { (action) in
            /*
             if !self.selectedDate.isEmpty {
             if let slots = self.slotsData.object(forKey: self.selectedDate) as? [String] {
             self.slotTimes = slots
             }
             }*/
            
            self.slotTimes.removeAll()
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            self.getTimeSlots(strPincode: self.txtPinCode.text!, strDate: self.selectedDate, completionHandler: {(success) in
                if success {
                    
                }
            })
            
        }
        
        
        let slotSelected = UIAlertAction(title: "Done", style: .default) { (action) in
            let seperatedDateKey = self.selectedDate.components(separatedBy: " ")
            self.scheduled_at = seperatedDateKey[0] + " \(self.selectedTimeSlot)"
            print("Selected date and slot :\(self.scheduled_at)")
            //let date = self.getFormattedDateAndTime(dateString: self.scheduled_at )
            self.txtDate.text =  self.scheduled_at//date
        }
        
        
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        if sentBy == "dates"{
            editRadiusAlert.addAction(selectSlot)
        }else  if sentBy == "slots"{
            editRadiusAlert.addAction(slotSelected)
        }
        
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(editRadiusAlert, animated: true)
    }
    
    /**
     Instance notification button instantiation method
     */
    func setupNotificationBarButton() {
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onNotificationButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "cart_empty"), for: .normal)
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        self.btnBarBadge.badgeValue = String(describing: App.cartCount )
        self.navigationItem.rightBarButtonItem = self.btnBarBadge
    }
    
    func setDefaultValues(dict:NSDictionary){
        DispatchQueue.main.async {
            
            if(self.tableView.contentSize.height+170 > (self.view.frame.height - 64))
            {
                self.heightOfAddressView.constant = (self.view.frame.height-20)
            }
            else
            {
                self.heightOfAddressView.constant = self.tableView.contentSize.height+170
            }
            self.defaultDict = dict
            self.txtAddress1.text = dict.value(forKey: "address") as? String
            self.txtAddress2.text = dict.value(forKey: "landmark") as? String
            self.txtPinCode.text = dict.value(forKey: "pincode") as? String
            self.txtCity.text = dict.value(forKey: "city") as? String
            self.txtState.text = dict.value(forKey: "state") as? String
            
            self.strDefaultAdd = "\(String(describing: dict.value(forKey: "address") ?? "")) \(String(describing: dict.value(forKey: "landmark") ?? "")) \(String(describing: dict.value(forKey: "city") ?? "")) \(String(describing: dict.value(forKey: "state") ?? ""))"
            
            if(self.txtPinCode.text?.isEmpty == false)
            {
                let partnerID:String = "1"//String(format: "%@", self.dictPlan.object(forKey: "partner_id") as! CVarArg)
                print("partner_id :", partnerID)
                print("txtPinCode.text :",self.txtPinCode.text!)
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                self.CheckPinCode(partner_id: partnerID, pincode: self.txtPinCode.text!, completion: { (done) in
                    if(done)
                    {
                        print("done :",done)
                    }
                })
            }
        }
    }
    
    func isValidated() -> Bool {
        
        let name = self.txtOrderBy.text! as String
        let phonenumer = self.txtMobile.text! as String
        let email = self.txtEmail.text! as String
        
        if name.isEmpty == true {
            self.didShowAlert(title: "", message: "Please fill your name")
            return false
        }
        else if name.count < 3{
            self.didShowAlert(title: "", message: "Minimum 3 characters required in name")
            return false
        }
        else if (email.isEmpty == true || validateOnlyEmail(enteredEmail: email) == false)
        {
            if (phonenumer.isEmpty != true || phonenumer.count >= 10)
            {
                //self.didShowAlert(title: "", message: AlertMessages.VALIDATE_EMAIL)
            }
            else
            {
                self.didShowAlert(title: "", message: AlertMessages.VALIDATE_EMAIL)
                return false
            }
            
        }
        else if (phonenumer.isEmpty == true || phonenumer.count < 10) {
            self.didShowAlert(title: "", message: "Please enter a valid phone number")
            return false
        }
        if txtAddress1.text?.isEmpty == true {
            self.didShowAlert(title: "", message: "Address 1 required!")
            return false
        }
        else if txtAddress2.text?.isEmpty == true {
            self.didShowAlert(title: "", message: "Landmark required!")
            return false
        }else if txtState.text?.isEmpty == true {
            self.didShowAlert(title: "", message: "State required!")
            return false
        }else if txtCity.text?.isEmpty == true {
            self.didShowAlert(title: "", message: "City required!")
            return false
        }else if txtPinCode.text?.isEmpty == true {
            self.didShowAlert(title: "", message: "Pincode required")
            return false
        }else if (txtPinCode.text?.count)! < 6 {
            self.didShowAlert(title: "", message: "Pincode must contain atleast 6 digits")
            return false
        }
        else if txtDate.text?.isEmpty == true{
            self.didShowAlert(title: "", message: "Please select date")
            return false
        }
        else if !checkCurrentDateAndTime(){
            self.didShowAlert(title: "", message: "Please select future date and time")
            return false
        }
        /*
         else if !btnAcceptAgree.isSelected{
         self.didShowAlert(title: "", message: "Please accept agreement")
         return false
         }*/
        
        return true
    }
    
    func checkCurrentDateAndTime() -> Bool
    {
        //Ref date
        let selectedDate = self.dateStringToDate(string: self.txtDate.text ?? "", format: "dd-MMM-yyyy HH:mm")
        let selectedStr = self.dateString(date:selectedDate, format:"yyyy-MM-dd HH:mm:ss")
        let finalDate = self.dateStringToDate(string: selectedStr, format: "yyyy-MM-dd HH:mm:ss")
        print("finalDate :",finalDate.description)
        
        //Get calendar
        let calendar = NSCalendar.current
        
        let component = calendar.dateComponents([.day, .month, .year, .hour, .minute, .second], from: Date())
        
        //Convert to NSDate
        let today = calendar.date(from: component)
        print("current date :",today?.description ?? "")
        
        if finalDate.timeIntervalSince(today!).sign == .minus {
            //less than today
            return false
        } else {
            //greater than today
            return true
        }
    }
    
    // MARK: - Picker Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(pickerView.tag == 1){
            return self.slotDates.count
        } else if(pickerView.tag == 2){
            return self.slotTimes.count
        } else  {
            return 0;
        }
    }
    
    // Return the title of each row in your picker ... In my case that will be the profile name or the username string
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1){
            //print("Picker view : \(pickerView.tag) ,Slot date:\(self.slotDates[row])")
            self.selectedDate = self.slotDates[row]
            //let formatedDate = getFormattedDate(dateString: self.selectedDate)
            //return formatedDate
            return self.selectedDate
            
        } else if(pickerView.tag == 2){
            //print("Picker view : \(pickerView.tag) ,Slot time:\(slotTimes[row])")
            let time =  self.slotTimes[row]
            self.selectedTimeSlot = time
            //return  UTCToLocalForCV(date: time)
            return  self.selectedTimeSlot
        } else  {
            return "";
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1){
            //  print("Picker view : \(pickerView.tag) ,Slot date:\(self.slotDates[row])")
            self.selectedDate = self.slotDates[row]
        } else if(pickerView.tag == 2){
            // print("Picker view : \(pickerView.tag) ,Slot time:\(slotTimes[row])")
            let time =  self.slotTimes[row]
            self.selectedTimeSlot = time
        }
    }
    
    // MARK: - Razorpay delegate methods
    
//    func onPaymentSuccess(_ payment_id: String) {
//        print("paymentid::\(payment_id) ")
//        razorpay = nil
//        self.paymentGateWay = "razorpay"
//        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
//        bookingConfirm(appointmentID: bookingDict.object(forKey: "apt_id") as! String, transactionID: payment_id, completion: {(done,dict) in
//            if(done)
//            {
//                if(self.isFromCart)
//                {
//                    self.removeFromCart(packageID: Int(self.dictPlan.object(forKey: "package_id") as! String)!, completion: {(done) in
//                        if(done)
//                        {
//                            let arrTemp : NSMutableArray =  App.cartArray.mutableCopy() as! NSMutableArray
//                            App.cartArray.removeAllObjects()
//                            for obj in arrTemp {
//                                let dict :NSDictionary = obj as! NSDictionary
//                                let strID = String(describing: dict.object(forKey: "package_id") ?? "0")
//                                let strPackageID = String(describing: self.dictPlan.object(forKey: "package_id") ?? "0")
//                                if(Int(strPackageID) != Int(strID)!)
//                                {
//                                    App.cartArray.add(dict)
//                                }
//
//                            }
//                            print("dictConfirm",dict)
//                            self.dictConfirm = dict
//                            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE, sender: self)
//                        }
//                    })
//                }
//                else
//                {
//                    print("dictConfirm",dict)
//                    self.dictConfirm = dict
//                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE, sender: self)
//                }
//
//            }
//        })
//
//    }
    
//    func onPaymentError(_ code: Int32, description str: String) {
//        print("Status code:\(code)  message:\(str)")
//        razorpay = nil
//        self.paymentGateWay = "razorpay"
//        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
//        bookingConfirm(appointmentID: bookingDict.object(forKey: "apt_id") as! String, transactionID:"", completion: {(done,dict) in
//            if(done && dict.value(forKey: "failure") != nil)
//            {
//                if(dict.value(forKey: "failure") as! String == "422")
//                {
//                    let alert = UIAlertController(title: "Payment Failure", message: str, preferredStyle: UIAlertController.Style.alert)
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
//                        _ = self.navigationController?.popViewController(animated: true)
//                    }
//                    alert.addAction(okAction)
//                    self.present(alert, animated: true, completion: nil)
//                }
//            }
//        })
//
//    }
    
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        print("walletName : ",walletName,"paymentData : ",paymentData.debugDescription)
        
        var emailID = ""
        var mobileNo = ""
        var user_id = ""
        var planAmount : Int = 0
        var strOrderID = ""
        if let val = self.bookingDict["amount"] as? Int{
            planAmount = val
            print("planAmount : ",val)
        }
        else
        {
            return
        }
        if let val = self.bookingDict["apt_id"] as? String{
            strOrderID = val
        }
        else
        {
            return
        }
        if let user = App.userDetails {
            if let uEmail = user.email {
                emailID = uEmail
            }
            
            if let uid = user.user_id {
                user_id = "\(uid)"
            }
            
            if let uPhone = user.phone {
                mobileNo = uPhone
            }
        }
        
        mobileNo = paymentData?["contact"] as? String ?? "8822127127"
        emailID = paymentData?["email"] as? String ?? "appaccount@doconline.com"
        print("mobileNo : ",mobileNo,"emailID : ",emailID)
        
//        let merchantConfig = PGMerchantConfiguration.default();
//        merchantConfig?.checksumGenerationURL = AppURLS.URL_PAYTM_CHECKSUM
//        //merchantConfig?.checksumValidationURL = "https://c0ae37f4.ngrok.io/PaytmChecksum/verifyChecksum.php"
//        merchantConfig?.merchantID = PaytmCredentials.liveMerchantID
//        merchantConfig?.website = PaytmCredentials.liveWebsite
//        merchantConfig?.industryID = PaytmCredentials.liveIndustryID
//        merchantConfig?.channelID = PaytmCredentials.channelID
        
        let httpBody = "\(Keys.KEY_PAYTM_CALLBACK_URL)=\(PaytmCredentials.callBackUrl)\(strOrderID)&\(Keys.KEY_PAYTM_CHANNEL_ID)=\(PaytmCredentials.channelID)&\(Keys.KEY_PAYTM_CUST_ID)=\(user_id)&\(Keys.KEY_PAYTM_INDUSTRY_TYPE_ID)=\(PaytmCredentials.liveIndustryID)&\(Keys.KEY_PAYTM_MID)=\(PaytmCredentials.liveMerchantID)&\(Keys.KEY_PAYTM_ORDER_ID)=\(strOrderID)&\(Keys.KEY_PAYTM_TXN_AMOUNT)=\(planAmount)&\(Keys.KEY_PAYTM_WEBSITE)=\(PaytmCredentials.liveWebsite)&\(Keys.KEY_PAYTM_EMAIL)=\(emailID)&\(Keys.KEY_PAYTM_MOBILE_NO)=\(mobileNo)"
        print("httpBody :",httpBody)
        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        self.getCheckSum(AppURLS.URL_PAYTM_CHECKSUM, httpBody, completionHandler :  { (success) -> Void in
            if success {
                
                DispatchQueue.main.async(execute: {
                    
                    print("CHECKSUMHASH :",self.strCheckSum)
                    let odrDict : Dictionary<AnyHashable,Any> = [
                        Keys.KEY_PAYTM_CALLBACK_URL :  "\(PaytmCredentials.callBackUrl)\(strOrderID)",
                        Keys.KEY_PAYTM_CHANNEL_ID : PaytmCredentials.channelID,
                        Keys.KEY_PAYTM_CHECKSUMHASH : self.strCheckSum,
                        Keys.KEY_PAYTM_CUST_ID : user_id,
                        Keys.KEY_PAYTM_INDUSTRY_TYPE_ID :  PaytmCredentials.liveIndustryID,
                        Keys.KEY_PAYTM_MID : PaytmCredentials.liveMerchantID,
                        Keys.KEY_PAYTM_ORDER_ID : strOrderID,
                        Keys.KEY_PAYTM_TXN_AMOUNT : "\(planAmount)",
                        Keys.KEY_PAYTM_WEBSITE : PaytmCredentials.liveWebsite,
                        Keys.KEY_PAYTM_EMAIL : emailID,
                        Keys.KEY_PAYTM_MOBILE_NO : mobileNo
                    ]
                    print("odrDict :",odrDict.debugDescription)
//                     let order: PGOrder = PGOrder(params: odrDict)
                    
//                    let transactionController = PGTransactionViewController.init(transactionFor: order)
//                    transactionController? .serverType = eServerTypeProduction
//                    transactionController? .merchant = merchantConfig
//                    transactionController? .delegate = self
//                    self.navigationController?.pushViewController(transactionController!, animated: true)
                })
                
            }else {
            }
        })
        
    }
    
    // MARK: Paytm Delegate methods.
//    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
//        print("responseString : ",responseString)
//        if(responseString == "")
//        {
//
//        }
//        else
//        {
//            let jsonData : NSMutableDictionary = (responseString as String).parseJSONString as! NSMutableDictionary
//            print("jsonData : ",jsonData)
//            let status = jsonData.value(forKey: "STATUS") as! String
//            var paymentID = ""
//            if(status == "TXN_FAILURE")
//            {
//                paymentID = ""
//            }
//            else if(status == "TXN_SUCCESS")
//            {
//                paymentID = jsonData.value(forKey: "TXNID") as! String
//            }
//            _ = navigationController?.popViewController(animated: true)
//
//            self.paymentGateWay = "paytm"
//            razorpay = nil
//            DispatchQueue.main.async(execute: {
//                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
//            })
//
//            bookingConfirm(appointmentID: bookingDict.object(forKey: "apt_id") as! String, transactionID: paymentID, completion: {(done,dictResponse) in
//                if(done)
//                {
//                    var statusResponse = "0"
//                    if let val = dictResponse["failure"]
//                    {
//                        statusResponse = val as! String
//                    }
//
//                    if(statusResponse == "422")
//                    {
//                        self.didShowAlert(title: "Paytm", message: "Payment failed")
//                    }
//                    else if(self.isFromCart)
//                    {
//                        self.removeFromCart(packageID: Int(self.dictPlan.object(forKey: "package_id") as! String)!, completion: {(done) in
//                            if(done)
//                            {
//                                let arrTemp : NSMutableArray =  App.cartArray.mutableCopy() as! NSMutableArray
//                                App.cartArray.removeAllObjects()
//                                for obj in arrTemp {
//                                    let dict :NSDictionary = obj as! NSDictionary
//                                    let strID = String(describing: dict.object(forKey: "package_id") ?? "0")
//                                    let strPackageID = String(describing: self.dictPlan.object(forKey: "package_id") ?? "0")
//                                    if(Int(strPackageID) != Int(strID)!)
//                                    {
//                                        App.cartArray.add(dict)
//                                    }
//
//                                }
//                                print("dictConfirm from cart",dictResponse)
//                                self.dictConfirm = dictResponse
//                                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE, sender: self)
//                            }
//                        })
//                    }
//                    else
//                    {
//                        print("dictConfirm",dictResponse)
//                        self.dictConfirm = dictResponse
//                        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE, sender: self)
//                    }
//
//                }
//            })
//        }
//    }
    
//    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
//        _ = navigationController?.popViewController(animated: true)
//    }
    
//    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
//        print("error :",error.localizedDescription.debugDescription)
//        print("controller :",controller.merchant.merchantID)
//        _ = navigationController?.popViewController(animated: true)
//    }
    
    // MARK: - UIButton Action
    
    @objc func onNotificationButtonClick() {
        print("Notification button Clicked ")
        if(App.cartCount != 0)
        {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_CART_SEGUE, sender: self)
        }
        else
        {
            self.didShowAlert(title: "Cart", message: "There are no items in cart")
        }
    }
    
    @IBAction func ClickBtnDOB(_ sender: UIButton) {
        
        //let picker = DateTimePicker.show()
        //        let picker = DateTimePicker.show(minimumDate: Date())
        //        picker.timeZone = TimeZone.current
        //        picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        //        picker.isDatePickerOnly = false // to hide time and show only date picker
        //        picker.completionHandler = { date in
        //            // do something after tapping done
        //            print("Done tapped : ",date.description,"Date string : ",self.dateString(date:date, format:"yyyy-MM-dd HH:mm:ss +0000"))
        //            self.txtDate.text = self.dateString(date:date, format:"dd-MMM-yyy HH:mm")//"yyyy-MM-dd HH:mm:ss +0000")
        //
        //        }
    }
    
    @IBAction func ClickBtnAddFamily(_ sender: UIButton) {
//        vwFamily.isHidden = false
//        vwBG.isHidden = false
//        tableView.reloadData()
        
        if (UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER))
        {
            
        }
        else{
            
            if (self.familyMemrs.count > 0)
            {
                self.imgExpand.transform = CGAffineTransform(rotationAngle: -0.999*CGFloat.pi)
                tableFamilyView.reloadData()
                LC_tableFamilyViewHeight.constant = self.tableFamilyView.contentSize.height
            }
            else
            {
            }
            
        }
        
    }
    
    @IBAction func ClickBtnConfirm(_ sender: UIButton) {
        
        //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE, sender: self)
        //return
        
        if (isValidated())
        {
            getJsonData()
            App.isDiagnostics = true
            App.consultationDate = self.dateString(date:self.dateStringToDate(string: self.txtDate.text ?? "", format: "dd-MMM-yyyy HH:mm"), format:"yyyy-MM-dd HH:mm:ss")
            App.selectedSlotTime = App.consultationDate
            App.selectedUserForBooking = App.user_full_name + "(myself)"
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONSENT_FORM, sender: self)
        }
        
    }
    
//    func showPaymentForm(options:Dictionary<AnyHashable,Any>) {
//        razorpay = RazorpayCheckout.initWithKey(rz_key, andDelegate: self)
//        razorpay.setExternalWalletSelectionDelegate(self)
//        razorpay.open(options, displayController: self)
//
//    }
    
    @IBAction func ClickBtnDone(_ sender: UIButton){
        print("membersCount : ",membersCount)
        print("selectedMembers : ",familyMemrs.count, "selectedMembers : ",familyMemrs.description)
        //vwFamily.isHidden = true
        vwBG.isHidden = true
        
        //self.getJsonData()
        
    }
    
    
    @IBAction func ClickBtnViewHistory(_ sender: UIButton) {
        
        //self.vwAppointmentDetails.isHidden = true
        //self.vwBG.isHidden = true
        //NotificationCenter.default.post(name: Notification.Name(rawValue: "displayHistory"), object: nil)
        //_ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClickBtnAddress(_ sender: UIButton) {
        if(App.addressArray.count > 0)
        {vwChooseAddress.isHidden = false}
        else
        {
            let alert = UIAlertController(title: "Address", message: "You do not have any address, try to add one.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SET_ADDRESS, sender: self)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    @IBAction func ClickBtnClose(_ sender: UIButton) {
        
        vwChooseAddress.isHidden = true
        
    }
    @IBAction func ClickBtnOK(_ sender: UIButton) {
        
        for obj in App.addressArray {
            let dict :NSDictionary = obj as! NSDictionary
            let selected = dict.object(forKey: "is_default") as! Bool
            if(selected)
            {
                setDefaultValues(dict: dict)
                vwChooseAddress.isHidden = true
                break;
            }
        }
        
    }
    @IBAction func ClickBtnCancel(_ sender: UIButton) {
        vwChooseAddress.isHidden = true
    }
    @IBAction func ClickBtnNeedPrint(_ sender: UIButton) {
//        sender.isSelected = !sender.isSelected
        if isNeedPrint{
            isNeedPrint = false
            let checkImg = UIImage(named: "UncheckedBox")?.withRenderingMode(.alwaysTemplate)
            sender.setImage(checkImg, for: UIControl.State.normal)
            sender.tintColor = Theme.navigationGradientColor![0]!
        }else{
            isNeedPrint = true
            let checkImg = UIImage(named: "CheckedBox")?.withRenderingMode(.alwaysTemplate)
            sender.setImage(checkImg, for: UIControl.State.normal)
            sender.tintColor = Theme.navigationGradientColor![0]!
        }
        print("isNeedPrint \(isNeedPrint)")
    }
    @IBAction func ClickBtnAgree(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func ClickBtnDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        /*
         scrVW.setContentOffset(CGPoint(x: scrVW.contentOffset.x, y: txtDate.frame.origin.y-100), animated: true)
         if(picker != nil)
         {
         picker.removeFromSuperview()
         picker = nil
         }
         picker = DateTimePicker.show(minimumDate: Date())
         picker.doneBackgroundColor = UIColor.init(hexString: "#6dbe45")
         picker.timeZone = TimeZone.current
         picker.timeInterval = .fifteen
         picker.highlightColor = UIColor.init(hexString: "6dbe45")
         picker.isDatePickerOnly = false // to hide time and show only date picker
         picker.completionHandler = { date in
         // do something after tapping done
         self.scrVW.setContentOffset(CGPoint(x: self.scrVW.contentOffset.x, y: self.scrVW.contentSize.height - self.scrVW.frame.size.height), animated: true)
         print("picker.selectedDateString :",self.picker.selectedDateString)
         print("picker.selectedDate :",self.picker.selectedDate.description)
         print("Done tapped : ",date.description,"Date string : ",self.dateString(date:date, format:"yyyy-MM-dd HH:mm:ss +0000"))
         self.txtDate.text = self.dateString(date:date, format:"dd-MMM-yyy HH:mm")//"yyyy-MM-dd HH:mm:ss +0000")
         self.picker.removeFromSuperview()
         self.picker = nil
         }*/
        
        
        if self.slotDates.count > 0//self.slotsData != nil
        {
            self.showPickerInActionSheet(sentBy: "dates")
        }else {
            self.didShowAlert(title: "", message: "No slots available")
        }
        
    }
    
    func getJsonData(){
        
        if (UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER))
        {
            jsonString = "{\"self\":0}"
            membersCount = 1
        }
        else
        {
            jsonString = "{\"self\":0}"
            membersCount = 1
            
            let jsonObject: NSMutableDictionary = NSMutableDictionary()
            if(self.familyMemrs.count > 0 && selectedUserIndex != 0)
            {
                let user = self.familyMemrs[selectedUserIndex]
                let arr = user.name?.components(separatedBy: " ")
                let strName = getUserName(arrString: arr!)//arr![1] + " " + arr![2]
                jsonObject.setValue(user.id, forKey: strName)
                
                membersCount = 1
                
                let jsonData: NSData
                
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
                    jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    print("json string = \(jsonString)")
                    
                } catch _ {
                    print ("JSON Failure")
                }
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func UpdateUIFields()  {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
                self.userDataModel  = User.get_user_profile_default()
                print("status:\(UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS))  User First name :\(self.userDataModel.first_name!)")
                //self.txtOrderBy.text = self.userDataModel.first_name! + self.userDataModel.middle_name! + self.userDataModel.last_name!
                self.txtOrderBy.text = self.userDataModel.full_name
                self.strOrderBy = self.userDataModel.first_name! + self.userDataModel.middle_name! + self.userDataModel.last_name!
                /*
                let strAddress = self.userDataModel.address1! + "\n" + self.userDataModel.address2!
                if strAddress == "\n"
                {
                    self.txtAddress1.text = "Address"
                }
                else
                {
                    self.txtAddress1.text = self.userDataModel.address1! + "\n" + self.userDataModel.address2!
                }*/
                self.txtAddress1.text = self.userDataModel.address1!
                self.txtAddress2.text =   self.userDataModel.address2!
                self.txtPinCode.text = self.userDataModel.pincode!
                self.txtMobile.text = self.userDataModel.phone!
                self.txtEmail.text = self.userDataModel.email!
                self.txtCity.text = self.userDataModel.city!
                self.txtState.text = self.userDataModel.state!
                
                let partnerID:String = "1"
                print("partner_id :", partnerID)
                print("txtPinCode.text :",self.txtPinCode.text!)
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                if !self.txtPinCode.text!.isEmpty
                {
                    self.CheckPinCode(partner_id: partnerID, pincode: self.txtPinCode.text!, completion: { (done) in
                        if(done)
                        {
                            print("done :",done)
                        }
                    })
                }
            }
        }
    }
    
    func ClickBtnCheck(sender:UIButton) {
        
        let indexPath = IndexPath(row: sender.indexTag!, section: 0)
        
        let cell = tableView.cellForRow(at: indexPath) as! DiagnosticFamilyCell
        cell.btnCheck.isSelected = true
        let arrTemp : NSMutableArray =  App.addressArray.mutableCopy() as! NSMutableArray
        App.addressArray.removeAllObjects()
        for obj in arrTemp {
            let dict :NSDictionary = obj as! NSDictionary
            let dictAdd :NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
            let strID = String(describing: dictAdd.object(forKey: "address_id") ?? "0")
            dictAdd.setValue(false, forKey: "is_default")
            if(cell.btnCheck.tag == Int(strID)!)
            {
                dictAdd.setValue(true, forKey: "is_default")
            }
            
            
            App.addressArray.add(dictAdd)
        }
        tableView.reloadData()
        
    }
    
    // MARK: - Web services
    
    @objc func bookConsultation(completion: @escaping (_ done:Bool,_ dict:NSDictionary) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                DispatchQueue.main.async {
                    self.stopAnimating()
                    completion(false,[:])
                }
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            //if(self.strDefaultAdd == "" || self.strDefaultAdd.isEmpty)
            //{
            self.strDefaultAdd = "\(String(describing: self.txtAddress1.text ?? "")) \(String(describing: self.txtAddress2.text ?? "")) \(String(describing: self.txtState.text ?? "")) \(String(describing: self.txtCity.text ?? ""))"
            //}
            
            var strEmailId = self.txtEmail.text
            if(strEmailId == "")
            {
                strEmailId = "diagnosticreports@doconline.com"
            }
            dataToPost = "\(Keys.KEY_ADDRESS_FULL)=\(self.strDefaultAdd)&\(Keys.KEY_PINCODE)=\(self.txtPinCode.text ?? "")&\(Keys.KEY_MOBILE_NUMBER)=\(self.txtMobile.text ?? "")&\(Keys.KEY_EMAIL)=\(strEmailId  ?? "")&\(Keys.KEY_ORDER_BY)=\(self.strOrderBy)&\(Keys.KEY_APPOINTMENT_DATE)=\(self.dateString(date:self.dateStringToDate(string: self.txtDate.text ?? "", format: "dd-MMM-yyyy HH:mm"), format:"yyyy-MM-dd HH:mm:ss") )&\(Keys.KEY_PRINT_OUT)=\(self.isNeedPrint ? 1:0)&\(Keys.KEY_BENEFICIARY_ID)=\(jsonString ?? "")&\(Keys.KEY_BENEFICIARY_COUNT)=\(self.membersCount)&\(Keys.KEY_PACKAGE_ID)=\(dictPlan.value(forKey: "package_id") ?? 0)&\(Keys.KEY_PARTNER_ID)=\(1)"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_DIAGNOSTICS_APPOINTMENT
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        completion(false,[:])
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        completion(false,[:])
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    print("data=\(data)")
                                    completion(true,data)
                                    self.lbl_appointmentID.text = data.object(forKey: "apt_id") as? String
                                    self.lbl_appointmentDate.text = data.object(forKey: "apt_dt") as? String
                                    self.lbl_PackageName.text = data.object(forKey: "package_name") as? String
                                    if((data.object(forKey: "b2b_available") as? Int) == 0)
                                    {
                                        UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER)
                                    }
                                    else
                                    {
                                        UserDefaults.standard.set(true, forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER)
                                    }
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false,[:])
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false,[:])
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    
    @objc func getAddress(completion: @escaping (_ done:Bool,_ dictDefault:NSDictionary) -> ()) {
        print("*****")
        if !NetworkUtilities.isConnectedToNetwork()
        {
            DispatchQueue.main.async {
                self.stopAnimating()
                completion(false,[:])
            }
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        //self.linearBar.startAnimation()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_ADDRESS_LIST
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    //self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        completion(false,[:])
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.stopAnimating()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        
                                        completion(false,[:])
                                        //self.didShowAlert(title: "", message: message)
                                        //self.linearBar.stopAnimation()
                                        let alert = UIAlertController(title: "Address", message: message, preferredStyle: UIAlertController.Style.alert)
                                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                                            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SET_ADDRESS, sender: self)
                                        }
                                        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                        
                                        alert.addAction(okAction)
                                        alert.addAction(cancel)
                                        self.present(alert, animated: true, completion: nil)
                                        self.stopAnimating()
                                    })
                                }
                            }
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                
                                DispatchQueue.main.async(execute: {
                                    App.addressArray = data.mutableCopy() as! NSMutableArray
                                    var isDefaultAddress : Bool  = false
                                    for obj in App.addressArray {
                                        let dict :NSDictionary = obj as! NSDictionary
                                        let strDefault = String(describing: dict.object(forKey: "is_default") ?? "0")
                                        if strDefault == "1"
                                        {
                                            completion(true,dict)
                                            isDefaultAddress = true
                                            break
                                        }
                                    }
                                    if isDefaultAddress == false
                                    {
                                        completion(true,App.addressArray.object(at: 0) as! NSDictionary)
                                    }
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                    self.tableView.reloadData()
                                    
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    completion(false,[:])
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            completion(false,[:])
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    @objc func bookingConfirm(appointmentID:String,transactionID:String,completion: @escaping (_ done:Bool,_ dict:NSDictionary) -> ()) {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                DispatchQueue.main.async {
                    self.stopAnimating()
                    completion(false,[:])
                }
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            dataToPost = "\(Keys.KEY_APPOINTMENT_DIAG_ID)=\(appointmentID)&\(Keys.KEY_TRANSACTION_ID)=\(transactionID)&\(Keys.KEY_PAYMENT_GATEWAY)=\(self.paymentGateWay)"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_DIAGNOSTICS_APPOINTMENT_CONFIRM
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        completion(false,[:])
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        completion(false,[:])
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    self.stopAnimating()
                                    print("data=\(data)")
                                    let succ = data.object(forKey: "success") as! Int
                                    if(succ == 0)
                                    {
                                        completion(true,["failure":"422"])
                                    }
                                    else if (succ == 1)
                                    {
                                        completion(true,data)
                                    }
                                    
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    completion(false,[:])
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completion(false,[:])
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    @objc func removeFromCart(packageID:Int,completion: @escaping (_ done:Bool) -> ()) {
        
        //self.linearBar.startAnimation()
        
        let defaults = UserDefaults.standard
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            //self.startAnimating()
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_CART_REMOVE
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            
            request.httpMethod = HTTPMethods.DELETE
            
            let userData = "{\"\(Keys.KEY_PACKAGE_ID)\":\"\(packageID)\"}"
            
            request.httpBody = userData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        print("Error==> : \(error.localizedDescription)")
                        completion(false)
                        //self.linearBar.stopAnimation()
                        self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        completion(false)
                                        //self.linearBar.stopAnimation()
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    App.cartCount = data.object(forKey: "updated_cart_count") as! Int
                                    App.cartAmount = data.object(forKey: "updated_cart_amount") as! Float
                                    completion(true)
                                    //self.linearBar.stopAnimation()
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    completion(false)
                                    //self.linearBar.stopAnimation()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            completion(false)
                            //self.linearBar.stopAnimation()
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    @objc func CheckPinCode(partner_id:String,pincode:String,completion: @escaping (_ done:Bool) -> ()) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async(execute: {
                completion(false)
                self.stopAnimating()
            })
            return
        }
        //self.linearBar.startAnimation()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = "\(AppURLS.URL_DIAGNOSTICS_PINCODE)/\(partner_id)/\(pincode)"
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        completion(false)
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        //self.linearBar.stopAnimation()
                        self.stopAnimating()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        
                                        self.lbl_PinCode.textColor = UIColor.red
                                        self.lbl_PinCode.text = message
                                        self.btnPin.isSelected = false
                                        completion(true)
                                        //self.linearBar.stopAnimation()
                                        self.stopAnimating()
                                    })
                                }
                            }
                            print("performing error handling time slots:\(resultJSON)")
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            
                            print("data=\(data.debugDescription)")
                            if code == 201 && status == "success"
                            {
                                
                                DispatchQueue.main.async(execute: {
                                    self.lbl_PinCode.textColor = UIColor.init(hexString: "6dbe45")//UIColor.red
                                    self.lbl_PinCode.text = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String
                                    self.btnPin.isSelected = true
                                    completion(true)
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    completion(false)
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            completion(false)
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
    }
    
    /**
     Method performs request to server for family members
     */
    func getFamilyMembers(completionHandler : @escaping (_ success:Bool) -> Void) {
        self.tempfamilyMemrs.removeAll()
        let id = User.get_user_profile_default().id ?? 0
        let user = User(id: id , userName: App.user_full_name, type: "self", userAvatar: App.avatarUrl,age:"")
        self.tempfamilyMemrs.append(user)
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async(execute: {
                completionHandler(false)
                self.stopAnimating()
            })
            return
        }
        
        let slotURL = AppURLS.URL_Family_mem + "-members"
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.httpMethod = HTTPMethods.GET
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    completionHandler(false)
                    self.stopAnimating()
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling family mem:\(resultJSON)")
                        DispatchQueue.main.async(execute: {
                            completionHandler(false)
                            self.stopAnimating()
                        })
                    }
                    else {
                        print("family mem response:\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        
                        if let dict = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                            completionHandler(true)
                            self.familyMem = dict
                            App.familyMembers = dict
                            print("Fetched users")
                            
                            var count = 0
                            for mem in dict {
                                count += 1
                                if let name = mem.object(forKey:Keys.KEY_FULLNAME) as? String,let id = mem.object(forKey: Keys.KEY_USER_ID) as? Int , let bookable = mem.object(forKey: Keys.KEY_BOOKABLE) as? Bool ,let age = mem.object(forKey: Keys.KEY_AGE) as? String{
                                    if bookable {
                                        let user = User(id: id, userName: name, type: "family", userAvatar: "",age: age)
                                        self.tempfamilyMemrs.append(user)
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async(execute: {
                                
                                if count < 3 { //self.tempfamilyMemrs.count < 4 {
                                    let addNew = User(id: 0, userName: "Add New", type: "Other", userAvatar: "AddFamily",age:"")
                                    self.tempfamilyMemrs.append(addNew)
                                }
                                self.familyMemrs =  self.tempfamilyMemrs
                                print("Fetched users : ",self.familyMemrs.description)
                            })
                        }
                    }
                    
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        completionHandler(false)
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-w@objc ell-formatted JSON : \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    /**
     Method performs request to server for total family members
     */
    func getTotalFamilyMembers(completionHandler : @escaping (_ success:Bool) -> Void) {
        self.tempfamilyMemrs.removeAll()
        let id = User.get_user_profile_default().id ?? 0
        let user = User(id: id , userName: App.user_full_name, type: "self", userAvatar: App.avatarUrl,age:"")
        self.tempfamilyMemrs.append(user)
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async(execute: {
                completionHandler(false)
                self.stopAnimating()
            })
            return
        }
        
        let slotURL = AppURLS.URL_Family_mem
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.httpMethod = HTTPMethods.GET
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    completionHandler(false)
                    self.stopAnimating()
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling family mem:\(resultJSON)")
                        DispatchQueue.main.async(execute: {
                            completionHandler(false)
                            self.stopAnimating()
                        })
                    }
                    else {
                        print("Total family mem response:\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        
                        if let dict = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                            completionHandler(true)
                            self.familyMem = dict
                            App.familyMembers = dict
                        }
                    }
                    
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        completionHandler(false)
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-w@objc ell-formatted JSON : \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    /**
     Method performs request to server for total family members
     */
    func getTimeSlots(strPincode : String,strDate : String ,completionHandler : @escaping (_ success:Bool) -> Void) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async(execute: {
                completionHandler(false)
                self.stopAnimating()
            })
            return
        }
        
        let convertedDate = self.dateStringToDate(string: strDate, format: "dd-MMM-yyyy")
        let stringDate = self.dateString(date: convertedDate, format: "yyyy-MM-dd")
        
        
        let strUrl = AppURLS.URL_DIAGNOSTICS_TIME_SLOTS
        var parId : Int = 0
        if (self.dictPlan.object(forKey: "partner_id") as? Int) != nil {
            parId = (self.dictPlan.object(forKey: "partner_id") as? Int)!
        }
        else
        {
            parId = Int(self.dictPlan.object(forKey: "partner_id") as! String)!
        }
        print("partner id :",parId)
        
        let slotURL = "\(strUrl)\(parId)/\(strPincode)/\(stringDate)"//AppURLS.URL_Family_mem
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.httpMethod = HTTPMethods.GET
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    completionHandler(false)
                    self.stopAnimating()
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if response is json do the following
                do
                {
                    
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling timeslots:\(resultJSON)")
                        DispatchQueue.main.async(execute: {
                            completionHandler(false)
                            self.stopAnimating()
                        })
                    }
                    else {
                        print("Slots response:\(resultJSON)")
                        
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                        
                        if code == 200 && status == "success"
                        {
                            print("Success")
                            completionHandler(true)
                            for obj in data
                            {
                                let dict : NSDictionary = obj as! NSDictionary
                                //let strTemp : String = dict.object(forKey: "Slot") as! String
                                //let arrTemp : [String] = strTemp.components(separatedBy: " - ")
                                self.slotTimes.append(dict.object(forKey: "start_time") as! String)
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                
                                if self.slotTimes.count == 0 {
                                    self.didShowAlert(title: "Sorry!", message: "No more slots available for the day")
                                }else {
                                    self.showPickerInActionSheet(sentBy: "slots")
                                }
                            })
                        }
                        else
                        {
                            
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                completionHandler(false)
                            })
                        }
                        
                    }
                    
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        completionHandler(false)
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-w@objc ell-formatted JSON : \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    // MARK: Paytm Checksum generator API
    func getCheckSum(_ actionURL:String,_ dataToPost:String,completionHandler : @escaping (_ success:Bool) -> Void ) {
        
        let config = URLSessionConfiguration.default
        let mySession = URLSession(configuration: config)
        let url = URL(string: actionURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.POST
        let postData = dataToPost
        print("Data==>\(postData)")
        print("URL==>\(actionURL)")
        request.httpBody = dataToPost.data(using: String.Encoding.utf8)
        let task = mySession.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    ///making complition handler false
                    completionHandler(false)
                    ///Stop spinning animation
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                    completionHandler(false)
                    if error.localizedDescription == "The request timed out."
                    {
                        self.didShowAlert(title: "Internet failure", message: "Internet connection appears to be offline")
                        
                    }
                    else
                    {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        
                    }
                })
            }
            
            if let data = data
            {
                print("data =\(data)")
            }
            
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                
                //if you response is json do the following
                do{
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        self.strCheckSum = resultJSON.object(forKey: "CHECKSUMHASH") as! String
                        print("resultJSON :",resultJSON)
                        print("CHECKSUMHASH :",resultJSON.object(forKey: "CHECKSUMHASH") as! String)
                        
                        //let dict : NSDictionary = resultJSON.object(at: 0) as! NSDictionary
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            completionHandler(true)
                        })
                        
                    }
                }catch let error {
                    completionHandler(false)
                    print("Received not-well-formatted JSON:=>\(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE {
            let destVC = segue.destination as! ThankYouViewController
            //destVC.packageID = self.packageID
            destVC.dictPlans = dictConfirm
        }else  if segue.identifier == StoryboardSegueIDS.ID_CONSENT_FORM {
            if let destVC = segue.destination as? ConsentFormViewController {
                destVC.strDiagnoMobile = self.txtMobile.text!
                App.selectedUserForBooking = self.familyMemrs[self.selectedUserIndex].name!
                destVC.delegate = self
                destVC.isFromDiagnosis = true
            }
        }
    }
    
    
    func getUserName(arrString:[String]) -> String
    {
        var strName = ""
        if arrString.count >= 4 {
            strName = arrString[1] + " " + arrString[2] + " " + arrString[3]
        }
        else if arrString.count == 3 {
            strName = arrString[1] + " " + arrString[2]
        }
        else if arrString.count == 2 {
            strName = arrString[0] + " " + arrString[1]
            if strName != "Add New"
            {
                strName = arrString[1]
            }
        }
        return strName
    }
    
    
}

extension DiagnosticsPlanDetailsViewController : ConsentFormDelegate {
    func didUserAcceptedPolicyOnFollowUp(accept: Bool, appointmentId: String, followUpReason: String) {
        
    }
    
    func didUserAcceptPolicy(accept: Bool) {
        App.isFromView = FromView.ConsentFormView
        self.navigationController?.popViewController(animated: true)
        if accept {
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            bookConsultation(completion: {(done,dict) in
                if(done)
                {
                    print("dict :",dict)
                    self.bookingDict = dict
                    var email = ""
                    var phone = ""
                    var user_id = ""
                    
                    if let user = App.userDetails {
                        if let uEmail = user.email {
                            email = uEmail
                            print("email :",email)
                        }
                        
                        if let uid = user.user_id {
                            user_id = "\(uid)"
                        }
                        
                        if let uPhone = user.phone {
                            phone = uPhone
                            print("phone :",phone)
                        }
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        if (UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER))
                        {
                            print("B2B USER")
                            self.paymentGateWay = ""
//                            self.razorpay = nil
                            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                            self.bookingConfirm(appointmentID: self.bookingDict.object(forKey: "apt_id") as! String, transactionID:"", completion: {(done,dict) in
                                if(done && dict.value(forKey: "failure") != nil)
                                {
                                    if(dict.value(forKey: "failure") as! String == "422")
                                    {
                                        let alert = UIAlertController(title: "Payment Failure", message: "B2B payment flow", preferredStyle: UIAlertController.Style.alert)
                                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                        alert.addAction(okAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else
                                {
                                    print("dictConfirm",dict)
                                    self.dictConfirm = dict
                                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_THANKYOU_SEGUE, sender: self)
                                }
                            })
                        }
                        else
                        {
                            let options : Dictionary<AnyHashable,Any> = [
                                "key": self.rz_key,
                                "amount" : "\(dict.value(forKey: "amount") ?? "")00",
                                "appointment_id" :  dict.value(forKey: "apt_id") ?? "",
                                //"recurring": true ,
                                "name": "DocOnline",
                                "image" : "https://app.doconline.com/assets/images/logo.png" ,
                                "description": "one_time_payment",
                                "external" : ["wallets" : ["paytm"]],
                                "prefill" : [
                                    "email" : self.txtEmail.text,
                                    "contact": self.txtMobile.text
                                ],
                                "notes": [
                                    "doconline_user_id" : user_id,
                                    "platform": "ios",
                                    "address": "DocOnline",
                                    "action_type" : "onetime",
                                    //"subscription_id" :  sub_id
                                ]
                            ]
//                            self.showPaymentForm(options: options)
                        }
                    })
                }
            })
        }
    }
}

extension DiagnosticsPlanDetailsViewController: UITableViewDelegate ,UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tableFamilyView {
            let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.DropDown, for: indexPath) as! DropDownTableViewCell
            
            let user = self.familyMemrs[indexPath.row]
            
            cell.lb_name.text = user.name!
            /*let arr = user.name?.components(separatedBy: " ")
            let strName = getUserName(arrString: arr!)//arr![1] + " " + arr![2]
            cell.lb_name.text = strName*/
            
            /*
             cell.iv_profilePic.layer.cornerRadius = cell.iv_profilePic.frame.size.width / 2
             cell.iv_profilePic.clipsToBounds = true*/
            
            if user.avatar!.isEmpty {
                cell.iv_profilePic.image = UIImage(named:"User_icon")
            }else if user.avatar!.lowercased().isEqual("addfamily"){
                cell.iv_profilePic.image = UIImage(named:"AddFamily")
            }else {
                //let url = URL(string:user.avatar!)
                //cell.iv_profilePic.kf.setImage(with: url)
                cell.iv_profilePic.image = UIImage(named:"User_icon")
            }
            
            cell.backgroundColor = UIColor.white
            
            switch self.selectedUserIndex {
            case indexPath.row:
                cell.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
                break
            case indexPath.row:
                cell.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
                break
            case indexPath.row:
                cell.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
                break
            default:
                cell.backgroundColor = UIColor.white
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addressList", for: indexPath) as! DiagnosticFamilyCell
            cell.delegate = self
            let dict = App.addressArray.object(at: indexPath.row) as! NSDictionary
            cell.lblName.text = dict.value(forKey: "title") as? String
            cell.btnCheck.isSelected = dict.object(forKey: "is_default") as! Bool
            let strID = String(describing: dict.object(forKey: "address_id") ?? "0")
            cell.btnCheck.tag = Int(strID)!
            cell.btnCheck.indexTag = indexPath.row
            return cell
        }
        //return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == tableFamilyView)
        {
            let user = self.familyMemrs[indexPath.row]
            self.selectedUserIndex = indexPath.row
            if user.name!.lowercased().isEqual("add new") {
                self.selectedUserIndex = 0
                
                if self.familyMem.count == 5
                {
                    self.didShowAlert(title: "", message: "You can't add more than 3 family members")
                }else {
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_FAMILY_MEM, sender: self)
                }
            }
            else
            {
                txtOrderBy.text = user.name
                /*
                let arr = user.name?.components(separatedBy: " ")
                let strName = getUserName(arrString: arr!)//arr![1] + " " + arr![2]
                txtOrderBy.text = strName*/
            }
            LC_tableFamilyViewHeight.constant = 0.0
            UIView.animate(withDuration: 0.5) {
                self.imgExpand.transform = CGAffineTransform.identity
            }
        }
        else{
            let cell = tableView.cellForRow(at: indexPath) as! DiagnosticFamilyCell
            cell.btnCheck.isSelected = true
            let arrTemp : NSMutableArray =  App.addressArray.mutableCopy() as! NSMutableArray
            App.addressArray.removeAllObjects()
            for obj in arrTemp {
                let dict :NSDictionary = obj as! NSDictionary
                let dictAdd :NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                let strID = String(describing: dictAdd.object(forKey: "address_id") ?? "0")
                dictAdd.setValue(false, forKey: "is_default")
                if(cell.btnCheck.tag == Int(strID)!)
                {
                    dictAdd.setValue(true, forKey: "is_default")
                    
                }
                
                App.addressArray.add(dictAdd)
            }
            tableView.reloadData()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == tableFamilyView)
        {
            return self.familyMemrs.count
        }
        else
        {
            return App.addressArray.count//self.members.count
        }
    }
}

extension DiagnosticsPlanDetailsViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField != txtPinCode || textField != txtMobile || textField != txtDate{
            textField.text?.capitalizeFirstLetter()
        }
        if textField == self.txtMobile {
            
            let newLength = (self.txtMobile.text?.count)! + string.count - range.length
            return newLength <= 10
            
        }else if textField == self.txtPinCode {
            
            let newLength = (self.txtPinCode.text?.count)! + string.count - range.length
            if(newLength == 6)
            {
                let partnerID:String = "1"//String(format: "%@", self.dictPlan.object(forKey: "partner_id") as! CVarArg)
                print("partner_id :", partnerID)
                print("txtPinCode.text :","\(self.txtPinCode.text!)\(string)")
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
                self.CheckPinCode(partner_id: partnerID, pincode: "\(self.txtPinCode.text!)\(string)", completion: { (done) in
                    if(done)
                    {
                        print("done :",done)
                        self.view.endEditing(true)
                    }
                })
            }
            else
            {
                self.lbl_PinCode.textColor = UIColor.red
                self.lbl_PinCode.text = "Service not available in the selected area."//0A5D85
                self.btnPin.isSelected = false
            }
            
            return newLength <= 6
            
        }
        else if textField == self.txtOrderBy {
            
            var characterSet = CharacterSet.letters
            characterSet.insert(charactersIn: " ")
            let unwantedStr = string.trimmingCharacters(in: characterSet)
            return unwantedStr.count == 0
        }
        
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("scrVW.contentOffset.y : ",scrVW.contentOffset.y)
    }
    
}

protocol DiagnosticFamilyCellDelegate: class
{
    func ClickBtnCheck(sender:UIButton)
}

class DiagnosticFamilyCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnCheck: UIButton!
    weak var delegate: DiagnosticFamilyCellDelegate? = nil
    
    @IBAction func ClickCkeckBtn(_ sender: UIButton) {
        print("selected")
        delegate?.ClickBtnCheck(sender: sender)
    }
    
}

extension DiagnosticsPlanDetailsViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Address"
            textView.textColor = UIColor.lightGray
        }
    }
    
}

extension DiagnosticsPlanDetailsViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
    }
}

