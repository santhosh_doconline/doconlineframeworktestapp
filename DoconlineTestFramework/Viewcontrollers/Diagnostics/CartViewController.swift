//
//  CartViewController.swift
//  DocOnline
//
//  Created by Doconline india on 25/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class CartViewController: UIViewController,DiagnosticCartCellDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var mainBackground: UIView!
    @IBOutlet var shadowLayer: UIView!
    @IBOutlet var lblTotal: UILabel!
    var dictPlans : NSDictionary!
    var cartAmount = 0.0
    
    var expectedContentLength = 0
    ///linear bar instance reference
    //let linearBar: LinearProgressBar = LinearProgressBar()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        mainBackground.layer.cornerRadius = 5
        mainBackground.layer.masksToBounds = true
        
        shadowLayer.setShadowView(opacity: 0.4, radius: 8)
        
        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        getCartCollection()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblTotal.text = "Subtotal(\(String(describing: App.cartCount)) items): \(String(describing: App.cartAmount))"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
     */
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE {
            let destVC = segue.destination as! DiagnosticsPlanDetailsViewController
            destVC.isFromCart = true
            App.isFromView = FromView.CartView
            destVC.packageID = dictPlans.object(forKey: "package_id") as? Int
            destVC.partnerID  = dictPlans.object(forKey: "partner_id") as? Int
            destVC.dictPlan  = dictPlans
        }
    }
    
    
    // MARK: - DiagnosticCartCellDelegate
    
    func clickBuyBtn(sender:UIButton){
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        print("sender.descriptiveDictionary :",sender.descriptiveDictionary ?? [:])
        dictPlans = sender.descriptiveDictionary
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_DETAILS_SEGUE, sender: self)
        UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_DIAGNOSTICS_B2B_USER)
    }
    func clickDeleteBtn(sender:UIButton){
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let alert = UIAlertController(title: "Cart", message: "Are you sure you want to delete this item?", preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            self.removeFromCart(packageID: sender.tag, completion: {(done) in
                if(done)
                {
                    let indexPath = IndexPath(row: sender.indexTag!, section: 0)
                    let cell = self.tableView.cellForRow(at: indexPath) as! DiagnosticsCartCell
                    let arrTemp : NSMutableArray =  App.cartArray.mutableCopy() as! NSMutableArray
                    App.cartArray.removeAllObjects()
                    for obj in arrTemp {
                        let dict :NSDictionary = obj as! NSDictionary
                        let strID = String(describing: dict.object(forKey: "package_id") ?? "0")
                        if(cell.btnDelete.tag != Int(strID)!)
                        {
                            App.cartArray.add(dict)
                        }
                        
                    }
                    self.tableView.reloadData()
                    self.didShowAlert(title: "Cart", message: "Item removed from cart")
                }
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    // MARK: - Web services
    
    override func getCartCollection() {
        print("*****")
        
        //self.linearBar.startAnimation()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_CART_COLLECTION
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        //self.linearBar.stopAnimation()
                        self.stopAnimating()
                    })
                    
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("get cart = \(resultJSON)")
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        //self.linearBar.stopAnimation()
                                        self.stopAnimating()
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            print("data=\(data.debugDescription)")
                            if code == 200 && status == "success"
                            {
                                let arrTemp : NSArray =  data.object(forKey: "cart_data") as! NSArray
                                //let arrTemp1 : NSArray =  data.object(forKey: "general") as! NSArray
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    App.cartArray = arrTemp.mutableCopy() as! NSMutableArray
                                    App.cartCount = data.object(forKey: "cart_count") as! Int
                                    App.cartAmount = data.object(forKey: "cart_amount") as! Float
                                    self.lblTotal.text = "Subtotal(\(String(describing: App.cartCount)) items): \(String(describing: App.cartAmount))"
                                    self.tableView.reloadData()
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                    
                                    print("*1*1*1*1")
                                })
                                
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                })
                                
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    @objc func removeFromCart(packageID:Int,completion: @escaping (_ done:Bool) -> ()) {
        
        //self.linearBar.startAnimation()
        
        let defaults = UserDefaults.standard
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            //self.startAnimating()
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_CART_REMOVE
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            
            request.httpMethod = HTTPMethods.DELETE
            
            let userData = "{\"\(Keys.KEY_PACKAGE_ID)\":\"\(packageID)\"}"
            
            request.httpBody = userData.data(using: String.Encoding.utf8)
        
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        print("Error==> : \(error.localizedDescription)")
                        completion(false)
                        //self.linearBar.stopAnimation()
                        self.stopAnimating()
                        self.didShowAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("remove from cart:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        completion(false)
                                        //self.linearBar.stopAnimation()
                                        self.stopAnimating()
                                        self.didShowAlert(title: "", message: message)
                                    })
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    App.cartCount = data.object(forKey: "updated_cart_count") as! Int
                                    App.cartAmount = data.object(forKey: "updated_cart_amount") as! Float
                                    self.lblTotal.text = "Subtotal(\(String(describing: App.cartCount)) items): \(String(describing: App.cartAmount))"
                                    completion(true)
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    completion(false)
                                    //self.linearBar.stopAnimation()
                                    self.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            completion(false)
                            //self.linearBar.stopAnimation()
                            self.stopAnimating()
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }

}

extension CartViewController: UITableViewDelegate ,UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiagnosticsCartCell", for: indexPath) as! DiagnosticsCartCell
        cell.delegate = self
        
        cell.mainBackground.layer.cornerRadius = 5
        cell.mainBackground.layer.masksToBounds = true
        
        cell.shadowLayer.setShadowView(opacity: 0.4, radius: 8)
        
        let dict = App.cartArray.object(at: indexPath.row) as! NSDictionary
        cell.lblName.text = "\(String(describing: dict.object(forKey: "package_name") ?? ""))\n\n\u{20B9}\(String(describing: dict.object(forKey: "price") ?? ""))"
        let strID = String(describing: dict.object(forKey: "package_id") ?? "0")
        let strAmount = String(describing: dict.object(forKey: "price") ?? "0")
        cartAmount = Double(Float(cartAmount) + Float(strAmount)!)
        cell.btnDelete.tag = Int(strID)!
        cell.btnDelete.indexTag = indexPath.row
        cell.btnBuy.descriptiveDictionary = dict
        cell.btnBuy.tag = indexPath.row
        cell.btnBuy.backgroundColor = Theme.buttonBackgroundColor
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //let cell = tableView.cellForRow(at: indexPath as IndexPath) as! DiagnosticsCartCell
        
        //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_DIAGNOSTICS_TEST_SEGUE, sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return App.cartArray.count
    }
    

}

protocol DiagnosticCartCellDelegate: class
{
    func clickBuyBtn(sender:UIButton)
    func clickDeleteBtn(sender:UIButton)
}

class DiagnosticsCartCell: UITableViewCell {
    
    @IBOutlet var mainBackground: UIView!
    @IBOutlet var vwList: UIView!
    @IBOutlet var imgTest: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnBuy: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var shadowLayer: UIView!
    @IBOutlet weak var heightOfLblTxtConstraint: NSLayoutConstraint!
    weak var delegate: DiagnosticCartCellDelegate? = nil
    
    
    
    @IBAction func clickBuyBtn(_ sender: UIButton) {
        print("selected")
        delegate?.clickBuyBtn(sender: sender)
    }
    
    @IBAction func clickDeleteBtn(_ sender: UIButton) {
        print("selected")
        delegate?.clickDeleteBtn(sender: sender)
    }
    
}

