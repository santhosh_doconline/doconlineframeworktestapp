//
//  ThankYouViewController.swift
//  DocOnline
//
//  Created by Doconline india on 27/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {
    
    @IBOutlet var mainBackground: UIView!
    @IBOutlet var shadowLayer: UIView!
    @IBOutlet var lblAppID: UILabel!
    @IBOutlet var lblPackageName: UILabel!
    @IBOutlet var lblDate: UILabel!
    var dictPlans : NSDictionary!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.setHidesBackButton(true, animated: false)
        shadowLayer.setShadowView(opacity: 0.4, radius: 8)
        
        mainBackground.layer.cornerRadius = 5
        mainBackground.layer.masksToBounds = true
        if(dictPlans == nil){
            lblDate.text = "      Date and Time              30th, Jan 2018, 10:30 AM"
            lblPackageName.text = "     Package Name              LIPID PROFILE TEST"
            lblAppID.text = "     Appointment ID            THY000987654321"
        }
        else{
        
            let strDate =  self.dateString(date:self.dateStringToDate(string: "\(String(describing: dictPlans.value(forKey: "apt_dt") ?? "2018-03-20 11:59:00"))", format: "yyyy-MM-dd HH:mm:ss"), format:"dd, MMM yyyy, HH:mm")
            
            lblDate.text = "     Date and Time              \(strDate)"
            lblPackageName.text = "     Package Name              \(dictPlans.value(forKey: "package_name") ?? "")"
            lblAppID.text = "     Appointment ID            \(dictPlans.value(forKey: "apt_id") ?? "")"
        }
        
        
    }
    
    @IBAction func ClickBtnHome(_ sender: UIButton) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func ClickBtnAppointment(_ sender: UIButton) {
        _ = self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "AppointmentView"), object: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
