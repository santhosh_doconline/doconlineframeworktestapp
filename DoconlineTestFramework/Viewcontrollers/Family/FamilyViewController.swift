//
//  FamilyViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 11/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import SwiftMessages

class FamilyViewController: UIViewController, NVActivityIndicatorViewable {

    
    @IBOutlet var lc_addMemberButtonHeight: NSLayoutConstraint! //44
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lb_status: UILabel!
    @IBOutlet var bt_addFamilyMem: UIButton!
    @IBOutlet var lb_messageOfConsentForm: UILabel!
    
    @IBOutlet var bt_consentFormCheckBox: UIButton!
    @IBOutlet var vw_consentForm: UIView!
    
    var members = [FamilyMembers]()
    
    var selectedMemDetails : FamilyMembers?
    var editStatus = false
    
    var isFamilyMemberAllowed = false
    var familyGuardianName = ""
    var isFromHomeViewToConsentForm = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isHidden = false
        self.bt_addFamilyMem.isHidden = true
        self.activityIndicator.startAnimating()
        self.vw_consentForm.isHidden = true
        
        if isFromHomeViewToConsentForm {
            self.isFamilyMemberAllowed = false
            self.checkIsFamilyMem(httpMethod: HTTPMethods.PATCH, completion: { (success) in
                if success {
                    DispatchQueue.main.async {
                        self.vw_consentForm.isHidden = false
                        self.tableView.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }
                }
            })
        }else {
            self.checkIsFamilyMem(httpMethod: HTTPMethods.GET) { (success) in
                if success {
                    DispatchQueue.main.async {
                        self.vw_consentForm.isHidden = false
                        self.tableView.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.vw_consentForm.isHidden = true
                    }
                    self.getFamilyMembers()
                }
            }
        }

        tableView.tableFooterView = UIView(frame: CGRect.zero)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFamilyMembers), name: NSNotification.Name(rawValue:"ReloadFamilymem"), object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapMessageLabel(_:)))
        self.lb_messageOfConsentForm.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_addFamilyMem.backgroundColor = Theme.buttonBackgroundColor
    }
    
    @objc func didTapMessageLabel(_ tapGestureRecognizer:UITapGestureRecognizer) {
         self.checkBoxTapped()
    }
    
    func checkBoxTapped() {
        if self.bt_consentFormCheckBox.currentImage ==  UIImage(named:"CheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor]){
            self.isFamilyMemberAllowed = false
            self.showConfirmationAlert()
        }else {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONSENT_FORM, sender: self)
        }
    }
    
    func checkCountNHideButton() {
        if self.members.count < App.appSettings?.userFamilyLimitInfo?.familyMembersAllowed ?? 0 {
            self.bt_addFamilyMem.isHidden = false
            self.lc_addMemberButtonHeight.constant = 44
        }else {
            self.bt_addFamilyMem.isHidden = true
            self.lc_addMemberButtonHeight.constant = 0
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func showConfirmationAlert() {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you don't want to allow \(self.familyGuardianName) to book a consultation on behalf of you.", preferredStyle: .alert)
        let formattedDate = NSMutableAttributedString()
        formattedDate.normalWithSystem("Are you sure you don't want to allow ").boldWithSystem(self.familyGuardianName).normalWithSystem(" to book a consultation on behalf of you.")
        alert.setValue(formattedDate, forKey: "attributedMessage")
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.checkIsFamilyMem(httpMethod: HTTPMethods.PATCH, completion: { (success) in
                if success {
                    DispatchQueue.main.async {
                        self.vw_consentForm.isHidden = false
                        self.tableView.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }
                }
            })
        }
        let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }

   
    @objc @IBAction func cancelTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func consentFormCheckBoxTapped(_ sender: UIButton) {
        self.checkBoxTapped()
    }
    
    func checkIsFamilyMem(httpMethod:String ,completion:@escaping (_ success:Bool) -> Void) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        self.tableView.isHidden = false
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        

        let url = URL(string: AppURLS.CONSENT_STATUS_FAMILY_MEM)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        
        request.httpMethod = httpMethod
        if httpMethod.isEqual(HTTPMethods.PATCH) {
            let dic = [Keys.KEY_BOOKING_CONSENT: self.isFamilyMemberAllowed]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                request.httpBody = jsonData
            }catch _{
                print("Error converting to json")
            }
        }
        session.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                completion(false)
            }
            
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                do
                {
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        print("Family mem consent status check:\(resultJSON)")
                        var isDataAvailable = false
                        if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary , let name = data.object(forKey: Keys.KEY_NAME) as? String ,let bookingConsent = data.object(forKey: Keys.KEY_BOOKING_CONSENT) as? Int{
                            self.familyGuardianName = name
                            self.isFamilyMemberAllowed = bookingConsent == 1 ? true : false
                            isDataAvailable = true
                        }else if (resultJSON.object(forKey: Keys.KEY_DATA) as? String) != nil {
                            isDataAvailable = true
                        }else {
                            isDataAvailable = false
                        }
                        
                        DispatchQueue.main.async {
                            let formattedConsentMessage = NSMutableAttributedString()
                            formattedConsentMessage.normal("Allow ").boldWithSystem("\(self.familyGuardianName.capitalizingFirstLetter())").normal(" to book a consultation on behalf of me")
                            self.lb_messageOfConsentForm.attributedText = formattedConsentMessage
                           
//                            self.lb_messageOfConsentForm.text = "Allow \(self.familyGuardianName) to book a consultation on behalf of me"
                            let checkedImg = UIImage(named:"CheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
                            let uncheckedImg = UIImage(named:"UncheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
                            self.bt_consentFormCheckBox.setImage( self.isFamilyMemberAllowed ? checkedImg : uncheckedImg , for: .normal)
                        }
                        
                        completion(isDataAvailable)
                    }
                }catch let err {
                    print("Error while checking status:\(err.localizedDescription)")
                    completion(false)
                }
            }
        }.resume()
    }
    
   @objc func getFamilyMembers() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
    
       DispatchQueue.main.async {
         self.tableView.isHidden = false
         self.activityIndicator.isHidden = false
         self.activityIndicator.startAnimating()
         self.vw_consentForm.isHidden = true
       }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let slotURL = AppURLS.URL_Family_mem
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.httpMethod = HTTPMethods.GET
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    //AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    self.tableView.isHidden = true
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if you response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling family mem:\(resultJSON)")
                        DispatchQueue.main.async(execute: {
                            self.didShowAlert(title: "", message: (error?.localizedDescription)!)
                           // AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                            self.tableView.isHidden = true
                            self.activityIndicator.stopAnimating()
                        })
                    }
                    else {
                        print("family mem response:\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        
                        if code == 200 && status == "success"
                        {
                            if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                self.members.removeAll()
                                
                                for mem in data {
                                    
                                    var id : Int?
                                    var email = ""
                                    var avatar_url = ""
                                    var full_name = ""
                                    var first_name = ""
                                    var dob = ""
                                    var gender = ""
                                    var user_id : Int?
                                    var family_id : Int?
                                    var created_at = ""
                                    var mobile_no = ""
                                    var mrn_no = ""
                                    var mobile_verified : Bool?
                                    var isVerified = false
                                    var isActive = false
                                    var statusDisplay = ""
                                    
                                    if let memID = mem.object(forKey: Keys.KEY_ID) as? Int {
                                        id = memID
                                    }
                                    
                                    if let emailid = mem.object(forKey: Keys.KEY_EMAIL) as? String {
                                        email = emailid
                                    }
                                    
                                    if let mobile = mem.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                        mobile_no = mobile
                                    }
                                    
                                    if let mrn = mem.object(forKey: Keys.KEY_MRN_NO) as? String {
                                        mrn_no = mrn
                                    }
                                    
                                    if let mobileVerified = mem.object(forKey: Keys.KEY_MOBILE_VERIFIED) as? Int {
                                        if mobileVerified == 0 {
                                           mobile_verified = false
                                        }else {
                                            mobile_verified = true
                                        }
                                    }
                                    
                                    if let avatr = mem.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                        avatar_url = avatr
                                    }
                                    
                                    if let fllname = mem.object(forKey: Keys.KEY_FULLNAME) as? String {
                                        full_name = fllname
                                    }
                                    
                                    if let frstname = mem.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                        first_name = frstname
                                    }
                                    
                                    if let dateofbirth = mem.object(forKey: Keys.KEY_DOB) as? String {
                                        dob = dateofbirth
                                    }
                                    
                                    if let memgener = mem.object(forKey: Keys.KEY_GENDER) as? String {
                                        gender = memgener
                                    }
                                    
                                    if let pivot = mem.object(forKey: Keys.KEY_PIVOT) as? NSDictionary {
                                        if let userid = pivot.object(forKey: Keys.KEY_USER_ID) as? Int {
                                            user_id = userid
                                        }
                                        
                                        if let familyid = pivot.object(forKey: Keys.KEY_FAMILY_ID) as? Int {
                                            family_id = familyid
                                        }
                                        
                                        if let created = pivot.object(forKey: Keys.KEY_CREATED_AT) as? String {
                                            created_at = created
                                        }
                                    }
                                    
                                    if let isverified = mem.object(forKey: Keys.KEY_IS_VERIFIED) as? Int {
                                        isVerified = isverified == 0 ? false : true
                                    }
                                    
                                    if let isactive = mem.object(forKey: Keys.KEY_IS_ACTIVE) as? Int {
                                        isActive = isactive == 0 ? false : true
                                    }
                                    
                                    if let statusdisplay = mem.object(forKey: Keys.KEY_STATUS_DISPLAY) as? String {
                                        statusDisplay = statusdisplay
                                    }
                                    
                                    
                                    
                                    let familyMem = FamilyMembers(id: id!,prefix:"" ,email: email, avatar_url: avatar_url, mrn_no: mrn_no, mobile_no: mobile_no, mobile_verified: mobile_verified!, full_name: full_name, first_name: first_name, dob: dob, gender: gender, familyid: family_id!, userid: user_id!, createdat: created_at,isVerified:isVerified,isActive:isActive,statusDisplay:statusDisplay)
                                    
                                    if let memDetails = mem.object(forKey: Keys.KEY_DETAILS) as? NSDictionary {
                                        var firstName = ""
                                        var lastName = ""
                                        var middleName = ""
                                        var fullName = ""
                                        var prefix = ""
                                        if let fname = memDetails.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                             firstName = fname
                                        }
                                        if let lname = memDetails.object(forKey: Keys.KEY_LAST_NAME) as? String {
                                            lastName = lname
                                        }
                                        if let mname = memDetails.object(forKey: Keys.KEY_MIDDLE_NAME) as? String {
                                            middleName = mname
                                        }
                                        if let fullname = memDetails.object(forKey: Keys.KEY_FULLNAME) as? String {
                                            fullName = fullname
                                        }
                                        if let fprefix = memDetails.object(forKey: Keys.KEY_PREFIX) as? String {
                                            prefix = fprefix
                                        }
                                        let userDetails = User()
                                        userDetails.first_name = firstName
                                        userDetails.last_name = lastName
                                        userDetails.middle_name = middleName
                                        userDetails.prefix = prefix
                                        familyMem.userDetails = userDetails
                                    }
                                    
                                    self.members.append(familyMem)
                                }
                            }
                        }
                        
                        DispatchQueue.main.async(execute: {
                            self.vw_consentForm.isHidden = true
                            if self.members.count == 0 {
                                self.tableView.isHidden = true
                                self.bt_addFamilyMem.setTitle("+Add A Family Member", for: .normal)
                            }else {
                                self.tableView.isHidden = false
                                self.bt_addFamilyMem.setTitle("+Add Other Family Member", for: .normal)
                            }
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                            self.tableView.reloadData()
                            
                            self.checkCountNHideButton()
                             if let appSettings = App.appSettings , let famliyInfo = appSettings.userFamilyLimitInfo , let isEnabled = famliyInfo.isMemberEnabled {
                                if !isEnabled {
                                     self.showStatusLine(color: .red, message: famliyInfo.message ?? "You cannot add family members")
                                }
                            }
                        })
                    }
                    
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        self.tableView.isHidden = true
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                        self.lb_status.text = "No family members found"
                        self.vw_consentForm.isHidden = true
                    })

                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    /**
     Method shows status line message when view loads
     */
    func showStatusLine(color:UIColor,message:String) {
        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
        status.backgroundView.backgroundColor = color
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: message)
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.presentationContext = .window(windowLevel: .statusBar)
        SwiftMessages.show(config: statusConfig, view: status)
    }
    
    func performNetworkCall(URLstring:String , HttpMethod : String,completionHandler : @escaping (_ success:Bool) -> Void) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        //startAnimating()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        self.activityIndicator.startAnimating()
        
        let url = URL(string: URLstring)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HttpMethod
        session.dataTask(with: request) { (data, response, error) in
            if error != nil
            {
                completionHandler(false)
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stopAnimating()
                })
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    
                    if httpResponse.statusCode == 204 {
                        completionHandler(true)
                    }else {
                        do {
                            if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                                let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: nil)
                                if !errorStatus {
                                    completionHandler(false)
                                    print("performing error handling notifications perform network call :\(jsonData)")
                                    
                                    if let message = jsonData.object(forKey: Keys.KEY_MESSAGE) as? String {
                                        DispatchQueue.main.async {
                                            print("Delete failed:=>\(message)")
                                            self.didShowAlert(title: "", message: message)
                                        }
                                    }
                                }
                            }
                            
                        }catch let error {
                            print("Recieved not a well formatted json :\(error.localizedDescription)")
                        }
                    }
                }
                
                DispatchQueue.main.async(execute: {
                     self.tableView.reloadData()
                     self.setupView()
                })
            }
            }.resume()
        
    }

    
    func setupView() {
        if self.members.count == 0 {
            self.tableView.isHidden = true
            self.lb_status.text = "No family members!"
        }else {
            self.tableView.isHidden = false
        }
    }

    
    @objc func editFamilyMemTapped(sender:UIButton) {
        print("button tag : \(sender.tag)")
        let memberDetails = self.members[sender.tag]
        self.selectedMemDetails = memberDetails
        self.editStatus = true
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_FAMILY_MEM, sender: self)
    }
    
    
    @IBAction func addNewMemTapped(_ sender: UIButton) {
        if App.appSettings?.userFamilyLimitInfo?.familyMembersAllowed ?? 0 == 0 {
            self.didShowAlert(title: "", message: App.appSettings?.userFamilyLimitInfo?.message ?? "You cannot add family members.")
            return
        }
        
        if members.count < App.appSettings?.userFamilyLimitInfo?.familyMembersAllowed ?? 0 {
            self.editStatus = false
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_FAMILY_MEM, sender: self)
        }else {
             self.didShowAlert(title: "SORRY", message: "You have reached maximum limit of adding family members")
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_ADD_FAMILY_MEM {
            let destVC = segue.destination as! FamilyMemDetailsViewController
            destVC.familyMem = selectedMemDetails
            destVC.editStatus = editStatus
        }else  if segue.identifier == StoryboardSegueIDS.ID_CONSENT_FORM {
            if let destVC = segue.destination as? FamilyMemberConsentFormViewController {
                destVC.delegate = self
            }
        }
        
    }
}

extension FamilyViewController : ConsentFormDelegate {
    func didUserAcceptedPolicyOnFollowUp(accept: Bool, appointmentId: String, followUpReason: String) {
        
    }
    
    func didUserAcceptPolicy(accept: Bool) {
        self.navigationController?.popViewController(animated: true)
        if accept {
            self.isFamilyMemberAllowed = true
            self.checkIsFamilyMem(httpMethod: HTTPMethods.PATCH, completion: { (success) in
                if success {
                    DispatchQueue.main.async {
                        self.vw_consentForm.isHidden = false
                        self.tableView.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }
                }
            })
        }
    }
}

extension FamilyViewController : FamilyTableViewCellDelegate {
    func didTapRequestLink(_ sender: UIButton) {
        let member = self.members[sender.tag]
        if (member.email ?? "").isEmpty && (member.mobile_no ?? "").isEmpty {
            self.didShowAlert(title: "", message: "Mobile Number/Email ID of your family member is required to send activation link")
            return
        }
        
        self.startAnimating()
        NetworkCall.performGet(url: AppURLS.REQUEST_ACTIVATION_LINK_FAMILY_MEMBER + "\(member.id ?? 0)") { (success, response, statusCode, error) in
            DispatchQueue.main.async {
                print("response")
                self.stopAnimating()
                self.tableView.reloadData()
                if let code = statusCode , code == 200 || code == 201 {
                     self.didShowAlert(title: "", message: "Activation information is sent to your family member Mobile Number/Email ID. Please ensure your Family member activates his/her account in order to use DocOnline’s services.")
                }else {
                    _ = statusCode == nil ? false : self.check_Status_Code(statusCode: statusCode! , data: nil)
                    if let err = error {
                        print("Error while getting subscription details:\(err.localizedDescription)")
                    }
                    
                    if let resp = response , let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String{
                        self.didShowAlert(title: "", message: message)
                    }
                }
            }
        }
    }
}

extension FamilyViewController : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.members.count > 0
        {
            return self.members.count
        }
        
        let rect = CGRect(x: 0,y: 0,width: self.tableView.bounds.size.width,height: self.tableView.bounds.size.height)
        
        let noDataLabel: UILabel = UILabel(frame: rect)
        
        noDataLabel.text = "No family members"
        noDataLabel.textColor = UIColor.gray
        noDataLabel.textAlignment = NSTextAlignment.center

        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let member = self.members[indexPath.row]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.date(from: member.dob!)
        let age = calculate_age(date: dateString!)
        let isActiveAndAgeAbove16 = !(member.isActive ?? true) && age >= 16 && !((member.email ?? "").isEmpty && (member.mobile_no ?? "").isEmpty)
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: isActiveAndAgeAbove16 ? TV_CV_CELL_IDENTIFIERS.FAMILY_ACTIVATION : TV_CV_CELL_IDENTIFIERS.FAMILY, for: indexPath) as! FamilyTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.FAMILY_ACTIVATION, for: indexPath) as! FamilyTableViewCell
        
        if cell.bt_requestLink != nil {
            cell.bt_requestLink.tag = indexPath.row
            print("set tag gor button:\(indexPath.row)")
            if(member.isActive! || age < 16)
            {
                cell.bt_requestLink.setTitle("Activated", for: .normal)
            }
            else
            {
                cell.cellDelegate = self
            }
        }
        
        cell.bt_edit_tapped.tag = indexPath.row
        
        cell.lb_familyMemName.text = member.full_name!
        print("date of birth:\(member.dob!)")
        cell.lb_date_of_birth.text = self.getFormattedDateOnly(dateString: member.dob!)
        cell.lb_gender.text = member.gender! + ")"
    
        cell.bt_edit_tapped.isHidden = member.isActive ?? true
        
        
        if (member.mobile_no ?? "").isEmpty {
            cell.lb_mobileNo.isHidden = true
            cell.iv_mobileIconImage.isHidden = true
            cell.lc_mobileIconHeight.constant = 0
            cell.contentView.layoutIfNeeded()
        }else {
            cell.lb_mobileNo.isHidden = false
            cell.iv_mobileIconImage.isHidden = false
            cell.lc_mobileIconHeight.constant = 20
            cell.lb_mobileNo.text = member.mobile_no
            cell.contentView.layoutIfNeeded()
        }
        
        if member.email!.isEmpty == true {
             cell.lb_email.text = "N/A"
        }else {
            cell.lb_email.text = member.email!
        }

        let url = URL(string: member.avatar_url!)
        cell.iv_profile.kf.setImage(with: url)
      
//        if age >= 16 {
//            print("Greated than age 16")
//            cell.bt_edit_tapped.isHidden = true
//        }else {
//            cell.bt_edit_tapped.isHidden = false
//        }
        
        cell.bt_edit_tapped.addTarget(self, action: #selector(FamilyViewController.editFamilyMemTapped(sender:)), for: UIControl.Event.touchUpInside)
        
        if age == 0 {
         // cell.lb_age.text = "(Age: below 1 year,"
          cell.lb_date_of_birth.text = self.getFormattedDateOnly(dateString: member.dob!) + "(Age: below 1 year," + member.gender! + ")"
        }else if age == 1 && age < 2 {
         // cell.lb_age.text = "(Age: 1,"
          cell.lb_date_of_birth.text = self.getFormattedDateOnly(dateString: member.dob!) + "(Age: 1,"  + member.gender! + ")"
        }else if age > 1 {
          // cell.lb_age.text = "(Age: \(age),"
           cell.lb_date_of_birth.text = self.getFormattedDateOnly(dateString: member.dob!) + "(Age: \(age)," + member.gender! + ")"
        }

        cell.iv_profile.layer.cornerRadius = cell.iv_profile.frame.size.width / 2
        cell.iv_profile.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            let member = self.members[indexPath.row]
            let urlstring = "\(AppURLS.URL_Family_mem)/\(member.id!)"
            print("delete url=\(urlstring) index:\(indexPath.row)***")
            self.performNetworkCall(URLstring: urlstring, HttpMethod: HTTPMethods.DELETE ,completionHandler:  { (success) in
                if success {
                    self.members.remove(at: indexPath.row)

                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stopAnimating()
                        let indexpath = IndexPath(item: indexPath.row, section: 0)
                        self.tableView.deleteRows(at: [indexpath], with: UITableView.RowAnimation.left)
                        self.tableView.reloadData()
                        self.didShowAlert(title: "", message: "Successfully deleted!")
                        
                        App.appSettings?.userFamilyLimitInfo?.familyMemberCount = (App.appSettings?.userFamilyLimitInfo?.familyMemberCount ?? 0) - 1
                        if let settings = App.appSettings {
                            AppSettings.encodAppSettingsDataAndSaveInUserDefaults(appSettings: settings)
                        }
                        
                        if self.members.count == 0 { //Add a family member
                            self.bt_addFamilyMem.setTitle("+Add a family member", for: .normal)
                        }else {
                            self.bt_addFamilyMem.setTitle("+Add other family member", for: .normal)
                        }
            
                        self.checkCountNHideButton()
                    })
                }else {
                    print("family mem delete failed")
                }
                
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                })
            })
        }
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 123
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension UIViewController {
    func checkssIsFamilyMem(httpMethod:String,fname:String,isAllowed:Bool ,completion:@escaping (_ success:Bool,_ guardianName:String,_ isAllowed:Bool) -> Void) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let url = URL(string: AppURLS.CONSENT_STATUS_FAMILY_MEM)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        
        request.httpMethod = httpMethod
        if httpMethod.isEqual(HTTPMethods.PATCH) {
            let dic = [Keys.KEY_BOOKING_CONSENT: isAllowed]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                request.httpBody = jsonData
            }catch _{
                print("Error converting to json")
                completion(false, fname, isAllowed)
            }
        }
        session.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
               completion(false, fname, isAllowed)
            }
            
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                do
                {
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        print("Family mem consent status check:\(resultJSON)")
                        var isDataAvailable = false
                        if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary , let name = data.object(forKey: Keys.KEY_NAME) as? String ,let bookingConsent = data.object(forKey: Keys.KEY_BOOKING_CONSENT) as? Int{
                            completion(true, name, bookingConsent == 1 ? true : false)
                        }else if (resultJSON.object(forKey: Keys.KEY_DATA) as? String) != nil {
                             completion(true, fname, isAllowed)
                        }else {
                            completion(false, fname, isAllowed)
                        }
                    }
                }catch let err {
                    print("Error while checking status:\(err.localizedDescription)")
                    completion(false, fname, isAllowed)
                }
            }
            }.resume()
    }
}



