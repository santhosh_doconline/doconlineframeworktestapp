//
//  FamilyMemDetailsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 11/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class FamilyMemDetailsViewController: UIViewController ,NVActivityIndicatorViewable{

    
    @IBOutlet var iv_profilePic: UIImageView!
    
    @IBOutlet var lb_emailNotRequiredStatus: UILabel!
    @IBOutlet var vw_gradient: UIView!
    @IBOutlet var iv_profileBackground: UIImageView!
    @IBOutlet weak var vw_firstname: UIView!
    @IBOutlet weak var vw_lastname: UIView!
    @IBOutlet weak var vw_email: UIView!
    @IBOutlet weak var vw_dob: UIView!
    @IBOutlet weak var vw_gender: UIView!
    @IBOutlet weak var vw_prefix: UIView!
    @IBOutlet weak var vw_middlename: UIView!
    
    @IBOutlet weak var lb_date_of_birth_message: UILabel!
    
    @IBOutlet weak var tf_middle_name: UITextField!
    @IBOutlet weak var tf_firstname: UITextField!
    @IBOutlet weak var tf_lastname: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_dob: UITextField!
    @IBOutlet weak var tf_gender: UITextField!
    @IBOutlet weak var tf_prefix: UITextField!
    @IBOutlet weak var tf_phoneNumber: UITextField!
    
    @IBOutlet weak var bt_add: UIBarButtonItem!
    
    @IBOutlet weak var iv_family_mem_pic: UIImageView!
    
    @IBOutlet weak var LC_email_view_height: NSLayoutConstraint!
    
    @IBOutlet weak var bt_save_family_member: UIButton!
    
    let picker = UIImagePickerController()
    var pickedImage : UIImage!
    
    var selectedPrefix = ""
    var selectedDateOfBirth = ""
    var memAge = 0
    var selectedGender = ""
    
    
    var familyMem : FamilyMembers?
    var editStatus = false

    
    func openCamera()
    {
        if !self.checkCameraPermission()
        {
            //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
            self.didShowAlertForDeniedPermissions( message: "Please provide access for the camera to upload pictures")
            return
        }

        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            self .present(picker, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    
    
    func openGallary()
    {
        if !self.checkLibraryPermission()
        {
            // self.didShowAlert(title: "Access denied!", message: "Please provide access to upload pictures from your Gallery")
            self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
            return
        }
        
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(picker, animated: true, completion: nil)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vwGrad = GradientView()
        vwGrad.frame =  CGRect(x: 0.0, y: 0.0, width: self.vw_gradient.frame.size.width, height: self.vw_gradient.frame.size.height)
        vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.vw_gradient.addSubview(vwGrad)
        
      //  self.navigationController?.navigationBar.shadowImage = UIImage()

//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.profile_pic_tapped(_:)))
//        self.iv_profilePic.addGestureRecognizer(tapGesture)
        
        
        self.iv_profilePic.layer.borderColor = UIColor.white.cgColor
        self.iv_profilePic.layer.borderWidth = 1
        self.iv_profilePic.layer.cornerRadius = self.iv_profilePic.frame.size.width / 2
        self.iv_profilePic.clipsToBounds = true

       self.shadowEffect(views: [vw_firstname,vw_middlename,vw_lastname,vw_dob,vw_email,vw_gender,vw_prefix])
        getNamePrefixes()
//        self.LC_email_view_height.constant = 0
//        self.vw_email.isHidden = true
  
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_save_family_member.backgroundColor = Theme.buttonBackgroundColor
        showEditDetails()
    }
    
    ///Shows the editing details on UI
    func showEditDetails() {
        if editStatus {
            if let family = self.familyMem {
                let name = family.full_name!
                let lstName = name.components(separatedBy: " ")
                
                if let userDetails = family.userDetails {
                    self.tf_prefix.text = userDetails.prefix
                    self.tf_firstname.text = userDetails.first_name
                    self.tf_lastname.text = userDetails.last_name
                    self.tf_middle_name.text = userDetails.middle_name
                    self.selectedPrefix = userDetails.prefix ?? "Mr."
                }
                
                self.selectedDateOfBirth = family.dob!
                self.tf_dob.text = self.getFormattedDateOnly(dateString: family.dob!)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let dateString = dateFormatter.date(from:  family.dob!)
                
                self.memAge = self.calculate_age(date: dateString!)
                print("Age is :\(self.memAge)")
                self.tf_email.text = family.email
                self.tf_gender.text = family.gender
                self.tf_phoneNumber.text = family.mobile_no
                self.selectedGender = family.gender ?? ""

                self.memAge = self.calculate_age(date: dateString!)
                if self.memAge < 16 {
                    self.lb_emailNotRequiredStatus.isHidden = false
                }else if self.memAge >= 16{
                    self.lb_emailNotRequiredStatus.isHidden = true
                }
            }
        }
    }
    
    
    
    @IBAction func genderTapped(_ sender: UIButton) {
        let actionView = UIAlertController(title: "Select", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let MRAction = UIAlertAction(title: "Male", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_gender.text = "Male"
            self.selectedGender = "Male"
        }
        let MSaction = UIAlertAction(title: "Female", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_gender.text = "Female"
            self.selectedGender = "Female"
        }
        
        let TSaction = UIAlertAction(title: "Transgender", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_gender.text = "Transgender"
            self.selectedGender = "Transgender"
        }
        
        let  performAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
        })
        
        actionView.addAction(performAction)
        actionView.addAction(MRAction)
        actionView.addAction(MSaction)
        actionView.addAction(TSaction)
        self.present(actionView, animated: true, completion: nil)
    }

    @IBAction func prefixTapped(_ sender: UIButton) {
        if App.namePrefixes == nil {
           print("No prefixes")
            App.namePrefixes = [
                "Mr.": "Mr.",
                "Mrs.": "Mrs.",
                "Dr.": "Dr.",
                "Prof.": "Prof.",
                "Miss": "Miss" ]
        }
        
        let actionView = UIAlertController(title: "Select", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        for (key,value) in App.namePrefixes {
            let  performAction = UIAlertAction(title: value as? String, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                self.tf_prefix.text = value as? String
                self.selectedPrefix = key as! String
            })
             actionView.addAction(performAction)
        }
        
       let performCancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            })
            
        actionView.addAction(performCancelAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func dateOfBirthTapped(_ sender: UIButton) {
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dateSelected = date {
                let dateSt = self.getDateString(date: dateSelected)
                let formStr = self.getDateFormat(date: date!)
                let seperatedDate = dateSt.components(separatedBy: " ")
                print("Date:(self.dateConverterUTCToCurrent(utcDate: dateSt ))")
                self.selectedDateOfBirth = seperatedDate[0]
                print("Date:\(self.selectedDateOfBirth)")
                self.tf_dob.text = self.getFormattedDate(dateString: formStr)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let dateString = dateFormatter.date(from: seperatedDate[0])
                
                self.memAge = self.calculate_age(date: dateString!)
                print("Age is :\(self.memAge)")
                
                let years = Calendar.current.dateComponents([.year], from: dateSelected, to: Date()).year
                print("Years :\(years)")
                
                if years! > 100 {
                    self.lb_date_of_birth_message.isHidden = false
                    self.selectedDateOfBirth = ""
                    self.tf_dob.text = ""
                }else {
                    self.lb_date_of_birth_message.isHidden = true
                }
                
                if self.memAge < 16 {
                    self.lb_emailNotRequiredStatus.isHidden = false
                }else if self.memAge >= 16{
                    self.lb_emailNotRequiredStatus.isHidden = true
                }
            }
        }
    }
   
    @IBAction func addDetailsTapped(_ sender: UIButton) {
        
        let prefix = self.tf_prefix.text! as String
        let firstName = self.tf_firstname.text! as String
        let middleName = self.tf_middle_name.text! as String
        let lastName = self.tf_lastname.text! as String
        let dob = self.tf_dob.text! as String
        let email = self.tf_email.text! as String
        let gender = self.tf_gender.text! as String
        let phoneNumber = self.tf_phoneNumber.text! as String
        
        if prefix.isEmpty == true
        {
            self.didShowAlert(title: "Info", message: "Prefix required!")
           // AlertView.sharedInsance.showInfo(title: "Info", message: "Prefix required!")
        }
        else if firstName.isEmpty == true
        {
            self.didShowAlert(title: "Info", message: "First Name required!")
           // AlertView.sharedInsance.showInfo(title: "Info", message: "Firstname required!")
        }else if firstName.count < 3
        {
             self.didShowAlert(title: "", message: "Minimum 3 characters required in Firstname")
           // AlertView.sharedInsance.showInfo(title: "Info", message: "Firstname should be minimum three characters")
        }
        else if lastName.isEmpty == true {
            self.didShowAlert(title: "Info", message: "Last Name required!")
        }
        else if dob.isEmpty == true {
            self.didShowAlert(title: "Info", message: "Date of birth required!")
        }else if phoneNumber.isEmpty && email.isEmpty && self.memAge >= 16{
            self.didShowAlert(title: "", message: "Either email or phone number required")
        }
//        else if self.memAge >= 16 && email.isEmpty == true {
//            self.didShowAlert(title: "Info", message: "Email required!")
//        }
        else if !email.isEmpty && !validateEmail(enteredEmail: email) {
            self.didShowAlert(title: "Info", message: AlertMessages.VALIDATE_EMAIL)
        }else if !phoneNumber.isEmpty && !validatePhoneNumber(enteredPhoneNumber: phoneNumber) {
            self.didShowAlert(title: "", message: "Please enter a valid phone number")
        }else if gender.isEmpty == true {
            self.didShowAlert(title: "Info", message: "Gender required!")
           // AlertView.sharedInsance.showInfo(title: "Info", message: "Gender required!")
        }else {
            
            var mobileNum = phoneNumber
            if let mobile = phoneNumber.convertStringToNumberalIfDifferentLanguage() {
                mobileNum = mobile.stringValue
               // print("Entered mobile numer:\(mobileNum)")
            }
            
            view.resignFirstResponder()
            let stringData = "\(Keys.KEY_PREFIX)=\(self.selectedPrefix)&\(Keys.KEY_FIRST_NAME)=\(firstName)&\(Keys.KEY_MIDDLE_NAME)=\(middleName)&\(Keys.KEY_LAST_NAME)=\(lastName)&\(Keys.KEY_DATE_OF_BIRTH)=\(self.selectedDateOfBirth)&\(Keys.KEY_EMAIL)=\(email)&\(Keys.KEY_MOBILE_NO)=\(mobileNum)&\(Keys.KEY_GENDER)=\(self.selectedGender)"
            
            if editStatus {
                print("Family mem data:\(stringData)")
                if let family = familyMem {
                    let urlString = AppURLS.URL_Family_mem + "/\(family.id!)"
                    startAnimating()
                    self.postFamilyData(urlString: urlString, httpMethod: HTTPMethods.PATCH, httpBody: stringData, tag: 2)
                 
                }else {
                    print("data modal is nil no member data")
                }
            }else {
                startAnimating()
                print("Family mem data:\(stringData)")
                let urlString = AppURLS.URL_Family_mem
                self.postFamilyData(urlString: urlString, httpMethod: HTTPMethods.POST, httpBody: stringData, tag: 1)
            }
        }
    }
    
    
    func postFamilyData(urlString:String,httpMethod:String,httpBody:String,tag:Int) {
        let urlstrng = urlString
        let url = URL(string: urlstrng)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API)
        request.httpMethod = httpMethod
        
        print("URL health==>\(urlString)")
        print("HTTPMethod==>\(httpMethod)")
        request.httpBody = httpBody.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            if let error = error {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                   // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data{
                print("data =\(data)")
            }
            if let response = response {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                //if you response is json do the following
                do{
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    
                  
                        if httpResponse.statusCode == 403 {
                            let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as! String
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                self.didShowAlert(title: "", message: message)
                            })
                        }else {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                            }
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                            if !errorStatus {
                                print("performing error handling:\(resultJSON)")
                            }
                            else {
                                print("==>response adding family: \(resultJSON)")
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    var message = ""
                                    var title = ""
                                    let email = self.tf_email.text! as String
//                                    if self.memAge >= 16 && !email.isEmpty{
//                                        title = tag == 1 ? "Successfully added" : "Successfully edited"
//                                        message = "Activation link is sent to your family member mail id"
//                                    }else if self.memAge < 16  && !email.isEmpty {
//                                        title = tag == 1 ? "Successfully added" : "Successfully edited"
//                                        message = "Activation link is sent to your family member mail id"
//                                    }else {
//                                        title = ""
//                                        message = tag == 1 ? "Successfully added" : "Successfully edited"
//                                    }
                                    
                                    title = tag == 1 ? "Successfully added" : "Successfully updated"
                                    
                                    if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                        
                                        if let isMinor = data.object(forKey: Keys.KEY_IS_MINOR) as? Int , isMinor == 0 {
                                            message = "Activation information is sent to your family member Mobile Number/Email ID. Please ensure your Family member activates his/her account in order to use DocOnline’s services."
                                        }
                                        
                                        if let notify = data.object(forKey: Keys.KEY_NOTIFY) as? Int , notify == 1 {
                                            message = "Activation information is sent to your family member Mobile Number/Email ID. Please ensure your Family member activates his/her account in order to use DocOnline’s services."
                                        }
                                    }
                                    
                                    
                                    if tag == 1
                                    {
                                        App.appSettings?.userFamilyLimitInfo?.familyMemberCount = (App.appSettings?.userFamilyLimitInfo?.familyMemberCount ?? 0) + 1
                                        if let settings = App.appSettings {
                                            AppSettings.encodAppSettingsDataAndSaveInUserDefaults(appSettings: settings)
                                        }
                                        
                                        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                                        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"ReloadFamilymem"), object: nil)
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                        
                                        alert.addAction(action)
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    }else if tag == 2{
                                        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                                        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"ReloadFamilymem"), object: nil)
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                        
                                        alert.addAction(action)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                })
                            }

                        }
                    
                }catch let error
                {
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON:\(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
   
   @objc func profile_pic_tapped(_ sender:AnyObject) {
        print("Profile pic taped")
        let alert:UIAlertController=UIAlertController(title: "Select", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }

    
    func uploadProfilePic() {
        let bound = generateBoundaryString()
        //startAnimating()
        var request = URLRequest(url: URL(string:  AppURLS.URL_UserAvatar )!)
        print("profileupdate url:==>\(AppURLS.URL_UserAvatar)")
        //  print("Registration Data:==>\(httpBody)")
        //  request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue("multipart/form-data; boundary=\(bound)", forHTTPHeaderField: "Content-Type")
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = createBodyWithImage(parameters: [self.pickedImage], boundary: bound) as Data
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                   // self.stopAnimating()
                   self.didShowAlert(title: "", message: error.localizedDescription)
                  //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
                print("Error==> : \(error.localizedDescription)")
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                //if you response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling upload profile pic :\(resultJSON)")
                    }
                    else {
                        print("response :=\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                        
                        if code == 200 && status == "success"
                        {
                            print("Successfully updated profile pic")
                            DispatchQueue.main.async(execute: {
                                App.avatarUrl = data.object(forKey: Keys.KEY_AVATAR_URL) as! String
//                                self.iv_profile_pic.image =  self.pickedImage
//                                self.stopAnimating()
                                self.didShowAlert(title: "Success", message: "updated profile pic")
                              //  AlertView.sharedInsance.showSuccessAlert(title: status, message: "updated profile pic")
                            })
                            //shpuld change the profile pic after uploaded
                        }
                        else
                        {
                            //have to hande error response
                            let errors = data.object(forKey: "errors") as! NSDictionary
                            print("Error***=>\(errors)")
                            DispatchQueue.main.async(execute: {
                                //self.stopAnimating()
                            })
                        }
                    }
                }catch let error{
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    //                    DispatchQueue.main.async(execute: {
                    //                        self.didShowAlert(title: "", message: error.localizedDescription)
                    //                    })
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
    func createBodyWithImage(parameters: [UIImage],boundary: String) -> NSData {
        let body = NSMutableData()
        
        if parameters.count != 0 {
            var i = 0;
            for image in parameters {
                let filename = "profile.jpeg"   //should change id to app id
                let data = image.jpegData(compressionQuality: 0.4);
                let mimetype = mimeTypeForPath(path: filename)
                let key = "avatar"
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                body.append(data!)
                body.appendString(string: "\r\n")
                i += 1;
            }
            
        }
        body.appendString(string: "--\(boundary)--\r\n")
        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
        return body
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FamilyMemDetailsViewController  : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        // self.iv_profilePic.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage //2
        self.dismiss(animated: true) {
            self.uploadProfilePic()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker canceled")
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension FamilyMemDetailsViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let charecterSet = CharacterSet.letters

        if textField == self.tf_firstname {
            var characterSet = CharacterSet.letters
            characterSet.insert(charactersIn: " ")
            let unwantedStr = string.trimmingCharacters(in: characterSet)
            return unwantedStr.count == 0
        }else if textField == self.tf_lastname {
            var characterSet = CharacterSet.letters
            characterSet.insert(charactersIn: " ")
            let unwantedStr = string.trimmingCharacters(in: characterSet)
            return unwantedStr.count == 0
        }else if textField == self.tf_middle_name {
            var characterSet = CharacterSet.letters
            characterSet.insert(charactersIn: " ")
            let unwantedStr = string.trimmingCharacters(in: characterSet)
            return unwantedStr.count == 0
        }else if textField == self.tf_phoneNumber {
            
            let characterSet =  CharacterSet(charactersIn: "*#,;+")
            let replacement = string.rangeOfCharacter(from: characterSet) == nil
            let newLength = (self.tf_phoneNumber.text?.count)! + string.count - range.length
            let resultingString = newLength <= 10
            
            return resultingString && replacement
            
        }
        
        return true
    }
    
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
