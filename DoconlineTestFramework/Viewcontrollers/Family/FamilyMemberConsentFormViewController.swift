//
//  FamilyMemberConsentFormViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 28/06/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit

class FamilyMemberConsentFormViewController: UIViewController {

    var delegate:ConsentFormDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func acceptTapped(_ sender: UIButton) {
        delegate?.didUserAcceptPolicy(accept: true)
    }
    
    @IBAction func declineTapped(_ sender: UIButton) {
        delegate?.didUserAcceptPolicy(accept: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
