//
//  VitalInfoImageViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 10/10/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class VitalInfoImageViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var bt_close: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.delegate = self
     //   self.webView.navigationDelegate = self
        
//        self.activityIndicator.startAnimating()
//        let url = URL(string: AppURLS.VITAL_INFO_IMAGE_URL)
//        let request = URLRequest(url: url!)
//        self.webView.load(request)
        
        self.imageView.kf.indicatorType = .activity
        self.imageView.kf.setImage(with: URL(string:App.appSettings?.vitalImageInfoUrl ?? ""), placeholder: UIImage(named:"DoconlineVitalInfo"), options: nil, progressBlock: { (start, end) in

        }) { (vitalInfoImage, error, cache, url) in
            self.imageView.image = vitalInfoImage
           // self.setScrollSize()
        }
        self.addDoubleTapToZoom()
    }
    
    func addDoubleTapToZoom() {
        let tapGeture = UITapGestureRecognizer(target: self, action: #selector(self.onDoubleTap(_:)))
        tapGeture.numberOfTapsRequired = 2
        self.scrollView.addGestureRecognizer(tapGeture)
    }
    
    @objc func onDoubleTap(_ tapGesture: UITapGestureRecognizer) {
        let scale = min(self.scrollView.zoomScale * 2, self.scrollView.maximumZoomScale)
        print("scale:\(scale)")
        if scale != scrollView.zoomScale {
            let point = tapGesture.location(in: self.imageView)
            self.scrollView.zoomToPoint(point: point, withScale: 5, animated: true)
        }else {
            let point = tapGesture.location(in: self.imageView)
            self.scrollView.zoomToPoint(point: point, withScale: 1, animated: true)
        }
    }
    
    
    func setScrollSize() {
        if let image = self.imageView.image {
            print("image width:\(image.size.width) height:\(image.size.height)")
            DispatchQueue.main.async {
                self.scrollView.contentSize = image.size
                self.imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            }
        }
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension VitalInfoImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}

extension UIScrollView {
    func zoomToPoint(point: CGPoint, withScale scale: CGFloat, animated: Bool) {
        var x, y, width, height: CGFloat
        
        //Normalize current content size back to content scale of 1.0f
        width = (self.contentSize.width / self.zoomScale)
        height = (self.contentSize.height / self.zoomScale)
        let contentSize = CGSize(width: width, height: height)
        
        //translate the zoom point to relative to the content rect
        x = (point.x / self.bounds.size.width) * contentSize.width
        y = (point.y / self.bounds.size.height) * contentSize.height
        let zoomPoint = CGPoint(x: x, y: y)
        
        //derive the size of the region to zoom to
        width = self.bounds.size.width / scale
        height = self.bounds.size.height / scale
        let zoomSize = CGSize(width: width, height: height)
        
        //offset the zoom rect so the actual zoom point is in the middle of the rectangle
        x = zoomPoint.x - zoomSize.width / 2.0
        y = zoomPoint.y - zoomSize.height / 2.0
        width = zoomSize.width
        height = zoomSize.height
        let zoomRect = CGRect(x: x, y: y, width: width, height: height)
        
        //apply the resize
        self.zoom(to: zoomRect, animated: animated)
    }
}
