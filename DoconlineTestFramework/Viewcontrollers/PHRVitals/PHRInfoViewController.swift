//
//  PHRInfoViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 24/08/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

//@IBDesignable class UnitsLabel: UILabel {
//
//    @IBInspectable var borderColor: UIColor = UIColor.lightGray
//    @IBInspectable var topInset: CGFloat = 4.0
//    @IBInspectable var bottomInset: CGFloat = 4.0
//    @IBInspectable var leftInset: CGFloat = 4.0
//    @IBInspectable var rightInset: CGFloat = 4.0
//    @IBInspectable var radius: CGFloat {
//        get {
//            return self.layer.cornerRadius
//        }set {
//            self.layer.cornerRadius = CGFloat(newValue)
//        }
//    }
//
//    override class var layerClass: AnyClass {
//        return CAGradientLayer.self
//    }
//
//    override func drawText(in rect: CGRect) {
//        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
//        super.drawText(in: rect.inset(by: insets))
//    }
//
//    override var intrinsicContentSize: CGSize {
//        let size = super.intrinsicContentSize
//        return CGSize(width: size.width + leftInset + rightInset,
//                      height: size.height + topInset + bottomInset)
//    }
//
//    func setup() {
//        layer.borderWidth = 0.5
//        layer.borderColor = borderColor.cgColor
//        clipsToBounds = true
//    }
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        setup()
//    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setup()
//    }
//    override func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        setup()
//    }
//}

protocol PHRInfoViewDelegate {
    func didDoneUpdatingDetails()
}

class PHRInfoViewController: UIViewController , NVActivityIndicatorViewable  {

    @IBOutlet var tableVIew: UITableView!
    
    @IBOutlet var bt_addOrEdit: UIButton!
    
    @IBOutlet var vw_back: UIView!
    @IBOutlet var lc_updateInfoHeight: NSLayoutConstraint!
    @IBOutlet var bt_vitalInfo: UIBarButtonItem!
    
    var dateLabel: UILabel!
    var navIconIV: UIImageView!
    var stackView: UIStackView!
    var bt_update: UIBarButtonItem!
    
    var isImageViewRotated = false
    var isInUpdateMode = false
    
    var vitalTemplate: VitalsTemplate?
    var selectedVitalRecord: VitalRecord?
    
    
    var recordDatesTableView: UITableView!
    var selectedRecordId = ""
    var isEditRecord = false
    var isNewRecord = false
    
    var delegate: PHRInfoViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableVIew.estimatedRowHeight = 70
        self.tableVIew.rowHeight = UITableView.automaticDimension
        
        self.vw_back.isHidden = true
        self.getVitalInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_addOrEdit.backgroundColor = Theme.buttonBackgroundColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.tableVIew.reloadData()
        }
    }
    
    func getVitalInfo() {
        self.startAnimating()
        VitalsTemplate.getVitalTemplateKeys(onCompletion: { (success, statusCode, response, vitalsTemplate)  in
            let isNoError = self.check_Status_Code(statusCode: statusCode, data: response)
            if isNoError {
                self.vitalTemplate = vitalsTemplate
                if let recordFields = self.vitalTemplate?.recordFields , let rootFields = self.vitalTemplate?.rootFileds {
                    self.vitalTemplate?.totalFields = rootFields + recordFields
                }
                
                if let selectedRecord = self.selectedVitalRecord  {
                   self.updateTemplateArrayWithValues(record: selectedRecord)
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
                self.tableVIew.reloadData()
                self.showUpdateInfoButton()
                self.vitalTemplate?.totalFields.forEach{ print("dghbsbg:\($0.fields?.count)") }
            }
        })
    }
    
    func updateTemplateArrayWithValues(record:VitalRecord) {
        for field in self.vitalTemplate?.totalFields ?? [] {
            if (field.key ?? "").isEqual(UserVitalInfo.SERILIZATION_KEY.RECORDED_AT) {
                field.fieldValue = record.recordedAt
            }else {
                let vital = record.recordValues?.filter{ ($0.type ?? "").isEqual(field.key ?? "") }.first
                
                if let value = vital?.fieldValue {
                  field.fieldValue =  value
                }
                
                if let value = vital?.valueObj {
                    field.valueObj =  value
                    var fields = [VitalTemplateFields]()
                    
                    field.fields?.forEach{
                        if ($0.key ?? "").contains("mm") {
                            $0.fieldValue = value.mm
                        }
                        
                        if ($0.key ?? "").contains("hg") {
                            $0.fieldValue = value.hg
                        }
                        fields.append($0)
                    }
                    field.fields = fields
                    print("dfgihwiuf\(field.fields?.first?.fieldValue ?? 0)")
                }
            }
            print("Field template\(field.dicRepresentation())  fndsfsf:\(field.valueObj?.hg ?? 0)\n")
        }
    }
    
    func showUpdateInfoButton() {
        //self.bt_update = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(self.editTapped(_:)))
        
        if self.selectedVitalRecord != nil && !self.isNewRecord && !self.isEditRecord {
             self.navigationItem.rightBarButtonItems = [self.bt_vitalInfo]//,self.bt_update]
            self.navigationItem.title  = "Vital Details"
            self.lc_updateInfoHeight.constant = 0
            self.bt_addOrEdit.isHidden = true
        }else if self.selectedVitalRecord == nil && self.isNewRecord && !self.isEditRecord {
            self.navigationItem.rightBarButtonItems = [self.bt_vitalInfo]
            self.navigationItem.title  = "Add New Vital"
            self.bt_addOrEdit.setTitle("Add", for: .normal)
            self.lc_updateInfoHeight.constant = 45
            self.bt_addOrEdit.isHidden = false
        }else if self.selectedVitalRecord != nil && !self.isNewRecord && self.isEditRecord {
            self.navigationItem.rightBarButtonItems = [self.bt_vitalInfo] 
            self.navigationItem.title  = "Edit Vital"
            self.bt_addOrEdit.setTitle("Update", for: .normal)
            self.lc_updateInfoHeight.constant = 45
            self.bt_addOrEdit.isHidden = false
        }

        UIView.animate(withDuration: 0.5) {
            self.tableVIew.reloadData()
            self.view.layoutIfNeeded()
        }
        self.vw_back.isHidden = (self.vitalTemplate?.totalFields ?? []).isEmpty
    }
    


    /// Edit action to make editable values list
    ///
    /// - Parameter sender: UIBarButtonItem
    @objc func editTapped(_ sender: UIBarButtonItem) {
        self.isEditRecord = true
        self.isNewRecord = false
        self.showUpdateInfoButton()
    }
    
    @objc func cancelTapped(_ sender: UIBarButtonItem) {
        self.showUpdateInfoButton()
        self.vw_back.isHidden = (self.vitalTemplate?.userVitalInfo?.latestVital?.vitals ?? []).isEmpty  || (self.vitalTemplate?.totalFields ?? []).isEmpty
    }
    
    @IBAction func updateValuesEdited(_ sender: UIButton) {
        let bloodFieldCheck = self.isBloodPressureFieldsEmpty()
        if !self.isDateSelected() {
            self.didShowAlert(title: "", message: "Please select record date")
        }else if bloodFieldCheck.isOnlyOneZero {
            self.didShowAlert(title: "", message: "Please enter values in both mm and Hg fields")
        }else if self.isAllFieldsEmpty() && bloodFieldCheck.isBothZero {
             self.didShowAlert(title: "", message: "Please fill atleast one field")
        } else {
            self.updateRecord(method: self.isNewRecord ? HTTPMethods.POST : HTTPMethods.PUT)
        }
    }
    
    func isDateSelected() -> Bool {
        let date = self.vitalTemplate?.totalFields.filter{ ($0.unit ?? "").lowercased() == "date" }.first
                
        return date?.fieldValue ?? 0 != 0
    }
    
    ///checking all fields are empty or not except date of record also type for checking the mmhg fields to show diffre
    ///
    /// - Returns: boolean and message to display
    func isAllFieldsEmpty() -> Bool{
        var isEmpty = true
        if let cells = self.tableVIew.visibleCells as? [PHInfoTableViewCell] {
            for cell in cells {
                if cell.tf_value != nil,cell.lb_units != nil, let unit = cell.lb_units.text , !unit.lowercased().isEqual("date")
                {
                    if unit.lowercased().isEqual("mmhg") {
                        isEmpty = self.isBloodPressureFieldsEmpty().isBothZero
                    }else {
                        if !cell.tf_value.text!.isEmpty {
                            isEmpty = false
                        }
                    }
                }
            }
        }
        return isEmpty
    }
    
    func isBloodPressureFieldsEmpty() -> (isBothZero:Bool,isOnlyOneZero:Bool){
        var mm: Double = 0
        var hg: Double = 0
        
        for field in self.vitalTemplate?.totalFields ?? [] {
            
            let mmfield = field.fields?.filter{ ($0.unit ?? "").lowercased() == "mm"}.first
            if let mmvalue = mmfield{
                mm = mmvalue.fieldValue ?? 0
            }
            
            let hgfield = field.fields?.filter{ ($0.unit ?? "").lowercased() == "hg"}.first
            if let mmvalue = hgfield{
                hg = mmvalue.fieldValue ?? 0
            }
        }
        print("mmdgbd:\(mm) && hg:\(hg)")
        let isAnyOneZero = (mm > 0 && hg == 0) || (mm == 0 && hg > 0)
        return ((mm == 0 && hg == 0),isAnyOneZero)  //mm == 0 && hg == 0 ||
    }
    
    /// performs update record for edit or new record
    ///
    /// - Parameter method: request type `HTTPMethod`
    func updateRecord(method:String) {
        self.startAnimating()
        self.vitalTemplate?.updateRecord(with: self.selectedRecordId, method: method, onCompletion: { (success, message,statusCode,response)  in
        
            let isNoError = self.check_Status_Code(statusCode: statusCode, data: response)
            if isNoError {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: isNoError ? "Success" : "", message: message, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        if success {
                            self.isEditRecord = !success
                            self.isNewRecord = !success
                            self.showUpdateInfoButton()
                            self.delegate?.didDoneUpdatingDetails()
                        }
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            DispatchQueue.main.async {
                self.stopAnimating()
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension PHRInfoViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension PHRInfoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.vitalTemplate?.totalFields.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fields = self.vitalTemplate?.totalFields ?? []
        var cellIdentifier = "VitalInfo"
        if !fields.isEmpty {
            let isInnerFieldEmpty = (fields[indexPath.row].fields ?? []).isEmpty
            if (fields[indexPath.row].unit ?? "").lowercased().isEqual("date") && isInnerFieldEmpty && (self.isNewRecord || self.isEditRecord) {
                cellIdentifier = "UpdateVitalDate"
            }else if !(fields[indexPath.row].unit ?? "").lowercased().isEqual("date") && isInnerFieldEmpty  && (self.isNewRecord || self.isEditRecord) {
                cellIdentifier = "UpdateVitalInfo"
            }else if !isInnerFieldEmpty && (self.isNewRecord || self.isEditRecord) {
                cellIdentifier = "VitalFieldsCV" //VitalFields  //VitalFieldsCV
            }else {
                cellIdentifier = "VitalInfo"
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PHInfoTableViewCell
        cell.selectionStyle = .none
        
        if let fields = self.vitalTemplate {
            cell.setUpValue(vital: fields.totalFields[indexPath.row] )
            
            if cellIdentifier.isEqual("UpdateVitalInfo") && cell.tf_value != nil {
                cell.tf_value.tag = indexPath.row
                cell.tf_value.addTarget(self, action: #selector(self.fieldStateChanged(_:)), for: .editingChanged)
            }
            
            if cellIdentifier.isEqual("UpdateVitalDate") && cell.bt_date != nil {
                cell.bt_date.tag = indexPath.row
                cell.dateDelegate = self
            }
            
            if cellIdentifier.isEqual("VitalFieldsCV") && cell.collectionView != nil {
                cell.collectionView.tag = indexPath.row
                cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
                cell.collectionView.reloadData()
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     //   guard let tableViewCell = cell as? PHInfoTableViewCell , tableViewCell.collectionView != nil else { return }
       // tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
       // UIView.animate(withDuration: 0.5) {
       //     self.view.layoutIfNeeded()
       // }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.isEditRecord || self.isNewRecord ? 70 : 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // if let cell = tableView.cellForRow(at: indexPath) as? PHInfoTableViewCell , cell.collectionView != nil{
          //  return UITableViewAutomaticDimension
       // }
        return self.isEditRecord || self.isNewRecord ? UITableView.automaticDimension : 50
    }
    
    @objc func fieldStateChanged(_ sender: UITextField) {
        self.vitalTemplate?.totalFields[sender.tag].fieldValue = Double(sender.text ?? "")
        print("Textfield tag:\(sender.tag) value:\(self.vitalTemplate?.totalFields[sender.tag].fieldValue ?? 0)")
    }
}

extension PHRInfoViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.vitalTemplate?.totalFields ?? []).indices.contains(collectionView.tag) ? self.vitalTemplate?.totalFields[collectionView.tag].fields?.count ?? 0  : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FieldItem", for: indexPath) as! PHInfoTableCollectionViewCell
        
        if let fields = self.vitalTemplate?.totalFields[collectionView.tag].fields {
            cell.tf_value.tag = indexPath.row
            cell.tf_value.addTarget(self, action: #selector(self.fieldsValueChanged(_:)), for: .editingChanged)
            print("dfjbsdbf:\(fields[indexPath.row].fieldValue ?? 0) dnsdn:\(fields[indexPath.row].key ?? "")")
            cell.setUpValue(vital: fields[indexPath.row])
        }
        return cell
    }
    
    @objc func fieldsValueChanged(_ sender: UITextField) {
        if let fieldSuperView = sender.superview ,let cell = fieldSuperView.superview as? PHInfoTableCollectionViewCell , let collectionView = cell.superview as? UICollectionView ,let row = collectionView.indexPath(for:cell)?.row {
            self.vitalTemplate?.totalFields[collectionView.tag].fields?[row].fieldValue = Double(sender.text ?? "")
            print("Textfield tag:\(sender.tag) value:\( self.vitalTemplate?.totalFields[collectionView.tag].fields?[row].fieldValue ?? 0) colelctionviewtag:\(collectionView.tag)")
        }
    }
}

extension PHRInfoViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width * 0.43 , height:40)
    }
}

extension PHRInfoViewController: PHRInfoTVCellDelegate {
    func didTapSelectDate(at: Int) {
        var minimumDate: Date!
       
        if let gregorian = NSCalendar(calendarIdentifier:.gregorian)
        {
            if let minDate = gregorian.date(byAdding: .year, value: -100, to: Date())
            {
                minimumDate = minDate
            }
        }
        
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate:minimumDate, maximumDate: Date() , datePickerMode: .dateAndTime) {
            (date) -> Void in
            
            if let dateSelected = date {
                guard let cell = self.tableVIew.cellForRow(at: IndexPath(row: at, section: 0)) as? PHInfoTableViewCell else { return }
                
                if cell.bt_date != nil {
                    let displayTime = Double(dateSelected.timeIntervalSince1970).getDateStringFromTimeStamp()
                    cell.bt_date.setTitle(displayTime , for: .normal)
                    self.vitalTemplate?.totalFields[at].fieldValue = Double(dateSelected.timeIntervalSince1970)
                }
            }
        }
    }
    
    func didChangeText(at: Int, text: String) {
        
    }
}


protocol PHRInfoTVCellDelegate : class {
    func didTapSelectDate(at:Int)
    func didChangeText(at:Int,text:String)
}

class PHInfoTableViewCell: UITableViewCell {
    @IBOutlet var typeName: UILabel!
    @IBOutlet var value: UILabel!
    
    //edit view
    @IBOutlet var tf_value: UITextField!
    @IBOutlet var lb_units: UnitsLabel!
    @IBOutlet var lc_unitsWidth: NSLayoutConstraint!
    
    @IBOutlet var tf_min: UITextField!
    @IBOutlet var tf_max: UITextField!
    
    
    @IBOutlet var bt_date: UIButton!
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lc_collectionViewHeight: NSLayoutConstraint!
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.reloadData()
        self.lc_collectionViewHeight.constant = 40
       // self.lc_collectionViewHeight.constant = 40 + self.collectionView.contentSize.height
        UIView.animate(withDuration: 0.5) {
           self.contentView.layoutIfNeeded()
        }
        collectionView.reloadData()
    }
    
    weak var dateDelegate: PHRInfoTVCellDelegate?
    
    func setUpValue(vital:VitalTemplateFields) {
        self.typeName.text = vital.label
        var formatedValue = ""
      //  let decimalPoint = (vital.key ?? "").isEqual(UserVitalInfo.SERILIZATION_KEY.HEIGHT) || (vital.key ?? "").isEqual(UserVitalInfo.SERILIZATION_KEY.WEIGHT) ? 1 : 0
        if let value = vital.fieldValue {
          formatedValue = String(format: "%.2f",value)
        }
        if self.value != nil {
            self.value.text = (vital.unit ?? "").lowercased().isEqual("date") ? "\(vital.fieldValue?.getDateStringFromTimeStamp() ?? "")" : "\(formatedValue) \(vital.unit ?? "")"
            if let obj = vital.valueObj , let mm = obj.mm , let hg = obj.hg , !(vital.unit ?? "").lowercased().isEqual("date"){
                self.value.text = "\(mm)/\(hg) \(vital.unit ?? "")"
            }
        }
        
        if self.tf_value != nil , self.lb_units != nil ,self.lc_unitsWidth != nil {
            self.tf_value.text = "\(formatedValue)"
            self.tf_value.delegate = self
            if vital.unit == nil {
                self.lb_units.text = ""
                self.lb_units.isHidden = true
                self.lc_unitsWidth.constant = 0
            }else {
                self.lb_units.text = vital.unit
                self.lb_units.isHidden = false
            }
        }
        
        if self.tf_min != nil, self.tf_max != nil, let obj = vital.valueObj , let mm = obj.mm , let hg = obj.hg , self.lb_units != nil ,self.lc_unitsWidth != nil{
            
             self.tf_min.text = String(format: "%.2f",mm)
             self.tf_max.text = String(format: "%.2f",hg)
            self.tf_max.delegate = self
            self.tf_min.delegate = self
            
            
            if vital.unit == nil {
                self.lb_units.text = ""
                self.lb_units.isHidden = true
                self.lc_unitsWidth.constant = 0
            }else {
                self.lb_units.text = vital.unit
                self.lb_units.isHidden = false
            }
        }
        
        if self.bt_date != nil {
            self.bt_date.setTitle(vital.fieldValue?.getDateStringFromTimeStamp() ?? "", for: .normal)
        }
    }
    
    @IBAction func selectDateTapped(_ sender: UIButton) {
        print("Button date tag:\(sender.tag)")
        self.dateDelegate?.didTapSelectDate(at: sender.tag)
    }
}

extension PHInfoTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxNoOfDigitsBeforeDecimal = 3;
        
        let maxNoOfDigitsAfterDecimal = 2;
        
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        let seperatedString = newString?.components(separatedBy: ".")
        if (seperatedString?.count)! == 1 && (newString?.count)! > maxNoOfDigitsBeforeDecimal {
            return false
        }
        
        if (seperatedString?.count)! > 2 {
            return false
        }
        
        if (seperatedString?.count)! >= maxNoOfDigitsAfterDecimal {
            let  sepStr = String(format: "%@", (seperatedString?[1])!)
            return !(sepStr.count > maxNoOfDigitsAfterDecimal);
        }
        
        return true
    }
}

class PHInfoTableCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var tf_value: UITextField!
    @IBOutlet var lb_units: UnitsLabel!
    @IBOutlet var lc_unitsWidth: NSLayoutConstraint!
    
    func setUpValue(vital:VitalTemplateFields) {
        if self.tf_value != nil , self.lb_units != nil ,self.lc_unitsWidth != nil  {
            if let mm = vital.fieldValue , (vital.unit ?? "").lowercased().isEqual("mm") {
                 self.tf_value.text = String(format: "%.2f",mm)
            }
            
            if let hg = vital.fieldValue , (vital.unit ?? "").lowercased().isEqual("hg"){
                 self.tf_value.text = String(format: "%.2f",hg)
            }
           
            self.tf_value.delegate = self
            if vital.unit == nil {
                self.lb_units.text = ""
                self.lb_units.isHidden = true
                self.lc_unitsWidth.constant = 0
            }else {
                self.lb_units.text = vital.unit
                self.lb_units.isHidden = false
            }
        }
    }
}
extension PHInfoTableCollectionViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxNoOfDigitsBeforeDecimal = 3;
        
        let maxNoOfDigitsAfterDecimal = 2;
        
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        let seperatedString = newString?.components(separatedBy: ".")
        if (seperatedString?.count)! == 1 && (newString?.count)! > maxNoOfDigitsBeforeDecimal {
            return false
        }
        
        if (seperatedString?.count)! > 2 {
            return false
        }
        
        if (seperatedString?.count)! >= maxNoOfDigitsAfterDecimal {
            let  sepStr = String(format: "%@", (seperatedString?[1])!)
            return !(sepStr.count > maxNoOfDigitsAfterDecimal);
        }
        
        return true
    }
}

