//
//  VitalRecordsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 28/08/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class VitalRecordsViewController: UIViewController , NVActivityIndicatorViewable{

    
    @IBOutlet var recordsTableView: UITableView!
    
    var vitalTemplate: VitalsTemplate?
    var vitalRecord: VitalPages?
    var records = [VitalRecord]()
    
    var cellIdentifier = "RecordDate"
    
    var selectedRecordId = ""
    var selectedVitalRecord: VitalRecord?
    var isEditRecord = false
    var isNewRecord = false
    
    ///linear bar instance reference
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLinearProgressBar()
        self.recordsTableView.tableFooterView = UIView(frame: .zero)
        //self.getListOfVitalDates(url: AppURLS.URL_GET_VITAL_RECORDS, tag: 0)
        self.getVitalValues(url: AppURLS.URL_GET_VITAL_RECORDS, tag: 0)
    }
    
    func getVitalValues(url:String,tag:Int) {
        self.linearBar.startAnimation()
        NetworkCall.performGet(url: url) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting template:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.linearBar.stopAnimation()
                    self.didShowAlert(title: "", message: err.localizedDescription)
                }
            }
            
            var serialisationDic = [String:Any]()
            
            if success {
                if let resp = response ,
                    let data = resp.object(forKey: Keys.KEY_DATA) as? [String: Any] {
                    if let currentPage = data[Keys.KEY_CURRENTPAGE] as? Int { serialisationDic[Keys.KEY_CURRENTPAGE] = currentPage }
                    if let nxtPage = data[Keys.KEY_NEXTPAGE] as? Int { serialisationDic[Keys.KEY_NEXTPAGE] = nxtPage }
                    if let perPage = data[Keys.KEY_PERPAGE] as? Int { serialisationDic[Keys.KEY_PERPAGE] = perPage }
                    if let prevPage = data[Keys.KEY_PREVPAGE] as? Int { serialisationDic[Keys.KEY_PREVPAGE] = prevPage }
                    if let total = data[Keys.KEY_TOTAL] as? Int { serialisationDic[Keys.KEY_TOTAL] = total }
                    
                    if let dataArray = data[Keys.KEY_DATA] as? [[String:Any]] {
                        var dataDicArray = [[String:Any]]()
                        for fieldData in dataArray {
                            var sinngleData = [String:Any]()
                            if let id = fieldData["_id"] as? String { sinngleData["_id"] = id }
                            if let createdAt = fieldData["created_at"] as? String { sinngleData["created_at"] = createdAt }
                            if let profileType = fieldData["profile_type"] as? String { sinngleData["profile_type"] = profileType }
                            if let reocrdedAt = fieldData["recorded_at"] as? Double { sinngleData["recorded_at"] = reocrdedAt }
                            if let userID = fieldData["user_id"] as? Int { sinngleData["user_id"] = userID }
                            
                            var recordData = [[String:Any]]()
                            if let records = fieldData["records"] as? [[String:Any]] {
                                for record in records {
                                    var singleRecord = [String:Any]()
                                    if let value  = record["value"] as? Double {
                                       singleRecord["value"] = value
                                    }
                                    if let type  = record["type"] as? String {
                                        singleRecord["type"] = type
                                    }
                                    if let value  = record["value"] as? [String:Any] {
                                        singleRecord["value_obj"] = value
                                    }
                                    recordData.append(singleRecord)
                                }
                                sinngleData["records"] =  recordData
                            }
                            dataDicArray.append(sinngleData)
                        }
                        serialisationDic[Keys.KEY_DATA] = dataDicArray
                    }
                    
                    print("converted data:\(serialisationDic)")
                    do {
                        let dicData = try JSONSerialization.data(withJSONObject: serialisationDic, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        let vitalRecord = try decoder.decode(VitalPages.self, from: dicData)
                        self.vitalRecord = vitalRecord
                        if tag == 1 {
                            self.records.removeAll()
                        }
                        self.records += vitalRecord.recordData ?? []
                        
                        print("Total:\(self.vitalRecord?.total ?? 0) count:\(self.records.count) nextpage:\(vitalRecord.nextPage)")
                       
                    }catch let err {
                        print("Errfbdufbdfjdnsf:\(err.localizedDescription)")
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.linearBar.stopAnimation()
                self.recordsTableView.reloadData()
                self.recordsTableView.isHidden = self.records.isEmpty
            }
        }
    }
    
    func checkDOBUpdated() {
        if (User.get_user_profile_default().dob ?? "").isEmpty
        {
            let alert = UIAlertController(title: "Profile", message: "Looks like you haven't updated your profile with complete details. Please update DOB and Gender.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
               self.performSegue(withIdentifier: StoryboardSegueIDS.ID_MY_PROFILE_SEGUE, sender: self)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    /*
    linear bar configuration
    */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
    }
    

    /// Get all the vital record data
    ///
    /// - Parameters:
    ///   - url: url to be passed with page number if pagination enabled
    ///   - tag: tag is used to check if we want to refresh the array or removeAll and add again use tag 1 for removing all from array
    func getListOfVitalDates(url:String,tag:Int) {
        self.linearBar.startAnimation()
        VitalPages.getVitalRecodsData(url: url) { (success, vitalPages, statusCode, response) in
            let isNoError = self.check_Status_Code(statusCode: statusCode, data: response)
            
            if isNoError {
                DispatchQueue.main.async {
                    self.vitalRecord = vitalPages
                    if tag == 1 {
                        self.records.removeAll()
                    }
                    self.records += vitalPages?.recordData ?? []
                    print("Total:\(self.vitalRecord?.total ?? 0) count:\(self.records.count) nextpage:\(vitalPages?.nextPage)")
                }
            }
            
            DispatchQueue.main.async {
                self.linearBar.stopAnimation()
                self.recordsTableView.reloadData()
                self.recordsTableView.isHidden = self.records.isEmpty
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            self.linearBar.stopAnimation()
        }
    }

    
// MARK: - UIButton Actions
    @IBAction func addNewTapped(_ sender:UIBarButtonItem) {
        self.isEditRecord = false
        self.isNewRecord = true
        self.selectedRecordId = ""
        self.selectedVitalRecord = nil
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VIEW_EDIT_RECORD, sender: self)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_VIEW_EDIT_RECORD ,
            let destVC = segue.destination as? PHRInfoViewController {
            destVC.isEditRecord = self.isEditRecord
            destVC.isNewRecord = self.isNewRecord
            destVC.selectedRecordId = self.selectedRecordId
            destVC.selectedVitalRecord = self.selectedVitalRecord
            destVC.vitalTemplate = self.vitalTemplate
            destVC.delegate = self
        }
    }
    
}

extension VitalRecordsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! VitalRecordsTableViewCell
        cell.setupCell(record: self.records[indexPath.row], with: indexPath.row)
        cell.cellDelegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isEditRecord = false
        self.isNewRecord = false
        self.selectedRecordId = self.records[indexPath.row]._id ?? ""
        self.selectedVitalRecord = self.records[indexPath.row]
        print("dbgfhdbfhw:\(self.selectedVitalRecord?.recordValues?.forEach{ $0.dicRepresentation() })")
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VIEW_EDIT_RECORD, sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}

extension VitalRecordsViewController : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        if !decelerate {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= scrollView.contentSize.height {
                print("scrollViewDidEndDragging method called")
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            print("Scrolling did end decelerating method called")
            if let nextPage = self.vitalRecord?.nextPage {
                //self.getListOfVitalDates(url: AppURLS.URL_GET_VITAL_RECORDS + "?page=\(nextPage)", tag: 0 )
                 self.getVitalValues(url: AppURLS.URL_GET_VITAL_RECORDS + "?page=\(nextPage)", tag: 0)
            }
        }
    }
}

extension VitalRecordsViewController: VRCellDelegate {
    func didTapEdit(at index: Int) {
        self.isEditRecord = true
        self.isNewRecord = false
        self.records[index].isEditMode = true
        self.selectedRecordId = self.records[index]._id ?? ""
        self.selectedVitalRecord = self.records[index]
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_VIEW_EDIT_RECORD, sender: self)
    }
    
    func didTapDelete(at index: Int) {
        let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete vital", preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "Yes! Delete", style: .destructive, handler: { (action) in
            self.startAnimating()
            self.records[index].deleteRecord(completion: { (success) in
                DispatchQueue.main.async {
                    if success {
                        self.records.remove(at: index)
                    }
                    self.stopAnimating()
                    self.recordsTableView.reloadData()
                    self.recordsTableView.isHidden = self.records.isEmpty
                    self.didShowAlert(title: success ? "" : "Sorry!", message: success ? "Record deleted successfully" : "Unable to delete record. Please try again")
                }
            })
        })
        
        let cancel = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(cancel)
        alert.addAction(deleteAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension VitalRecordsViewController: PHRInfoViewDelegate {
    func didDoneUpdatingDetails() {
        self.navigationController?.popViewController(animated: true)
        //self.getListOfVitalDates(url: AppURLS.URL_GET_VITAL_RECORDS , tag: 1)
         self.getVitalValues(url: AppURLS.URL_GET_VITAL_RECORDS, tag: 1)
    }
}


protocol VRCellDelegate: class{
    func didTapDelete(at index:Int)
    func didTapEdit(at index:Int)
}

class VitalRecordsTableViewCell: UITableViewCell {
    @IBOutlet var dateNTime: UILabel!
    @IBOutlet var bt_edit: UIButton!
    @IBOutlet var bt_delete: UIButton!
    
    weak var cellDelegate: VRCellDelegate?
    
    func setupCell(record:VitalRecord,with tag:Int) {
        self.dateNTime.text = record.recordedAt?.getDateStringFromTimeStamp()
        self.bt_edit.tag = tag
        self.bt_delete.tag = tag
    }
    
    @IBAction func deleteTapped(_ sender: UIButton) {
        self.cellDelegate?.didTapDelete(at: sender.tag)
    }
    
    @IBAction func editTapped(_ sender:UIButton) {
         self.cellDelegate?.didTapEdit(at: sender.tag)
    }
}

