//
//  AddOnsListViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 02/02/18.
//  Copyright © 2018 ConversionBug. All rights reserved.
//

import UIKit


class AddOnsListViewController: UIViewController {

    @IBOutlet var tableIVew: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.tableIVew.tableFooterView = UIView(frame: CGRect.zero)
       self.tableIVew.reloadData()
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_BMI_SEGUE {
            let destVC = segue.destination as! BMIViewController
            destVC.isFromDashboard = true
        }
//        else if segue.identifier == StoryboardSegueIDS.ID_FITBIT_SEGUE {
//            let destVC = segue.destination as! FitbitViewController
//            destVC.isFromDashboard = true
//        }
    }
}

extension AddOnsListViewController : UITableViewDelegate ,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.AddOns, for: indexPath) as! AddOnsListTableViewCell
        if indexPath.row == 0 {
            cell.lb_title.text = "BMI (Body Mass Index)"
            cell.lb_subContent.text = "Know your BMI"
            cell.iv_icon.image = UIImage(named: "ic_icon_bmi")
        }
        /*
        else if indexPath.row == 1{
            cell.lb_title.text = "FITBIT"
            cell.lb_subContent.text = "FITBIT"
            cell.iv_icon.image = UIImage(named: "smartwatch")
            
        }*/
        else if indexPath.row == 1{
            cell.lb_title.text = "Speed Test"
            cell.lb_subContent.text = "Know your Internet Speed"
            cell.iv_icon.image = UIImage(named: "ic_icon_bmi")
            
        }
        else if indexPath.row == 2{
            cell.lb_title.text = "HRA"
            cell.lb_subContent.text = "HRA"
            cell.iv_icon.image = UIImage(named: "ic_icon_bmi")
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BMI_SEGUE, sender: self)
        }
        /*else if indexPath.row == 1{
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_FITBIT_SEGUE_USER, sender: self)
        }*/
        else if indexPath.row == 1{
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SPEED_SEGUE, sender: self)
        }
        else if indexPath.row == 2{
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_HRA_SEGUE, sender: self)
        }
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 65
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
}



