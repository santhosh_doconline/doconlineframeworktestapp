//
//  PlaceOrderViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 26/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class PlaceOrderViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var tv_shippingAddress: UITextView!
    @IBOutlet weak var lb_subtotal: UILabel!
    
    @IBOutlet weak var medicineTableView: UITableView!
    
    @IBOutlet weak var bt_place_order: UIButton!
    
    @IBOutlet weak var LC_medicineTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lb_date: UILabel!
    
    ///concatenated shipping address instance
    var shippingAddress = ""
    ///Model instance for shipping address
    var shippingModal : ShippinAddress?
    ///Subtotal instance
    var subTotal : Double = 0.0
    ///appointment id instance
    var appointment_id = ""
    ///instance of string type dictionary
    var dataDict = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.shadowEffect(views: [tv_shippingAddress])
        self.buttonCornerRadius(buttons: [bt_place_order])
        
        let currentDate = "\(Date())"
        let dateString = currentDate.components(separatedBy: " ")
        let formatedDate = getFormattedDate(dateString: "\(dateString[0]) \(dateString[1])")
        self.lb_date.text = "Date : \(formatedDate)"
        
        medicineTableView.rowHeight = UITableView.automaticDimension
        medicineTableView.estimatedRowHeight = 69
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tv_shippingAddress.text = self.shippingAddress
        self.medicineTableView.reloadData()
        calculateSubTotal()
        UIView.animate(withDuration: 0.5) {
           self.LC_medicineTableViewHeight.constant = self.medicineTableView.contentSize.height //+ 30
           self.view.layoutIfNeeded()
        }
    }
   
    ///Method calculates the subtotal and creates the dictionary with product id and packs
    func calculateSubTotal() {
        for medicine in App.prescribedMedicines {
            let total =  Double(medicine.maxPacks!) * Double(medicine.price!)!
            self.subTotal += total
            
            dataDict[medicine.product_id!] = String(Int(medicine.maxPacks!))
            
            self.appointment_id = medicine.appointment_id!
        }
        self.lb_subtotal.text = "SUB-TOTAL : ₹\(self.subTotal)"
        print("Items Data : \(dataDict) total:\(self.subTotal)")
    }
    
    @IBAction func placeOrderTapped(_ sender: UIButton) {
       // let dataString = "{\"pincode\":\"\(App.shippingPincode)\",\(self.itemsData),\"address1\":\"\(self.shippingAddress)\"}"
        
        let object = [ "pincode" : App.shippingPincode,
                       "items" : self.dataDict ,
                       "address1" : self.shippingAddress] as [String : Any]
        var jsonData : Data?
        do {
              jsonData = try JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch {
            print("Error while converting to json")
        }

        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
            
        }
        startAnimating()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let procureURL = AppURLS.URL_Order_Medicines + self.appointment_id
        var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = jsonData
 
        do {
            if let json = try JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary
            {
                print("MEdicine data:\(json)")
            }
        }catch {
            print("Error while converting to json")
        }

        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async(execute: {
                    print("Error==> : \(error.localizedDescription)")
                    self.stopAnimating()
                 self.didShowAlert(title: "", message: error.localizedDescription)
                // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)

              })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
               print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                do   {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus      {
                        print("performing error handling procure medicine\(resultJSON)")
                        if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                self.didShowAlert(title: "", message: message)
                                //AlertView.sharedInsance.showFailureAlert(title: "", message: message)
                               // AlertView.sharedInsance.showInfo(title: "", message: message)
                            })
                        }
                    }else {
                        print("Place order response:\(resultJSON)")
                        if let bookedData = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                           // let custId = bookedData.object(forKey: Keys.KEY_CUSTOMER_ID) as! Int
                           // let orderAmount = bookedData.object(forKey: Keys.KEY_ORDER_AMOUNT) as! NSNumber
                            let orderId = bookedData.object(forKey: Keys.KEY_ORDER_ID) as! String
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                let alert = UIAlertController(title: "Order Placed Successfully", message: "Order ID : \(orderId)\n Payment Option\n Cash on Delivery (COD).", preferredStyle: UIAlertController.Style.alert)
                                let okay = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                                    App.isFromProcureMedicine = false
                                    App.prescribedMedicines.removeAll()
                                    App.shippingPincode = ""
                                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_GOTO_APPOINTMENTS, sender: self)
                                 })
                                alert.addAction(okay)
                                self.present(alert, animated: true, completion: nil)
                            })
                        }
                    }
                }catch let error{
                     self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                }
            }
       }.resume()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlaceOrderViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return App.prescribedMedicines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.MEDICINES, for: indexPath) as! PlaceOrderTableViewCell
        let medicine = App.prescribedMedicines[indexPath.row]
        cell.lb_medicineName.text = "\(medicine.name!) | \(medicine.product_id!)"
        cell.lb_quantity.text = "Packs: \(Int(medicine.maxPacks!))"
        cell.lb_price.text = "X Price : ₹\(medicine.price!)"
        let total = Double(medicine.maxPacks!) * Double(medicine.price!)!
        cell.lb_total.text = "Total : ₹\(total)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}




