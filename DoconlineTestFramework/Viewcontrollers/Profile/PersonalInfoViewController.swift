//
//  PersonalInfoViewController.swift
//  DocOnline
//
//  Created by dev-3 on 24/07/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class PersonalInfoViewController: UIViewController ,NVActivityIndicatorViewable,LanguageSelectionDelegate{


    @IBOutlet weak var lb_have_a_code: UILabel!
    @IBOutlet weak var sw_coupon_code: UISwitch!
    
    @IBOutlet weak var vw_coupon: UIView!
    @IBOutlet weak var tf_coupon_code: UITextField!

    @IBOutlet weak var LC_coupon_view_height: NSLayoutConstraint!
    
    ///Boolean to check the has coupon switch changed
    var HAS_COUPON = false
    ///Boolean to check languages view appeared or not
    static var isFromLanguages = false
    
    @IBOutlet weak var languagesCollectionView: UICollectionView!
    @IBOutlet weak var vw_suffix: UIView!
    @IBOutlet weak var vw_firstName: UIView!
    @IBOutlet weak var vw_middleName: UIView!
    @IBOutlet weak var vw_lastName: UIView!
    @IBOutlet weak var vw_date_of_birtg: UIView!
    @IBOutlet weak var vw_gender: UIView!
    @IBOutlet var vw_personalInfoBack: UIView!
    
    @IBOutlet weak var tf_dateOfBirth: UITextField!
    @IBOutlet weak var tf_suffix: UITextField!
    @IBOutlet weak var tf_firstName: UITextField!
    @IBOutlet weak var tf_middleName: UITextField!
    @IBOutlet weak var tf_lastName: UITextField!
    @IBOutlet weak var tf_gender: UITextField!
    @IBOutlet var tf_langauge: UITextField!
    
    @IBOutlet weak var iv_expand_arrow: UIImageView!
    @IBOutlet weak var iv_expand_arrow_2: UIImageView!
    
    @IBOutlet weak var bt_update_personal_info: UIButton!
    @IBOutlet weak var lb_date_of_birth_message: UILabel!
    @IBOutlet weak var lb_english_default_lang: UILabel!
    
    @IBOutlet weak var vw_date_picker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dp_bottom_constraint: NSLayoutConstraint!
    
    @IBOutlet weak var dp_height_constraint: NSLayoutConstraint!
    
    ///model Instance to store the User details
    var userDataModel :  User!
    
    ///Instance to picker view
    let picker = UIImagePickerController()
    ///selected image instance
    var pickedImage : UIImage!
    
    ///instance to store selected prefix
    var selectedPrefix = ""
    ///Instance to get selected date of birth string
    var selectedDateOfBirth = ""
    
    ///minimum date instance to display dates
    var minimumDate:Date!
    ///maximum date instance to display dates
    var maximumDate:Date!
    
    
    static var delegate: ProfileViewAlignDelegate?
    
    ///long press gesture recognizer
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNamePrefixes()
        PersonalInfoViewController.isFromLanguages = false
        Languages.changePreferedStatus{ (success) in
            if success {
                DispatchQueue.main.async {
                    print("Count:\(App.selectedLanguages.count)")
                    
                    if App.selectedLanguages.count != 0 {
                        for lang in App.selectedLanguages {
                            if let langName = lang.lang_name {
//                                if langName.lowercased().isEqual("english") {
//                                    print("its english")
//                                     self.tf_langauge.placeholder = "Language"
//                                }else {
                                    self.tf_langauge.text =  langName
//                                }
                            }
                        }
                    }
                
                    let sortedPreLang = App.selectedLanguages.sorted { $0.preferedCount! < $1.preferedCount! }
                    App.selectedLanguages = sortedPreLang
                  //  self.languagesCollectionView.reloadData()
                    print("Step three finished")
                }
            }
        }
        
        UpdateUIFields()
        
      //  getUserMainProfile() 
      //  buttonCornerRadius(buttons: [bt_update_personal_info])
      ///  shadowEffect(views: [vw_suffix,vw_firstName,vw_middleName,vw_lastName,vw_date_of_birtg,vw_gender,vw_coupon])
        
      //  self.languagesCollectionView.reloadData()
     //   self.languagesCollectionView.layer.cornerRadius = 10
        
//        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
//        languagesCollectionView.addGestureRecognizer(longPressGesture)
        
//        let swipedUp = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwipeUp(_:)))
//        swipedUp.direction = .up
//        self.vw_personalInfoBack.addGestureRecognizer(swipedUp)
//
        let swipedDown = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwipeDown(_:)))
        swipedDown.direction = .down
        self.vw_personalInfoBack.addGestureRecognizer(swipedDown)
        
    }
    
    @objc func didSwipeDown(_ gesture:UISwipeGestureRecognizer) {
        PersonalInfoViewController.delegate?.didScrollDown()
    }
    
    @objc func didSwipeUp(_ gesture:UISwipeGestureRecognizer) {
         PersonalInfoViewController.delegate?.didScrollUp()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Personal info \(#function)")
        bt_update_personal_info.backgroundColor = Theme.buttonBackgroundColor
        if !PersonalInfoViewController.isFromLanguages {
           UpdateUIFields()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if App.selectedLanguages.count != 0 {
            for lang in App.selectedLanguages {
                if let langName = lang.lang_name {
                    self.tf_langauge.text =  langName
                }
            }
        }
    }
    
    ///Handles the long press gesture recognizer
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = languagesCollectionView.indexPathForItem(at: gesture.location(in: languagesCollectionView)) else {
                break
            }
            languagesCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            languagesCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            languagesCollectionView.endInteractiveMovement()
        default:
            languagesCollectionView.cancelInteractiveMovement()
        }
    }
    
    
//MARK:- Language selection delegate method
    func didFinishPickingLanguages(_ success: Bool, languages: [Languages], prefered: String) {
        if success {
            App.selectedLanguages = languages
            self.tf_langauge.text = prefered
          //  self.languagesCollectionView.reloadData()
            self.dismiss(animated: true, completion: nil)
            print("Delegate performed : language:\(prefered)")
        }
    }
    
    
    
    
    /**
     Method performs request to get user profile
     */
    func getUserMainProfile() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
              self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            self.userDataModel = User.get_user_profile_default()
            self.UpdateUIFields()
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            //  startAnimating()
            let url = URL(string: AppURLS.URL_UserProfile)
            print("Profile URl==>\(url)")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                           self.didShowAlert(title: "Sorry", message: "we couldn't get your profile right now. please try again later")
                      //  AlertView.sharedInsance.showFailureAlert(title: "Sorry!", message: "we couldn't get your profile right now. please try again later")
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling get user profile:\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                if code == 200 && responseStatus == "success"
                                {
                                    var prefix = ""
                                    var mrnNo = ""
                                    var first_name = ""
                                    var middle_name = ""
                                    var last_name = ""
                                    var dob = ""
                                    var gender = ""
                                    var address1 = ""
                                    var address2 = ""
                                    var uCountryId = 0
                                    var state = ""
                                    var city = ""
                                    var pincode = ""
                                    var mobile = ""
                                    var alternateContact = ""
                                    var user_id :Int!
                                    var id :Int!
                                    var email = ""
                                    
                                    if let emailID = data.object(forKey: Keys.KEY_EMAIL) as? String {
                                        email = emailID
                                    }
                                    
                                    if let mrn = data.object(forKey: Keys.KEY_MRN_NO) as? String
                                    {
                                        mrnNo = mrn
                                    }
                                    
                                    if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int
                                    {
                                        user_id = userid
                                    }
                                    if let uid = data.object(forKey: Keys.KEY_ID) as? Int
                                    {
                                        id = uid
                                    }
                                    if let prefixType = data.object(forKey: Keys.KEY_PREFIX) as? String
                                    {
                                        prefix = prefixType
                                    }
                                    if let fName = data.object(forKey: Keys.KEY_FIRST_NAME) as? String
                                    {
                                        first_name = fName
                                    }
                                    if let lname = data.object(forKey: Keys.KEY_LAST_NAME) as? String
                                    {
                                        last_name = lname
                                    }
                                    if let mname = data.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                    {
                                        middle_name = mname
                                    }
                                    if let dateob = data.object(forKey: Keys.KEY_DATE_OF_BIRTH) as? String
                                    {
                                        dob = dateob
                                    }
                                    if let ugender = data.object(forKey: Keys.KEY_GENDER) as? String
                                    {
                                        gender = ugender
                                    }
                                    if let add1 = data.object(forKey: Keys.KEY_ADDRESS_1) as? String
                                    {
                                        address1 = add1
                                    }
                                    if let add2 = data.object(forKey: Keys.KEY_ADDRESS_2) as? String
                                    {
                                        address2 = add2
                                    }
                                    if let cID = data.object(forKey: Keys.KEY_COUNTRY_ID) as? Int
                                    {
                                        uCountryId = cID
                                    }
                                    if let ustate = data.object(forKey: Keys.KEY_STATE) as? String
                                    {
                                        state = ustate
                                    }
                                    if let ucity = data.object(forKey: Keys.KEY_CITY) as? String
                                    {
                                        city = ucity
                                    }
                                    if let upincode = data.object(forKey: Keys.KEY_PIN_CODE) as? String
                                    {
                                        pincode = upincode
                                    }
                                    
                                    if let umobile = data.object(forKey: Keys.KEY_MOBILE_NO) as? String
                                    {
                                        mobile = umobile
                                    }
                                    
                                    //else {
                                    
                                    //                                        if let mobileStored = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
                                    //                                            mobile = mobileStored
                                    //                                        }
                                    // }
                                    
                                    if let ualtMob = data.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                    {
                                        alternateContact = ualtMob
                                    }
                                    
                                    UserDefaults.standard.set(mobile, forKey: UserDefaltsKeys.MOBILE_NUMBER)
                                    
                                    let userDetails = User(avatar: App.avatarUrl, user_id: user_id, prefix: prefix, middle_name: middle_name, phone: mobile,alternate_phone:alternateContact, address1: address1, address2: address2, city: city, state: state, pincode: pincode, id: id, first_name: first_name, last_name: last_name, email: email, gender: gender, dob: dob, fullname: "", country: uCountryId)
                                    userDetails.mrnNumber = mrnNo
                                    self.userDataModel = userDetails
                                    
                                    User.store_user_profile_default(dataModel: userDetails)
                                    
                                    DispatchQueue.main.async(execute: {
                                        self.UpdateUIFields()
                                        //self.stopAnimating()
                                    })
                                    
                                }else {
                                    print("Error while fetching data")
                                }
                            }
                        }
                        catch _
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON")
                        }
                    }
                }
                
            }).resume()
            
            // Bounce back to the main thread to update the UI
        }
    }

    ///updates the UI with fetched user details
    func UpdateUIFields()  {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
                self.userDataModel  = User.get_user_profile_default()
                print("status:\(UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS))  User First name :\(self.userDataModel.first_name!)")
                self.tf_firstName.text = self.userDataModel.first_name!
                self.tf_lastName.text = self.userDataModel.last_name!
                self.tf_middleName.text = self.userDataModel.middle_name!
                self.tf_suffix.text = self.userDataModel.prefix!
                if self.userDataModel.dob!.isEmpty != true {
                    self.tf_dateOfBirth.text = self.getFormattedDateOnly(dateString: self.userDataModel.dob!)
                    self.selectedDateOfBirth = self.userDataModel.dob!
                }
                
                self.tf_gender.text = self.userDataModel.gender!
                if let prefixes = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_NAME_PREFIXES) as? NSDictionary {
                    for (key,value) in prefixes {
                        if (value as! String) == self.userDataModel.prefix! {
                            self.selectedPrefix = key as! String
                            print("Selected prfix:\(self.selectedPrefix)")
                        }
                    }
                }
            }
            ///Relaad language collection view
//            self.languagesCollectionView.reloadData()
        }
    }

    
    @IBAction func languagesTapped(_ sender: UIButton) {
         self.performSegue(withIdentifier: StoryboardSegueIDS.ID_LANGUAGES, sender: sender)
    }
    
    @IBAction func genderTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_expand_arrow_2.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Select", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let MRAction = UIAlertAction(title: "Male", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_gender.text = "Male"
            print("\(self.tf_gender.text! as String)")
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_expand_arrow_2.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let MSaction = UIAlertAction(title: "Female", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_gender.text = "Female"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_expand_arrow_2.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let TSaction = UIAlertAction(title: "Transgender", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.tf_gender.text = "Transgender"
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_expand_arrow_2.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let  performAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_expand_arrow_2.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) *    180.0)
            })
        })
        
        actionView.addAction(performAction)
        actionView.addAction(MRAction)
        actionView.addAction(MSaction)
        actionView.addAction(TSaction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func datePickerActionButtonsTapped(_ sender: UIButton) {
        print("DatePicker :\(datePicker.date)")
        UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.dp_bottom_constraint.constant = -1000
        }, completion: nil)
    }
   
    @IBAction func datePickerTapped(_ sender: UIDatePicker) {
        
        self.dp_height_constraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            
            self.view.layoutIfNeeded()
            
        }) { (success) in
            print("closed")
            print("Date selected : \(sender.date)")
        }

    }
    
    @IBAction func dateOfBirthTapped(_ sender: UIButton) {

        if let gregorian = NSCalendar(calendarIdentifier:.gregorian)
        {
            if let minDate = gregorian.date(byAdding: .year, value: -100, to: Date())
            {
                minimumDate = minDate
            }
            
            if let maxDate = gregorian.date(byAdding: .year, value: -18, to: Date())
            {
                maximumDate = maxDate
            }
        }

        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate:minimumDate, maximumDate:maximumDate, datePickerMode: .date) {
            (date) -> Void in

            if let dateSelected = date {
                let dateSt = self.getDateString(date: dateSelected)
                let formStr = self.getDateFormat(date: date!)
                let seperatedDate = dateSt.components(separatedBy: " ")
                print("Date:(self.dateConverterUTCToCurrent(utcDate: dateSt ))")
                self.selectedDateOfBirth = seperatedDate[0]
                print("Date:\(self.selectedDateOfBirth)")
                self.tf_dateOfBirth.text = self.getFormattedDate(dateString: formStr)

              let years = Calendar.current.dateComponents([.year], from: dateSelected, to: Date()).year
              let days = Calendar.current.dateComponents([.day], from: dateSelected, to: Date()).day
                print("Years :\(years) days : \(days)")

                if years! > 100 {
                    self.lb_date_of_birth_message.isHidden = false
                    self.selectedDateOfBirth = ""
                    self.tf_dateOfBirth.text = ""
                }else if years! <= 0 && days! == 0{
                    self.lb_date_of_birth_message.isHidden = false
                    self.selectedDateOfBirth = ""
                    self.tf_dateOfBirth.text = ""
                }else {
                    self.lb_date_of_birth_message.isHidden = true
                }
            }
        }
    }
    
    
    @IBAction func suffixButtonTapped(_ sender: UIButton) {
        if App.namePrefixes == nil {
            print("No prefixes")
            App.namePrefixes = [
                "Mr.": "Mr.",
                "Mrs.": "Mrs.",
                "Dr.": "Dr.",
                "Prof.": "Prof.",
                "Miss": "Miss"
            ]
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Select", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        for (key,value) in App.namePrefixes {
            let  performAction = UIAlertAction(title: value as? String, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                self.tf_suffix.text = value as? String
                self.selectedPrefix = key as! String
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) *    180.0)
                })
            })
            actionView.addAction(performAction)
        }
        
        let  performCancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) *    180.0)
            })
        })
        
        actionView.addAction(performCancelAction)
        self.present(actionView, animated: true, completion: nil)
    }

    @IBAction func updateProfileTapped(_ sender: Any) {
        
        let prefix = self.tf_suffix.text!
        let firstName = self.tf_firstName.text!
        let lastName = self.tf_lastName.text!
        let middleName = self.tf_middleName.text!
        let dob = self.tf_dateOfBirth.text!
        let gender = self.tf_gender.text!
      //  let couponCode = self.tf_coupon_code.text! as String
        
        if prefix.isEmpty == true {
            self.didShowAlert(title: "Prefix required!", message: "")
           // AlertView.sharedInsance.showInfo(title: "Prefix Required!", message: "Please select a prefix")
        }else if firstName.isEmpty == true {
            self.didShowAlert(title: "FirstName required!", message: "Please fill your FirstName")
          //  AlertView.sharedInsance.showInfo(title: "FirstName required!", message: "Please fill your FirstName")
        }else if firstName.count  < 3{
            self.didShowAlert(title: "FirstName required!", message: "Minimum 3 characters required")
          //  AlertView.sharedInsance.showInfo(title: "FirstName", message: "Should be minimum three characters")
        }else if lastName.isEmpty == true {
            self.didShowAlert(title: "LastName required!", message: "Please fill your LastName")
           // AlertView.sharedInsance.showInfo(title: "LastName required!", message: "Please fill your LastName")
        }else if dob.isEmpty == true {
            self.didShowAlert(title: "D.O.B required!", message: "Please fill your D.O.B")
         //   AlertView.sharedInsance.showInfo(title: "D.O.B required!", message: "Please fill your D.O.B")
        }else if gender.isEmpty == true {
            self.didShowAlert(title: "Gender required!", message: "Please fill your Gender")
           // AlertView.sharedInsance.showInfo(title: "Gender required!", message: "Please fill your Gender")
        }
//        else if couponCode.isEmpty && self.HAS_COUPON {
//
//            self.didShowAlert(title: "", message: "Please enter coupon code")
//            // AlertView.sharedInsance.showInfo(title: "Coupon Code", message: "Please enter coupon code")
//
//        }
        else {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            let userDic : [String:Any] = [Keys.KEY_PREFIX: self.selectedPrefix,
                                          Keys.KEY_FIRST_NAME: firstName,
                                          Keys.KEY_MIDDLE_NAME: middleName,
                                          Keys.KEY_LAST_NAME: lastName,
                                          Keys.KEY_DATE_OF_BIRTH: self.selectedDateOfBirth,
                                          Keys.KEY_GENDER:gender,
                                          Keys.KEY_ENABLE_COUPON: self.HAS_COUPON,
                                          Keys.KEY_COUPON_CODE: ""]
            
            let profileURL = AppURLS.URL_UserProfile
            
            do {
                let userData = try JSONSerialization.data(withJSONObject: userDic, options: .prettyPrinted)
                
                self.startAnimating()
                print("profileupdate url:==>\(profileURL)")
                print("Registration Data:==>\(userData)")
                
                NetworkCall.performPATCH(url: profileURL, data: userData, completionHandler: { (success, response, statusCode, error) in
                    
                    if let error = error
                    {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                            print("Error==> : \(error.localizedDescription)")
                            self.didShowAlert(title: "", message: error.localizedDescription)
                            //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                        })
                    }
                    
                    if let resultJSON = response
                    {
                        let errorStatus = self.check_Status_Code(statusCode: statusCode ?? 0 , data: resultJSON)
                        if !errorStatus {
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                            })
                            print("performing error handling update user profile:\(resultJSON)")
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Successfully updated response:\(resultJSON)")
                                var prefix = ""
                                var first_name = ""
                                var mrnNo = ""
                                var middle_name = ""
                                var last_name = ""
                                var dob = ""
                                var gender = ""
                                var address1 = ""
                                var address2 = ""
                                var uCountryId = 0
                                var state = ""
                                var city = ""
                                var pincode = ""
                                var mobile = ""
                                var alternateContact = ""
                                var user_id :Int!
                                var id :Int!
                                var email = ""
                                var coupon_applied = ""
                                
                                if let coup = data.object(forKey: Keys.KEY_COUPON_APPLIED) as? String
                                {
                                    coupon_applied = coup
                                }
                                
                                if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int
                                {
                                    user_id = userid
                                }
                                
                                if let mrnNumber = data.object(forKey: Keys.KEY_MRN_NO) as? String
                                {
                                    mrnNo = mrnNumber
                                }
                                
                                if let uid = data.object(forKey: Keys.KEY_ID) as? Int
                                {
                                    id = uid
                                }
                                if let prefixType = data.object(forKey: Keys.KEY_PREFIX) as? String
                                {
                                    prefix = prefixType
                                }
                                if let fName = data.object(forKey: Keys.KEY_FIRST_NAME) as? String
                                {
                                    first_name = fName
                                }
                                if let lname = data.object(forKey: Keys.KEY_LAST_NAME) as? String
                                {
                                    last_name = lname
                                }
                                if let mname = data.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                {
                                    middle_name = mname
                                }
                                if let dateob = data.object(forKey: Keys.KEY_DATE_OF_BIRTH) as? String
                                {
                                    dob = dateob
                                }
                                if let ugender = data.object(forKey: Keys.KEY_GENDER) as? String
                                {
                                    gender = ugender
                                }
                                if let add1 = data.object(forKey: Keys.KEY_ADDRESS_1) as? String
                                {
                                    address1 = add1
                                }
                                if let add2 = data.object(forKey: Keys.KEY_ADDRESS_2) as? String
                                {
                                    address2 = add2
                                }
                                if let cID = data.object(forKey: Keys.KEY_COUNTRY_ID) as? Int
                                {
                                    uCountryId = cID
                                }
                                if let ustate = data.object(forKey: Keys.KEY_STATE) as? String
                                {
                                    state = ustate
                                }
                                if let ucity = data.object(forKey: Keys.KEY_CITY) as? String
                                {
                                    city = ucity
                                }
                                if let upincode = data.object(forKey: Keys.KEY_PIN_CODE) as? String
                                {
                                    pincode = upincode
                                }
                                
                                if let ualtMob = data.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                {
                                    alternateContact = ualtMob
                                }
                                
                                if let emailID = data.object(forKey: Keys.KEY_EMAIL) as? String {
                                    email = emailID
                                }else {
                                    email = User.get_user_profile_default().email!
                                }
                                
                                if let umobile = data.object(forKey: Keys.KEY_MOBILE_NO) as? String
                                {
                                    mobile = umobile
                                }else {
                                    mobile = User.get_user_profile_default().phone!
                                }
                                
                                
                                let userDetails = User(avatar: App.avatarUrl, user_id: user_id, prefix: prefix, middle_name: middle_name, phone: mobile,alternate_phone: alternateContact, address1: address1, address2: address2, city: city, state: state, pincode: pincode, id: id, first_name: first_name, last_name: last_name, email: email, gender: gender, dob: dob, fullname: "", country: uCountryId)
                                userDetails.mrnNumber = mrnNo
                                App.user_full_name = "\(prefix) \(firstName) \(lastName) \(mrnNo)"
                                self.userDataModel = userDetails
                                User.store_user_profile_default(dataModel: userDetails)
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    self.UpdateUIFields()
                                    
                                    var successMessage = "Profile updated."
                                    if self.HAS_COUPON {
                                        successMessage += "\n\n\(coupon_applied)"
                                    }
                                    
                                    
                                    self.didShowAlert(title: "Success", message: successMessage )
                                    // AlertView.sharedInsance.showSuccessAlert(title: "", message: "Profile updated")
                                })
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    let errors = data.object(forKey: "errors") as! NSDictionary
                                    print("Error***=>\(errors)")
                                    self.stopAnimating()
                                })
                            }
                        }
                    }
                })
                
            }catch {
                print("Errorr")
            }
            
            //let userData = "{\"\(Keys.KEY_PREFIX)\":\"\(self.selectedPrefix)\", \"\(Keys.KEY_FIRST_NAME)\":\"\(firstName)\", \"\(Keys.KEY_MIDDLE_NAME)\":\"\(middleName)\", \"\(Keys.KEY_LAST_NAME)\":\"\(lastName)\", \"\(Keys.KEY_DATE_OF_BIRTH)\":\"\(self.selectedDateOfBirth)\" ,\"\(Keys.KEY_GENDER)\":\"\(gender)\",\"\(Keys.KEY_ENABLE_COUPON)\":\"\(self.HAS_COUPON)\",\"\(Keys.KEY_COUPON_CODE)\":\"\"}"
            
        }
    }

    /**
      Used to get the string width
     - Parameter text: pass the string to get the width
     - Parameter font: use the font which you want
     - Returns CGFloat: width of the string
     */
    func textWidth(text: String, font: UIFont?) -> CGFloat {
        let attributes = font != nil ? [NSAttributedString.Key.font: font!] : [:]
        return text.size(withAttributes: attributes).width
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_LANGUAGES {
            let destVC = segue.destination as! UINavigationController
            let langVC = destVC.topViewController as! LanguagesViewController
            langVC.delegate = self
        }
    }
    
    //MARK:- Switch button
    
    @IBAction func haveCouponSwitchTapped(_ sender: UISwitch) {
        if sender.isOn {
            HAS_COUPON = true
            adjustViewCouponViewHeight(bool: false, height: 42)
        }else {
            HAS_COUPON = false
            adjustViewCouponViewHeight(bool: true, height: 0)
            self.tf_coupon_code.text = ""
        }
    }
    
    ///adjusts the coupon textfiel height
    func adjustViewCouponViewHeight(bool: Bool,height:CFloat) {
        self.LC_coupon_view_height.constant = CGFloat(height)
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            if success {
                //self.vw_coupon.isHidden = bool
            }
        }
    }
    

}



extension PersonalInfoViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return App.selectedLanguages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TV_CV_CELL_IDENTIFIERS.LANGUAGE, for: indexPath) as! ImagesCollectionViewCell
        let lang = App.selectedLanguages[indexPath.row]
        let lng_name = lang.lang_name == nil ? "" : lang.lang_name!
            cell.lb_language.text = lng_name
            cell.lb_language.layer.cornerRadius =  10
            cell.lb_language.clipsToBounds = true
            cell.lb_language.backgroundColor = UIColor(hexString: "#05424C")
            cell.lb_language.textColor = UIColor.white
       
//            if indexPath.row == 0 {
//                cell.lb_language.backgroundColor = UIColor.red
//                cell.lb_language.textColor = UIColor.white
//            }else if indexPath.row == 1 {
//                cell.lb_language.backgroundColor = UIColor.white
//                cell.lb_language.textColor = UIColor.black
//            }else if indexPath.row == 2 {
//                cell.lb_language.backgroundColor = UIColor(hexString: "#409A3C")
//                cell.lb_language.textColor = UIColor.white
//            }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    
        print("Starting Index: \(sourceIndexPath.item)")
        print("Ending Index: \(destinationIndexPath.item)")
        let temp = App.selectedLanguages[sourceIndexPath.row]
        App.selectedLanguages[sourceIndexPath.row] = App.selectedLanguages[destinationIndexPath.row]
        App.selectedLanguages[destinationIndexPath.row] = temp
        
        self.languagesCollectionView.reloadData()
        self.languagesCollectionView.reloadData()
    }
}

extension PersonalInfoViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let language = App.selectedLanguages[indexPath.row].lang_name!
        let width = textWidth(text: language, font: nil)
        return CGSize(width: width + 20, height: 22)
    }
}

extension PersonalInfoViewController : UIScrollViewDelegate {

    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            print(" you reached end of the table")
            PersonalInfoViewController.delegate?.didScrollUp()
        }
    }
}


extension PersonalInfoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        var characterSet = CharacterSet.letters
        characterSet.insert(charactersIn: " ")
        let unwantedStr = string.trimmingCharacters(in: characterSet)
        if textField == self.tf_firstName {
//            if string.rangeOfCharacter(from: charecterSet.inverted) != nil {
//                return false
//            }
           
            return unwantedStr.count == 0
        }else if textField == self.tf_lastName {
           
            return unwantedStr.count == 0
        }else if textField == self.tf_middleName {
            
            return unwantedStr.count == 0
        }

        return true
    }
    
}



