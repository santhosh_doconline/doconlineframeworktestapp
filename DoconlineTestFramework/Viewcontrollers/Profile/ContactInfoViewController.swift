//
//  ContactInfoViewController.swift
//  DocOnline
//
//  Created by dev-3 on 24/07/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

var countrySelected = ""
var countryID : Int!

class ContactInfoViewController: UIViewController,NVActivityIndicatorViewable{

    
    @IBOutlet weak var vw_city: UIView!
    @IBOutlet weak var vw_passcpde: UIView!
    @IBOutlet weak var vw_country: UIView!
    @IBOutlet weak var vw_state: UIView!
    @IBOutlet weak var vw_address_line1: UIView!
    @IBOutlet weak var vw_address_line2: UIView!
 //   @IBOutlet weak var vw_phone: UIView!
//    @IBOutlet weak var vw_alternate_phone: UIView!
    
    @IBOutlet weak var tf_passcode: UITextField!
    @IBOutlet weak var tf_address_line2: UITextField!
    @IBOutlet weak var tf_address_line1: UITextField!
    @IBOutlet weak var tf_country: UITextField!
    @IBOutlet weak var tf_state: UITextField!
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_alternateContactNumber: UITextField!
  //  @IBOutlet weak var tf_phone: UITextField!
  //  @IBOutlet weak var tf_alternate_phone: UITextField!
    
  //  @IBOutlet weak var lb_email: UILabel!
    @IBOutlet weak var bt_contact_update_outlet: UIButton!
    @IBOutlet weak var lb_tap_icon_status: UILabel!
    
 //   @IBOutlet weak var bt_edit_phone: UIButton!
    
    @IBOutlet weak var LC_phone_vwheight: NSLayoutConstraint!
     var userDataModel : User!
    
    ///Shipping address for delivery if neducubes
    var shippingAddress = ""
    ///Model Instance for shipping address
    var shippinAddressModal : ShippinAddress?
    var userPhoneNum = ""
    
    static var delegate: ProfileViewAlignDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UpdateUIFields()
//        buttonCornerRadius(buttons: [bt_contact_update_outlet])
//        shadowEffect(views: [vw_city,vw_alternate_phone,vw_passcpde,vw_country,vw_state,vw_address_line1,vw_address_line2,vw_phone])
        
       // self.tf_phone.addTarget(self, action: #selector(ContactInfoViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        print("Contact info screen")
    }

    override func viewWillAppear(_ animated: Bool) {
        bt_contact_update_outlet.backgroundColor = Theme.buttonBackgroundColor
        if countrySelected.isEmpty != true {
            self.tf_country.text = countrySelected
        }else {
            UpdateUIFields()
        }
        
          print("Contact info screen")
    }
    
    ///textField delegate method
//    func textFieldDidChange(_ textField: UITextField) {
//        if textField.text!.count  == 10{
//            let phone = self.tf_phone.text! as String
//            let mobileData = ["\(Keys.KEY_MOBILE_NO)" : phone]
//
//            if phone == self.userDataModel.phone! {
//                print("same number entered")
//               self.lb_tap_icon_status.isHidden = true
//            }else {
//                var jsonData = Data()
//                do {
//                    jsonData = try JSONSerialization.data(withJSONObject: mobileData, options: .prettyPrinted) as Data
//                    updateNumber(dataToPost: jsonData)
//
//                } catch {
//                    print("Error while converting to jsondata:\(error.localizedDescription)")
//                }
//            }
//          }
//     }
    
    /**
     Update user phone number
     - Parameter dataToPost: pass mobile number json converted to data
     */
    func updateNumber(dataToPost:Data) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
           self.didShowAlert(title: "", message: AlertMessages.NETWORK_ERROR)
          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error!", message:  AlertMessages.NETWORK_ERROR)
            return
        }

        startAnimating()
        let url = URL(string: AppURLS.URL_ChanegMobileNumber)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HTTPMethods.PUT
        request.httpBody = dataToPost
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                //    self.tf_phone.isEnabled = true
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                   // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("httpresponse:\(response as! HTTPURLResponse)")
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("perform error handling verifying number:\(resultJSON)")
                        if let mobile = resultJSON.object(forKey: Keys.KEY_MOBILE_NO) as? [String] {
                            let valueString = mobile[0]
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                             //   self.tf_phone.isEnabled = true
                                self.didShowAlert(title: "", message: valueString)
                               //  AlertView.sharedInsance.showFailureAlert(title: "", message: valueString)
                            })
                        }
                    }
                    else {
                        print("Mobile number change response:\(resultJSON)")

                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                          //  self.tf_phone.isEnabled = false
                         //   let phone = self.tf_phone.text! as String
                            UserDefaults.standard.set(self.userPhoneNum, forKey: UserDefaltsKeys.MOBILE_NUMBER)
                            UserDefaults.standard.set(self.userPhoneNum, forKey: UserDefaltsKeys.KEY_USER_PHONE)
                        //    self.bt_edit_phone.setImage(UIImage(named:"EditRow"), for: UIControlState.normal)
                            self.lb_tap_icon_status.isHidden = true
                            App.isFromAnotherView = true
                            self.instantiate_to_view(withIdentifier: "MobileVerificationView")
                        })
                        
                    }
                }catch let error{
                  //  self.tf_phone.isEnabled = true
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                }
            }
        }).resume()
    }
    
    
    ///Updates the UI on user details fetch
    func UpdateUIFields()  {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
                self.userDataModel  = User.get_user_profile_default()
                print("status:\(UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS))  User First name :\(self.userDataModel.phone!)")
                self.tf_address_line1.text = self.userDataModel.address1!
                self.tf_address_line2.text = self.userDataModel.address2!
                self.tf_country.text = self.getCountryNameById(countryID: self.userDataModel.country!)
                self.tf_alternateContactNumber.text = self.userDataModel.alternate_phone 
                
                if self.userDataModel.email!.isEmpty == true {
                    if let email = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_LOGGED_USER_EMAIL) as? String {
                    //    self.lb_email.text = "Email : " + email
                    //  self.LC_phone_vwheight.constant = 88
                    //    self.lb_email.isHidden = false
                    }else {
                        print("No email id stored")
                     //   self.LC_phone_vwheight.constant = 40
                      //  self.lb_email.isHidden = true
                    }
                }else {
                   // self.lb_email.text = "Email : " + self.userDataModel.email!
                  //  self.LC_phone_vwheight.constant = 88
                 //   self.lb_email.isHidden = false
                }
                print("Phone::=>\(self.userDataModel.phone!)")
                
//                if let email = UserDefaults.standard.value(forKey: UserDefaltsKeys.KEY_USER_NAME) as? String {
//                    self.lb_email.text = email
//                    self.LC_phone_vwheight.constant = 88
//                    self.lb_email.isHidden = false
//                }else {
//                    print("No email id stored")
//                    self.LC_phone_vwheight.constant = 40
//                    self.lb_email.isHidden = true
//                }
                
                self.tf_state.text = self.userDataModel.state!
                self.tf_city.text = self.userDataModel.city!
                self.tf_passcode.text = self.userDataModel.pincode!
                
              //  self.tf_phone.isEnabled = false
                if self.userDataModel.phone!.isEmpty == true {
                    print("User phone is empty")
                 //   self.bt_edit_phone.setTitle("Add", for: UIControlState.normal)
                     //self.tf_phone.isEnabled = true
                     //self.bt_edit_phone.setImage(UIImage(named:"Save"), for: UIControlState.normal)
                     // self.lb_tap_icon_status.isHidden = false
                    
                }else {
                    self.userPhoneNum = self.userDataModel.phone!
                    if !App.isMobileVerified {
                     //   self.bt_edit_phone.setTitle("Verify", for: UIControlState.normal)
                    }else {
                       // self.bt_edit_phone.setTitle("Change", for: UIControlState.normal)
                    }
                    //self.tf_phone.isEnabled = false
                    //self.bt_edit_phone.setImage(UIImage(named:"EditRow"), for: UIControlState.normal)
                   // self.lb_tap_icon_status.isHidden = true
                }
                
//                if let mobileStored = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
//                    self.tf_phone.text = mobileStored
//                }else {
//                    self.tf_phone.isEnabled = true
//                    self.bt_edit_phone.setImage(UIImage(named:"Save"), for: UIControlState.normal)
//                }
                
                if countryID == nil {
                    countryID = self.userDataModel.country!
                }
                
                if App.isFromProcureMedicine {
                    self.tf_passcode.text = App.shippingPincode
                    self.tf_passcode.isEnabled = false
                    self.bt_contact_update_outlet.setTitle("Save Address", for: UIControl.State.normal)
                }else {
                    self.tf_passcode.isEnabled = true
                    self.bt_contact_update_outlet.setTitle("Update Contact Info", for: UIControl.State.normal)
                }
            }
        }
    }
    
    
    /**
     Method returns country name by id
     - Parameter countryID: pass the country id 
     - Returns: country name
     */
    func getCountryNameById(countryID:Int) -> String{
    
        print("check:\(countryID) id:\(self.userDataModel.country!)")
        if countryID == 0 {
            print("no country selected")
            return ""
        }
        var countryToShow = ""
        
        if let countries = UserDefaults.standard.object(forKey: "ListCountry") as? NSDictionary {
            if let ids = countries.allKeys as? [String] {
                for id in ids {
                    if id == "\(countryID)" {
                        countryToShow = countries.object(forKey: id) as! String
                    }
                }
            }
        }else {
            print("Not stored list")
        }
        
//        if App.countriesList.count == 0 {
//            App.countriesList = Country.getCountries()
//        }else {
//            let country = App.countriesList.map { $0.id == countryID }
//            countryToShow = country.name!
//        }
//    
//        for country in App.countriesList {
//            if countryID == country.id! {
//                countryToShow = country.name!
//            }
//        }


        return countryToShow
    }

    @IBAction func editPhoneTapped(_ sender: UIButton) {
        
        if App.isFromProcureMedicine
        {
           App.isFromView = 0
        }
        else {
          App.isFromView = FromView.ProfileView
        }
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CHANGE_MOBILE_NUMBER, sender: self)
        
//        if bt_edit_phone.currentImage == UIImage(named : "EditRow") {
//            self.tf_phone.isEnabled = true
//            self.bt_edit_phone.setImage(UIImage(named:"Save"), for: UIControlState.normal)
//            self.lb_tap_icon_status.isHidden = false
//            
//        }else if bt_edit_phone.currentImage == UIImage(named : "Save") {
//            let phone = self.tf_phone.text! as String
//            if phone.isEmpty == true {
//                self.didShowAlert(title: "Phone number", message: "required!")
//            }else {
//                let mobileData = ["\(Keys.KEY_MOBILE_NO)" : phone]
//                print("Mobile data : \(mobileData)")
//                if phone == self.userDataModel.phone! {
//                    print("same number entered")
//                    self.lb_tap_icon_status.isHidden = true
//                    self.bt_edit_phone.setImage(UIImage(named:"EditRow"), for: UIControlState.normal)
//                }else {
//                    var jsonData = Data()
//                    do {
//                        jsonData = try JSONSerialization.data(withJSONObject: mobileData, options: .prettyPrinted) as Data
//                        updateNumber(dataToPost: jsonData)
//                        
//                    } catch {
//                        print("Error while converting to jsondata:\(error.localizedDescription)")
//                    }
//                }
//            }
//        }
    }
    
    
    @IBAction func nextTapped(_ sender: UIBarButtonItem) {
     //   let phone = self.tf_phone.text!
     //   let alternatePhone = self.tf_alternate_phone.text!
        let addressLine1 = self.tf_address_line1.text!
        let addressLine2 = self.tf_address_line2.text!
        let city = self.tf_city.text!
        let state = self.tf_state.text!
        let passcode = self.tf_passcode.text!
        let country = self.tf_country.text!

//        if phone.isEmpty == true {
//            self.didShowAlert(title: "Phone required", message: "Please type your Phone number")
//           // AlertView.sharedInsance.showInfo(title: "Phone Required!", message: "Please add your Phone number")
//        }else
        if addressLine1.isEmpty == true {
           self.didShowAlert(title: "", message: "Address can't be blank")
          //   AlertView.sharedInsance.showInfo(title: "", message: "Address can't be blank")
        }else if addressLine1.count < 10 {
            self.didShowAlert(title: "", message: "Please enter minimum of 10 characters")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Address Line1 should be minimum 10 charecters")
        }else if city.isEmpty == true {
            self.didShowAlert(title: "", message: "City can't be blank")
           //  AlertView.sharedInsance.showInfo(title: "", message: "City can't be blank")
        }else if city.count < 3 {
             self.didShowAlert(title: "", message: "Minimum 3 characters required")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Please enter a valid city name")
        }else if state.isEmpty == true {
            self.didShowAlert(title: "", message: "State can't be blank")
           //  AlertView.sharedInsance.showInfo(title: "", message: "State can't be blank")
        }else if state.count < 3 {
             self.didShowAlert(title: "", message: "Minimum 3 characters required")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Please enter a valid state name")
        }else if passcode.isEmpty == true {
            self.didShowAlert(title: "", message: "Pincode can't be blank")
          //   AlertView.sharedInsance.showInfo(title: "", message: "Pincode can't be blank")
        }else if passcode.count < 6 {
             self.didShowAlert(title: "", message: "Pincode can't be blank")
           // AlertView.sharedInsance.showInfo(title: "", message: "Please enter a valid pincode")
        }else if country.isEmpty == true {
            self.didShowAlert(title: "", message: "Country can't be blank")
           // AlertView.sharedInsance.showInfo(title: "", message: "Country can't be blank")
        }else {

            self.shippinAddressModal = ShippinAddress(address1: addressLine1, address2: addressLine2, city: city, state: state, country: country, pincode: passcode, phone:  self.userPhoneNum)
            
            self.shippingAddress = """
                                    \(addressLine1), \(addressLine2)
                                    \(city), \(state), \(passcode)
                                    \(country),
                                   """
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_PLACE_ORDER, sender: sender)
        }
    }
    
    @IBAction func countryTapped(_ sender: UIButton) {
        let storyBoard = storyboard?.instantiateViewController(withIdentifier: "Countries") as! UINavigationController
        self.present(storyBoard, animated: true, completion: nil)
    }
    
    /**
     Unwind segue action
     */
    @IBAction func unwindToAddress(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func contactInfoTapped(_ sender: UIButton) {
      //  let phone = self.tf_phone.text!
       // let alternate_phone = self.tf_alternate_phone.text!
        let addressLine1 = self.tf_address_line1.text!
        let addressLine2 = self.tf_address_line2.text!
        let alternateContact = self.tf_alternateContactNumber.text!
        let city = self.tf_city.text!
        let state = self.tf_state.text!
        let passcode = self.tf_passcode.text!
        let country = self.tf_country.text!
        let address2 = ""
        
//        if phone.isEmpty == true {
//             self.didShowAlert(title: "Phone required", message: "Please type your Phone number")
//         //   AlertView.sharedInsance.showInfo(title: "Phone Required!", message: "Please type your Phone number")
//        }else
        if addressLine1.isEmpty == true {
            self.didShowAlert(title: "", message: "Address can't be blank")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Address can't be blank")
        }else if addressLine1.count < 10 {
            self.didShowAlert(title: "Address Line 1", message: "Minimum 10 characters required")
            //  AlertView.sharedInsance.showInfo(title: "", message: "Address can't be blank")
        }else if city.isEmpty == true {
             self.didShowAlert(title: "", message: "City can't be blank")
           // AlertView.sharedInsance.showInfo(title: "", message: "City can't be blank")
        }else if state.isEmpty == true {
              self.didShowAlert(title: "", message: "State can't be blank")
           // AlertView.sharedInsance.showInfo(title: "", message: "State can't be blank")
        }else if passcode.isEmpty == true {
            self.didShowAlert(title: "", message: "Pincode can't be blank")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Pincode can't be blank")
        }else if country.isEmpty == true {
             self.didShowAlert(title: "", message: "Country can't be blank")
           // AlertView.sharedInsance.showInfo(title: "", message: "Country can't be blank")
        }else if city.count < 3 {
             self.didShowAlert(title: "City", message: "Minimum three characters required")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Please enter a valid city name")
        }else if state.count < 3 {
             self.didShowAlert(title: "State", message: "Minimum three characters required")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Please enter a valid state name")
        }else if passcode.count != 6 {
             self.didShowAlert(title: "Pincode", message: "Should be 6 digits")
          //  AlertView.sharedInsance.showInfo(title: "", message: "Please enter a valid pincode")
        }else {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
              //  AlertView.sharedInsance.showFailureAlert(title: "Network Error!", message:AlertMessages.NETWORK_ERROR)
                return
            }
            
            var pincode = ""
            if let pcode = passcode.convertStringToNumberalIfDifferentLanguage() {
                pincode = pcode.stringValue
            }
            
            
            startAnimating()
            let userData = "{\"\(Keys.KEY_MOBILE_NO)\":\"\(self.userPhoneNum)\",\"\(Keys.KEY_ALTERNATE_CONTACT_NO)\":\"\(alternateContact)\" ,\"\(Keys.KEY_ADDRESS_1)\":\"\(addressLine1)\" ,\"\(Keys.KEY_ADDRESS_2)\":\"\(addressLine2)\" ,\"\(Keys.KEY_CITY)\":\"\(city)\" ,\"\(Keys.KEY_STATE)\":\"\(state)\" ,\"\(Keys.KEY_PIN_CODE)\":\"\(pincode)\" ,\"\(Keys.KEY_COUNTRY_ID)\":\"\(countryID!)\"}"
            
            
            var request = URLRequest(url: URL(string: AppURLS.URL_UserProfile)!)
            print("profileupdate url:==>\(AppURLS.URL_UserProfile)")
            print("Registration Data:==>\(userData)")
            
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.PATCH
            request.httpBody = userData.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                      //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                            })
                            print("performing error handling update user profile:\(resultJSON)")
                        }
                        else {
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 200 && status == "success"
                            {
                                print("Successfully updated response:\(resultJSON)")
                                var prefix = ""
                                var first_name = ""
                                var middle_name = ""
                                var last_name = ""
                                var dob = ""
                                var gender = ""
                                var address1 = ""
                                var address2 = ""
                                var uCountryId = 356
                                var state = ""
                                var city = ""
                                var pincode = ""
                                var mobile = ""
                                var alternateContact = ""
                                var user_id :Int!
                                var id :Int!
                                var email = ""
                                
                                if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int
                                {
                                    user_id = userid
                                }
                                if let uid = data.object(forKey: Keys.KEY_ID) as? Int
                                {
                                    id = uid
                                }
                                if let prefixType = data.object(forKey: Keys.KEY_PREFIX) as? String
                                {
                                    prefix = prefixType
                                }
                                if let fName = data.object(forKey: Keys.KEY_FIRST_NAME) as? String
                                {
                                    first_name = fName
                                }
                                if let lname = data.object(forKey: Keys.KEY_LAST_NAME) as? String
                                {
                                    last_name = lname
                                }
                                if let mname = data.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                {
                                    middle_name = mname
                                }
                                if let dateob = data.object(forKey: Keys.KEY_DATE_OF_BIRTH) as? String
                                {
                                    dob = dateob
                                }
                                if let ugender = data.object(forKey: Keys.KEY_GENDER) as? String
                                {
                                    gender = ugender
                                }
                                if let add1 = data.object(forKey: Keys.KEY_ADDRESS_1) as? String
                                {
                                    address1 = add1
                                }
                                if let add2 = data.object(forKey: Keys.KEY_ADDRESS_2) as? String
                                {
                                    address2 = add2
                                }
                                if let cID = data.object(forKey: Keys.KEY_COUNTRY_ID) as? String
                                {
                                    uCountryId = Int(cID)!
                                    print("Updated country id : \(uCountryId)")
                                }
                                if let ustate = data.object(forKey: Keys.KEY_STATE) as? String
                                {
                                    state = ustate
                                }
                                if let ucity = data.object(forKey: Keys.KEY_CITY) as? String
                                {
                                    city = ucity
                                }
                                if let upincode = data.object(forKey: Keys.KEY_PIN_CODE) as? String
                                {
                                    pincode = upincode
                                }
        
                                if let ualtMob = data.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                {
                                    alternateContact = ualtMob
                                }
                                
                                if let emailID = data.object(forKey: Keys.KEY_EMAIL) as? String {
                                    email = emailID
                                }else {
                                    email = User.get_user_profile_default().email!
                                }
                                
                                if let umobile = data.object(forKey: Keys.KEY_MOBILE_NO) as? String
                                {
                                    mobile = umobile
                                }else {
                                    mobile = User.get_user_profile_default().phone!
                                }

                                
                                let userDetails = User(avatar: App.avatarUrl, user_id: user_id, prefix: prefix, middle_name: middle_name, phone: mobile,alternate_phone:alternateContact, address1: address1, address2: address2, city: city, state: state, pincode: pincode, id: id, first_name: first_name, last_name: last_name, email: email, gender: gender, dob: dob, fullname: "", country: uCountryId)
                                self.userDataModel = userDetails
                                User.store_user_profile_default(dataModel: userDetails)
                                
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    self.UpdateUIFields()
                                    self.tf_country.text = self.getCountryNameById(countryID: uCountryId)
                                    self.tf_country.text = self.getCountryNameById(countryID: self.userDataModel.country!)
                                    self.didShowAlert(title: "Success", message: AlertMessages.UPDATE_PROFILE)
                                   // AlertView.sharedInsance.showSuccessAlert(title: status, message: AlertMessages.UPDATE_PROFILE)
                                })
                            }
                            else
                            {
                                DispatchQueue.main.async(execute: {
                                    let errors = data.object(forKey: "errors") as! NSDictionary
                                    print("Error***=>\(errors)")
                                    self.stopAnimating()
                                })
                            }
                            
                            //   DispatchQueue.main.async(execute: {
                            //     self.getUserProfile()
                            //  })
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
            })
            task.resume()
        }
    }

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_PLACE_ORDER {
            let destVC = segue.destination as! PlaceOrderViewController
            destVC.shippingAddress = self.shippingAddress
            destVC.shippingModal = self.shippinAddressModal
        }
    }
    

}

extension ContactInfoViewController : UIScrollViewDelegate {
    
    
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            print(" you reached end of the table")
            ContactInfoViewController.delegate?.didScrollUp()
        }
    }
    
}

extension ContactInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         let charecterSet = CharacterSet.letters
//         if textField == self.tf_phone {
//            let newLength = (self.tf_phone.text?.count)! + string.characters.count - range.length
//            return newLength <= 10
//
//        }else
            if textField == self.tf_passcode{
            let newLength = (self.tf_passcode.text?.count)! + string.count - range.length
            return newLength <= 6
        }else if textField == self.tf_city {
            if string.rangeOfCharacter(from: charecterSet.inverted) != nil {
                return false
            }
        }else if textField == self.tf_state {
            if string.rangeOfCharacter(from: charecterSet.inverted) != nil {
                return false
            }
        }else if textField == self.tf_alternateContactNumber {
                
                let characterSet =  CharacterSet(charactersIn: "*#,;+")
                let replacement = string.rangeOfCharacter(from: characterSet) == nil
                let newLength = (self.tf_alternateContactNumber.text?.count)! + string.count - range.length
                let resultingString = newLength <= 10
                
                return resultingString && replacement
                
        }
        
        return true
    }
}

