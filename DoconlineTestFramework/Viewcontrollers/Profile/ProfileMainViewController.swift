//
//  ProfileMainViewController.swift
//  DocOnline
//
//  Created by dev-3 on 24/07/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
//import GoogleSignIn



protocol ProfileViewAlignDelegate {
    func didScrollUp()
    func didScrollDown()
}

class ProfileMainViewController: UIViewController ,NVActivityIndicatorViewable {

    
    /**
       Enum class for tab tapped
     */
    enum TabIndex : Int {
        ///case if personal info tab tapped
        case firstChildTab = 0
        ///case if contact info tab tapped
        case secondChildTab = 1
        ///case if family mem password info tab tapped
        case thirdChildTab = 2
        ///case if Helath Profile info tab tapped
        case fourthChildTab = 3
    }
    
    @IBOutlet var vw_gradient: UIView!
    @IBOutlet var iv_profileBackImage: UIImageView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iv_profile_pic: UIImageView!
    @IBOutlet var vw_contentView: UIView!
    @IBOutlet var LC_gradViewHeight: NSLayoutConstraint!
    @IBOutlet var bt_attachImage: UIButton!
    
    ///reference to the current view controller
    var currentViewController: UIViewController?
    
    ///viewcontroller object reference for personal info
    lazy var personalInfoChildTabVC: UIViewController? = {
        let personalInfoChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_PERSONAL_INFO_VC)
        return personalInfoChildTabVC
    }()
    
    ///viewcontroller object reference for contact info
    lazy var contactInfoChildTabVC : UIViewController? = {
        let contactInfoChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_CONTACT_INFO_VC)
        return contactInfoChildTabVC
    }()
    
    ///viewcontroller object reference for family members
    lazy var familyMembers : UIViewController? = {
        let familyMem = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_FAMILY_MEMBERS)
        return familyMem
    }()
    
//    ///viewcontroller object reference for change password
//    lazy var passwordChangeChildTabVC : UIViewController? = {
//        let passwordChangeChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_CHANGE_PASSWORD)
//        return passwordChangeChildTabVC
//    }()
    
    ///viewcontroller object reference for change password
    lazy var healthProfileChildTabVC : UIViewController? = {
        let healthProfileTab = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_HEALTH_PROFILE_VIEW)
        return healthProfileTab
    }()
    
    ///Data modal object reference of user
    var userDataModel : User!
    
    ///image picker reference
    let picker = UIImagePickerController()
    ///Instance to set picked image
    var pickedImage : UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Ask permissions
        didAskForCameraPermission()
        didAskForPhotoLibraryPermission()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileMainViewController.profile_pic_tapped(_:)))
        self.iv_profile_pic.addGestureRecognizer(tapGesture)
        
        self.iv_profile_pic.layer.borderColor = UIColor.white.cgColor
        self.iv_profile_pic.layer.borderWidth = 1
        self.iv_profile_pic.layer.cornerRadius = self.iv_profile_pic.frame.size.width / 2
        self.iv_profile_pic.clipsToBounds = true
        
        segmentedControl.selectedSegmentIndex = TabIndex.firstChildTab.rawValue
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
        
       //  getNamePrefixes()
       // getUserProfile()
        App.isFromPasswordSetOption = false
        UpdateUIFields()

        let vwGrad = GradientView()
        vwGrad.frame =  CGRect(x: 0.0, y: 0.0, width: self.vw_gradient.frame.size.width, height: self.vw_gradient.frame.size.height)
        vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.vw_gradient.addSubview(vwGrad)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
//       self.bt_attachImage.layer.cornerRadius = self.bt_attachImage.frame.size.width / 2
  //     self.bt_attachImage.clipsToBounds = true
    }

    func adjustViewHeightOnScroll(hideBackView:Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            if hideBackView {
                self.LC_gradViewHeight.constant = 50
            }else {
                self.LC_gradViewHeight.constant = 175
            }
            self.view.layoutSubviews()
        }) { (success) in
            self.iv_profile_pic.isHidden = hideBackView
        }
    }
    
    func didScrollUp() {
          print("^^^^^^^^ \n SWIPED UP \n^^^^^^^^")
        adjustViewHeightOnScroll(hideBackView: true)
    }
    
    func didScrollDown() {
          print("^^^^^^^^ \n SWIPED DOWN \n^^^^^^^^")
        adjustViewHeightOnScroll(hideBackView: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        PersonalInfoViewController.delegate = self
//        HealthProfileViewController.delegate = self
//        ContactInfoViewController.delegate = self
    }

    /**
      Method show action sheet for profi@objc le pic
     */
   @objc func profile_pic_tapped(_ sender:AnyObject) {
        print("Profile pic taped")
        let alert:UIAlertController=UIAlertController(title: "Select", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            if !self.checkCameraPermission()
            {
                //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
                self.didShowAlertForDeniedPermissions(message: "Please provide access for the camera to upload pictures")
                return
            }
            
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            if !self.checkLibraryPermission()
            {
                // self.didShowAlert(title: "Access denied!", message: "Please provide access to upload pictures from your Gallery")
                self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
                return
            }
            
            self.openGallary()
        }
        
        if App.avatarUrl.contains("default-avatar")
        {
            print("User didn't updated profile pic")
        }
        else
        {
            let removeAction = UIAlertAction(title: "Remove Pic", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.uploadProfilePic(type: 2, httpMethod: HTTPMethods.DELETE)
            }
            
             alert.addAction(removeAction)
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
       
        
    }
    
    /**
     Method performs request to get user profile
     */
     func getUserMainProfile() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            self.userDataModel = User.get_user_profile_default()
            self.UpdateUIFields()
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            //  startAnimating()
            let url = URL(string: AppURLS.URL_UserProfile)
            print("Profile URl==>\(url)")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.didShowAlert(title: "Sorry", message: "We couldn't get your profile right now. please try again later")
                       // AlertView.sharedInsance.showFailureAlert(title: "Sorry!", message: "we couldn't get your profile right now. please try again later")
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling get user profile:\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                if code == 200 && responseStatus == "success"
                                {
                                    var prefix = ""
                                    var first_name = ""
                                    var middle_name = ""
                                    var last_name = ""
                                    var dob = ""
                                    var gender = ""
                                    var address1 = ""
                                    var address2 = ""
                                    var uCountryId = 0
                                    var state = ""
                                    var city = ""
                                    var pincode = ""
                                    var mobile = ""
                                    var alternateContact = ""
                                    var user_id :Int!
                                    var id :Int!
                                    var email = ""
                                    
                                    if let emailID = data.object(forKey: Keys.KEY_EMAIL) as? String {
                                        email = emailID
                                    }
                                    
                                    if let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int
                                    {
                                        user_id = userid
                                    }
                                    if let uid = data.object(forKey: Keys.KEY_ID) as? Int
                                    {
                                        id = uid
                                    }
                                    if let prefixType = data.object(forKey: Keys.KEY_PREFIX) as? String
                                    {
                                        prefix = prefixType
                                    }
                                    if let fName = data.object(forKey: Keys.KEY_FIRST_NAME) as? String
                                    {
                                        first_name = fName
                                    }
                                    if let lname = data.object(forKey: Keys.KEY_LAST_NAME) as? String
                                    {
                                        last_name = lname
                                    }
                                    if let mname = data.object(forKey: Keys.KEY_MIDDLE_NAME) as? String
                                    {
                                        middle_name = mname
                                    }
                                    if let dateob = data.object(forKey: Keys.KEY_DATE_OF_BIRTH) as? String
                                    {
                                        dob = dateob
                                    }
                                    if let ugender = data.object(forKey: Keys.KEY_GENDER) as? String
                                    {
                                        gender = ugender
                                    }
                                    if let add1 = data.object(forKey: Keys.KEY_ADDRESS_1) as? String
                                    {
                                        address1 = add1
                                    }
                                    if let add2 = data.object(forKey: Keys.KEY_ADDRESS_2) as? String
                                    {
                                        address2 = add2
                                    }
                                    if let cID = data.object(forKey: Keys.KEY_COUNTRY_ID) as? Int
                                    {
                                        uCountryId = cID
                                    }
                                    if let ustate = data.object(forKey: Keys.KEY_STATE) as? String
                                    {
                                        state = ustate
                                    }
                                    if let ucity = data.object(forKey: Keys.KEY_CITY) as? String
                                    {
                                        city = ucity
                                    }
                                    if let upincode = data.object(forKey: Keys.KEY_PIN_CODE) as? String
                                    {
                                        pincode = upincode
                                    }
                                    
                                    if let umobile = data.object(forKey: Keys.KEY_MOBILE_NO) as? String
                                    {
                                        mobile = umobile
                                    }
                                    
                                    //else {
                                        
//                                        if let mobileStored = UserDefaults.standard.value(forKey: UserDefaltsKeys.MOBILE_NUMBER) as? String {
//                                            mobile = mobileStored
//                                        }
                                   // }
                                    
                                    if let ualtMob = data.object(forKey: Keys.KEY_ALTERNATE_CONTACT_NO) as? String
                                    {
                                        alternateContact = ualtMob
                                    }
                                    
                                   UserDefaults.standard.set(mobile, forKey: UserDefaltsKeys.MOBILE_NUMBER)
                                    
                                    let userDetails = User(avatar: App.avatarUrl, user_id: user_id, prefix: prefix, middle_name: middle_name, phone: mobile,alternate_phone:alternateContact, address1: address1, address2: address2, city: city, state: state, pincode: pincode, id: id, first_name: first_name, last_name: last_name, email: email, gender: gender, dob: dob, fullname: "", country: uCountryId)
                                    self.userDataModel = userDetails
                                    User.store_user_profile_default(dataModel: userDetails)
                                    
                                    DispatchQueue.main.async(execute: {
                                        self.UpdateUIFields()
                                        //self.stopAnimating()
                                    })
                                    
                                }else {
                                    print("Error while fetching data")
                                }
                            }
                        }
                        catch _
                        {
                            
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON")
                        }
                    }
                }
                
            }).resume()
            
            // Bounce back to the main thread to update the UI
        }
    }

    /**
     Method updates ui views
     */
    func UpdateUIFields()  {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
                self.userDataModel  = User.get_user_profile_default()
                print("status:\(UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS))  User First name :\(self.userDataModel.first_name!)")
            }
            
            if App.avatarUrl.isEmpty != true {
                let url = URL(string: App.avatarUrl)
                self.iv_profile_pic.kf.setImage(with: url)
            }
        }
    }

    ///Opens the camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            self .present(picker, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    
    //Opens the gallery
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(picker, animated: true, completion: nil)
        }
    }

    /**
     Method performs request to update user profile pic
     */
    func uploadProfilePic(type:Int , httpMethod:String) {
        let bound = generateBoundaryString()
        startAnimating()
        var request = URLRequest(url: URL(string:  AppURLS.URL_UserAvatar )!)
        print("profileupdate url:==>\(AppURLS.URL_UserAvatar)")
        //  print("Registration Data:==>\(httpBody)")
        //  request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue("multipart/form-data; boundary=\(bound)", forHTTPHeaderField: "Content-Type")
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = httpMethod
        
        if type == 1 {
            request.httpBody = createBodyWithImage(parameters: [self.pickedImage], boundary: bound) as Data
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                  //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
                print("Error==> : \(error.localizedDescription)")
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                
                if type == 2 && httpResponse.statusCode == 204 {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.iv_profile_pic.image = UIImage(named: "Default-avatar")
                        App.avatarUrl = BaseUrl + "images/default-avatar.jpg"
                        self.didShowAlert(title: "Success", message: "Profile pic removed.")
                       // AlertView.sharedInsance.showSuccessAlert(title: "Success!", message: "Profile pic removed")
                    })
                }
                else
                {
                //if you response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling upload profile pic :\(resultJSON)")
                    }
                    else {
                        print("response :=\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                        
                        if code == 200 && status == "success"
                        {
                            print("Successfully updated profile pic")
                            DispatchQueue.main.async(execute: {
                                App.avatarUrl = data.object(forKey: Keys.KEY_AVATAR_URL) as! String
                                self.iv_profile_pic.image =  self.pickedImage
                                self.stopAnimating()
                                self.didShowAlert(title: "Success", message: "Updated profile pic")
                              //  AlertView.sharedInsance.showSuccessAlert(title: status, message: "Profile pic updated")
                            })
                            //shpuld change the profile pic after uploaded
                        }
                        else
                        {
                            //have to hande error response
                            let errors = data.object(forKey: "errors") as! NSDictionary
                            print("Error***=>\(errors)")
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                            })
                        }
                    }
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    //                    DispatchQueue.main.async(execute: {
                    //                        self.didShowAlert(title: "", message: error.localizedDescription)
                    //                    })
                    print("Received not-well-formatted JSON : \(error.localizedDescription)")
                }
              }
            }
        })
        task.resume()
    }
    
    ///creates the body with parameters for image uploading
    func createBodyWithImage(parameters: [UIImage],boundary: String) -> NSData {
        let body = NSMutableData()
        
        if parameters.count != 0 {
            var i = 0;
            for image in parameters {
                let filename = "profile.jpeg"   //should change id to app id
                let data = image.jpegData(compressionQuality: 0.4);
                let mimetype = mimeTypeForPath(path: filename)
                let key = "avatar"
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                body.append(data!)
                body.appendString(string: "\r\n")
                i += 1;
            }
            
        }
        body.appendString(string: "--\(boundary)--\r\n")
        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
        return body
    }

    @IBAction func logutTapped(_ sender: UIBarButtonItem) {
        

        let alert = UIAlertController(title: "Are you sure?", message: "you want to logout", preferredStyle: UIAlertController.Style.alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in

            UserDefaults.standard.set(App.apvoip_token, forKey: UserDefaltsKeys.PREVIOUS_APVOIP_TOKEN)
            UserSession.sharedInstace.makePresentAccessTokenToPrevious()
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.DID_USER_LOGED_OUT)
            UserSession.sharedInstace.logourUser(accessToken: App.getUserAccessToken(), apVoipToken: App.apvoip_token)
            
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_NAME)
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_HAS_LOGIN_KEY)
            UserDefaults.standard.set(false, forKey:  UserDefaltsKeys.KEY_HEALTH_STATUS)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.TOKENTYPE)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.ACCESS_TOKEN)
            UserDefaults.standard.set(false, forKey: UserDefaltsKeys.KEY_TOKEN_REGISTRATION)
            UserDefaults.standard.set(false,forKey: UserDefaltsKeys.KEY_USER_STATUS)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.MOBILE_NUMBER)
            UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_LOGGED_USER_EMAIL)
            //  UserDefaults.standard.removeObject(forKey: UserDefaltsKeys.KEY_USER_PHONE)
            
             User.remove_user_profile_default()
            
            App.user_full_name = ""
            App.userEmail = ""
            App.avatarUrl = ""
            App.imagesURLString.removeAll()
            App.ImagesArray.removeAll()
            App.bookingAttachedImages.removeAll()
            App.subscription_details = nil
            App.appointmentsList.removeAll()
            
            App.didUserSubscribed = false
            App.b2bUserType = ""
            App.userSubscriptionType = ""
            App.subscription_details = nil
            App.pending_subscription = nil
            App.canUpgradeSubscription = false
            App.notificationReadCount = 0
            UserDefaults.standard.set(App.notificationReadCount, forKey: UserDefaltsKeys.KEY_NOTIFI_READ_COUNT)
//            GIDSignIn.sharedInstance().signOut()
            
            RefreshTimer.sharedInstance.stopGlobalTimer()
            
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyBoard.instantiateViewController(withIdentifier: StoryBoardIds.ID_LOGIN_VIEW) as! LoginViewController
//            self.present(vc, animated: true, completion: nil)
            
        })
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Switching Tabs Functions
    @IBAction func switchTabs(_ sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParent()
        
        displayCurrentTab(sender.selectedSegmentIndex)
       // hideSideMenuView()
    }
    
    /**
     Method displays current selected tab view
     */
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    /**
     Method switches viewcontrollers on selected tab index
     */
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = personalInfoChildTabVC
            
        case TabIndex.secondChildTab.rawValue :
            vc = contactInfoChildTabVC
            
        case TabIndex.thirdChildTab.rawValue :
         //   App.isFromPasswordSetOption = false
            vc = familyMembers
        case TabIndex.fourthChildTab.rawValue :
            //   App.isFromPasswordSetOption = false
            vc = healthProfileChildTabVC
        default:
            return nil
        }
        
        return vc
    }

    @IBAction func attachImageTapped(_ sender: UIButton) {
        self.profile_pic_tapped(sender)
    }
    
    /**
     Unwind segue action
     */
    @IBAction func unwindToProfile(segue:UIStoryboardSegue) {

    }
    
    @IBAction func menuTapped(_ sender: UIBarButtonItem) {
       // toggleSideMenuView()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
extension ProfileMainViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        // self.iv_profilePic.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage //2
        self.dismiss(animated: true) {
            self.uploadProfilePic(type: 1, httpMethod: HTTPMethods.POST)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker canceled")
        self.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
