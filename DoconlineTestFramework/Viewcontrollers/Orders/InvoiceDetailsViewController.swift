//
//  InvoiceDetailsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 07/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import QuickLook
import PDFReader

class InvoiceDetailsViewController: UIViewController ,NVActivityIndicatorViewable , QLPreviewControllerDataSource{

    @IBOutlet weak var lb_invoice_number: UILabel!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var lb_payment_status: UILabel!
    @IBOutlet weak var lb_amount: UILabel!
    @IBOutlet weak var lb_amount_paid: UILabel!
    @IBOutlet weak var lb_amount_due: UILabel!
    @IBOutlet weak var lb_customer_name: UILabel!
    @IBOutlet weak var lb_customer_email: UILabel!
    @IBOutlet weak var lb_customer_contact: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lb_start_date: UILabel!
    @IBOutlet weak var lb_end_date: UILabel!
    
    @IBOutlet weak var lb_billing_period_title: UILabel!
    @IBOutlet weak var vw_black: UIView!
    @IBOutlet weak var lb_status_message: UILabel!
    @IBOutlet var lb_discountTitle: UILabel!
    @IBOutlet var lb_discountValue: UILabel!
    @IBOutlet var lc_discountHeight: NSLayoutConstraint!
    
    ///Instance of model billing details
    var billingDetails : MyBillings?
    ///pdf download url
    var pdfDownloadUrl = ""
    var invoiceID = ""
    var items = [SubscriptionPlan]()
    var urlList : [URL] = []
    ///google doc view base url
    var googleDocViewURL = "http://docs.google.com/gview?embedded=true&url="

    @IBOutlet weak var LC_tv_height_constraint: NSLayoutConstraint!
    
    ///linear bar instance reference
    private let linearBar: LinearProgressBar = LinearProgressBar()
    
    /**
     linear bar configuration
     */
    fileprivate func configureLinearProgressBar() {
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.linearBar.stopAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  linearBar.startAnimation()
        self.lb_status_message.isHidden = true
        self.vw_black.isHidden = false
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        getInvoiceDetails()
    }
    

    ///fetches the invoice details
    func getInvoiceDetails() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        if let billing = billingDetails ,let id = billing.id {
            self.startAnimating()
            NetworkCall.performGet(url: AppURLS.URL_BILLINGS + "/\(id)") { (success, response, statusCode, error) in
                if !success {
                    _ = statusCode == nil ? false : self.check_Status_Code(statusCode: statusCode! , data: nil)
                    if let err = error {
                        print("Error while getting subscription details:\(err.localizedDescription)")
                    }
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        self.tableView.reloadData()
                    }
                }else {
                    if let jsonData = response {
                        if let data = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                            
                            if let downloadURL = data.object(forKey: Keys.KEY_DOWNLOAD_URL) as? String {
                                self.pdfDownloadUrl = downloadURL
                                print("PDF Download url:\(downloadURL)")
                            }
                            
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.vw_black.isHidden = true
                                self.tableView.reloadData()
                                self.showDetails()
                            }
                        }
                    }
                }
            }
        }
    }
    

    ///Updates the UI on fetch
    func showDetails() {
        if let billing = billingDetails {
            self.lb_invoice_number.text = billing.id
            self.lb_date.text = getFormattedDateAndTime(dateString: self.getDateStringFromTimeStamp(timeStamp: Double(billing.issuedAt)).1)
            
            if billing.paymentStatus.lowercased().isEqual("paid") {
                 self.lb_payment_status.textColor = UIColor.init(hexString: "#32CD32")
            }else if billing.paymentStatus.lowercased().isEqual("issued"){
                 self.lb_payment_status.textColor = UIColor.init(hexString: "#fda64c")
            }else {
                 self.lb_payment_status.textColor = UIColor.init(hexString: "#b1d489")
            }
            
            self.lb_payment_status.text = billing.paymentStatus.uppercased()
            //   print("Billing started at:\(billing.bilingStartedAt) Billing Ended At:\(billing.billingEndAt)")
            
            if let billingStart = billing.bilingStartedAt , let billingEnd = billing.billingEndAt {
                print("Billing started at:\(billingStart) Billing Ended At:\(billingEnd)")
                if billingStart == 0  ||  billingEnd == 0{
                    self.lb_start_date.isHidden = true
                    self.lb_billing_period_title.isHidden = true
                }else {
                    let startDate = self.getDateStringFromTimeStamp(timeStamp: billing.bilingStartedAt).1
                    let endDate = self.getDateStringFromTimeStamp(timeStamp: billing.billingEndAt).1
                    print("***Start date:\(startDate) end date:\(endDate)")
                   
                    
                    self.lb_start_date.isHidden = false
                    self.lb_billing_period_title.isHidden = false
                    self.lb_start_date.text = self.getFormattedDate(dateString: startDate) + " to " + self.getFormattedDate(dateString: endDate)
                }
            }else {
                  print("Noooooo")
                self.lb_start_date.isHidden = true
                self.lb_billing_period_title.isHidden = true
            }
          
            
            var amount = "00.00"
            var paidamount = "00.00"
            var dueamount = "00.00"
            
            if billing.amount != 0 {
                amount = String(billing.amount)
                amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
            }
            
            if billing.amountPaid != 0 {
                paidamount = String(billing.amountPaid)
                paidamount.insert(contentsOf: ".", at: paidamount.index(paidamount.endIndex, offsetBy: -2))
            }
            
            if billing.amountDue != 0 {
                dueamount = String(billing.amountDue)
                dueamount.insert(contentsOf: ".", at: dueamount.index(dueamount.endIndex, offsetBy: -2))
            }else {
                dueamount = "0.00"
            }
            
            if !billing.appliedCouponDiscount.isEmpty && !billing.appliedCouponDiscount.isEqual("0") {
                var discountApplied = billing.appliedCouponDiscount
                discountApplied.insert(contentsOf: ".", at: discountApplied.index(discountApplied.endIndex, offsetBy: -2))
                self.lb_discountValue.text = "₹" + discountApplied
                self.lc_discountHeight.constant = 18
            }else {
                self.lc_discountHeight.constant = 0
            }
            
            self.lb_discountTitle.isHidden = billing.appliedCouponDiscount.isEmpty || billing.appliedCouponDiscount.isEqual("0")
            self.lb_discountValue.isHidden = billing.appliedCouponDiscount.isEmpty || billing.appliedCouponDiscount.isEqual("0")
        
           
            self.lb_amount.text = "₹" + amount
            self.lb_amount_paid.text = "₹" + paidamount
            self.lb_amount_due.text = "₹" + dueamount
    
            self.lb_customer_name.text = billing.customerName
            self.lb_customer_email.text = billing.customerEmail
            
         //   print("Customer name:\(billing.customerName) Email:\( billing.customerEmail) plans items:\(billing.subscribedPlan.count)")
            
            self.items = billing.subscribedPlan
         //    print("Customer name:\(billing.customerName) Email:\( billing.customerEmail) plans items:\( self.items.count)")
           adjustTableView()
            
        //    print("Item Name:\(self.items[0].plan_name) invoice_IDD:\(billing.id)")
            self.invoiceID = billing.id
            self.linearBar.stopAnimation()
            self.vw_black.isHidden = true
        }else {
            
            self.lb_status_message.isHidden = false
            self.vw_black.isHidden = false
        }
    }
    
     ///Adjusts the tablew view height with the content
    func adjustTableView() {
        UIView.animate(withDuration: 0.5) {
            self.tableView.reloadData()
            self.LC_tv_height_constraint.constant = 100 + self.tableView.contentSize.height
            self.view.layoutIfNeeded()
        }
    }
 
    @IBAction func downloadTapped(_ sender: UIBarButtonItem) {
        if self.pdfDownloadUrl.isEmpty {
            self.didShowAlert(title: "", message: "PDF is not available to download.Please try again later")
            return
        }

     if let billing = billingDetails {
        let document = PDFDocument(url: URL(string: self.pdfDownloadUrl)! )
        let readerController = PDFViewController.createNew(with: document!, title: billing.id!, actionButtonImage: nil, actionStyle: .activitySheet, backButton: nil, isThumbnailsEnabled: false, startPageIndex: 1)
        navigationController?.pushViewController(readerController, animated: true)
     }
 
      // self.performSegue(withIdentifier:"PDFView",sender:sender)
    }
    
    //MARK  : QLPreview Datasource
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return urlList.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return urlList[index] as QLPreviewItem
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "PDFView"{
            if let destVC = segue.destination as? PdfViewController {
                destVC.pdfURl = self.pdfDownloadUrl
                destVC.invoice_id = self.invoiceID
            }
        }
    }
    

}

extension InvoiceDetailsViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.InvoicePlan, for: indexPath) as! InvoiceTableViewCell

        let plan = items[indexPath.row]
        
        cell.lb_plan_name.text = plan.name
        cell.lb_item_id.text = plan.description
        cell.lb_count.text = "#\(indexPath.row + 1)"
        var amount = "00.00"
        if plan.plan_amount != 0 && plan.plan_amount != nil {
            amount = String(plan.plan_amount!)
            amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
        }else {
            amount = "₹00.00"
        }
        cell.lb_plan_amount.text = "₹" + amount
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}







