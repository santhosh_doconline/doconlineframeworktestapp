//
//  OrderDetailsViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class OrderDetailsViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var tv_order_status : UITextView!
    @IBOutlet weak var tv_shipping_address : UITextView!
    @IBOutlet weak var lb_order_id : UILabel!
    @IBOutlet weak var lb_total_amount: UILabel!
    @IBOutlet weak var vw_back : UIView!
    @IBOutlet weak var LC_heigtOfTableView: NSLayoutConstraint!
    
    ///Model instance for order details
    var orderDetailsModal : MyOrders?
    ///selected order id from list
    var selected_order_id : Int?
    ///ordered items list
    var items = [OrderItems]()
    
    ///current status of the order
    var currentStatus = ""
    ///order time stamp
    var timeStamp = ""
    ///order id
    var order_ID = ""
    ///sub total amount
    var subTotalAmount = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrdersDetails()
        self.tableView.tableFooterView = UIView(frame:CGRect.zero)
        self.tableView.estimatedRowHeight = 180
        self.tableView.rowHeight = UITableView.automaticDimension
    }

    ///fetches the selected order details
    func getOrdersDetails() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
          // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: "Internet Connection Failure")
            return
            
        }
        startAnimating()
        self.vw_back.isHidden = true
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: AppURLS.URL_Orders_History + "/\(self.selected_order_id!)")
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.vw_back.isHidden = true
                })
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                        if !errorStatus {
                            print("performing error handling get order details:\(jsonData)")
                        }
                        else {
                            print("Orders response : \(jsonData)")
                            if let order = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    var address1 = ""
                                    var address2 = ""
                                    var deliveredon = ""
                                
                                if let medplus = order.object(forKey: Keys.KEY_MEDPLUS) as? NSDictionary {
                                    if let timeStamp = medplus.object(forKey: Keys.KEY_TIME_STAMP_C) as? String {
                                        self.timeStamp = timeStamp
                                    }
                                    
                                    if let currStatus = medplus.object(forKey: Keys.KEY_CURRENT_STATUS) as? String {
                                        self.currentStatus = currStatus
                                    }
                                    
                                    if let orderid = medplus.object(forKey: Keys.KEY_ORDER_ID) as? String {
                                        self.order_ID = orderid
                                    }
                                }

                                    let id = order.object(forKey: Keys.KEY_ID) as? Int
                                    let app_id = order.object(forKey: Keys.KEY_APPOINTMENT_ID) as? Int
                                    let userid = order.object(forKey: Keys.KEY_USER_ID) as? Int
                                    let medplusid = order.object(forKey: Keys.KEY_MEDPLUS_ID) as? String
                                    let orderid =  order.object(forKey: Keys.KEY_ORDER_ID_U) as? String
                                    let orderamount = order.object(forKey: Keys.KEY_ORDER_AMOUNT_U) as? String
                                    let pincode = order.object(forKey: Keys.KEY_PINCODE) as? String
                                
                                    if let add1 = order.object(forKey: Keys.KEY_ADDRESS_1) as? String {
                                        address1 = add1
                                    }
                                    if let add2 = order.object(forKey: Keys.KEY_ADDRESS_2) as? String {
                                        address2 = add2
                                    }
                                    if let delivery = order.object(forKey: Keys.KEY_DELIVERED_ON) as? String {
                                        deliveredon = delivery
                                    }
                                    let status = order.object(forKey: Keys.KEY_STATUS) as? Int
                                    let createdat = order.object(forKey: Keys.KEY_CREATED_AT) as? String
                                    let updated = order.object(forKey: Keys.KEY_UPDATED_AT) as? String
                                
                                
                                
                                let nxtPage = ""
                                let from = 0
                                let to = 0
                                
                                let orderDetails = MyOrders(id: id == nil ? 0 : id! , appointment_id: app_id == nil ? 0 : app_id! , user_id: userid == nil ? 0 : userid!, medplus_id: medplusid == nil ? "" : medplusid! , order_id: orderid == nil ? "" : orderid! , order_amount: String(orderamount == nil ? "" : orderamount!) , pincode: pincode == nil ? "" : pincode! , address1: address1, address2: address2, status: status == nil ? 0 : status! , deliverd_on: deliveredon, created_at: createdat ==  nil ? "\(Date())" : createdat!, updated_at: updated ==  nil ? "\(Date())" : updated!,from: from, last_page: 0, nextpageurl: nxtPage, to: to, total: 0)
                                self.orderDetailsModal = orderDetails
                                
                                
                                if let items = order.object(forKey: "items") as? [NSDictionary] {
                                    for item in items {
                                        var manf = ""
                                        var packsize = 0
                                        var discount = 0
                                        var product_type = ""
                                        var mrp = 0.0
                                        var avalQty  = ""
                                        
                                        let id = item.object(forKey: Keys.KEY_ID) as! Int
                                        let prescribedOrderID = item.object(forKey: Keys.KEY_PRESCRIPTION_ORDER_ID) as! Int
                                        let productid = item.object(forKey: Keys.KEY_PRODUCT_ID) as! String
                                        let name = item.object(forKey: Keys.KEY_NAME) as! String
                                        let qty = item.object(forKey: Keys.KEY_QTY) as! String
                                        let price = item.object(forKey: Keys.KEY_PRICE) as! String
                                        let prce = Double(price)
                                        let qtity = Double(qty)
                                        let total = prce! * qtity!
                                        self.subTotalAmount += total
                                        
                                        if let extradata = item.object(forKey: Keys.KEY_EXTRA_DATA) as? NSDictionary {
                                            if let manfacturer = extradata.object(forKey: Keys.KEY_MANUFACTERER) as? String {
                                                 manf = manfacturer
                                            }
                                            
                                            if let pcksize = extradata.object(forKey: Keys.KEY_PACK_SIZE) as? Int {
                                                packsize = pcksize
                                            }
                                           
                                            if let dscount = extradata.object(forKey: Keys.KEY_DISCOUNT_PERCENTAGE) as? Int {
                                                 discount = dscount
                                            }
                                            
                                            if let prodType = extradata.object(forKey: Keys.KEY_PRODUCT_FORM) as? String {
                                                product_type = prodType
                                            }
                                            
                                            if let mrpprce = extradata.object(forKey: Keys.KEY_MRP) as? NSNumber {
                                                 mrp = mrpprce.doubleValue
                                            }

                                            if let qty = extradata.object(forKey: Keys.KEY_AVAILABLE_QUANTITY) as? Int {
                                                avalQty = "\(qty)"
                                            }else {
                                                 avalQty = "0"
                                            }
                                           
                                        }
                                        
                                        let itemDetails = OrderItems(itemid: id, pre_orderid: prescribedOrderID, productid: productid, name: name, qty: qty, price: price, manufacture: manf, packsize: packsize, discount: discount, producform: product_type, mrp: mrp, availbleqty: avalQty)
                                        self.items.append(itemDetails)
                                        print("items==count:\(self.items.count)")
                                    }
                                }
                                
                                self.orderDetailsModal?.items = self.items
                                
                                DispatchQueue.main.async(execute: {
                                    self.vw_back.isHidden = false
                                    self.stopAnimating()
                                     self.updateUI()
                                    self.tableView.reloadData()
                                })
                            }
                        }
                    }
                    catch let error
                    {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON=>\(error.localizedDescription)")
                    }
                }
            }
        }).resume()
    }
    
    ///updates the UI on fetch
    func updateUI() {
        self.lb_order_id.text = "Order ID: " + self.order_ID
        self.tv_order_status.text = self.currentStatus
        self.tv_shipping_address.layer.cornerRadius = 8
        self.tv_order_status.layer.cornerRadius = 8
        if let order = self.orderDetailsModal {
            var mainAddress = "Date: \(self.timeStamp)\n" + order.address1!
            mainAddress += order.address2!.isEmpty ?  ", " + order.pincode! :  ", " + order.address2! + ", " + order.pincode!
            self.tv_shipping_address.text = mainAddress
            self.tableView.reloadData()
            
            UIView.animate(withDuration: 0.5, animations: {
                self.LC_heigtOfTableView.constant = self.tableView.contentSize.height + 5
                self.view.layoutIfNeeded()
            })
        }
        self.lb_total_amount.text =  String(format: "Grand Total: ₹%.2f", self.subTotalAmount)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrderDetailsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.ORDER, for: indexPath) as! OrderDetailsTableViewCell
        let orderItem = self.items[indexPath.row]
        if orderItem.productForm!.isEmpty == true {
            cell.lb_item_type.isHidden = true
            cell.lb_item_type.text = ""
        }else {
           cell.lb_item_type.isHidden = false
           cell.lb_item_type.text = "Type : \(orderItem.productForm!)"
        }
        
        let Price = Double(orderItem.price!)
        
        cell.lb_product_id.text = "Product ID: \(orderItem.product_id!)"
        cell.lb_medicine_name.text = orderItem.name!
        cell.lb_manufacturer.text = "Manufacturer: " + orderItem.manufaturer!
        cell.lb_pack_size.text = "Pack Size: \(orderItem.packsize!)"
        cell.lb_mrp.text = String(format: "₹%.2f", orderItem.mrp!)  //"₹\(orderItem.mrp!)"
        cell.lb_discount.text = "\(orderItem.discount!)"
        cell.lb_price.text = String(format: "₹%.2f", Price!)             //"₹" + orderItem.price!
        let quantity = Double(orderItem.qty!)
        cell.lb_quantity.text = "Quantity: \(orderItem.qty!)"
        let price = Double(orderItem.price!)
        cell.lb_total.text = String(format: "₹%.2f", quantity! * price!)   //"₹\(quantity! * price!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}




