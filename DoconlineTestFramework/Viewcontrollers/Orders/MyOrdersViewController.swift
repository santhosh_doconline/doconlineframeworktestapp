//
//  MyOrdersViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 29/09/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class MyOrdersViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var ordersTableView : UITableView!
    @IBOutlet var lb_noOrders: UILabel!
    
    var orders = [MyOrders]()
    ///count for starting notification
    var listShowingFromNTo = ""
    ///instance for on veiwload loads upcoming url
    var firstPageUrl = ""
    ///if appointments has next list url uses this instance
    var nextPageUrl = ""
    ///called when scrolling of list ends and set bool value
    var isScrollingFinished :Bool!
    ///Bool value to check list ended stop network request
    var isLoadingListItems:Bool = true
    ///instance to get total appointments count
    var totalAppListCount : Int!
 
    ///selected order id
    var selectedOrderID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

       isScrollingFinished = false
        firstPageUrl = AppURLS.URL_Orders_History
        getOrdersList()
        self.ordersTableView.tableFooterView = UIView(frame: CGRect.zero)

    }

    ///Used to fetch the orders list performing the rest call
    func getOrdersList() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
            
        }
        
        if isLoadingListItems == false
        {
            //self.didShowAlert(title: "Sorry", message: "List ended total count is\(self.appointmentsList.count)")
            print("orders list ended total count is\(self.orders.count)")
        }
        else
        {
        var productListUrl = ""
        
        if isScrollingFinished == false
        {
            productListUrl = firstPageUrl
            print("Products list url-->:\(productListUrl)")
        }
        else
        {
            productListUrl = orders[0].next_page_url!
            print("Products list url-->:\(productListUrl)")
            
            if productListUrl.isEmpty == true {
                print("No next page url")
                print("Total chats count:==> \(self.orders.count)")
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.ordersTableView.reloadData()
                })
                return
            }
        }
        
        startAnimating()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
         let url = URL(string: productListUrl)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.ordersTableView.reloadData()
                    self.didShowAlert(title: "", message: (error?.localizedDescription)!)
                  //  AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                })
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                        if !errorStatus {
                            print("performing error handling get appointments list:\(jsonData)")
                        }
                        else {
                            print("Orders response : \(jsonData)")
                            var nxtPage = ""
                            var from = 0
                            var to = 0
                            let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            let currentPage = data.object(forKey: Keys.KEY_CURRENT_PAGE) as! Int
                            let total = data.object(forKey: Keys.KEY_TOTAL) as! Int
                            let last_page = data.object(forKey: Keys.KEY_LAST_PAGE) as! Int
                            
                            print("Total : \(total) current:\(currentPage)")
                            
                            if let nepageurl = data.object(forKey: Keys.KEY_NEXT_PAGE_URL) as? String {
                                nxtPage = nepageurl
                            }
                            if let frm = data.object(forKey: Keys.KEY_FROM) as? Int {
                                from = frm
                            }
                            if let too = data.object(forKey: Keys.KEY_TO) as? Int {
                                to = too
                            }
                            
                            print("next page ur:\(nxtPage)")
                            
                            if let orders = data.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                if self.orders.count == total
                                {
                                    print("List ended")
                                    self.isLoadingListItems = false
                                }
                                else
                                {
                                    self.isLoadingListItems = true
                                    
                                    for order in orders {
                                        var deliveredon = ""
                                        
                                        if let id = order.object(forKey: Keys.KEY_ID) as? Int {
                                            if let orderid =  order.object(forKey: Keys.KEY_ORDER_ID_U) as? String {
                                                if let orderamount = order.object(forKey: Keys.KEY_ORDER_AMOUNT_U) as? Double {
                                                    if let pincode = order.object(forKey: Keys.KEY_PINCODE) as? String {
                                                        if let delivery = order.object(forKey: Keys.KEY_DELIVERED_ON) as? String {
                                                            deliveredon = delivery
                                                            if  let status = order.object(forKey: Keys.KEY_STATUS) as? Int {
                                                                if let createdat = order.object(forKey: Keys.KEY_CREATED_AT) as? String {
                                                                    let orderDetails = MyOrders(id: id, appointment_id: 0, user_id: 0, medplus_id: "", order_id: orderid , order_amount: String(orderamount) , pincode: pincode, address1: "", address2: "", status: status, deliverd_on: deliveredon, created_at: createdat, updated_at: "",from: from, last_page: last_page, nextpageurl: nxtPage, to: to, total: total)
                                                                    self.orders.append(orderDetails)
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                    self.ordersTableView.reloadData()
                                    if self.orders.count == 0 {
                                        self.lb_noOrders.isHidden = false
                                        self.ordersTableView.isHidden = true
                                    }else {
                                        self.lb_noOrders.isHidden = true
                                        self.ordersTableView.isHidden = false
                                    }
                                })
                            }
                        }
                    }
                    catch let error
                    {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON=>\(error.localizedDescription)")
                    }
                }
            }
        }).resume()
      }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_ORDER_DETAILS {
            let destVC = segue.destination as! OrderDetailsViewController
            destVC.selected_order_id = Int(self.selectedOrderID)
        }
    }
    

}

extension  MyOrdersViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.ORDER , for: indexPath) as! MyOrdersTableViewCell
        let order = self.orders[indexPath.row]
        let orderAmount = Double(order.order_amount!)
        cell.lb_order_id.text = "Order ID : " +  order.order_id!
        cell.lb_amount.text = String(format: "Amount : ₹%.2f", orderAmount!)      
        let date = getFormattedDateAndTime(dateString: order.created_at!)
        cell.lb_date.text = "Date : " + date
        
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order = self.orders[indexPath.row]
        self.selectedOrderID = "\(order.id!)"
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ORDER_DETAILS, sender: self)
    }
    
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 62
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
}

extension MyOrdersViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        if !decelerate {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= scrollView.contentSize.height {
                isScrollingFinished = true
                getOrdersList()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Scrolling did end decelerating method called")
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            isScrollingFinished = true
            getOrdersList()
        }
    }
}



