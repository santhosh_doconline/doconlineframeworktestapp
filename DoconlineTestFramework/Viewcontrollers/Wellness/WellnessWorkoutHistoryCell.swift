//
//  WellnessWorkoutHistoryCell.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 24/06/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit

class WellnessWorkoutHistoryCell: UITableViewCell {
    
    
    @IBOutlet weak var workoutName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cellSetup(workout: WellnessTrackerInner){
        workoutName.text = workout.outlet_name
        
    }

}
