//
//  WellnessClassDetailsVC.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 26/04/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
import MapKit

class WellnessClassDetailsVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var workoutImg: UIImageView!
    @IBOutlet weak var imageIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgHeightCnst: NSLayoutConstraint!
    @IBOutlet weak var outletTitle: UILabel!
    @IBOutlet weak var importantInfoContainer: UIView!
    @IBOutlet weak var bookBtn: UIButton!
    @IBOutlet weak var fitmeSeats: UILabel!
    @IBOutlet weak var bookSlotBtn: UIButton!
    
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var scrollContainer: UIView!
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var bookSlotContainer: UIView!
    @IBOutlet weak var seatLeftContainer: UIView!
    @IBOutlet weak var slotContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var seatLeftContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var bookBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var editBtnWidth: NSLayoutConstraint!
    
    var workoutDetailId: String = ""
    var outletName: String = ""
    var classFordate = ""
    var distance = ""
//    var dateShowing = ""
    
    var slotExpectedTime = ""
    
    var latitude : CLLocationDegrees!
    var longitude : CLLocationDegrees!
    
    let locationManager = CLLocationManager()
    
    var classDetail: WellnessClassDetails?
    var classDetailsWrapper : WellnessClassDetailsWrapper?
    
    
    var indicatorView : UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .whiteLarge
        indicator.color = UIColor(red: 109/255, green: 191/255, blue: 0, alpha: 1.0)
        indicator.startAnimating()
        return indicator
    }()
    
    var indicatorView1 : UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .white
        indicator.color = UIColor(red: 109/255, green: 191/255, blue: 0, alpha: 1.0)
        indicator.startAnimating()
        return indicator
    }()
    
    var backgroundView : UIView = {
        let bg_view = UIView()
        bg_view.backgroundColor = .white
        return bg_view
    }()
    
    var backgroundView1 : UIView = {
        let bg_view = UIView()
        bg_view.backgroundColor = .white
        return bg_view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        editBtnWidth.constant = 0
        
        
        imgBack.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        imgBack.layer.shadowColor = UIColor.black.cgColor
        imgBack.layer.shadowRadius = 4
        imgBack.layer.shadowOpacity = 0.25
        imgBack.layer.masksToBounds = false;
        imgBack.clipsToBounds = false;
        
        backgroundView.frame = self.view.frame
        backgroundView.addSubview(indicatorView)
        
        // Ask for Authorisation from the User.
//        self.locationManager.requestAlwaysAuthorization()
//
//        // For use in foreground
//        self.locationManager.requestWhenInUseAuthorization()
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        }

        workoutImg.layer.cornerRadius = 10
        
        self.outletTitle.text = outletName
        self.navigationController?.navigationBar.isHidden = true
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        bookBtn.backgroundColor = Theme.buttonBackgroundColor
        getClassDetails()
    }
    
    
 
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
//        let sourceLatitude = locValue.latitude
//        let sourceLongitude = locValue.longitude
//        locationManager.stopUpdatingLocation()
//        let coords = classDetail?.GoogleMapsCoordiates.split(separator: ",")
//        let destLatitude: CLLocationDegrees = ("\(coords![0])" as NSString).doubleValue
//        let destLongitude: CLLocationDegrees = ("\(coords![1])" as NSString).doubleValue
//
////        if (UIApplication.shared.canOpenURL(URL(string:"http://maps.google.com/")!))
////        {
//            UIApplication.shared.open(URL(string: "http://maps.google.com/?daddr=\(destLatitude),\(destLongitude)&saddr=\(sourceLatitude),\(sourceLongitude)")!, options: [:], completionHandler: nil)
//
////        } else
////        {
////            UIApplication.shared.open(URL(string: "http://maps.apple.com/?daddr=\(destLatitude),\(destLongitude)&saddr=\(sourceLatitude),\(sourceLongitude)")!, options: [:], completionHandler: nil)
////        }
//
//    }
    
    override func viewWillLayoutSubviews() {
        
        indicatorView.center = backgroundView.center
//        indicatorView1.center = backgroundView1.center
    }
    
    
    
    func getClassDetails(){
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        self.view.addSubview(backgroundView)
        
//        let tokens = App.getWellnessUserAccessToken()
//        let token_type = tokens.token_type
//        let access_token = tokens.access_token
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let classDetailURL = AppURLS.URL_WELLNESS_WORKOUT_DETAILS
        let url = URL(string: "\(classDetailURL)\(workoutDetailId)")
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.GET
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    //                    self.vw_activity_indicator.isHidden = true
                    //                    self.activityIndicator.stopAnimating()
                    self.backgroundView.removeFromSuperview()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            print("res wellness \((response as! HTTPURLResponse).statusCode)")
            let statusCode = (response as! HTTPURLResponse).statusCode
            print("response \(response)")
            
            
            
            if let response = response, statusCode == 200{
                do{
                    let resultsData = String(data: data!, encoding: String.Encoding.utf8)
                    print("resultsData \(resultsData)")
                    let classDetailsWrapper = try? JSONDecoder().decode(WellnessClassDetailsWrapper.self, from: data!) as WellnessClassDetailsWrapper
                    if let tempClassDetail = (classDetailsWrapper?.data){
                        self.classDetail = tempClassDetail
                        self.imageLoader(workoutID: (self.classDetail?.WorkoutID)!)
                        DispatchQueue.main.async {
                            self.workoutName.text = self.classDetail!.WorkoutName
                            self.address.text = self.classDetail!.StreetAddress+","+self.classDetail!.StreetAddress2
                            self.dateTime.text = (self.classDetail!.FromTime)+" To "+(self.classDetail!.ToTime)
                            self.fitmeSeats.text = "Available seats - \(self.classDetail!.FitmeSeat)"
                            self.doHandleBookBtn()
                            if let desc = self.classDetail!.Description, !desc.isEmpty{
                                self.importantInfoContainer.isHidden = false
                                self.desc.text = desc.replacingOccurrences(of: "<br/>", with: "")
                            }
                            else{
                                self.importantInfoContainer.isHidden = true
                                self.desc.text = ""
                            }
                            self.backgroundView.removeFromSuperview()
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            self.backgroundView.removeFromSuperview()
                        }
                    }
                    
                }
            }else{
                DispatchQueue.main.async {
                    self.backgroundView.removeFromSuperview()
                }
                
                self.checkOnlyStatus(statusCode: statusCode)
            }
        }).resume()
    }
    
    func imageLoader(workoutID: Int){
        self.workoutImg.loadImage(workoutID: workoutID, indicator: imageIndicator)
    }
    
    @IBAction func chooseSlot(_ sender: UIButton) {
        self.showTimeSlot()
    }
    
    
    @IBAction func bookClass(_ sender: UIButton)
    {
        if self.slotExpectedTime.isEmpty{
            self.didShowAlert(title: "Alert!!", message: "Please schedule your slot")
            return
        }
        
        let alert = UIAlertController(title: "Book Workout", message: "Are you sure you want to book workout", preferredStyle: UIAlertController.Style.alert)
        let okay = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.bookSaveInLocalServer(workoutDetailsId: self.classDetail!.WorkoutDetailsID, fromTime: self.classDetail!.FromTime)
//            self.showTimeSlot()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okay)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func doHandleBookBtn(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let today = formatter.string(from: Date())
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        if today == classFordate{
            let scheduleTime = formatter.date(from: classFordate+" "+self.classDetail!.ToTime)
            let diff = Calendar.current.dateComponents([.minute], from: Date(), to: scheduleTime!).minute
            
            
                bookBtn.isHidden = true
                bookBtnHeight.constant = 0
                slotContainerHeight.constant = 0
                seatLeftContainerHeight.constant = 0
                bookSlotContainer.isHidden = true
                seatLeftContainer.isHidden = true
            //change diff! >= (min) to disable book btn before few minutes of end time
             if diff! >= 0 && self.classDetail!.FitmeSeat > 0{
                bookBtn.isHidden = false
                bookBtnHeight.constant = 40
                slotContainerHeight.constant = 30
                seatLeftContainerHeight.constant = 30
                bookSlotContainer.isHidden = false
                seatLeftContainer.isHidden = false
            }
        }
    }
    
    
    //Show TimePicker
    func showTimeSlot(){
        
        var minDateStr = ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        
        let f1 = DateFormatter()
        f1.dateFormat = "yyyy-MM-dd"
        let today = f1.string(from: Date())
        
        if today == classFordate{
            let scheduleTime = formatter.date(from: classFordate+" "+self.classDetail!.FromTime)
            let diff = Calendar.current.dateComponents([.minute], from: Date(), to: scheduleTime!).minute
            
            if diff! < 0{
                minDateStr = formatter.string(from: Date())
            }else{
                minDateStr = classFordate+" "+self.classDetail!.FromTime
            }
        }else{
            minDateStr = classFordate+" "+self.classDetail!.FromTime
        }
        
        let minimumDate = formatter.date(from: minDateStr)
        let maximumDate = formatter.date(from: classFordate+" "+self.classDetail!.ToTime)
        
        DatePickerDialog().show("Choose time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate:minimumDate, maximumDate:maximumDate, datePickerMode: .time) {
            (date) -> Void in
            if let date = date{
                formatter.dateFormat = "h:mm a"
                let slotTime = formatter.string(from: date)
                print("selected date is \(slotTime)")
                
                self.slotExpectedTime = slotTime
                self.editBtnWidth.constant = 30
                self.bookSlotBtn.setTitleColor(UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1.0), for: UIControl.State.normal)
                self.bookSlotBtn.isUserInteractionEnabled = false
                self.bookSlotBtn.setTitle("Selected slot: "+slotTime, for: UIControl.State.normal)
//                self.bookSaveInLocalServer(workoutDetailsId: self.classDetail!.WorkoutDetailsID, fromTime: self.classDetail!.FromTime)
            }
        }
    }
    
    func bookSaveInLocalServer(workoutDetailsId: Int, fromTime: String){
//        showTimeSlot()
        
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        indicatorView.center = self.view.center
        self.view.addSubview(indicatorView)
        
//        let tokens = App.getWellnessUserAccessToken()
//        let token_type = tokens.token_type
//        let access_token = tokens.access_token
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let bookURL = AppURLS.URL_WELLNESS_BOKKING_LOCAL
        let url = URL(string: bookURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let today = formatter.string(from: Date())
        let expected_time = self.slotExpectedTime.split(separator: " ")
//        let expected_time = self.classDetail!.FromTime.split(separator: " ")
        
        let postData = "\(Keys.KEYS_WELLNESS_WORKOUT_ID)=\(self.classDetail!.WorkoutDetailsID)&\(Keys.KEYS_WELLNESS_EXPECTED_TIME)=\(expected_time[0]) \(expected_time[1].lowercased())&\(Keys.KEYS_WELLNESS_BOOK_DATE)=\(classFordate)&\(Keys.KEYS_WELLNESS_DISTANCE)=\(self.distance)" //\(self.classDetail!.FromTime)
        
        print("booking data \(postData)")
        request.httpBody = postData.data(using: String.Encoding.utf8)
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    //                    self.vw_activity_indicator.isHidden = true
                    //                    self.activityIndicator.stopAnimating()
                    self.indicatorView.removeFromSuperview()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    let res = String(data: data!, encoding: String.Encoding.utf8)
                    print("book res err \(res!)")
                })
            }
//            print("res wellness-LOCAL \((response as! HTTPURLResponse).statusCode)")
            print("res wellness-LOCAL \(response as? HTTPURLResponse)")
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if let response = response, statusCode == 200{
                do{
                    let res = String(data: data!, encoding: String.Encoding.utf8)
                    print("book res \(res!)")
                    let bookClass = try? JSONDecoder().decode(WellnessBookClass.self, from: data!) as! WellnessBookClass
                    DispatchQueue.main.async {
                        self.indicatorView.removeFromSuperview()
                        
                            let alert = UIAlertController(title: "Success!!", message: bookClass?.message, preferredStyle: UIAlertController.Style.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {
                                action in
//                                for vc in (self.navigationController?.viewControllers)!{
//                                    if vc is HomeViewController{
//                                        self.navigationController?.popToViewController(vc, animated: true)
//                                    }
//                                }
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(okAction)
                            
                            self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
            }else if statusCode == 422{
                let bookClass = try? JSONDecoder().decode(WellnessBookClass.self, from: data!) as! WellnessBookClass
                DispatchQueue.main.async {
                    self.indicatorView.removeFromSuperview()
                }
                self.didShowAlert(title: "Sorry!!", message: (bookClass?.message)!)
//                self.checkOnlyStatus(statusCode: statusCode)
            }else{
                DispatchQueue.main.async {
                    self.indicatorView.removeFromSuperview()
                }
                self.checkOnlyStatus(statusCode: statusCode)
            }
        }).resume()
        
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func showMap(_ sender: UIButton) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
//        self.locationManager.startUpdatingLocation()
        let coords = classDetail?.GoogleMapsCoordiates.split(separator: ",")
        let destLatitude: CLLocationDegrees = ("\(coords![0])" as NSString).doubleValue
        let destLongitude: CLLocationDegrees = ("\(coords![1])" as NSString).doubleValue
        print("slat \(self.latitude) slong \(self.longitude) dlat \(destLatitude) dlong \(destLongitude)")
        UIApplication.shared.open(URL(string: "http://maps.google.com/?daddr=\(destLatitude),\(destLongitude)&saddr=\(self.latitude!),\(self.longitude!)")!, options: [:], completionHandler: nil)
    
    }
    
   

}
