//
//  NearestClassesCell.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 25/04/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
import SDWebImage

class NearestClassesCell: UITableViewCell {
    
    @IBOutlet weak var outletName: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var workoutImg: UIImageView!
    @IBOutlet weak var loaderIndicator: UIActivityIndicatorView!
    @IBOutlet weak var workoutContainer: UIView!
    @IBOutlet weak var extraIndicator: UIView!
    @IBOutlet weak var extraIndicatorCnstrnt: NSLayoutConstraint!
    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        print("layouter")
        imgWidth.constant = workoutImg.bounds.height
        if let workoutContainerView = workoutContainer, let extraViewer = extraIndicator{
        workoutContainer.addShadowLayer(offset: CGSize.init(width: 5.0, height: 5.0), color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), radius: 10.0, opacity: 0.6)
            
            
            workoutImg.clipsToBounds = true
            workoutImg.layer.cornerRadius = 10.0
        
        extraIndicator.clipsToBounds = true
        extraIndicator.layer.cornerRadius = 10.0
        if #available(iOS 11.0, *) {
            extraIndicator.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
//            extraIndicator.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        }
            extraIndicator.backgroundColor = Theme.buttonBackgroundColor
        }
    }
    
    
    
    
    
    func configureCell(classInfo: WellnessInnerData) {
        self.outletName.text = classInfo.len
        self.area.text = classInfo.a2+","+classInfo.c
        self.time.text = classInfo.ft+" To "+classInfo.tt
        self.distance.text = "\(classInfo.dist)km"
        self.workoutName.text = classInfo.cn
        self.workoutImg.loadImage(workoutID: classInfo.WorkoutID, indicator: loaderIndicator)
//        let imgURL = URL(string: AppURLS.URL_WELLNESS_WORKOUT_IMAGE+"100")
//
//            workoutImg.sd_setImage(with: imgURL!, completed: { (image, error, cache, url) in
//                DispatchQueue.main.async {
//                    print("inside img loader")
//                    self.loaderIndicator.stopAnimating()
//                }
//
//            })
//
        
    }
    
    func configureHistoryCell(classInfo: WellnessTrackerInner){
        self.outletName.text = classInfo.outlet_name
        self.area.text = classInfo.city
        
        self.time.text = (classInfo.booked_for_date != nil ? classInfo.booked_for_date! : "")+" "+(classInfo.expected_time != nil ? classInfo.expected_time! : "")
        self.distance.text = classInfo.distance != nil ? classInfo.distance!+"km" : ""
        self.workoutImg.loadImage(workoutID: Int(classInfo.workout_id != nil ? classInfo.workout_id! : "0")!, indicator: loaderIndicator)
        self.workoutName.text = classInfo.workout_name
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
