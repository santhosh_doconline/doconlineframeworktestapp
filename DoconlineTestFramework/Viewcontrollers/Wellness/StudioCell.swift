//
//  StudioCell.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 13/08/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit

class StudioCell: UITableViewCell {
    
    @IBOutlet weak var outletName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var workouts: UILabel!
    @IBOutlet weak var workoutContainer: UIView!
    @IBOutlet weak var extraIndicator: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        if let workoutContainerView = workoutContainer{
            workoutContainerView.addShadowLayer(offset: CGSize.init(width: 5.0, height: 5.0), color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), radius: 10.0, opacity: 0.6)
            //workoutImg.clipsToBounds = true
            //workoutImg.layer.cornerRadius = 10.0
            
            extraIndicator.clipsToBounds = true
            extraIndicator.layer.cornerRadius = 10.0
            if #available(iOS 11.0, *) {
                extraIndicator.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
                //            extraIndicator.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
            }
            extraIndicator.backgroundColor = Theme.buttonBackgroundColor
        }
    }
    
    func configCell(cellInfo: StudioInnerData){
        outletName.text = cellInfo.OutletName
        address.text = cellInfo.StreetAddress2+","+cellInfo.CityName
        distance.text = "\(cellInfo.Distance)km"
        workouts.text = ""//cellInfo.workouts.isEmpty ? "NA" : cellInfo.workouts
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
