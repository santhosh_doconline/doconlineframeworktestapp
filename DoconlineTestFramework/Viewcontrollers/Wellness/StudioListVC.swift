//
//  StudioListVC.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 13/08/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlacesSearchController

class StudioListVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var studioList: UITableView!
    @IBOutlet weak var lblSearch: FRHyperLabel!
    
    let locationManager = CLLocationManager()
    var latitude : CLLocationDegrees!
    var longitude : CLLocationDegrees!
    var locAddress = ""
    var loadStatus = true
    
    var selectedIndex = 0
    
    var studioListData: WellnessStudioList?
    
    let GoogleMapsAPIServerKey = "AIzaSyAGbCEXM7_27p2_TfoL51eONx0UluFdrSQ"
    
    lazy var placesSearchController: GooglePlacesSearchController = {
        let controller = GooglePlacesSearchController(delegate: self,
                                                      apiKey: GoogleMapsAPIServerKey,
                                                      placeType: .address,
                                                      searchBarPlaceholder: "Enter address"
        )
        return controller
    }()
    
    
    
    let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
    var handler:Any?
    
    var getStudioStatus = true
    
    var vwSearch: GoogleSearchApiView!
    var transparentBackground: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.visibleViewController?.title = "Studio List"
        
        handler = {
            (hyperLabel: FRHyperLabel!, substring: String!) -> Void in
            
            //action here
            print("its started")
            let str = self.lblSearch.attributedText?.string
            let arrStr = str?.components(separatedBy: "   ")
            self.placesSearchController.searchBar.text = arrStr![0]
            self.placesSearchController.modalPresentationStyle = UIModalPresentationStyle.popover
            self.present(self.placesSearchController, animated: true, completion: nil)
        }
        
        addSearchView()
        vwSearch.delegate = self
        self.transparentBackground.isHidden = true
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapSearchView))
        doubleTapRecognizer.numberOfTapsRequired = 1
        doubleTapRecognizer.delegate = self
        transparentBackground.addGestureRecognizer(doubleTapRecognizer)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
        locationManager.stopUpdatingLocation()
        getAddress { (address) in
            
            print("loc \(address)")
            self.locAddress = address
            self.lblSearch.attributedText = NSAttributedString(string: address + "   " + "Change", attributes: self.attributes)
            self.lblSearch.setLinksForSubstrings(["Change"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
            if self.getStudioStatus{
                self.getStudioList()
                self.getStudioStatus = false
            }
            
        }
        
        
    }
    
    @objc func enteredForeGround(){
        updateListOnLocationEnabled()
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("its appear")
        self.navigationController!.visibleViewController!.title = "Studio List"
        //        // Ask for Authorisation from the User.
        NotificationCenter.default.addObserver(self, selector: #selector(self.enteredForeGround), name: UIApplication.willEnterForegroundNotification, object: nil)
        updateListOnLocationEnabled()
    }
    
    func addSearchView(){
        if self.transparentBackground == nil{
            self.transparentBackground = UIView(frame: UIScreen.main.bounds)
            self.transparentBackground.backgroundColor = UIColor(white: 0.0, alpha: 0.54)
            UIApplication.shared.keyWindow!.addSubview(self.transparentBackground)
            self.vwSearch = self.setupSearchView()
            self.transparentBackground.addSubview(vwSearch)
            UIApplication.shared.keyWindow!.bringSubviewToFront(self.transparentBackground)
            self.view.bringSubviewToFront(transparentBackground)
        }
    }
    
    func setupSearchView() -> GoogleSearchApiView{
        let tempVwSearch = GoogleSearchApiView(frame: CGRect(x: 0, y: 20, width: Int(UIScreen.main.bounds.width), height: Int(UIScreen.main.bounds.height)))
        return tempVwSearch
    }
    
    @objc func onTapSearchView(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.transparentBackground.isHidden = true
            self.vwSearch.textfieldAddress.resignFirstResponder()
        }
    }
    
    func updateListOnLocationEnabled(){
        
        self.locationManager.requestAlwaysAuthorization()
        getStudioStatus = true
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            if loadStatus{
                locationManager.startUpdatingLocation()
                loadStatus = false
            }
        }else{
            self.ShowSettings()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    func getStudioList(){
        var dataToPost = ""
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        dataToPost = "\(Keys.KEYS_WORKOUT_STUDIO_LONG)=\(self.longitude!)&\(Keys.KEYS_WORKOUT_STUDIO_LAT)=\(self.latitude!)&\(Keys.KEYS_WORKOUT_STUDIO_LOCATION)=\(self.locAddress)"
        print("data to ppst \(dataToPost)")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        self.showSpinner(onView: self.view)
        let classListURL = AppURLS.URL_WELLNESS_STUDIO_LIST
        let url = URL(string: classListURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        let postData = dataToPost
        request.httpBody = postData.data(using: String.Encoding.utf8)
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    //                    self.vw_activity_indicator.isHidden = true
                    //                    self.activityIndicator.stopAnimating()
                    self.removeSpinner()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    return
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let statusCode = response as? HTTPURLResponse
            {
                if let response = data, statusCode.statusCode == 200{
                    do{
                        let datares = String(data: data!, encoding: String.Encoding.utf8)
                        print("error res \(datares)")
                        self.studioListData?.data.removeAll()
                        self.studioListData = try JSONDecoder().decode(WellnessStudioList.self, from: data!) as WellnessStudioList
                        print("total classes \(self.studioListData!.data.count)")
                        self.removeSpinner()
                        if (self.studioListData?.data.count)! > 0
                        {

                            
                            DispatchQueue.main.async {
                                self.studioList.reloadData()
//                                self.backgroundView.removeFromSuperview()
                                
                            }
           
                        }else{
                            DispatchQueue.main.async {
                                self.studioList.reloadData()
//                                self.backgroundView.removeFromSuperview()
                                let blanker = UIView()
                                let lbl = UILabel()
                                lbl.frame = self.studioList.bounds
                                lbl.text = "Sorry!!!\nNo workouts/services available"
                                lbl.numberOfLines = 0
                                lbl.textAlignment = .center
                                blanker.addSubview(lbl)
                                self.studioList.tableFooterView = blanker
//                                self.removeSpinner()
                            }
                        }
                        
                    }
                    catch{
                        self.removeSpinner()
                        print("json parse error")
                    }
                }else{
                    let datares = String(data: data!, encoding: String.Encoding.utf8)
                    print("error res \(datares)")
                    self.removeSpinner()
                    self.checkOnlyStatus(statusCode: statusCode.statusCode)
                    
                }
            }
            
        }).resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StudioWorkoutVC"{
            let studioWrkoutVC = segue.destination as! StudioWorkoutVC
            let addrss = studioListData?.data[selectedIndex]
            studioWrkoutVC.address = (addrss!.StreetAddress1)+","+(addrss!.StreetAddress2)+","+(addrss!.CityName)
            studioWrkoutVC.partnerName = addrss!.PartnerName
            studioWrkoutVC.studioName = addrss!.OutletName
            studioWrkoutVC.distance = addrss!.Distance
        }
        
    }
    
    @IBAction func clickedBtnSearch(_ sender: Any) {
        //action here
        print("its started")
        let str = self.lblSearch.attributedText?.string
        let arrStr = str?.components(separatedBy: "   ")
        /*self.placesSearchController.searchBar.text = arrStr![0]
        self.placesSearchController.modalPresentationStyle = UIModalPresentationStyle.popover
        self.present(self.placesSearchController, animated: true, completion: nil)*/
        self.transparentBackground.isHidden = false
        vwSearch.textfieldAddress.text = arrStr![0]
        vwSearch.textfieldAddress.becomeFirstResponder()
    }

}


extension StudioListVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "StudioWorkoutVC", sender: nil)
    }
}


extension StudioListVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let countClass = studioListData?.data.count, countClass > 0{
            return (studioListData?.data.count)!
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StudioCell
        let studioInfo = self.studioListData?.data[indexPath.row]
        cell.configCell(cellInfo: studioInfo!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100//135
    }
}

extension StudioListVC: GooglePlacesAutocompleteViewControllerDelegate {
    func viewController(didAutocompleteWith place: PlaceDetails) {
        print(place.description)
        //        searchBar.text = place.description
        placesSearchController.isActive = false
        var address = ""
        
        if let streetNum = place.streetNumber{
            address += streetNum+","
        }
        if let name = place.name{
            address += name+","
        }
        if let subLocality = place.subLocality{
            address += subLocality+","
        }
        if let locality = place.locality{
            address += locality+","
        }
        if let postalCode = place.postalCode{
            address += postalCode
        }
//        searchBar.text = address
        locAddress = address
        self.lblSearch.attributedText = NSAttributedString(string: address + "   " + "Change", attributes: self.attributes)
        self.lblSearch.setLinksForSubstrings(["Change"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
        
        self.latitude = place.coordinate?.latitude
        self.longitude = place.coordinate?.longitude
        
        getStudioList()
        
    }
    
    
}

extension StudioListVC{
    
    func getAddress(handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                address += locationName + ","
            }
            
            // Street address
            //            if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
            //                address += street + ", "
            //            }
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address += city + ","
            }
            
            // Zip code
            if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
                address += zip
            }
            
            
            
            // Passing address back
            handler(address)
        })
    }
}

extension StudioListVC : GoogleSearchApiDelegate{
    func hideSearchView() {
        self.transparentBackground.isHidden = true
    }
    func slectedUserIndex(place: GApiResponse.PlaceInfo,formattedAddress:String) {
        print("slectedUserIndex : \(place.formattedAddress)")
        locAddress = formattedAddress//place.formattedAddress
        
        self.lblSearch.attributedText = NSAttributedString(string: formattedAddress + "   " + "Change", attributes: self.attributes)
        self.lblSearch.setLinksForSubstrings(["Change"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
        
        self.latitude = place.latitude
        self.longitude = place.longitude
        getStudioList()
    }
}

extension StudioListVC : UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if touch.view!.isDescendant(of: self.vwSearch.tableviewSearch) || touch.view!.isDescendant(of: self.vwSearch.txtView) {
            return false
        }
        return true
    }
    
}

