//
//  WellnessProgramViewController.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 25/04/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlacesSearchController

class WellnessClassesListVC: UIViewController, CLLocationManagerDelegate {
    
    var nearestClasses : WellnessNearestClass?
    var filteredClassesList = [WellnessInnerData]()
    
    @IBOutlet weak var classesTblView: UITableView!
    @IBOutlet weak var lblSearch: FRHyperLabel!
    //@IBOutlet weak var vwSearch: GoogleSearchApiView!
    var vwSearch: GoogleSearchApiView!
    var transparentBackground: UIView!
    
    let dayHeaderView = DayHeaderView()
    var state = DayViewState()
    
    let calendarView = CGCalendarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50.0))
    let calendar : Calendar = Calendar.current
    
    let locationManager = CLLocationManager()
    
    var selectedOutletIndex = 0
    
    var indicatorView : UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .whiteLarge
        indicator.color = UIColor(red: 109/255, green: 191/255, blue: 0, alpha: 1.0)
        indicator.startAnimating()
        return indicator
    }()
    
    var backgroundView : UIView = {
        let bg_view = UIView()
        bg_view.backgroundColor = .white
        return bg_view
    }()
    
    lazy var searchBar = UISearchBar(frame: .zero)
    
    var classesForDate = ""
    let dateFormatterr = DateFormatter()
    
    var latitude : CLLocationDegrees!
    var longitude : CLLocationDegrees!
    
    var loadStatus = true
    let GoogleMapsAPIServerKey = "AIzaSyAGbCEXM7_27p2_TfoL51eONx0UluFdrSQ"
    
    lazy var placesSearchController: GooglePlacesSearchController = {
        let controller = GooglePlacesSearchController(delegate: self,
                                                      apiKey: GoogleMapsAPIServerKey,
                                                      placeType: .address,
                                                      searchBarPlaceholder: "Enter address"
                                                      
        )
        return controller
    }()
    
    let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
    var handler:Any?
    
    var getClassesStatus = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        classesTblView.rowHeight = UITableView.automaticDimension
        classesTblView.estimatedRowHeight = 1000
        
        /*dayHeaderView.delegate = self
        self.view.addSubview(dayHeaderView)
        dayHeaderView.anchorAndFillEdge(.top, xPad: 0, yPad: 0, otherSize: 88.0)
        dayHeaderView.state = state
        let style = CalendarStyle()
        style.header.backgroundColor = UIColor.clear
        style.header.daySymbols.weekDayColor = UIColor.white
        style.header.daySymbols.weekendColor = UIColor.white
        style.header.swipeLabel.textColor = UIColor.white
        style.header.daySelector.weekendTextColor = UIColor.black
        dayHeaderView.updateStyle(style.header)
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.dayHeaderView.frame
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.dayHeaderView.insertSubview(vwGrad, at: 0)
        }*/
        setUpCalenderView()
//        searchBar.placeholder = "Enter Address"
        searchBar.delegate = self
        searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.clear
        textFieldInsideSearchBar?.textColor = .white
        textFieldInsideSearchBar?.layer.borderColor = UIColor.white.cgColor
        textFieldInsideSearchBar?.layer.borderWidth = 0.5
        textFieldInsideSearchBar?.layer.cornerRadius = 10
        textFieldInsideSearchBar?.attributedPlaceholder = NSAttributedString(string: "Enter Address",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        let iconView = textFieldInsideSearchBar?.leftView as? UIImageView
            
        iconView?.image = iconView?.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        iconView?.tintColor = UIColor.white
        //UnComment to enable search func
        //self.navigationController?.visibleViewController?.navigationItem.titleView = searchBar
        
//        backgroundView.frame = CGRect(x: 0, y: 88, width: classesTblView.bounds.width, height: classesTblView.bounds.height)
        setupNavigationBar()
        //comment to disbale navigation title
        self.navigationController?.visibleViewController?.title = "Workout List"
        
        
        dateFormatterr.dateFormat = "yyyy-MM-dd"
        classesForDate = dateFormatterr.string(from: Date())
        
        self.navigationController?.navigationBar.isHidden = false
        
        handler = {
            (hyperLabel: FRHyperLabel!, substring: String!) -> Void in
            
            //action here
            print("its started")
            let str = self.lblSearch.attributedText?.string
            let arrStr = str?.components(separatedBy: "   ")
            self.placesSearchController.searchBar.text = arrStr![0]
            self.placesSearchController.modalPresentationStyle = UIModalPresentationStyle.popover
            self.present(self.placesSearchController, animated: true, completion: nil)
        }
        
        addSearchView()
        vwSearch.delegate = self
        self.transparentBackground.isHidden = true
        
        
//        App.selectedDate = currentDate().dateOnly()
        
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapSearchView))
        doubleTapRecognizer.numberOfTapsRequired = 1
        doubleTapRecognizer.delegate = self
        transparentBackground.addGestureRecognizer(doubleTapRecognizer)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func enteredForeGround(){
        updateListOnLocationEnabled()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("its appear")
        self.navigationController!.visibleViewController!.title = "Workout List"
//        // Ask for Authorisation from the User.
        NotificationCenter.default.addObserver(self, selector: #selector(self.enteredForeGround), name: UIApplication.willEnterForegroundNotification, object: nil)
        updateListOnLocationEnabled()
    }
    
    func addSearchView(){
        if self.transparentBackground == nil{
            self.transparentBackground = UIView(frame: UIScreen.main.bounds)
            self.transparentBackground.backgroundColor = UIColor(white: 0.0, alpha: 0.54)
            UIApplication.shared.keyWindow!.addSubview(self.transparentBackground)
            self.vwSearch = self.setupSearchView()
            self.transparentBackground.addSubview(vwSearch)
            UIApplication.shared.keyWindow!.bringSubviewToFront(self.transparentBackground)
            self.view.bringSubviewToFront(transparentBackground)
        }
    }
    
    func setupSearchView() -> GoogleSearchApiView{
        let tempVwSearch = GoogleSearchApiView(frame: CGRect(x: 0, y: 20, width: Int(UIScreen.main.bounds.width), height: Int(UIScreen.main.bounds.height)))
        return tempVwSearch
    }
    
    @objc func onTapSearchView(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.transparentBackground.isHidden = true
            self.vwSearch.textfieldAddress.resignFirstResponder()
        }
    }
    
    func updateListOnLocationEnabled(){
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        self.getClassesStatus = true
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            if loadStatus{
                locationManager.startUpdatingLocation()
                loadStatus = false
            }
        }else{
            self.ShowSettings()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    func setUpCalenderView() {
        self.calendarView.calendar = calendar
        self.calendarView.backgroundColor = UIColor.clear
        self.calendarView.rowCellClass = CGCalendarCell.self
        
        let cal = Calendar.current
        self.calendarView.firstDate = cal.startOfDay(for: Date())
        self.calendarView.lastDate = cal.date(byAdding: .day, value: 7, to: self.calendarView.firstDate)!
        
        self.calendarView.delegate = self
        self.calendarView.selectedDate  = cal.startOfDay(for: Date())
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.calendarView.bounds
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.calendarView.insertSubview(vwGrad, at: 0)
        }
        
        self.view.addSubview(calendarView)
        self.calendarView.anchorAndFillEdge(.top, xPad: 0, yPad: 0, otherSize: 58.0)
    }
    
    
    func currentDate() -> Date {
        print("current date :",Date().debugDescription)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        let strDate = dateFormatter.string(from: Date())
        print("current date in string :",strDate)
        let yourDate = dateFormatter.date(from: strDate)
        return yourDate!
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
        locationManager.stopUpdatingLocation()
        getAddress { (address) in
            
            print("loc \(address)")
            self.searchBar.text = address
            self.lblSearch.attributedText = NSAttributedString(string: address + "   " + "Change", attributes: self.attributes)
            self.lblSearch.setLinksForSubstrings(["Change"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
            if self.getClassesStatus{
                self.getNearestClasses()
                self.getClassesStatus = false
            }
            
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied{
            self.ShowSettings()
        }
    }
    
    func getNearestClasses(){
        var dataToPost = ""
        print("latitude \(latitude) longitude \(longitude) classesForDate \(classesForDate)")
        if latitude == nil || longitude == nil  {
            return
        }
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
//        backgroundView.frame = CGRect(x: 0, y: 128, width: classesTblView.bounds.width, height: classesTblView.bounds.height)
//        self.view.addSubview(backgroundView)
//        indicatorView.center = CGPoint(x: self.view.bounds.size.width/2, y: self.view.bounds.size.height/2)
//        backgroundView.addSubview(indicatorView)
        
//        self.vw_activity_indicator.isHidden = false
//        self.activityIndicator.startAnimating()
        self.showSpinner(onView: self.view)
        dataToPost = "\(Keys.KEYS_WORKOUT_LON)=\(longitude!)&\(Keys.KEYS_WORKOUT_LAT)=\(latitude!)&\(Keys.KEYS_WORKOUT_DATE)=\(classesForDate)&\(Keys.KEYS_WORKOUT_LOC)=\(self.searchBar.text!)"
        print("data to ppst \(dataToPost)")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let classListURL = AppURLS.URL_WELLNESS_WORKOUT_LIST
        let url = URL(string: classListURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        let postData = dataToPost
        request.httpBody = postData.data(using: String.Encoding.utf8)
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
//                    self.vw_activity_indicator.isHidden = true
//                    self.activityIndicator.stopAnimating()
//                    self.backgroundView.removeFromSuperview()
                    self.removeSpinner()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    return
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let statusCode = response as? HTTPURLResponse
            {
            if let response = data, statusCode.statusCode == 200{
                do{
                    let datares = String(data: data!, encoding: String.Encoding.utf8)
                    print("error res \(datares)")
                    self.nearestClasses?.data.removeAll()
                    self.self.filteredClassesList.removeAll()
                    self.nearestClasses = try? JSONDecoder().decode(WellnessNearestClass.self, from: data!) as WellnessNearestClass
//                    print("welness info \(self.nearestClasses![0].len)")
                    print("total classes \(self.nearestClasses!.data.count)")
                    if (self.nearestClasses?.data.count)! > 0
                    {
                        for classInfo in (self.nearestClasses?.data)!{
                            if self.doRemovePrevClass(classInfo: classInfo){
                                self.filteredClassesList.append(classInfo)
                            }
                        }
                        self.filteredClassesList = self.filteredClassesList.sorted(by: { Float($0.dist) < Float($1.dist) })
//                        self.nearestClasses?.sortByDistance()
                        
                        self.filteredClassesList.forEach{
                            print("sort id \($0.id)")
                            print("sort name \($0.len)")
                            print("sort ff \($0.ft)")
                            print("sort tt \($0.tt)")
                            print("sort dist \($0.dist)")
                            print("------------------------------------")
                        }
                        
                        DispatchQueue.main.async {
                            self.classesTblView.reloadData()
//                            self.backgroundView.removeFromSuperview()
                            self.removeSpinner()
                        }
                        
                    }else{
                        DispatchQueue.main.async {
                            self.classesTblView.reloadData()
//                            self.backgroundView.removeFromSuperview()
                            let blanker = UIView()
                            let lbl = UILabel()
                            lbl.frame = self.classesTblView.bounds
                            lbl.text = "Sorry!!!\nNo workouts/services available"
                            lbl.numberOfLines = 0
                            lbl.textAlignment = .center
                            blanker.addSubview(lbl)
                            self.classesTblView.tableFooterView = blanker
                            self.removeSpinner()
                        }
                    }
                    
                }
            }else{
                let datares = String(data: data!, encoding: String.Encoding.utf8)
                print("error res \(datares)")
                self.removeSpinner()
                self.checkOnlyStatus(statusCode: statusCode.statusCode)
                
            }
            }
            
        }).resume()
    }
    
    func doRemovePrevClass(classInfo: WellnessInnerData)->Bool{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let today = formatter.string(from: Date())
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        if today == classesForDate{
            let scheduleTime = formatter.date(from: classesForDate+" "+classInfo.tt)
            let diff = Calendar.current.dateComponents([.minute], from: Date(), to: scheduleTime!).minute
            
            if diff! <= 0 {
                return false
            }
        }
        return true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "workoutdetails"{
            let detailVC = segue.destination as! WellnessClassDetailsVC
            detailVC.workoutDetailId = self.filteredClassesList[selectedOutletIndex].id
            detailVC.outletName = self.filteredClassesList[selectedOutletIndex].len
            detailVC.classFordate = classesForDate
            detailVC.latitude = self.latitude
            detailVC.longitude = self.longitude
//            detailVC.dateShowing = classesForDate
            print(" source lat \(self.latitude) long \(self.longitude)")
            detailVC.distance = "\(self.filteredClassesList[selectedOutletIndex].dist)"
        }
        
    }
    
    
    
    
    
    func loadClasses(date: Date){
        let formatter = DateFormatter()
        
//        formatter.timeZone = NSTimeZone(abbreviation: "GMT")! as TimeZone
        formatter.dateFormat = "yyyy-MM-dd"
        
//        let date1 = Calendar.current.date(byAdding: .day, value: 1, to: date)
        
        let todayStr = formatter.string(from: Date())
        let today = formatter.date(from: todayStr)
        print("today is \(today) selected date \(date)")
        let diffDays = Calendar.current.dateComponents([.day], from: today!, to: date).day!
        print("dif date \(diffDays)")
        if diffDays < 0{
            dayHeaderView.state?.move(to: Date())
            classesForDate = formatter.string(from: Date())
        }else{
            
            
            
            let myString = formatter.string(from: date) // string purpose I add here
            print("did select date \(myString)")
            classesForDate = myString
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            print("did select date d \(yourDate)")
        }
        
        getNearestClasses()
    }
    
    // MARK: - UIButton Actions
    @IBAction func clickedBtnSearch(_ sender: Any) {
        //action here
        print("its started")
        let str = self.lblSearch.attributedText?.string
        let arrStr = str?.components(separatedBy: "   ")
        //self.placesSearchController.searchBar.text = arrStr![0]
        //self.placesSearchController.modalPresentationStyle = UIModalPresentationStyle.popover
        //self.present(self.placesSearchController, animated: true, completion: nil)
        self.transparentBackground.isHidden = false
        vwSearch.textfieldAddress.text = arrStr![0]
        vwSearch.textfieldAddress.becomeFirstResponder()
    }
    
}


extension WellnessClassesListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filteredClassesList.count > 0{
            return self.filteredClassesList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NearestClassesCell", for: indexPath) as! NearestClassesCell
        print("index is \(indexPath.row)")
        cell.configureCell(classInfo: self.filteredClassesList[indexPath.row])
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        selectedOutletIndex = indexPath.row
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_WELLNESS_CLASS_DETAILS, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}


extension WellnessClassesListVC : DayHeaderViewDelegate
{
    func didSelectDate(_ date: Date) {
        print("didSelectDate : ",date)
        loadClasses(date: date)
    }
    
    func didMoveToDate(_ date: Date) {
        print("didMoveToDate : ",date)
        loadClasses(date: date)
    }
    
}

extension WellnessClassesListVC: GooglePlacesAutocompleteViewControllerDelegate {
    func viewController(didAutocompleteWith place: PlaceDetails) {
        print(place.description)
//        searchBar.text = place.description
        placesSearchController.isActive = false
        var address = ""
        
        if let streetNum = place.streetNumber{
            address += streetNum+","
        }
        if let name = place.name{
            address += name+","
        }
        if let subLocality = place.subLocality{
            address += subLocality+","
        }
        if let locality = place.locality{
            address += locality+","
        }
        if let postalCode = place.postalCode{
            address += postalCode
        }
        searchBar.text = address
        
        self.lblSearch.attributedText = NSAttributedString(string: address + "   " + "Change", attributes: self.attributes)
        self.lblSearch.setLinksForSubstrings(["Change"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
        
        self.latitude = place.coordinate?.latitude
        self.longitude = place.coordinate?.longitude
        
        getNearestClasses()
        
    }
    
    
}

extension WellnessClassesListVC: UISearchBarDelegate{
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("its started")
        placesSearchController.searchBar.text = searchBar.text
        placesSearchController.modalPresentationStyle = UIModalPresentationStyle.popover
        present(placesSearchController, animated: true, completion: nil)
    }
}


extension WellnessClassesListVC{
    
    func getAddress(handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                address += locationName + ", "
            }
            
            // Street address
//            if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
//                address += street + ", "
//            }
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address += city + ", "
            }
            
            // Zip code
            if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
                address += zip + ", "
            }
            
            
            
            // Passing address back
            handler(address)
        })
    }
}

extension WellnessClassesListVC : CGCalendarViewDelegate
{
    func calendarView(_ calendarView: CGCalendarView!, didSelect date: Date!) {
        loadClasses(date: date)
    }
}

extension WellnessClassesListVC : GoogleSearchApiDelegate{
    func hideSearchView() {
        self.transparentBackground.isHidden = true
    }
    func slectedUserIndex(place: GApiResponse.PlaceInfo,formattedAddress:String) {
        print("slectedUserIndex : \(place.formattedAddress)")
        searchBar.text = formattedAddress//place.formattedAddress
        
        self.lblSearch.attributedText = NSAttributedString(string: formattedAddress + "   " + "Change", attributes: self.attributes)
        self.lblSearch.setLinksForSubstrings(["Change"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
        
        self.latitude = place.latitude
        self.longitude = place.longitude
        getNearestClasses()
    }
}

extension WellnessClassesListVC : UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if touch.view!.isDescendant(of: self.vwSearch.tableviewSearch) || touch.view!.isDescendant(of: self.vwSearch.txtView) {
            return false
        }
        return true
    }
    
}
