//
//  StudioWorkoutVC.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 14/08/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class StudioWorkoutVC: UIViewController, CLLocationManagerDelegate {
    
    let dayHeaderView = DayHeaderView()
    var state = DayViewState()
    
    let calendarView = CGCalendarView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50.0))
    let calendar : Calendar = Calendar.current
    
    
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var wrkLister: UITableView!
    @IBOutlet weak var addressLbl: UILabel!
    var address = ""
    var partnerName = ""
    var selectedDate = ""
    var studioName = ""
    var distance : Float = 0
    
    @IBOutlet weak var addressViewHeight: NSLayoutConstraint!
    var selectedOutletIndex = 0
    
    let locationManager = CLLocationManager()
    
    var workout : [String: [NSDictionary]] = [String: [NSDictionary]]()
    
    var latitude : CLLocationDegrees!
    var longitude : CLLocationDegrees!
    
//    var workoutList : [String: Any?]!
    
    var formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressViewHeight.constant = 50
        addressContainerView.isHidden = false
        // Do any additional setup after loading the view.
        
        /*dayHeaderView.delegate = self
        self.view.addSubview(dayHeaderView)
        dayHeaderView.anchorAndFillEdge(.top, xPad: 0, yPad: 0, otherSize: 88.0)
        dayHeaderView.state = state
        
        let style = CalendarStyle()
        style.header.backgroundColor = UIColor.clear
        style.header.daySymbols.weekDayColor = UIColor.white
        style.header.daySymbols.weekendColor = UIColor.white
        style.header.swipeLabel.textColor = UIColor.white
        style.header.daySelector.weekendTextColor = UIColor.black
        dayHeaderView.beginningOfWeek(Date())
        dayHeaderView.updateStyle(style.header)
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.dayHeaderView.bounds
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.dayHeaderView.insertSubview(vwGrad, at: 0)
        }*/
        setUpCalenderView()
        self.navigationController?.visibleViewController?.title = studioName
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        formatter.dateFormat = "yyyy-MM-dd"
        selectedDate = formatter.string(from: Date())
        getWorkoutsList()
        addressLbl.text = address
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            if loadStatus{
                locationManager.startUpdatingLocation()
//                loadStatus = false
//            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem?.title = ""
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setUpCalenderView() {
        self.calendarView.calendar = calendar
        self.calendarView.backgroundColor = UIColor.clear
        self.calendarView.rowCellClass = CGCalendarCell.self
        
        let cal = Calendar.current
        self.calendarView.firstDate = cal.startOfDay(for: Date())
        self.calendarView.lastDate = cal.date(byAdding: .day, value: 7, to: self.calendarView.firstDate)!
        
        self.calendarView.delegate = self
        self.calendarView.selectedDate  = cal.startOfDay(for: Date())
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.calendarView.bounds
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.calendarView.insertSubview(vwGrad, at: 0)
        }
        
        self.view.addSubview(calendarView)
        self.calendarView.anchorAndFillEdge(.top, xPad: 0, yPad: 0, otherSize: 58.0)
    }
    
    func getWorkoutsList(){
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        self.showSpinner(onView: self.view)
        let classListURL = AppURLS.URL_WELLNESS_STUDIO_WORKOUT_LIST+partnerName
        print("studio url \(classListURL)")
        let url = URL(string: classListURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.GET
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    //                    self.vw_activity_indicator.isHidden = true
                    //                    self.activityIndicator.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    self.removeSpinner()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    return
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let statusCode = response as? HTTPURLResponse
            {
                if let _ = data, statusCode.statusCode == 200{
                    do{
                        let datares = String(data: data!, encoding: String.Encoding.utf8)
                        print("workout studio res \(String(describing: datares))")
                        
                        let workoutRes = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                        
                        
                        
                        guard let datas = workoutRes["data"] as? [String: [NSDictionary]] else{
                            DispatchQueue.main.async {
                                self.showNoWorkoutBanner()
                                self.removeSpinner()
                            }
                            return
                        }
                        
                        print("datas to \(datas)")
                        
                        for workoutInfo in datas.enumerated(){
                            var filteredClass = [NSDictionary]()
                            for workout in workoutInfo.element.value{
                                if self.doRemovePrevClass(date: workoutInfo.element.key, classInfo: workout){
                                    filteredClass.append(workout)
                                }
                            }
                            self.workout[workoutInfo.element.key] = filteredClass
                        }
                        print("workout list updated \(self.workout)")
                        self.removeSpinner()
                        if let currentDayCount = self.workout[self.selectedDate], currentDayCount.count > 0
                        {
                            
                            DispatchQueue.main.async {
                                self.wrkLister.reloadData()
                                //                                self.backgroundView.removeFromSuperview()
                                self.addressViewHeight.constant = 50
                                self.addressContainerView.isHidden = false
                                self.wrkLister.tableFooterView?.isHidden = true
                            }
                            
                        }else{
                            DispatchQueue.main.async {
                                self.showNoWorkoutBanner()
                                self.wrkLister.tableFooterView?.isHidden = false
                            }
                        }
                        
                    }
                    catch{
                        self.removeSpinner()
                        print("json parse error")
                    }
                }else{
                    let datares = String(data: data!, encoding: String.Encoding.utf8)
                    print("error res \(datares)")
                    self.removeSpinner()
                    self.checkOnlyStatus(statusCode: statusCode.statusCode)
                }
            }
            
        }).resume()
    }
    
    func showNoWorkoutBanner(){
        self.wrkLister.reloadData()
        //                                self.backgroundView.removeFromSuperview()
        addressViewHeight.constant = 0
        addressContainerView.isHidden = true
        let blanker = UIView()
        let lbl = UILabel()
        lbl.frame = self.wrkLister.bounds
        lbl.text = "Sorry!!!\nNo workouts available"
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        blanker.addSubview(lbl)
        self.wrkLister.tableFooterView = blanker
        
    }
    
    
    func doRemovePrevClass(date: String, classInfo: NSDictionary)->Bool{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let today = formatter.string(from: Date())
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        print("selected date \(selectedDate)")
        if today == date{
            let scheduleTime = formatter.date(from: "\(today) \(classInfo["tt"] as! String)")
            let diff = Calendar.current.dateComponents([.minute], from: Date(), to: scheduleTime!).minute
            print("its in now")
            if diff! <= 0 {
                return false
            }
        }
        return true
    }
    

    func loadClasses(date: Date){
        let formatter = DateFormatter()
        
        //        formatter.timeZone = NSTimeZone(abbreviation: "GMT")! as TimeZone
        formatter.dateFormat = "yyyy-MM-dd"
        
        //        let date1 = Calendar.current.date(byAdding: .day, value: 1, to: date)
        
        let todayStr = formatter.string(from: Date())
        let today = formatter.date(from: todayStr)
        print("today is \(today) selected date \(date)")
        let diffDays = Calendar.current.dateComponents([.day], from: today!, to: date).day!
        print("dif date \(diffDays)")
        if diffDays < 0{
            dayHeaderView.state?.move(to: Date())
            selectedDate = formatter.string(from: Date())
            
        }else{
            
            selectedDate = formatter.string(from: date)
            print("selected date \(selectedDate)")
            
            
            
        }
        if let _ = self.workout[selectedDate]{
            self.addressViewHeight.constant = 50
            self.addressContainerView.isHidden = false
            self.wrkLister.tableFooterView?.isHidden = true
            wrkLister.reloadData()
        }else{
            self.wrkLister.tableFooterView?.isHidden = false
            showNoWorkoutBanner()
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
        locationManager.stopUpdatingLocation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "workoutdetails"{
            let detailVC = segue.destination as! WellnessClassDetailsVC
            let wrkoutInfo = self.workout[selectedDate]![selectedOutletIndex]
            print("workout info \(wrkoutInfo)")
            detailVC.workoutDetailId = "\(wrkoutInfo["id"] as! String)"
            detailVC.outletName = wrkoutInfo["len"] as! String
            detailVC.classFordate = selectedDate
            detailVC.latitude = self.latitude
            detailVC.longitude = self.longitude
            //            detailVC.dateShowing = classesForDate
            print(" source lat \(self.latitude) long \(self.longitude)")
            detailVC.distance = "\(distance)"
        }
    
}
}


extension StudioWorkoutVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        selectedOutletIndex = indexPath.row
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_WELLNESS_CLASS_DETAILS, sender: nil)
    }
    
}

extension StudioWorkoutVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let workoutList = self.workout[selectedDate],workoutList.count > 0{
            return workoutList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StdWrkoutCell
        let workoutInfo = self.workout[selectedDate]![indexPath.row]
        cell.configCell(workout: workoutInfo, distance: self.distance)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
}


extension StudioWorkoutVC : DayHeaderViewDelegate
{
    func didSelectDate(_ date: Date) {
        print("didSelectDate : ",date)
       
        loadClasses(date: date)
        
    }
    
    func didMoveToDate(_ date: Date) {
        print("didMoveToDate : ",date)
        loadClasses(date: date)
    }
    
}

extension StudioWorkoutVC : CGCalendarViewDelegate
{
    func calendarView(_ calendarView: CGCalendarView!, didSelect date: Date!) {
        loadClasses(date: date)
    }
}



