//
//  WellnessWorkoutTracker.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 24/06/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//


import UIKit
//import NVActivityIndicatorView

/*
 * This class is for Appointments listing
 */



class WellnessWorkoutTracker: UIViewController ,NVActivityIndicatorViewable ,UIBarPositioningDelegate{
    
    
    @IBOutlet var upcomingNPrevSegmentControl: UISegmentedControl!
    
    @IBOutlet var toolBar: UIToolbar!
    
    //Outlets
    //  @IBOutlet weak var bt_upcommingOutlet: UIButton!
    //  @IBOutlet weak var bt_previousOutlet: UIButton!
    
    @IBOutlet weak var vw_bookAConsultation: UIView!
    @IBOutlet var vw_gradientForToolBar: UIView!
    
    @IBOutlet weak var noAppointmentsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    ///hex string for selected button background
    let selectedButtonBGColorCode = "#F6846A"
    
    ///instance for selected appointment id
    var selectedAppointment = ""
    
    ///count for starting notification
    var listShowingFromNTo = ""
    
    ///instance for on veiwload loads upcoming url
    var firstPageUrl = ""
    
    ///if appointments has next list url uses this instance
    var nextPageUrl = ""
    
    ///called when scrolling of list ends and set bool value
    var isScrollingFinished :Bool!
    
    ///Bool value to check list ended stop network request
    var isLoadingListItems:Bool = true
    
    ///instance to get total appointments count
    var totalAppListCount : Int!
    
    //status message to show when no appoinments called in tableview delegate method numberOfRowsInsection
    var noAppointmetns = "Knock ! Knock!\nYour scheduled upcoming  appointments await you here!"
    
    //Instance variable for appointments array to load in tableview
    
    var currentPage : Int = 1
    var isLoadingList : Bool = true
    
    var workoutList : WellnessWorkoutList?
    var workoutListHistory : [WellnessTrackerInner]? = [WellnessTrackerInner]()
    var workoutListUpcoming : [WellnessTrackerInner]? = [WellnessTrackerInner]()
    var workoutListCancelled : [WellnessTrackerInner]? = [WellnessTrackerInner]()
    
    ///Button tags for checking which button tapped
    var UP_COMING_BUTTON_TAG = 1
    var PREVIOUS_BUTTON_TAG = 2
    var selectedButtonTag = 0
    var isUpcoming = 1
    
    var selectedWorkout = 0
    
    var refreshStatus = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///setting scrolling false to call to set firsPageUrl instance url
        isScrollingFinished = false
        
        //initialising tableview shadow
        tableViewSetup()
        self.navigationController?.visibleViewController?.title = "My Workouts"
        
        //method which perform request to server to fetch notifications
        
        ///New UI
        setupNavigationBar()
        
//        getAppointmentsList(urlStrin: AppURLS.URL_WELLNESS_WORKOUT_TRACKER, tag: 0)
//        self.getAppointmentsList(pageNo:currentPage , tag:0)
        
        DispatchQueue.main.async {
            let vwGrad = GradientView()
            vwGrad.frame = self.toolBar.frame
            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.toolBar.insertSubview(vwGrad, at: 0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("its refreshing")
        workoutListHistory?.removeAll()
        workoutListUpcoming?.removeAll()
        workoutListCancelled?.removeAll()
        selectedType = 0
        upcomingNPrevSegmentControl.selectedSegmentIndex = 0
        
        currentPage = 1
        self.getAppointmentsList(pageNo:currentPage , tag:0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("its appear")
        self.navigationController!.visibleViewController!.title = "My Workouts"
    }
    
    ///UIBarPostionDelegate method
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    ///
    func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //  self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        //  self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "6DBF00")
    }
    
    
    /**
     Method used to setup table view with shadow
     */
    func tableViewSetup() {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.reloadData()
        tableView.layer.shadowOpacity = 0.3
        tableView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        tableView.layer.shadowRadius = 2.0
        tableView.layer.shadowColor = UIColor.darkGray.cgColor
        tableView.layer.cornerRadius = 5
        tableView.tableFooterView?.backgroundColor = .blue
    }
    
 
    func loadMoreItemsForList(){
        currentPage += 1
        getAppointmentsList(pageNo: currentPage, tag: 0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.isLoadingList = true
    }
    
    
    
    func getAppointmentsList(pageNo:Int , tag:Int)
    {
    
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error!", message:AlertMessages.NETWORK_ERROR)
            return
        }
    
        
        let categoryType = self.selectedType == 0 ? "upcoming" : self.selectedType == 1 ? "previous" : "cancelled"
        let workoutListUrl = AppURLS.URL_WELLNESS_WORKOUT_TRACKER+"\(categoryType)"+"/"+"\(pageNo)"
        print("wrk \(workoutListUrl)")
        if self.isLoadingList
        {
            startAnimating()
            self.isLoadingList = false
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
    
            let url = URL(string: workoutListUrl)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.tableView.reloadData()
                        self.didShowAlert(title: "Oops..", message: (error?.localizedDescription)!)
                        // AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                        let resdata = String(data: data!, encoding: String.Encoding.utf8)
                        print("response in history = \(String(describing: resdata))")
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    if let response = response,statusCode == 200
                    {
                        print("pagination url = \(response.url!)")
                        let resdata = String(data: data!, encoding: String.Encoding.utf8)
                        print("response = \(String(describing: resdata))")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do{
                            let resultsData = String(data: data!, encoding: String.Encoding.utf8)
                            print("resultsData \(resultsData ?? "")")
                            let workoutHistory = try? JSONDecoder().decode(WellnessWorkoutList.self, from: data!) as WellnessWorkoutList
                            if (workoutHistory?.data.records.count)! > 0{
                                (workoutHistory?.data.records)!.forEach{
                                    //Following logic when there was a single api for upcoming, history, cancelled
                                   /* if let isAttended = $0.is_attended, isAttended == 0{
                                        if let tempBook = ($0.booked_for_date),let tempTime = ($0.to_time){
                                            if self.workoutSatusDecider(forDate: tempBook, forTime: tempTime){
                                                if $0.is_cancelled == 0{
                                                    self.workoutListUpcoming?.append($0)
                                                }
                                            }else{
                                                if $0.is_cancelled == 0{
                                                    self.workoutListHistory?.append($0)
                                                }
                                            }
                                            if $0.is_cancelled == 1{
                                                self.workoutListCancelled?.append($0)
                                            }
                                        }
                                    }else{
                                        self.workoutListHistory?.append($0)
                                    }*/
                                    //till here
                                    
                                    //Following logic for seperate api call for upcoming, history, cancelled
                                    if self.selectedType == 0{
                                        self.workoutListUpcoming?.append($0)
                                    }
                                    if self.selectedType == 1{
                                        self.workoutListHistory?.append($0)
                                    }
                                    if self.selectedType == 2{
                                        self.workoutListCancelled?.append($0)
                                    }
                                }
                                    print("total list-history \(String(describing: self.workoutListHistory?.count))")
                                    print("total list-upcoming \(String(describing: self.workoutListUpcoming?.count))")
                                    print("total list-cancelled \(String(describing: self.workoutListCancelled?.count))")
                                    //                            }else{
                                    //                                self.workoutListHistory = workoutHistory?.data
                                    //
                                    //                            }
                                    self.isLoadingList = true
                                    DispatchQueue.main.async {
                                        self.tableView.isHidden = false
                                        self.noAppointmentsLabel.isHidden = true
                                    }
                                    
                                    self.showEmptyList()
                                }else{
                                    self.isLoadingList = false
                                    DispatchQueue.main.async {
                                        self.stopAnimating()
                                        self.tableView.isHidden = false
                                        self.noAppointmentsLabel.isHidden = true
                                    }
                                    
                                    self.showEmptyList()
                                    
                                }
                            
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                
                            }
                            
                        }
                        catch let error
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
//                                self.tableView.reloadData()
                            })
                            print("Received not-well-formatted JSON=>\(error.localizedDescription)")
                        }
                        
                    }else if statusCode == 429{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
//                            self.tableView.reloadData()
                        })
                    }
                }
    
            }).resume()
        }
        
    }
    
    func showEmptyList(){
        if self.selectedType == 0{
            if self.workoutListUpcoming?.count == 0{
            DispatchQueue.main.async {
                self.tableView.isHidden = true
                self.noAppointmentsLabel.text = "You have no upcoming classes to attend"
                self.noAppointmentsLabel.isHidden = false
            }
            }else{
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        }
        if self.selectedType == 1{
        if self.workoutListHistory?.count == 0{
            DispatchQueue.main.async {
                self.tableView.isHidden = true
                self.noAppointmentsLabel.text = "Your history is empty"
                self.noAppointmentsLabel.isHidden = false
            }
        }else{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            }
        }
        if self.selectedType == 2{
            if self.workoutListCancelled?.count == 0{
                DispatchQueue.main.async {
                    self.tableView.isHidden = true
                    self.noAppointmentsLabel.text = "No cancelled workouts"
                    self.noAppointmentsLabel.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func workoutSatusDecider(forDate date: String,forTime time: String)->Bool{
        let slotTime = date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let today = Date()
        let todayStr = formatter.string(from: Date())
        print("today is \(todayStr)")
        let toDayFormatted = formatter.date(from: todayStr)
        
        let resDate = formatter.date(from: slotTime)
        let day = Calendar.current.dateComponents([.day], from: toDayFormatted!, to: resDate!).day
        print("days \(day)")
        
        
        
//        let curDate = formatter.date(from: curDateStr)
        let date = Date()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm a"
        let curStr = formatter.string(from: date)
        print("curDate \(curStr)")
        print("res date \(slotTime)")
        
        //calculating time part
        let resTime = time.split(separator: " ")
        let curTime = curStr.split(separator: " ")
        
        print("res \(resTime[0]) \(resTime[1])")
        
        let resHHMM = resTime[0].split(separator: ":")
        let curHHMM = curTime[0].split(separator: ":")
        
        let resAMType = resTime[1].uppercased()
        let curAMType = curTime[1].uppercased()
        
        //true- upcoming
        //false- history
        if day! < 0{
            return false
        }else if day! > 0{
            return true
        }else{
            if resAMType == "AM" && curAMType == "PM"{
                return false
            }else if resAMType == "PM" && curAMType == "AM"{
                return true
            }else{
                //hours calculation goes here
                if Int("\(resHHMM[0])")! > Int("\(curHHMM[0])")!{
                    return true
                }else if Int("\(resHHMM[0])")! == Int("\(curHHMM[0])")!{
                    if Int("\(resHHMM[1])")! > Int("\(curHHMM[1])")!{
                        return true
                    }else{
                        return false
                    }
                }else{
                    return false
                }
            }
        }
        
    }
    
    var selectedType = 0
    
    @IBAction func previousNUpcomingSegmentTapped(_ sender: UISegmentedControl) {
//        self.nextPageUrl = ""
//        self.workoutListHistory!.removeAll()
        print("its working fine bro \(sender.selectedSegmentIndex)")
        if sender.selectedSegmentIndex == 0 {
            self.workoutListUpcoming?.removeAll() //uncomment to get single api func
            selectedType = 0
        }else if sender.selectedSegmentIndex == 1 {
            self.workoutListHistory?.removeAll() //uncomment to get single api func
            selectedType = 1
        }else if sender.selectedSegmentIndex == 2 {
            self.workoutListCancelled?.removeAll() //uncomment to get single api func
            selectedType = 2
        }
        
        //Uncomment to get single api func - upcoming, history, cancelled
        /*
        self.tableView.isHidden = false
        self.noAppointmentsLabel.isHidden = true
        self.showEmptyList()
         */
        
        //Implemented to get seperate api func upcoming, history, cancelled
        currentPage = 1
        self.isLoadingList = true
        self.getAppointmentsList(pageNo:currentPage , tag:0)
    }
    
    
     //Method checks if appointmets count and according to that shows or hides tableview
 
    func hideTableView() {
        if workoutListHistory!.count == 0 {
            self.tableView.isHidden = true
            self.noAppointmentsLabel.isHidden = false
            self.noAppointmentsLabel.text = noAppointmetns
        }else {
            self.noAppointmentsLabel.isHidden = true
            self.tableView.isHidden = false
        }
    }
 
   
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == StoryboardSegueIDS.ID_WELLNESS_CLASS_CANCELLATION {
            let destVC = segue.destination as! WellnessClassCancellationVC
            destVC.workoutInfo = workoutListUpcoming![selectedWorkout]
            destVC.delegate = self
            destVC.cancelPos = selectedWorkout
        }
    }
    func changeDateFormatte(classInfo: WellnessTrackerInner)->String{
        if let strDate = classInfo.booked_for_date{
            let strConvertedDate :  String = self.dateString(date: self.dateStringToDate(string: strDate, format: "yyyy-MM-dd"), format: "dd-MMM-yyyy")
            return strConvertedDate+" "+(classInfo.expected_time != nil ? classInfo.expected_time! : "")
        }
        return ""
    }
}
    
    




//MARK:- UITableViewDatasource ,UITableViewDelegate methods
extension WellnessWorkoutTracker : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*
        if selectedType, let workoutList = workoutListUpcoming?.count, workoutList>0{
            return workoutList
        }else if !selectedType,let workoutList = workoutListHistory?.count, workoutList>0{
            return workoutList
        }*/
        if selectedType == 0{
            if let workoutList = workoutListUpcoming?.count, workoutList>0{
                return workoutList
            }
        }
        else if selectedType == 1{
            if let workoutList = workoutListHistory?.count, workoutList>0{
                return workoutList
            }
        }
        else if selectedType == 2{
            if let workoutList = workoutListCancelled?.count, workoutList>0{
                return workoutList
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NearestClassesCell
        // let values = self.workoutListHistory[indexPath.row]
        if selectedType == 0{
            cell.configureHistoryCell(classInfo: self.workoutListUpcoming![indexPath.row])
            cell.time.text = self.changeDateFormatte(classInfo: self.workoutListUpcoming![indexPath.row])
            cell.extraIndicatorCnstrnt.constant = 20
        }else if selectedType == 1{
            cell.configureHistoryCell(classInfo: self.workoutListHistory![indexPath.row])
            cell.time.text = self.changeDateFormatte(classInfo: self.workoutListHistory![indexPath.row])
            cell.extraIndicatorCnstrnt.constant = 0
        }else if selectedType == 2{
            cell.configureHistoryCell(classInfo: self.workoutListCancelled![indexPath.row])
            cell.time.text = self.changeDateFormatte(classInfo: self.workoutListCancelled![indexPath.row])
            cell.extraIndicatorCnstrnt.constant = 0
        }
        
        
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedType == 0{
            selectedWorkout = indexPath.row
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_WELLNESS_CLASS_CANCELLATION, sender: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
}


extension WellnessWorkoutTracker : UIScrollViewDelegate {
    
   /*
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        
        if scrollOffset == 0
        {
            // then we are at the top
            print("YOure reached top")
        }
        else if scrollOffset + scrollViewHeight == scrollContentSizeHeight
        {
            // then we are at the end
            print("YOure reached Down")
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        if !decelerate {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= scrollView.contentSize.height {
                print("Scrolling did reach top")
                isScrollingFinished = true
                getAppointmentsList(urlStrin:  "", tag: self.selectedButtonTag)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Scrolling did end decelerating method called down")
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            isScrollingFinished = true
            getAppointmentsList(urlStrin:  "", tag: self.selectedButtonTag)
        }
    }*/
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && isLoadingList){
            self.loadMoreItemsForList()
            
            
//            self.isLoadingList = false
        }
    }
    
}

extension WellnessWorkoutTracker: CancelUpdateDelegate{
    func updateOnCancel(pos: Int) {
//        self.workoutListUpcoming?.remove(at: pos)
//        showEmptyList()
    }
    
    
}




