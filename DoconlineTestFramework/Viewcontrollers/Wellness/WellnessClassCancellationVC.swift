//
//  WellnessClassCancellationVC.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 26/06/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import MapKit


protocol CancelUpdateDelegate {
    func updateOnCancel(pos: Int)
}


class WellnessClassCancellationVC: UIViewController, NVActivityIndicatorViewable, CLLocationManagerDelegate {
    
    var workoutInfo: WellnessTrackerInner?
    
    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var outletAddress: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cancelTitle: UILabel!
    @IBOutlet weak var cancelReason: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet var cancelReasonPopup: UIView!
    @IBOutlet weak var reason: UITextView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var workoutImg: UIImageView!
    @IBOutlet weak var loaderIndicator: UIActivityIndicatorView!
    @IBOutlet weak var outletTitle: UILabel!
    @IBOutlet weak var bookId: UILabel!
    @IBOutlet weak var descTitle: UILabel!
    @IBOutlet weak var expectedTime: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var qrcodeImg: UIImageView!
    @IBOutlet weak var qrcodeBtn: UIButton!
    @IBOutlet weak var qrLabel: UILabel!
    @IBOutlet weak var cancelReasonOkBtn: UIButton!
    
    
    var container = UIView()
    
    var delegate: CancelUpdateDelegate?
    var cancelPos = 0
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.outletTitle.text = workoutInfo?.outlet_name
        self.bookId.text = "Booking ID: "+(workoutInfo?.booking_id)!
        workoutName.text = workoutInfo?.workout_name
        outletAddress.text = (workoutInfo?.address)!+","+(workoutInfo?.city)!
        var timeSchedule = ""
        if let fromTime = workoutInfo?.from_time{
            timeSchedule = fromTime
        }
        if let toTime = workoutInfo?.to_time{
            timeSchedule = timeSchedule+" To "+toTime
        }
        
        time.text = timeSchedule
        //expectedTime.text = "Slot booked at: "+(workoutInfo?.booked_for_date)!+" "+(workoutInfo?.expected_time)!
        expectedTime.text = self.changeDateFormatte(classInfo: workoutInfo!)
        
        if !(workoutInfo?.description.isEmpty)!{
            desc.text = workoutInfo?.description.replacingOccurrences(of: "<br/>", with: "")
            desc.isHidden = false
            descTitle.isHidden = false
        }
        
        cancelBtn.layer.cornerRadius = 10
        
        if workoutInfo?.is_attended == 1{
            cancelBtn.isHidden = true
            qrcodeImg.isHidden = true
            qrcodeBtn.isHidden = true
            qrLabel.isHidden = true
        }
        
        workoutImg.loadImage(workoutID: Int((workoutInfo?.workout_id)!)!, indicator: loaderIndicator)
        /*
        if workoutInfo?.is_cancelled == 0{
            cancelTitle.isHidden = true
            cancelReason.isHidden = true
        }else{
            cancelReason.text = workoutInfo?.cancel_reason
            cancelBtn.isHidden = true
        }*/
        self.navigationController?.navigationBar.isHidden = true
        reason.delegate = self
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        imgBack.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        imgBack.layer.shadowColor = UIColor.black.cgColor
        imgBack.layer.shadowRadius = 4
        imgBack.layer.shadowOpacity = 0.25
        imgBack.layer.masksToBounds = false;
        imgBack.clipsToBounds = false;
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scroller.contentOffset.y = 0
        cancelReasonOkBtn.backgroundColor = Theme.buttonBackgroundColor
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let sourceLatitude = locValue.latitude
        let sourceLongitude = locValue.longitude
        locationManager.stopUpdatingLocation()
        let coords = workoutInfo?.lat_long!.split(separator: ",")
        let destLatitude: CLLocationDegrees = ("\(coords![0])" as NSString).doubleValue
        let destLongitude: CLLocationDegrees = ("\(coords![1])" as NSString).doubleValue
        
        //        if (UIApplication.shared.canOpenURL(URL(string:"http://maps.google.com/")!))
        //        {
        UIApplication.shared.open(URL(string: "http://maps.google.com/?daddr=\(destLatitude),\(destLongitude)&saddr=\(sourceLatitude),\(sourceLongitude)")!, options: [:], completionHandler: nil)
        
        //        } else
        //        {
        //            UIApplication.shared.open(URL(string: "http://maps.apple.com/?daddr=\(destLatitude),\(destLongitude)&saddr=\(sourceLatitude),\(sourceLongitude)")!, options: [:], completionHandler: nil)
        //        }
        
    }
    

    @IBAction func showMap(_ sender: UIButton) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

    @IBAction func doCancel(_ sender: UIButton) {
        let alert = UIAlertController(title: "Cancel Workout", message: "Are you sure you want to cancel workout", preferredStyle: UIAlertController.Style.alert)
        let okay = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.reason.text = "Please specify reason"
            self.reason.textColor = UIColor.lightGray
            
            self.container.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
            self.container.backgroundColor = .black
            self.container.alpha = 0.7
            self.view.addSubview(self.container)
            self.cancelReasonPopup.frame = self.view.bounds
            self.view.addSubview(self.cancelReasonPopup)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okay)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func cancelPopup(_ sender: UIButton) {
        container.removeFromSuperview()
        cancelReasonPopup.removeFromSuperview()
    }
    
    
    @IBAction func reasonCancelOk(_ sender: UIButton) {
        
        if self.reason.textColor == UIColor.lightGray{
            self.didShowAlert(title: "Alert", message: "Please enter reason")
        }else{
            print("entered \(self.reason.text ?? "")")
            
            if self.reason.text!.rangeOfCharacter(from: CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -,.01234567890").inverted) == nil
            {
                print("reason count is \(self.reason.text!.count)")
                if self.reason.text!.count >= 5 && self.reason.text!.count <= 200{
                    if self.reason.text!.rangeOfCharacter(from: CharacterSet(charactersIn: " -,.01234567890").inverted) == nil{
                        self.didShowAlert(title: "Alert", message: "Plaese enter valid reason")
                    }else{
                        self.doCancelWorkout()
                    }
                }else{
                    self.didShowAlert(title: "Alert", message: "Reason must be atleast 5-200 charaters")
                }
            }
            else {
                self.didShowAlert(title: "Alert", message: "Special characters are not allowed")
            }
        
        }
        
    }
    
    
    func doCancelWorkout(){
    
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        self.startAnimating()
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let cancelURL = AppURLS.URL_WELLNESS_WORKOUT_CANCEL
        let url = URL(string: cancelURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        
        
        
        let postData = "\(Keys.KEYS_WORKOUT_CANCEL_WORKOUTID)=\(Int(self.workoutInfo!.workout_id!)!)&\(Keys.KEYS_WORKOUT_CANCEL_BOOKINGID)=\(Int(self.workoutInfo!.booking_id!)!)&\(Keys.KEYS_WORKOUT_CANCEL_REASON)=\(self.reason.text!)" //\(self.classDetail!.FromTime)
        
        print("booking data \(postData)")
        request.httpBody = postData.data(using: String.Encoding.utf8)
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    //                    self.vw_activity_indicator.isHidden = true
                    //                    self.activityIndicator.stopAnimating()
                    self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    let res = String(data: data!, encoding: String.Encoding.utf8)
                    print("error cancel \(res!)")
                })
            }
            //            print("res wellness-LOCAL \((response as! HTTPURLResponse).statusCode)")
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if let _ = response, statusCode == 200{
                do{
                    let res = String(data: data!, encoding: String.Encoding.utf8)
                    print("book res \(res!)")
                    
                    let cancelRes = try? JSONDecoder().decode(WellnessCancelClass.self, from: data!) as WellnessCancelClass
                    
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        let alert = UIAlertController(title: "Success!!", message: cancelRes!.message , preferredStyle: UIAlertController.Style.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {
                            action in
//                            self.delegate?.updateOnCancel(pos: self.cancelPos)
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
            }else{
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
                print("codeeee \(statusCode)")
//                self.didShowAlert(title: "Sorry!!", message: "Looks like you have already registered for this class")
                                self.checkOnlyStatus(statusCode: statusCode)
            }
        }).resume()
        
    }
    
    
    func doMarkAttendance(outletId: String){
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        self.startAnimating()
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let cancelURL = AppURLS.URL_WELLNESS_WORKOUT_ATTENDANCE
        let url = URL(string: cancelURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.httpMethod = HTTPMethods.POST
        
        
        
        let postData = "\(Keys.KEYS_WORKOUT_ATTENDANCE_BOOKINGID)=\(Int((self.workoutInfo?.booking_id)!)!)&\(Keys.KEYS_WORKOUT_ATTENDANCE_OUTLETID)=\(outletId)" //\(self.classDetail!.FromTime)
        
        print("booking data \(postData)")
        request.httpBody = postData.data(using: String.Encoding.utf8)
        
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    //                    self.vw_activity_indicator.isHidden = true
                    //                    self.activityIndicator.stopAnimating()
                    self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    let res = String(data: data!, encoding: String.Encoding.utf8)
                    print("error cancel \(res!)")
                })
            }
            //            print("res wellness-LOCAL \((response as! HTTPURLResponse).statusCode)")
            let statusCode = (response as! HTTPURLResponse).statusCode
            let attendanceRes = try? JSONDecoder().decode(WellnessAttendance.self, from: data!) as WellnessAttendance
            if let _ = response, statusCode == 200{
                do{
                    let res = String(data: data!, encoding: String.Encoding.utf8)
                    print("book res \(res!)")
                    
                    
                    let alert = UIAlertController(title: "Alert", message: (attendanceRes?.message)!, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: {
                        action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
            }else if statusCode == 422{
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
                self.didShowAlert(title: "Sorry!!", message: (attendanceRes?.message)!)
                
            }else{
                self.checkOnlyStatus(statusCode: statusCode)
            }
        }).resume()
    }
    
    @IBAction func scanCodeForAttendance(_ sender: UIButton) {
        print("its clicked")
        
        let vc = QRScannerController()
        vc.qrScannerDelegate = self
        let closeBtn = UIButton(frame: CGRect(x: vc.view.bounds.width-80, y: 20, width: 60, height: 30))
        closeBtn.setTitle("Cancel", for: .normal)
        closeBtn.addTarget(self, action: #selector(self.closeQRVC(sender:)), for: .touchUpInside)
        vc.view.addSubview(closeBtn)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func closeQRVC(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func changeDateFormatte(classInfo: WellnessTrackerInner)->String{
        if let strDate = classInfo.booked_for_date{
            let strConvertedDate :  String = self.dateString(date: self.dateStringToDate(string: strDate, format: "yyyy-MM-dd"), format: "dd-MMM-yyyy")
            return strConvertedDate+" "+(classInfo.expected_time != nil ? classInfo.expected_time! : "")
        }
        return ""
    }
    
}


extension WellnessClassCancellationVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please type your text here"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension WellnessClassCancellationVC: QrScannerDelegate {
    func getScannedData(data: String) {
        print(data) //here's the scan result
        self.dismiss(animated: true, completion: nil)
         self.doMarkAttendance(outletId: data)
        
    }
}
