//
//  StdWrkoutCell.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 19/08/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit

class StdWrkoutCell: UITableViewCell {
    
    
    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var workoutContainer: UIView!
    @IBOutlet weak var workoutImg: UIImageView!
    @IBOutlet weak var workoutImgWidth: NSLayoutConstraint!
    @IBOutlet weak var loaderIndicator: UIActivityIndicatorView!
    @IBOutlet weak var extraIndicatorView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        workoutImgWidth.constant = workoutImg.bounds.height
        if let workoutContainerView = workoutContainer, let extraViewer = extraIndicatorView{
            workoutContainer.addShadowLayer(offset: CGSize.init(width: 5, height: 5), color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.7), radius: 10.0, opacity: 0.6)
            
            workoutImg.clipsToBounds = true
            workoutImg.layer.cornerRadius = 10.0
            
            extraIndicatorView.clipsToBounds = true
            extraIndicatorView.layer.cornerRadius = 10.0
            if #available(iOS 11.0, *) {
                extraIndicatorView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
                //            extraIndicator.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
            }
            extraIndicatorView.backgroundColor = Theme.buttonBackgroundColor
        }
    }
    
    func configCell(workout info: NSDictionary, distance: Float){
        workoutName.text = info["cn"] as? String ?? " "
        time.text = "\(info["ft"] as! String) - \(info["tt"] as! String)"
        self.distance.text = "\(distance)km"
        self.workoutImg.loadImage(workoutID: info["WorkoutID"] as? Int ?? 0, indicator: loaderIndicator)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
