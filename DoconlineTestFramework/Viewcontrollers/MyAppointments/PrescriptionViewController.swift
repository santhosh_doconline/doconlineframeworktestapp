//
//  PrescriptionViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 05/08/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
import WebKit

class PrescriptionViewController: UIViewController {
//,URLSessionDownloadDelegate ,UIDocumentInteractionControllerDelegate
    @IBOutlet weak var containerOfWebView: UIView!
    @IBOutlet var stackVw: UIStackView!
    @IBOutlet var bt_diagnostic: UIButton!
    @IBOutlet weak var LC_stack_vw_height: NSLayoutConstraint!
    var webView:WKWebView!
    ///google doc view base url
    var googleDocViewURL = "http://docs.google.com/gview?embedded=true&url="
    
    ///appointment id
    var appointment_id = ""
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bt_procure_medicine: UIButton!
    
    let html = ""
    
    ///Instance var for download task
    var downloadTask: URLSessionDownloadTask!
    ///URLSession instance var reference
    var backgroundSession: URLSession!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
//        backgroundSession = URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        self.LC_stack_vw_height.constant = 0.0
        self.stackVw.removeArrangedSubview(self.bt_procure_medicine!)
        self.bt_procure_medicine?.isHidden = true
        
        self.stackVw.removeArrangedSubview(self.bt_diagnostic!)
        self.bt_diagnostic?.isHidden = true
        
        getPrescriptionDetails()
       // self.buttonCornerRadius(buttons: [bt_procure_medicine])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bt_diagnostic.backgroundColor = Theme.buttonBackgroundColor
        bt_procure_medicine.backgroundColor = Theme.buttonBackgroundColor
    }

    ///loads the pdf with google doc view
    func googleDocView() {
        let urlStrng = self.googleDocViewURL + "\(AppURLS.URL_Prescription)\(self.appointment_id)/pdf"
        print("Doc url : \(urlStrng)")
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        self.webView = WKWebView(frame: self.containerOfWebView.bounds, configuration: config)
        self.webView.clipsToBounds = true
        self.containerOfWebView.addSubview(self.webView)
        let url = URL(string: urlStrng)
        let request = URLRequest(url: url!)
        self.webView.load(request)
    }
    
    /**
      Method gets prescription details and shows the prescription
     */
    func getPrescriptionDetails() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
           // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            self.activityIndicator.stopAnimating()
            return
        }
        
        self.activityIndicator.startAnimating()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: "\(AppURLS.URL_Prescription)\(self.appointment_id)")
            print("URL==>\(AppURLS.URL_Prescription)\(self.appointment_id)")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
           // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request) { (data, response, error) in
                if error != nil
                {
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: (error?.localizedDescription)!)
                      //  AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                        self.activityIndicator.stopAnimating()
                    })
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        
                        let htmlString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("html string:==>\(htmlString!) **end")
                        DispatchQueue.main.async(execute: {
                            //self.webView.loadHTMLString("\(htmlString!)" , baseURL: nil)
                            if htmlString!.contains("<div style=\"display: inline-block;width: 100%;margin-bottom: 5px;box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;border: 1px solid #f3f3f3;padding: 10px; padding-bottom:2px;\">")
                            {
                                self.stackVw.addArrangedSubview(self.bt_procure_medicine!)
                                self.bt_procure_medicine?.isHidden = false
                                self.LC_stack_vw_height.constant = 40.0
                            }
                            else
                            {
                                self.stackVw.removeArrangedSubview(self.bt_procure_medicine!)
                                self.bt_procure_medicine?.isHidden = true
                            }
                            
                            if htmlString!.contains("Lab Tests:")
                            {
                                self.stackVw.addArrangedSubview(self.bt_diagnostic!)
                                self.bt_diagnostic?.isHidden = false
                                self.LC_stack_vw_height.constant = 40.0
                            }
                            else
                            {
                                self.stackVw.removeArrangedSubview(self.bt_diagnostic!)
                                self.bt_diagnostic?.isHidden = true
                            }
                            
                            let preferences = WKPreferences()
                            preferences.javaScriptEnabled = true
                            
                            let config = WKWebViewConfiguration()
                            config.preferences = preferences
                            self.webView = WKWebView(frame: self.containerOfWebView.bounds, configuration: config)
                            self.webView.clipsToBounds = true
                            self.containerOfWebView.addSubview(self.webView)
                            self.webView.loadHTMLString("\(htmlString!)", baseURL: nil)
                            self.activityIndicator.stopAnimating()
                        })
                        
                        
                    }
                }
            }.resume()
        }
    
     }
    
    @IBAction func procure_medicine_tapped(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ENTER_PINCODE, sender: sender)
    }
    
    @IBAction func diagnostic_tapped(_ sender: UIButton) {
        
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "NavigateDiagnostics"), object: nil)
    }
    
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        
//        
        let url = URL(string: "\(AppURLS.URL_Prescription)\(self.appointment_id)/pdf")!
        print("Donload url : \(url)")
//        downloadTask = backgroundSession.downloadTask(with: url)
//        downloadTask.resume()
        
        //downloadPDF()
    }
    
/*
    func downloadPDF(){
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let destinationFileUrl = documentsUrl.appendingPathComponent("Appointment_id_\(self.appointment_id).jpg")
        
        //Create URL to the source file you want to download
        let fileURL = URL(string: "\(AppURLS.URL_Prescription)\(self.appointment_id)/pdf")
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL!)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                     self.showFileWithPath(path: destinationFileUrl.path)
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
            }
        }
        task.resume()
    }
    

    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/Appointment_id_\(self.appointment_id).pdf"))
        print("Document URL :\(destinationURLForFile)")
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            showFileWithPath(path: destinationURLForFile.path)
        }
        else{
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                // show file
                showFileWithPath(path: destinationURLForFile.path)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
   
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
        progressView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
    }
    
    
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?){
        downloadTask = nil
        progressView.setProgress(0.0, animated: true)
        if (error != nil) {
            print(error!.localizedDescription)
        }else{
            print("The task finished transferring data successfully")
        }
    }

    func showFileWithPath(path: String){
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }
    
    
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }

    */
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_ENTER_PINCODE {
            if let destVC = segue.destination as? UIOrederProcessViewController {
                destVC.appointment_id = self.appointment_id
            }
        }
    }
    

}

extension String {
    
    func contains(_ find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(_ find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
