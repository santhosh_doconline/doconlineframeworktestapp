//
//  AppointmentSummaryViewController.swift
//  DocOnline
//
//  Created by dev-3 on 13/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
//import BSImagePicker
import Photos
import SwiftMessages
import UserNotifications
//import Firebase
//import BIZPopupView
import MobileCoreServices




/*
 * Appointment summay view class
 */

class AppointmentSummaryViewController: UIViewController ,NVActivityIndicatorViewable ,RatingViewDelegate,CustomCosmosRatingDelegate{

    //new UI
    @IBOutlet var iv_patientPic: UIImageView!
    @IBOutlet var lb_patientDetails: UILabel!
    @IBOutlet var lb_doctorSpecialisation: UILabel!
    @IBOutlet var rv_doctorRating: CosmosView!
    @IBOutlet var lb_doctorRemarks: UILabel!
    @IBOutlet var lb_provisionalDiagnosis: UILabel!
    @IBOutlet var iv_typeOfCall: UIImageView!
    @IBOutlet var tv_notes: UILabel!
    @IBOutlet var vw_rateDoctor: UIView!
    @IBOutlet var bt_editNotes: UIButton!
    @IBOutlet var lb_doctorRatingAverage: UILabel!
    @IBOutlet var bt_view_prescription: UIButton!
    @IBOutlet weak var loaderIndicator: UIActivityIndicatorView!
    
    @IBOutlet var lb_rateDoctorTitle: UILabel!
    
    @IBOutlet weak var lb_followUpDate: UILabel!
    
    //outlets
    @IBOutlet weak var iv_doctorPic: UIImageView!
    @IBOutlet weak var bt_attachPhotos_outlet: UIButton!
    @IBOutlet weak var lb_cancelled_status: UILabel!
    @IBOutlet weak var lb_no_attachments: UILabel!
    
    
    @IBOutlet var lb_bookingID: UILabel!
    
    @IBOutlet weak var lb_symptom: UILabel!
    @IBOutlet weak var lb_consultationfor: UILabel!
    @IBOutlet weak var lb_dr_name: UILabel!
    @IBOutlet weak var lb_typeOfCall: UILabel!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var bt_cancel_outlet: UIButton!
    @IBOutlet weak var bt_calendar_outlet: UIButton!
    @IBOutlet weak var vw_blank: UIView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imagesCVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lb_notes: UILabel!
    @IBOutlet weak var lb_patient_name: UILabel!
    @IBOutlet weak var vw_rating: CosmosView!
    @IBOutlet weak var LC_rating_height: NSLayoutConstraint!
    @IBOutlet weak var caledar_width: NSLayoutConstraint!
    @IBOutlet weak var cancel_width: NSLayoutConstraint!
    @IBOutlet weak var lb_symptomSeverity: UILabel!
    
    @IBOutlet var ratings: [UIImageView]!
    @IBOutlet var ratingBtns: [UIButton]!
    @IBOutlet weak var vwMoreWidth: NSLayoutConstraint!
    
    
    
    ///linear bar animation instance
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    ///appointment id instance
    var appointmetnID = ""
    
    ///upcoming or previous
    var isUpcoming = 0
    
    ///instance for appointment data modal
    var dataModel : Appointments!
    
    ///instance variable for patient data modal
    var patientModal : Patient!
    
    ///instance for doctor modal
    var doctorModal : Doctor!
    
    ///Booked for self or family variable instance
    var bookedfor = 0
    
    ///Booked for self or family variable instance
    var publicAppointmentID = ""
    
    ///call type video or audio
    var calltype = 0
    
    ///call channel internet or regular
    var callChannel = 0
    
    ///User added notes
    var addedNotes = ""
    
    ///Doctor added notes
    var doctorNotes = ""
    
    ///Doctor added symptoms
    var doctorSymptoms = ""
    
    ///Doctor Provisional diagnosis
    var doctorProvisional = ""
    
    ///Doctor Followup Date
    var doctorFollowupDate = ""
    
    ///Doctor added symptoms severity
    var doctorSymptomsSeverity = ""
    
    ///status to show cancel appoitment(1) or view prescription(2) or hide button(0)
    var consultation_status : Int!
    
    ///date of appointment
    var scheduledat = ""
    
    ///if images appended to appointment stores in attachments
    var attachments = [String]()
    
    ///Newly added for caption images
    var tempNewAtachments = [ImagesCollectionViewCellModal]()
    var isNewUpload = true
    var selectedIndexOfImages = 0
    
    ///if user wants to attach images selected images array
    var images = [UIImage]()
    
    ///instance to store selected image from attachments
    var selectedAttachment = ""
    
    ///instance to get selected image from images array
    var selectedImage : UIImage!
    
    ///used for showing attach images button if appointment is in future
    var minutes : Int!
    
    ///image picker instance
    let picker = UIImagePickerController()
    
    ///instance for picked image
    var pickedImage : UIImage!
    var previouslyPickedImage : UIImage!      //not used
    
    /// checks if attached photos through camera and show alert
    var isPictureTakenByCamera = false
    var user_id = ""
    
    ///default rating
    var appointment_rating = 0
    static var doctorRatingView : BIZPopupViewController?
    
    var isEventExist = false
    
    //check time and status off app to show remove file option
    var isActiveAndAvailbleToRemoveFile = true
    
    var emptyEmoji = [UIImage(named: "very_poor"),UIImage(named: "poor"),UIImage(named: "good"),UIImage(named: "satisfied"),UIImage(named: "awesome")]
    var filledEmoji = [UIImage(named: "very_poor_filled"),UIImage(named: "poor_filled"),UIImage(named: "good_filled"),UIImage(named: "satisfied_filled"),UIImage(named: "awesome_filled")]
    
    var transparentBackground: UIView!
    var webVW: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rv_doctorRating.settings.fillMode = .precise
//        self.vw_rating.settings.fillMode = .full
        self.lb_typeOfCall.textColor = Theme.buttonBackgroundColor
        self.bt_attachPhotos_outlet.setTitleColor(Theme.buttonBackgroundColor, for: .normal)
        //Ask permissions
        didAskForCameraPermission()
        didAskForPhotoLibraryPermission()
        self.loaderIndicator.color = Theme.buttonBackgroundColor
        configureLinearProgressBar()
        updateRatingView(hide: true, height: 0)
         ///to show blacnk when fetching appointemt details
        self.vw_blank.isHidden = false
        
        picker.delegate = self
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        
      //  makeProfilePicRound()
        getAppointmentDetails()
        
        
        
      //  buttonCornerRadius(buttons: [self.bt_cancel_outlet,self.bt_calendar_outlet])
    }
    
    
    
    /**
     Adding tap gesture to rating view
     */
    func addTapGestureToRating() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.performActionOnRatingTap(sender:)))
        if self.appointment_rating > 0 {
           self.vw_rating.addGestureRecognizer(gesture)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bt_view_prescription.backgroundColor = Theme.buttonBackgroundColor
         bt_calendar_outlet.backgroundColor = Theme.buttonBackgroundColor
    }
    
    @IBAction func ratingPicker(_ sender: UIButton) {
        if self.appointment_rating > 0 {
            ratingBtns.forEach{
                $0.isUserInteractionEnabled = false
            }
            for i in 0..<self.ratings.count{
                if i == Int(self.appointment_rating-1){
                    self.ratings[i].image = filledEmoji[i]
                }else{
                    self.ratings[i].image = emptyEmoji[i]
                }
            }
        }else{
            for i in 0..<self.ratings.count{
                if i == Int(sender.tag-1){
                    self.ratings[i].image = filledEmoji[i]
                }else{
                    self.ratings[i].image = emptyEmoji[i]
                }
            }
            DispatchQueue.main.async {
                guard let ratingView = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_DOCTOR_RATING_VIEW) as? RatingView else { return }
                ratingView.isImmediateAfterCall = false
                ratingView.delegate = self
                App.timeDuration = ""
                ratingView.docotorDetails = self.doctorModal
                ratingView.defaultRating = Double(sender.tag)
                ratingView.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 300)
                let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: ratingView)
                segue.configure(layout: .bottomCard)
                segue.dimMode = .blur(style: .dark, alpha: 0.7, interactive: false)
                segue.messageView.configureDropShadow()
                segue.presentationStyle = .bottom
                self.prepare(for: segue, sender: nil)
                segue.perform()
            }
        }
    }
    
    
    func didSetRating(_ rating: Double) {
        self.vw_rating.text = "\(rating)"
        guard rating != 0 else { return}
        DispatchQueue.main.async {
            guard let ratingView = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIds.ID_DOCTOR_RATING_VIEW) as? RatingView else { return }
            ratingView.isImmediateAfterCall = false
            ratingView.delegate = self
            App.timeDuration = ""
            ratingView.docotorDetails = self.doctorModal
            ratingView.defaultRating = rating
            ratingView.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 300)
            let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: ratingView)
            segue.configure(layout: .bottomCard)
            segue.dimMode = .blur(style: .dark, alpha: 0.7, interactive: false)
            segue.messageView.configureDropShadow()
            segue.presentationStyle = .bottom
            self.prepare(for: segue, sender: nil)
            segue.perform()
        }
    }
    
    /**
     Shows rating view to user on rating tap
     */
    @objc func performActionOnRatingTap(sender:UITapGestureRecognizer) {
        print("Rating view tapped")
        if self.appointment_rating > 0 {
            self.didShowAlert(title: "Info", message: "You have already rated this appointment")
        }
    }
 
    //MARK:- RatingViewDelegate Method implementation
    func didTapeSubmit(_ success: Bool, rating: Double, jsonData: Data) {
         self.dismiss(animated: true, completion: {
            let ratingURL = AppURLS.URL_BookAppointment + "/\(self.appointmetnID)/rating"
            self.getRatingStatus(urlString: ratingURL, httpMethod: HTTPMethods.POST, jsonData: jsonData, type: 2, completionHandler: { (success, response) in
                if success {
                    DispatchQueue.main.async {
                        self.lb_rateDoctorTitle.text = "Rated"
                        for i in 0..<self.ratings.count{
                            if i == Int(rating-1){
                                self.ratings[i].image = self.filledEmoji[i]
                            }else{
                                self.ratings[i].image = self.emptyEmoji[i]
                            }
                        }
//                        self.vw_rating.rating = rating
//                        self.vw_rating.text = "\(rating)"
                        self.appointment_rating = Int(rating)
//                        self.vw_rating.delegate = rating > 0 ? nil : self
//                        self.vw_rating.updateOnTouch = rating > 0 ? false : true
//                        self.addTapGestureToRating()
                        self.showSuccessMessage(title: "Rating submitted", message: "Thank you for your feedback" ,duration: .seconds(seconds: 4))
                        
                    }
                }else {
                    DispatchQueue.main.async {
//                        self.vw_rating.rating = 0
//                        self.vw_rating.text = "0"
                    }
                }
            })
        })
    }
    
    func didTapNotNow() {
        for i in 0..<self.ratings.count{
                self.ratings[i].image = emptyEmoji[i]
        }
        
//        self.vw_rating.rating = 0
//        self.vw_rating.text = "0"
    }
    
    
    
    /**
     Configuring linear bar color
     */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.linearBar.stopAnimation()
    }

    /**
       Method stops animation shows success toast message
     */
     func toStopAnimating() {
        print("Animation stopped**")
        DispatchQueue.main.async(execute: {
            self.stopAnimating()
            self.isPictureTakenByCamera = false
            //Displays alert message success if images uploaded
            let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
            view.configureTheme(.success)
           // view.configureContent(title: "Success", body: "Photos have been added to your appointment")
            view.configureDropShadow()
            view.configureContent(title: "Success", body: "Photos have been updated to your appointment", iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
                SwiftMessages.hide()
            }
            
            App.ImagesArray.removeAll()
            
            var config = SwiftMessages.defaultConfig
            config.duration = .seconds(seconds: 3)
            SwiftMessages.show(config: config, view: view)
            self.imagesCollectionView.reloadData()
            
            if App.imagesURLString.count != 0 {
               self.attachments.removeAll()
               self.attachments = App.imagesURLString
               self.adjustViewPhotos(tag: self.attachments as [String])
               self.imagesCollectionView.reloadData()
            }
        })
    }
    
    ///displays success message after request
    func showSuccessMessage(title:String, message:String,duration:SwiftMessages.Duration) {
        let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
        view.configureTheme(.success)
        view.configureDropShadow()
        view.configureContent(title: title, body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = duration
        SwiftMessages.show(config: config, view: view)
    }

    
    /**
       Displays alert message if images not uploaded
     */
    func failedMessageView()
    {
        DispatchQueue.main.async {
            self.stopAnimating()
            self.isPictureTakenByCamera = false
            let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
            view.configureTheme(.error)
            // view.configureContent(title: "Sorry!", body: "Photos failed to upload")
            view.configureDropShadow()
            view.configureContent(title: "Sorry!", body: "Photos failed to upload", iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
                SwiftMessages.hide()
            }
            
            var config = SwiftMessages.defaultConfig
            config.duration = .seconds(seconds: 10)
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    
    
    /**
     method used to make profile pic round
     */
    func makeProfilePicRound() {
        self.iv_doctorPic.layer.cornerRadius = self.iv_doctorPic.frame.size.width / 2
        self.iv_doctorPic.layer.borderColor = UIColor.red.cgColor
        self.iv_doctorPic.layer.borderWidth = 1
        self.iv_doctorPic.clipsToBounds = true
        
        self.iv_doctorPic.layer.shadowOpacity = 0.5
        self.iv_doctorPic.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        self.iv_doctorPic.layer.shadowRadius = 5.0
        self.iv_doctorPic.layer.shadowColor = UIColor.gray.cgColor
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isPictureTakenByCamera {
            print("Viewdid appear after picking photo")
           // self.imagesCollectionView.reloadData()
          //  self.adjustViewPhotos(tag: App.ImagesArray as [UIImage])
           // self.uploadSelectedImagesToServer(id: Int(self.appointmetnID)!, tag: 4)
            //showImagesConfrimationAlert()
        }

    }
    
    override func willMove(toParent parent: UIViewController?) {
        App.ImagesArray.removeAll()
    }
    
    /**
     Method uploads images to server with parameters
     - Parameter id: appoitment id should be passed
     - Parameter tag: to know from which class it is called
     */
    func uploadSelectedImagesToServer(id:Int,tag:Int) {
       
        if App.ImagesArray.count != 0 {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
                return
            }
           
            startAnimating()
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let boundary = generateBoundaryString()
            let urlString = "\(AppURLS.URL_BookAppointment)/\(id)/attachments"
            print("ImageUrl = \(urlString)")
            let url = URL(string: urlString)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethods.POST
            request.httpBody = createBodyWithParameters(parameters: App.ImagesArray, boundary: boundary) as Data
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling uploading consultation images:\(resultJSON)")
                        }
                        else {
                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            //clear images array after successfull upload **
                            if httpResponse.statusCode == 201 || httpResponse.statusCode == 200{
                                App.ImagesArray.removeAll()
                                App.imagesURLString.removeAll()
                                if tag == 4 {
                                    if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                        for image in data {
                                            let fileName = image.object(forKey: Keys.KEY_FILE_NAME) as! String
                                            App.imagesURLString.append(fileName)
                                        }
                                    }
                                   self.toStopAnimating()
                                }
                            }else {
                                if tag == 4 {
                                   self.failedMessageView()
                                }
                            }
                            print("Images upload response:\(resultJSON)  status:\(status)")
                        }
                    }catch let error{
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
            
        }else {
            print("User not logged in")
        }
    }

    /**
     Method performs request to get appointment details
     */
    func getAppointmentDetails() {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
          //  AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
       // startAnimating()
        DispatchQueue.global(qos: .userInitiated).async {[weak self] in
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: "\(AppURLS.URL_BookAppointment)/\(self!.appointmetnID)")
            print("URL==>\(AppURLS.URL_BookAppointment)/\(self!.appointmetnID)")
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request) { (data, response, error) in
                if error != nil
                {
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print("appointment summary is \(jsonData)")
                            let errorStatus = self!.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            
                            if !errorStatus
                            {
                                print("performing error handling get appointment details :\(jsonData)")
                            }
                            else
                            {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                print("app details data=\(data)")
                                if code == 200 && responseStatus == "success"
                                {
                                    
                                    print("**")
                                    if let publicAppID = data.object(forKey: Keys.KEY_PUBLIC_APPOINTMENT_ID) as? String {
                                        self!.publicAppointmentID = publicAppID
                                    }
                                    
                                    if let bookedFor = data.object(forKey: Keys.KEY_BOOKED_FOR_USER_ID) as? Int {
                                        self!.bookedfor = bookedFor
                                    }
                                    
                                    if let callType = data.object(forKey: Keys.KEY_CALL_TYPE) as? Int {
                                        self!.calltype = callType
                                    }
                                    
                                    if let callChannel = data.object(forKey: Keys.KEY_CALL_CHANNEL) as? Int {
                                        self?.callChannel = callChannel
                                    }
                                    
                                    if let notes = data.object(forKey: Keys.KEY_NOTES) as? String {
                                        self!.addedNotes = notes
                                    }
                                    
                                    if let dnotes = data.object(forKey: Keys.KEY_DOCTOR_NOTES) as? String {
                                        self!.doctorNotes = dnotes
                                    }
                                    
                                    if let symptom = data.object(forKey: Keys.KEY_SYMPTOM) as? NSDictionary{
                                        if let symptom_name = symptom.object(forKey: Keys.KEY_SYMPTOM) as? String
                                        {
                                            self!.doctorSymptoms = symptom_name
                                            print("symptom name is \(symptom_name)")
                                        }
                                        if let symptom_severity = symptom.object(forKey: Keys.KEY_SYMPTOM_SEVERITY) as? String{
                                            self!.doctorSymptomsSeverity = symptom_severity
                                            print("symptom severity is \(symptom_severity)")
                                        }
      
                                    }
                                    if let provisional_name = data.object(forKey: Keys.KEY_PROVISIONAL_DIAGNOSIS) as? String
                                    {
                                        self!.doctorProvisional = provisional_name
                                        print("symptom name is \(provisional_name)")
                                    }
                                    
                                    if let followup_date = data.object(forKey: Keys.KEY_FOLOWUP_DATE) as? String
                                    {
                                        self?.doctorFollowupDate = followup_date
                                        print("symptom name is \(followup_date)")
                                    }
                                    
                                    if let symptom = data.object(forKey: Keys.KEY_SYMPTOM) as? NSArray{
                                        var arraySymptoms:[(symptoms:String,severity:String)] = []
                                        for (indexSym,obj) in symptom.enumerated(){
                                            var strSymptom = "--"
                                            var strSeverity = "--"
                                            let objArray : NSDictionary = obj as! NSDictionary
                                            if let objSeverity : NSDictionary = (objArray.value(forKey: Keys.KEY_SYMPTOM_SEVERITY) as? NSDictionary){
                                                if let severity_name = objSeverity.object(forKey: "name") as? String{
                                                    strSeverity = severity_name
                                                    if indexSym == 0{
                                                        self!.doctorSymptomsSeverity = strSeverity
                                                    }
                                                }
                                            }
                                            if let objSymptom : NSDictionary = (objArray.value(forKey: Keys.KEY_SYMPTOM) as? NSDictionary){
                                                if let symptom_name = objSymptom.object(forKey: "name") as? String{
                                                    strSymptom = symptom_name
                                                    if indexSym == 0{
                                                        self!.doctorSymptoms = strSymptom
                                                    }
                                                }
                                            }
                                            arraySymptoms.append((symptoms: strSymptom, severity: strSeverity))
                                            
                                        }
                                        DispatchQueue.main.async(execute: {
                                            self!.addSymptomsView(arraySymptoms: arraySymptoms)
                                        })
                                    }
                                    
                                    if let scheduled = data.object(forKey: Keys.KEY_SCHEDULED_AT) as? String {
                                        self!.scheduledat = scheduled
                                        print("Date String:=>\(self!.scheduledat)  date:\(self!.stringToDateConverter(date: self!.scheduledat))")
                                        let utcDate = self!.stringToDateConverter(date: self!.scheduledat)
                                        let current = Date()
                                        let calculatedSeconds = utcDate.timeIntervalSince(current)
                                        self!.minutes = Int(calculatedSeconds) / 60 % 60
                                        
                                        self!.isEventExist = self!.isEventAddedToCalender()
                                        print("isEventExist : ",self!.isEventExist)
                                    }
                                    
                                    if let attachments = data.object(forKey: Keys.KEY_ATTACHMENTS) as? [String] {
                                        self!.attachments = attachments
                                        print("Attachments = \(self!.attachments)")
                                    }
                                    
                                    if let attachmentsNew = data.object(forKey: Keys.KEY_ATTACHMENTS) as? [[String:Any]] {
                                        for (index,newAttach) in attachmentsNew.enumerated() {
                                             let id = newAttach[Keys.KEY_ID] as? Int
                                             let appointmentID = newAttach[Keys.KEY_APPOINTMENT_ID] as? Int
                                             let title = newAttach[Keys.KEY_TITLE] as? String
                                             let fileURL = newAttach[Keys.FILE_URL] as? String
                                        
                                                var isFileExtension = false
                                                for fileExtension in App.fileExtentionTypes {
                                                    if (fileURL ?? "").contains(fileExtension) {
                                                        isFileExtension = true
                                                        break
                                                    }else {
                                                         isFileExtension = false
                                                    }
                                                }
                                               print("isFileExtension:\(isFileExtension)")
                                            DispatchQueue.main.async{
                                                if isFileExtension {
                                                    var picture = Picture(fileURl: fileURL ?? "" ,picCaption: title ?? "", attachmentId: id ?? 0, appointmentId: appointmentID ?? 0, type: .file)
                                                    picture.isNewUpload = false
                                                    self!.tempNewAtachments.append(ImagesCollectionViewCellModal(picture: picture , index: index ) )
                                                }else {
                                                    var picture = Picture(imageUrl: fileURL ?? "" ,picCaption: title ?? "", attachmentId: id ?? 0, appointmentId: appointmentID ?? 0, type: .image)
                                                    picture.isNewUpload = false
                                                    self!.tempNewAtachments.append(ImagesCollectionViewCellModal(picture: picture , index: index ) )
                                                }
                                                
                                            }
                                        }
                                        print("temp count \(self!.tempNewAtachments.count)")
                                    }
                                    
                                    if let status = data.object(forKey: Keys.KEY_STATUS) as? Int {
                                        self!.consultation_status = status
                                        print("Status **= \(self!.consultation_status!)")
                                        if status == BookingConsultation.STATUS_CALL_FINISHED || status == BookingConsultation.STATUS_CALL_PLACED_NO_PRESCRIPTION {
                                            let ratingURl = AppURLS.URL_BookAppointment + "/\(self!.appointmetnID)/rating"
                                            
                                            self!.getRatingStatus(urlString: ratingURl, httpMethod: HTTPMethods.GET, jsonData: nil, type: 1) { (success, response) in
                                                if success {
                                                    if let respDic = response {
                                                        print("**Rating check response:\(respDic)")
                                                        if let rating = respDic.object(forKey: Keys.KEY_DATA) as? Int {
                                                            self!.appointment_rating = rating
                                                            DispatchQueue.main.async {
//                                                                if status == BookingConsultation.STATUS_CALL_FINISHED || status == BookingConsultation.STATUS_CALL_PLACED_NO_PRESCRIPTION {
//                                                                    self.updateRatingView(hide: false, height: 55)
//                                                                }
                                                                print("Doctor rating ==>\(rating)")
//                                                                self.vw_rating.rating = Double(rating)
//                                                                self.vw_rating.text = "\(rating)"
                                                                
                                                                if rating > 0 {
                                                                    self!.lb_rateDoctorTitle.text = "Rated"
//                                                                    self.vw_rating.updateOnTouch = false
//                                                                    self.addTapGestureToRating()
                                                                    self!.ratingBtns.forEach{
                                                                        $0.isUserInteractionEnabled = false
                                                                    }
                                                                    for i in 0..<self!.ratings.count{
                                                                        if i == Int(rating-1){
                                                                            self!.ratings[i].image = self!.filledEmoji[i]
                                                                        }else{
                                                                            self!.ratings[i].image = self!.emptyEmoji[i]
                                                                        }
                                                                    }
                                                                }else {
//                                                                    self.vw_rating.delegate = self
//                                                                    self.vw_rating.updateOnTouch = true
                                                                    self!.lb_rateDoctorTitle.text = "Rate Doctor"
                                                                    for i in 0..<self!.ratings.count{
                                                                        self!.ratings[i].image = self!.emptyEmoji[i]
                                                                    }
                                                                }
                                                                self!.updateRatingView(hide: false, height: 55)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }else{
                                            DispatchQueue.main.async {
                                                self!.vw_rateDoctor.isHidden = true
                                            }
                                        }
                                    }
                                    
                                    if let user = data.object(forKey: Keys.KEY_USER) as? NSDictionary{
                                        
                                        if let id = user.object(forKey: Keys.KEY_ID) as? Int {
                                            self!.user_id = "\(id)"
                                            print("User id::=>\(id)")
                                        }
                
                                    }
                                    
                                    if let patient = data.object(forKey: Keys.KEY_PATIENT) as? NSDictionary{
                                        var id = 0
                                        var email = ""
                                        var full_name = ""
                                        var firstname = ""
                                        var dob = ""
                                        var gender = ""
                                        var avatar_url = ""
                                        var age = ""
                                        
                                        if let P_id = patient.object(forKey: Keys.KEY_ID) as? Int {
                                            id = P_id
                                        }
                                        if let p_email = patient.object(forKey: Keys.KEY_EMAIL) as? String {
                                            email = p_email
                                        }
                                        if let p_fullname = patient.object(forKey: Keys.KEY_FULLNAME) as? String {
                                            full_name = p_fullname
                                        }
                                        
                                        if let p_firstname = patient.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                            firstname = p_firstname
                                        }
                                        
                                        if let p_dob = patient.object(forKey: Keys.KEY_DOB) as? String {
                                            dob = p_dob
                                            let pdobDate = self!.stringToDateConverterWithFormat(date: dob, format: "yyyy-MM-dd")
                                            let page = self!.calculate_age(date: pdobDate)
                                            age = "\(page)"
                                        }
                                        
                                        if let p_gender = patient.object(forKey: Keys.KEY_GENDER) as? String {
                                            if p_gender.lowercased().isEqual("male") {
                                                gender = "M"
                                            }else if p_gender.lowercased().isEqual("female") {
                                                gender = "F"
                                            }else {
                                                gender = "T"
                                            }
                                        }
                                        
                                        if let pavatar = patient.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                            avatar_url = pavatar
                                        }
                                        
                                        DispatchQueue.main.async {
                                            var patientDetails = ""
                                            if !age.isEmpty {
                                                patientDetails = "\(firstname) \(gender),\(age)"
                                            }else {
                                                patientDetails = "\(firstname) \(gender)"
                                            }
                                            self!.lb_patientDetails.text = patientDetails
                                            
                                            self!.iv_patientPic.layer.cornerRadius =  self!.iv_patientPic.frame.size.width / 2
                                            self!.iv_patientPic.clipsToBounds = true
                                            
                                            if let imgeURl = URL(string: avatar_url) {
                                                self!.iv_patientPic.kf.setImage(with: imgeURl, placeholder: UIImage(named:"user_placeholder_nocircle"), options: nil, progressBlock: nil, completionHandler: nil)
                                            }else {
                                                self!.iv_patientPic.image = UIImage(named:"user_placeholder_nocircle")
                                            }
                                        }
                                        
                                        let patientDetails = Patient(id: id, email: email, avatar: avatar_url, firstName: firstname, fullname: full_name, dob: dob, gender: gender, age: age)
                                        self!.patientModal = patientDetails
                                        print("Patient id==>:\(id) gender:\(gender) pavatar:\(avatar_url) p_dob:\(dob) age:\(age)")
                                       
                                        
                                    }
                                    
                                    if let doctor = data.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary{
                                        
                                        if let id = doctor.object(forKey: Keys.KEY_ID) as? Int {
                                            if let fristName = doctor.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                                if let middle = doctor.object(forKey: Keys.KEY_MIDDLE_NAME) as? String {
                                                    if let last = doctor.object(forKey: Keys.KEY_LAST_NAME) as? String {
                                                        if let practnum = doctor.object(forKey: Keys.KEY_PRACTITIONER_NUMBER) as? Int {
                                                            if let avatar = doctor.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                                if let prefix = doctor.object(forKey: Keys.KEY_PREFIX) as? String {
                                                                    if let specialisation = doctor.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                                        if let fullName = doctor.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                                            if let ratings = doctor.object(forKey: Keys.KEY_RATINGS) as? Double {
                                                                                self!.doctorModal = Doctor(prefix:prefix,id:id,first_name:fristName,middle_name:middle,last_name:last,practitioner_number:practnum,avatar_url:avatar,fullname:fullName,specialization:specialisation,rating:ratings)
                                                                                
                                                                                self!.doctorModal.appointment_id = self!.appointmetnID
                                                                                print("Doctor ratings:\(ratings)")
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        self!.updateUIOnFetch()
                                        self!.imagesCollectionView.reloadData()
                                        self!.hideOrViewAttachButton()
                                        if self!.tempNewAtachments.count != 0 {
                                            self!.bt_attachPhotos_outlet.setTitle("Attach", for: UIControl.State.normal)
                                        }
                                        self!.stopAnimating()
                                    })
                                    
                                }else {
                                    print("Error while fetching appointment details:\(responseStatus) ")
                                }
                                
                            }
                        }
                        catch let error
                        {
                            self!.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            print("Received not-well-formatted JSON appointment summary:\(error.localizedDescription)")
                        }
                    }
                }
            }.resume()
        }
    }
    

    /**
       method checks appointment time if it is less than 5 min cancel button get hides
     */
    func check_cancelation_time_exceeded() {
        let currentTime =  Date()
        let toTime = stringToDateConverter(date: self.scheduledat)
        let calculatedSeconds = toTime.timeIntervalSince(currentTime)
         print("To time:\(toTime)  current time:\(currentTime)")

        print("Cancelation seconds:\(calculatedSeconds)")
        if calculatedSeconds > 300 {
            self.isActiveAndAvailbleToRemoveFile = true
            //self.bt_cancel_outlet.isHidden = false
            self.bt_attachPhotos_outlet.isHidden = false
            cancel_width.constant = (self.view.frame.width-30)/2//145.0
            caledar_width.constant = (self.view.frame.width-30)/2//145.0
            if isEventExist
            {
                caledar_width.constant = 0.0
                cancel_width.constant = (self.view.frame.width-30)
            }
        }else {
            self.isActiveAndAvailbleToRemoveFile = false
            //self.bt_cancel_outlet.isHidden = true
            self.bt_attachPhotos_outlet.isHidden = true
            cancel_width.constant = 0.0
            caledar_width.constant = 0.0
        }
    }
    
    /**
     updates rating view height
     */
    func updateRatingView(hide:Bool,height:CGFloat) {
//        self.vw_rating.isHidden = hide
        UIView.animate(withDuration: 0.5) {
            self.LC_rating_height.constant = height
        }
    }
    
    /**
       updates ui after fetching appointment details
     */
    func updateUIOnFetch() {
        self.lb_bookingID.text = self.publicAppointmentID
        if self.calltype == 1 {
            if self.callChannel == BookingConsultation.REGULAR_CALL {
                self.iv_typeOfCall.image = UIImage(named: "Phone")?.withRenderingMode(.alwaysTemplate)
            }else {
                self.iv_typeOfCall.image = UIImage(named: "audio_icon")?.withRenderingMode(.alwaysTemplate)
            }
            self.lb_typeOfCall.text = "Audio"
            self.iv_typeOfCall.tintColor = Theme.buttonBackgroundColor
        }else {
            self.lb_typeOfCall.text = "Video"
            self.iv_typeOfCall.image = UIImage(named: "video_icon")
        }
        
        if self.addedNotes.isEmpty {
            self.tv_notes.text = "No Notes added"
        //    self.bt_editNotes.isHidden = true
        }else {
          //  self.bt_editNotes.isHidden = false
             self.tv_notes.text = self.addedNotes
        }
        
        if self.doctorSymptoms.isEmpty{
            self.lb_symptom.text = "No symptom found"
        }else{
            self.lb_symptom.text = self.doctorSymptoms
        }
        
        if self.doctorSymptomsSeverity.isEmpty{
            self.lb_symptomSeverity.text = "No symptom severity found"
        }else{
            self.lb_symptomSeverity.text = self.doctorSymptomsSeverity
        }
        
        if self.doctorNotes.isEmpty {
            self.lb_doctorRemarks.text = "No Remarks yet!"
        }else {
            if(self.doctorNotes.count > 200)
            {
                var textOnLabel = self.doctorNotes
                if(textOnLabel.count > 200){
                    
                    textOnLabel = String(textOnLabel[textOnLabel.startIndex..<textOnLabel.index(textOnLabel.startIndex, offsetBy: 200)])
                }
                self.lb_doctorRemarks.text = textOnLabel
                let readmoreFont = UIFont.boldSystemFont(ofSize: 12.0)//UIFont(name: "Helvetica-Oblique", size: 12.0)
                let readmoreFontColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)//UIColor.blue
                DispatchQueue.main.async {
                    self.lb_doctorRemarks.addTrailing(with: "... ", moreText: "Show more", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                }
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapReadMore(_:)))
                tapGesture.numberOfTapsRequired = 1
                self.lb_doctorRemarks.addGestureRecognizer(tapGesture)
                self.lb_doctorRemarks.isUserInteractionEnabled = true
            }
            else
            {
                self.lb_doctorRemarks.text = self.doctorNotes
            }
        }
        
        //***FollowUp date by doctor***///
        if self.doctorFollowupDate.isEmpty {
            self.lb_followUpDate.text = "No followup date"
        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let toDate = formatter.date(from: self.doctorFollowupDate)
            formatter.dateFormat = "dd-MMM-yyyy"
            self.lb_followUpDate.text = formatter.string(from: toDate!)
        }
        
        if self.doctorProvisional.isEmpty {
            self.lb_provisionalDiagnosis.text = "No Provisonal diagnosis!"
        }else {
            if(self.doctorProvisional.count > 50)
            {
                var textOnLabel = self.doctorProvisional
                if(textOnLabel.count > 50){
                    
                    textOnLabel = String(textOnLabel[textOnLabel.startIndex..<textOnLabel.index(textOnLabel.startIndex, offsetBy: 50)])
                }
                self.lb_provisionalDiagnosis.text = textOnLabel
                let readmoreFont = UIFont.boldSystemFont(ofSize: 12.0)//UIFont(name: "Helvetica-Oblique", size: 12.0)
                let readmoreFontColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)//UIColor.blue
                DispatchQueue.main.async {
                    self.lb_provisionalDiagnosis.addTrailing(with: "... ", moreText: "Show more", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 50)
                }
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapProvisonalReadMore(_:)))
                tapGesture.numberOfTapsRequired = 1
                self.lb_provisionalDiagnosis.addGestureRecognizer(tapGesture)
                self.lb_provisionalDiagnosis.isUserInteractionEnabled = true
            }
            else
            {
                self.lb_provisionalDiagnosis.text = self.doctorProvisional
            }
        }
        
        
        
        
//        if self.bookedfor == Int(self.user_id) {
//            self.lb_consultationfor.text = "Self"
//        }else {
//            self.lb_consultationfor.text = "Family"
//        }

        if self.consultation_status == BookingConsultation.STATUS_APPOINTMENT_ACTIVE {
            adjustViewNStatus(button:false,status_label:false,message:"Active", statusColor: hexStringToUIColor(hex: "#4CAF50"), hasPrescription: false)
            check_cancelation_time_exceeded()
        }else if self.consultation_status == BookingConsultation.STATUS_CALL_FINISHED {
            isActiveAndAvailbleToRemoveFile = false
            adjustViewNStatus(button:false,status_label:false,message:"Finished", statusColor: hexStringToUIColor(hex: "#03A9F4"), hasPrescription: true)
           // self.bt_cancel_outlet.setTitle("View Prescription", for: UIControlState.normal)
            caledar_width.constant = 0.0
            cancel_width.constant = (self.view.frame.width-30)
        }else if self.consultation_status == BookingConsultation.STATUS_APPOINTMENT_CANCELLED  {
             isActiveAndAvailbleToRemoveFile = false
           adjustViewNStatus(button:true,status_label:false,message:"Appointment Cancelled", statusColor: hexStringToUIColor(hex: "#ff3232"), hasPrescription: false)
        }else if self.consultation_status ==  BookingConsultation.STATUS_NOT_ATTENDED  {
             isActiveAndAvailbleToRemoveFile = false
            adjustViewNStatus(button:true,status_label:false,message:"Call Not Attended", statusColor: hexStringToUIColor(hex: "#ff3232"), hasPrescription: false)
        }else if self.consultation_status ==  BookingConsultation.STATUS_CALL_NOT_PLACED  {
             isActiveAndAvailbleToRemoveFile = false
            adjustViewNStatus(button:true,status_label:false,message:"Call Not Placed By Doctor", statusColor: hexStringToUIColor(hex: "#ff3232"), hasPrescription: false)
        }//Call placed no prescription status
        else if self.consultation_status ==  BookingConsultation.STATUS_CALL_PLACED_NO_PRESCRIPTION  {
             isActiveAndAvailbleToRemoveFile = false
            adjustViewNStatus(button:true,status_label:false,message:"Call Placed No Prescription", statusColor: hexStringToUIColor(hex: "#03A9F4"), hasPrescription: false)
        }
        
//        if self.patientModal != nil {
// 
//            if let patientName = self.patientModal.firstName ,let patienGender = self.patientModal.gender,let patientAvatar = self.patientModal.avatar_url{
//                var patientDetails = ""
//                
//                if let age = self.patientModal.age {
//                    patientDetails = "\(patientName) \(patienGender),\(age)"
//                }else {
//                    patientDetails = "\(patientName) \(patienGender)"
//                }
//  
//                self.lb_patientDetails.text = patientDetails
//                self.iv_patientPic.layer.cornerRadius =  self.iv_patientPic.frame.size.width / 2
//                 self.iv_patientPic.clipsToBounds = true
//                if let imgeURl = URL(string: patientAvatar) {
//                    self.iv_patientPic.kf.setImage(with: imgeURl)
//                }else {
//                    self.iv_patientPic.image = UIImage(named:"Default-avatar")
//                }
//                
//            }
//        }else {
//            print("Patient data is empty")
//        }
        
        print("date-->\(self.scheduledat)")
        let formatedDate = getFormattedDateAndTime(dateString: self.scheduledat)
        self.lb_date.text = formatedDate
        
        
        if let doctor = self.doctorModal {
            self.lb_dr_name.text = doctor.full_name
            self.lb_doctorSpecialisation.text = doctor.specialisation
            self.iv_doctorPic.kf.setImage(with: URL(string: doctor.avatar_url), placeholder: UIImage(named:"doctor_placeholder_nocircle") , options: nil, progressBlock: nil, completionHandler: nil)
            if let rating = doctor.ratings {
                self.rv_doctorRating.rating = Double(rating)
                self.lb_doctorRatingAverage.text = "\(rating)"
            }else {
                self.rv_doctorRating.rating = Double(0)
                self.lb_doctorRatingAverage.text = "0"
            }
        }

        self.adjustViewPhotos(tag: self.tempNewAtachments as [ImagesCollectionViewCellModal])
    }
    
    func adjustViewNStatus(button:Bool,
                           status_label:Bool,
                           message:String,
                           statusColor:UIColor,
                           hasPrescription:Bool) {
        //self.bt_cancel_outlet.isHidden = button
        if button == true
        {
            cancel_width.constant = 0.0
            caledar_width.constant = 0.0
        }
        else
        {
            cancel_width.constant = (self.view.frame.width-30)/2//145.0
            caledar_width.constant = (self.view.frame.width-30)/2//145.0
            if isEventExist
            {
                caledar_width.constant = 0.0
                cancel_width.constant = (self.view.frame.width-30)
            }
        }
        self.lb_cancelled_status.isHidden = status_label
        self.lb_cancelled_status.text = message
        self.lb_cancelled_status.textColor = Theme.buttonBackgroundColor
        
        
        if hasPrescription {
            self.bt_view_prescription.isHidden = false
            self.bt_cancel_outlet.isHidden = true
        }else {
            self.bt_view_prescription.isHidden = true
            self.bt_cancel_outlet.isHidden = false
        }
    }
    
    /**
      shows or hides attach photos button
     */
    func hideOrViewAttachButton() {
        print("Appo_summary_time:\(minutes)")
        if minutes != nil && minutes > 0 && self.consultation_status == 1{
            self.bt_attachPhotos_outlet.isHidden = false
        }else {
            self.bt_attachPhotos_outlet.isHidden = true
        }
        print("Notification scheduled time:\(minutes)")
    }
    

    /**
       method adjusts collection view height
     */
    func adjustViewPhotos(tag:[Any]) {
        DispatchQueue.main.async(execute: {
            if tag.count == 0 {
                 self.imagesCollectionView.reloadData()
                self.imagesCVHeightConstraint.constant = self.imagesCollectionView.contentSize.height
                self.imagesCollectionView.isHidden = true
                self.lb_no_attachments.isHidden = false
                print("Attachments empty")
            }else{
                print("Attachments there")
                self.imagesCollectionView.reloadData()
                self.lb_no_attachments.isHidden = true
                self.imagesCVHeightConstraint.constant = self.imagesCollectionView.contentSize.height
                self.imagesCollectionView.isHidden = false
            }
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
            self.stopAnimating()
            self.vw_blank.isHidden = true
        })
    }

    //MARK:- Add upcoming appointment to calendar
    @IBAction func calendarTapped(_ sender: UIButton) {
        let toTime = self.stringToDateConverter(date: self.scheduledat)
        var eventAlreadyExists = false
        CalendarManager.shared.requestAuthorization() { (allowed) in
            if allowed {
                CalendarManager.shared.getEvents(startDate: toTime, endDate: toTime.addingTimeInterval(15 * 60 * 1)) { (error, existingEvents) in
                    for singleEvent in existingEvents! {
                        if singleEvent.title == "DocOnline" && singleEvent.startDate == toTime && singleEvent.endDate == toTime.addingTimeInterval(15 * 60 * 1) {
                            self.didShowAlert(title: "Oops !", message: "Event has already been added to your Calendar")
                            eventAlreadyExists = true
                            break
                        }
                    }
                    if !eventAlreadyExists
                    {
                        CalendarManager.shared.createEvent(completion: { (event) in
                            guard let event = event else { return }
                            
                            event.startDate = toTime
                            event.endDate = event.startDate.addingTimeInterval(15 * 60 * 1)
                            
                            event.title = "DocOnline"
                            event.notes = "Appointment with doctor"
                            event.location = "Consultation"
                            
                            CalendarManager.shared.saveEvent(event: event) { (saveError) in
                                
                                if (saveError == nil)
                                {
                                    self.didShowAlert(title: "Event successfully added", message: "Your appointment schedule has been added to your calendar successfully")
                                    self.caledar_width.constant = 0.0
                                    self.cancel_width.constant = (self.view.frame.width-30)
                                }
                                
                            }
                            
                        })
                    }
                }
            }
            else
            {
                self.didShowAlert(title: "Calendar", message: "Grant access to calendar")
            }
        }
        
    }
    
    
    func isEventAddedToCalender() -> Bool {
        let toTime = self.stringToDateConverter(date: self.scheduledat)
        var eventAlreadyExists = false
        CalendarManager.shared.requestAuthorization() { (allowed) in
            if allowed {
                CalendarManager.shared.getEvents(startDate: toTime, endDate: toTime.addingTimeInterval(15 * 60 * 1)) { (error, existingEvents) in
                    for singleEvent in existingEvents! {
                        if singleEvent.title == "DocOnline" && singleEvent.startDate == toTime && singleEvent.endDate == toTime.addingTimeInterval(15 * 60 * 1) {
                            //self.didShowAlert(title: "Oops !", message: "Event has already been added to your Calendar")
                            eventAlreadyExists = true
                            break
                        }
                    }
                    
                }
            }
            else
            {
                //self.didShowAlert(title: "Access denied !", message: "Please provide access for your Calendar to save your scheduled Appointment")
            }
        }
        return eventAlreadyExists
    }
    
    //MARK:- remove appointment event from calendar on appointment cancellation
    func removeAppointmentFromCalendar()
    {
        let toTime = self.stringToDateConverter(date: self.scheduledat)
        var eventAlreadyExists = false
        var eventID = ""
        CalendarManager.shared.requestAuthorization() { (allowed) in
            if allowed {
                CalendarManager.shared.getEvents(startDate: toTime, endDate: toTime.addingTimeInterval(15 * 60 * 1)) { (error, existingEvents) in
                    for singleEvent in existingEvents! {
                        if singleEvent.title == "DocOnline" && singleEvent.startDate == toTime && singleEvent.endDate == toTime.addingTimeInterval(15 * 60 * 1) {
                            eventID = singleEvent.eventIdentifier
                            eventAlreadyExists = true
                            break
                        }
                    }
                    if eventAlreadyExists
                    {
                        CalendarManager.shared.removeEvent(eventId: eventID, completion: { (removeError) in
                            if(removeError == nil)
                            {
                                print("event ID removed")
                            }
                        })
                    }
                }
                
            }
            else
            {
                self.didShowAlert(title: "Calendar", message: "Grant access to calendar")
            }
        }
    }
    
//MARK:- Cancel appointment action if status = 1 canclation button or if status = 2 view prescription
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.check_cancelation_time_exceeded()
       
        if self.consultation_status == 1 && sender.tag == 1 {
            
            if !self.isActiveAndAvailbleToRemoveFile {
                self.didShowAlert(title: "", message: "Appointment can't be cancel before 5 minutes on your schedule")
                return
            }
            
            print("date:== \(stringToDateConverter(date:self.scheduledat))")
            
            let alert = UIAlertController(title: "Are you sure?", message: "Do you want to cancel appointment", preferredStyle: UIAlertController.Style.alert)
            
            let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.startAnimating()
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)
                let url = URL(string: "\(AppURLS.URL_BookAppointment)/\(self.appointmetnID)/cancel")
                print("URL==>\(AppURLS.URL_BookAppointment)/\(self.appointmetnID)")
                var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
                request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
                request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
                request.setValue(NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API) //New header for identification
                request.httpMethod = HTTPMethods.PATCH
                session.dataTask(with: request) { (data, response, error) in
                    if error != nil
                    {
                        print("Error while fetching data\(String(describing: error?.localizedDescription))")
                    }
                    else
                    {
                        if let response = response
                        {
                            print("url = \(response.url!)")
                            print("response = \(response)")
                            let httpResponse = response as! HTTPURLResponse
                            print("response code = \(httpResponse.statusCode)")

                            if httpResponse.statusCode == 204  {
                                print("Appointment canceled**")
                                self.isActiveAndAvailbleToRemoveFile = false
                                DispatchQueue.main.async {
                                    self.imagesCollectionView.reloadData()
                                }
                            
                                UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
                                    for notification:UNNotificationRequest in notificationRequests {
                                        if notification.identifier == "AppointmentAlert\(self.appointmetnID)" {
                                             UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers:  [notification.identifier])
                                            print("Local notification removed of id:\(self.appointmetnID)")
                                        }
                                    }
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    //self.bt_cancel_outlet.isHidden = true
                                    //self.cancel_width.constant = 0.0
                                    
                                    self.cancel_width.constant = 0.0
                                    self.caledar_width.constant = 0.0
                                    
                                    self.bt_attachPhotos_outlet.isHidden = true
                                    self.lb_cancelled_status.text = "Appointment Cancelled"
                                    self.lb_cancelled_status.textColor = Theme.buttonBackgroundColor
                                   // self.didShowAlert(title: "", message: "Appointment cancelled")
                                    self.showSuccessMessage(title: "Success", message: "Appointment Cancelled", duration: .seconds(seconds: 4))
                                  //  AlertView.sharedInsance.showSuccessAlert(title: "", message: "Appointment cancelled")
                                    self.removeAppointmentFromCalendar()
                                    self.stopAnimating()
                                    if App.tempAppointments.count != 0 && App.tempAppointments.count > 0{
                                        for (index,app) in App.tempAppointments.enumerated() {
                                            if let appid = app.id {
                                                if "\(appid)" == self.appointmetnID {
                                                    App.tempAppointments.remove(at: index)
                                                    print("Appoitnment id:\(appid) removed at index:\(index)")  //
                                                }
                                            }
                                        }
                                    }
                                    
                                })
                                
                               // RefreshTimer.sharedInstance.getAppointments()
                                
                            }else{
                                do
                                {
                                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                                    if !errorStatus {
                                        print("performing error handling appointment cancel :\(jsonData)")
                                    }
                                }
                                catch let error
                                {
                                    DispatchQueue.main.async(execute: {
                                        self.stopAnimating()
                                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                                    })
                                    print("Received not-well-formatted JSON app cancellation=>\(error.localizedDescription)")
                                }
                             }
                        }
                    }
                }.resume()
                
            }
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        else if self.consultation_status == 2  && sender.tag == 2 //veiw prescription
        {
            
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_PRESCRITPTION_VIEW, sender: sender)
        }
    }
    
    
   
    /**
     Converts UTC date string to local string and date
     - Parameter dateString: takes string date format
     - Returns: date string and Date from given string
     */
    func convertDate(dateString :String) ->(String,Date) {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormator.date(from: dateString)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let localDate = dateFormator.string(from: dt!)
        return (localDate,dateFormator.date(from: localDate)!)
    }
    
///////////
    /**
     Method calculates time and removes 5 min from the appointment time to show the remainder before appointment
     */
    func calculateTimeToShowRemainder() {
        print("\(#function) called")
        let currentTime =  Date()
        let toTime = stringToDateConverter(date: self.scheduledat)
        let seconds = toTime.timeIntervalSince(currentTime)
        let timeToshow = seconds - 300
        print("date should show loacl notification local:\(convertDate(dateString: self.scheduledat).0) utc:\(self.scheduledat)")
        let calendar = Calendar(identifier: .indian)
        let reducedDate = calendar.date(byAdding: .minute, value: -5, to: convertDate(dateString: self.scheduledat).1)
        print("Reduced date:\(reducedDate!)")
        let components = calendar.dateComponents(in: .current, from: reducedDate!)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        

        print("Time to show notification:\(timeToshow)")
        let selectedDate = self.scheduledat
        let selectedTime = selectedDate.components(separatedBy: " ")
        let time = "\(getFormattedDate(dateString: self.scheduledat)) at \(getTimeInAMPM(date:selectedTime[1]))"
        //UNUserNotificationCenter.current().delegate = self
        schedulteAppointmentRemainder(timeInterVal: newComponents, time: time)
    }
    
    /**
     Method setups actions for notification remainder
     */
    func setupActionsForNotification() {
        let dismissAction = UNNotificationAction(identifier: NotificationActionIdentifiers.DismissAction , title: "Dismiss", options: UNNotificationActionOptions.destructive)
        let viewAction = UNNotificationAction(identifier: NotificationActionIdentifiers.ViewAction , title: "View", options: UNNotificationActionOptions.foreground)
        let notificationCategory = UNNotificationCategory(identifier: NotificationCategoryIdentifiers.AlertCategory , actions: [dismissAction,viewAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([notificationCategory])
    }
    
    /**
     Method setups local notification remainder with time interval
     - Parameter timeInterVal: pass time or date components to schedule remainder
     - Parameter time: to show time in notification
     */
    func schedulteAppointmentRemainder(timeInterVal : DateComponents , time:String) {
        setupActionsForNotification()
        print("\(#function) called--")
        let content = UNMutableNotificationContent()
        content.title = "Appointment remainder"
        content.body = "You have an appointment with DocOnline at \(time). Make sure you are logged in and internet connection is working properly. Thank You"
        content.categoryIdentifier = "AlertCategory"
        content.userInfo = ["local_appointment_id":"\(self.appointmetnID)"]
        content.sound = UNNotificationSound.default
        
        // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterVal, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: timeInterVal, repeats: false)
        
        let requestIdentifier = "AppointmentAlert\(self.appointmetnID)"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
                print("Notification scheduling error:\(error?.localizedDescription)")
            }else{
                print("Appointment notification scheduled successfully")
            }
        }
        //App.selectedSlotTime = ""
    }
    
    ///used to test local notification **Not used**
    @IBAction func notifyTapped(_ sender: UIBarButtonItem) {
        var dateComponents = DateComponents()
        dateComponents.second = 3
        setupActionsForNotification()
        schedulteAppointmentRemainder(timeInterVal: dateComponents, time: "7:15 pm")
    }

////////
    
    
    /**
      Method shows action sheet with options
    */
    func imageSelecttionOtions() {
         let fileSize = App.FileSize
        
        let alert = UIAlertController(title: "Options", message: String(format: "Max %.0f MB / File", fileSize), preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (UIAlertAction) in
            
            if !self.checkCameraPermission()
            {
                //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
                self.didShowAlertForDeniedPermissions(message: "Please provide access for the camera to upload pictures")
                return
            }
            
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
            {
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self .present(self.picker, animated: true, completion: nil)
            }
        }
        
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { (UIAlertAction) in
            
            
            if !self.checkLibraryPermission()
            {
               // self.didShowAlert(title: "Access denied!", message: "Please provide access to upload pictures from your Gallery")
                self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
                return
            }
            
            self.isPictureTakenByCamera = false

            let imgPickerView = BSImagePickerViewController()
//            let mainCount = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.FILE_ATTACHMENT_LIMIT).numberValue?.intValue ?? 5
            let mainCount = 5
            if self.tempNewAtachments.count > 0 && self.tempNewAtachments.count < mainCount {
                let imageCount = mainCount - self.tempNewAtachments.count
                imgPickerView.maxNumberOfSelections = imageCount
            }else {
                imgPickerView.maxNumberOfSelections = mainCount
            }

            self.bs_presentImagePickerController(imgPickerView, animated: true, select: { (assest) in
                  print("Selected: \(assest)")
            }, deselect: { (asset) in
                print("Deselected: \(asset)")
            }, cancel: { (assets) in
                print("Cancel: \(assets)")
            }, finish: { (assets) in
                print("Finish: \(assets)")

               // self.startAnimating()
                for asset in assets {
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .opportunistic
                    options.isSynchronous = true
                    options.resizeMode = .fast

                    let imageManager = PHImageManager.default()

//                    imageManager.requestImageData(for: asset, options: options, resultHandler: { (imageData, value, orientation, reqOtpions) in
//
//                        if let data = imageData {
//                            if let image = UIImage(data: data) {
//                                App.ImagesArray.append(image)
//                                print("Image selection BSImagePicker:\(image)")
//                            }
//                        }
//
//                    })

                    imageManager.requestImage(for: asset, targetSize: CGSize(width:CGFloat(640), height:CGFloat(480)), contentMode: .aspectFill, options: options, resultHandler: { (resultThumbnail, resultDic) in
                        if let thumbNail = resultThumbnail {
                            //App.ImagesArray.append(thumbNail)
                            print("Image selection BSImagePicker:\(resultDic)")
                            var picData = Picture(pic: thumbNail, picCaption: "", type: .image)
                            picData.isNewUpload = true
                            App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                        }
                    })
                }

                DispatchQueue.main.async(execute: {
                    self.bt_attachPhotos_outlet.setTitle("Attach", for: UIControl.State.normal)
                    self.isNewUpload = true
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APP_IMAG_VIEW, sender: self)
                    print("Images count of selcted images:\(App.ImagesArray.count)")
                })

               // self.uploadSelectedImagesToServer(id: Int(self.appointmetnID)!, tag: 4)

            }, completion: nil)
           
        }
        let fileAction = UIAlertAction(title: "Pick a File", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.openDocumentPicker()
        }
       

        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(fileAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    //attaching photos action
    @IBAction func attachPhotosTapped(_ sender: UIButton) {
        self.check_cancelation_time_exceeded()
//        let fileCount = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.FILE_ATTACHMENT_LIMIT).stringValue ?? ""
        let fileCount = ""
        if self.attachments.count >= 5 {
            self.didShowAlert(title: "Sorry!", message: "Maximum \(fileCount) files allowed")
        }else if App.ImagesArray.count >= 5 {
            self.didShowAlert(title: "Sorry!", message: "Maximum \(fileCount) files allowed")
        }else if self.tempNewAtachments.count >= 5 {
            self.didShowAlert(title: "Sorry!", message: "Maximum \(fileCount) files allowed")
        }else if !self.isActiveAndAvailbleToRemoveFile {
            self.didShowAlert(title: "", message: "File upload not allowed before 5 minutes on your schedule")
            self.imagesCollectionView.reloadData()
        }else {
            App.bookingAttachedImages.removeAll()
            imageSelecttionOtions()
        }
    }
    
    @IBAction func editNotesTapped(_ sender: UIButton) {
    }
    
    /**
     Method shows alert
     */
    func showImagesConfrimationAlert() {
        let alert  = UIAlertController(title: "Info", message: "Would you like to attach another image?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
            {
//                self.picker.sourceType = UIImagePickerControllerSourceType.camera
//                self.present(self.picker, animated: true, completion: nil)
                self.imageSelecttionOtions()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Attach", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
             self.isPictureTakenByCamera = false
             //self.startAnimating()
             self.linearBar.startAnimation()
             self.uploadImagesToServer(id: Int(self.appointmetnID)!, tag: 4)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
      Method shows alert
     */
    func showAlertAfterSeletingImages() {
        let alert  = UIAlertController(title: "Confirmation", message: "Would you like to attach this selected images?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.linearBar.startAnimation()
            self.uploadImagesToServer(id: Int(self.appointmetnID)!, tag: 4)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            App.ImagesArray.removeAll()
            self.imagesCollectionView.reloadData()
            self.bt_attachPhotos_outlet.setTitle("Attach", for: UIControl.State.normal)
            self.adjustViewPhotos(tag: App.ImagesArray as [UIImage])
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tapReadMore(_ sender: UITapGestureRecognizer) {
        
        if self.lb_doctorRemarks.text?.range(of:"... Show more") != nil {
            print("Show more....")
            self.lb_doctorRemarks.text = self.doctorNotes
            
            let readmoreFont = UIFont.boldSystemFont(ofSize: 12.0)//UIFont(name: "Helvetica-Oblique", size: 12.0)
            let readmoreFontColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)//UIColor.green
            DispatchQueue.main.async {
                self.lb_doctorRemarks.addTrailing(with: "", moreText: " Show less", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: (self.lb_doctorRemarks.text?.count)!)
            }
        }
        else
        {
            print("Show less....")
            var textOnLabel = self.doctorNotes
            if(textOnLabel.count > 200){
                
                textOnLabel = String(textOnLabel[textOnLabel.startIndex..<textOnLabel.index(textOnLabel.startIndex, offsetBy: 200)])
            }
            self.lb_doctorRemarks.text = textOnLabel
            let readmoreFont = UIFont.boldSystemFont(ofSize: 12.0)//UIFont(name: "Helvetica-Oblique", size: 12.0)
            let readmoreFontColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)//UIColor.blue
            DispatchQueue.main.async {
                self.lb_doctorRemarks.addTrailing(with: "... ", moreText: "Show more", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 200)
                self.scrollToTop(animated: true)
            }
        }
        
    }
    
    @objc func tapProvisonalReadMore(_ sender: UITapGestureRecognizer) {
        
        if self.lb_provisionalDiagnosis.text?.range(of:"... Show more") != nil {
            print("Show more....")
            self.lb_provisionalDiagnosis.text = self.doctorProvisional
            
            let readmoreFont = UIFont.boldSystemFont(ofSize: 12.0)//UIFont(name: "Helvetica-Oblique", size: 12.0)
            let readmoreFontColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)//UIColor.green
            DispatchQueue.main.async {
                self.lb_provisionalDiagnosis.addTrailing(with: "", moreText: " Show less", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: (self.lb_provisionalDiagnosis.text?.count)!)
            }
        }
        else
        {
            print("Show less....")
            var textOnLabel = self.doctorProvisional
            if(textOnLabel.count > 50){
                
                textOnLabel = String(textOnLabel[textOnLabel.startIndex..<textOnLabel.index(textOnLabel.startIndex, offsetBy: 50)])
            }
            self.lb_provisionalDiagnosis.text = textOnLabel
            let readmoreFont = UIFont.boldSystemFont(ofSize: 12.0)//UIFont(name: "Helvetica-Oblique", size: 12.0)
            let readmoreFontColor = hexStringToUIColor(hex: StandardColorCodes.BLUE)//UIColor.blue
            DispatchQueue.main.async {
                self.lb_provisionalDiagnosis.addTrailing(with: "... ", moreText: "Show more", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, lengthForVisibleString: 50)
                //self.scrollToTop(animated: true)
            }
        }
        
    }
    
   
    //MARK: - Document picker controller
    func openDocumentPicker(){ //doc, docx, xls, xlsx, pdf
        let docs = String(kUTTypeCompositeContent)
        let presentation = String(kUTTypePresentation)
        let spreadSheet = String(kUTTypeSpreadsheet)//kUTTypeSpreadsheet)
        let pdf = String(kUTTypePDF)
        let microsftDoc = "com.microsoft.word.doc"
        let microsftDocx = "org.openxmlformats.wordprocessingml.document"
        let microsftxls = "com.microsoft.excel.xls"
        let gsheet = "com.google.gsheet"
        //"public.item","public.data" ,"public.content"
        let importMenu = UIDocumentPickerViewController(documentTypes: [pdf,presentation,spreadSheet,docs,microsftDoc,microsftDocx,microsftxls,gsheet], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func removeSelectedFileAt(index:Int) {
        self.check_cancelation_time_exceeded()
        if !self.isActiveAndAvailbleToRemoveFile {
            self.didShowAlert(title: "", message: "File can't be removed before 5 minutes on your schedule")
            self.imagesCollectionView.reloadData()
            return
        }
        
        if self.tempNewAtachments.indices.contains(index) {
            let alert = UIAlertController(title: "Delete Attachment", message: "Are you sure?", preferredStyle: .alert)
            let delete = UIAlertAction(title: "Delete", style: .destructive) { (action) in
                let attachment = self.tempNewAtachments[index]
                let urlString = "\(AppURLS.URL_BookAppointment)/\(self.appointmetnID)/attachments/\(attachment.picture?.attachmentId ?? 0)"
                print("Delete attachement url:\(attachment.picture?.attachmentId ?? 0)")
                self.startAnimating()
                NetworkCall.performDELETE(url: urlString,completionHandler: { (success, response, status, error) in
                    if success && status == 204 {
                        DispatchQueue.main.async {
                            self.tempNewAtachments.remove(at: index)
                            self.imagesCollectionView.reloadData()
                            self.adjustViewPhotos(tag: self.tempNewAtachments as [ImagesCollectionViewCellModal])
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                })
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(delete)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_APP_IMAG_VIEW {
            if let destVC = segue.destination as? ImageCrouselViewController {
                destVC.isFromSummaryPage = true
                destVC.appintmentId = self.appointmetnID
                destVC.delegate = self
                destVC.selectedImageIndex = self.selectedIndexOfImages
                destVC.isNewUpload = self.isNewUpload
            }
        }else if segue.identifier == StoryboardSegueIDS.ID_PRESCRITPTION_VIEW {
            if let destVC = segue.destination as? PrescriptionViewController {
                 destVC.appointment_id = self.appointmetnID
            }
        }
    }
    
    
    func addSymptomsView(arraySymptoms:[(symptoms:String,severity:String)]) {
        if arraySymptoms.count > 1 {
            vwMoreWidth.constant = 100.0
        }
        else{
            vwMoreWidth.constant = 0.0
        }
        let tableStyle = ".tableClass {width: 100%; border-collapse: collapse; font-family: Helvetica; font-size: 13px; table-layout: fixed;}" +
            ".headingClass td {border-bottom: 1px solid gray;}" +
            ".cellClass {font-family: monospace}" +
            ".tableClass tr td{text-align:center;border:1px solid #ddd;padding:10px;}" +
        ".tableClass tr th{text-align:center;border:1px solid #ddd;padding:10px;}"
        
        let htmlTable = generateTableWithArray(array: arraySymptoms, andTableStyle: tableStyle, forTableClassName: "tableClass", andRowClassNames: ["Symptoms", "Severity"], andCellClassNames: arraySymptoms)
        //print("htmlTable :",htmlTable);
        
        if self.transparentBackground == nil{
            self.transparentBackground = UIView(frame: UIScreen.main.bounds)
            self.transparentBackground.backgroundColor = UIColor(white: 0.0, alpha: 0.54)
            UIApplication.shared.keyWindow!.addSubview(self.transparentBackground)
            webVW = UIWebView(frame: CGRect(x: 20.0, y: 30.0, width: (UIScreen.main.bounds.width-40.0), height: (UIScreen.main.bounds.height-60.0)))
            webVW.delegate = self;
            self.transparentBackground.addSubview(webVW)
            
            let btnClose: UIButton = UIButton(frame: CGRect(x: webVW.frame.size.width-15, y: -15.0, width: 30.0, height: 30.0))
            //btnClose.setImage(UIImage(named: "close"), for: .normal)
            btnClose.setImage(getImage(image: UIImage(named: "close")!, backgroundColor: UIColor.white), for: .normal)
            btnClose.addTarget(self, action: #selector(AppointmentSummaryViewController.clickedBtnClose(_:)), for: .touchUpInside)
            webVW.addSubview(btnClose)
            
            UIApplication.shared.keyWindow!.bringSubviewToFront(self.transparentBackground)
            self.view.bringSubviewToFront(transparentBackground)
            
            self.transparentBackground.isHidden = true
            let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapSymptomView))
            doubleTapRecognizer.numberOfTapsRequired = 1
            doubleTapRecognizer.delegate = self
            transparentBackground.addGestureRecognizer(doubleTapRecognizer)
        }
        
        webVW.loadHTMLString(htmlTable, baseURL: nil)
        
        
    }
    
    func generateTableWithArray(array: [(symptoms:String,severity:String)], andTableStyle style: String?, forTableClassName tableClassName: String?, andRowClassNames rowClassNames: [String?]?, andCellClassNames cellClassNames: [(symptoms:String,severity:String)]?) -> String {
        var htmlString = ""
        
        htmlString += style          == nil ? "" : "<style>\(style!)</style>\n"
        htmlString += tableClassName == nil ? "<table>\n" : "<table class=\"\(tableClassName!)\">\n"
        htmlString += "<tr>"
        for (index,_) in rowClassNames!.enumerated() {
            if rowClassNames?.indices.contains(index) ?? false, let className = rowClassNames?[index] {
                htmlString += "<th>\(className)</th>"
            } else {
                //htmlString += "<tr>\n"
            }
        }
        htmlString += "</tr>\n"
        
        
        for (_,cell) in cellClassNames!.enumerated() {
            htmlString += "<tr>"
            htmlString += "<td>\(cell.symptoms)</td><td>\(cell.severity)</td>"
            htmlString += "</tr>\n"
        }
        
        htmlString += "</table>"
        
        return htmlString
    }
    
    @objc func onTapSymptomView(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.transparentBackground.isHidden = true
        }
    }
    
    @IBAction func clickBtnVwMore(_ sender: UIButton){
        self.transparentBackground.isHidden = false
    }
    
    @objc func clickedBtnClose(_ sender: UIButton){
        self.transparentBackground.isHidden = true
    }
    func getImage(image: UIImage, backgroundColor: UIColor)->UIImage?{
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        backgroundColor.setFill()
        //UIRectFill(CGRect(origin: .zero, size: image.size))
        let rect = CGRect(origin: .zero, size: image.size)
        let path = UIBezierPath(arcCenter: CGPoint(x:rect.midX, y:rect.midY), radius: rect.midX, startAngle: 0, endAngle: 6.28319, clockwise: true)
        path.fill()
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension AppointmentSummaryViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pckdImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.pickedImage = pckdImage
            var picData = Picture(pic: pckdImage , picCaption: "", type: .image)
            picData.isNewUpload = true
            App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1 ))
        }
        
        isPictureTakenByCamera = true
        //self.pickedImage != nil ?  App.ImagesArray.append(self.pickedImage ) : print("No image to apend")
        print("is picked form camera:\(isPictureTakenByCamera)")
        self.dismiss(animated: true) {
            self.isNewUpload = true
            self.selectedIndexOfImages = 0
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APP_IMAG_VIEW, sender: self)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker canceled")
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AppointmentSummaryViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tempNewAtachments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.tempNewAtachments[indexPath.row].cellInstance(collectionView, cellForItemAt: indexPath)
        cell.bt_delete.tag = indexPath.row
        cell.bt_delete.isHidden = !self.isActiveAndAvailbleToRemoveFile
        cell.cellDelegate = self
        
        if self.tempNewAtachments[indexPath.row].picture?.type?.rawValue ?? 0 == FileType.file.rawValue {
            cell.iv_image.contentMode = .scaleAspectFit
        }else {
            cell.iv_image.contentMode = .scaleAspectFill
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        App.bookingAttachedImages = self.tempNewAtachments
        self.isNewUpload = false
        self.selectedIndexOfImages = indexPath.row
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APP_IMAG_VIEW, sender: self)
    }
    
}

extension AppointmentSummaryViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 25
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/3, height: 100)
    }
}

extension AppointmentSummaryViewController: ImageCrouselDelegate {
    func didCompleteAttachingImages(images: [ImagesCollectionViewCellModal]) {
        self.navigationController?.popViewController(animated: true)
        self.tempNewAtachments = images
        self.imagesCollectionView.reloadData()
        self.adjustViewPhotos(tag: self.tempNewAtachments as [ImagesCollectionViewCellModal])
        App.bookingAttachedImages.removeAll()
    }
}

extension AppointmentSummaryViewController : ImagesCollectionViewDelegate {
    func didTapRemoveImge(at: Int) {
        self.removeSelectedFileAt(index: at)
    }
    
    func didTapVieFile(at: Int) {
    }
}

extension AppointmentSummaryViewController : UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result1 : \(myURL.absoluteString)")
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if  let myURL = urls.first {
            let pathComponent = myURL.lastPathComponent.components(separatedBy: ".")
            if self.sizePerMB(url:myURL) > App.FileSize {
                DispatchQueue.main.async {
                     let fileSize = App.FileSize
                    self.didShowAlert(title: "Sorry!", message: String(format: "Selected file size is more than %.0f MB", fileSize))
                }
            }else {
                DispatchQueue.main.async {
                    var picData = Picture(fileURL:myURL.absoluteString,picCaption: "" ,type: .file )
                    picData.isNewUpload = true
                    App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                    
                    self.isNewUpload = true
                    self.selectedIndexOfImages = 0
                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APP_IMAG_VIEW, sender: self)
                    print("import result 2: \(myURL.absoluteString) count:\(App.bookingAttachedImages.count)")
                }
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
    }
}

/////
//extension UIViewController {
//    /**
//     Method performs request to check appoitnment is rated or not
//     */
//    func getRatingStatus(urlString:String,httpMethod:String,jsonData:Data?,type:Int,completionHandler: @escaping (_ success:Bool,_ response:NSDictionary?) -> Void) {
//
//        let url = URL(string: urlString)
//        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
//        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
//        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
//        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
//        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
//        request.httpMethod = httpMethod
//
//        if type == 2 {
//            if let data = jsonData {
//                request.httpBody = data
//            }
//        }
//
//        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
//            if let error = error
//            {
//                 completionHandler(false,nil)
//                DispatchQueue.main.async(execute: {
//                    print("Error==> : \(error.localizedDescription)")
//                    self.didShowAlert(title: "", message: error.localizedDescription)
//                })
//            }
//            if let data = data
//            {
//                print("data =\(data)")
//            }
//            if let response = response
//            {
//                print("url = \(response.url!)")
//                print("response = \(response)")
//                let httpResponse = response as! HTTPURLResponse
//                print("response code = \(httpResponse.statusCode)")
//                print("httpresponse:\(response as! HTTPURLResponse)")
//
//                if httpResponse.statusCode == 204 && type == 2 {
//                    completionHandler(true,nil)
//                }else
//                {
//                    do {
//                        if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
//
//                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
//                            if !errorStatus {
//                                print("performing error handling update user profile:\(resultJSON)")
//                                completionHandler(false,resultJSON)
//                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String ,  type == 2{
//                                    DispatchQueue.main.async {
//                                        self.didShowAlert(title: "Info", message: message)
//                                    }
//                                }
//                            }else {
//                                completionHandler(true,resultJSON)
//                                print("Checking rating response:\(resultJSON)")
//                            }
//
//                            print("Rating response:\(resultJSON)")
//                        }
//
//                    }catch let error {
//                        completionHandler(false,nil)
//                        print("Recieved a well formatted json:\(error.localizedDescription)")
//                    }
//                }
//            }
//
//        }).resume()
//    }
//}

extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor, lengthForVisibleString: Int) {
        let readMoreText: String = trailingText + moreText
        
        //let lengthForVisibleString: Int = 200
        let mutableString: String = self.text!
        
        //let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        //let readMoreLength: Int = (readMoreText.count)
        //let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        //let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedStringKey.font: self.font])
        //let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedStringKey.font: moreTextFont, NSAttributedStringKey.foregroundColor: moreTextColor])
        
        let answerAttributed = NSMutableAttributedString(string: mutableString, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: readMoreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
}

extension UIViewController {
    
    func scrollToTop(animated: Bool) {
        if let tv = self as? UITableViewController {
            tv.tableView.setContentOffset(CGPoint.zero, animated: animated)
        } else if let cv = self as? UICollectionViewController{
            cv.collectionView?.setContentOffset(CGPoint.zero, animated: animated)
        } else {
            for v in view.subviews {
                if let sv = v as? UIScrollView {
                    sv.setContentOffset(CGPoint.zero, animated: animated)
                }
            }
        }
    }
}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}

extension AppointmentSummaryViewController : UIWebViewDelegate{
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("Webview fail with error \(error)");
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return true;
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("Webview started Loading")
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Webview did finish load")
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(CGSize.zero)
        webView.frame = CGRect(x: (UIScreen.main.bounds.width - webView.frame.size.width)/2, y: (UIScreen.main.bounds.height - webView.frame.size.height)/2, width: webView.frame.size.width, height: webView.frame.size.height)
        if webView.frame.size.height >= (UIScreen.main.bounds.height - 60){
            webView.frame = CGRect(x: 20.0, y: 30.0, width: (UIScreen.main.bounds.width-40.0), height: (UIScreen.main.bounds.height-60.0))
        }
    }
}

extension AppointmentSummaryViewController : UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if touch.view!.isDescendant(of: self.webVW) {
            return false
        }
        return true
    }
    
}
