//
//  HRAViewController.swift
//  DocOnline
//
//  Created by Mac on 25/06/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class HRAViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var iv_name_expand: UIImageView!
    @IBOutlet weak var tableFamilyView: UITableView!
    @IBOutlet weak var LC_tableFamilyViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var iv_gender_epand: UIImageView!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var iv_BP_expand: UIImageView!
    @IBOutlet weak var txtHeight: UITextField!
    @IBOutlet weak var txtWeight: UITextField!
    @IBOutlet weak var txtWaist: UITextField!
    @IBOutlet weak var txtBP: UITextField!
    @IBOutlet weak var txtSBP: UITextField!
    @IBOutlet weak var txtDBP: UITextField!
    @IBOutlet weak var txtSugar: UITextField!
    @IBOutlet weak var iv_Sugar_expand: UIImageView!
    @IBOutlet weak var txtHBP: UITextField!
    @IBOutlet weak var iv_HBP_expand: UIImageView!
    @IBOutlet weak var LC_BP_height: NSLayoutConstraint!
    @IBOutlet weak var txtHypertension: UITextField!
    @IBOutlet weak var iv_Hyper_expand: UIImageView!
    @IBOutlet weak var LC_Hyper_height: NSLayoutConstraint!
    @IBOutlet weak var txtCardiovascular: UITextField!
    @IBOutlet weak var iv_Card_expand: UIImageView!
    @IBOutlet weak var txtAtrial: UITextField!
    @IBOutlet weak var iv_Atrial_expand: UIImageView!
    @IBOutlet weak var txtVentricular: UITextField!
    @IBOutlet weak var iv_Ventricular_expand: UIImageView!
    @IBOutlet weak var txtParentalSugar: UITextField!
    @IBOutlet weak var iv_ParentalSugar_expand: UIImageView!
    @IBOutlet weak var txtParentalHyper: UITextField!
    @IBOutlet weak var iv_ParentalHyper_expand: UIImageView!
    @IBOutlet weak var txtParentalCardic: UITextField!
    @IBOutlet weak var iv_ParentalCardic_expand: UIImageView!
    @IBOutlet weak var txtsmoke: UITextField!
    @IBOutlet weak var iv_smoke_expand: UIImageView!
    @IBOutlet weak var txtcigar: UITextField!
    @IBOutlet weak var LC_cigar_height: NSLayoutConstraint!
    @IBOutlet weak var txtPhysical: UITextField!
    @IBOutlet weak var iv_Physical_expand: UIImageView!
    @IBOutlet weak var txtCalorie: UITextField!
    @IBOutlet weak var calculateBtn: UIButton!
    
    
    var dictMenStroke : NSMutableDictionary = NSMutableDictionary()
    var dictWomenStroke : NSMutableDictionary = NSMutableDictionary()
    var dictMenAge : NSMutableDictionary = NSMutableDictionary()
    var dictWomenAge : NSMutableDictionary = NSMutableDictionary()
    var dictMenUnTreated : NSMutableDictionary = NSMutableDictionary()
    var dictMenTreated : NSMutableDictionary = NSMutableDictionary()
    var dictWomenUnTreated : NSMutableDictionary = NSMutableDictionary()
    var dictWomenTreated : NSMutableDictionary = NSMutableDictionary()
    
    var arrayResults:[(key:String,value:String,risk:String,color:UIColor,rowHeight:CGFloat)] = []
    var arrayInputs:[(user_id:Int,height:Int,weight:Int,waist_circumference:Int,know_bp_readings:Int,sbp:Int,dbp:Int,diabetes:Int,blood_pressure:Int,hypertension:Int,cardiovascular:Int,atrial_fibrillation:Int,ventricular_hypertrophy:Int,parents_diabetic:Int,parents_hbp:Int,parents_cardiac_condition:Int,smoking_condition:Int,cigarettes_per_day:Int,physical_activity_state:Int,calories_intake:Int,bmi:Double,ibw:Double,bmr:Double,calories_required:Double,diabetes_risk:Int,hypertention_risk:String,cvd_risk:String,stroke_risk:String)] = []
    var arrayValidation:[(height_min:Int,height_max:Int,weight_min:Int,weight_max:Int,waist_min:Int,waist_max:Int,sbp_min:Int,sbp_max:Int,dbp_min:Int,dbp_max:Int,cigarettes_min:Int,cigarettes_max:Int,calories_min:Int,calories_max:Int)] = []
    
    var tempfamilyMemrs = [User]()
    var familyMem = [NSDictionary]()
    var familyMemrs = [User]()
    var selectedUserIndex = 0
    var userDataModel : User!
    ///Sessions
    static let config = URLSessionConfiguration.default
    var session : URLSession = URLSession.shared
    static var cookieJar = HTTPCookieStorage.shared
    
    var selectedName = 0
    var selectedPhysical = 0
    
    var selectedUserAge = 0
    var currentTextField : UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        LC_BP_height.constant = 0.0
        LC_Hyper_height.constant = 0.0
        LC_cigar_height.constant = 0.0
        LC_tableFamilyViewHeight.constant = 0.0
        
        dictMenStroke = [
            "1" : "3", "2" : "3", "3" : "4",
            "4" : "4", "5" : "5", "6" : "5",
            "7" : "6", "8" : "7", "9" : "8",
            "10" : "10", "11" : "11", "12" : "13",
            "13" : "15", "14" : "17", "15" : "20",
            "16" : "22", "17" : "26", "18" : "29",
            "19" : "33", "20" : "37", "21" : "42",
            "22" : "47", "23" : "52", "24" : "57",
            "25" : "63", "26" : "68", "27" : "74",
            "28" : "79", "29" : "84", "30" : "88"
        ]
        
        dictWomenStroke = [
            "1" : "1", "2" : "1", "3" : "2",
            "4" : "2", "5" : "2", "6" : "3",
            "7" : "4", "8" : "4", "9" : "5",
            "10" : "6", "11" : "8", "12" : "9",
            "13" : "11", "14" : "13", "15" : "16",
            "16" : "19", "17" : "23", "18" : "27",
            "19" : "32", "20" : "37", "21" : "43",
            "22" : "50", "23" : "57", "24" : "64",
            "25" : "71", "26" : "78", "27" : "84"
        ]
        
        dictMenAge = [
            "54" : "0", "55" : "0", "56" : "0",
            "57" : "1", "58" : "1", "59" : "1",
            "60" : "2", "61" : "2", "62" : "2",
            "63" : "3", "64" : "3", "65" : "3",
            "66" : "4", "67" : "4", "68" : "4",
            "69" : "5", "70" : "5", "71" : "5", "72" : "5",
            "73" : "6", "74" : "6", "75" : "6",
            "76" : "7", "77" : "7", "78" : "7",
            "79" : "8", "80" : "8", "81" : "8",
            "82" : "9", "83" : "9", "84" : "9",
            "85" : "10"
        ]
        
        dictWomenAge = [
            "54" : "0", "55" : "0", "56" : "0",
            "57" : "1", "58" : "1", "59" : "1",
            "60" : "2", "61" : "2", "62" : "2",
            "63" : "3", "64" : "3",
            "65" : "4","66" : "4", "67" : "4",
            "68" : "5","69" : "5", "70" : "5",
            "71" : "6", "72" : "6","73" : "6",
            "74" : "7", "75" : "7","76" : "7",
            "77" : "8", "78" : "8",
            "79" : "9", "80" : "9", "81" : "9",
            "82" : "10", "83" : "10", "84" : "10"
        ]
        
        dictMenUnTreated = [
            "97" : "0", "98" : "0", "99" : "0", "100" : "0", "101" : "0", "102" : "0", "103" : "0", "104" : "0", "105" : "0",
            "106" : "1", "107" : "1", "108" : "1", "109" : "1", "110" : "1", "111" : "1", "112" : "1", "113" : "1", "114" : "1", "115" : "1",
            "116" : "2", "117" : "2", "118" : "2", "119" : "2", "120" : "2", "121" : "2", "122" : "2", "123" : "2", "124" : "2", "125" : "2",
            "126" : "3", "127" : "3", "128" : "3", "129" : "3", "130" : "3", "131" : "3", "132" : "3", "133" : "3", "134" : "3", "135" : "3",
            "136" : "4", "137" : "4", "138" : "4", "139" : "4", "140" : "4", "141" : "4", "142" : "4", "143" : "4", "144" : "4", "145" : "4",
            "146" : "5", "147" : "5", "148" : "5", "149" : "5", "150" : "5", "151" : "5", "152" : "5", "153" : "5", "154" : "5", "155" : "5",
            "156" : "6", "157" : "6", "158" : "6", "159" : "6", "160" : "6", "161" : "6", "162" : "6", "163" : "6", "164" : "6", "165" : "6",
            "166" : "7", "167" : "7", "168" : "7", "169" : "7", "170" : "7", "171" : "7", "172" : "7", "173" : "7", "174" : "7", "175" : "7",
            "176" : "8", "177" : "8", "178" : "8", "179" : "8", "180" : "8", "181" : "8", "182" : "8", "183" : "8", "184" : "8", "185" : "8",
            "186" : "9", "187" : "9", "188" : "9", "189" : "9", "190" : "9", "191" : "9", "192" : "9", "193" : "9", "194" : "9", "195" : "9",
            "196" : "10", "197" : "10", "198" : "10", "199" : "10", "200" : "10", "201" : "10", "202" : "10", "203" : "10", "204" : "10",
            "205" : "10"
        ]
        
        dictMenTreated = [
            "97" : "0", "98" : "0", "99" : "0", "100" : "0", "101" : "0", "102" : "0", "103" : "0", "104" : "0", "105" : "0",
            "106" : "1", "107" : "1", "108" : "1", "109" : "1", "110" : "1", "111" : "1", "112" : "1",
            "113" : "2", "114" : "2", "115" : "2","116" : "2", "117" : "2",
            "118" : "3", "119" : "3", "120" : "3", "121" : "3", "122" : "3", "123" : "3",
            "124" : "4", "125" : "4", "126" : "4", "127" : "4", "128" : "4", "129" : "4",
            "130" : "5", "131" : "5", "132" : "5", "133" : "5", "134" : "5", "135" : "5",
            "136" : "6", "137" : "6", "138" : "6", "139" : "6", "140" : "6", "141" : "6", "142" : "6",
            "143" : "7", "144" : "7", "145" : "7", "146" : "7", "147" : "7", "148" : "7", "149" : "7", "150" : "7",
            "151" : "8", "152" : "8", "153" : "8", "154" : "8", "155" : "8", "156" : "8", "157" : "8", "158" : "8", "159" : "8", "160" :"8",
            "161" : "8",
            "162" : "9", "163" : "9", "164" : "9", "165" : "9","166" : "9", "167" : "9", "168" : "9", "169" : "9", "170" : "9", "171" : "9",
            "172" : "9", "173" : "9", "174" : "9", "175" : "9","176" : "9",
            "177" : "10", "178" : "10", "179" : "10", "180" : "10", "181" : "10", "182" : "10", "183" : "10", "184" : "10", "185" : "10",
            "186" : "10", "187" : "10", "188" : "10", "189" : "10", "190" : "10", "191" : "10", "192" : "10", "193" : "10", "194" : "10",
            "195" : "10", "196" : "10", "197" : "10", "198" : "10", "199" : "10", "200" : "10", "201" : "10", "202" : "10", "203" : "10",
            "204" : "10", "205" : "10"
        ]
        
        dictWomenUnTreated = [
            "95" : "1", "96" : "1", "97" : "1", "98" : "1", "99" : "1", "100" : "1", "101" : "1", "102" : "1", "103" : "1", "104" : "1",
            "105" : "1", "106" : "1",
            "107" : "2", "108" : "2", "109" : "2", "110" : "2", "111" : "2", "112" : "2", "113" : "2", "114" : "2", "115" : "2",
            "116" : "2", "117" : "2", "118" : "2",
            "119" : "3", "120" : "3", "121" : "3", "122" : "3", "123" : "3", "124" : "3", "125" : "3", "126" : "3", "127" : "3",
            "128" : "3", "129" : "3", "130" : "3",
            "131" : "4", "132" : "4", "133" : "4", "134" : "4", "135" : "4", "136" : "4", "137" : "4", "138" : "4", "139" : "4",
            "140" : "4", "141" : "4", "142" : "4", "143" : "4",
            "144" : "5", "145" : "5", "146" : "5", "147" : "5", "148" : "5", "149" : "5", "150" : "5", "151" : "5", "152" : "5",
            "153" : "5", "154" : "5", "155" : "5",
            "156" : "6", "157" : "6", "158" : "6", "159" : "6", "160" : "6", "161" : "6", "162" : "6", "163" : "6", "164" : "6",
            "165" : "6", "166" : "6", "167" : "6",
            "168" : "7", "169" : "7", "170" : "7", "171" : "7", "172" : "7", "173" : "7", "174" : "7", "175" : "7", "176" : "7",
            "177" : "7", "178" : "7", "179" : "7", "180" : "7",
            "181" : "8", "182" : "8", "183" : "8", "184" : "8", "185" : "8", "186" : "8", "187" : "8", "188" : "8", "189" : "8",
            "190" : "8", "191" : "8", "192" : "8",
            "193" : "9", "194" : "9", "195" : "9", "196" : "9", "197" : "9", "198" : "9", "199" : "9", "200" : "9", "201" : "9",
            "202" : "9", "203" : "9", "204" : "9",
            "205" : "10", "206" : "10", "207" : "10", "208" : "10", "209" : "10", "210" : "10", "211" : "10", "212" : "10", "213" : "10",
            "214" : "10", "215" : "10", "216" : "10"
        ]
        
        dictWomenTreated = [
            "95" : "1", "96" : "1", "97" : "1", "98" : "1", "99" : "1", "100" : "1", "101" : "1", "102" : "1", "103" : "1", "104" : "1",
            "105" : "1", "106" : "1",
            "107" : "2", "108" : "2", "109" : "2", "110" : "2", "111" : "2", "112" : "2", "113" : "2",
            "114" : "3", "115" : "3", "116" : "3", "117" : "3", "118" : "3", "119" : "3",
            "120" : "4", "121" : "4", "122" : "4", "123" : "4", "124" : "4", "125" : "3",
            "126" : "5", "127" : "5", "128" : "5", "129" : "5", "130" : "5", "131" : "5",
            "132" : "6", "133" : "6", "134" : "6", "135" : "6", "136" : "6", "137" : "6", "138" : "6", "139" : "6",
            "140" : "7", "141" : "7", "142" : "7", "143" : "7", "144" : "7", "145" : "7", "146" : "7", "147" : "7", "148" : "7",
            "149" : "8", "150" : "8", "151" : "8", "152" : "8", "153" : "8", "154" : "8", "155" : "8", "156" : "8", "157" : "8",
            "158" : "8", "159" : "8", "160" : "8",
            "161" : "9", "162" : "9", "163" : "9", "164" : "9", "165" : "9", "166" : "9", "167" : "9", "168" : "9", "169" : "9",
            "170" : "9", "171" : "9", "172" : "9", "173" : "9", "174" : "9", "175" : "9", "176" : "9", "177" : "9", "178" : "9",
            "179" : "9", "180" : "9", "181" : "9", "182" : "9", "183" : "9", "184" : "9", "185" : "9", "186" : "9", "187" : "9",
            "188" : "9", "189" : "9", "190" : "9", "191" : "9", "192" : "9", "193" : "9", "194" : "9", "195" : "9", "196" : "9",
            "197" : "9", "198" : "9", "199" : "9", "200" : "9", "201" : "9", "202" : "9", "203" : "9", "204" : "9",
            "205" : "10", "206" : "10", "207" : "10", "208" : "10", "209" : "10", "210" : "10", "211" : "10", "212" : "10", "213" : "10",
            "214" : "10", "215" : "10", "216" : "10"
        ]
        
        UpdateUIFields()
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        self.getFamilyMembers(completionHandler: {(success) in
            if success {
                dispatchGroup.leave()
            }
            else{
                dispatchGroup.leave()
            }
        })
        
        dispatchGroup.enter()
        self.getValidationList { (success) in
            if success {
                dispatchGroup.leave()
            }
            else{
                self.arrayValidation.append((height_min: 121,
                                             height_max: 275,
                                             weight_min: 30,
                                             weight_max: 500,
                                             waist_min: 10,
                                             waist_max: 999,
                                             sbp_min: 50,
                                             sbp_max: 300,
                                             dbp_min: 30,
                                             dbp_max: 300,
                                             cigarettes_min: 1,
                                             cigarettes_max: 200,
                                             calories_min: 1000,
                                             calories_max: 5000))
                dispatchGroup.leave()
            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        calculateBtn.backgroundColor = Theme.buttonBackgroundColor
    }
    
    func UpdateUIFields()  {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS) {
                self.userDataModel  = User.get_user_profile_default()
                print("status:\(UserDefaults.standard.bool(forKey: UserDefaltsKeys.KEY_USER_STATUS))  User First name :\(self.userDataModel.first_name!)")
                //self.txtName.text = self.userDataModel.first_name! + self.userDataModel.middle_name! + self.userDataModel.last_name!
                self.txtName.text = self.userDataModel.full_name! + "(myself)"
                self.txtGender.text = self.userDataModel.gender!
                self.txtAge.text = String(self.calcAge(birthday: self.userDataModel.dob!))
                self.showAgeAlert()
            }
        }
    }
    
    func showAgeAlert(){
        let doubleAge = self.txtAge.text!.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        if doubleAge < 20
        {
            let alert = UIAlertController(title: "Age", message: "For calculating Health Risk Assesment(HRA) the minimum age must be 20 years", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
            }
            //let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            //alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func resetInputData(){
        
        txtHeight.text = ""
        txtWeight.text = ""
        txtWaist.text = ""
        txtBP.text = ""
        txtSBP.text = ""
        txtDBP.text = ""
        txtSugar.text = ""
        txtHBP.text = ""
        txtCardiovascular.text = ""
        txtAtrial.text = ""
        txtVentricular.text = ""
        txtParentalSugar.text = ""
        txtParentalHyper.text = ""
        txtParentalCardic.text = ""
        txtsmoke.text = ""
        txtcigar.text = ""
        txtPhysical.text = ""
        txtCalorie.text = ""
    }
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        self.selectedUserAge = age!
        return age!
    }
    
    // MARK: - UIButton Actions
    @IBAction func clickedBtnName(_ sender: Any) {
        
        /*if (self.familyMemrs.count > 0)
        {
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_name_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            tableFamilyView.reloadData()
            LC_tableFamilyViewHeight.constant = self.tableFamilyView.contentSize.height
        }
        else
        {
        }*/
        
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_name_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Family Members", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_name_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        if self.familyMemrs.count != 0{
            for (index,obj) in self.familyMemrs.enumerated() {
                var name = obj.name
                if obj.name == self.userDataModel.full_name
                {
                    name = obj.name! + "(myself)"
                }
                let tempSelected = self.selectedName
                let selfAction = UIAlertAction(title: name, style: UIAlertAction.Style.default) { (UIAlertAction) in
                    self.txtName.text = name
                    self.txtGender.text = obj.gender
                    var age = obj.age
                    if let tempAge = obj.age{
                        let arrAge = tempAge.components(separatedBy: " ")
                        age = arrAge[0]
                    }
                    self.txtAge.text = (obj.name == self.userDataModel.full_name) ? String(self.calcAge(birthday: self.userDataModel.dob!)) : age
                    self.selectedName = index
                    self.showAgeAlert()
                    UIView.animate(withDuration: 0.5, animations: {
                        self.iv_name_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                    })
                    if tempSelected != self.selectedName {
                        self.resetInputData()
                    }
                }
                actionView.addAction(selfAction)
            }
        }
        self.present(actionView, animated: true, completion: nil)
        
    }
    
    @IBAction func clickedBtnGender(_ sender: Any) {
        /*
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_gender_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Gender?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let maleAction = UIAlertAction(title: "Male", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtGender.text = "Male"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_gender_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let femaleAction = UIAlertAction(title: "Female", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtGender.text = "Female"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_gender_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_gender_epand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(maleAction)
        actionView.addAction(femaleAction)
        self.present(actionView, animated: true, completion: nil)*/
    }
    
    @IBAction func clickedBtnBP(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_BP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Do you know your recent blood pressure readings?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtBP.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_BP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                self.LC_BP_height.constant = 105.0
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtBP.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_BP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                self.LC_BP_height.constant = 0.0
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_BP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                //self.LC_BP_height.constant = 0.0
            })
            _ = self.isValidated(txtfield: self.txtBP,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnSugar(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_Sugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Are you ever diagnosed with Diabetes (High Sugar Levels)?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtSugar.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Sugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtSugar.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Sugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Sugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtSugar,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnHBP(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_HBP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Are you ever diagnosed with High Blood Pressure?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtHBP.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_HBP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtHBP.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_HBP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_HBP_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtHBP,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnHyper(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_Hyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Are you on treatment for your hypertension?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtHypertension.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Hyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                self.LC_Hyper_height.constant = 156.0
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtHypertension.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Hyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                self.LC_Hyper_height.constant = 0.0
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Hyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                //self.LC_Hyper_height.constant = 0.0
            })
            _ = self.isValidated(txtfield: self.txtHypertension,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnCard(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_Card_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "History of cardiovascular disease?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtCardiovascular.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Card_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtCardiovascular.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Card_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Card_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtCardiovascular,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnAtrial(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_Atrial_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "History of Atrial fibrillation?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtAtrial.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Atrial_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtAtrial.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Atrial_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Atrial_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtAtrial,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnVentricular(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_Ventricular_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "History of Ventricular hypertrophy?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtVentricular.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Ventricular_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtVentricular.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Ventricular_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Ventricular_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtVentricular,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnParentalSugar(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_ParentalSugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Parental Diabetes?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "One parent", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalSugar.text = "One parent"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalSugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "Both parents", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalSugar.text = "Both parents"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalSugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noneAction = UIAlertAction(title: "None", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalSugar.text = "None"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalSugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalSugar_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtParentalSugar,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        actionView.addAction(noneAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnParentalHyper(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_ParentalHyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Parental Hypertension?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "One parent", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalHyper.text = "One parent"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalHyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "Both parents", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalHyper.text = "Both parents"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalHyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noneAction = UIAlertAction(title: "None", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalHyper.text = "None"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalHyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalHyper_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtParentalHyper,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        actionView.addAction(noneAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnParentalCardic(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_ParentalCardic_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Any Parental Cardiac Problem?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "One parent", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalCardic.text = "One parent"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalCardic_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "Both parents", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalCardic.text = "Both parents"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalCardic_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noneAction = UIAlertAction(title: "None", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtParentalCardic.text = "None"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalCardic_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_ParentalCardic_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtParentalCardic,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        actionView.addAction(noneAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnSmoke(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_smoke_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Do you currently smoke?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtsmoke.text = "Yes"
            //self.smokingStatus = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoke_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                self.LC_cigar_height.constant = 68.0
            })
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtsmoke.text = "No"
            //self.smokingStatus = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoke_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                self.LC_cigar_height.constant = 0.0
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_smoke_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtsmoke,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnPhysical(_ sender: Any) {
        /*if let currentTF = currentTextField {
            currentTF.resignFirstResponder()
        }*/
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_Physical_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Describe your Physical activity?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let yesAction = UIAlertAction(title: "Sedentary lifestyle", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtPhysical.text = "Sedentary lifestyle"
            self.selectedPhysical = 1
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Physical_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let noAction = UIAlertAction(title: "Mild exercise", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtPhysical.text = "Mild exercise"
            self.selectedPhysical = 2
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Physical_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let moderate = UIAlertAction(title: "Moderate exercise", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtPhysical.text = "Moderate exercise"
            self.selectedPhysical = 3
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Physical_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        let vigorous = UIAlertAction(title: "Vigorous exercise", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.txtPhysical.text = "Vigorous exercise"
            self.selectedPhysical = 4
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Physical_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_Physical_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
            _ = self.isValidated(txtfield: self.txtPhysical,isToSave: false)
        }
        
        actionView.addAction(cancelAction)
        actionView.addAction(yesAction)
        actionView.addAction(noAction)
        actionView.addAction(moderate)
        actionView.addAction(vigorous)
        self.present(actionView, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnCalculate(_ sender: Any) {
        
        self.showAgeAlert()
        
        if !isValidated(isToSave: true) {
            return
        }
        
        self.arrayResults.removeAll()
        self.arrayInputs.removeAll()
        
        //---------BMI-------------
        let doubleBMI = self.calculateBMI(weight: txtWeight.text ?? "0", height: txtHeight.text ?? "0")
        print("BMI::=> ",String(format:"%.2f", doubleBMI))
        //dictResults.setValue(String(format:"%.2f", doubleBMI), forKeyPath: "BMI")
        if doubleBMI < 18.5{
            self.arrayResults.append((key:"Body Mass Index",value: String(format:"%.2f", doubleBMI) + " Kg/m\u{00B2}", risk: "Under Weight", color: UIColor.init(hexString: "#1a648e"),rowHeight:188))
        }
        else if doubleBMI >= 18.5 && doubleBMI < 23.0{
            self.arrayResults.append((key:"Body Mass Index",value: String(format:"%.2f", doubleBMI) + " Kg/m\u{00B2}", risk: "Normal Weight", color: UIColor.init(hexString: "#6dbd45"),rowHeight:188))
        }
        else if doubleBMI >= 23.0 && doubleBMI < 25.0{
            self.arrayResults.append((key:"Body Mass Index",value: String(format:"%.2f", doubleBMI) + " Kg/m\u{00B2}", risk: "Over Weight", color: UIColor.init(hexString: "#f2ae80"),rowHeight:188))
        }
        else if doubleBMI >= 25.0 && doubleBMI < 30.0{
            self.arrayResults.append((key:"Body Mass Index",value: String(format:"%.2f", doubleBMI) + " Kg/m\u{00B2}", risk: "Obese I", color: UIColor.init(hexString: "#f07436"),rowHeight:188))
        }
        else if doubleBMI >= 30.0{
            self.arrayResults.append((key:"Body Mass Index",value: String(format:"%.2f", doubleBMI) + " Kg/m\u{00B2}", risk: "Obese II", color: UIColor.init(hexString: "#c9444f "),rowHeight:188))
        }
        
        //---------Ideal Body Weight-------------
        let doubleIdealBodyWeight = self.calculateIdealBodyWeight(height: txtHeight.text ?? "0")
        print("Ideal Body Weight::=> ",String(format:"%.2f", doubleIdealBodyWeight))
        let doubleWeight = self.txtWeight.text!.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        self.arrayResults.append((key:"Ideal Body Weight",value: String(format:"%.0f", doubleIdealBodyWeight.rounded()) + " Kgs", risk: String(format:"%.0f", doubleWeight.rounded()), color: RGB(r: 21, g: 145, b: 211),rowHeight:95))
        
        //---------Calories-------------
        let doubleBMR = self.calculateBMR(gender: txtGender.text!, weight: txtWeight.text!, height: txtHeight.text!, age: txtAge.text!)
        let doubleExe = self.calculateCal(exercise_level: txtPhysical.text!)
        print("BMR*Physical::=> ",String(format:"%.2f", doubleExe))
        let doubleCal = self.txtCalorie.text!.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        self.arrayResults.append((key:"Calories",value: String(format:"%.0f", doubleExe.rounded()) + " Cal", risk: String(format:"%.0f", doubleCal.rounded()), color: RGB(r: 21, g: 145, b: 211),rowHeight:95))
        
        //---------Diabetes-------------
        let diabetesScore = self.calculateDiabetesScore(gender: txtGender.text!, waist: self.txtWaist.text!, exercise_level: txtPhysical.text!, age: txtAge.text!, parentalDiabetes: self.txtParentalSugar.text!)
        print("diabetesScore::=> ",diabetesScore)
        if diabetesScore < 60{
            self.arrayResults.append((key:"Diabetes",value: String(diabetesScore), risk: "Low", color: RGB(r: 21, g: 145, b: 211),rowHeight:88))
        }
        else {
            self.arrayResults.append((key:"Diabetes",value: String(diabetesScore), risk: "High", color: RGB(r: 135, g: 206, b: 235),rowHeight:88))
        }
        
        //---------Hypertension-------------
        var finalHyperValue = ""
        let doubleAge = self.txtAge.text!.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        if self.txtBP.text == "Yes" && Int(doubleAge) >= 20{
            
            let doubleSBP = self.txtSBP.text!.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
            let doubleDBP = self.txtDBP.text!.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
            if Int(doubleSBP) >= 140 && Int(doubleDBP) >= 90{
                self.arrayResults.append((key:"Hypertension",value: "0.0", risk: "Hypertensive", color: RGB(r: 21, g: 145, b: 211),rowHeight:88))
            }
            else{
                let (double_4_Hyper,double_2_Hyper,double_1_Hyper) = self.calculateHypertension(age: self.txtAge.text!, gender: self.txtGender.text!, sbp: self.txtSBP.text!, dbp: self.txtDBP.text!, smoke: self.txtsmoke.text!, parentalHyper: self.txtParentalHyper.text!,isOptimal:"No")
                print("double_4_Hyper::=> ",String(format:"%.4f", double_4_Hyper))
                print("double_2_Hyper::=> ",String(format:"%.4f", double_2_Hyper))
                print("double_1_Hyper::=> ",String(format:"%.4f", double_1_Hyper))
                
                let (doubleOptimal_4_Hyper,doubleOptimal_2_Hyper,doubleOptimal_1_Hyper) = self.calculateHypertension(age: self.txtAge.text!, gender: self.txtGender.text!, sbp: "110.0", dbp: "70.0", smoke: "No", parentalHyper: "None",isOptimal:"Yes")
                print("doubleOptimal_4_Hyper::=> ",String(format:"%.4f", doubleOptimal_4_Hyper))
                print("doubleOptimal_2_Hyper::=> ",String(format:"%.4f", doubleOptimal_2_Hyper))
                print("doubleOptimal_1_Hyper::=> ",String(format:"%.4f", doubleOptimal_1_Hyper))
                
                let finalValue = String(format:"%.0f", (double_4_Hyper*100).rounded()) + "% " + String(format:"%.0f", (doubleOptimal_4_Hyper*100).rounded()) + "% " + String(format:"%.0f", (double_2_Hyper*100).rounded()) + "% " + String(format:"%.0f", (doubleOptimal_2_Hyper*100).rounded()) + "% " + String(format:"%.0f", (double_1_Hyper*100).rounded()) + "% " + String(format:"%.0f", (doubleOptimal_1_Hyper*100).rounded()) + "%"
                finalHyperValue = String(format:"%.0f", (double_4_Hyper*100).rounded()) + " " + String(format:"%.0f", (doubleOptimal_4_Hyper*100).rounded()) + " " + String(format:"%.0f", (double_2_Hyper*100).rounded()) + " " + String(format:"%.0f", (doubleOptimal_2_Hyper*100).rounded()) + " " + String(format:"%.0f", (double_1_Hyper*100).rounded()) + " " + String(format:"%.0f", (doubleOptimal_1_Hyper*100).rounded())
                
                self.arrayResults.append((key:"Hypertension",value: finalValue, risk: "High", color: RGB(r: 21, g: 145, b: 211),rowHeight:218))
            }
        }
        else if Int(doubleAge) < 20{
            self.arrayResults.append((key:"Hypertension",value: "0.0", risk: "Age", color: RGB(r: 21, g: 145, b: 211),rowHeight:218))
        }else{
            self.arrayResults.append((key:"Hypertension",value: "0.0", risk: "SBP", color: RGB(r: 21, g: 145, b: 211),rowHeight:218))
        }
        
        //---------CVD-------------
        var finalCVDValue = ""
        if self.txtBP.text == "Yes" && Int(doubleAge) >= 30 {
            let (doubleCVD,doubleHeart) = self.calculateCVD(age: self.txtAge.text!, gender: self.txtGender.text!, sbp: self.txtSBP.text!, smoke: self.txtsmoke.text!, hyperTRT: self.txtHypertension.text!, sugar: self.txtSugar.text!,isOptimal: "No")
            print("doubleCVD::=> ",String(format:"%.4f", doubleCVD))
            print("double Heart::=> ",String(format:"%.4f", doubleHeart))
            
            let (doubleOptimalCVD,_) = self.calculateCVD(age: self.txtAge.text!, gender: self.txtGender.text!, sbp: "110.0", smoke: "No", hyperTRT: "No", sugar: "No",isOptimal:"Yes")
            print("doubleOptimalCVD::=> ",String(format:"%.4f", doubleOptimalCVD))
            
            let (doubleNormalCVD,_) = self.calculateCVD(age: self.txtAge.text!, gender: self.txtGender.text!, sbp: "125.0", smoke: "No", hyperTRT: "No", sugar: "No",isOptimal:"Normal")
            print("doubleNormalCVD::=> ",String(format:"%.4f", doubleNormalCVD))
            
            var risk = "Low"
            if (doubleCVD*100) > 30{
                risk = "High"
            }
            
            finalCVDValue = String(format:"%.2f", doubleCVD*100) + " " + String(format:"%.2f", doubleNormalCVD*100) + " " + String(format:"%.2f", doubleOptimalCVD*100) + " " + String(format:"%.0f", doubleHeart)
            
            self.arrayResults.append((key:"Cardiovascular Disease",value: finalCVDValue, risk: risk, color: RGB(r: 21, g: 145, b: 211),rowHeight:228))
        }
        else if Int(doubleAge) < 30{
            self.arrayResults.append((key:"Cardiovascular Disease",value: "0.0", risk: "Age", color: RGB(r: 21, g: 145, b: 211),rowHeight:228))
        }
        else{
            self.arrayResults.append((key:"Cardiovascular Disease",value: "0.0", risk: "SBP", color: RGB(r: 21, g: 145, b: 211),rowHeight:228))
        }
        
        //---------STROKE-------------
        var finalStrokeValue = ""
        let minAge = 54.0
        let maxAge = (self.txtGender.text! == "Male" ? 85.0 : 84.0)
        if doubleAge >= minAge && doubleAge <= maxAge {
            let (doubleStroke,intPoints) = self.calculateStroke(age: self.txtAge.text!, gender: self.txtGender.text!, sbp: self.txtSBP.text!, smoke: self.txtsmoke.text!, hyperTRT: self.txtHypertension.text!, sugar: self.txtSugar.text!, hyperCVD: self.txtCardiovascular.text!, hyperAF: self.txtAtrial.text!, hyperVH: self.txtVentricular.text!)
            print("doubleStroke::=> ",String(format:"%.4f", doubleStroke))
            self.arrayResults.append((key:"Stroke",value: String(format:"%.0f", doubleStroke) + "%", risk: "Low", color: RGB(r: 135, g: 206, b: 235),rowHeight:88))
            finalStrokeValue = String(format:"%.0f", doubleStroke) + " " + String(intPoints)
        }
        else{
            self.arrayResults.append((key:"Stroke",value: "0.0", risk: "Age", color: RGB(r: 135, g: 206, b: 235),rowHeight:178))
        }
        
        var intUserId : Int?
        if self.selectedName == 0 {
            intUserId = (UserDefaults.standard.value(forKey: UserDefaltsKeys.CURRENT_USER_ID) as! Int)
            
        }
        else{
            intUserId = self.familyMemrs[self.selectedName].id!
            
        }
        
        self.arrayInputs.append((user_id: intUserId!, height: Int(self.txtHeight.text!)!, weight: Int(self.txtWeight.text!)!, waist_circumference: Int(self.txtWaist.text!)!, know_bp_readings: self.txtBP.text == "Yes" ? 1 : 0, sbp: self.txtBP.text == "Yes" ? Int(self.txtSBP.text!)! : 0, dbp: self.txtBP.text == "Yes" ? Int(self.txtDBP.text!)! : 0, diabetes: self.txtSugar.text == "Yes" ? 1 : 0, blood_pressure: self.txtSugar.text == "Yes" ? 1 : 0, hypertension: self.txtHBP.text == "Yes" ? 1 : 0, cardiovascular: self.txtCardiovascular.text == "Yes" ? 1 : 0, atrial_fibrillation: self.txtAtrial.text == "Yes" ? 1 : 0, ventricular_hypertrophy: self.txtVentricular.text == "Yes" ? 1 : 0, parents_diabetic: (self.txtParentalSugar.text == "One parent" ? 1 : (self.txtParentalSugar.text == "Both parents" ? 2 : 0)), parents_hbp: (self.txtParentalHyper.text == "One parent" ? 1 : (self.txtParentalHyper.text == "Both parents" ? 2 : 0)), parents_cardiac_condition: (self.txtParentalCardic.text == "One parent" ? 1 : (self.txtParentalCardic.text == "Both parents" ? 2 : 0)), smoking_condition: self.txtsmoke.text == "Yes" ? 1 : 0, cigarettes_per_day: self.txtsmoke.text == "Yes" ? Int(self.txtcigar.text!)! : 0, physical_activity_state: self.selectedPhysical, calories_intake: Int(self.txtCalorie.text!)!, bmi: doubleBMI.rounded(toPlaces: 2), ibw: doubleIdealBodyWeight.rounded(toPlaces: 2), bmr: doubleBMR.rounded(toPlaces: 2), calories_required: doubleExe.rounded(toPlaces: 2), diabetes_risk: diabetesScore, hypertention_risk: finalHyperValue, cvd_risk: finalCVDValue, stroke_risk: finalStrokeValue))
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_HRA_RESULTS_SEGUE, sender: self)
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_HRA_RESULTS_SEGUE {
            let destVC = segue.destination as! HRAResultsViewController
            destVC.arrayResults = self.arrayResults
            destVC.arrayInputs = self.arrayInputs
        }
    }
    
    func RGB(r:Double,g:Double,b:Double) -> UIColor {
        return UIColor(red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(g/255.0), alpha: 1.0)
    }
    
    
    // MARK: - Calculation
    
    func calculateBMI(weight:String,height:String) -> Double {
        // Do any additional setup after loading the view.
        let doubleWeight = weight.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        let doubleHeight = height.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        
        let doubleBMI = self.bmi(hight: doubleHeight/100, wighte: doubleWeight)
        return doubleBMI
    }
    
    func calculateIdealBodyWeight(height:String) -> Double{
        
        let doubleHeight = height.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        let doubleIdealBodyWeight = 0.9*(doubleHeight - 100)
        return doubleIdealBodyWeight
    }
    
    func calculateBMR(gender:String,weight:String,height:String,age:String)-> Double{
        let doubleWeight = weight.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        let doubleHeight = height.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        let doubleAge = age.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        if gender == "Male"{
            return 88.362 + (13.397 * doubleWeight) + (4.799 * doubleHeight) - (5.677 * doubleAge)
        }
        else if gender == "Female"{
            return 447.593 + (9.247 * doubleWeight) + (3.098 * doubleHeight) - (4.330 * doubleAge)
        }
        return 0.0
    }
    
    /**
     BMI Function
     this function is to calculate the body mass index
     by passing the hight and wighte
     @param hight Double
     @param wighte Double
     @return Bmi :Double .
     */
    func bmi(hight:Double,wighte:Double)-> Double{
        let x = wighte /  ( hight * hight )
        return x
    }
    
    func calculateCal(exercise_level:String)-> Double{
        let doubleBMR = self.calculateBMR(gender: txtGender.text!, weight: txtWeight.text!, height: txtHeight.text!, age: txtAge.text!)
        print("BMR::=> ",String(format:"%.2f", doubleBMR))
        if exercise_level == "Sedentary lifestyle" {
            return doubleBMR * 1.2
        }
        else if exercise_level == "Mild exercise" {
            return doubleBMR * 1.375
        }
        else if exercise_level == "Moderate exercise" {
            return doubleBMR * 1.55
        }
        else if exercise_level == "Vigorous exercise" {
            return doubleBMR * 1.725
        }
        return 0.0
    }
    
    func calculateDiabetesScore(gender:String,waist:String,exercise_level:String,age:String,parentalDiabetes:String)-> Int{
        var ageScore = 0
        var waistScore = 0
        var physicalScore = 0
        var parentalScore = 0
        
        
        let doubleAge = age.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        if doubleAge < 35{
            ageScore = 0
        }
        if doubleAge >= 35{
            ageScore = 20
        }
        if doubleAge >= 50{
            ageScore = 30
        }
        
        let doubleWaist = waist.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        if doubleWaist < 80{
            waistScore = 0
        }
        if doubleWaist >= 80{
            waistScore = 10
            if gender == "Male"{
                waistScore = 0
            }
        }
        if doubleWaist >= 90{
            waistScore = 20
            if gender == "Male"{
                waistScore = 10
            }
        }
        if doubleWaist >= 100{
            if gender == "Male"{
                waistScore = 20
            }
        }
        
        if exercise_level == "Sedentary lifestyle" {
            physicalScore = 30
        }
        else if exercise_level == "Mild exercise" {
            physicalScore = 20
        }
        else if exercise_level == "Moderate exercise" {
            physicalScore = 10
        }
        else if exercise_level == "Vigorous exercise" {
            physicalScore = 0
        }
        
        if parentalDiabetes == "None" {
            parentalScore = 0
        }
        else if parentalDiabetes == "One parent" {
            parentalScore = 10
        }
        else if parentalDiabetes == "Both parents" {
            parentalScore = 20
        }
        
        return ageScore+waistScore+physicalScore+parentalScore
    }
    
    func calculateHypertension(age:String,gender:String,sbp:String,dbp:String,smoke:String,parentalHyper:String,isOptimal:String) -> (Double,Double,Double){
        
        //1 – exp[ – exp((ln(4) – [22.94954 + ∑ Xß])/0.87692)
        let doubleAge = age.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        
        let interceptBeta = 22.949536 //1
        let ageBeta = -0.156412 * doubleAge //2
        let genderBeta = gender == "Male" ? 0.0 : (1.0 * -0.202933) //3
        let bmiBeta = (isOptimal == "No" ? self.calculateBMI(weight: txtWeight.text!, height: txtHeight.text!) : 22.5) * -0.033881//4
        let sbpBeta = self.txtBP.text == "Yes" ?  ((sbp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0) * -0.05933) : 0.0 //5
        let dbpBeta = self.txtBP.text == "Yes" ?  ((dbp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0) * -0.128468) : 0.0 //6
        let smokeBeta = (smoke == "Yes" ? (1 * -0.190731) : 0.0) //7
        let parentalBeta = (parentalHyper == "One parent" ? 1.0 : (parentalHyper == "Both parents" ? 2.0 : 0.0)) * -0.166121 //8
        let ageDbpBeta = (doubleAge * (dbp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0)) * 0.001624 //9
        
        let sumOfBeta = interceptBeta + ageBeta + genderBeta + bmiBeta + sbpBeta + dbpBeta + smokeBeta + parentalBeta + ageDbpBeta
        print("sumOfBeta::=> ",String(format:"%.2f", sumOfBeta))
        
        let double4Results = 1-exp(-exp((log(Double(4))-(sumOfBeta))/0.876925))
        let double2Results = 1-exp(-exp((log(Double(2))-(sumOfBeta))/0.876925))
        let double1Results = 1-exp(-exp((log(Double(1))-(sumOfBeta))/0.876925))
        return (double4Results,double2Results,double1Results)
    
    }
    
    func calculateCVD(age:String,gender:String,sbp:String,smoke:String,hyperTRT:String,sugar:String,isOptimal:String) -> (Double,Double){
        //1-0.94833exp(ΣßX – 26.0145) ---> Female
        //1-0.88431exp(ΣßX – 23.9388) ---> Male
        
        let doubleAge = age.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        
        let ageBeta = (gender == "Male" ? 3.11296 : 2.72107) * log(doubleAge) //1
        let sbpValue = self.txtBP.text == "Yes" ? (sbp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0) : 0.0
        let sbpBeta = log(sbpValue) * (hyperTRT == "Yes" ? (gender == "Male" ? 1.92672 : 2.88267) : (gender == "Male" ? 1.85508 : 2.81291)) //2
        let smokeBeta = (smoke == "Yes" ? 1 : 0) * (gender == "Male" ? 0.70953 : 0.61868) //3
        let bmiBeta = log((isOptimal == "No" ? self.calculateBMI(weight: txtWeight.text!, height: txtHeight.text!) : (isOptimal == "Normal" ? 22.5 : 22.0))) * (gender == "Male" ? 0.79277 : 0.51125) //4
        let sugarBeta = (sugar == "Yes" ? 1 : 0) * (gender == "Male" ? 0.5316 : 0.77763) //5
    
        let sumOfBeta = ageBeta + sbpBeta + smokeBeta + bmiBeta + sugarBeta - (gender == "Male" ? 23.9388 : 26.0145)
        print("sumOfBeta of CVD ::=> ",String(format:"%.2f", sumOfBeta + (gender == "Male" ? 23.9388 : 26.0145)))
        print("sumOfBeta of CVD - Coeff ::=> ",String(format:"%.2f", sumOfBeta))
        let doubleResults = 1-pow((gender == "Male" ? 0.88431 : 0.94833), exp(sumOfBeta))
        
        if isOptimal == "No"{
            let sbpHeartBeta = log(sbpValue) * (gender == "Male" ? 1.85508 : 2.81291)
            let sumOfHeartBeta = sbpHeartBeta + smokeBeta + bmiBeta + sugarBeta - (gender == "Male" ? 23.9388 : 26.0145)
            let consti_num =  exp(-(sumOfHeartBeta)/(gender == "Male" ? 3.11296 : 2.72107))
            let consti_denom = pow(-(log(gender == "Male" ? 0.88431 : 0.94833)), 1/(gender == "Male" ? 3.11296 : 2.72107))
            let consti = consti_num * (1/consti_denom)
            let expo = 1 * (1/(gender == "Male" ? 3.11296 : 2.72107))
            let term = pow(-(log(1-doubleResults)), expo)
            let heartAge = consti * term
            print("heartAge ::=> ",String(format:"%.2f", heartAge))
            return (doubleResults,heartAge)
        }
    
        return (doubleResults,0.0)
    }
    
    func calculateStroke(age:String,gender:String,sbp:String,smoke:String,hyperTRT:String,sugar:String,hyperCVD:String,hyperAF:String,
                         hyperVH:String) -> (Double,Int) {
        
        var ageValue = 0.0
        let doubleAge = age.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        let minAge = 54.0
        let maxAge = (self.txtGender.text! == "Male" ? 85.0 : 84.0)
        if doubleAge >= minAge && doubleAge <= maxAge {
            let ageTemp = (gender == "Male" ? dictMenAge.value(forKey: age) : dictWomenAge.value(forKey: age)) as! String
            ageValue = ageTemp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        }
        
        var trtValue = 0.0
        let sbpValue = self.txtBP.text == "Yes" ? (sbp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0) : 0.0
        
        if sbpValue >= 97.0 && sbpValue <= 205.0 && gender == "Male" {
            let trtTemp = (hyperTRT == "Yes" ? dictMenTreated.value(forKey: sbp) : dictMenUnTreated.value(forKey: sbp)) as! String
            trtValue = trtTemp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        }
        else if sbpValue >= 95.0 && sbpValue <= 216.0 && gender == "Female"{
            let trtTemp = (hyperTRT == "Yes" ? dictWomenTreated.value(forKey: sbp) : dictWomenUnTreated.value(forKey: sbp)) as! String
            trtValue = trtTemp.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0
        }
        else if sbpValue > 205.0 && gender == "Male"{
            trtValue = 10.0
        }
        else if sbpValue > 216.0 && gender == "Female"{
            trtValue = 10.0
        }
        
        var sugarValue = 0.0
        var smokeValue = 0.0
        var cvdValue = 0.0
        var afValue = 0.0
        var vhValue = 0.0
        
        sugarValue = (sugar == "Yes" ? (gender == "Male" ? 2.0 : 3.0) : 0)
        smokeValue = (smoke == "Yes" ? 3.0 : 0)
        
        cvdValue = (hyperCVD == "Yes" ? (gender == "Male" ? 4.0 : 2.0) : 0)
        afValue = (hyperAF == "Yes" ? (gender == "Male" ? 4.0 : 6.0) : 0)
        
        vhValue = (hyperVH == "Yes" ? (gender == "Male" ? 5.0 : 9.0) : 0)
        
        let sumTotal = ageValue + trtValue + sugarValue + smokeValue + cvdValue + afValue + vhValue
        print("sumTotal of stroke ::=> ",String(format:"%.2f", sumTotal))
        let intSum = Int(sumTotal)
        var strResults = ""
        if (intSum > 30 && gender == "Male") || (intSum > 27 && gender == "Female"){
            strResults = (gender == "Male" ? "88" : "84")
        }
        else if intSum != 0{
            strResults = (gender == "Male" ? dictMenStroke.value(forKey: String(intSum)) : dictWomenStroke.value(forKey: String(intSum))) as! String
        }
        else{
            strResults = "0"
        }
        
        return (strResults.convertStringToNumberalIfDifferentLanguage()?.doubleValue ?? 0.0,intSum)
    }
    
    // MARK: - Web Service
    /**
     Method performs request to server for family members
     */
    func getFamilyMembers(completionHandler : @escaping (_ success:Bool) -> Void) {
        self.tempfamilyMemrs.removeAll()
        let id = User.get_user_profile_default().id ?? 0
        let gender = User.get_user_profile_default().gender ?? ""
        let user = User(id: id , userName: App.user_full_name, type: "self", userAvatar: App.avatarUrl,age:"",gender:gender)
        self.tempfamilyMemrs.append(user)
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async(execute: {
                completionHandler(false)
                self.stopAnimating()
            })
            return
        }
        
        let slotURL = AppURLS.URL_Family_mem //+ "-members"
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.httpMethod = HTTPMethods.GET
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    completionHandler(false)
                    self.stopAnimating()
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling family mem:\(resultJSON)")
                        DispatchQueue.main.async(execute: {
                            completionHandler(false)
                            self.stopAnimating()
                        })
                    }
                    else {
                        print("family mem response:\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        
                        if let dict = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                            self.familyMem = dict
                            //App.familyMembers = dict
                            print("Fetched users")
                            
                            var count = 0
                            for mem in dict {
                                count += 1
                                /*
                                if let name = mem.object(forKey:Keys.KEY_FULLNAME) as? String,let id = mem.object(forKey: Keys.KEY_USER_ID) as? Int ,let age = mem.object(forKey: Keys.KEY_AGE) as? String{
                                    var gender = ""
                                    if let tempGender = mem.object(forKey: Keys.KEY_GENDER) as? String{
                                        gender = tempGender
                                    }
                                    let user = User(id: id, userName: name, type: "family", userAvatar: "",age: age, gender: gender)
                                    self.tempfamilyMemrs.append(user)
                                }*/
                                if let pivot = mem.object(forKey: Keys.KEY_PIVOT) as? NSDictionary {
                                    if let userid = pivot.object(forKey: Keys.KEY_USER_ID) as? Int,let name = mem.object(forKey:Keys.KEY_FULLNAME) as? String,let dateofbirth = mem.object(forKey: Keys.KEY_DOB) as? String,let memgener = mem.object(forKey: Keys.KEY_GENDER) as? String {
                                        
                                        let user = User(id: userid, userName: name, type: "family", userAvatar: "",age: String(self.calcAge(birthday: dateofbirth)), gender: memgener)
                                        self.tempfamilyMemrs.append(user)
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async(execute: {
                                
                                /*if count < 3 {
                                    let addNew = User(id: 0, userName: "Add New", type: "Other", userAvatar: "AddFamily",age:"")
                                    self.tempfamilyMemrs.append(addNew)
                                }*/
                                self.familyMemrs =  self.tempfamilyMemrs
                                print("Fetched users : ",self.familyMemrs.description)
                                completionHandler(true)
                            })
                        }
                    }
                    
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        completionHandler(false)
                        self.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-w@objc ell-formatted JSON : \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    func getValidationList(completionHandler : @escaping (_ success:Bool) -> Void) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            DispatchQueue.main.async {
                completionHandler(false)
                self.stopAnimating()
            }
            return
        }
        
        self.arrayValidation.removeAll()
        NetworkCall.performGet(url:AppURLS.URL_GET_HRA_VALIDATION) { (success, response, statusCode, error) in
            if let err = error {
                print("Error while getting records:\(err.localizedDescription)")
                DispatchQueue.main.async {
                    completionHandler(false)
                    self.stopAnimating()
                }
            }
            
            if let resp = response , let data = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                DispatchQueue.main.async {
                    print("Search Data:\(data)")
                    let dictSearch = data as NSDictionary
                    self.arrayValidation.append((height_min: dictSearch.value(forKey: "height_min") as! Int,
                                                 height_max: dictSearch.value(forKey: "height_max") as! Int,
                                                 weight_min: dictSearch.value(forKey: "weight_min") as! Int,
                                                 weight_max: dictSearch.value(forKey: "weight_max") as! Int,
                                                 waist_min: dictSearch.value(forKey: "waist_min") as! Int,
                                                 waist_max: dictSearch.value(forKey: "waist_max") as! Int,
                                                 sbp_min: dictSearch.value(forKey: "sbp_min") as! Int,
                                                 sbp_max: dictSearch.value(forKey: "sbp_max") as! Int,
                                                 dbp_min: dictSearch.value(forKey: "dbp_min") as! Int,
                                                 dbp_max: dictSearch.value(forKey: "dbp_max") as! Int,
                                                 cigarettes_min: dictSearch.value(forKey: "cigarettes_min") as! Int,
                                                 cigarettes_max: dictSearch.value(forKey: "cigarettes_max") as! Int,
                                                 calories_min: dictSearch.value(forKey: "calories_min") as! Int,
                                                 calories_max: dictSearch.value(forKey: "calories_max") as! Int))
                    print("arrayValidation:\(self.arrayValidation)")
                    
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(true)
                self.stopAnimating()
            }
        }
    }
    
    func isValidated(txtfield:UITextField? = UITextField(),isToSave:Bool) -> Bool {
        
        var validation = Validation()
        /*validation.minimumLength = 0
        validation.maximumLength = 40
        
        if (!validation.validateString(self.txtName.text!)){
            if self.txtName.text!.count < 3{
                self.didShowAlert(title: "", message: "Minimum 3 characters required in name")
                return false
            }
            self.didShowAlert(title: "", message: "Please fill your name")
            return false
        }
        
        validation.minimumLength = 2
        validation.maximumLength = 3
        if (!validation.validateString(self.txtAge.text!)){
            self.didShowAlert(title: "", message: "Please fill your Age")
            return false
        }
        
        validation.minimumLength = 1
        validation.maximumLength = 10
        if (!validation.validateString(self.txtGender.text!)){
            self.didShowAlert(title: "", message: "Please select gender")
            return false
        }*/
        
        validation.minimumLength = 2
        validation.maximumLength = 3
        validation.maximumValue = Double(self.arrayValidation[0].height_max)
        validation.minimumValue = Double(self.arrayValidation[0].height_min)
        if ((!validation.validateString(self.txtHeight.text!) && txtfield == self.txtHeight) || (!validation.validateString(self.txtHeight.text!) && isToSave)){
            self.didShowAlert(title: "Height", message: "Please enter a value between \(self.arrayValidation[0].height_min) and \(self.arrayValidation[0].height_max)")
            return false
        }
        validation.maximumValue = Double(self.arrayValidation[0].weight_max)
        validation.minimumValue = Double(self.arrayValidation[0].weight_min)
        if ((!validation.validateString(self.txtWeight.text!) && txtfield == self.txtWeight) || (!validation.validateString(self.txtWeight.text!) && isToSave)){
            self.didShowAlert(title: "Weight", message: "Please enter a value between \(self.arrayValidation[0].weight_min) and \(self.arrayValidation[0].weight_max)")
            return false
        }
        validation.maximumValue = Double(self.arrayValidation[0].waist_max)
        validation.minimumValue = Double(self.arrayValidation[0].waist_min)
        if ((!validation.validateString(self.txtWaist.text!) && txtfield == self.txtWaist) || (!validation.validateString(self.txtWaist.text!) && isToSave)){
            self.didShowAlert(title: "Waist Circumference", message: "Please enter a value between \(self.arrayValidation[0].waist_min) and \(self.arrayValidation[0].waist_max)")
            return false
        }
        
        validation.minimumLength = 2
        validation.maximumLength = 3
        if ((!validation.validateString(self.txtBP.text!) && txtfield == self.txtBP) || (!validation.validateString(self.txtBP.text!) && isToSave)){
            self.didShowAlert(title: "Blood pressure", message: "Please choose one option for recent Blood pressure readings")
            return false
        }
        
        if self.txtBP.text! == "Yes" {
            validation.minimumLength = 2
            validation.maximumLength = 3
            validation.maximumValue = Double(self.arrayValidation[0].sbp_max)
            validation.minimumValue = Double(self.arrayValidation[0].sbp_min)
            if ((!validation.validateString(self.txtSBP.text!) && txtfield == self.txtSBP) || (!validation.validateString(self.txtSBP.text!) && isToSave)){
                self.didShowAlert(title: "Systolic Blood Pressure", message: "Please enter a value between \(self.arrayValidation[0].sbp_min) and \(self.arrayValidation[0].sbp_max)")
                return false
            }
            validation.maximumValue = Double(self.arrayValidation[0].dbp_max)
            validation.minimumValue = Double(self.arrayValidation[0].dbp_min)
            if ((!validation.validateString(self.txtDBP.text!) && txtfield == self.txtDBP) || (!validation.validateString(self.txtDBP.text!) && isToSave)){
                self.didShowAlert(title: "Diastolic Blood Pressure", message: "Please enter a value between \(self.arrayValidation[0].dbp_min) and \(self.arrayValidation[0].dbp_max)")
                return false
            }
        }
        
        validation.minimumLength = 2
        validation.maximumLength = 3
        if ((!validation.validateString(self.txtSugar.text!) && txtfield == self.txtSugar) || (!validation.validateString(self.txtSugar.text!) && isToSave)){
            self.didShowAlert(title: "Diabetes", message: "Please choose one option for Diabetes")
            return false
        }
        
        if ((!validation.validateString(self.txtHBP.text!) && txtfield == self.txtHBP) || (!validation.validateString(self.txtHBP.text!) && isToSave)){
            self.didShowAlert(title: "High Blood Pressure", message: "Please choose one option for High Blood Pressure")
            return false
        }
        
        if ((!validation.validateString(self.txtHypertension.text!) && txtfield == self.txtHypertension) || (!validation.validateString(self.txtHypertension.text!) && isToSave)){
            self.didShowAlert(title: "Hypertension", message: "Please choose one option for treatment for your Hypertension")
            return false
        }
        
        if self.txtHypertension.text! == "Yes" {
            if ((!validation.validateString(self.txtCardiovascular.text!) && txtfield == self.txtCardiovascular) || (!validation.validateString(self.txtCardiovascular.text!) && isToSave)){
                self.didShowAlert(title: "Cardiovascular disease", message: "Please choose one option for Cardiovascular disease")
                return false
            }
            if ((!validation.validateString(self.txtAtrial.text!) && txtfield == self.txtAtrial) || (!validation.validateString(self.txtAtrial.text!) && isToSave)){
                self.didShowAlert(title: "Atrial fibrillation", message: "Please choose one option for Atrial fibrillation")
                return false
            }
            if ((!validation.validateString(self.txtVentricular.text!) && txtfield == self.txtVentricular) || (!validation.validateString(self.txtVentricular.text!) && isToSave)){
                self.didShowAlert(title: "Ventricular Hypertension", message: "Please choose one option for Ventricular Hypertension")
                return false
            }
        }
        
        validation.minimumLength = 4
        validation.maximumLength = 12
        if ((!validation.validateString(self.txtParentalSugar.text!) && txtfield == self.txtParentalSugar) || (!validation.validateString(self.txtParentalSugar.text!) && isToSave)){
            self.didShowAlert(title: "Parental Diabetes", message: "Please choose one option for Parental Diabetes")
            return false
        }
        
        if ((!validation.validateString(self.txtParentalHyper.text!) && txtfield == self.txtParentalHyper) || (!validation.validateString(self.txtParentalHyper.text!) && isToSave)){
            self.didShowAlert(title: "Parental Hypertension", message: "Please choose one option for Parental Hypertension")
            return false
        }
        
        if ((!validation.validateString(self.txtParentalCardic.text!) && txtfield == self.txtParentalCardic) || (!validation.validateString(self.txtParentalCardic.text!) && isToSave)){
            self.didShowAlert(title: "Parental Cardic", message: "Please choose one option for Parental Cardic")
            return false
        }
        
        validation.minimumLength = 2
        validation.maximumLength = 3
        if ((!validation.validateString(self.txtsmoke.text!) && txtfield == self.txtsmoke) || (!validation.validateString(self.txtsmoke.text!) && isToSave)){
            self.didShowAlert(title: "Smoking", message: "Please choose one option for Smoking")
            return false
        }
        
        if self.txtsmoke.text! == "Yes" {
            validation.minimumLength = 1
            validation.maximumLength = 3
            validation.maximumValue = Double(self.arrayValidation[0].cigarettes_max)
            validation.minimumValue = Double(self.arrayValidation[0].cigarettes_min)
            if ((!validation.validateString(self.txtcigar.text!) && txtfield == self.txtcigar) || (!validation.validateString(self.txtcigar.text!) && isToSave)){
                self.didShowAlert(title: "Cigarettes", message: "Please enter a value between \(self.arrayValidation[0].cigarettes_min) and \(self.arrayValidation[0].cigarettes_max)")
                return false
            }
        }
        
        validation.minimumLength = 13
        validation.maximumLength = 19
        if ((!validation.validateString(self.txtPhysical.text!) && txtfield == self.txtPhysical) || (!validation.validateString(self.txtPhysical.text!) && isToSave)){
            self.didShowAlert(title: "Physical Activity", message: "Please choose one option for Physical Activity")
            return false
        }
        
        validation.minimumLength = 4
        validation.maximumLength = 4
        validation.maximumValue = Double(self.arrayValidation[0].calories_max)
        validation.minimumValue = Double(self.arrayValidation[0].calories_min)
        if ((!validation.validateString(self.txtCalorie.text!) && txtfield == self.txtCalorie) || (!validation.validateString(self.txtCalorie.text!) && isToSave)){
            self.didShowAlert(title: "Calories", message: "Please enter a value between \(self.arrayValidation[0].calories_min) and \(self.arrayValidation[0].calories_max)")
            return false
        }
        
        return true
    }
}

extension HRAViewController : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.txtcigar) || (textField == self.txtHeight) || (textField == self.txtWeight) || (textField == self.txtWaist) || (textField == self.txtSBP) || (textField == self.txtDBP){
            let newLength = (textField.text?.count)! + string.count - range.length
            return newLength <= 3
        }
        else if textField == self.txtCalorie {
            let newLength = (self.txtCalorie.text?.count)! + string.count - range.length
            return newLength <= 4
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //if (self.presentedViewController == nil) {
            _ = self.isValidated(txtfield: textField,isToSave: false)
        //}
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
}

extension HRAViewController: UITableViewDelegate ,UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.DropDown, for: indexPath) as! DropDownTableViewCell
        
        let user = self.familyMemrs[indexPath.row]
        
        cell.lb_name.text = user.name!
        /*let arr = user.name?.components(separatedBy: " ")
         let strName = getUserName(arrString: arr!)//arr![1] + " " + arr![2]
         cell.lb_name.text = strName*/
        
        /*
         cell.iv_profilePic.layer.cornerRadius = cell.iv_profilePic.frame.size.width / 2
         cell.iv_profilePic.clipsToBounds = true*/
        
        if user.avatar!.isEmpty {
            cell.iv_profilePic.image = UIImage(named:"User_icon")
        }else if user.avatar!.lowercased().isEqual("addfamily"){
            cell.iv_profilePic.image = UIImage(named:"AddFamily")
        }else {
            //let url = URL(string:user.avatar!)
            //cell.iv_profilePic.kf.setImage(with: url)
            cell.iv_profilePic.image = UIImage(named:"User_icon")
        }
        
        cell.backgroundColor = UIColor.white
        
        switch self.selectedUserIndex {
        case indexPath.row:
            cell.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
            break
        case indexPath.row:
            cell.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
            break
        case indexPath.row:
            cell.backgroundColor = hexStringToUIColor(hex: StandardColorCodes.LITE_GREEN)
            break
        default:
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let user = self.familyMemrs[indexPath.row]
        self.selectedUserIndex = indexPath.row
        if user.name!.lowercased().isEqual("add new") {
            self.selectedUserIndex = 0
            
            if self.familyMem.count == 5
            {
                self.didShowAlert(title: "", message: "You can't add more than 3 family members")
            }else {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_FAMILY_MEM, sender: self)
            }
        }
        else
        {
            txtName.text = user.name
            /*
             let arr = user.name?.components(separatedBy: " ")
             let strName = getUserName(arrString: arr!)//arr![1] + " " + arr![2]
             txtOrderBy.text = strName*/
        }
        LC_tableFamilyViewHeight.constant = 0.0
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_name_expand.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
        })
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.familyMemrs.count
    }
}

public struct Validation {
    public var minimumLength = 0
    public var maximumLength: Int?
    public var maximumValue: Double?
    public var minimumValue: Double?
    public var characterSet: CharacterSet?
    public var format: String?
    
    public init() {}
    
    // Making complete false will cause minimumLength, minimumValue and format to be ignored
    // this is useful for partial validations, or validations where the final string is
    // in process of been completed. For example when entering characters into an UITextField
    public func validateString(_ string: String, complete: Bool = true) -> Bool {
        var valid = true
        
        if complete {
            valid = (string.count >= self.minimumLength)
        }
        
        if valid {
            if let maximumLength = self.maximumLength {
                valid = (string.count <= maximumLength)
            }
        }
        
        if valid {
            let formatter = NumberFormatter()
            let number = formatter.number(from: string)
            if let number = number {
                if let maximumValue = self.maximumValue {
                    valid = (number.doubleValue <= maximumValue)
                }
                
                if valid && complete {
                    if let minimumValue = self.minimumValue {
                        valid = (number.doubleValue >= minimumValue)
                    }
                }
            }
        }
        
        if valid {
            if let characterSet = self.characterSet {
                let stringCharacterSet = CharacterSet(charactersIn: string)
                valid = characterSet.superSetOf(other: stringCharacterSet)
            }
        }
        
        if valid && complete {
            if let format = self.format {
                let regex = try! NSRegularExpression(pattern: format, options: .caseInsensitive)
                let range = regex.rangeOfFirstMatch(in: string, options: .reportProgress, range: NSRange(location: 0, length: string.count))
                valid = (range.location == 0 && range.length == string.count)
            }
        }
        
        return valid
    }
}

extension CharacterSet {
    // Workaround for crash in Swift:
    // https://github.com/apple/swift/pull/4162
    func superSetOf(other: CharacterSet) -> Bool {
        return CFCharacterSetIsSupersetOfSet(self as CFCharacterSet, ((other as NSCharacterSet).copy() as! CFCharacterSet))
    }
}



