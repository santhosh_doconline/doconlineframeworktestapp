//
//  HRAResultsViewController.swift
//  DocOnline
//
//  Created by Mac on 15/07/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class HRAResultsViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    var arrayResults:[(key:String,value:String,risk:String,color:UIColor,rowHeight:CGFloat)]!
    var arrayInputs:[(user_id:Int,height:Int,weight:Int,waist_circumference:Int,know_bp_readings:Int,sbp:Int,dbp:Int,diabetes:Int,blood_pressure:Int,hypertension:Int,cardiovascular:Int,atrial_fibrillation:Int,ventricular_hypertrophy:Int,parents_diabetic:Int,parents_hbp:Int,parents_cardiac_condition:Int,smoking_condition:Int,cigarettes_per_day:Int,physical_activity_state:Int,calories_intake:Int,bmi:Double,ibw:Double,bmr:Double,calories_required:Double,diabetes_risk:Int,hypertention_risk:String,cvd_risk:String,stroke_risk:String)]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "referred")
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        saveBtn.backgroundColor = Theme.buttonBackgroundColor
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func clickedBtnSave(_ sender: Any) {
        self.saveCalculation()
    }
    
    // MARK: - WebService
    
    @objc func saveCalculation() {
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                return
            }
            
            
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            let (jsonStrHyper,jsonStrCVD,jsonStrStroke) = self.getJson()
            
            dataToPost = "{\"\(Keys.KEY_USER_ID)\":\(arrayInputs[0].user_id), \"\(Keys.KEY_HEIGHT)\":\(arrayInputs[0].height), \"\(Keys.KEY_WEIGHT)\":\(arrayInputs[0].weight), \"\(Keys.KEY_KNOW_BP_READINGS)\":\(arrayInputs[0].know_bp_readings), \"\(Keys.KEY_SBP)\":\(arrayInputs[0].sbp), \"\(Keys.KEY_DBP)\":\(arrayInputs[0].dbp), \"\(Keys.KEY_WAIST_CIRCUMFERENCE)\":\(arrayInputs[0].waist_circumference) ,\"\(Keys.KEY_DIABETES)\":\(arrayInputs[0].diabetes),\"\(Keys.KEY_BP)\":\(arrayInputs[0].blood_pressure),\"\(Keys.KEY_HYPER)\":\(arrayInputs[0].hypertension),\"\(Keys.KEY_CARDIO)\":\(arrayInputs[0].cardiovascular),\"\(Keys.KEY_ATRIAL)\":\(arrayInputs[0].atrial_fibrillation),\"\(Keys.KEY_VH)\":\(arrayInputs[0].ventricular_hypertrophy),\"\(Keys.KEY_PARENTAL_DIABETES)\":\(arrayInputs[0].parents_diabetic),\"\(Keys.KEY_PARENTAL_HBP)\":\(arrayInputs[0].parents_hbp),\"\(Keys.KEY_PARENTAL_CARDIO)\":\(arrayInputs[0].parents_cardiac_condition),\"\(Keys.KEY_SMOKE_CONDITION)\":\(arrayInputs[0].smoking_condition),\"\(Keys.KEY_CIGARETES)\":\(arrayInputs[0].cigarettes_per_day),\"\(Keys.KEY_PHYSICAL_ACTIVITY)\":\(arrayInputs[0].physical_activity_state),\"\(Keys.KEY_CALORIES_INTAKE)\":\(arrayInputs[0].calories_intake),\"\(Keys.KEY_BMI)\":\(arrayInputs[0].bmi),\"\(Keys.KEY_IBW)\":\(arrayInputs[0].ibw),\"\(Keys.KEY_BMR)\":\(arrayInputs[0].bmr),\"\(Keys.KEY_CALORIES_REQUIRED)\":\(arrayInputs[0].calories_required),\"\(Keys.KEY_DIABETES_RISK)\":\(arrayInputs[0].diabetes_risk),\"\(Keys.KEY_HYPER_RISK)\":\(jsonStrHyper),\"\(Keys.KEY_CVD_RISK)\":\(jsonStrCVD),\"\(Keys.KEY_STROKE_RISK)\":\(jsonStrStroke)}"
            let url = URL(string: AppURLS.URL_SAVE_HRA)
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            
            let postData = dataToPost
            print("Data==>\(postData)")
            
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        self.stopAnimating()
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("response:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            
                            if httpResponse.statusCode == 422
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    DispatchQueue.main.async(execute: {
                                        self.didShowAlert(title: "", message: message)
                                        self.stopAnimating()
                                    })
                                }
                            }
                        }
                        else {
                            print("response:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSArray
                            print("data***=>\(data)")
                            
                            if code == 200 && status == "success"
                            {
                                print("Success")
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    self.stopAnimating()
                                    let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as! String
                                    //self.didShowAlert(title: "", message: message)
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
                                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
                                        _ = self.navigationController?.popViewController(animated: true)
                                    }
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                })
                            }
                            else
                            {
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    func setAttributeString(mainString:String,subString:String,color:UIColor) -> NSMutableAttributedString {
        let range = (mainString as NSString).range(of: subString)
        let attribute = NSMutableAttributedString.init(string: mainString)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        return attribute
    }

    func appendAttributeStrings(firstString:NSMutableAttributedString,secondString:NSMutableAttributedString) -> NSMutableAttributedString {
        let combination = NSMutableAttributedString()
        combination.append(firstString)
        combination.append(secondString)
        return combination
    }
    
    func getJson() -> (String, String, String) {
        let arrHyperSep = arrayInputs![0].hypertention_risk.components(separatedBy: " ")
        let arrCVDSep = arrayInputs![0].cvd_risk.components(separatedBy: " ")
        let arrStrokeSep = arrayInputs![0].stroke_risk.components(separatedBy: " ")
        var dictHyper : [[String:[String:Int]]]?
        var jsonStrHyper = "[{}]"
        var jsonStrCVD = "{}"
        var jsonStrStroke = "{}"
        let dictCVD : NSMutableDictionary = NSMutableDictionary()
        let dictStroke : NSMutableDictionary = NSMutableDictionary()
        
        if arrayResults![4].risk == "Age" || arrayResults![4].risk == "SBP" || arrayResults![4].risk == "Hypertensive"{
           print("empty Json Hyper")
        }
        else{
            if arrHyperSep.count > 0 {
                dictHyper = ([
                    [
                        "1-year" : [
                            "score" : Int(arrHyperSep[4]),
                            "optimal":Int(arrHyperSep[5]),
                        ],
                        "2-years" : [
                            "score" : Int(arrHyperSep[2]),
                            "optimal":Int(arrHyperSep[3]),
                        ],
                        "4-years" : [
                            "score" : Int(arrHyperSep[0]),
                            "optimal":Int(arrHyperSep[1]),
                        ]
                    ]
                    ] as! [[String : [String : Int]]])
                
                do {
                    let jsonHyper = try JSONSerialization.data(withJSONObject: dictHyper as Any, options: JSONSerialization.WritingOptions()) as NSData
                    jsonStrHyper = NSString(data: jsonHyper as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    print("json Hyper string = \(jsonStrHyper)")
                }catch _ {
                    print ("JSON Hyper Failure")
                }
            }
        }
        if arrayResults![5].risk == "Age" || arrayResults![5].risk == "SBP"{
            print("empty Json CVD")
        }
        else{
            if arrCVDSep.count > 0 {
                dictCVD.setValue(arrCVDSep[0], forKey: "score")
                dictCVD.setValue(arrCVDSep[1], forKey: "normal")
                dictCVD.setValue(arrCVDSep[2], forKey: "optimal")
                dictCVD.setValue(arrCVDSep[3], forKey: "heart_age")
                
                do {
                    let jsonCVD = try JSONSerialization.data(withJSONObject: dictCVD, options: JSONSerialization.WritingOptions()) as NSData
                    jsonStrCVD = NSString(data: jsonCVD as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    print("json CVD string = \(jsonStrCVD)")
                }catch _ {
                    print ("JSON CVD Failure")
                }
            }
        }
        
        if arrayResults![6].risk == "Age"{
            print("empty Json Stroke")
        }
        else{
            if arrStrokeSep.count > 0 {
                dictStroke.setValue(arrStrokeSep[0], forKey: "probability")
                dictStroke.setValue(arrStrokeSep[1], forKey: "points")
                
                do {
                    let jsonStroke = try JSONSerialization.data(withJSONObject: dictStroke, options: JSONSerialization.WritingOptions()) as NSData
                    jsonStrStroke = NSString(data: jsonStroke as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    print("json Stroke string = \(jsonStrStroke)")
                }catch _ {
                    print ("JSON Stroke Failure")
                }
            }
        }
        return(jsonStrHyper,jsonStrCVD,jsonStrStroke)
    }
    
}

extension HRAResultsViewController : UITableViewDelegate ,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6//arrayResults.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BMIViewCell", for: indexPath) as! BMIViewCell
            cell.setVwBG(borderColor: UIColor.white)
            cell.setLblBG()
            cell.lblBMI.text = "BMI - " + arrayResults![indexPath.row].value
            cell.lblBMI.attributedText = self.setAttributeString(mainString: cell.lblBMI.text!, subString: arrayResults![indexPath.row].value,color: UIColor.init(hexString: "#157197"))
            cell.lblIBW.text = arrayResults![1].value
            cell.lblCal.text = arrayResults![2].value
            cell.lblRisk.text = arrayResults![indexPath.row].risk
            cell.lblRisk.backgroundColor = arrayResults![indexPath.row].color
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiabetesViewCell", for: indexPath) as! DiabetesViewCell
            cell.setVwBG(borderColor: UIColor.white)
            cell.img.image = UIImage(named: "Diabeties")
            cell.lblDiabetes.text = "Diabetes - " + arrayResults![3].value
            cell.lblDiabetes.attributedText = self.setAttributeString(mainString: cell.lblDiabetes.text!, subString: arrayResults![3].value, color: UIColor.init(hexString: "#157197"))
            cell.lblRisk.text = "Risk Level - " + arrayResults![3].risk
            cell.lblRisk.attributedText = self.setAttributeString(mainString: cell.lblRisk.text!, subString: arrayResults![3].risk, color: arrayResults![3].risk == "High" ? UIColor.red : UIColor.init(hexString: "#6DBF00"))
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 2{
            if arrayResults![4].risk == "Hypertensive"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiabetesViewCell", for: indexPath) as! DiabetesViewCell
                cell.setVwBG(borderColor: UIColor.white)
                cell.lblDiabetes.text = "Hypertension"
                cell.lblRisk.text = "Hypertensive"
                cell.lblRisk.textColor = UIColor.init(hexString: "#DF6F67")
                cell.img.image = UIImage(named: "Hypertension")
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "HypertensionViewCell", for: indexPath) as! HypertensionViewCell
                cell.setVwBG(borderColor: UIColor.white)
                cell.selectionStyle = .none
                cell.heightOfLblError.constant = 0.0
                if arrayResults![4].risk == "Age" || arrayResults![4].risk == "SBP" {
                    cell.heightOfLblError.constant = 120.0
                    return cell
                }
                else if arrayResults![4].risk == "Hypertensive"{
                    cell.heightOfLblError.constant = 120.0
                    cell.lbl_error.text = "You are Hypertensive"
                    return cell
                }
                
                let arrSep = arrayResults![4].value.components(separatedBy: " ")
                
                cell.lbl_risk_1.text = "1Yr - Risk - " + arrSep[4] + " , " + "Optimal - " + arrSep[5]
                let attr1 = self.setAttributeString(mainString: "1Yrs - Risk - " + arrSep[4], subString: arrSep[4], color: UIColor.init(hexString: "#157197"))
                let attr2 = self.setAttributeString(mainString: " , " + "Optimal - " + arrSep[5], subString: arrSep[5], color: UIColor.init(hexString: "#157197"))
                cell.lbl_risk_1.attributedText = self.appendAttributeStrings(firstString: attr1, secondString: attr2)
                
                cell.lbl_risk_2.text = "2Yrs - Risk - " + arrSep[2] + " , " + "Optimal - " + arrSep[3]
                let attr21 = self.setAttributeString(mainString: "2Yrs - Risk - " + arrSep[2], subString: arrSep[2], color: UIColor.init(hexString: "#157197"))
                let attr22 = self.setAttributeString(mainString: " , " + "Optimal - " + arrSep[3], subString: arrSep[3], color: UIColor.init(hexString: "#157197"))
                cell.lbl_risk_2.attributedText = self.appendAttributeStrings(firstString: attr21, secondString: attr22)
                
                cell.lbl_risk_4.text = "4Yrs - Risk - " + arrSep[0] + " , " + "Optimal - " + arrSep[1]
                let attr41 = self.setAttributeString(mainString: "4Yrs - Risk - " + arrSep[0], subString: arrSep[0], color: UIColor.init(hexString: "#157197"))
                let attr42 = self.setAttributeString(mainString: " , " + "Optimal - " + arrSep[1], subString: arrSep[1], color: UIColor.init(hexString: "#157197"))
                cell.lbl_risk_4.attributedText = self.appendAttributeStrings(firstString: attr41, secondString: attr42)
                
                return cell
            }
        }
        else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CVDViewCell", for: indexPath) as! CVDViewCell
            cell.setVwBG(borderColor: UIColor.white)
            cell.selectionStyle = .none
            cell.heightOfLblError.constant = 0.0
            if arrayResults![5].risk == "Age" || arrayResults![5].risk == "SBP"{
                cell.heightOfLblError.constant = 150.0
                return cell
            }
            let arrSep = arrayResults![5].value.components(separatedBy: " ")
            cell.lbl_risk_Heart.text = "Heart Age - " + arrSep[3]
            cell.lbl_risk_score.text = arrSep[0]
            cell.lbl_risk_normal.text = arrSep[1]
            cell.lbl_risk_optimal.text = arrSep[2]
            cell.lbl_risk.text = "Risk Level - " + arrayResults![5].risk
            cell.lbl_risk.attributedText = self.setAttributeString(mainString: cell.lbl_risk.text!, subString: arrayResults![5].risk, color: arrayResults![5].risk == "High" ? UIColor.red : UIColor.init(hexString: "#6DBF00"))
            
            return cell
        }
        else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StrokeViewCell", for: indexPath) as! StrokeViewCell
            cell.setVwBG(borderColor: UIColor.white)
            cell.selectionStyle = .none
            cell.heightOfLblError.constant = 0.0
            if arrayResults![6].risk == "Age"{
                cell.heightOfLblError.constant = 100.0
                return cell
            }
            cell.lblStroke.text = "Stroke Probability"
            cell.lblRisk.text = arrayResults![6].value
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "referred", for: indexPath)
            cell.textLabel?.text = "Reference : "
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12.0)
            cell.textLabel?.attributedText = self.appendAttributeStrings(firstString: self.setAttributeString(mainString: (cell.textLabel?.text)!, subString: "Reference : ", color: UIColor.black), secondString: "Framingham Heart Study".underline(location: 0, lenght: 22))
            //cell.textLabel?.underline()
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 5{
            let urlString = "https://www.framinghamheartstudy.org"
            if let url = URL(string: urlString)
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt IndexPath:IndexPath) -> CGFloat
    {
        if IndexPath.row == 0{
            return arrayResults![0].rowHeight
        }
        else if IndexPath.row == 1{
            return arrayResults![3].rowHeight
        }
        else if IndexPath.row == 2{
            return arrayResults![4].rowHeight
        }
        else if IndexPath.row == 3{
            return arrayResults![5].rowHeight
        }
        else if IndexPath.row == 4{
            return arrayResults![6].rowHeight
        }
        else if IndexPath.row == 5{
            return 44.0
        }
        return 0
    }
}

class BMIViewCell: UITableViewCell {
    
    @IBOutlet var lblBMI: UILabel!
    @IBOutlet var lblIBW: UILabel!
    @IBOutlet var lblCal: UILabel!
    @IBOutlet var lblRisk: UILabel!
    @IBOutlet var vwBG: UIView!
    
    func setVwBG(borderColor:UIColor){
        // corner radius
        vwBG.layer.cornerRadius = 5
        
        // border
        vwBG.layer.borderWidth = 2.0
        vwBG.layer.borderColor = borderColor.cgColor
        
        // shadow
        vwBG.layer.shadowColor = UIColor.black.cgColor
        vwBG.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG.layer.shadowOpacity = 0.7
        vwBG.layer.shadowRadius = 2.0
        
    }
    
    func setLblBG(){
        // corner radius
        //lblRisk.roundCorners(topLeft: 2.0, topRight: 2.0, bottomLeft: 2.0, bottomRight: 2.0)
        
        // shadow
        lblRisk.layer.shadowColor = UIColor.black.cgColor
        lblRisk.layer.shadowOffset = CGSize(width: 2, height: 2)
        lblRisk.layer.shadowOpacity = 0.7
        lblRisk.layer.shadowRadius = 2.0
    }
}
    
class DiabetesViewCell: UITableViewCell {
        
    @IBOutlet var lblDiabetes: UILabel!
    @IBOutlet var lblRisk: UILabel!
    @IBOutlet var vwBG: UIView!
    @IBOutlet var img: UIImageView!
    
    func setVwBG(borderColor:UIColor){
        // corner radius
        vwBG.layer.cornerRadius = 5
        
        // border
        vwBG.layer.borderWidth = 2.0
        vwBG.layer.borderColor = borderColor.cgColor
        
        // shadow
        vwBG.layer.shadowColor = UIColor.black.cgColor
        vwBG.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG.layer.shadowOpacity = 0.7
        vwBG.layer.shadowRadius = 2.0
    }
    
}

class HypertensionViewCell: UITableViewCell {
    
    @IBOutlet var lbl_risk_1: UILabel!
    @IBOutlet var lbl_risk_2: UILabel!
    @IBOutlet var lbl_risk_4: UILabel!
    
    @IBOutlet var vwBG: UIView!
    @IBOutlet var vwBG1: UIView!
    @IBOutlet var vwBG2: UIView!
    @IBOutlet var vwBG4: UIView!
    
    @IBOutlet var lbl_error: UILabel!
    @IBOutlet weak var heightOfLblError: NSLayoutConstraint!
    
    func setVwBG(borderColor:UIColor){
        // corner radius
        vwBG.layer.cornerRadius = 5
        
        // border
        vwBG.layer.borderWidth = 2.0
        vwBG.layer.borderColor = borderColor.cgColor
        
        // shadow
        vwBG.layer.shadowColor = UIColor.black.cgColor
        vwBG.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG.layer.shadowOpacity = 0.7
        vwBG.layer.shadowRadius = 2.0
        
        // shadow
        vwBG1.layer.shadowColor = UIColor.black.cgColor
        vwBG1.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG1.layer.shadowOpacity = 0.7
        vwBG1.layer.shadowRadius = 2.0
        
        // shadow
        vwBG2.layer.shadowColor = UIColor.black.cgColor
        vwBG2.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG2.layer.shadowOpacity = 0.7
        vwBG2.layer.shadowRadius = 2.0
        
        // shadow
        vwBG4.layer.shadowColor = UIColor.black.cgColor
        vwBG4.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG4.layer.shadowOpacity = 0.7
        vwBG4.layer.shadowRadius = 2.0
    }
    
}

class CVDViewCell: UITableViewCell {
    
    @IBOutlet var lbl_risk_Heart: UILabel!
    @IBOutlet var lbl_risk_score: UILabel!
    @IBOutlet var lbl_risk_optimal: UILabel!
    @IBOutlet var lbl_risk_normal: UILabel!
    @IBOutlet var lbl_risk: UILabel!
    
    @IBOutlet var vwBG: UIView!
    @IBOutlet var vwBG1: UIView!
    
    @IBOutlet var lbl_error: UILabel!
    @IBOutlet weak var heightOfLblError: NSLayoutConstraint!
    
    
    func setVwBG(borderColor:UIColor){
        // corner radius
        vwBG.layer.cornerRadius = 5
        
        // border
        vwBG.layer.borderWidth = 2.0
        vwBG.layer.borderColor = borderColor.cgColor
        
        // shadow
        vwBG.layer.shadowColor = UIColor.black.cgColor
        vwBG.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG.layer.shadowOpacity = 0.7
        vwBG.layer.shadowRadius = 2.0
        
        // shadow
        vwBG1.layer.shadowColor = UIColor.black.cgColor
        vwBG1.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG1.layer.shadowOpacity = 0.7
        vwBG1.layer.shadowRadius = 2.0
    
    }
    
}

class StrokeViewCell: UITableViewCell {
    
    @IBOutlet var lblStroke: UILabel!
    @IBOutlet var lblRisk: UILabel!
    @IBOutlet var vwBG: UIView!
    @IBOutlet weak var heightOfLblError: NSLayoutConstraint!
    
    func setVwBG(borderColor:UIColor){
        // corner radius
        vwBG.layer.cornerRadius = 5
        
        // border
        vwBG.layer.borderWidth = 2.0
        vwBG.layer.borderColor = borderColor.cgColor
        
        // shadow
        vwBG.layer.shadowColor = UIColor.black.cgColor
        vwBG.layer.shadowOffset = CGSize(width: 2, height: 2)
        vwBG.layer.shadowOpacity = 0.7
        vwBG.layer.shadowRadius = 2.0
    }
    
}

extension String {
    func underline(location:Int,lenght:Int) -> NSMutableAttributedString {
        
        let labelAtributes:[NSAttributedString.Key : Any]  = [
            NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#DF6F67"),
            NSAttributedString.Key.underlineColor: UIColor.init(hexString: "#DF6F67"),
            NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 12.0)
        ]
        let attributedString = NSMutableAttributedString(string: self,attributes: labelAtributes)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: location, length: lenght))
        return attributedString
    }
}
