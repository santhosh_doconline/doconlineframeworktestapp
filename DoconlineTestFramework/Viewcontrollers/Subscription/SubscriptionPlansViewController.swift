//
//  SubscriptionPlansViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 23/11/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import Firebase
//import NVActivityIndicatorView
//import Razorpay
import SwiftMessages
//import AppsFlyerLib


protocol ContinueUserPreviousActionsDelegate {
    func doUserPreviousActionIfFinishedSubscription()
}


class SubscriptionPlansViewController: UIViewController , NVActivityIndicatorViewable
    
//    ,RazorpayPaymentCompletionProtocol,ExternalWalletSelectionProtocol
    
//    , PGTransactionDelegate
{
   
    //one time coupon code changes
    @IBOutlet var vw_applyOneTimeCouponCode: UIView!
    @IBOutlet var lc_couponOTPTopConstraint: NSLayoutConstraint!
    @IBOutlet var tf_couponCodeOTP: UITextField!
    @IBOutlet var bt_applyCouponOTP: UIButton!
    @IBOutlet var lb_couponSuccessOrFailureMsg_OTP: UILabel!
    @IBOutlet var lb_planName_OTP: UILabel!
    @IBOutlet var lb_planAmount_OTP: UILabel!
    @IBOutlet var lb_discountedAmount_OTP: UILabel!
    @IBOutlet var lb_toBePaid_OTP: UILabel!
    @IBOutlet var bt_cancel_OTP: UIButton!
    @IBOutlet var bt_proceedToPay_OTP: UIButton!
    var isDiscountAppliedForOTP = false
    var discountedAmountOnOTP = ""
    var appliedDiscountCouponCode = ""
    
    
    @IBOutlet weak var lc_couponViewHeight: NSLayoutConstraint!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var vw_couponView: UIView!
    @IBOutlet var tf_coupon: UITextField!
    @IBOutlet var bt_couponApply: UIButton!
    
    @IBOutlet weak var vw_subscriptionStatus: CardView!
    @IBOutlet weak var LC_vw_subscrption_status: NSLayoutConstraint!
    
    @IBOutlet var iv_endDate: UIImageView!
    @IBOutlet var iv_startDate: UIImageView!
    @IBOutlet var LC_cancelButtonHeight: NSLayoutConstraint!
    @IBOutlet var LC_pendingPlanStatusHeight: NSLayoutConstraint!
    @IBOutlet var lb_noPlansStatus: UILabel!
    @IBOutlet var lb_pending_status: UILabel!
    @IBOutlet var LC_tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var lb_endOrNextTitle: UILabel!
    @IBOutlet weak var lb_status_message: UILabel!
   // @IBOutlet weak var lb_separator: UILabel!
    @IBOutlet weak var lb_subscribed_date_title: UILabel!
    @IBOutlet weak var lb_Sub_date: UILabel!
    @IBOutlet weak var lb_end_date_title: UILabel!
    @IBOutlet weak var lb_end_date: UILabel!
    @IBOutlet weak var lb_next_due_date_title: UILabel!
    @IBOutlet weak var lb_next_due_date: UILabel!
    
    @IBOutlet var lc_membershipStatusHeight: NSLayoutConstraint!
    @IBOutlet var LC_currentPlanNameHeight: NSLayoutConstraint! 
    @IBOutlet var lb_currentPlanName: UILabel!
    @IBOutlet var lb_next_due_title: UILabel!
    @IBOutlet var LC_tableView_height: NSLayoutConstraint!
    
    @IBOutlet var bt_cancel_subcription: UIButton!
    

    ///razorpay key instance
    private let rz_key = RazorpayDetails.rzKey
    ///razorpay secret instance
//    private let rz_secret = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.RZPAY_SECRET).stringValue!
    private let rz_secret = ""
    ///not used
    private let rz_authorization = "Basic cnpwX3Rlc3Rfb0J   MWGc1Q1lUclNqZUo6bUVLdzdQZ2U4ODhhN0E3WGRwR1J5eUdu"
    ///Instance of razorpay object
//    private var razorpay : Razorpay!
    @IBOutlet weak var tableView: UITableView!
    
   ///Subscription plans
   private var plans = [SubscriptionPlan]()
    
    ///linear bar instance reference
    @IBOutlet weak var lb_swipe_left: UILabel!
    
    ///Linear loading progress bar instance
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    /**
     linear bar configuration
     */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
   // var userDataModel : User!
   private let cellScaling : CGFloat = 0.9
   private var paymentID = ""
   private var oneTimePaymentOrderID = ""
   private var paymentPlan = ""
   private var paymentType = SubscriptionType.Subscription
   private var planType = ""
   private var choosenPlan:SubscriptionPlan?
    
    ///timer declaration
    var userSubscribedStatusCheckTimer : Timer?
    ///timer total seconds
    var totalSeconds : Int = 0
    var activePlanOrderCount = 0
    var currentPlan = ""
    var selectedPlan = 0
    
    ///Previous action delegate
    var previousActionDelegate : ContinueUserPreviousActionsDelegate?
    
    //paytm
    var strCheckSum = ""
    private var paymentGateWay = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lb_currentPlanName.textColor = Theme.buttonBackgroundColor
        hideOrAdjustViews(hide: true, viewHide: true, Height: 0)
        self.lb_pending_status.isHidden = true
        configureLinearProgressBar()
        getOneTimePaymentPlanDetails { (success) in
//            if !App.b2bUserType.lowercased().isEqual(PlanType.B2BPAID) {
               self.getSubscriptionPlans()
//            }else {
//                self.loadTableViewOnPlansFetch()
//            }
        }
        
       self.tableView.tableFooterView = UIView()
       self.tableView.estimatedRowHeight = 218
       self.tableView.rowHeight = UITableView.automaticDimension
        
        //self.bt_couponApply.cornerRadius = 5
        self.showOrHideCouponViewBasedOnUserType()
        self.bt_couponApply.backgroundColor = Theme.buttonBackgroundColor
    }
    
    func showOrHideCouponViewBasedOnUserType() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.lc_couponViewHeight.constant =  App.b2bUserType.lowercased().isEqual(PlanType.B2BPAID) ? 0 : 55
                self.vw_couponView.isHidden = App.b2bUserType.lowercased().isEqual(PlanType.B2BPAID)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    ///adjusts the views
    func hideOrAdjustViews(hide:Bool,viewHide:Bool,Height:CGFloat) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.LC_tableView_height.constant = self.tableView.contentSize.height //+ 200
            UIView.animate(withDuration: 0.3) {
                self.lb_end_date.isHidden = true
                self.lb_Sub_date.isHidden = hide
                self.lb_next_due_date.isHidden = hide
                self.lb_end_date_title.isHidden = true
                self.lb_subscribed_date_title.isHidden = hide
                self.lb_next_due_date_title.isHidden = hide
                // self.lb_separator.isHidden = hide
                self.lb_status_message.isHidden = viewHide
                self.vw_subscriptionStatus.isHidden = viewHide
                self.LC_vw_subscrption_status.constant = Height
                self.lb_currentPlanName.isHidden = hide
                self.LC_currentPlanNameHeight.constant = hide ? 0 : 25
                self.iv_endDate.isHidden = hide
                self.iv_startDate.isHidden = hide
                self.tableView.reloadData()
                self.lc_couponViewHeight.constant =  App.didUserSubscribed ? 0 : 55
                self.vw_couponView.isHidden = App.didUserSubscribed ? true : false
                if self.plans.count > 0 {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.linearBar.stopAnimation()
        if self.userSubscribedStatusCheckTimer != nil {
            self.userSubscribedStatusCheckTimer?.invalidate()
            self.userSubscribedStatusCheckTimer = nil
            self.totalSeconds = 0
        }
    }
    
    func getOneTimePaymentPlanDetails(completion: @escaping (_ success:Bool) -> Void) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        self.linearBar.startAnimation()
        
        let url = URL(string: AppURLS.URL_ONE_TIME_PLAN)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(App.getUserAccessToken() , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_ONE_TIME_VERSION_VALUE , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API) //New header for identification versioning
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request) { (data, response, error) in
            if error != nil
            {
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
                completion(false)
            }else {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("Subscription plans:\(jsonData)")
                            
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                completion(false)
                                print("performing error handling get one time plans list:\(jsonData)")
                            }else {
                              
                                var internal_order = 0
                                var plantype = ""
                                var type = ""
                                if let items = jsonData.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                    for item in items {
                                        var packageDiscrp = ""
                                       if let entity = item.object(forKey: Keys.KEY_ENTITY) as? String ,
                                        let period = item.object(forKey: Keys.KEY_PERIOD) as? String ,
                                        let description = item.object(forKey: Keys.KEY_DESCRIPTION) as? String ,
                                        let name = item.object(forKey: Keys.KEY_NAME) as? String ,
                                        let extra = item.object(forKey: Keys.KEY_EXTRA) as? NSDictionary ,
                                        let displayPrice = extra.object(forKey: Keys.KEY_DISPLAY_PRICE) as? Int ,
                                        let displayName = extra.object(forKey: Keys.KEY_DISPLAY_NAME) as? String ,
                                        let displayColor = extra.object(forKey: Keys.KEY_DISPLAY_COLOR) as? String ,
                                        let displayPeriod = extra.object(forKey: Keys.KEY_DISPLAY_PERIOD) as? String ,
                                        let featured = extra.object(forKey: Keys.KEY_FEAUTURED) as? Bool ,
                                        let iconImage = extra.object(forKey: Keys.KEY_ICON_IMAGE) as? String,
                                        let crossPrice = extra.object(forKey: Keys.KEY_CROSS_PRICE) as? Int ,
                                        let discountText = extra.object(forKey: Keys.KEY_DISCOUNT_TEXT) as? String {
                                            
                                            if let internalOrder = extra.object(forKey: Keys.KEY_INTERNAL_ORDER) as? Int {
                                                internal_order = internalOrder
                                                print("$$$$$$$=>Internal Order:=\(internal_order)=$$$$$$$")
                                            }
                                            
                                            if let onlyType = extra.object(forKey: Keys.KEY_TYPE) as? String {
                                                type = onlyType
                                                print("$$$$$$$=>One time type:=\(onlyType)=$$$$$$$")
                                            }
                                        
                                            if let planType = extra.object(forKey: Keys.KEY_PLAN_TYPE) as? String {
                                              plantype = planType
                                               print("$$$$$$$=>One time Plan type:=\(planType)=$$$$$$$")
                                            }
                                            
                                            if let packages = extra.object(forKey: Keys.KEY_PACKAGES) as? [String] {
                                                for pack in packages {
                                                    //let bulletPoint: String = "\u{2022}"
                                                    let formattedString: String = "\(pack)\n"
                                                    packageDiscrp += formattedString
                                                    print("Formatte string:\(packageDiscrp)")
                                                }
                                                
                                                let plan = SubscriptionPlan(id: "", amount: displayPrice, period: period, interval: 0, plan_name: displayName, description: description, currency: "", total_count: 0, cross_price: crossPrice, discount_text: discountText, packages:  packageDiscrp ,featured:featured,displayName:displayName,displayPeriod:displayPeriod,displayAmount:displayPrice,displayColor:displayColor,internal_order:internal_order)
                                                plan.iconURL = iconImage
                                                plan.type = type
                                                plan.planType = plantype
                                                self.plans.append(plan)
                                            }
                                        }
                                    }
                                 }
                                 completion(true)
                            }
                        }
                    }catch let err {
                        completion(false)
                        print("Error while getting onetime plan:\(err.localizedDescription)")
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    }
                }
            }
        }.resume()
        
    }
    
    //fetches the current subscription plans list
    func getSubscriptionPlans() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
   
        // self.linearBar.startAnimation()
        
        let url = URL(string: AppURLS.URL_RAZORPAY_PLANS)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
       // request.setValue(rz_key, forHTTPHeaderField: "key-id")
        request.httpMethod = HTTPMethods.GET
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if error != nil
            {
                DispatchQueue.main.async {
                       self.linearBar.stopAnimation()
                }
                print("Error while fetching data\(String(describing: error?.localizedDescription))")
            }
            else
            {
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    do
                    {
                        if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("Subscription plans:\(jsonData)")
                            
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling get subscription plans list:\(jsonData)")
                            }else {
                                
                                if let items = jsonData.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                    for item in items {
                                        if let planId = item.object(forKey: Keys.KEY_ID) as? String { //
                                            if let period = item.object(forKey: Keys.KEY_PERIOD) as? String {
                                                if let interval = item.object(forKey: Keys.KEY_INTERVAL) as? Int { //
                                                    if let innerItem = item.object(forKey: Keys.KEY_ITEM) as? NSDictionary {//
                                                        if let amount = innerItem.object(forKey: Keys.KEY_AMOUNT) as? Int {//
                                                            if let name = innerItem.object(forKey: Keys.KEY_NAME) as? String {//
                                                                if let currency = innerItem.object(forKey: Keys.KEY_CURRENCY) as? String {//
                                                                    if let totalcount = item.object(forKey: Keys.KEY_TOTAL_COUNT) as? Int {//
                                                                        var discount_text = ""
                                                                        var packageDiscrp = ""
                                                                        var crossPrice = 0
                                                                        var featured = false
                                                                        var displayName = ""
                                                                        var displayPrice = 0
                                                                        var displayPeriod = ""
                                                                        var displayColor = ""
                                                                        var internal_order = 0
                                                                        var itemPlanDescr = ""
                                                                        var iconUrl = ""
                                                                        var type = ""
                                                                        
                                                                        if let planDescription = innerItem.object(forKey: Keys.KEY_DESCRIPTION) as? String {
                                                                            itemPlanDescr = planDescription
                                                                        }
                                                                        
                                                                        if let extra = item.object(forKey: Keys.KEY_EXTRA) as? NSDictionary {
                                                                            if let dispName = extra.object(forKey: Keys.KEY_DISPLAY_NAME) as? String {
                                                                                displayName = dispName
                                                                            }
                                                                            
                                                                            if let dispPrice = extra.object(forKey: Keys.KEY_DISPLAY_PRICE) as? Int {
                                                                                displayPrice = dispPrice
                                                                                print("Display price :\(dispPrice)")
                                                                            }
                                                                            
                                                                            if let dispPeriod = extra.object(forKey: Keys.KEY_DISPLAY_PERIOD) as? String {
                                                                                displayPeriod = dispPeriod
                                                                                print("Display period :\(displayPeriod)")
                                                                            }
                                                                            
                                                                            if let isPlanFeatured = extra.object(forKey: Keys.KEY_FEAUTURED) as? Bool {
                                                                                featured = isPlanFeatured
                                                                            }
                                                                            
                                                                            if let discountText = extra.object(forKey: Keys.KEY_DISCOUNT_TEXT) as? String {
                                                                                discount_text = discountText
                                                                            }
                                                                            
                                                                            if let crossprice = extra.object(forKey: Keys.KEY_CROSS_PRICE) as? Int {
                                                                                crossPrice = crossprice
                                                                            }
                                                                            
                                                                            if let dspColor = extra.object(forKey: Keys.KEY_DISPLAY_COLOR) as? String {
                                                                                displayColor = dspColor
                                                                            }
                                                                            
                                                                            if let icon = extra.object(forKey:Keys.KEY_ICON_IMAGE) as? String {
                                                                                iconUrl = icon
                                                                            }
                                                                            
                                                                            if let internalOrder = extra.object(forKey: Keys.KEY_INTERNAL_ORDER) as? Int {
                                                                                internal_order = internalOrder
                                                                                print("$$$$$$$=>Internal Order:=\(internal_order)=$$$$$$$")
                                                                            }
                                                                            
                                                                            if let onlyType = extra.object(forKey: Keys.KEY_TYPE) as? String {
                                                                                type = onlyType
                                                                                print("$$$$$$$=>Plan type:=\(type)=$$$$$$$")
                                                                            }
                                                                            
                                                                            if let packages = extra.object(forKey: Keys.KEY_PACKAGES) as? [String] {
                                                                                for pack in packages {
                                                                                    //let bulletPoint: String = "\u{2022}"
                                                                                    let formattedString: String = "\(pack)\n"
                                                                                    packageDiscrp += formattedString
                                                                                    print("Formatte string:\(packageDiscrp)")
                                                                                }
                                                                                
                                                                                let plan = SubscriptionPlan(id: planId, amount: amount, period: period, interval: interval, plan_name: name, description: itemPlanDescr, currency: currency, total_count: totalcount, cross_price: crossPrice, discount_text: discount_text, packages: packageDiscrp ,featured:featured,displayName:displayName,displayPeriod:displayPeriod,displayAmount:displayPrice,displayColor:displayColor,internal_order:internal_order)
                                                                                plan.iconURL = iconUrl
                                                                                plan.type = type
                                                                                self.plans.append(plan)
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                               self.loadTableViewOnPlansFetch()
                            }
                        }
                    }catch let err {
                        print("Error while getting subscription plans:\(err.localizedDescription)")
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    }
                }
            }
            
        }).resume()
    }
    
    func loadTableViewOnPlansFetch() {
        DispatchQueue.main.async {
            self.linearBar.stopAnimation()
            self.tableView.reloadData()
            self.checkSubscriptionDetails()
            
            if self.plans.count == 0 {
                self.lb_noPlansStatus.isHidden = false
            }else {
                self.lb_noPlansStatus.isHidden = true
            }
        }
    }
    
    ///checks the subscription details and updates the ui
    func checkSubscriptionDetails() {
        let viewWidth = self.view.frame.width
        print("View width:\(viewWidth)")
        
        func hideLabeles(endDate:String,nextDueDate:String) {
            if endDate.isEmpty && !nextDueDate.isEmpty{
                self.bt_cancel_subcription.isHidden = false
                self.LC_cancelButtonHeight.constant = 34
                self.lb_next_due_date_title.text = "Next Due :"
                self.iv_endDate.image = UIImage(named: "image_start")
                self.lb_next_due_date.text = nextDueDate
                self.hideOrAdjustViews(hide: false, viewHide: false, Height: 155)
            }else if !endDate.isEmpty {
                self.bt_cancel_subcription.isHidden = true
                self.LC_cancelButtonHeight.constant = 0
                self.lb_next_due_date_title.text = "Ends On :"
                self.iv_endDate.image = UIImage(named: "image_end")
                self.lb_next_due_date.text = endDate
                self.hideOrAdjustViews(hide: false, viewHide: false, Height: 120)
            }else {
                self.lb_next_due_date_title.isHidden = true
                self.lb_next_due_date.isHidden = true
                self.iv_endDate.isHidden = true
            }
        }
        
        //Presents the plans subcribe status view height and text
        func showChoosePlanUIIfNotSubscribed(with text:String, statusColor:UIColor) {
            self.lb_status_message.text = text
            self.lb_status_message.textColor = Theme.buttonBackgroundColor
            self.bt_cancel_subcription.isHidden = true
            self.lb_pending_status.isHidden = true
            self.LC_pendingPlanStatusHeight.constant = 0
            self.LC_cancelButtonHeight.constant = 0
        }
        
        if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("b2b") {
            self.hideOrAdjustViews(hide: true, viewHide: false, Height: viewWidth <= 375 ? 48 : 31)
            showChoosePlanUIIfNotSubscribed(with: "You are under DOCONLINE ENTERPRISE membership", statusColor: UIColor.init(hexString: "#32CD32"))
            //   self.vw_couponView.isHidden = true
        }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("b2c"){
            var nextDueDate = ""
            var endDate = ""
            var traiEndsAt = ""
            
            if let subDetails = App.subscription_details {
                self.hideOrAdjustViews(hide: false, viewHide: false, Height: 120)
                let createddate = subDetails.trail_ends_at == nil ? "" : self.getFormattedDateAndTime(dateString: subDetails.trail_ends_at!)
                endDate = subDetails.ends_at!.isEmpty ? "" :  self.getFormattedDateAndTime(dateString: subDetails.ends_at!)
                
                
                if let chargeAt = subDetails.next_charge_date {
                    nextDueDate = chargeAt.isEmpty ? "" : self.getFormattedDateAndTime(dateString: subDetails.next_charge_date!)
                }
                
                if let traiendsAt = subDetails.trail_ends_at {
                    traiEndsAt = traiendsAt.isEmpty ? "" : self.getFormattedDateAndTime(dateString: traiendsAt)
                }
                
                
                print("Ends at ===>\(self.convertToLocal(time: subDetails.ends_at!))")
                self.lb_status_message.text = "You are under"
                self.lb_currentPlanName.text = "\(subDetails.plan_name != nil ? subDetails.plan_name!.uppercased() : "")"
                self.currentPlan = subDetails.plan_name != nil ? subDetails.plan_name! : self.lb_status_message.text!
                print("Display name: \(self.currentPlan)")
                self.lb_end_date.text = endDate
                self.lb_Sub_date.text = createddate
                
                self.lb_status_message.textColor = UIColor.init(hexString: "#32CD32")
                self.bt_cancel_subcription.isHidden = true
                // self.vw_couponView.isHidden = true
                
                if !traiEndsAt.isEmpty {
                    self.lb_Sub_date.text = traiEndsAt
                    print("Trail ends at:\(traiEndsAt)")
                }
                
                hideLabeles(endDate: endDate, nextDueDate: nextDueDate)
                
                self.activePlanOrderCount = subDetails.internal_order == nil ? 0 : subDetails.internal_order!
                
                print("SubscribedDate:\(createddate)  End date:\(endDate) NextDue:\(nextDueDate) internalOrder:\( self.activePlanOrderCount)")
                self.tableView.reloadData()
                
                if let pending = App.pending_subscription {
                    let displayName = pending.display_name == nil ? "" :  pending.display_name!
                    self.lb_pending_status.text = "Your membership will be upgraded to the \(displayName.uppercased()) once the present membership ends."
                    self.lb_pending_status.isHidden = false
                    self.LC_pendingPlanStatusHeight.constant = 37
                    self.hideOrAdjustViews(hide: false, viewHide: false, Height: 155)
                }else {
                    self.LC_pendingPlanStatusHeight.constant = 0
                    self.lb_pending_status.isHidden = true
                    self.hideOrAdjustViews(hide: false, viewHide: false, Height: endDate.isEmpty ? 155 : 120)
                }
                
                if endDate.isEmpty && nextDueDate.isEmpty{
                    self.lb_next_due_date_title.isHidden = true
                    self.lb_next_due_date.isHidden = true
                    self.iv_endDate.isHidden = true
                    self.bt_cancel_subcription.isHidden = false
                    self.LC_cancelButtonHeight.constant = 34
                    self.LC_vw_subscrption_status.constant = 130
                }
                
            }else {
                self.hideOrAdjustViews(hide: true, viewHide: true, Height: 31) //48
                self.LC_tableViewTopConstraint.constant = 8
                showChoosePlanUIIfNotSubscribed(with: "CHOOSE YOUR PLAN & PAY", statusColor: .red)
                // self.vw_couponView.isHidden = false
            }
        }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.FAMILY_PACK) ||
             App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.CORPARATE_PACKAGE){
            var chargeAtDate = ""
            var endDate = ""
            
            if let subDetails = App.subscription_details {
                self.hideOrAdjustViews(hide: false, viewHide: false, Height: 120)
                endDate = subDetails.ends_at!.isEmpty ? "" :  self.getFormattedDateAndTime(dateString: subDetails.ends_at!)
                
                if let chargeAt = subDetails.next_charge_date {
                    chargeAtDate = chargeAt.isEmpty ? "" : self.getFormattedDateAndTime(dateString: subDetails.next_charge_date!)
                }
                
                self.lb_status_message.text = "You are under"
                self.lb_currentPlanName.text = "\(subDetails.plan_name != nil ? subDetails.plan_name!.uppercased() : "")"
                self.currentPlan = subDetails.plan_name != nil ? subDetails.plan_name! : self.lb_status_message.text!
                print("Display name: \(self.currentPlan)")
                self.lb_end_date.text = endDate
                self.lb_Sub_date.text = chargeAtDate
                
                self.lb_status_message.textColor = UIColor.init(hexString: "#32CD32")
                self.bt_cancel_subcription.isHidden = true
                self.LC_pendingPlanStatusHeight.constant = 0
                self.lb_pending_status.isHidden = true
                hideLabeles(endDate: endDate, nextDueDate: chargeAtDate)
            }else {
                self.hideOrAdjustViews(hide: true, viewHide: true, Height: 31) //48
                showChoosePlanUIIfNotSubscribed(with: "CHOOSE YOUR PLAN & PAY", statusColor: .red)
            }
            
            
        }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.ONE_TIME) || App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.B2BPAID){
            self.hideOrAdjustViews(hide: true, viewHide: false, Height: 31)
            showChoosePlanUIIfNotSubscribed(with: "You have 1 consultation credit", statusColor: UIColor.init(hexString: "#32CD32"))
            
        }else if App.didUserSubscribed  {
            self.hideOrAdjustViews(hide: true, viewHide: false, Height: viewWidth <= 375 ? 48 : 31)
            showChoosePlanUIIfNotSubscribed(with: "You are under DOCONLINE \(App.userSubscriptionType.uppercased()) membership", statusColor: UIColor.init(hexString: "#32CD32"))
            //  self.vw_couponView.isHidden = true
        }else {
            self.hideOrAdjustViews(hide: true, viewHide: false, Height: 31) //48
            showChoosePlanUIIfNotSubscribed(with: "CHOOSE YOUR PLAN & PAY", statusColor: .red)
            // self.vw_couponView.isHidden = false
        }
        
       
        
        
        if let subDetails = App.subscription_details {
            if (subDetails.status ?? "").isEqual(PlanType.PaymentStatus.HALTED) && App.userSubscriptionType.lowercased().isEqual(PlanType.B2C) {
                var tableViewIndex = 1
                for (index,plan) in self.plans.enumerated() {
                    if plan.plan_id ?? "" == subDetails.plan_id ?? "" {
                        tableViewIndex = index
                    }
                }
                let indexPath = IndexPath(row: tableViewIndex, section: 0)
                if let cell = tableView.cellForRow(at: indexPath) {
                    let bottom = CGPoint(x: 0, y: cell.center.y)
                    scrollView.setContentOffset(bottom, animated: true)
                }else {
                    let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
                    scrollView.setContentOffset(bottomOffset, animated: true)
                }
            }
        }
    }
    
    
    func convertToLocal(time:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: time)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
        let timeStamp =  date == nil ? "" : dateFormatter.string(from: date!)
        return timeStamp
    }
   
    
    ///initializes the razorpay object and shows the payment page
    /// - Parameter options : takes the option to payment page initialization
    func showPaymentForm(options:Dictionary<AnyHashable,Any>) {
//        razorpay = Razorpay.initWithKey(rz_key, andDelegate: self)
//        razorpay.setExternalWalletSelectionDelegate(self)
//        razorpay.open(options, display: self)
    }
    
    /**
     Unwind segue action
     */
    @IBAction func unwindToSubscriptionPlans(segue:UIStoryboardSegue) {
        
    }

    ///performs the upgrade action
    @objc func upgradeSubscription(_ sender: UIButton) {
        
        if (App.subscription_details?.status ?? "").isEqual(PlanType.PaymentStatus.HALTED){
            self.confirmIsPaymentIsCancelled(tag: sender.tag)
            return
        }
        
        let plan = self.plans[sender.tag]
        self.planType = plan.type ?? ""
        
        if planType == PlanType.ONE_TIME {
         
        }else if planType == PlanType.RECURRING {
            let alert = UIAlertController(title: "You are currently subscribed to a \(self.currentPlan) plan", message: "Do you want to upgrade to a \(plan.display_name != nil ? plan.display_name! : "") Plan?", preferredStyle: .alert)
            let Okay = UIAlertAction(title: "Upgrade", style: .default) { (action) in
                
                if !NetworkUtilities.isConnectedToNetwork()
                {
                    self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                    return
                }
                
                self.planType = plan.type ?? ""
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)
                let procureURL =  AppURLS.URL_SUBSCRIPTION_UPGRADE + "\(plan.plan_id ?? "")"
                var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
                request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
                request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
                request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
                request.httpMethod = HTTPMethods.POST
                request.httpBody = "platform=ios".data(using: .utf8)
                self.startAnimating()
                
                session.dataTask(with: request) { (data, response, error) in
                    if let error = error
                    {
                        DispatchQueue.main.async(execute: {
                            self.linearBar.stopAnimation()
                            self.stopAnimating()
                            print("Error==> : \(error.localizedDescription)")
                            self.didShowAlert(title: "", message: error.localizedDescription)
                            // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                        })
                    }
                    if let data = data
                    {
                        print("data =\(data)")
                    }
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                                print("Upgrade Subscribed response :\(resultJSON)")
                                
                                let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                                if !errorStatus {
                                    print("performing error handling get subscription :\(resultJSON)")
                                    if httpResponse.statusCode == 412 {
                                        if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                            if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                                DispatchQueue.main.async {
                                                    self.stopAnimating()
                                                    self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.SubscriptionPlans)
                                                }
                                            }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
                                                DispatchQueue.main.async {
                                                    self.stopAnimating()
                                                    self.showVerifyMobileMessage(title: "Email Verification", message: email, segue: StoryboardSegueIDS.ID_VERIFY_EMAIL, tag: 2, fromView: FromView.SubscriptionPlans)
                                                }
                                            }
                                        }
                                    }else if let message = resultJSON.object(forKey:Keys.KEY_MESSAGE) as? String {
                                        DispatchQueue.main.async {
                                            self.stopAnimating()
                                            self.didShowAlert(title:"",message:message)
                                        }
                                    }
                                }else
                                {
                                    
                                    var email = ""
                                    var phone = ""
                                    var address = ""
                                    var user_id = ""
                                    
                                    if let user = App.userDetails {
                                        if let uEmail = user.email {
                                            email = uEmail
                                        }
                                        
                                        if let uid = user.user_id {
                                            user_id = "\(uid)"
                                        }
                                        
                                        if let uPhone = user.phone {
                                            phone = uPhone
                                        }
                                        
                                        if let uadd1 = user.address1 , let uadd2 = user.address2 , let ucity = user.city ,let ustate = user.state ,let upincode = user.pincode , let ucountry = user.country {
                                            //  let countryName = self.getCountryNameById(countryID:ucountry)
                                            address = "\(uadd1)\(uadd2.isEmpty ? "" : ",\(uadd2)"),\(ucity),\(ustate),\(upincode),\(ucountry)"
                                            address = uadd1
                                        }
                                    }
                                    
                                    if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                        if let sub_id = data.object(forKey: Keys.KEY_RAZORPAY_ID) as? String {
                                            // let logo = UIImage(named:"DoctorLogo")
                                            let options : Dictionary<AnyHashable,Any> = [
                                                "key": self.rz_key,
                                                //  "amount" : plan.plan_amount!,
                                                "subscription_id" :  sub_id,
                                                //   "recurring": true ,
                                                "name": "DocOnline",
                                                "image" : "https://app.doconline.com/assets/images/logo.png" ,
                                                "description": plan.plan_name!,
                                                "prefill" : [
                                                    "email" : email,
                                                    "contact": phone
                                                ],
                                                "notes": [
                                                    "doconline_user_id" : user_id,
                                                    "platform": "ios",
                                                    "address": address,
                                                    "action_type" : "upgrade",
                                                    "subscription_id" :  sub_id
                                                ]
                                            ]
                                            self.paymentID = sub_id
                                            self.paymentType = SubscriptionType.Upgrade
                                            self.paymentPlan = plan.display_name == nil ? "" : plan.display_name!
                                            DispatchQueue.main.async(execute: {
                                                print("Razorpay options:\(options)")
                                                self.stopAnimating()
                                                self.showPaymentForm(options:options )
                                            })
                                        }
                                    }
                                }
                            }
                        }catch let error{
                            print("err:\(error.localizedDescription)")
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            })
                        }
                        
                    }
                    }.resume()
                
            }
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(cancel)
            alert.addAction(Okay)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func confirmIsPaymentIsCancelled(tag:Int) {
        if let subscription = App.subscription_details {
            if (subscription.status ?? "").isEqual(PlanType.PaymentStatus.CANCELLED) || !App.didUserSubscribed {
                self.retryPaymentIfHaltedState(tag: tag)
            }else if (subscription.status ?? "").isEqual(PlanType.PaymentStatus.HALTED){
                let alert = UIAlertController(title: "Note", message: "Your Current subscription plan will be canceled. However, on renewing your membership you can resume access to our services", preferredStyle: .alert)
                let retryPayment = UIAlertAction(title: "Retry Payment", style: .default, handler: { (action) in
                    self.cancelSubscription(completionHandler: { (success) in
                        self.retryPaymentIfHaltedState(tag: tag)
                    })
                })
                let cancel = UIAlertAction(title: "Cancel", style: .cancel , handler: nil)
                alert.addAction(retryPayment)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func retryPaymentIfHaltedState(tag:Int) {
        let plan = self.plans[tag]
        self.subscribePlan(plan: plan)
    }

    
    ///performs the new subscription action
    @objc func subscribeNow(_ sender: UIButton) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        let plan = self.plans[sender.tag]
        self.planType = plan.type ?? ""
    //   print("Plant type:::=>\(self.planType)")
        if planType == PlanType.ONE_TIME {
            
            if plan.display_amount != nil && plan.display_amount! != 0 {
                selectedPlan = 0
                var amount = String(plan.display_amount!)
                amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
                let onlyAmount = amount.components(separatedBy: ".")
                
                if(App.ChatType == MessageServer.PRODUCTION)
                {
//                    guard let tracker = GAI.sharedInstance().defaultTracker else {return}
//                    let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
//                        withCategory: "Select Plan \(onlyAmount[0]) Click in iOS",
//                        action: "Payment Plan Selection",
//                        label: "Select Plan \(onlyAmount[0]) Click in iOS to select the plan",
//                        value: nil).build()
//                    tracker.send(eventTracker as! [AnyHashable: Any])
                    
//                    AppsFlyerTracker.shared().trackEvent(AFEventInitiatedCheckout, withValues: [
//                        AFEventParamContentType : "ONE_TIME"
//                        ]);
                }
                
            }
            
           // self.getOrderIdForOneTimePayment(plan: plan)
            //New one time couopon code
            self.lb_couponSuccessOrFailureMsg_OTP.isHidden = true
            self.lb_planName_OTP.text = plan.display_name ?? ""
            var planAmount = String(plan.display_amount ?? 000)
            planAmount.insert(contentsOf: ".", at: planAmount.index(planAmount.endIndex, offsetBy: -2))

            self.lb_planAmount_OTP.text = "₹\(planAmount)"
            self.lb_discountedAmount_OTP.text = "₹0.00"
            self.lb_toBePaid_OTP.text = "₹\(planAmount)"
            self.bt_applyCouponOTP.setTitle("Apply", for: .normal)
            self.choosenPlan = plan
            
            if App.b2bUserType.lowercased().isEqual(PlanType.B2BPAID) || App.b2bUserType.lowercased().isEqual(PlanType.B2B) {
                if self.planType.lowercased().isEqual(PlanType.ONE_TIME) ,let choosenPlan = self.choosenPlan {
                    self.getOrderIdForOneTimePayment(plan: choosenPlan, coupon:  self.appliedDiscountCouponCode)
                }
            }else if (plan.planType ?? "").isEqual(PlanType.FAMILY_PACK)  {
                self.openCouponViewForOTP(show: true) //self.getOrderIdForOneTimePayment(plan: plan, coupon: "")
            } else {
                self.openCouponViewForOTP(show: true)
            }
        } else if planType == PlanType.RECURRING {
            
            if plan.display_amount != nil && plan.display_amount! != 0 {
                selectedPlan = 1
                var amount = String(plan.display_amount!)
                amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
                let onlyAmount = amount.components(separatedBy: ".")
                if(App.ChatType == MessageServer.PRODUCTION)
                {
//                    guard let tracker = GAI.sharedInstance().defaultTracker else {return}
//                    let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
//                        withCategory: "Select Plan \(onlyAmount[0]) Click in iOS",
//                        action: "Payment Plan Selection",
//                        label: "Select Plan \(onlyAmount[0]) Click in iOS to select the plan",
//                        value: nil).build()
//                    tracker.send(eventTracker as! [AnyHashable: Any])
                    
//                    AppsFlyerTracker.shared().trackEvent(AFEventInitiatedCheckout, withValues: [
//                        AFEventParamContentType : "SUBSCRIPTION"
//                        ]);
                }
            }
            
             self.subscribePlan(plan: plan)
        }
    }
    
    func getOrderIdForOneTimePayment(plan:SubscriptionPlan ,coupon:String) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let procureURL = AppURLS.URL_GET_OREDER_ID_ONE_TIME_PAYMENT
        var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
      //  request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
      //  request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_ONE_TIME_VERSION_VALUE , forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API)
        request.httpMethod = HTTPMethods.POST

        //one time coupon code changes
       // var couponData: [String:Any] = [ Keys.KEY_PLAN_TYPE: plan.planType ?? ""]
        var jsonDataString = "\(Keys.KEY_PLAN_TYPE)=\(plan.planType ?? "")"
        if !coupon.isEmpty {
            jsonDataString += "&\(Keys.KEY_DISCOUNT_CODE)=\(coupon)"
        }

        request.httpBody = jsonDataString.data(using: String.Encoding.utf8)

        //end
        print("djfohfh:\(jsonDataString)")
        startAnimating()
        
        session.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    self.linearBar.stopAnimation()
                    self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                do
                {
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        print("Subscribed response :\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling get subscription :\(resultJSON)")
                            if httpResponse.statusCode == 412 {
                                if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                        DispatchQueue.main.async {
                                            self.stopAnimating()
                                            self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.SubscriptionPlans)
                                        }
                                    }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
                                        DispatchQueue.main.async {
                                            self.stopAnimating()
                                            self.showVerifyMobileMessage(title: "Email Verification", message: email, segue: StoryboardSegueIDS.ID_VERIFY_EMAIL, tag: 2, fromView: FromView.SubscriptionPlans)
                                        }
                                    }
                                }
                            }else if let message = resultJSON.object(forKey:Keys.KEY_MESSAGE) as? String {
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    self.didShowAlert(title:"",message:message)
                                }
                            }
                        }else
                        {
                            
                            var email = ""
                            var phone = ""
                            var address = ""
                            var user_id = ""
                            
                            if let user = App.userDetails {
                                if let uEmail = user.email {
                                    email = uEmail
                                }
                                
                                if let uid = user.user_id {
                                    user_id = "\(uid)"
                                }
                                
                                if let uPhone = user.phone {
                                    phone = uPhone
                                }
                                
                                if let uadd1 = user.address1 , let uadd2 = user.address2 , let ucity = user.city ,let ustate = user.state ,let upincode = user.pincode , let ucountry = user.country {
                                    //  let countryName = self.getCountryNameById(countryID:ucountry)
                                    address = "\(uadd1)\(uadd2.isEmpty ? "" : ",\(uadd2)"),\(ucity),\(ustate),\(upincode),\(ucountry)"
                                    address = uadd1
                                }
                            }
                            
                            if let code = resultJSON.object(forKey: Keys.KEY_CODE) as? Int,
                                let data = resultJSON.object(forKey: Keys.KEY_DATA) as? String {
                                if code == 200 {
                                    let options : Dictionary<AnyHashable,Any> = [
                                        "key": self.rz_key,
                                        "name": "DocOnline",
                                        "order_id" :  data,
                                        "image" : "https://app.doconline.com/assets/images/logo.png" ,
                                        "description": plan.description ?? "",
                                        //"amount" : plan.display_amount ?? 15000 ,
                                        "external" : ["wallets" : ["paytm"]],
                                        "prefill" : [
                                            "email" : email,
                                            "contact": phone
                                        ],
                                        "notes": [
                                            "doconline_user_id" : user_id,
                                            "platform": "ios",
                                            "address": address,
                                            "action_type" : "fresh",
                                            "order_id" :  data
                                        ]
                                    ]
                        
                                    
                                    self.oneTimePaymentOrderID = data
                                    self.paymentType = SubscriptionType.OneTime
                                    self.paymentPlan = plan.display_name ?? ""
                                    
                                    DispatchQueue.main.async(execute: {
                                        print("Razorpay options:\(options)")
                                        self.stopAnimating()
                                        self.showPaymentForm(options:options )
                                    })
                                }
                            }
                        }
                    }
                }catch let error{
                    print("err:\(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    })
                }
                
            }
            }.resume()
    }
    
    func subscribePlan(plan:SubscriptionPlan) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let procureURL = AppURLS.URL_SUBSCRIBE_NOW + "\(plan.plan_id!)"
        var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.POST
        request.httpBody = "platform=ios".data(using: .utf8)
        
        startAnimating()
        
        session.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    self.linearBar.stopAnimation()
                    self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                do
                {
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        print("Subscribed response :\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling get subscription :\(resultJSON)")
                            if httpResponse.statusCode == 412 {
                                if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                        DispatchQueue.main.async {
                                            self.stopAnimating()
                                            self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.SubscriptionPlans)
                                        }
                                    }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
                                        DispatchQueue.main.async {
                                            self.stopAnimating()
                                            self.showVerifyMobileMessage(title: "Email Verification", message: email, segue: StoryboardSegueIDS.ID_VERIFY_EMAIL, tag: 2, fromView: FromView.SubscriptionPlans)
                                        }
                                    }
                                }
                            }else if let message = resultJSON.object(forKey:Keys.KEY_MESSAGE) as? String {
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    self.didShowAlert(title:"",message:message)
                                }
                            }
                        }else
                        {
                            
                            var email = ""
                            var phone = ""
                            var address = ""
                            var user_id = ""
                            
                            if let user = App.userDetails {
                                if let uEmail = user.email {
                                    email = uEmail
                                }
                                
                                if let uid = user.user_id {
                                    user_id = "\(uid)"
                                }
                                
                                if let uPhone = user.phone {
                                    phone = uPhone
                                }
                                
                                if let uadd1 = user.address1 , let uadd2 = user.address2 , let ucity = user.city ,let ustate = user.state ,let upincode = user.pincode , let ucountry = user.country {
                                    //  let countryName = self.getCountryNameById(countryID:ucountry)
                                    address = "\(uadd1)\(uadd2.isEmpty ? "" : ",\(uadd2)"),\(ucity),\(ustate),\(upincode),\(ucountry)"
                                    address = uadd1
                                }
                            }
                            
                            if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                if let sub_id = data.object(forKey: Keys.KEY_RAZORPAY_ID) as? String {
                                    let logo = UIImage(named:"DoctorLogo")
                                    let options : Dictionary<AnyHashable,Any> = [
                                        "key": self.rz_key,
                                        //   "amount" : plan.plan_amount!,
                                        "subscription_id" :  sub_id,
                                        //   "recurring": true ,
                                        "name": "DocOnline",
                                        "image" : "https://app.doconline.com/assets/images/logo.png" ,
                                        "description": plan.plan_name!,
                                        "prefill" : [
                                            "email" : email,
                                            "contact": phone
                                        ],
                                        "notes": [
                                            "doconline_user_id" : user_id,
                                            "platform": "ios",
                                            "address": address,
                                            "action_type" : "fresh",
                                            "subscription_id" :  sub_id
                                        ]
                                    ]
                                    self.paymentID = sub_id
                                    self.planType = plan.type ?? ""
                                    self.paymentType = SubscriptionType.Subscription
                                    self.paymentPlan = plan.display_name == nil ? "" : plan.display_name!
                                    DispatchQueue.main.async(execute: {
                                        print("Razorpay options:\(options)")
                                        self.stopAnimating()
                                        self.showPaymentForm(options:options )
                                    })
                                }
                            }
                        }
                    }
                }catch let error{
                    print("err:\(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    })
                }
                
            }
            }.resume()
    }
    
    @objc func getSuccessStatusAfterOneTimePaymentSuccess() {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        /*let data = [Keys.KEY_RAZORPAY_ORDER_ID : self.oneTimePaymentOrderID ,
                    Keys.KEY_RAZORPAY_PAYEMENT_ID : self.paymentID,
                    Keys.KEY_PAYMENT_GATEWAY: self.paymentGateWay]*/
        
         let couponCode = self.tf_coupon.text! as String
         var data = [ Keys.KEY_RAZORPAY_ORDER_ID : self.oneTimePaymentOrderID ,
                     Keys.KEY_RAZORPAY_PAYEMENT_ID : self.paymentID,
                     Keys.KEY_PAYMENT_GATEWAY: self.paymentGateWay,
                     Keys.KEY_COUPON_CODE : couponCode]
        if couponCode == ""{
            data = [ Keys.KEY_RAZORPAY_ORDER_ID : self.oneTimePaymentOrderID ,
                     Keys.KEY_RAZORPAY_PAYEMENT_ID : self.paymentID,
                     Keys.KEY_PAYMENT_GATEWAY: self.paymentGateWay]
        }
        print("Payment success check data:\(data)")
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            NetworkCall.performPOST(url: AppURLS.URL_ONE_TIME_PAYMENT_SUCCESS, data: jsonData) { (succes, response, statusCode, error) in
              //  print("Success check of payment:\(succes) resp:\(response)")
                if statusCode ?? 0 == 200 || succes{
                    DispatchQueue.main.async {
                        self.tf_coupon.text = ""
                    }
                    let plan = self.plans[self.selectedPlan]
                    if plan.display_amount != nil && plan.display_amount! != 0 {
                        var amount = String(plan.display_amount!)
                        amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
                        let onlyAmount = amount.components(separatedBy: ".")
                        if(App.ChatType == MessageServer.PRODUCTION)
                        {
//                            GATracker.shared().trackEvent(withCategory: GATRackerCategories.Payment.rawValue , action: GATRackerActions.OneTimePaymentSuccess.rawValue , label: plan.display_name ?? "ios", value:  NSNumber(value: Double(onlyAmount[0]) ?? 0))
                            
//                            AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [
//                                AFEventParamContentId:"1",
//                                AFEventParamContentType : "ONE_TIME",
//                                AFEventParamRevenue: onlyAmount[0],
//                                AFEventParamCurrency:"INR"
//                                ]);
                        }
                    }
                    self.getUserState { (success,newuser) in
                        print("Finish fetching userstate : \(success) ")
                        
                        let randomInt = self.randomIntFrom(start: 2, to: 4)
                        if App.didUserSubscribed && App.isFromView == FromView.BookingView {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                self.stopAnimating()
                                self.previousActionDelegate?.doUserPreviousActionIfFinishedSubscription()
                            }
                        }else if App.didUserSubscribed && App.isFromView == FromView.ChatHistoryView {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                self.stopAnimating()
                                self.previousActionDelegate?.doUserPreviousActionIfFinishedSubscription()
                            }
                        }else if App.didUserSubscribed && App.isFromView == FromView.HomeView {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                self.stopAnimating()
                                self.previousActionDelegate?.doUserPreviousActionIfFinishedSubscription()
                            }
                        }else if App.isFromView == FromView.SubscriptionPlans || App.isFromView == FromView.SettingsView {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                self.performSegue(withIdentifier:StoryboardSegueIDS.ID_PAYMENT_STATUS_SEGUE,sender:self)
                            }
                        }else{
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                self.performSegue(withIdentifier:StoryboardSegueIDS.ID_PAYMENT_STATUS_SEGUE,sender:self)
                            }
                        }
                        
                    }
                }
                else if statusCode ?? 0 == 422 && self.paymentGateWay == "paytm"
                {
                    if let message = response?.object(forKey: Keys.KEY_MESSAGE) as? String {
                        DispatchQueue.main.async {
                            self.stopAnimating()
                            self.didShowAlert(title: "Paytm", message: message)
                        }
                    }
                }
                else {
                    //  let _ = self.check_Status_Code(statusCode: statusCode ?? 0 , data: response)
                    let randomInt = self.randomIntFrom(start: 2, to: 4)
                    self.perform(#selector(self.getSuccessStatusAfterOneTimePaymentSuccess), with: nil, afterDelay: TimeInterval(randomInt))
                }
            }
        }catch let err {
            print("Error while converting one time payment success data :\(err.localizedDescription)")
            let randomInt = self.randomIntFrom(start: 2, to: 4)
            self.perform(#selector(self.getSuccessStatusAfterOneTimePaymentSuccess), with: nil, afterDelay: TimeInterval(randomInt))
        }
    }
    
    ///Performs the rest call to get the subcribed payment status and updates the UI
    func getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom:Int) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }

      //  self.startAnimating()

        NetworkCall.performGet(url: AppURLS.URL_User_state) { (success, response, statusCode, error) in
            if !success {
                _ = statusCode == nil ? false : self.check_Status_Code(statusCode: statusCode! , data: nil)
                if let err = error {
                    print("Error while getting subscription details:\(err.localizedDescription)")
                }
                DispatchQueue.main.async {
                    self.stopAnimating()
                    if isCalledFrom == 1 || isCalledFrom == 5{
                        self.tableView.reloadData()
                        self.checkSubscriptionDetails()
                    }
                }
            }else {
                if let jsonData = response {
                    if let data = jsonData.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                        self.getSubscriptionDetails(data: data, completionHandler: {
                            print("Completed")
                            let randomInt = self.randomIntFrom(start: 2, to: 4)
                            if App.didUserSubscribed && App.isFromView == FromView.BookingView {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                    self.stopAnimating()
                                  //  self.performSegue(withIdentifier: StoryboardSegueIDS.ID_GOTO_BOOK_CONSULTATION_AFTER_PAYMENT, sender: self)
                                   self.previousActionDelegate?.doUserPreviousActionIfFinishedSubscription()
                                }
                            }else if App.didUserSubscribed && App.isFromView == FromView.ChatHistoryView {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                    self.stopAnimating()
                                   // NotificationCenter.default.post(name: NSNotification.Name(rawValue:"ConnectToChat"), object: nil)
                                   // self.performSegue(withIdentifier: StoryboardSegueIDS.ID_GOTO_CHAT_HISOTORY_AFTER_PAYMENT, sender: self)
                                   self.previousActionDelegate?.doUserPreviousActionIfFinishedSubscription()
                                }
                            }else if App.didUserSubscribed && App.isFromView == FromView.HomeView {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                    self.stopAnimating()
                                   self.previousActionDelegate?.doUserPreviousActionIfFinishedSubscription()
                                }
                            }else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(randomInt)) {
                                    if isCalledFrom != 5{ ///(*)
                                     self.getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom: 3)
                                   }
                                }
                            }
  
                            DispatchQueue.main.async {
                                if isCalledFrom == 1 {
                                    self.stopAnimating()
                                    var endDate = ""
                                    if let subDetails = App.subscription_details {
                                        endDate = (subDetails.ends_at ?? "").isEmpty ? "" : self.getFormattedDateAndTime(dateString: subDetails.ends_at ?? "")
                                    }
                                   
                                    self.didShowAlert(title: "Your Membership Plan has been cancelled", message: "You will no longer be billed for DocOnline Membership Plus. However, you will continue to have access to our services until the end of your current billing cycle : \(endDate)")
                                }
                                self.tableView.reloadData()  ///changed 7th
                                self.checkSubscriptionDetails()  ///
                                print("Subscription###:=>\(App.didUserSubscribed)")
                            }
                        })
                    }
                }
            }
        }
    }
    
    /**
     method runs in call time duration timer
     */
    func runUserStateCheckTimer() {
        self.userSubscribedStatusCheckTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector (SubscriptionPlansViewController.updateUserStateCheck), userInfo: nil, repeats: true)
    }
    
    /**
     method updates in call time duration
     */
    @objc func updateUserStateCheck()
    {
        DispatchQueue.main.async {
            self.showStatusLine(color: UIColor.red, message: "Taking longer than usual.Please wait")
        }
    }
    
    
    /**
     Method shows status line message when view loads
     */
    func showStatusLine(color:UIColor,message:String) {
        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
        status.backgroundView.backgroundColor = color
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: message)
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.presentationContext = .window(windowLevel: .statusBar)
        SwiftMessages.show(config: statusConfig, view: status)
    }
    
    /**
     Method returns country name by id
     - Parameter countryID: pass the country id
     - Returns: country name
     */
    func getCountryNameById(countryID:Int) -> String{
        if countryID == 0 {
            print("no country selected")
            return ""
        }
        var countryToShow = ""
        
        if let countries = UserDefaults.standard.object(forKey: "ListCountry") as? NSDictionary {
            if let ids = countries.allKeys as? [String] {
                for id in ids {
                    if id == "\(countryID)" {
                        countryToShow = countries.object(forKey: id) as! String
                    }
                }
            }
        }else {
            print("Not stored list")
        }
        return countryToShow
    }
 
    // MARK: Razorpay delegate methods
    func onPaymentSuccess(_ payment_id: String) {
        print("paymentid::\(payment_id) ")
        let plan = self.plans[selectedPlan]
        
        if plan.display_amount != nil && plan.display_amount! != 0 {
            var amount = String(plan.display_amount!)
            amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
            let onlyAmount = amount.components(separatedBy: ".")
            let doubleAmount = Double(onlyAmount[0])
            if(App.ChatType == MessageServer.PRODUCTION)
            {
                //                    guard let tracker = GAI.sharedInstance().defaultTracker else {return}
                //                    let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
                //                        withCategory: "Plan \(onlyAmount[0]) Payment Success in iOS",
                //                        action: "Payment",
                //                        label: "Plan \(onlyAmount[0]) Payment Success in iOS for Payment",
                //                        value: nil).build()
                //                    tracker.send(eventTracker as! [AnyHashable: Any])
                
                var action = ""
//                if (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME)  {
//                    action = GATRackerActions.OneTimePaymentSuccess.rawValue
//                }else if (plan.type ?? "").lowercased().isEqual(PlanType.RECURRING){
//                    action = GATRackerActions.SubscriptionPaymentSuccess.rawValue
//                }
                
//                GATracker.shared().trackEvent(withCategory: GATRackerCategories.Payment.rawValue , action: action , label: plan.display_name ?? "ios", value: NSNumber(value: doubleAmount ?? 0) )
                
//                AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [
//                    AFEventParamContentId:"2",
//                    AFEventParamContentType : "SUBSCRIPTION",
//                    AFEventParamRevenue: onlyAmount[0],
//                    AFEventParamCurrency:"INR"
//                    ]);
            }
        }
        
        if self.planType == PlanType.ONE_TIME {
            self.paymentID = payment_id
            self.paymentGateWay = "razorpay"
//            razorpay = nil
            self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, displayTimeThreshold: 5, minimumDisplayTime: 5, backgroundColor: UIColor.black.withAlphaComponent(0.5), textColor: nil)
            self.runUserStateCheckTimer()
            self.getSuccessStatusAfterOneTimePaymentSuccess()
        }else if self.planType == PlanType.RECURRING {
            
            if App.isFromView == FromView.SubscriptionPlans || App.isFromView == FromView.SettingsView {
                self.paymentID = payment_id
                self.performSegue(withIdentifier:StoryboardSegueIDS.ID_PAYMENT_STATUS_SEGUE,sender:self)
            }else if App.isFromView == FromView.BookingView || App.isFromView == FromView.ChatHistoryView {
//                razorpay = nil
                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, displayTimeThreshold: 5, minimumDisplayTime: 5, backgroundColor: UIColor.black.withAlphaComponent(0.5), textColor: nil)
                self.runUserStateCheckTimer()
                self.getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom: 4)
            }
        }
    }
    
 // MARK :- Razorpay delegate methods
    func onPaymentError(_ code: Int32, description str: String) {
        print("Status code:\(code)  message:\(str)")
        
        let plan = self.plans[selectedPlan]
        if plan.display_amount != nil && plan.display_amount! != 0 {
            var amount = String(plan.display_amount!)
            amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
            let onlyAmount = amount.components(separatedBy: ".")
            let doubleAmount = Double(onlyAmount[0])
            if(App.ChatType == MessageServer.PRODUCTION)
            {
//                guard let tracker = GAI.sharedInstance().defaultTracker else {return}
//                let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
//                    withCategory: "Plan \(onlyAmount[0]) Payment Failure in iOS",
//                    action: "Payment",
//                    label: "Plan \(onlyAmount[0]) Payment Failure in iOS for Payment",
//                    value: nil).build()
//                tracker.send(eventTracker as! [AnyHashable: Any])
                var action = ""
//                if (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME)  {
//                    action = GATRackerActions.OneTimePaymentFailed.rawValue
//                }else if (plan.type ?? "").lowercased().isEqual(PlanType.RECURRING){
//                    action = GATRackerActions.SubscriptionPaymentFailed.rawValue
//                }
                
//                GATracker.shared().trackEvent(withCategory: GATRackerCategories.Payment.rawValue , action: action , label: plan.display_name ?? "ios", value: NSNumber(value: doubleAmount ?? 0) )
            
            }
        }
//
//        razorpay = nil
        let alert = UIAlertController(title: "Payment Failure", message: str, preferredStyle: UIAlertController.Style.alert)
        let oky = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(oky)
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelSubscription(completionHandler: @escaping (_ success:Bool) -> Void) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            completionHandler(false)
            return
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let procureURL = AppURLS.URL_CANCEL_SUBSCRIPTION
        var request = URLRequest(url: URL(string: procureURL)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.POST
        
        self.startAnimating()
        session.dataTask(with: request) { (data, response, error) in
            if let error = error
            {
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                     completionHandler(false)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                _ =  self.check_Status_Code(statusCode: httpResponse.statusCode , data: nil)
                if httpResponse.statusCode == 204 {
                    print("Subscription cancelled")
                    
                    self.getUserState(completion: { (succces,newuser) in
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.checkSubscriptionDetails()
                            completionHandler(true)
                        }
                    })
                    
                    //  self.getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom: 1)
                }else if httpResponse.statusCode == 422 {
                    do
                    {
                        if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            if let message = resultJSON.object(forKey: Keys.KEY_MESSAGE) as? String {
                                DispatchQueue.main.async {
                                    self.didShowAlert(title: "", message: message)
                                     completionHandler(false)
                                }
                            }
                        }
                    }catch let error{
                        print("Error:\(error.localizedDescription)")
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                         completionHandler(false)
                    }
                }
            }
            }.resume()
    }
    
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        print("walletName : ",walletName,"paymentData : ",paymentData.debugDescription)
    
        var emailID = ""
        var mobileNo = ""
        var user_id = ""
        var planAmount = ""
        if let user = App.userDetails {
            if let uEmail = user.email {
                emailID = uEmail
            }
            
            if let uid = user.user_id {
                user_id = "\(uid)"
            }
            
            if let uPhone = user.phone {
                mobileNo = uPhone
            }
        }
        
        let plan = self.plans[selectedPlan]
        if plan.display_amount != nil && plan.display_amount! != 0 {
            var amount = String(plan.display_amount!)
            amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
            let onlyAmount = amount.components(separatedBy: ".")
            planAmount = onlyAmount[0]
        }
        
        mobileNo = paymentData?["contact"] as? String ?? "8822127127"
        emailID = paymentData?["email"] as? String ?? "appaccount@doconline.com"
        print("mobileNo : ",mobileNo,"emailID : ",emailID)
        
//        let merchantConfig = PGMerchantConfiguration.default();
//        merchantConfig?.checksumGenerationURL = AppURLS.URL_PAYTM_CHECKSUM
//        //merchantConfig?.checksumValidationURL = "https://c0ae37f4.ngrok.io/PaytmChecksum/verifyChecksum.php"
//        merchantConfig?.merchantID = PaytmCredentials.liveMerchantID
//        merchantConfig?.website = PaytmCredentials.liveWebsite
//        merchantConfig?.industryID = PaytmCredentials.liveIndustryID
//        merchantConfig?.channelID = PaytmCredentials.channelID
//
        let strOrderID = self.oneTimePaymentOrderID//"DocOn\(orderID)"
        let httpBody = "\(Keys.KEY_PAYTM_CALLBACK_URL)=\(PaytmCredentials.callBackUrl)\(strOrderID)&\(Keys.KEY_PAYTM_CHANNEL_ID)=\(PaytmCredentials.channelID)&\(Keys.KEY_PAYTM_CUST_ID)=\(user_id)&\(Keys.KEY_PAYTM_INDUSTRY_TYPE_ID)=\(PaytmCredentials.liveIndustryID)&\(Keys.KEY_PAYTM_MID)=\(PaytmCredentials.liveMerchantID)&\(Keys.KEY_PAYTM_ORDER_ID)=\(strOrderID)&\(Keys.KEY_PAYTM_TXN_AMOUNT)=\(planAmount)&\(Keys.KEY_PAYTM_WEBSITE)=\(PaytmCredentials.liveWebsite)&\(Keys.KEY_PAYTM_EMAIL)=\(emailID)&\(Keys.KEY_PAYTM_MOBILE_NO)=\(mobileNo)"
        print("httpBody :",httpBody)
        self.getCheckSum(AppURLS.URL_PAYTM_CHECKSUM, httpBody, completionHandler :  { (success) -> Void in
            if success {
                
                DispatchQueue.main.async(execute: {
                    
                    print("CHECKSUMHASH :",self.strCheckSum)
                    let odrDict : Dictionary<AnyHashable,Any> = [
                        Keys.KEY_PAYTM_CALLBACK_URL :  "\(PaytmCredentials.callBackUrl)\(strOrderID)",
                        Keys.KEY_PAYTM_CHANNEL_ID : PaytmCredentials.channelID,
                        Keys.KEY_PAYTM_CHECKSUMHASH : self.strCheckSum,
                        Keys.KEY_PAYTM_CUST_ID : user_id,
                        Keys.KEY_PAYTM_INDUSTRY_TYPE_ID :  PaytmCredentials.liveIndustryID,
                        Keys.KEY_PAYTM_MID : PaytmCredentials.liveMerchantID,
                        Keys.KEY_PAYTM_ORDER_ID : strOrderID,
                        Keys.KEY_PAYTM_TXN_AMOUNT : planAmount,
                        Keys.KEY_PAYTM_WEBSITE : PaytmCredentials.liveWebsite,
                        Keys.KEY_PAYTM_EMAIL : emailID,
                        Keys.KEY_PAYTM_MOBILE_NO : mobileNo
                    ]
                    print("odrDict :",odrDict.debugDescription)
//                    let order: PGOrder = PGOrder(params: odrDict)
                    
                    /*PGServerEnvironment.selectServerDialog(self.view) { (type) in
                        if (type == eServerTypeStaging) {
                            let transactionController = PGTransactionViewController.init(transactionFor: order)
                            transactionController? .serverType = eServerTypeStaging
                            transactionController? .merchant = merchantConfig
                            //transactionController?.loggingEnabled = true
                            transactionController? .delegate = self
                            //transactionController?.useStaging = true
                            self.navigationController?.pushViewController(transactionController!, animated: true)
                        }
                        else if (type == eServerTypeProduction)
                        {
                            let transactionController = PGTransactionViewController.init(transactionFor: order)
                            transactionController? .serverType = eServerTypeProduction
                            transactionController? .merchant = merchantConfig
                            transactionController? .delegate = self
                            self.navigationController?.pushViewController(transactionController!, animated: true)
                        }
                        else
                        {
                            print("None")
                        }
                    }*/
                    
//                    let transactionController = PGTransactionViewController.init(transactionFor: order)
//                    transactionController? .serverType = eServerTypeProduction
//                    transactionController? .merchant = merchantConfig
//                    transactionController? .delegate = self
//                    self.navigationController?.pushViewController(transactionController!, animated: true)
                })
                
            }else {
            }
        })
        
    }
    // MARK: Paytm Checksum generator API
    func getCheckSum(_ actionURL:String,_ dataToPost:String,completionHandler : @escaping (_ success:Bool) -> Void ) {
        
        let config = URLSessionConfiguration.default
        let mySession = URLSession(configuration: config)
        let url = URL(string: actionURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
        //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
        request.httpMethod = HTTPMethods.POST
        let postData = dataToPost
        print("Data==>\(postData)")
        print("URL==>\(actionURL)")
        request.httpBody = dataToPost.data(using: String.Encoding.utf8)
        let task = mySession.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    ///making complition handler false
                    completionHandler(false)
                    ///Stop spinning animation
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                    completionHandler(false)
                    if error.localizedDescription == "The request timed out."
                    {
                        self.didShowAlert(title: "Internet failure", message: "Internet connection appears to be offline")
                        
                    }
                    else
                    {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        
                    }
                })
            }
            
            if let data = data
            {
                print("data =\(data)")
            }
            
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                
                //if you response is json do the following
                do{
                    if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                        self.strCheckSum = resultJSON.object(forKey: "CHECKSUMHASH") as! String
                        print("resultJSON :",resultJSON)
                        print("CHECKSUMHASH :",resultJSON.object(forKey: "CHECKSUMHASH") as! String)
                        completionHandler(true)
                        //let dict : NSDictionary = resultJSON.object(at: 0) as! NSDictionary
                        
                        
                    }
                }catch let error {
                    completionHandler(false)
                    print("Received not-well-formatted JSON:=>\(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
    
    
    @IBAction func cancelSubscriptionTapped(_ sender: UIButton) {
        
       let alert = UIAlertController(title: "Are you sure?", message: "Do you want to cancel Membership", preferredStyle: UIAlertController.Style.alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (action) in
            self.cancelSubscription(completionHandler: { (success) in
                DispatchQueue.main.async {
                    self.stopAnimating()
                    var endDate = ""
                    if let subDetails = App.subscription_details {
                        endDate = (subDetails.ends_at ?? "").isEmpty ? "" : self.getFormattedDateAndTime(dateString: subDetails.ends_at ?? "")
                    }
                    self.didShowAlert(title: "Your Membership Plan has been cancelled", message: "You will no longer be billed for DocOnline Membership Plus. However, you will continue to have access to our services until the end of your current billing cycle : \(endDate)")
                    self.tableView.reloadData()  ///changed 7th
                    self.checkSubscriptionDetails()
                }
            })
        }
        let cancel = UIAlertAction(title: "cancel", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(cancel)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func applyCouponCodeTapped(_ sender: UIButton) {
        let couponCode = self.tf_coupon.text! as String
         self.tf_coupon.resignFirstResponder()
        if couponCode.isEmpty {
            self.didShowAlert(title: "", message: "Coupon Code required!")
        }else {
            let couponData = [Keys.KEY_COUPON_CODE : couponCode]
            if let jsonData = try? JSONSerialization.data(withJSONObject: couponData, options: JSONSerialization.WritingOptions.prettyPrinted) {
                startAnimating()
                NetworkCall.performPOST(url: AppURLS.URL_PROMO_CODE, data: jsonData, completionHandler: { (success, response, statuscode, error) in
                    
                    if let statusCode = statuscode , let resp = response {
                        _ =  self.check_Status_Code(statusCode: statusCode , data: resp)
                        
                        if statusCode == 200 {
                            DispatchQueue.main.async {
                                if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary,let message = data.object(forKey: Keys.KEY_MESSAGE) as? String {
                                    
                                    print("message : ",message)
                                    self.openRazorPayInCouponCode(data: data)
                                }
                                //self.tf_coupon.text = ""
                                self.scrollView.setContentOffset(CGPoint.zero, animated: true)
                            }
                            self.getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom: 5)
                        }else if statusCode == 422 {
                            if let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String {
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    self.didShowAlert(title: "", message: message)
                                }
                            }
                        }
                        
                    }
                    
                    if let err = error {
                        print("Error performing promo code:\(err.localizedDescription)")
                    }
                    
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                })
            }
        }
    }
    
    func openRazorPayInCouponCode(data:NSDictionary){
        guard let orderID = data.object(forKey: Keys.KEY_ORDER_ID_U) as? String,let hasPrice = data.object(forKey: Keys.KEY_HAS_PRICE) as? Bool else{
            if let message = data.object(forKey: Keys.KEY_MESSAGE) as? String{
                self.didShowAlert(title: "", message: message)
            }
            return
        }
        print("hasPrice : ",hasPrice)
        var email = ""
        var phone = ""
        var address = ""
        var user_id = ""
        
        if let user = App.userDetails {
            if let uEmail = user.email {
                email = uEmail
            }
            
            if let uid = user.user_id {
                user_id = "\(uid)"
            }
            
            if let uPhone = user.phone {
                phone = uPhone
            }
            
            if let uadd1 = user.address1 , let uadd2 = user.address2 , let ucity = user.city ,let ustate = user.state ,let upincode = user.pincode , let ucountry = user.country {
                //  let countryName = self.getCountryNameById(countryID:ucountry)
                address = "\(uadd1)\(uadd2.isEmpty ? "" : ",\(uadd2)"),\(ucity),\(ustate),\(upincode),\(ucountry)"
                address = uadd1
            }
        }
        
        
        let options : Dictionary<AnyHashable,Any> = [
            "key": self.rz_key,
            "name": "DocOnline",
            "order_id" :  "\(orderID)",
            "image" : "https://app.doconline.com/assets/images/logo.png" ,
            "description": "Coupon Code",
            //"amount" : plan.display_amount ?? 15000 ,
            "external" : ["wallets" : ["paytm"]],
            "prefill" : [
                "email" : email,
                "contact": phone
            ],
            "notes": [
                "doconline_user_id" : user_id,
                "platform": "ios",
                "address": address,
                "action_type" : "fresh",
                "order_id" :  "\(orderID)"
            ]
        ]

        
        self.oneTimePaymentOrderID = orderID
        self.paymentType = SubscriptionType.OneTime
        self.planType = PlanType.ONE_TIME
        self.paymentPlan = "Coupon Code"
        
        DispatchQueue.main.async(execute: {
            print("Razorpay options:\(options)")
            self.stopAnimating()
            self.showPaymentForm(options:options )
        })
        
    }
//
//    // MARK: Paytm Delegate methods.
//    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
//        print("responseString : ",responseString)
//        if(responseString == "")
//        {
//
//        }
//        else
//        {
//            let jsonData : NSMutableDictionary = (responseString as String).parseJSONString as! NSMutableDictionary
//            print("jsonData : ",jsonData)
//            _ = navigationController?.popViewController(animated: true)
//            let plan = self.plans[selectedPlan]
//            if self.planType == PlanType.ONE_TIME {
//                let status = jsonData.value(forKey: "STATUS") as! String
//                if(status == "TXN_FAILURE")
//                {
//                    self.paymentID = ""
//                }
//                else if(status == "TXN_SUCCESS")
//                {
//                    self.paymentID = jsonData.value(forKey: "TXNID") as! String
//                }
//                self.paymentGateWay = "paytm"
//                print("self.paymentID::\(self.paymentID) ")
//                razorpay = nil
//                self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, displayTimeThreshold: 5, minimumDisplayTime: 5, backgroundColor: UIColor.black.withAlphaComponent(0.5), textColor: nil)
//                self.runUserStateCheckTimer()
//                self.getSuccessStatusAfterOneTimePaymentSuccess()
//            }else if self.planType == PlanType.RECURRING {
//
//                if plan.display_amount != nil && plan.display_amount! != 0 {
//                    var amount = String(plan.display_amount!)
//                    amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
//                    let onlyAmount = amount.components(separatedBy: ".")
////                    if(App.ChatType == MessageServer.PRODUCTION)
////                    {
////                        guard let tracker = GAI.sharedInstance().defaultTracker else {return}
////                        let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
////                            withCategory: "Plan \(onlyAmount[0]) Payment Success in iOS",
////                            action: "Payment",
////                            label: "Plan \(onlyAmount[0]) Payment Success in iOS for Payment",
////                            value: nil).build()
////                        tracker.send(eventTracker as? [AnyHashable: Any])
////                    }
//                }
//
//                if App.isFromView == FromView.SubscriptionPlans || App.isFromView == FromView.SettingsView {
//                    self.paymentID = jsonData.value(forKey: "ORDERID") as! String
//                    print("self.paymentID::\(self.paymentID) ")
//                    self.performSegue(withIdentifier:StoryboardSegueIDS.ID_PAYMENT_STATUS_SEGUE,sender:self)
//                }else if App.isFromView == FromView.BookingView || App.isFromView == FromView.ChatHistoryView {
//                    razorpay = nil
//                    self.startAnimating(CGSize(width:100,height:120), message: "Processing", messageFont: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.white, padding: 20, displayTimeThreshold: 5, minimumDisplayTime: 5, backgroundColor: UIColor.black.withAlphaComponent(0.5), textColor: nil)
//                    self.runUserStateCheckTimer()
//                    self.getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom: 4)
//                }
//            }
//        }
//
//    }
//
//    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
//        _ = navigationController?.popViewController(animated: true)
//    }
//
//    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
//        print("error :",error.localizedDescription.debugDescription)
//        print("controller :",controller.merchant.merchantID)
//        _ = navigationController?.popViewController(animated: true)
//    }
    
    
    // MARK: - One time payment coupon code apply
    
    func openCouponViewForOTP(show:Bool) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
              self.lc_couponOTPTopConstraint.constant =  show ? 0 : 1800
             self.view.layoutIfNeeded()
        }, completion: { (success) in
        })
    }
    
    func applyDicountCouponCodeOTP(coupon:String) {
        let url = AppURLS.APPLY_DISCOUNT_ONE_TIME_PAYMENT
        var couponData: [String:Any] = [Keys.KEY_DISCOUNT_CODE : coupon,Keys.KEY_PLAN_TYPE:""]
        if let selectedPlan = self.choosenPlan {
            couponData[Keys.KEY_PLAN_TYPE] = selectedPlan.planType ?? ""
        }
        print("Dataapply coupon:\(couponData)")
        self.startAnimating()
        if let jsonData = try? JSONSerialization.data(withJSONObject: couponData, options: JSONSerialization.WritingOptions.prettyPrinted) {
            NetworkCall.performPOST(url: url, data: jsonData, completionHandler: { (success, response, statusCode, error) in
                if let statusCode = statusCode , let resp = response {
                    _ =  self.check_Status_Code(statusCode: statusCode , data: resp)
                    
                    if statusCode == 200 {
                        DispatchQueue.main.async {
                            if let data = resp.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                var ddiscount = 0.0
                                var damount = 0.0
                                var dplanAmount = 0.0
                                var dcode = coupon
                                
                                if let discount = data.object(forKey: Keys.KEY_DISCOUNT) as? Int {
                                    ddiscount = Double(discount)
                                }else {
                                    if let discount = data.object(forKey: Keys.KEY_DISCOUNT) as? String {
                                         ddiscount = Double(discount) ?? 0
                                    }
                                }
                                
                                if let amount = data.object(forKey: Keys.KEY_AMOUNT) as? Double{
                                    damount = amount
                                }else {
                                    if let amount = data.object(forKey: Keys.KEY_AMOUNT) as? String {
                                        damount = Double(amount) ?? 0
                                    }
                                }
                            
                                if let plamAmount = data.object(forKey: Keys.KEY_PLAN_AMOUNT) as? Int {
                                    dplanAmount = Double(plamAmount)
                                }else {
                                    if let plamAmount = data.object(forKey: Keys.KEY_PLAN_AMOUNT) as? String {
                                        dplanAmount = Double(plamAmount) ?? 0
                                    }
                                }
                               
                                dcode = (data.object(forKey: Keys.KEY_CODE) as? String ?? coupon)
                                
                                self.lb_planAmount_OTP.text = String(format: "₹%.2f", dplanAmount)
                                self.lb_discountedAmount_OTP.text = String(format: "₹%.2f", ddiscount)
                                self.lb_toBePaid_OTP.text = String(format: "₹%.2f", damount)
                                self.lb_couponSuccessOrFailureMsg_OTP.text = "Coupon Applied Successfully"
                                self.lb_couponSuccessOrFailureMsg_OTP.isHidden = false
                                self.lb_couponSuccessOrFailureMsg_OTP.textColor = self.hexStringToUIColor(hex: StandardColorCodes.GREEN)
                                self.bt_applyCouponOTP.setTitle("Remove", for: .normal)
                                self.isDiscountAppliedForOTP = true
                                self.discountedAmountOnOTP = "\(ddiscount)00"
                                self.appliedDiscountCouponCode = dcode
                            }
                            self.stopAnimating()
                        }
                    }else if statusCode == 422 {
                        if let message = resp.object(forKey: Keys.KEY_MESSAGE) as? String {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.isDiscountAppliedForOTP = false
                                self.lb_couponSuccessOrFailureMsg_OTP.isHidden = false
                                self.lb_couponSuccessOrFailureMsg_OTP.textColor = UIColor.red
                                self.lb_couponSuccessOrFailureMsg_OTP.text = message
                                self.appliedDiscountCouponCode = ""
                            }
                        }
                    }
                }
                
                if let err = error {
                    self.isDiscountAppliedForOTP = false
                    self.appliedDiscountCouponCode = ""
                    print("Error performing promo code:\(err.localizedDescription)")
                }
                
                DispatchQueue.main.async {
                    self.isDiscountAppliedForOTP = false
                    self.stopAnimating()
                }
            })
        }
    }
    
    @IBAction func applyCouponForOTP(_ sender: UIButton) {
        let discountCoupon = self.tf_couponCodeOTP.text!
        self.tf_couponCodeOTP.resignFirstResponder()
        if discountCoupon.isEmpty {
            self.lb_couponSuccessOrFailureMsg_OTP.isHidden = false
            self.lb_couponSuccessOrFailureMsg_OTP.text = "Coupon code required!"
            self.lb_couponSuccessOrFailureMsg_OTP.textColor = UIColor.red
            return
        }
        
        if (sender.currentTitle ?? "").lowercased().isEqual("apply") {
            self.applyDicountCouponCodeOTP(coupon: discountCoupon)
        }else {
            if let selectedPlan = self.choosenPlan {
                if (selectedPlan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) {
                    var planAmount = String(selectedPlan.display_amount ?? 00)
                    planAmount.insert(contentsOf: ".", at: planAmount.index(planAmount.endIndex, offsetBy: -2))
                    self.lb_planAmount_OTP.text = "₹\(planAmount)"
                    self.lb_discountedAmount_OTP.text = "₹0.00"
                    self.lb_toBePaid_OTP.text = "₹\(planAmount)"
                    self.bt_applyCouponOTP.setTitle("Apply", for: .normal)
                    self.tf_couponCodeOTP.text = ""
                    self.appliedDiscountCouponCode = ""
                    self.lb_couponSuccessOrFailureMsg_OTP.text = ""
                    self.lb_couponSuccessOrFailureMsg_OTP.isHidden = true
                }
            }
        }
    }
    
    @IBAction func cancelOrProceedToPayOTP(_ sender: UIButton) {
        if sender.tag == 1 {
            self.tf_couponCodeOTP.text = ""
            self.appliedDiscountCouponCode = ""
            self.openCouponViewForOTP(show: false)
        }else {
            if let choosenPlan = self.choosenPlan , (choosenPlan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME){
                self.getOrderIdForOneTimePayment(plan: choosenPlan, coupon:  self.appliedDiscountCouponCode)
            }
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_PAYMENT_STATUS_SEGUE {
            if let destVC = segue.destination as? PaymentStatusViewController {
                destVC.isPaymentSuccessful = self.paymentID.isEmpty ? false : true
                destVC.payment_id = self.paymentID
                destVC.paymentPlanName = self.paymentPlan
                destVC.paymentType = self.paymentType
                destVC.order_id = self.oneTimePaymentOrderID
                destVC.delegate = self
            }
        }else if segue.identifier == StoryboardSegueIDS.ID_VERIFY_EMAIL {
            let destVC = segue.destination as! EmailVerificationViewController
            destVC.delegate = self
        }
    }
    
}

extension SubscriptionPlansViewController : EmailVerificationDelegate {
    func didVerifyEmail(_ email: String, _ success: Bool) {
        if success {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension SubscriptionPlansViewController : PaymentSuccessDelegate {
    func didTapeDone(success: Bool) {
        if success {
            self.dismiss(animated: true, completion: {
                self.getSubscriptionDetailsAfterPaymentSuccess(isCalledFrom: 2)
            })
        }
    }
}

extension SubscriptionPlansViewController : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.plans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {//Onetime
        let plan = self.plans[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) ? "Onetime" : "Subscription", for: indexPath) as! SubscriptionPlansCollectionViewCell

      //  print("Cell plan type:\(plan.planType)")
        if (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) || (plan.period ?? "").isEmpty {
            cell.lb_amountPerMonth.isHidden = true
        }else {
            cell.lb_amountPerMonth.isHidden = false
            cell.lb_amountPerMonth.text = plan.display_period == nil ? "" : "/\(plan.display_period ?? "")"
        }
        
        
        if plan.display_amount != nil && plan.display_amount! != 0 {
            var amount = String(plan.display_amount!)
            amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
            let onlyAmount = amount.components(separatedBy: ".")
            cell.lb_plan_amount.text = onlyAmount[0]
            if cell.iv_limitedPeriodOfferRibbon != nil {
                cell.iv_limitedPeriodOfferRibbon.isHidden = true
            }
            
            if (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) && (plan.featured ?? false)
            {
                if plan.cross_price != nil && plan.cross_price! != 0 {
                    var amount = String(plan.cross_price!)
                    amount.insert(contentsOf: ".", at: amount.index(amount.endIndex, offsetBy: -2))
                    let crossAmount = amount.components(separatedBy: ".")
                    let strPrice = "\(onlyAmount[0])  ₹\(crossAmount[0])"
                    let myMutableString = self.setAttributeString(strPrice, strLength: onlyAmount[0].count, fontSize1: 30, fontSize2: 18, isToStrike: true, strikeLocation: onlyAmount[0].count+2, strikeLength: crossAmount[0].count + 1)
                    cell.lb_plan_amount.attributedText = myMutableString
                    cell.lb_plan_amount.numberOfLines = 0
                    cell.lb_plan_amount.sizeToFit()
                    if cell.iv_limitedPeriodOfferRibbon != nil {
                        cell.iv_limitedPeriodOfferRibbon.isHidden = false
                    }
                }
            }
        }

        if plan.display_period != nil {
            cell.lb_per_month.text = plan.display_period!
        }
        
        
        cell.lb_plan_period.text = plan.display_name != nil ? plan.display_name!.uppercased() : ""
        
         let url = URL(string: BaseUrl + plan.iconURL)
         cell.iv_subscriptionIcon.kf.setImage(with: url)
        print("Icons urls:\(BaseUrl + plan.iconURL)")
        
        cell.bt_subscription.tag = indexPath.row
        cell.bt_upgrade_now.tag = indexPath.row
      //  buttonCornerRadius(buttons: [cell.bt_subscription,cell.bt_upgrade_now])
        cell.tv_description.text = plan.packages!
        
        cell.bt_subscription.addTarget(self, action: #selector(self.subscribeNow(_:)), for: UIControl.Event.touchUpInside)
        cell.bt_upgrade_now.addTarget(self, action: #selector(self.upgradeSubscription(_:)), for: .touchUpInside)
        
//        cell.vw_back.layer.cornerRadius = 15
//        cell.vw_back.clipsToBounds = true
        
        cell.lb_subscribed_status.isHidden = true

        cell.bt_subscription.isHidden = App.didUserSubscribed ? true : false
        cell.bt_subscription.setTitle("Choose", for: .normal)

        
        if App.didUserSubscribed && App.canUpgradeSubscription && App.userSubscriptionType.lowercased().isEqual("b2c"){
            if let order = plan.internal_order {
                if order > self.activePlanOrderCount {
                    cell.bt_upgrade_now.isHidden = false
                }else {
                    cell.bt_upgrade_now.isHidden = true
                }
            }
        }else {
            cell.bt_upgrade_now.isHidden = true
        }
        
//        if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.ONE_TIME) && (plan.planType ?? "").isEqual(PlanType.RECURRING) {
//            cell.bt_subscription.isHidden = false
//        }
        
       
        if let subDetails = App.subscription_details ,
            !App.userSubscriptionType.lowercased().isEqual(PlanType.FAMILY_PACK) ,               !App.userSubscriptionType.lowercased().isEqual(PlanType.CORPARATE_PACKAGE) {
          //  print("Subs:\(subDetails.plan_id!)  curent:\(plan.plan_id!)")
            if subDetails.plan_id != nil && subDetails.plan_id == plan.plan_id! {
                cell.lb_activePlan.isHidden = false
            }else {
                cell.lb_activePlan.isHidden = true
            }
            
            /* HALTED STATE CODE : START */
            if (subDetails.status ?? "").isEqual(PlanType.PaymentStatus.HALTED) && (plan.type ?? "").lowercased().isEqual(PlanType.RECURRING) && App.userSubscriptionType.lowercased().isEqual(PlanType.B2C) {
                cell.lb_activePlan.textColor = UIColor.red
                cell.lb_activePlan.text = "SERVICES HALTED"
                cell.bt_upgrade_now.isHidden = false
                cell.bt_upgrade_now.setTitle("Retry Payment", for: .normal)
            }else {
                cell.bt_upgrade_now.isHidden = true
                cell.bt_upgrade_now.setTitle("UPGRADE", for: .normal)
                cell.lb_activePlan.text = "ACTIVE"
                cell.lb_activePlan.textColor = self.hexStringToUIColor(hex: StandardColorCodes.GREEN)
            }
            /* HALTED STATE CODE : END */
        }else {
            if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.ONE_TIME) &&  (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) && (plan.planType ?? "").lowercased().isEqual(PlanType.ONE_TIME) || App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.B2BPAID) &&  (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) && (plan.planType ?? "").lowercased().isEqual(PlanType.ONE_TIME)  {
                cell.lb_activePlan.isHidden = false
            }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.FAMILY_PACK) &&  App.b2bUserType.lowercased().isEqual(PlanType.B2C) && (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) && (plan.planType ?? "").lowercased().isEqual(PlanType.FAMILY_PACK){
                cell.lb_activePlan.isHidden = false
            }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual(PlanType.CORPARATE_PACKAGE) &&  App.b2bUserType.lowercased().isEqual(PlanType.B2BPAID) && (plan.type ?? "").lowercased().isEqual(PlanType.ONE_TIME) && (plan.planType ?? "").lowercased().isEqual(PlanType.CORPARATE_PACKAGE){
                cell.lb_activePlan.isHidden = false
            }else if App.didUserSubscribed && App.userSubscriptionType.lowercased().isEqual("family") &&  (plan.plan_id ?? "") == App.subscribedAndActivePlanId ,!App.subscribedAndActivePlanId.isEmpty {
                cell.lb_activePlan.isHidden = false
            }else {
                cell.lb_activePlan.isHidden = true
            }
        }
        
//        let featured = plan.featured == nil ? false : plan.featured!
//        if featured {
//            cell.lb_recommended_status.isHidden = false
//        //  cell.lb_plan_period.backgroundColor = UIColor.init(hexString: "#7eb668")
//        }else {
//            cell.lb_recommended_status.isHidden = true
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 218
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.LC_tableView_height.constant = self.tableView.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


extension SubscriptionPlansViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5) {
          //  self.lb_swipe_left.isHidden = true
        }
    }
}

//
//@IBDesignable
//class CardView: UIView {
//
//    @IBInspectable var cornRadius: CGFloat = 3
//
//    @IBInspectable var shadowOffsetWidth: Int = 0
//    @IBInspectable var shadowOffsetHeight: Int = 0
//    @IBInspectable var shadowColor: UIColor? = UIColor.black.withAlphaComponent(0.2)
//    @IBInspectable var shadowOpacity: Float = 0.8
//
//    override func layoutSubviews() {
//        layer.cornerRadius = cornRadius
//        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornRadius)
//
//        layer.masksToBounds = false
//        layer.shadowColor = shadowColor?.cgColor
//        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
//        layer.shadowOpacity = shadowOpacity
//        layer.shadowPath = shadowPath.cgPath
//    }
//
//}

//extension String
//{
//    var parseJSONString: AnyObject?
//    {
//        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
//        if let jsonData = data
//        {
//            // Will return an object or nil if JSON decoding fails
//            do
//            {
//                let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
//                if let jsonResult = message as? NSMutableArray {
//                    return jsonResult //Will return the json array output
//                } else if let jsonResult = message as? NSMutableDictionary {
//                    return jsonResult //Will return the json dictionary output
//                } else {
//                    return nil
//                }
//            }
//            catch let error as NSError
//            {
//                print("An error occurred: \(error)")
//                return nil
//            }
//        }
//        else
//        {
//            // Lossless conversion of the string was not possible
//            return nil
//        }
//    }
//}




