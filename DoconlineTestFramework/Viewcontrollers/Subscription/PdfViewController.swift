//
//  PdfViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 08/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
import WebKit

class PdfViewController: UIViewController {

   
    
    var webView: WKWebView!
    
    ///google doc view base url
    var googleDocViewURL = "http://docs.google.com/gview?embedded=true&url="
    
    var pdfURl = ""
    var invoice_id = ""
    
    ///linear bar instance reference
    private let linearBar: LinearProgressBar = LinearProgressBar()
    
    /**
     linear bar configuration
     */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        //linearBar.heightForLinearBar = 2
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.linearBar.startAnimation()
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        
        self.webView = WKWebView(frame: self.view.bounds,configuration:config)
        self.webView.clipsToBounds = true
        self.view.addSubview(self.webView)
        webView.navigationDelegate = self
        configureLinearProgressBar()
        googleDocView()
    }
    
    

    func googleDocView() {
       // let urlStrng = self.googleDocViewURL + pdfURl
     //   print("Doc url : \(urlStrng)")
        let request = URLRequest(url: URL(string: pdfURl)!)
        webView.load(request)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.linearBar.stopAnimation()
    }

    @IBAction func shareTapped(_ sender: UIBarButtonItem) {
       loadPDFAndShare()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension PdfViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.linearBar.stopAnimation()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.didShowAlert(title: "", message: "\(error.localizedDescription)")

    }
    
    func savePdf(){
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let documentName = paths + "\(invoice_id).pdf"
        let pdfDoc = NSData(contentsOf:URL(string: pdfURl )!)
        fileManager.createFile(atPath: documentName as String, contents: pdfDoc as Data?, attributes: nil)
    }
    
    func loadPDFAndShare(){
        
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let documentoPath = paths + "\(invoice_id).pdf"
        
        if fileManager.fileExists(atPath: documentoPath){
            let documento = NSData(contentsOfFile: documentoPath)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            present(activityViewController, animated: true, completion: nil)
        }
        else {
            print("document was not found")
        }
    }
}





