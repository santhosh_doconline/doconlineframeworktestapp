//
//  PaymentStatusViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 01/12/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit

protocol PaymentSuccessDelegate  {
    func didTapeDone(success:Bool)
}

enum SubscriptionType : String{
    case Upgrade
    case Subscription
    case OneTime
}

class PaymentStatusViewController: UIViewController {

    @IBOutlet weak var iv_icon: UIImageView!
    @IBOutlet weak var lb_payment_status: UILabel!
    
    @IBOutlet weak var lb_payment_summary: UILabel!
    @IBOutlet weak var bt_go_back: UIButton!
    
    @IBOutlet var bt_book_appointment: UIButton!
    @IBOutlet var bt_goto_billings: UIButton!
    
    
    let PAYMENT_SCUCCESS_TITLE = "Congratulations!"
    let PAYMENT_FAILED_TITLE = "Payment Failed"
    var PAYMENT_SUCCESS_STATUS = "We have received your payment for (Membership ID: "
    let PAYMENT_FAILED_STATUS = "Failed to process payment."
    
    let UPGRADE_PAYMENT_SUCCESS_STATUS = "We have received your request for upgradation of your membership to (Membership ID:).It will be effective as soon as current billing cycle ends."
    
    ///Boolean to check the payment success or not
    var isPaymentSuccessful = false
    ///payment id to display
    var payment_id = ""
    ///one time payment order od
    var order_id = ""
    ///payment plan name to display
    var paymentPlanName = ""
    
    var delegate : PaymentSuccessDelegate?
    
    ///used to check the subscription of new one or upgradation
    var paymentType = SubscriptionType.Subscription
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if paymentType == SubscriptionType.Subscription {
            self.PAYMENT_SUCCESS_STATUS = "We have received your payment for \(paymentPlanName.lowercased()) (Membership ID: \(payment_id))"
        }else if paymentType == SubscriptionType.Upgrade {
             self.PAYMENT_SUCCESS_STATUS = "We have received your request for upgradation of your membership to \(paymentPlanName.lowercased()) (Membership ID: \(payment_id)).It will be effective as soon as current billing cycle ends."
        }else if paymentType == SubscriptionType.OneTime {
             self.PAYMENT_SUCCESS_STATUS = "We have received your payment for \(paymentPlanName.lowercased()) (Order ID: \(order_id))."
        }

        if isPaymentSuccessful {
            self.iv_icon.image = UIImage(named: "PaymentSuccess")
            self.lb_payment_status.text = PAYMENT_SCUCCESS_TITLE
            self.lb_payment_summary.text = self.PAYMENT_SUCCESS_STATUS
            self.bt_go_back.setTitle("Done", for: UIControl.State.normal)
            self.bt_go_back.tag = 1
        }else {
            self.iv_icon.image = UIImage(named: "PaymentFailed")
            self.lb_payment_summary.text = self.PAYMENT_SUCCESS_STATUS
            self.bt_go_back.setTitle("Try Again", for: UIControl.State.normal)
            self.bt_go_back.tag = 2
        }
        
        self.bt_book_appointment.layer.cornerRadius = self.bt_book_appointment.frame.size.height / 2
        self.bt_goto_billings.layer.cornerRadius = self.bt_goto_billings.frame.size.height / 2
        self.bt_book_appointment.clipsToBounds = true
        self.bt_goto_billings.clipsToBounds = true
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getUserState { (success,newuser) in
            print("Finish fetching userstate : \(success)")
        }
    }
    
    @IBAction func bookAppointmentOrGotoBillingsTapped(_ sender: UIButton) {
        App.isFromPaymentSuccessPage = true
        App.goToViewFromPayment = sender.tag
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let homeView = storyBoard.instantiateViewController(withIdentifier: StoryBoardIds.ID_HOME_NAVIGATION)
        self.present(homeView, animated: true, completion: nil)
    }
    
    
   
    @IBAction func doneTapped(_ sender: UIButton) {
        delegate?.didTapeDone(success: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
