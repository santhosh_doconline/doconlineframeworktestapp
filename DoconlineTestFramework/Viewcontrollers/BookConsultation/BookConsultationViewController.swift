//
//  BookConsultationViewController.swift
//  DocOnline
//
//  Created by dev-3 on 08/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
import Photos
//import NVActivityIndicatorView
//import BSImagePicker
import SwiftMessages
//import BIZPopupView
//import Firebase
import MobileCoreServices


class BookConsultationViewController: UIViewController   ,NVActivityIndicatorViewable ,LanguageSelectionDelegate ,UIPickerViewDelegate,UIPickerViewDataSource
{
    //newly added
    var selectedImageIndex = 0
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    ///linear bar instance reference
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    ///New UI
    @IBOutlet var dropDownShadowView: CardView!
    @IBOutlet var bt_internetCheckMark: UIButton!
    @IBOutlet var bt_telephoneCheckMark: UIButton!
    @IBOutlet var LC_dropDownViewHeight: NSLayoutConstraint!
    @IBOutlet var lb_addNotes: UILabel!
    @IBOutlet var LC_preferredLanguageViewHeight: NSLayoutConstraint! //default 26
    @IBOutlet var lb_preferredLang: UILabel!
    @IBOutlet weak var vw_preferredLang: UIView!
    @IBOutlet var lb_b2bUserStatusMsg: UnitsLabel!
    @IBOutlet var lc_b2bUserStatusMsgHeght: NSLayoutConstraint! //45
    
    @IBOutlet var sw_allowAccessForDocs: UISwitch!
    
    @IBOutlet weak var languagesCollectionView: UICollectionView!
    @IBOutlet weak var LC_languagesCV_Height: NSLayoutConstraint!
    @IBOutlet weak var sw_usePreferredLanguage: UISwitch!
    
    @IBOutlet weak var notesTextView: UITextView!
    
    @IBOutlet var dropDownTableView: UITableView!
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    @IBOutlet var iv_profilePicOfSelectedUser: UIImageView!
    @IBOutlet weak var imagesCVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vw_bookConsultantView: UIView!
    
    @IBOutlet weak var lb_consultant: UILabel!
    @IBOutlet weak var lb_selectedScheduledDate: UILabel!
    
    @IBOutlet weak var AI_waiting_time_indicatior: UIActivityIndicatorView!
    @IBOutlet weak var lb_waitingSecs: UILabel!
    @IBOutlet weak var lb_waitingMins: UILabel!
    @IBOutlet weak var iv_expand_arrow: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var lb_call_type: UILabel!
    @IBOutlet weak var vw_callView: UIView!
    @IBOutlet weak var sw_call_type: UISwitch!
    
    @IBOutlet weak var appointmentView: UIView!
    @IBOutlet weak var addNotesOrImagesView: UIView!
    @IBOutlet weak var bt_bookAppointmentOutlet: UIButton!
    @IBOutlet weak var bookConsultationBtnView: UIView!
    
    @IBOutlet weak var tv_notesDeleteButtonOutlet: UIButton!
    @IBOutlet weak var typeOfCallSegmentView: UIView!
    
    //how would you like to recieve a call outlets
    @IBOutlet weak var vw_type_of_recieving_call: UIView!
    @IBOutlet weak var LC_recieving_call_view_height: NSLayoutConstraint!
    @IBOutlet weak var lb_recieve_call_type: UILabel!
    @IBOutlet weak var lb_family_mem_name: UILabel!
    
    //button segmented controllers
    @IBOutlet weak var bt_waiting: UIButton!
    @IBOutlet weak var bt_schedule: UIButton!
    
    //video call
    @IBOutlet weak var lb_video: UILabel!
    @IBOutlet weak var iv_video_icon: UIImageView!
    @IBOutlet weak var vw_video_back: UIView!
    
    //audio
    @IBOutlet weak var vw_aduio_back: UIView!
    @IBOutlet weak var iv_audio_icon: UIImageView!
    @IBOutlet weak var lb_audio: UILabel!
    
    
    //waiting otulets
    @IBOutlet weak var lb_waiting: UILabel!
    @IBOutlet weak var iv_waiting_icon: UIImageView!
    @IBOutlet weak var vw_waiting_back: UIView!
    
    //schedule outlets
    @IBOutlet weak var iv_schedule_icon: UIImageView!
    
    @IBOutlet weak var lb_schedule: UILabel!
    @IBOutlet weak var vw_schedule_back: UIView!
    
    @IBOutlet weak var vw_activity_indicator: UIView!
    
    //FollowUp Outlets
    @IBOutlet var vw_followUpOuterView: UIView!
    @IBOutlet weak var iv_isFollowUpConsult: UIImageView!
    @IBOutlet weak var iv_isSecOpinion: UIImageView!
    @IBOutlet weak var iv_isNewConsult: UIImageView!
    
    @IBOutlet weak var consentFormChqImg: UIImageView!
    @IBOutlet weak var consentTCLbl: FRHyperLabel!
    
    @IBOutlet weak var consultationTypeBtn: UIButton!
    
    ///date formatter instance reference
    let dateFormatter = DateFormatter()
    
    ///picker var instance declaration
    let picker = UIImagePickerController()
    
    ///Booking success instance var declaration
    var dataModal : BookingSuccess!
    
    ///Doctor modal instance var declaration
    var doctorModal : Doctor!
    
    ///timer declaration
    var timer : Timer?
    var slotRefreshTimer : Timer?
    
    ///appointment status check timer
    var bookStatusTimer: Timer?
    
    ///timer total seconds
    var totalSeconds : Int = 0
    
    ///apppointment selected scheduled date instance
    var selectedScheduledDate = ""
    
    ///setting default call type
    var callType = BookingConsultation.CALL_TYPE_VIDEO
    
    ///setting default booking type
    var bookingType = BookingConsultation.BOOKING_TYPE_WAIT
    
    ///default consultation booked for
    var consultation_booked_for = BookingConsultation.CONSULTATION_SELF
    
    ///instance for appointment booked for
    var booked_for_user_id = ""
    
    ///instance var declaration for date
    var scheduled_at = ""
    
    ///instance notes var declaratiomn
    var notes = ""
    ///Not used
    var progressCount = 0
    
    ///Not used
    var buffer:NSMutableData = NSMutableData()
    ///Not used
    var session:URLSession?
    ///Not used
    var dataTask:URLSessionDataTask?
    // let url = URL(string:AppURLS.URL_Appointments_create)!
    var expectedContentLength = 0
    
    ///push notification data
    var userInfo = [AnyHashable : Any]()
    
    ///Instance to keep the generated job id while booking
    var bookingJobId = ""
    var isAppointmentBooked = false
    ///Not used
    var timeslotsSession : URLSession?
    
    ///setting default call channel
    var selectedCallChannel = BookingConsultation.INTERNET_CALL
    
    ///array for family members
    var familyMem = [NSDictionary]()
    var familyMemrs = [User]()
    var tempfamilyMemrs = [User]()
    
    ///wating time status check
    var waitingSlotsStatus = true
    
    ///instance to store selected image from attachments
    var selectedAttachment = ""
    
    ///instance to get selected image from images array
    var selectedImage : UIImage!
    
    
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    
    ///Instance to check the languages is prefered or not
    var isLanguagePreferred = BookingConsultation.STATUS_PREFERRED_LANGUAGES_YES
    
    ///Instance array for all languages
    var allLanguages = [Languages]()
    ///Instance for selected langauages
    var selectedLanguage = [Languages]()
    var datesNTimeSlots = [[String](),[String]()]
    
    ///Stores the slot dates to display in picker
    var slotDates = [String]()
    ///Stores the slot times to display in picker
    var slotTimes = [String]()
    var slotsData : NSDictionary!
    var selectedDate = ""
    var selectedTimeSlot = ""
    
    ///Container Blur view
    var container = UIView()
    
    var consultationType = ""
    var followUpApntId = ""
    var followUpReason = ""
    
    
    
    ///Selected user index for consultation
    var selectedUserIndex = 0
    
    var isConsultationForFamily = false
    var selectedFamilyMember: User?
    
    var followUpStatus = 0
    
    var consentAgreeStatus = false
    var appointment_id = ""
    var followup_reason = ""
    
    let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
    var handler:Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Ask permissions
        didAskForCameraPermission()
        didAskForPhotoLibraryPermission()
        self.activityIndicator.color = Theme.buttonBackgroundColor
        App.ImagesArray.removeAll()
        App.bookingAttachedImages.removeAll()
        configureLinearProgressBar()
        self.lb_family_mem_name.isHidden = true
        self.vw_activity_indicator.isHidden = true
        
        self.sw_usePreferredLanguage.isOn = true
        self.sw_usePreferredLanguage.onTintColor = Theme.switchOnTintColor!
        self.sw_usePreferredLanguage.thumbTintColor = Theme.switchThumbTintColor!
        
        hideRecieveCallView(bool: true, height: 0)
        hidePreferredLangView(bool: false, height: 26)
        App.selectedSlotTime = ""
        
        consentFormChqImg.image = UIImage(named: "UncheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        
        self.imagesCVHeightConstraint.constant = 0
        self.lb_waitingMins.textColor = Theme.buttonBackgroundColor
        self.lb_selectedScheduledDate.textColor = Theme.buttonBackgroundColor
        self.lb_preferredLang.textColor = Theme.buttonBackgroundColor
        
        let checkImg = UIImage(named: "radiochecker")?.withRenderingMode(.alwaysTemplate)
        let unCheckimg = UIImage(named: "radiounchecker")?.withRenderingMode(.alwaysTemplate)
            
            self.bt_internetCheckMark.setImage(checkImg, for: .normal)
            self.bt_internetCheckMark.setTitleColor(UIColor.black, for: .normal)
            self.bt_internetCheckMark.tintColor = Theme.navigationGradientColor![1]!
            self.bt_internetCheckMark.imageView?.contentMode = .scaleAspectFit
            self.bt_telephoneCheckMark.setImage(unCheckimg, for: .normal)
            self.bt_telephoneCheckMark.setTitleColor(UIColor.gray, for: .normal)
            self.bt_telephoneCheckMark.tintColor = Theme.navigationGradientColor![0]!
            self.selectedCallChannel = BookingConsultation.INTERNET_CALL
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificatonArrived(notification:)), name: NSNotification.Name(rawValue: "BookConsultationNotificationRecieved"), object: nil)
        
        //  NotificationCenter.default.addObserver(self, selector: #selector(self.bookConsultation), name: NSNotification.Name(rawValue: "BookConsultationNow"), object: nil)
        
        // NotificationCenter.default.addObserver(self, selector: #selector(BookConsultationViewController.getFamilyMembers(completionHandler:)), name: NSNotification.Name(rawValue: "GetFamilyMem"), object: nil)
        
        segmentedViewSetup()
        
        
        getTimeSlots()
        //   check_mobile_verifield()
        
        
        //        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        //        languagesCollectionView.addGestureRecognizer(longPressGesture)
        
        setDefaultUser()
        dorpDownSetup()
        // addTapGesture()
        notesTextView.isEditable = true
        
        self.consentTCLbl.attributedText = NSAttributedString(string: " I agree to terms and conditions", attributes: self.attributes)
        
        handler = {
            (hyperLabel: FRHyperLabel!, substring: String!) -> Void in
            
            //action here
            print("its started")
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONSENT_FORM, sender: self)
        }
        
        self.consentTCLbl.setLinksForSubstrings(["terms and conditions"], withLinkHandler: (self.handler as! (FRHyperLabel?, String?) -> Void))
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapPreferedLanguage(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.vw_preferredLang.addGestureRecognizer(tapGesture)
        self.vw_preferredLang.isUserInteractionEnabled = true
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.sw_allowAccessForDocs.isOn = (App.appSettings?.ehrDocumentConsent ?? 0) == 1 ? true : false
                self.sw_allowAccessForDocs.set(width: 0.0, height: 0.0) //changed as per ticket
            }
        }
    }
    
    
    func hideDropDown() {
        UIView.animate(withDuration: 0.5) {
            self.LC_dropDownViewHeight.constant = 0
            self.dropDownTableView.isHidden = true
            self.dropDownShadowView.isHidden = true
            self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
        }
    }
    
    ///setups the dropdown tableview
    func dorpDownSetup() {
        self.dropDownTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.dropDownTableView.isHidden = true
        self.dropDownShadowView.isHidden = true
        self.LC_dropDownViewHeight.constant = 0
    }
    
    ///sets the default user for booking consultation
    func setDefaultUser() {
        if !App.avatarUrl.isEmpty {
            let url = URL(string: App.avatarUrl)
            self.iv_profilePicOfSelectedUser.kf.setImage(with: url)
        }
        self.iv_profilePicOfSelectedUser.layer.cornerRadius = self.iv_profilePicOfSelectedUser.frame.size.width / 2
        self.iv_profilePicOfSelectedUser.clipsToBounds = true
        
        self.lb_consultant.text = App.user_full_name + "(myself)"
        App.selectedUserForBooking = App.user_full_name + "(myself)"
        App.isDiagnostics = false
        App.isConsultationForFamily = false
        self.lb_family_mem_name.isHidden = true
        self.booked_for_user_id = ""
        self.consultation_booked_for = BookingConsultation.CONSULTATION_SELF
        
        self.dropDownTableView.layer.borderColor = UIColor.lightGray.cgColor
        self.dropDownTableView.layer.borderWidth = 0.5
        self.addShadow(toViews:[self.vw_bookConsultantView, self.typeOfCallSegmentView, self.appointmentView])
    }
    
    
    
    func addShadow(toViews:[UIView]) {
        for view in toViews {
            //            view.layer.borderColor = UIColor.lightGray.cgColor
            //            view.layer.borderWidth = 0.5
            //            view.layer.cornerRadius = 3.0
            view.layer.shadowRadius = 1
            view.layer.shadowOpacity = 0.5
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
            view.clipsToBounds = false
        }
    }
    
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = languagesCollectionView.indexPathForItem(at: gesture.location(in: languagesCollectionView)) else {
                break
            }
            languagesCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            languagesCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            languagesCollectionView.endInteractiveMovement()
        default:
            languagesCollectionView.cancelInteractiveMovement()
        }
    }
    
    @IBAction func doCheckConsentAgreed(_ sender: UIButton) {
        if consentAgreeStatus{
            consentAgreeStatus = false
            consentFormChqImg.image = UIImage(named: "UncheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        }else{
            consentAgreeStatus = true
            consentFormChqImg.image = UIImage(named: "CheckedBox")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        }
    }
    
    
    
    func showPickerInActionSheet(sentBy: String) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 250)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        pickerView.delegate = self
        pickerView.dataSource = self
        
        if sentBy == "dates"{
            if let dates = self.slotsData {
                if let allDateKeys = dates.allKeys as? [String] {
                    //self.slotDates = allDateKeys
                    self.slotDates.removeAll()
                    for dateKey in allDateKeys {
                        if let values = dates.object(forKey: dateKey) as? [String] {
                            if values.count != 0 {
                                self.slotDates.append(dateKey)
                            }else {
                                print("Date has no slots")
                            }
                        }
                    }
                }
            }
            
            if !slotDates.isEmpty {
                self.slotDates.sort(by: { (dateString1, dateString2) -> Bool in
                    let date1 = self.stringToDateConverter(date: dateString1)
                    let date2 = self.stringToDateConverter(date: dateString2)
                    return date1 < date2
                })
            }
            
            // print("Sorted slot dates:\(self.slotDates)")
            
            pickerView.tag = 1
        } else if sentBy == "slots"{
            pickerView.tag = 2
        } else {
            pickerView.tag = 0
        }
        
        
        vc.view.addSubview(pickerView)
        let title = sentBy == "dates" ? "Select Date" : "Select Slot"
        let editRadiusAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        
        let selectSlot = UIAlertAction(title: "Show Slots", style: .default) { (action) in
            if !self.selectedDate.isEmpty {
                if let slots = self.slotsData.object(forKey: self.selectedDate) as? [String] {
                    self.slotTimes = slots
                }
            }
            if self.slotTimes.count == 0 {
                self.didShowAlert(title: "Sorry!", message: "We regret to inform there are no Doctors available at this moment. Please try after sometime.")
            }else {
                self.showPickerInActionSheet(sentBy: "slots")
            }
        }
        
        
        let slotSelected = UIAlertAction(title: "Done", style: .default) { (action) in
            let seperatedDateKey = self.selectedDate.components(separatedBy: " ")
            self.scheduled_at = seperatedDateKey[0] + " \(self.selectedTimeSlot)"
            App.selectedSlotTime = self.scheduled_at
            print("Selected date and slot :\(self.scheduled_at)")
            self.lb_selectedScheduledDate.isHidden = false
            let date = self.getFormattedDateAndTime(dateString: App.selectedSlotTime )
            self.lb_selectedScheduledDate.text =  date
        }
        
        
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        if sentBy == "dates"{
            editRadiusAlert.addAction(selectSlot)
            editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            if !slotDates.isEmpty  {
                self.present(editRadiusAlert, animated: true)
            }else {
                self.didShowAlert(title: "Sorry!", message: "We regret to inform there are no Doctors available at this moment. Please try after sometime.")
            }
        }else  if sentBy == "slots"{
            editRadiusAlert.addAction(slotSelected)
            editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(editRadiusAlert, animated: true)
        }
        
        
        
        
        //        let title = ""
        //        let message = "\n\n\n\n\n\n\n\n\n\n";
        //        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet);
        //        alert.isModalInPopover = true
        //
        //        let pickerFrame: CGRect = CGRect(x: 17, y: 52, width: alert.view.frame.size.width - 20, height: 100) // CGRectMake(left), top, width, height) - left and top are like margins
        //        let picker: UIPickerView = UIPickerView(frame: pickerFrame)
        //
        //
        //
        //        picker.delegate = self
        //        picker.dataSource = self
        //
        //        alert.view.addSubview(picker);
        //
        //        let toolFrame = CGRect(x: 17, y: 5, width: alert.view.frame.size.width - 20, height: 45)      //(17, 5, 270, 45)
        //        let toolView: UIView = UIView(frame: toolFrame)
        //
        //        let buttonCancelFrame: CGRect = CGRect(x: 0, y: 7, width: 100, height: 30)
        //        let buttonCancel: UIButton = UIButton(frame: buttonCancelFrame);
        //        buttonCancel.setTitle("Cancel", for: .normal);
        //        buttonCancel.setTitleColor(UIColor.blue, for: .normal);
        //        toolView.addSubview(buttonCancel)
        //
        //        buttonCancel.addTarget(self, action: #selector(self.cancelSelection(sender:)), for: .touchUpInside)
        //
        //        //add buttons to the view
        //        let buttonOkFrame: CGRect = CGRect(x: 170, y: 7, width: 100, height: 30)   //size & position of the button as placed on the toolView
        //
        //        //Create the Select button & set the title
        //        let buttonOk: UIButton = UIButton(frame: buttonOkFrame);
        //        buttonOk.setTitle("Select", for: .normal);
        //        buttonOk.setTitleColor(UIColor.blue, for: .normal);
        //        toolView.addSubview(buttonOk); //add to the subview
        //
        //        //Add the tartget. In my case I dynamicly set the target of the select button
        //        if(sentBy == "dates"){
        //            buttonOk.addTarget(self, action: #selector(self.datepicked(sender:)), for: .touchUpInside)
        //        } else if (sentBy == "user"){
        //            buttonOk.addTarget(self, action: #selector(self.slotPicked(sender:)), for: .touchUpInside)
        //        }
        //
        //        //add the toolbar to the alert controller
        //        alert.view.addSubview(toolView);
        //
        //        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func cancelSelection(sender: UIButton){
        print("Cancel");
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc  func datepicked(sender: UIButton) {
        
        if !self.selectedDate.isEmpty {
            if let slots = self.slotsData.object(forKey: self.selectedDate) as? [String] {
                self.slotTimes = slots
            }
        }
        
        self.dismiss(animated: true, completion: nil)
        self.showPickerInActionSheet(sentBy: "slots")
    }
    
    @objc  func slotPicked(sender: UIButton) {
        let seperatedDateKey = self.selectedDate.components(separatedBy: " ")
        self.scheduled_at = seperatedDateKey[0] + " \(self.selectedTimeSlot)"
        App.selectedSlotTime = self.scheduled_at
        print("Selected date and slot :\(self.scheduled_at)")
        self.lb_selectedScheduledDate.isHidden = false
        let date = getFormattedDateAndTime(dateString: App.selectedSlotTime)
        self.lb_selectedScheduledDate.text =  date
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(pickerView.tag == 1){
            return self.slotDates.count
        } else if(pickerView.tag == 2){
            return self.slotTimes.count
        } else  {
            return 0;
        }
    }
    
    // Return the title of each row in your picker ... In my case that will be the date or time slots
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1){
            //  print("Picker view : \(pickerView.tag) ,Slot date:\(self.slotDates[row])")
            self.selectedDate = self.slotDates[row]
            let formatedDate = getFormattedDate(dateString: self.selectedDate )
            return formatedDate
            
        } else if(pickerView.tag == 2){
            //   print("Picker view : \(pickerView.tag) ,Slot time:\(slotTimes[row])")
            let time =  self.slotTimes[row]
            self.selectedTimeSlot = time
            return  UTCToLocalForCV(date: time)
        } else  {
            return "";
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1){
            //  print("Picker view : \(pickerView.tag) ,Slot date:\(self.slotDates[row])")
            self.selectedDate = self.slotDates[row]
        } else if(pickerView.tag == 2){
            // print("Picker view : \(pickerView.tag) ,Slot time:\(slotTimes[row])")
            let time =  self.slotTimes[row]
            self.selectedTimeSlot = time
        }
    }
    
    
    /**
     linear bar configuration
     */
    fileprivate func configureLinearProgressBar(){
        linearBar.backgroundColor = UIColor.white
        linearBar.progressBarColor = UIColor(hexString: StandardColorCodes.GREEN)
        
        //linearBar.heightForLinearBar = 2
    }
    
    /*/** Not used start */
     func sessionSetup() {
     progressBar.progress = 0.0
     let configuration = URLSessionConfiguration.default
     let manqueue = OperationQueue.main
     timeslotsSession = URLSession(configuration: configuration, delegate: self, delegateQueue: manqueue)
     dataTask = timeslotsSession?.dataTask(with: URLRequest(url: URL(string: AppURLS.URL_Appointments_create)!))
     dataTask?.resume()
     }
     
     func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
     
     expectedContentLength = Int(response.expectedContentLength)
     print("session data length:=\(expectedContentLength)")
     completionHandler(URLSession.ResponseDisposition.allow)
     }
     
     func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
     buffer.append(data)
     
     let percentageDownloaded = Float(buffer.length) / Float(expectedContentLength)
     progressBar.progress =  percentageDownloaded
     }
     
     func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
     if error !=  nil {
     print("Error while getting slots:\(error?.localizedDescription)")
     }
     progressBar.progress = 0
     }
     /** Not used end */*/
    
    /**
     custom segmented type view setup on view load
     */
    func segmentedViewSetup() {
        //        self.appointmentView.layer.cornerRadius = 8
        //        self.vw_waiting_back.layer.cornerRadius = 8
        //        self.vw_schedule_back.layer.cornerRadius = 8
        
        //        self.typeOfCallSegmentView.layer.cornerRadius = 8
        //        self.vw_video_back.layer.cornerRadius = 8
        //        self.vw_aduio_back.layer.cornerRadius = 8
        
        waitingViewAdjustments(backColor: Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
        scheduleViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
        
        videoViewAdjustments(backColor: Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
        audioViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
        
        NotificationCenter.default.addObserver(self, selector: #selector(BookConsultationViewController.toShowUserNotesAdded), name: NSNotification.Name(rawValue: "NotesAdded"), object: nil)
    }
    
    /// Language selection delegate method
    ///
    /// - Parameters:
    ///   - success: boolean used to check process is success or not
    ///   - languages: after user selection takes the selected language
    ///   - prefered: not used(used to check selected is prefered or nor)
    func didFinishPickingLanguages(_ success: Bool, languages: [Languages], prefered: String) {
        if success {
            App.selectedLanguages = languages
            //self.languagesCollectionView.reloadData()
            self.dismiss(animated: true, completion: nil)
            print("Delegate performed : ",prefered)
            self.lb_preferredLang.text = prefered
        }
    }
    
    
    /// Method performs request to server for family members
    ///
    /// - Parameter completionHandler: function call back
    func getFamilyMembers(completionHandler : @escaping (_ success:Bool) -> Void) {
        self.tempfamilyMemrs.removeAll()
        let id = User.get_user_profile_default().id ?? 0
        let user = User(id: id , userName: App.user_full_name, type: "self", userAvatar: App.avatarUrl,age:"")
        self.tempfamilyMemrs.append(user)
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        //  self.vw_activity_indicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let slotURL = AppURLS.URL_Family_mem + "-members"
        let url = URL(string: slotURL)
        var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
        request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
        request.httpMethod = HTTPMethods.GET
        request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error
            {
                print("Error==> : \(error.localizedDescription)")
                DispatchQueue.main.async(execute: {
                    //  self.vw_activity_indicator.isHidden = true
                    //      self.activityIndicator.stopAnimating()
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    completionHandler(false)
                })
            }
            if let data = data
            {
                print("data =\(data)")
            }
            if let response = response
            {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
                print("expected lenght :\(httpResponse.expectedContentLength)")
                
                //if response is json do the following
                do
                {
                    let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                    let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                    if !errorStatus {
                        print("performing error handling family mem:\(resultJSON)")
                        DispatchQueue.main.async(execute: {
                            completionHandler(false)
                            self.vw_activity_indicator.isHidden = true
                            //  self.activityIndicator.stopAnimating()
                        })
                    }
                    else {
                        print("family mem response:\(resultJSON)")
                        let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                        let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                        print("Status=\(status) code:\(code)")
                        
                        if let dict = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                            completionHandler(true)
                            self.familyMem = dict
                            App.familyMembers = dict
                            print("Fetched users")
                            var count = 0
                            for mem in dict {
                                count += 1
                                if let name = mem.object(forKey:Keys.KEY_FULLNAME) as? String,let id = mem.object(forKey: Keys.KEY_USER_ID) as? Int , let _ = mem.object(forKey: Keys.KEY_BOOKABLE) as? Bool ,let age = mem.object(forKey: Keys.KEY_AGE) as? String{
                                    //                                    if bookable {
                                    let user = User(id: id, userName: name, type: "family", userAvatar: "",age: age )
                                    self.tempfamilyMemrs.append(user)
                                    //                                    }
                                }
                            }
                            
                            
                            DispatchQueue.main.async(execute: {
                                
                                if count < 3 { //self.tempfamilyMemrs.count < 4 {
                                    let addNew = User(id: 0, userName: "Add New", type: "Other", userAvatar: "AddFamily",age:"")
                                    self.tempfamilyMemrs.append(addNew)
                                }
                                
                                //         self.vw_activity_indicator.isHidden = true
                                //      self.activityIndicator.stopAnimating()
                                self.familyMemrs =  self.tempfamilyMemrs
                                // self.showFamliyAction()
                            })
                        }
                    }
                    
                }catch let error{
                    DispatchQueue.main.async(execute: {
                        completionHandler(false)
                        //       self.vw_activity_indicator.isHidden = true
                        //  self.activityIndicator.stopAnimating()
                    })
                    self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    print("Received not-w@objc ell-formatted JSON : \(error.localizedDescription)")
                }
            }
            
        }).resume()
    }
    
    /**
     Method called when notification fired from server for waiting appointment
     */
    @objc func notificatonArrived(notification: NSNotification) {
        // self.bookingJobId = UserDefaults.standard.value(forKey: "GeneratedJobID") as! String
        //do you updation of labels here
        print("\(#function) calling")
//        print("i got u buddy :\(App.notificationData)")
        self.userInfo = App.notificationData
        
        if self.bookingType == BookingConsultation.BOOKING_TYPE_WAIT || self.bookingType == BookingConsultation.BOOKING_TYPE_SCHEDULE {
            if let type = userInfo["type"] as? String {
                print("** Btype ** :\(type)")
                if type.contains(NotificationType.KEY_NOTIFICATION_TYPE_BOOKING_FAILED) {
                    
                    notifiationErrorHandling(userInfo: userInfo)
                    
                }
                else if type.contains(NotificationType.KEY_NOTIFICATION_TYPE_BOOKING_SUCCESS)
                {
                    if let jobID = userInfo["jobId"] as? String {
                        self.bookingJobId = (UserDefaults.standard.value(forKey: "GeneratedJobID")  as? String) ?? ""
                        print("** jobID ** :\(jobID) storedjobid:\(self.bookingJobId)")
                        if  self.bookingJobId == jobID {
                            
                            var patientName = ""
                            var apppointmentStatus = 1
                            if let status = userInfo[Keys.KEY_STATUS] as? String {
                                apppointmentStatus = Int(status)!
                            }
                            
                            do {
                                if let patientData = userInfo[Keys.KEY_PATIENT] as? String {
                                    let data = patientData.data(using: String.Encoding.utf8)
                                    if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                                        if let pFullName = jsonData.object(forKey: Keys.KEY_FULLNAME) as? String {
                                            patientName = pFullName
                                        }
                                    }
                                }
                            }catch let error {
                                print("Error while getting patient name from notification==>\(error.localizedDescription)")
                            }
                            
                            
                            if let bookedFor = userInfo["booked_for_user_id"] as? String{
                                if let appointmentID = userInfo["appointment_id"] as? String{
                                    if let callType = userInfo["call_type"] as? String{
                                        if let scheduledAt = userInfo["scheduled_at"] as? String{
                                            if let notes = userInfo["notes"] as? String{
                                                if let doctor = userInfo[Keys.KEY_DOCTOR] as? String {
                                                    if let data = doctor.data(using: String.Encoding.utf8) {
                                                        do {
                                                            if let jsonData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                                                                if let docFullName = jsonData.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                                    if let docSpecialisation = jsonData.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                                        if let docAvatar = jsonData.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                                            App.consultationBookingNotes = notes
                                                                            
                                                                            
                                                                            print("Book deteails---\(bookedFor) id\(appointmentID) call\(callType) schedule\(scheduledAt) notes:\(notes)")
                                                                            let bookedData = BookingSuccess(id: Int(appointmentID)!, userid:  self.consultation_booked_for, notes:  App.consultationBookingNotes, bookedfor: Int(bookedFor)!, calltype: Int(callType)!, scheduled: scheduledAt,patientName:patientName)
                                                                            bookedData.doctorName = docFullName
                                                                            bookedData.doctorSpecialisation = docSpecialisation
                                                                            bookedData.doctorAvatar = docAvatar
                                                                            bookedData.status = apppointmentStatus
                                                                            
                                                                            self.dataModal = bookedData
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }catch let err {
                                                            print("Doctor details error while converting to dic:\(err.localizedDescription) ")
                                                        }
                                                    }
                                                }else {
                                                    print(" Doctor details may not be a string")
                                                }
                                            }else {
                                                print("3) notes may not be a string")
                                            }
                                        }else {
                                            print("4) scheduled at  may not be a string")
                                        }
                                    }else {
                                        print("3) call type  may not be a string")
                                    }
                                }else {
                                    print("2) appointment id  may not be a string")
                                }
                            }else {
                                print("1) booked for user id  may not be a string")
                            }
                            
                            do {
                                if let doctorData = userInfo["doctor"] as? String {
                                    let data = doctorData.data(using: String.Encoding.utf8)
                                    if let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                                        if let id = jsonData.object(forKey: Keys.KEY_ID) as? Int {
                                            if let fristName = jsonData.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                                if let middle = jsonData.object(forKey: Keys.KEY_MIDDLE_NAME) as? String {
                                                    if let last = jsonData.object(forKey: Keys.KEY_LAST_NAME) as? String {
                                                        if let practnum = jsonData.object(forKey: Keys.KEY_PRACTITIONER_NUMBER) as? Int {
                                                            if let avatar = jsonData.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                                if let prefix = jsonData.object(forKey: Keys.KEY_PREFIX) as? String {
                                                                    if let specialisation = jsonData.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                                        if let fullName = jsonData.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                                            if let ratings = jsonData.object(forKey: Keys.KEY_RATINGS) as? Double {
                                                                                self.doctorModal = Doctor(prefix:prefix,id:id,first_name:fristName,middle_name:middle,last_name:last,practitioner_number:practnum,avatar_url:avatar,fullname:fullName,specialization:specialisation,rating:ratings)
                                                                                print("prefix==>>\(fristName) id:\(id) avatarURL : \(avatar)")
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }else {
                                        print("Doctor dictionary error")
                                    }
                                }
                                
                                
                            }catch let error {
                                print("Error while getting notification schedule==>\(error.localizedDescription)")
                            }
                            
                            
                            DispatchQueue.main.async(execute: {
                                self.invalidateBookStatusCheckTimer()
                                self.stopAnimating()
                                App.consultationBookingNotes = ""
                                App.consultationDate = ""
                                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BOOKING_SUCCESS, sender:self)
                                UserDefaults.standard.removeObject(forKey: "BookingNotification")
                            })
                            
                        }else {
                            print("soething wrong while fething data")
                        }
                    }
                }
            }
        }
    }
    
    
    /**
     Method called when notification fired from server
     performs if booking failed
     */
    func notifiationErrorHandling(userInfo:[AnyHashable:Any]) {
        if let error = userInfo["error"] as? String {
            print("** error ** :\(error)")
            DispatchQueue.main.async(execute: {
                self.stopAnimating()
                self.invalidateBookStatusCheckTimer()
                self.vw_activity_indicator.isHidden = true
                //     self.activityIndicator.stopAnimating()
                self.didShowAlert(title: "Booking failed!", message: error)
                // AlertView.sharedInsance.showInfo(title: "Booking failed!", message: error)
                UserDefaults.standard.removeObject(forKey: "BookingNotification")
            })
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        consultationTypeBtn.backgroundColor = Theme.buttonBackgroundColor
        bookConsultationBtnView.backgroundColor = Theme.buttonBackgroundColor
        print("viewwill appear called")
        // shadowEffect(views: [vw_bookConsultantView,addNotesOrImagesView])
        // shadowEffect(views: [vw_bookConsultantView,addNotesOrImagesView])
        followUpStatus = 0
        self.container.removeFromSuperview()
        self.vw_followUpOuterView.removeFromSuperview()
        
        self.didShowScheduleDate()
        //     self.toShowUserNotesAdded()
        adjustViewPhotos()
        
        if App.bookingAttachedImages.count >= 5  {// App.ImagesArray.count > 5 {
            //   self.didShowAlert(title: "Sorry", message: "Only 5 Files can be added")
            // AlertView.sharedInsance.showInfo(title: "", message: "Only 5 images can be added")
        }
    }
    
    /**
     Method calls when audio call tapped to show how to recieve a call options
     - Parameter bool: takes a bool value to hide or show.
     - Parameter height: takes height of options view.
     */
    func hideRecieveCallView(bool:Bool,height:CGFloat) {
        UIView.animate(withDuration: 0.5, animations: {
            self.vw_type_of_recieving_call.isHidden = bool
            self.LC_recieving_call_view_height.constant = height
            self.view.layoutIfNeeded()
            
        }) { (success) in
            if success {
                
            }
        }
    }
    
    /**
     Method calls when user changes the preferred language selection
     - Parameter bool: takes a bool value to hide or show.
     - Parameter height: takes height of language view.
     */
    func hidePreferredLangView(bool:Bool,height:CGFloat) {
        self.lb_preferredLang.text = App.lang_preferences_values.first
        UIView.animate(withDuration: 0.5, animations: {
            self.LC_preferredLanguageViewHeight.constant = height
            self.view.layoutIfNeeded()
        }) { (success) in
            self.vw_preferredLang.isHidden = bool
        }
    }
    
    func showB2BUserStatusMsg() {
        UIView.animate(withDuration: 0.5, animations: {
            let isHotline = App.appSettings?.hotline?.isHotline ?? 0
            self.lc_b2bUserStatusMsgHeght.constant = App.userSubscriptionType.lowercased().isEqual(PlanType.B2B) && isHotline == 1 ? 45 : 0
            self.lb_b2bUserStatusMsg.isHidden = App.userSubscriptionType.lowercased().isEqual(PlanType.B2B) && isHotline == 1 ? false : true
            self.view.layoutIfNeeded()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view did appear called")
        
        self.showB2BUserStatusMsg()
        //        self.slotRefreshTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.getTimeSlots), userInfo: nil, repeats: true)
        self.runOldTimer()
        
        //   self.getUserState()
        
        if self.timer == nil {
            self.runTimer()
        }
        
        self.getFamilyMembers(completionHandler: {(success) in
            if success {
                // self.showFamliyAction()
            }
        })
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view did disappear called")
        
        self.linearBar.stopAnimation()
        //  NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"BookConsultationNow"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // hideSideMenuView()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        App.isDiagnostics = false
        App.isConsultationForFamily = false
        super.viewWillDisappear(animated)
        
        if self.slotRefreshTimer != nil {
            self.slotRefreshTimer?.invalidate()
            self.slotRefreshTimer = nil
            print("Slot refresh timer invalidated :\(self.slotRefreshTimer == nil ? true : false)")
        }
        
        if self.timer != nil {
            self.timer?.invalidate()
            timer = nil
            print("Slot refresh timer invalidated :\(self.timer == nil ? true : false)")
        }
        
        
        self.vw_activity_indicator.isHidden = true
        self.activityIndicator.stopAnimating()
        self.linearBar.stopAnimation()
        
    }
    
    /**
     Method used to instatiate timer wit 1 sec
     */
    func runTimer() {
        if App.timeSlots.count != 0 {
            self.lb_waitingMins.isHidden = false
            let waitingSlots = App.timeSlots[0]
            
            let currentDate = "\(Date())"
            let onlyCurrentDate = currentDate.components(separatedBy: " ")
            
            let waitingDate = waitingSlots.dateKey.components(separatedBy: " ")
            
            print("Current Date : \(onlyCurrentDate[0]) dateKey:\(waitingDate[0])")
            
            if  onlyCurrentDate[0] == waitingDate[0] && waitingSlots.timesArray.count != 0
            {
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(BookConsultationViewController.updateTimer), userInfo: nil, repeats: true)
                waitingSlotsStatus = true
            }
            else
            {
                print("No waiting slots availbale")
                self.lb_waitingMins.isHidden =  false
                self.lb_waitingMins.text = "Waiting : N/A"
                waitingSlotsStatus = false
                self.bookingType = BookingConsultation.BOOKING_TYPE_SCHEDULE
                waitingViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
                scheduleViewAdjustments(backColor: Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
            }
        }else {
            self.lb_waitingMins.isHidden =  false
            self.lb_waitingMins.text = "Waiting : N/A"
        }
    }
    
    /**
     Method used to update timer with decreasing seconds
     */
    @objc func updateTimer()
    {
        timeString(time: calculate_time_remaining().0, nextSlot: calculate_time_remaining().1)
    }
    
    
    
    func refreshStatus() {
        let status = MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
        status.backgroundView.backgroundColor = UIColor.orange
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: "Refreshing waiting time...")
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.presentationContext = .window(windowLevel: .statusBar)
    }
    
    
    
    
    
    /* OLD dynamic waiting Timer code in book consulation */
    
    func runOldTimer() {
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(BookConsultationViewController.updateOldTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateOldTimer()
    {
        timeString(time: TimeInterval(totalSeconds))
    }
    
    func timeString(time:TimeInterval)  {
        // let day = Int(time) / 86400
        // let hours = Int(time) % 86400 / 3600
        let minutes = Int(time) / 60 % 60
        // let seconds = Int(time) % 60
        if totalSeconds == 0 {
            timer?.invalidate()
            print("Slots before :\( App.slots)")
            if !App.slots.isEmpty {
                App.slots.removeFirst()
            }
            self.lb_waitingMins.text = "Next slot waiting will be..."
            print("Slots After :\( App.slots)")
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "ReloadSlots"), object: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                // self.repeatTimer()
            })
            
        }else {
            self.lb_waitingMins.text = "Waiting : \(String(format:"%02i", minutes)) min"
            //self.lb_waitingSecs.text = ":\(String(format:"%02i", seconds)) secs"
        }
        
        refreshStatus()
        getTimeSlots()
    }
    
    
    func didShowWaitingTime() {
        if App.timeSlots.count != 0 {
            dateFormatter.dateFormat = "HH:mm:ss"
            self.lb_waitingMins.isHidden = false
            App.slots = App.timeSlots.first?.timesArray ?? []
            let firstSlot = App.slots.first
            if firstSlot != nil {
                let firsSlot_utc_to_local = UTCToLocal(date: firstSlot!)
                let toTime = dateFormatter.date(from: firsSlot_utc_to_local)
                let currentTime = getCurrentTime()
                let calculatedSeconds = (toTime?.timeIntervalSince(currentTime))
                print("Calculated Index:\(calculatedSeconds!)")
                totalSeconds = Int(calculatedSeconds!)
                print("loacl date:\(firstSlot) -> \(firsSlot_utc_to_local)  current:\(currentTime)")
                print("Time difference is :\(totalSeconds)")
                let minutes = Int(totalSeconds) / 60 % 60
                self.lb_waitingMins.text = "Waiting : \(String(format:"%02i", minutes)) min"
                runTimer()
            }else {
                self.lb_waitingMins.isHidden = false
                self.lb_waitingMins.text = "Waiting : N/A"
                runTimer()
            }
        }else {
            print("No time slots available")
            self.lb_waitingMins.isHidden = false
            self.lb_waitingMins.text = "Waiting : N/A"
        }
    }
    
    
    
    
    ///Not used
    func getFirstNSecSlot() -> (String,String){
        var firstSlot = ""
        var secondSlot = ""
        if App.timeSlots.count != 0 && App.timeSlots.count > 1 && App.timeSlots[0].timesArray.count > 0 {
            App.slots = App.timeSlots[0].timesArray
            let dateKey = App.timeSlots[0].dateKey
            let todayDate = dateKey!.components(separatedBy: " ")
            if App.slots.count > 1 {
                firstSlot = todayDate[0] + " \(App.slots[0])"
                secondSlot = todayDate[0] +  " \(App.slots[1])"
            }else if App.slots.count == 1{
                firstSlot = todayDate[0] + " \(App.slots[0])"
                secondSlot = ""
            }
        }else {
            print("Slots empty")
        }
        return (firstSlot,secondSlot)
    }
    
    /**
     method gets time in minutes of current hour and show remaining time for appointment in waiting
     */
    func calculate_time_remaining() -> (Int,Int) {
        
//        let callBackTime = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.APPOINTMENT_CALL_BACK_LIMIT).numberValue?.intValue ?? 15
        let callBackTime = 15

        let date = Date()
        let calendar = Calendar.current
        let minutes = calendar.component(.minute, from: date)
        
        //            print("FirstDate:\(getFirstNSecSlot().0) secondDate:\(getFirstNSecSlot().1) ")
        //            let fstDateSlot =  getFirstNSecSlot().0.isEmpty ? date : stringToDateConverter(date: getFirstNSecSlot().0)
        //            let secDateSlot = getFirstNSecSlot().1.isEmpty ? Date() : stringToDateConverter(date: getFirstNSecSlot().1)
        //            let totalFirstSlotSec = getFirstNSecSlot().0.isEmpty ? 600 : fstDateSlot.timeIntervalSince(date)
        //            let totalSecSlotSec = getFirstNSecSlot().1.isEmpty ? 0 : secDateSlot.timeIntervalSince(date)
        //            let totalFstMinutes = Int(totalFirstSlotSec) / 60 % 60
        //            let totalSecMinutes = Int(totalSecSlotSec) / 60 % 60
        //
        //            return (totalFstMinutes , totalSecMinutes)
        
        //  print("Current time:min\(minutes) ")
        
        /*if minutes >= 0 && minutes < 10
         {
         var remain = 10 - minutes
         
         if remain <= 5 {
         remain += 10
         }
         //  print("Remaining time seconds : \(remain)")
         return (remain , remain + 10)
         }
         
         if minutes >= 10 && minutes < 20
         {
         var remain = 20 - minutes
         //print("remaining_time : \(remain)")
         if remain <= 5 {
         remain += 10
         }
         //   print("Remaining time seconds : \(remain)")
         return (remain , remain + 10)
         }
         
         if minutes >= 20 && minutes < 30
         {
         var remain = 30 - minutes
         //print("remaining_time : \(remain)")
         if remain <= 5 {
         remain += 10
         }
         //   print("Remaining time seconds : \(remain)")
         return (remain , remain + 10)
         }
         
         if minutes >= 30 && minutes < 40
         {
         var remain = 40 - minutes
         //print("remaining_time : \(remain)")
         if remain <= 5 {
         remain += 10
         }
         //   print("Remaining time seconds : \(remain)")
         return (remain , remain + 10)
         }
         
         if minutes >= 40 && minutes < 50
         {
         var remain = 50 - minutes
         if remain <= 5 {
         remain += 10
         }
         //  print("Remaining time seconds : \(remain)")
         return (remain , remain + 10)
         }
         
         if minutes >= 50 && minutes < 60
         {
         var remain = 60 - minutes
         if remain <= 5 {
         remain += 10
         }
         // print("Remaining time seconds : \(remain)")
         return (remain , remain + 10)
         }
         return (0 , 0)*/
        
        for i in (0..<(60/callBackTime))
        {
            if minutes >= (callBackTime*i) && minutes < (callBackTime*(i+1))
            {
                var remain = (callBackTime*(i+1)) - minutes
                if remain <= 5 {
                    remain += callBackTime
                }
                // print("Remaining time seconds : \(remain)")
                return (remain , remain + callBackTime)
            }
        }
        
        return (0 , 0)
    }
    
    
    /**
     method shows the waiting time
     */
    func timeString(time:Int , nextSlot : Int)  {
        if time == 0 && nextSlot == 0 {
            self.lb_waitingMins.isHidden = true
        }else {
            self.lb_waitingMins.isHidden = false
            if nextSlot == 0 {
                self.lb_waitingMins.text = "Waiting : \(String(format:"%02i", time)) min"
            }else {
                self.lb_waitingMins.text = "Waiting : \(String(format:"%02i", time)) - \(nextSlot) min"
            }
        }
    }
    
    /**
     repeats timer
     NOTE: Not used
     */
    func repeatTimer() {
        if App.slots.count != 0 {
            dateFormatter.dateFormat = "HH:mm:ss"
            self.lb_waitingMins.isHidden = false
            let firstSlot = App.slots[0]
            let firsSlot_utc_to_local = UTCToLocal(date: firstSlot)
            let toTime = dateFormatter.date(from: firsSlot_utc_to_local)
            let currentTime = getCurrentTime()
            totalSeconds = Int((toTime?.timeIntervalSince(currentTime))!)
            print("loacl date:\(firstSlot) -> \(firsSlot_utc_to_local)  current:\(currentTime)")
            print("Time difference is :\(totalSeconds)")
            runTimer()
        }else {
            print("No time slots available")
            self.lb_waitingMins.isHidden = true
        }
    }
    
    
    /**
     Method displays scheduled date of appointment
     */
    func didShowScheduleDate() {
        if App.selectedSlotTime.isEmpty != true {
            self.lb_selectedScheduledDate.isHidden = false
            let date = getFormattedDateAndTime(dateString: App.selectedSlotTime)
            self.lb_selectedScheduledDate.text =  date
            self.scheduled_at = App.selectedSlotTime   ///Changed
        }else{
            self.lb_selectedScheduledDate.isHidden = true
        }
    }
    
    /**
     Method displays notes added by user
     */
    @objc func toShowUserNotesAdded() {
        if App.consultationBookingNotes.isEmpty != true {
            self.notesTextView.text = App.consultationBookingNotes
            // self.tv_notesDeleteButtonOutlet.isHidden = false
            self.notesTextView.isHidden = false
            //  self.notesTextView.layer.cornerRadius = 6
        }else{
            self.notesTextView.isHidden = true
            //  self.tv_notesDeleteButtonOutlet.isHidden = true
        }
    }
    
    /**
     Method performs request and get the time slots details from server
     */
    @objc func getTimeSlots() {
        print("*****")
        //startAnimating()
        self.linearBar.startAnimation()
        self.AI_waiting_time_indicatior.isHidden = false
        self.AI_waiting_time_indicatior.startAnimating()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let slotURL = AppURLS.URL_Appointments_create
            let url = URL(string: slotURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.httpMethod = HTTPMethods.GET
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    //self.stopAnimating()
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        //   AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                        self.AI_waiting_time_indicatior.isHidden = true
                        self.AI_waiting_time_indicatior.stopAnimating()
                        self.linearBar.stopAnimation()
                    })
                    // self.didShowAlert(title: "", message: error.localizedDescription)
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    self.expectedContentLength = Int(httpResponse.expectedContentLength)
                    print("expected lenght :\(httpResponse.expectedContentLength)")
                    
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling time slots:\(resultJSON)")
                        }
                        else {
                            print("Slots response:\(resultJSON)")
                            if let code = resultJSON.object(forKey: Keys.KEY_CODE) as? Int {
                                if let status = resultJSON.object(forKey: Keys.KEY_STATUS) as? String {
                                    print("Status=\(status) code:\(code)")
                                    if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                        self.slotsData = data
                                        if code == 200 && status == "success"
                                        {
                                            App.timeSlots.removeAll()
                                            for (key,value) in data {
                                                if let keyString = key as? String {
                                                    let separateKey = keyString.components(separatedBy: " ")
                                                    if let values = data.object(forKey: key) as? [String] {
                                                        let times = values
                                                        let currentDate = self.getCurrentDate()
                                                        if times.count == 0 {
                                                            print("This \(key) has no values")
                                                            //changed here to check no time slots and have only dates
                                                            let slotModal = TimeSlots(dateKey: key as! String, times: [String]())
                                                            if separateKey[0] == currentDate {
                                                                App.timeSlots.insert(slotModal, at: 0)
                                                                App.dates.insert(key as! String, at: 0)
                                                            }else {
                                                                App.timeSlots.append(slotModal)
                                                                App.dates.append(key as! String)
                                                            }
                                                        }else {
                                                            print("Todays date:=\(self.getCurrentDate())")
                                                            let slotModal = TimeSlots(dateKey: key as! String, times: values)
                                                            
                                                            print("Appendin values:\(key) value:\(value)")
                                                            if separateKey[0] == currentDate {
                                                                App.timeSlots.insert(slotModal, at: 0)
                                                                App.dates.insert(key as! String, at: 0)
                                                            }else {
                                                                App.timeSlots.append(slotModal)
                                                                App.dates.append(key as! String)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            print("Date keys = \(App.dates) ")
                                            
                                            DispatchQueue.main.async(execute: {
                                                // self.stopAnimating()
                                                self.timer?.invalidate()
                                                self.timer = nil
                                                self.didShowWaitingTime()
                                                //                                                self.runTimer() static timer code
                                                self.runOldTimer()
                                                SwiftMessages.hide()
                                                self.AI_waiting_time_indicatior.stopAnimating()
                                                self.AI_waiting_time_indicatior.isHidden = true
                                                self.linearBar.stopAnimation()
                                                
                                                print("*1*1*1*1")
                                            })
                                            
                                        }
                                        else
                                        {
                                            let errors = data.object(forKey: "errors") as! NSDictionary
                                            print("Error***=>\(errors)")
                                        }
                                    }
                                }
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.linearBar.stopAnimation()
                            self.AI_waiting_time_indicatior.stopAnimating()
                            self.AI_waiting_time_indicatior.isHidden = true
                        })
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                    
                }
                
            }).resume()
        }
        
    }
    
    
    /**
     Adjusts video button view on selection
     - Parameter backColor: Pass the color for background view
     - Parameter imageNLabelColor: pass the color for image and label
     */
    func videoViewAdjustments(backColor:UIColor,imageNLabelColor:UIColor) {
        self.vw_video_back.backgroundColor = backColor
        self.iv_video_icon.image =  self.iv_video_icon.image?.withRenderingMode(.alwaysTemplate)
        self.iv_video_icon.tintColor = imageNLabelColor
        self.lb_video.textColor = imageNLabelColor
    }
    
    /**
     Adjusts Audio button view on selection
     - Parameter backColor: Pass the color for background view
     - Parameter imageNLabelColor: pass the color for image and label
     */
    func audioViewAdjustments(backColor:UIColor,imageNLabelColor:UIColor) {
        self.vw_aduio_back.backgroundColor = backColor
        self.iv_audio_icon.image =  self.iv_audio_icon.image?.withRenderingMode(.alwaysTemplate)
        self.iv_audio_icon.tintColor = imageNLabelColor
        self.lb_audio.textColor = imageNLabelColor
    }
    
    //appointment view setup
    /**
     Adjusts waiting button view on selection
     - Parameter backColor: Pass the color for background view
     - Parameter imageNLabelColor: pass the color for image and label
     */
    func waitingViewAdjustments(backColor:UIColor,imageNLabelColor:UIColor) {
        self.vw_waiting_back.backgroundColor = backColor
        self.iv_waiting_icon.image =  self.iv_waiting_icon.image?.withRenderingMode(.alwaysTemplate)
        self.iv_waiting_icon.tintColor = imageNLabelColor
        self.lb_waiting.textColor = imageNLabelColor
    }
    
    /**
     Adjusts schedule button view on selection
     - Parameter backColor: Pass the color for background view
     - Parameter imageNLabelColor: pass the color for image and label
     */
    func scheduleViewAdjustments(backColor:UIColor,imageNLabelColor:UIColor) {
        self.vw_schedule_back.backgroundColor = backColor
        self.iv_schedule_icon.image =  self.iv_schedule_icon.image?.withRenderingMode(.alwaysTemplate)
        self.iv_schedule_icon.tintColor = imageNLabelColor
        self.lb_schedule.textColor = imageNLabelColor
    }
    
    /**
     Adjusts languages collection view height
     - Parameter withHeight: takes the hieght
     */
    func adjustLanguagesView(withHeight:CGFloat) {
        UIView.animate(withDuration: 0.5, animations: {
            self.LC_languagesCV_Height.constant = withHeight
            self.view.layoutIfNeeded()
        }) { (success) in
            if success {
                self.languagesCollectionView.isHidden = false
                self.languagesCollectionView.reloadData()
            }
        }
    }
    
    func postConsentChange(allow:Int) {
        let dic = ["document_consent": allow]
        if let data = try? JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted) {
            NetworkCall.performPATCH(url: AppURLS.URL_EHR_CONSENT, data: data) { (success, response, statusCode, error) in
                if let err = error {
                    print("Error while posting consent:\(err.localizedDescription)")
                    DispatchQueue.main.async {
                        self.didShowAlert(title:"",message:"Couldn't update the access. Please try again")
                        self.sw_allowAccessForDocs.isOn = !self.sw_allowAccessForDocs.isOn
                    }
                }else {
                    let check = self.check_Status_Code(statusCode: statusCode ?? 0, data: response)
                    
                    if check , let resp = response ,
                        let data =  resp.object(forKey: Keys.KEY_DATA ) as? [String:Any] ,
                        let consent =  data["ehr_document_consent"] as? Int {
                        print("Access updated:\(resp)")
                        DispatchQueue.main.async {
                            self.sw_allowAccessForDocs.isOn =  consent == 1 ? true : false
                            App.appSettings?.ehrDocumentConsent = consent
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.didShowAlert(title:"",message:"Couldn't update the access. Please try again")
                            self.sw_allowAccessForDocs.isOn = !self.sw_allowAccessForDocs.isOn
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func allowAccessForDocsTapped(_ sender: UISwitch) {
        self.postConsentChange(allow: sender.isOn ? 1 : 0)
    }
    
    
    ///switch to preferred langauges
    @IBAction func preferredLanguagesTapped(_ sender: UISwitch) {
        
        DispatchQueue.main.async {
            self.hideDropDown()
            if sender.isOn {
                self.hidePreferredLangView(bool: false, height: 26)
                self.isLanguagePreferred = BookingConsultation.STATUS_PREFERRED_LANGUAGES_YES
            }else {
                self.vw_preferredLang.isHidden = true
                self.hidePreferredLangView(bool: true, height: 0)
                self.isLanguagePreferred = BookingConsultation.STATUS_PREFERRED_LANGUAGES_NO
            }
        }
    }
    
    @IBAction func languageSelectTapped(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_LANGUAGES, sender: sender)
        
    }
    
    @objc func tapPreferedLanguage(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_LANGUAGES, sender: sender)
    }
    
    @IBAction func callSegmentedAction(_ sender: UIButton) {
        self.hideDropDown()
        if sender.tag == 1 {
            print("Video selected")
            //            self.sw_call_type.isEnabled = false
            //            self.sw_call_type.isOn = true
            videoViewAdjustments(backColor: Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
            audioViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
            self.callType = BookingConsultation.CALL_TYPE_VIDEO
            hideRecieveCallView(bool: true, height: 0)
            // self.lb_call_type.text = "Internet call(Doconline App)"
            self.selectedCallChannel = BookingConsultation.INTERNET_CALL
        }else if sender.tag == 2 {
            print("Audio selected")
            //  self.sw_call_type.isEnabled = true
            videoViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
            audioViewAdjustments(backColor:  Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
            self.callType = BookingConsultation.CALL_TYPE_AUDIO
            hideRecieveCallView(bool: false, height: 45)
        }
    }
    
    @IBAction func segmentedActionButton(_ sender: UIButton) {
        self.hideDropDown()
        if sender.tag == 1 {
            if App.timeSlots.count != 0 {
                let waitingSlots = App.timeSlots[0]
                
                let currentDate = "\(Date())"
                let onlyCurrentDate = currentDate.components(separatedBy: " ")
                
                let dateOfSlots = self.stringToDateConverter(date:  waitingSlots.dateKey)
                let dateOfSlotsDate = "\(dateOfSlots)"
                let dateFromSlotKey = dateOfSlotsDate.components(separatedBy: " ")
                
                // let waitingDate = waitingSlots.dateKey.components(separatedBy: " ")
                
                //    print("Current Date : \(onlyCurrentDate[0]) dateKey:\(dateFromSlotKey[0])")
                
                if  onlyCurrentDate[0] == dateFromSlotKey[0] && waitingSlots.timesArray.count != 0
                {
                    print("Waiting selected")
                    self.bookingType = BookingConsultation.BOOKING_TYPE_WAIT
                    waitingViewAdjustments(backColor: Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
                    scheduleViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
                    self.scheduled_at = ""
                    App.selectedSlotTime = ""
                    self.lb_selectedScheduledDate.isHidden = true
                    self.waitingSlotsStatus = true
                }
                else
                {
                    self.waitingSlotsStatus = false
                    self.didShowAlert(title: "Not Available", message: "Instant booking not available for the day.")
                    //  AlertView.sharedInsance.showInfo(title: "Sorry!", message: "Instant booking not available for the day.")
                }
            }
            
        }else if sender.tag == 2 {
            print("schedule selected")
            self.bookingType = BookingConsultation.BOOKING_TYPE_SCHEDULE
            waitingViewAdjustments(backColor: UIColor.white, imageNLabelColor: UIColor.gray)
            scheduleViewAdjustments(backColor: Theme.buttonBackgroundColor!, imageNLabelColor: UIColor.white)
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            if self.slotsData != nil {
                self.showPickerInActionSheet(sentBy: "dates")
            }else {
                //self.didShowAlert(title: "", message: "No slots available")
                self.didShowAlert(title: "", message: "We regret to inform there are no Doctors available at this moment. Please try after sometime.")
            }
            // self.performSegue(withIdentifier: "SlotBooking", sender: self)
        }
    }
    
    
    func pickerViewPresentation() {
        //        let vc = UIViewController()
        //        vc.preferredContentSize = CGSize(width: 250,height: 300)
        //        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        //        pickerView.delegate = self
        //        pickerView.dataSource = self
        //        vc.view.addSubview(pickerView)
        //        let editRadiusAlert = UIAlertController(title: "Choose distance", message: "", preferredStyle: UIAlertControllerStyle.alert)
        //        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        //        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        //        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //        self.present(editRadiusAlert, animated: true)
    }
    
    func selectCallTypeViewSetup() {
        //        for segmentedControl in [typeOfCallSegmentView] {
        //            segmentedControl?.layer.borderWidth = 0
        //            segmentedControl?.backgroundColor = UIColor.white
        //            segmentedControl?.styleDelegate = self
        //            segmentedControl?.delegate = self
        //        }
        //        //First segment control
        //        let yFirstCell = self.createCell(
        //            text: "VIDEO",
        //            image: self.createImage(withName: "Video_Dark")
        //        )
        //        yFirstCell.layout = .textWithImage
        //        let ySecondCell = self.createCell(
        //            text: "AUDIO",
        //            image: self.createImage(withName: "Audio_White")
        //        )
        //        ySecondCell.layout = .textWithImage
        //        self.typeOfCallSegmentView.roundedRelativeFactor = 0.2
        //        self.typeOfCallSegmentView.add(cells: [yFirstCell, ySecondCell])
        
        //second segment control
        //        let xFirstCell = self.createCell(
        //            text: "WAITING ROOM",
        //            image: self.createImage(withName: "Waiting")
        //        )
        //        xFirstCell.layout = .textWithImage
        //        let xSecondCell = self.createCell(
        //            text: "SCHEDULE",
        //            image: self.createImage(withName: "Home-BookAConsultation")
        //        )
        //        xSecondCell.layout = .textWithImage
        //        self.appointmentView.roundedRelativeFactor = 0.2
        //        self.appointmentView.add(cells: [xFirstCell, xSecondCell])
        
    }
    
    /* SParrow
     private func createCell(text: String, image: UIImage) -> SPSegmentedControlCell {
     let cell = SPSegmentedControlCell.init()
     cell.label.text = text
     cell.label.font = UIFont(name: "Avenir-Medium", size: 13.0)!
     cell.imageView.image = image
     return cell
     }
     
     private func createImage(withName name: String) -> UIImage {
     return UIImage.init(named: name)!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
     }
     
     func selectedState(segmentControlCell: SPSegmentedControlCell, forIndex index: Int) {
     print("selected index:=\(index) tag:\(segmentControlCell.superview?.tag)")
     SPAnimation.animate(0.1, animations: {
     segmentControlCell.imageView.tintColor = UIColor.white
     segmentControlCell.backgroundColor = UIColor.init(hexString: "#05424C")
     })
     
     UIView.transition(with: segmentControlCell.label, duration: 0.1, options: [.transitionCrossDissolve, .beginFromCurrentState], animations: {
     segmentControlCell.label.textColor = UIColor.white
     }, completion: nil)
     
     if segmentControlCell.superview!.tag == 1 {
     if index == 0 {
     self.callType = BookingConsultation.CALL_TYPE_VIDEO
     }else if index == 1 {
     self.callType = BookingConsultation.CALL_TYPE_AUDIO
     }
     
     }else if segmentControlCell.superview!.tag == 2 {
     if index == 1 {
     
     if !NetworkUtilities.isConnectedToNetwork()
     {
     //   self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
     AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
     return
     }
     
     self.performSegue(withIdentifier: "SlotBooking", sender: self)
     
     }else if index == 0 {
     
     }
     }
     }
     
     func normalState(segmentControlCell: SPSegmentedControlCell, forIndex index: Int) {
     SPAnimation.animate(0.1, animations: {
     segmentControlCell.imageView.tintColor = UIColor.init(hexString: "#05424C")
     segmentControlCell.backgroundColor = UIColor.white
     })
     
     UIView.transition(with: segmentControlCell.label, duration: 0.1, options: [.transitionCrossDissolve, .beginFromCurrentState], animations: {
     segmentControlCell.label.textColor = UIColor.init(hexString: "#05424C")
     }, completion: nil)
     }
     
     func indicatorViewRelativPosition(position: CGFloat, onSegmentControl segmentControl: SPSegmentedControl) {
     let percentPosition = position / (segmentControl.frame.width - position) / CGFloat(segmentControl.cells.count - 1) * 100
     let intPercentPosition = Int(percentPosition)
     print("scrolling: \(intPercentPosition)%")
     }
     */
    
    /**
     Shows the action sheet with options of family members
     */
    func showFamliyAction() {
        let actionView = UIAlertController(title: "Select family member", message: "Consultation can only be booked for age below 16", preferredStyle: UIAlertController.Style.actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            })
        }
        
        if self.familyMem.count < 3 {
            let addNew = UIAlertAction(title: "Add new member", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_FAMILY_MEM, sender: self)
            }
            actionView.addAction(addNew)
        }
        
        
        for mem in self.familyMem {
            
            let name = mem.object(forKey: Keys.KEY_FULLNAME) as! String
            let userid = mem.object(forKey: Keys.KEY_USER_ID) as! Int
            
            let member = UIAlertAction(title: name, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                self.booked_for_user_id = "\(userid)"
                
                self.lb_family_mem_name.isHidden = false
                self.lb_family_mem_name.text = "Consultation For : \(name)"
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
                })
            })
            actionView.addAction(member)
        }
        
        
        actionView.addAction(cancel)
        self.present(actionView, animated: true, completion: nil)
    }
    
    ///Shows the alert to subscribe if not subscribed
    func showSubscriptionAlert(message:String) {
        //        var topController : UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
        //        while ((topController.presentedViewController) != nil) {
        //            topController = topController.presentedViewController!
        //        }
        
        let alert = UIAlertController(title: "Membership", message: message, preferredStyle: UIAlertController.Style.alert)
        let viewPlans = UIAlertAction(title: "Choose", style: .default, handler: { (action) in
            App.isFromView = FromView.BookingView
            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS, sender: self)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        alert.addAction(viewPlans)
        // alert.definesPresentationContext = true
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func callCheckMarkTapped(_ sender: UIButton) {
        self.hideDropDown()
        if self.callType == BookingConsultation.CALL_TYPE_VIDEO {
            self.bt_internetCheckMark.setImage(#imageLiteral(resourceName: "GreenCheck"), for: .normal)
            self.bt_internetCheckMark.setTitleColor(UIColor.black, for: .normal)
            self.bt_telephoneCheckMark.setImage(#imageLiteral(resourceName: "GrayCheck"), for: .normal)
            self.bt_telephoneCheckMark.setTitleColor(UIColor.gray, for: .normal)
            self.selectedCallChannel = BookingConsultation.INTERNET_CALL
        }else {
            let checkImg = UIImage(named: "radiochecker")?.withRenderingMode(.alwaysTemplate)
            let unCheckimg = UIImage(named: "radiounchecker")?.withRenderingMode(.alwaysTemplate)
            if sender.tag == 1 {
                
                self.bt_internetCheckMark.setImage(checkImg, for: .normal)
                self.bt_internetCheckMark.setTitleColor(UIColor.black, for: .normal)
                self.bt_internetCheckMark.tintColor = Theme.navigationGradientColor![1]!
                self.bt_internetCheckMark.imageView?.contentMode = .scaleAspectFit
                self.bt_telephoneCheckMark.setImage(unCheckimg, for: .normal)
                self.bt_telephoneCheckMark.setTitleColor(UIColor.gray, for: .normal)
                self.bt_telephoneCheckMark.tintColor = Theme.navigationGradientColor![0]!
                self.selectedCallChannel = BookingConsultation.INTERNET_CALL
            }else {
                self.bt_internetCheckMark.setImage(unCheckimg, for: .normal)
                self.bt_internetCheckMark.setTitleColor(UIColor.gray, for: .normal)
                self.bt_internetCheckMark.tintColor = Theme.navigationGradientColor![0]!

                self.bt_telephoneCheckMark.setImage(checkImg, for: .normal)
                self.bt_telephoneCheckMark.setTitleColor(UIColor.black, for: .normal)
                self.bt_telephoneCheckMark.tintColor = Theme.navigationGradientColor![1]!

                self.selectedCallChannel = BookingConsultation.REGULAR_CALL
            }
        }
    }
    
    @IBAction func bookConsultationForTapped(_ sender: UIButton) {
        
        self.dropDownTableView.reloadData()
        //  self.dropDownTableView.dropShadow()
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            self.LC_dropDownViewHeight.constant = self.dropDownTableView.contentSize.height //+ 40
            self.dropDownTableView.isHidden = false
            self.dropDownShadowView.isHidden = false
        })
        
        
        //        let actionView = UIAlertController(title: "Book counsultation for?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        //        let selfAction = UIAlertAction(title: "YOURSELF", style: UIAlertActionStyle.default) { (UIAlertAction) in
        //            self.lb_consultant.text = "YOURSELF"
        //            App.isConsultationForFamily = false
        //            self.lb_family_mem_name.isHidden = true
        //            self.booked_for_user_id = ""
        //            self.consultation_booked_for = BookingConsultation.CONSULTATION_SELF
        //            UIView.animate(withDuration: 0.5, animations: {
        //                self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
        //            })
        //        }
        //
        //        let familyAction = UIAlertAction(title: "FAMILY", style: UIAlertActionStyle.default) { (UIAlertAction) in
        //            self.lb_consultant.text = "FAMILY"
        //            self.consultation_booked_for = BookingConsultation.CONSULTATION_FAMILY
        //            App.isConsultationForFamily = true
        //            UIView.animate(withDuration: 0.5, animations: {
        //                self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
        //            })
        //
        //            self.getFamilyMembers(completionHandler: {(success) in
        //                if success {
        //                   // self.showFamliyAction()
        //                }
        //            })
        //        }
        //
        //        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (UIAlertAction) in
        //            UIView.animate(withDuration: 0.5, animations: {
        //                self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
        //            })
        //        }
        //
        //
        //        actionView.addAction(selfAction)
        //        actionView.addAction(cancel)
        //        actionView.addAction(familyAction)
        //        self.present(actionView, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func callTypeTapped(_ sender: UISwitch) {
        
        if sender.isOn {
            self.lb_call_type.text = "Internet call(Doconline App)"
            self.selectedCallChannel = BookingConsultation.INTERNET_CALL
            print("call type:\( self.selectedCallChannel)")
        }else {
            self.lb_call_type.text = "Regular call(Cellular Network)"
            self.selectedCallChannel = BookingConsultation.REGULAR_CALL
            print("call type:\( self.selectedCallChannel)")
        }
    }
    
    
    @IBAction func refreshTapped(_ sender: UIBarButtonItem) {
        // runTimer()
        getTimeSlots()
    }
    
    func runAppointmentCheckTimer() {
        self.bookStatusTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.checkDBForAppointmentStatus), userInfo: nil, repeats: false)
        print("is timer valid:\(self.bookStatusTimer?.isValid)")
    }
    
    func invalidateBookStatusCheckTimer() {
        if self.bookStatusTimer != nil {
            self.bookStatusTimer?.invalidate()
            self.bookStatusTimer = nil
        }
    }
    
    @objc func checkDBForAppointmentStatus() {
        func showFailedAlert() {
            let alert = UIAlertController(title: "Booking Failed!", message: "Please check your internet connection and try again.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            let retry = UIAlertAction(title: "Retry", style: .default) { (action) in
                self.bookConsultation()
            }
            alert.addAction(ok)
            alert.addAction(retry)
            self.present(alert, animated: true, completion: nil)
        }
        
        if !self.bookingJobId.isEmpty , !self.isAppointmentBooked{
            let statusCheckURL = AppURLS.URL_BookAppointment + "/\(self.bookingJobId)/jobstatus"
            print("Status check url:\(statusCheckURL)")
            //            DispatchQueue.global(qos: .default).asyncAfter(deadline: .now() + .seconds(60)) {
            NetworkCall.performGet(url: statusCheckURL) { (success, response, statusCode, error) in
                if let err = error {
                    print("Error while performing status check of appointment:\(err.localizedDescription)")
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.vw_activity_indicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.invalidateBookStatusCheckTimer()
                        showFailedAlert()
                    })
                }else if success ,
                    let resp = response,
                    let userInfo = resp.object(forKey: Keys.KEY_DATA) as? [String:Any]{
                    print("got your response:\(userInfo)")
                    var patientName = ""
                    var apppointmentStatus = 1
                    if let status = userInfo[Keys.KEY_STATUS] as? Int {
                        apppointmentStatus = status
                    }
                    
                    
                    if let patientData = userInfo[Keys.KEY_PATIENT] as? NSDictionary {
                        if let pFullName = patientData.object(forKey: Keys.KEY_FULLNAME) as? String {
                            patientName = pFullName
                        }
                    }
                    
                    
                    
                    if let bookedFor = userInfo["booked_for_user_id"] as? Int{
                        if let appointmentID = userInfo["appointment_id"] as? Int{
                            if let callType = userInfo["call_type"] as? Int{
                                if let scheduledAt = userInfo["scheduled_at"] as? String{
                                    if let notes = userInfo["notes"] as? String{
                                        if let doctor = userInfo[Keys.KEY_DOCTOR] as? NSDictionary {
                                            
                                            if let docFullName = doctor.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                if let docSpecialisation = doctor.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                    if let docAvatar = doctor.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                        App.consultationBookingNotes = notes
                                                        //  print("Book deteails---\(bookedFor) id\(appointmentID) call\(callType) schedule\(scheduledAt) notes:\(notes)")
                                                        let bookedData = BookingSuccess(id: appointmentID, userid:  self.consultation_booked_for, notes:  App.consultationBookingNotes, bookedfor: bookedFor, calltype: callType, scheduled: scheduledAt,patientName:patientName)
                                                        bookedData.doctorName = docFullName
                                                        bookedData.doctorSpecialisation = docSpecialisation
                                                        bookedData.doctorAvatar = docAvatar
                                                        bookedData.status = apppointmentStatus
                                                        
                                                        self.dataModal = bookedData
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if let doctorData = userInfo["doctor"] as? NSDictionary {
                        if let id = doctorData.object(forKey: Keys.KEY_ID) as? Int {
                            if let fristName = doctorData.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                if let middle = doctorData.object(forKey: Keys.KEY_MIDDLE_NAME) as? String {
                                    if let last = doctorData.object(forKey: Keys.KEY_LAST_NAME) as? String {
                                        if let practnum = doctorData.object(forKey: Keys.KEY_PRACTITIONER_NUMBER) as? Int {
                                            if let avatar = doctorData.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                if let prefix = doctorData.object(forKey: Keys.KEY_PREFIX) as? String {
                                                    if let specialisation = doctorData.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                        if let fullName = doctorData.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                            if let ratings = doctorData.object(forKey: Keys.KEY_RATINGS) as? Double {
                                                                self.doctorModal = Doctor(prefix:prefix,id:id,first_name:fristName,middle_name:middle,last_name:last,practitioner_number:practnum,avatar_url:avatar,fullname:fullName,specialization:specialisation,rating:ratings)
                                                                print("prefix==>>\(fristName) id:\(id) avatarURL : \(avatar)")
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.vw_activity_indicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        App.consultationBookingNotes = ""
                        App.consultationDate = ""
                        self.invalidateBookStatusCheckTimer()
                        if self.dataModal != nil , self.doctorModal != nil {
                            self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BOOKING_SUCCESS, sender:self)
                            UserDefaults.standard.removeObject(forKey: "BookingNotification")
                        }
                    })
                    
                }else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.vw_activity_indicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.invalidateBookStatusCheckTimer()
                        showFailedAlert()
                    })
                    print("Something happend no appointment status to get")
                }
            }
            //            }
        }
    }
    
    /**
     performs booking consultation request
     */
    @objc func bookConsultation() {
        
        
        
        
        
        let defaults = UserDefaults.standard
        var dataToPost = ""
        let enteredNotes = self.notesTextView.text! as String
        App.consultationBookingNotes = enteredNotes
        
        print("\(#function) called")
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil
        {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                // AlertView.sharedInsance.showFailureAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                return
            }
            
            self.vw_activity_indicator.isHidden = false
            self.activityIndicator.startAnimating()
            //self.startAnimating()
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            
            let json: [String: Any] = [Keys.KEY_ISFOLLOWUP:Int(consultationType)!, Keys.KEY_FOLLOWUPREASON :followUpReason,Keys.KEY_FOLLOWUPID :Int(followUpApntId)!]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let convertedString = String(data: jsonData!, encoding: String.Encoding.utf8)
            
            dataToPost = "\(Keys.KEY_BOOKING_TYPE)=\(self.bookingType)&\(Keys.KEY_BOOKED_FOR)=\(self.consultation_booked_for)&\(Keys.KEY_BOOKED_FOR_USER_ID)=\(self.booked_for_user_id)&\(Keys.KEY_CALL_TYPE)=\(self.callType)&\(Keys.KEY_CALL_CHANNEL)=\(self.selectedCallChannel)&\(Keys.KEY_SCHEDULED_AT)=\(self.scheduled_at)&\(Keys.KEY_NOTES)=\(App.consultationBookingNotes)&\(Keys.KEY_LANG_PREF)=\(self.isLanguagePreferred)&\(Keys.KEY_FOLLOWUPDATA)=\(convertedString ?? "")"
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_BookAppointment
            let url = URL(string: bookURL)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.POST
            let postData = dataToPost
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = postData.data(using: String.Encoding.utf8)
            
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.vw_activity_indicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print("Book appointment result:\(resultJSON)")
                        
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            if httpResponse.statusCode == 402
                            {
                                if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    if let allKeys = data.allKeys as? [String] {
                                        if allKeys.contains(Keys.KEY_SUBSCRIPTION_ERROR) {
                                            if let message = data.object(forKey: Keys.KEY_SUBSCRIPTION_ERROR) as? String {
                                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                                                    self.vw_activity_indicator.isHidden = true
                                                    self.activityIndicator.stopAnimating()
                                                    self.showSubscriptionAlert(message: message)
                                                })
                                            }
                                        }
                                    }
                                }
                            }else if httpResponse.statusCode == 422
                            {
                                DispatchQueue.main.async(execute: {
                                    self.vw_activity_indicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
                                })
                            }
                            else
                            {
                                if let message = resultJSON.object(forKey: Keys.KEY_DATA) as? NSDictionary {
                                    if let mobile = message.object(forKey: Keys.KEY_MOBILE_NO) as? String {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                                            self.vw_activity_indicator.isHidden = true
                                            self.activityIndicator.stopAnimating()
                                            //if httpResponse.statusCode == 410 {
                                            self.showVerifyMobileMessage(title: "Mobile Verification", message: mobile, segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.BookingView)
                                            // self.showVerifyMobileMessage(message: mobile, fromView: FromView.BookingView)
                                        })
                                    }else if let email = message.object(forKey: Keys.KEY_EMAIL) as? String {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                                            self.vw_activity_indicator.isHidden = true
                                            self.activityIndicator.stopAnimating()
                                            // self.didShowAlert(title: "", message: email)
                                            self.showVerifyMobileMessage(title: "Email Verification", message: email, segue: StoryboardSegueIDS.ID_VERIFY_EMAIL, tag: 2, fromView: FromView.BookingView)
                                        })
                                    }
                                }
                            }
                            
                            print("performing error handling book consultation:\(resultJSON)")
                        }
                        else {
                            print("Book appointment result:\(resultJSON)")
                            
                            let code = resultJSON.object(forKey: Keys.KEY_CODE) as! Int
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            print("Status=\(status) code:\(code)")
                            let data = resultJSON.object(forKey: Keys.KEY_DATA) as! NSDictionary
                            
                            if code == 201 && status == "success"
                            {
                                //print("Success booking appointment:\(data)")
                                var patientName = ""
                                
                                let bookedFor = data.object(forKey: Keys.KEY_BOOKED_FOR_USER_ID) as? Int
                                let callType =  data.object(forKey: Keys.KEY_CALL_TYPE) as! Int
                                //let userid = data.object(forKey: Keys.KEY_USER_ID) as? Int
                                let scheduled =  data.object(forKey: Keys.KEY_SCHEDULED_AT) as? String
                                let id = data.object(forKey: Keys.KEY_APPOINTMENT_ID) as? Int
                                if let notes =  data.object(forKey: Keys.KEY_NOTES) as? String  {
                                    App.consultationBookingNotes = notes
                                }
                                
                                if let patientData =  data.object(forKey: Keys.KEY_PATIENT) as? NSDictionary {
                                    if let pFullName = patientData.object(forKey: Keys.KEY_FULLNAME) as? String {
                                        patientName = pFullName
                                    }
                                }
                                
                                print("Book---\(bookedFor)  \(callType) \(scheduled)")
                                self.dataModal = BookingSuccess(id: id!, userid: self.consultation_booked_for, notes:  App.consultationBookingNotes, bookedfor: bookedFor!, calltype: Int(callType), scheduled: scheduled!,patientName:patientName)
                                
                                
                                if let doctor = data.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary {
                                    if let id = doctor.object(forKey: Keys.KEY_ID) as? Int {
                                        if let fristName = doctor.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                            if let middle = doctor.object(forKey: Keys.KEY_MIDDLE_NAME) as? String {
                                                if let last = doctor.object(forKey: Keys.KEY_LAST_NAME) as? String {
                                                    if let practnum = doctor.object(forKey: Keys.KEY_PRACTITIONER_NUMBER) as? Int {
                                                        if let avatar = doctor.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                            if let prefix = doctor.object(forKey: Keys.KEY_PREFIX) as? String {
                                                                if let specialisation = doctor.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                                    if let fullName = doctor.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                                        if let ratings = doctor.object(forKey: Keys.KEY_RATINGS) as? Double {
                                                                            self.doctorModal = Doctor(prefix:prefix,id: id, first_name: fristName, middle_name: middle, last_name: last, practitioner_number: practnum, avatar_url: avatar,fullname:fullName,specialization:specialisation,rating:ratings)
                                                                            print("prefix==>>\(fristName) id:\(id) ava")
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    UserDefaults.standard.removeObject(forKey: "BookingNotification")
                                    //   self.vw_activity_indicator.isHidden = true
                                    //   self.activityIndicator.stopAnimating()
                                    App.consultationBookingNotes = ""
                                    App.consultationDate = ""
                                    selectedConvertedDateFeomslot = ""
                                    App.selectedSlotTime = ""
                                    self.performSegue(withIdentifier: StoryboardSegueIDS.ID_BOOKING_SUCCESS, sender: self)
                                })
                            }
                            else if code == 202 &&  status == "success"
                            {
                                if let jobID = data.object(forKey: Keys.KEY_JOB_ID) as? String {
                                    self.bookingJobId = jobID
                                    UserDefaults.standard.set(jobID, forKey: "GeneratedJobID")
                                    // self.checkDBForAppointmentStatus()
                                    DispatchQueue.main.async {
                                        self.runAppointmentCheckTimer()
                                    }
                                }
                                print("Booking waiting job id :\( self.bookingJobId)")
                                DispatchQueue.main.async(execute: {
                                    self.vw_activity_indicator.isHidden = false
                                    self.activityIndicator.startAnimating()
                                })
                            }
                            else
                            {
                                // have to handle errors
                                let errors = data.object(forKey: "errors") as! NSDictionary
                                print("Error***=>\(errors)")
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                                DispatchQueue.main.async(execute: {
                                    self.vw_activity_indicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
                                })
                            }
                        }
                    }catch let error{
                        DispatchQueue.main.async(execute: {
                            self.vw_activity_indicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                        })
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        //                        DispatchQueue.main.async(execute: {
                        //                            self.didShowAlert(title: "", message: error.localizedDescription)
                        //                        })
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
        }else {
            print("User not logged in")
        }
    }
    
    /**
     Unwind segue action
     */
    @IBAction func unwindToBookingView(segue:UIStoryboardSegue) {
        
    }
    
    
    @IBAction func checkFollowUpConsultation(_ sender: UIButton) {
        print("clicked at \(sender.tag)")
        
        if sender.tag == 111{
            updateFollowUpPop(isFollowUp: "radiochecker", secondFollowUp: "radiounchecker", isNewConsult: "radiounchecker")
            followUpStatus = 1
            consultationType = "1"
        }else if sender.tag == 222{
            updateFollowUpPop(isFollowUp: "radiounchecker", secondFollowUp: "radiochecker", isNewConsult: "radiounchecker")
            followUpStatus = 2
            consultationType = "0"
            followUpApntId = "0"
            followUpReason = "second_opinion"
        }else{
            updateFollowUpPop(isFollowUp: "radiounchecker", secondFollowUp: "radiounchecker", isNewConsult: "radiochecker")
            followUpStatus = 3
            consultationType = "0"
            followUpApntId = "0"
            followUpReason = ""
        }
    }
    
    func updateFollowUpPop(isFollowUp: String,secondFollowUp: String,isNewConsult: String){
        self.iv_isFollowUpConsult.image = UIImage(named: isFollowUp)?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        self.iv_isSecOpinion.image = UIImage(named: secondFollowUp)?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        self.iv_isNewConsult.image = UIImage(named: isNewConsult)?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
    }
    
    
    
    @IBAction func closeFollowUpConsulPopup(_ sender: UIButton) {
        self.container.removeFromSuperview()
        self.vw_followUpOuterView.removeFromSuperview()
    }
    
    
    
    @IBAction func bookConsultationTapped(_ sender: UIButton) {
        print("Book tapped")
        
        self.hideDropDown()
        if self.bookingType == BookingConsultation.BOOKING_TYPE_WAIT && !self.waitingSlotsStatus {
            self.didShowAlert(title: "Not Available", message: "Instant booking not available for the day..")
            return
        }
        
        if self.booked_for_user_id.isEmpty == true && self.consultation_booked_for == BookingConsultation.CONSULTATION_FAMILY  {
            self.didShowAlert(title: "", message: "Please select a family member")
            return
        }
        
        if self.bookingType == BookingConsultation.BOOKING_TYPE_SCHEDULE && scheduled_at.isEmpty == true {
            self.didShowAlert(title: "", message: "Please select date and time for consultation")
            return
        }
        
        if !App.isMobileVerified && self.selectedCallChannel == BookingConsultation.REGULAR_CALL {
            print("**Not verified")
            App.isFromView = FromView.BookingView
            // showVerifyMobileMessage(message: "", fromView: FromView.BookingView)
            self.showVerifyMobileMessage(title: "Mobile Verification", message: "", segue: StoryboardSegueIDS.ID_MOBILE_VERIFICATION, tag: 1, fromView: FromView.BookingView)
            return
        }
        
        
        if App.bookingAttachedImages.count > 5 {//App.ImagesArray.count > 5 {
//            let fileCount = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.FILE_ATTACHMENT_LIMIT).stringValue ?? ""
            let fileCount = ""

            self.didShowAlert(title: "Sorry!", message: "Maximum \(fileCount) files allowed")
            return
        }
        
        if self.scheduled_at.isEmpty == true {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let result = formatter.string(from: date)
            App.consultationDate = result
            print("BC local date:\(result)")
            
        }
        else
        {
            App.consultationDate = self.scheduled_at
        }
        
        //        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONSENT_FORM, sender: self)
        
        if !consentAgreeStatus{
            self.didShowAlert(title: "Alert!", message: "Please agree terms and conditions..")
            return
        }
        
        followUpStatus = 0
        
//        let checkImg = UIImage(named: "radiochecker")?.withRenderingMode(.alwaysTemplate)
//        let unCheckimg = UIImage(named: "radiounchecker")?.withRenderingMode(.alwaysTemplate)
        
        self.iv_isFollowUpConsult.image = UIImage(named: "radiounchecker")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        self.iv_isSecOpinion.image = UIImage(named: "radiounchecker")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        self.iv_isNewConsult.image = UIImage(named: "radiounchecker")?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        
        
        container.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        container.backgroundColor = .black
        container.alpha = 0.7
        self.view.addSubview(container)
        vw_followUpOuterView.frame = self.view.bounds
        self.view.addSubview(vw_followUpOuterView)
    }
    
    @IBAction func checkFollowUpStatus(_ sender: UIButton) {
        if followUpStatus != 0{
            self.container.removeFromSuperview()
            self.vw_followUpOuterView.removeFromSuperview()
            if followUpStatus == 1{
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_PASTAPPOINTMENT, sender: nil)
            }else{
//                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONSENT_FORM, sender: self)  //b4 booking via consent form screen
                
                self.bookConsultation()  //now directly through this screen with enabling terms and conditions
            }
        }else{
            self.didShowAlert(title: "Alert!!", message: "Please choose consultation type..")
        }
    }
    
    @IBAction func notesDeleteTapped(_ sender: UIButton) {
        self.notesTextView.isHidden = true
        self.notesTextView.text = ""
        App.consultationBookingNotes = ""
        self.tv_notesDeleteButtonOutlet.isHidden = true
    }
    
    @IBAction func attachPhotosOrNotesTapped(_ sender: UIButton) {
        self.hideDropDown()
        
        if App.bookingAttachedImages.count >= 5 {//App.ImagesArray.count >= 5 {
//            let fileCount = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.FILE_ATTACHMENT_LIMIT).stringValue ?? ""
            let fileCount = ""

            self.didShowAlert(title: "Sorry!", message: "Maximum \(fileCount) files allowed")
            return
        }
        
        
        if sender.tag == 1 {
            let fileSize = App.FileSize
            
            let alert = UIAlertController(title: "Options", message: String(format: "Max %.0f MB / File", fileSize), preferredStyle: UIAlertController.Style.actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (UIAlertAction) in
                
                if !self.checkCameraPermission()
                {
                    //self.didShowAlert(title: "Access denied!", message: "Please provide access for the camera to take picture")
                    self.didShowAlertForDeniedPermissions( message: "Please provide access for the camera to upload pictures")
                    return
                }
                
                if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                {
                    self.picker.sourceType = UIImagePickerController.SourceType.camera
                    self.present(self.picker, animated: true, completion: nil)
                }
            }
            
            let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { (UIAlertAction) in
                
                if !self.checkLibraryPermission()
                {
                    // self.didShowAlert(title: "Access denied!", message: "Please provide access to upload pictures from your Gallery")
                    self.didShowAlertForDeniedPermissions(message: "Please provide access to upload pictures from your Gallery")
                    return
                }
                
                
                let imgPickerView = BSImagePickerViewController()
//                let mainCount = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.FILE_ATTACHMENT_LIMIT).numberValue?.intValue ?? 5
                let mainCount = 5
                if App.bookingAttachedImages.count > 0 && App.bookingAttachedImages.count < mainCount{
                    let imageCount = mainCount - App.bookingAttachedImages.count
                    imgPickerView.maxNumberOfSelections = imageCount
                }else {
                    imgPickerView.maxNumberOfSelections = mainCount
                }
                
                
                self.bs_presentImagePickerController(imgPickerView, animated: true,
                                                     select: { (asset: PHAsset) -> Void in
                                                        print("Selected: \(asset)")
                                                        
                }, deselect: { (asset: PHAsset) -> Void in
                    
                    print("Deselected: \(asset)")
                    
                }, cancel: { (assets: [PHAsset]) -> Void in
                    print("Cancel: \(assets)")
                    
                }, finish: { (assets: [PHAsset]) -> Void in
                    
                    print("Finish: \(assets)")
                    //App.ImagesArray.removeAll()
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .opportunistic
                    options.isSynchronous = true
                    options.resizeMode = .fast
                    let imageManager = PHCachingImageManager()
                    
                    self.startAnimating()
                    for asset in assets {
                        imageManager.requestImage(for: asset, targetSize: CGSize(width:CGFloat(640), height:CGFloat(480)), contentMode: .aspectFill, options: options, resultHandler: { (resultThumbnail , _) in
                            
                            if let thumbNail = resultThumbnail {
                                //  App.ImagesArray.append(thumbNail)
                                let picData = Picture(pic: thumbNail, picCaption: "", type: .image)
                                picData.isNewUpload = true
                                App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                            }
                        })
                        
                    }
                    
                    DispatchQueue.main.async(execute: {
                        print("Images array : \( App.ImagesArray)")
                        self.imagesCollectionView.reloadData()
                        self.adjustViewPhotos()
                        self.stopAnimating()
                    })
                    
                }, completion: nil)
            }
            
            let fileAction = UIAlertAction(title: "Pick a File", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.openDocumentPicker()
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            
            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(galleryAction)
            alert.addAction(fileAction)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
            
        }else {
            //  App.consultationBookingNotes = self.notesTextView.text! as String
            let storyBaord = UIStoryboard(name: "Main", bundle: nil)
            let addNotesVC = storyBaord.instantiateViewController(withIdentifier: "AddNotes")
            let popUp = BIZPopupViewController(contentViewController: addNotesVC, contentSize: CGSize(width: self.view.frame.width - 20, height: 250))
            self.present(popUp!, animated: true, completion: nil)
            
            //            let alert = UIAlertController(title: "Add Notes", message: "", preferredStyle: .alert)
            //
            //            alert.addTextField { (textField) in
            //                textField.placeholder = "Entere here.."
            //            }
            //
            //            alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak alert] (_) in
            //                let textField = alert?.textFields![0]
            //                print("Text field: \(textField?.text)")
            //                if textField?.text?.isEmpty == true {
            //                    self.notesTextView.isHidden = true
            //                    self.tv_notesDeleteButtonOutlet.isHidden = true
            //                }else {
            //                    self.notesTextView.isHidden = false
            //                    self.tv_notesDeleteButtonOutlet.isHidden = false
            //                    self.notesTextView.text = textField?.text!
            //                    App.consultationBookingNotes = (textField?.text!)!
            //                }
            //
            //            }))
            //
            //            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
            //            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func showTopMessage(message:String,duration:SwiftMessages.Duration) {
        let view = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
        view.configureTheme(.info)
        view.configureDropShadow()
        view.configureContent(title: "", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Dismiss") { (UIButton) in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = duration
        SwiftMessages.show(config: config, view: view)
    }
    
    /**
     collection view height setup after loading photos
     */
    func adjustViewPhotos() {
        DispatchQueue.main.async(execute: {
            if  App.bookingAttachedImages.count ==  0 {
                self.imagesCollectionView.isHidden = true
                self.imagesCVHeightConstraint.constant = 0
            }else {
                self.imagesCollectionView.isHidden = false
                self.imagesCollectionView.reloadData()
                self.imagesCVHeightConstraint.constant = self.imagesCollectionView.contentSize.height
            }
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        })
    }
    
    /**
     Deletes selected photo at index path
     */
    @objc func deletePhoto(sender:UIButton) {
        print("Delete tapped\(sender.tag) image:\( App.ImagesArray[sender.tag])")
        App.bookingAttachedImages.remove(at: sender.tag)
        if  App.bookingAttachedImages.count ==  0 {
            
        }else {
            
        }
        adjustViewPhotos()
    }
    
    /**
     Used to get the label text width
     - Parameter text: The text string to get width
     - Parameter font : Takes the font of text
     */
    
    func textWidth(text: String, font: UIFont?) -> CGFloat {
        let attributes = font != nil ? [NSAttributedString.Key.font: font!] : [:]
        return text.size(withAttributes: attributes).width
    }
    
    
    //MARK: - Document picker controller
    func openDocumentPicker(){ //doc, docx, xls, xlsx, pdf
        let docs = String(kUTTypeCompositeContent)
        let presentation = String(kUTTypePresentation)
        let spreadSheet = String(kUTTypeSpreadsheet)//kUTTypeSpreadsheet)
        let pdf = String(kUTTypePDF)
        let microsftDoc = "com.microsoft.word.doc"
        let microsftDocx = "org.openxmlformats.wordprocessingml.document"
        let microsftxls = "com.microsoft.excel.xls"
        let gsheet = "com.google.gsheet"
        //"public.item","public.data" ,"public.content"
        let importMenu = UIDocumentPickerViewController(documentTypes: [pdf,presentation,spreadSheet,docs,microsftDoc,microsftDocx,microsftxls,gsheet], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegueIDS.ID_BOOKING_SUCCESS {
            if let dataModel = self.dataModal , let doctorModel = self.doctorModal ,
                let destVC = segue.destination as? BookingSuccessViewController {
                destVC.dataModal = dataModel
                destVC.bookingID = dataModel.id
                destVC.doctorModal = doctorModel
                destVC.selectedTimeSlot = self.scheduled_at
                self.isAppointmentBooked = true
            }
        }else if segue.identifier == StoryboardSegueIDS.ID_SLOT_BOOKING {
            let destVC = segue.destination as! AppointmentSlotBookingViewController
            if App.timeSlots.count > 0 {
                destVC.index = 0
            }
        }else if segue.identifier == StoryboardSegueIDS.ID_LANGUAGES {
            let destVC = segue.destination as! UINavigationController
            let langVC = destVC.topViewController as! LanguagesViewController
            langVC.delegate = self
        }
//        else  if segue.identifier == StoryboardSegueIDS.ID_APP_IMAG_VIEW {
//            if let destVC = segue.destination as? ImageCrouselViewController {
//                destVC.selectedImageIndex = self.selectedImageIndex
//                destVC.isNewUpload = true
//                destVC.isFromSummaryPage = false
//            }
//        }else  if segue.identifier == StoryboardSegueIDS.ID_CONSENT_FORM {
//            if let destVC = segue.destination as? ConsentFormViewController {
//                destVC.delegate = self
//                destVC.selectedUser = self.selectedFamilyMember
//                destVC.isConsultationForFamily = self.isConsultationForFamily
//            }
//        }else  if segue.identifier == StoryboardSegueIDS.ID_SUBSCRIPTION_PLANS {
//            if let destVC = segue.destination as? SubscriptionPlansViewController {
//                destVC.previousActionDelegate = self
//            }
//        }else if segue.identifier == StoryboardSegueIDS.ID_VERIFY_EMAIL {
//            let destVC = segue.destination as! EmailVerificationViewController
//            destVC.delegate = self
//        }
        else if segue.identifier == StoryboardSegueIDS.ID_PASTAPPOINTMENT{
            if let destVC = segue.destination as? PastAppointmentVC {
                destVC.bookAppointmentRef = self
                destVC.selectedUser = self.selectedFamilyMember
                destVC.isConsultationForFamily = self.isConsultationForFamily
            }
        }
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "BookConsultationNotificationRecieved"), object: nil)
    }
    
    
}

//extension BookConsultationViewController : EmailVerificationDelegate {
//    func didVerifyEmail(_ email: String, _ success: Bool) {
//        if success {
//            self.navigationController?.popViewController(animated: true)
//        }
//    }
//}

//extension BookConsultationViewController : ContinueUserPreviousActionsDelegate {
//    func doUserPreviousActionIfFinishedSubscription() {
//        self.navigationController?.popViewController(animated: true)
//        self.bookConsultation()
//    }
//}

extension BookConsultationViewController : ConsentFormDelegate {
    func didUserAcceptedPolicyOnFollowUp(accept: Bool, appointmentId: String, followUpReason: String) {
        //        self.navigationController?.popViewController(animated: true)
        for vc in (self.navigationController?.viewControllers)!{
            if let bookVC = vc as? BookConsultationViewController{
                self.navigationController?.popToViewController(bookVC, animated: true)
                break
            }
        }
        if accept{
            self.followUpApntId = appointmentId
            self.followUpReason = followUpReason
            print(self.followUpApntId)
            print("apnt id \(self.followUpApntId) reason \(self.followUpReason)")
            self.bookConsultation()
        }
    }

    func didUserAcceptPolicy(accept: Bool) {
        self.navigationController?.popViewController(animated: true)
        if accept {
            self.bookConsultation()
        }
    }
}

extension BookConsultationViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        //self.iv_profilePic.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if App.bookingAttachedImages.count >= 5 { //App.ImagesArray.count > 5 {
            //   self.didShowAlert(title: "Sorry", message: "Only 5 images can be added")
            //  AlertView.sharedInsance.showInfo(title: "Sorry!", message: "Only 5 images can be added")
        }else {
            
            let imgData: NSData = NSData(data: ( info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage).jpegData(compressionQuality: 0.4)!)
            let imageSize: Int = imgData.length
            _ = Double(imageSize) / 1024.0 / 1024.0
            print("size of image in MB: %f ", Double(imageSize) / 1024.0 / 1024.0)
            
            // if imageSizeInMB > 5 {
            //    self.didShowAlert(title: "", message: "Image size should be less than 5MB")
            // AlertView.sharedInsance.showInfo(title: "", message: "Image size should be less than 5MB")
            // }else
            // {
            App.ImagesArray.append(info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage)
            let picData = Picture(pic: info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage, picCaption: "", type: .image)
            picData.isNewUpload = true
            
            App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
            // }
        }
        self.imagesCollectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker canceled")
        self.dismiss(animated: true, completion: nil)
    }
    
}

//extension BookConsultationViewController : UIPickerViewDelegate ,UIPickerViewDataSource {
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 2
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//
//    }
//
//}

extension BookConsultationViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5) {
            self.LC_dropDownViewHeight.constant = 0
            self.dropDownTableView.isHidden = true
            self.dropDownShadowView.isHidden = true
            self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            self.view.resignFirstResponder()
        }
    }
}

extension BookConsultationViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.familyMemrs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TV_CV_CELL_IDENTIFIERS.DropDown, for: indexPath) as! DropDownTableViewCell
        
        let user = self.familyMemrs[indexPath.row]
        
        cell.lb_name.text = user.name!
        
        cell.iv_profilePic.layer.cornerRadius = cell.iv_profilePic.frame.size.width / 2
        cell.iv_profilePic.clipsToBounds = true
        
        if user.avatar!.isEmpty {
            cell.iv_profilePic.image = UIImage(named:"place_holder_circle")
        }else if user.avatar!.lowercased().isEqual("addfamily"){
            cell.iv_profilePic.image = UIImage(named:"AddFamily")
        }else {
            let url = URL(string:user.avatar!)
            cell.iv_profilePic.kf.setImage(with: url)
        }
        
        cell.backgroundColor = UIColor.white
        
        switch self.selectedUserIndex {
        case indexPath.row:
            cell.backgroundColor = Theme.buttonBackgroundColor
            break
        case indexPath.row:
            cell.backgroundColor = Theme.buttonBackgroundColor
            break
        case indexPath.row:
            cell.backgroundColor = Theme.buttonBackgroundColor
            break
        default:
            cell.backgroundColor = UIColor.white
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = self.familyMemrs[indexPath.row]
        self.selectedUserIndex = indexPath.row
        if user.name!.lowercased().isEqual("add new") {
            self.selectedUserIndex = 0
            
            if self.familyMem.count == 5 {
                self.didShowAlert(title: "", message    : "You can't add more than 3 family members")
            }else {
                self.performSegue(withIdentifier: StoryboardSegueIDS.ID_ADD_FAMILY_MEM, sender: self)
            }
        }else {
            
            if user.userType.lowercased().isEqual("family") {
                self.lb_consultant.text = user.name!
                self.booked_for_user_id = user.id != nil ? "\(user.id!)" : ""
                self.consultation_booked_for = BookingConsultation.CONSULTATION_FAMILY
                App.isDiagnostics = false
                App.isConsultationForFamily = true
                App.selectedUserForBooking = user.name!
                self.isConsultationForFamily = true
                self.selectedFamilyMember = user
            }else {
                self.lb_consultant.text = user.name! + "(myself)"
                App.selectedUserForBooking = user.name! + "(myself)"
                App.isDiagnostics = false
                App.isConsultationForFamily = false
                self.lb_family_mem_name.isHidden = true
                self.booked_for_user_id = ""
                self.consultation_booked_for = BookingConsultation.CONSULTATION_SELF
                self.isConsultationForFamily = false
            }
            
            if user.avatar!.isEmpty {
                self.iv_profilePicOfSelectedUser.image = UIImage(named:"place_holder_circle")
            }else if user.avatar!.lowercased().isEqual("addfamily"){
                
            }else {
                let url = URL(string:user.avatar!)
                self.iv_profilePicOfSelectedUser.kf.setImage(with: url)
            }
            
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.iv_expand_arrow.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)
            self.dropDownTableView.isHidden = true
            self.dropDownShadowView.isHidden = true
            self.LC_dropDownViewHeight.constant = 0
        })
    }
    
}

extension BookConsultationViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.languagesCollectionView {
            return App.selectedLanguages.count
        }
        return App.bookingAttachedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //        var cell : ImagesCollectionViewCell!
        //
        //        if collectionView == self.languagesCollectionView && collectionView.tag == 2 {
        //             cell = collectionView.dequeueReusableCell(withReuseIdentifier: TV_CV_CELL_IDENTIFIERS.LANGUAGE, for: indexPath) as! ImagesCollectionViewCell
        //            cell.lb_language.text = App.selectedLanguages[indexPath.row].lang_name!
        //            cell.lb_language.layer.cornerRadius =  10
        //            cell.lb_language.clipsToBounds = true
        //
        //            if indexPath.row == 0 {
        //                cell.lb_language.backgroundColor = UIColor.red
        //                cell.lb_language.textColor = UIColor.white
        //            }else if indexPath.row == 1 {
        //                cell.lb_language.backgroundColor = UIColor.white
        //                cell.lb_language.textColor = UIColor.black
        //            }else if indexPath.row == 2 {
        //                cell.lb_language.backgroundColor = UIColor(hexString: "#409A3C")
        //                cell.lb_language.textColor = UIColor.white
        //            }
        //
        //
        //        }else if collectionView == self.imagesCollectionView  && collectionView.tag == 1 {
        let  cell = App.bookingAttachedImages[indexPath.row].cellInstance(collectionView, cellForItemAt: indexPath)
        cell.cellDelegate = self
        cell.bt_delete.tag = indexPath.row
        cell.tf_caption.tag = indexPath.row
        
        if App.bookingAttachedImages[indexPath.row].picture?.type?.rawValue ?? 0 == FileType.file.rawValue {
            cell.iv_image.contentMode = .scaleAspectFit
        }else {
            cell.iv_image.contentMode = .scaleAspectFill
        }
        
        
        
        //            cell = collectionView.dequeueReusableCell(withReuseIdentifier: TV_CV_CELL_IDENTIFIERS.CELL, for: indexPath) as! ImagesCollectionViewCell
        //            cell.iv_image.tag = indexPath.row
        //            cell.bt_delete.tag = indexPath.row
        //            cell.iv_image.image =  App.ImagesArray[indexPath.row]
        
        //  cell.bt_delete.addTarget(self, action: #selector(BookConsultationViewController.deletePhoto(sender:)), for: .touchUpInside)
        //        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if App.bookingAttachedImages.count >= 5 { //App.ImagesArray.count == 0 {
            print("Langauge selected")
        }else {
            self.selectedImage = App.bookingAttachedImages[indexPath.row].picture?.pic //App.ImagesArray[indexPath.row]
        }
        self.selectedImageIndex = indexPath.row
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_APP_IMAG_VIEW, sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if collectionView == self.languagesCollectionView {
            print("Starting Index: \(sourceIndexPath.item)")
            print("Ending Index: \(destinationIndexPath.item)")
            let temp = App.selectedLanguages[sourceIndexPath.row]
            App.selectedLanguages[sourceIndexPath.row] = App.selectedLanguages[destinationIndexPath.row]
            App.selectedLanguages[destinationIndexPath.row] = temp
            self.languagesCollectionView.reloadData()
        }
    }
}

extension BookConsultationViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        if collectionView ==  self.imagesCollectionView {
        //            let padding: CGFloat = 25
        //            let collectionCellSize = collectionView.frame.size.width - padding
        //            return CGSize(width: collectionCellSize/3, height: 104)
        //        }
        //
        //        let language = App.selectedLanguages[indexPath.row].lang_name!
        //        let width = textWidth(text: language, font: nil)
        return CGSize(width: collectionView.frame.width, height: 80) //previous w:width+ 10 ,h:21
    }
}

extension BookConsultationViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.lb_addNotes.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.lb_addNotes.isHidden = false
        }
    }
}


extension BookConsultationViewController : ImagesCollectionViewDelegate {
    func didTapVieFile(at: Int) {
        
    }
    
    func didTapRemoveImge(at: Int) {
        //delete items at selected index
        App.bookingAttachedImages.remove(at:at)
        self.imagesCollectionView.reloadData()
    }
}

extension BookConsultationViewController : UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result1 : \(myURL.absoluteString)")
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if  let myURL = urls.first {
            print("Caonverte file size:\(self.sizePerMB(url:myURL))")
            if self.sizePerMB(url:myURL) > App.FileSize{
                DispatchQueue.main.async {
                    let fileSize = App.FileSize
                    self.didShowAlert(title: "Sorry!", message: String(format: "Selected file size is more than %.0f MB", fileSize))
                }
            }else {
                _ = myURL.lastPathComponent.components(separatedBy: ".")
                DispatchQueue.main.async {
                    let picData = Picture(fileURL:myURL.absoluteString,picCaption: "" ,type: .file )
                    picData.isNewUpload = true
                    App.bookingAttachedImages.append(ImagesCollectionViewCellModal(picture: picData , index: App.bookingAttachedImages.count == 0 ? 0 : App.bookingAttachedImages.count - 1  ) )
                    self.imagesCollectionView.reloadData()
                    self.adjustViewPhotos()
                    print("import result 2: \(myURL.absoluteString) count:\(App.bookingAttachedImages.count)")
                }
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
    }
}


//class ImagesCollectionViewCellModal {
//    var picture:Picture?
//    var index:Int?
//    var cellIdentifier = TV_CV_CELL_IDENTIFIERS.BookingImage
//    var cellFileIdentifier = TV_CV_CELL_IDENTIFIERS.BookingFile
//    var isFileSizeGreaterThan25MB = false
//
//    init(picture:Picture,index:Int) {
//        self.picture = picture
//        self.index = index
//    }
//
//    func cellInstance(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> ImagesCollectionViewCell {
//        if self.picture?.type?.rawValue  == FileType.file.rawValue {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellFileIdentifier, for: indexPath) as! ImagesCollectionViewCell
//
//            cell.webViewCellSetup(vm: self)
//            print("Loaded file cell")
//            return cell
//        }else {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImagesCollectionViewCell
//            cell.cellSetup(vm: self)
//            print("Loaded image cell")
//            return cell
//        }
//
//    }
//}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

extension UISwitch {
    
    func set(width: CGFloat, height: CGFloat) {
        
        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51
        
        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth
        
        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}
