//
//  PastAppointmentVC.swift
//  DocOnline
//
//  Created by SanthoshKumar.bangalore on 26/03/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView


class PastAppointmentVC: UIViewController, NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var noAppointmentslabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var arrowRotator: UIImageView!
    
    @IBOutlet weak var lb_followupReason: UILabel!
    
    var bookAppointmentRef: ConsentFormDelegate!
    
    ///instance for on veiwload loads upcoming url
    var firstPageUrl = ""
    
    ///if appointments has next list url uses this instance
    var nextPageUrl = ""
    
    ///called when scrolling of list ends and set bool value
    var isScrollingFinished :Bool!
    
    ///Bool value to check list ended stop network request
    var isLoadingListItems:Bool = true
    
    ///instance to get total appointments count
    var totalAppListCount : Int!
    
    //status message to show when no appoinments called in tableview delegate method numberOfRowsInsection
    var noAppointmetns = "You don't have any previous appointments for Consultation!"
    
    //Instance variable for appointments array to load in tableview
    var appointmentsList = [Appointments]()
    
    ///instance for selected appointment id
    var selectedAppointment = ""
    
    var selectedAppointmentId = ""
    var selectedAppointmentTag = -1
    var followUpReason = ""
    
    var noAppointmentPoster = UILabel()
    
    
    var selectedUser:User!
    var isConsultationForFamily = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        noAppointmentPoster.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        noAppointmentPoster.numberOfLines = 0
        self.view.addSubview(noAppointmentPoster)
        ///setting scrolling false to call to set firsPageUrl instance url
        isScrollingFinished = false
        
        //initialising tableview shadow
        tableViewSetup()
        
        
        //used to load first list of upcoming appointments
        firstPageUrl = AppURLS.URL_BookAppointment + "/previous"
        
//        setupNavigationBar()
        
        //method which perform request to server to fetch notifications
        getAppointmentsList(urlStrin: firstPageUrl, tag: 2)
        
//        DispatchQueue.main.async {
//            let vwGrad = GradientView()
//            vwGrad.frame = self.toolBar.frame
//            vwGrad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            self.toolBar.insertSubview(vwGrad, at: 0)
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.linearBar.stopAnimation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setupNavigationBar() {
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationItem.title = "Past Appointments"
        //  self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        //  self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "6DBF00")
    }
    

    /**
     Method used to setup table view with shadow
     */
    func tableViewSetup() {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.reloadData()
        tableView.layer.shadowOpacity = 0.3
        tableView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        tableView.layer.shadowRadius = 2.0
        tableView.layer.shadowColor = UIColor.darkGray.cgColor
        tableView.layer.cornerRadius = 5
    }
    
    
    /**
     Method performs request with url for appointments fetching
     - Parameter urlStrin: Pass the url string of appointments.
     - Parameter tag: to know previous or upcoming appointments type
     */
    func getAppointmentsList(urlStrin:String , tag:Int) {
        
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error!", message:AlertMessages.NETWORK_ERROR)
            return
        }
        
        if isLoadingListItems == false
        {
            //self.didShowAlert(title: "Sorry", message: "List ended total count is\(self.appointmentsList.count)")
            print("AppointmentList ended total count is\(self.appointmentsList.count)")
        }
        else
        {
            print("commitng")
            var productListUrl = ""
            
            if isScrollingFinished == false
            {
                productListUrl = firstPageUrl
                print("Products list url-->:\(productListUrl)")
            }
            else
            {
                productListUrl =   nextPageUrl //appointmentsList[0].next_page_url!
                print("Products list url-->:\(productListUrl)")
                
                if productListUrl.isEmpty == true {
                    print("No next page url")
                    print("Total appointments count:==> \(self.appointmentsList.count)")
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.tableView.reloadData()
                    })
                    return
                }
            }
            
            startAnimating()
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let url = URL(string: productListUrl)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.httpMethod = HTTPMethods.GET
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        self.tableView.reloadData()
                        self.didShowAlert(title: "Oops..", message: (error?.localizedDescription)!)
                        // AlertView.sharedInsance.showFailureAlert(title: "", message: (error?.localizedDescription)!)
                        
                    })
                    print("Error while fetching data\(String(describing: error?.localizedDescription))")
                }
                else
                {
                    if let response = response
                    {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: jsonData)
                            if !errorStatus {
                                print("performing error handling get appointments list:\(jsonData)")
                            }
                            else {
                                let code = jsonData.object(forKey: Keys.KEY_CODE) as! Int
                                let responseStatus = jsonData.object(forKey: Keys.KEY_STATUS) as! String
                                print("Status=\(responseStatus) code:\(code)")
                                let data = jsonData.object(forKey: Keys.KEY_DATA) as! NSDictionary
                                if code == 200 && responseStatus == "success"
                                {
                                    var nxtPage = ""
                                    var from = 0
                                    var to = 0
                                    var doctorName = ""
                                    var doctorSpecialisation = ""
                                    var doctorAvatar = ""
                                    var patientName = ""
                                    var patientGender = ""
                                    var patientAvatar = ""
                                    var age = ""
                                    
                                    let currentPage = data.object(forKey: Keys.KEY_CURRENT_PAGE) as! Int
                                    let total = data.object(forKey: Keys.KEY_TOTAL) as! Int
                                    let last_page = data.object(forKey: Keys.KEY_LAST_PAGE) as! Int
                                    if let nepageurl = data.object(forKey: Keys.KEY_NEXT_PAGE_URL) as? String {
                                        nxtPage = nepageurl
                                        self.nextPageUrl = nepageurl
                                        print("Next page url ==> \(nepageurl)")
                                    }else {
                                        self.nextPageUrl = ""
                                    }
                                    
                                    
                                    if let frm = data.object(forKey: Keys.KEY_FROM) as? Int {
                                        from = frm
                                    }
                                    if let too = data.object(forKey: Keys.KEY_TO) as? Int {
                                        to = too
                                    }
                                    print("** total:\(total) from:\(from) to:\(to)")
                                    
                                    if let appData = data.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                        if self.appointmentsList.count == total
                                        {
                                            print("List ended")
                                            self.isLoadingListItems = false
                                        }
                                        else
                                        {
                                            self.isLoadingListItems = true
                                            
                                            if tag == 1 {
                                                App.tempAppointments.removeAll()
                                            }
                                            
                                            for singleData in appData {
                                                
                                                if let patient = singleData.object(forKey: Keys.KEY_PATIENT) as? NSDictionary {
                                                    if let pname = patient.object(forKey: Keys.KEY_FIRST_NAME) as? String {
                                                        patientName = pname
                                                    }
                                                    if let pavatar = patient.object(forKey: Keys.KEY_AVATAR_URL) as? String {
                                                        patientAvatar = pavatar
                                                    }
                                                    if let pdob = patient.object(forKey: Keys.KEY_DOB) as? String {
                                                        print("D.O.B::\(pdob)")
                                                        let pdobDate = self.stringToDateConverterWithFormat(date: pdob, format: "yyyy-MM-dd")
                                                        let page = self.calculate_age(date: pdobDate)
                                                        age = "\(page)"
                                                    }
                                                    if let pgender = patient.object(forKey: Keys.KEY_GENDER) as? String {
                                                        if pgender.lowercased().isEqual("male") {
                                                            patientGender = "M"
                                                        }else if pgender.lowercased().isEqual("female") {
                                                            patientGender = "F"
                                                        }else {
                                                            patientGender = "T"
                                                        }
                                                    }
                                                }
                                                
                                                if let doctor = singleData.object(forKey: Keys.KEY_DOCTOR) as? NSDictionary {
                                                    if let name = doctor.object(forKey: Keys.KEY_FULLNAME) as? String {
                                                        doctorName = name
                                                    }
                                                    
                                                    if let specialization = doctor.object(forKey: Keys.KEY_SEPCIALIZATION) as? String {
                                                        doctorSpecialisation = specialization
                                                    }
                                                }
                                                
                                                
                                                let id = singleData.object(forKey: Keys.KEY_ID) as! Int
                                                let bookedFor = singleData.object(forKey: Keys.KEY_BOOKED_FOR) as! Int
                                                let callType = singleData.object(forKey: Keys.KEY_CALL_TYPE) as! Int
                                                let scheduled_at = singleData.object(forKey: Keys.KEY_SCHEDULED_AT) as! String
                                                let status = singleData.object(forKey: Keys.KEY_STATUS) as! Int
                                                let userNotes = singleData.object(forKey: Keys.KEY_NOTES) as! String
                                                
                                                
                                                let appointment = Appointments(id: id, current_page: currentPage, scheduled_at: scheduled_at, calltype: callType, booked_for: bookedFor, notes: userNotes, from: from, last_page: last_page, nextpageurl: nxtPage, to: to, total: total, status: status, doctorName: doctorName, doctorAvatar: doctorAvatar, doctorSpecialisation: doctorSpecialisation, patientName: patientName, patientGender: patientGender, patientAvatar: patientAvatar,age:age)
                                                
                                                print("Is appending url:\(nxtPage)")
                                                self.appointmentsList.append(appointment)
                                                
                                                
                                                if tag == 1 {
                                                    let dateOFAppointment = self.stringToDateConverter(date: scheduled_at)
                                                    let currentTime = Date()
                                                    let totalSeconds = dateOFAppointment.timeIntervalSince(currentTime)
                                                    if  status == 1 && totalSeconds > 0  {
                                                        
                                                        App.tempAppointments.append(appointment)  //
                                                        print("Appointments for timer count:=>\(App.appointmentsList.count)")
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    print("Appointments count:\(self.appointmentsList.count)")
                                }else {
                                    print("Error while fetching data")
                                }
                            }
                            
                            DispatchQueue.main.async(execute: {
                                if tag == 1 {
                                    App.upComingAppointments = self.appointmentsList
                                }else if tag == 2 {
                                    App.previousAppointments = self.appointmentsList
                                }
                                
                                self.tableView.reloadData()
                                self.hideTableView()
                                self.stopAnimating()
                            })
                        }
                        catch let error
                        {
                            self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                            DispatchQueue.main.async(execute: {
                                self.stopAnimating()
                                self.tableView.reloadData()
                            })
                            print("Received not-well-formatted JSON=>\(error.localizedDescription)")
                        }
                        
                    }
                }
                
            }).resume()
        }
    }
    
    
    /**
     Method checks if appointmets count and according to that shows or hides tableview
     */
    func hideTableView() {
        if appointmentsList.count == 0 {
            self.tableView.isHidden = true
            self.noAppointmentPoster.backgroundColor = .white
            
            self.noAppointmentPoster.isHidden = false
            self.noAppointmentPoster.textAlignment = .center
            self.noAppointmentPoster.text = noAppointmetns
        }else {
            self.noAppointmentPoster.isHidden = true
            self.tableView.isHidden = false
        }
    }
    
    @IBAction func chooseFollowupReason(_ sender: UIButton) {
        
        if selectedAppointmentId.isEmpty{
            self.didShowAlert(title: "Alert!!", message: "Please choose follow up appointment from the list..")
        }else{
        
        let reasons = ["Doctor insisted for a follow up consultaion","I am satisfied but still looking for a second opinion","I am dissatisfied and hence going for another consultation"]
        
        UIView.animate(withDuration: 0.5, animations: {
            self.arrowRotator.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        let actionView = UIAlertController(title: "Reason for follow up?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        for reason in reasons {
            let  performAction = UIAlertAction(title: reason, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
//                self.tf_suffix.text = value as? String
//                self.selectedPrefix = key as! String
                self.lb_followupReason.text = reason
                self.followUpReason = reason
                UIView.animate(withDuration: 0.5, animations: {
                    self.arrowRotator.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) *    180.0)
                })
            })
            
            actionView.addAction(performAction)
        }
        
        let  performCancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            UIView.animate(withDuration: 0.5, animations: {
                self.arrowRotator.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) *    180.0)
            })
        })
        
        actionView.addAction(performCancelAction)
        self.present(actionView, animated: true, completion: nil)
            
            UILabel.appearance(whenContainedInInstancesOf:
                [UIAlertController.self]).numberOfLines = 0
            
            UILabel.appearance(whenContainedInInstancesOf:
                [UIAlertController.self]).lineBreakMode = .byWordWrapping
        }
    }
    
    
    @IBAction func onFollowUpAppointment(_ sender: UIButton) {
        if !selectedAppointmentId.isEmpty && !followUpReason.isEmpty{
            //self.performSegue(withIdentifier: StoryboardSegueIDS.ID_CONSENT_FORM, sender: nil)  //b4 followup consultation booking via consent form screen
            bookAppointmentRef.didUserAcceptedPolicyOnFollowUp(accept: true, appointmentId: selectedAppointmentId, followUpReason: followUpReason)
        }else if selectedAppointmentId.isEmpty && followUpReason.isEmpty{
            self.didShowAlert(title: "Alert!!", message: "Please select follow up appointment and reason..")
        }else{
            self.didShowAlert(title: "Alert!!", message: "Please select follow up reason..")
        }
    }
    
    
    @IBAction func onCancelFollowUpAppointment(_ sender: UIButton) {
        print("cancelled maga")
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let vc = segue.destination as? ConsentFormViewController{
        if let vc = segue.destination as? ConsentFormViewController{
            vc.delegate = bookAppointmentRef
            vc.appointment_id = selectedAppointmentId
            vc.followup_reason = followUpReason
            vc.selectedUser = self.selectedUser
            vc.isConsultationForFamily = self.isConsultationForFamily
        }
    }
    */

}


//MARK:- UITableViewDatasource ,UITableViewDelegate methods
extension PastAppointmentVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PastAppointmentCell
        // let values = self.appointmentsList[indexPath.row]
//        cell.bt_pastAptSelection.tag = indexPath.row
        if selectedAppointmentTag == indexPath.row{
            cell.cellSetup(app: self.appointmentsList[indexPath.row], vc: self, selectionStatus: true)
        }else{
            cell.cellSetup(app: self.appointmentsList[indexPath.row], vc: self, selectionStatus: false)
        }
//        cell.cellSetup(app: self.appointmentsList[indexPath.row], vc: self)
        //        let dateString = values.scheduled_at
        //        let convertedDate = stringToDateConverter(date: dateString!)
        //
        //        let calendar = Calendar.current
        //
        //        let month = calendar.component(.month, from: convertedDate)
        //        let day = calendar.component(.day, from: convertedDate)
        //
        //        let monthName = month.getMonthName() // self.getMonthName(day: month)
        //
        //        var patientDetails = ""
        //        if values.age.isEmpty || values.age.isEqual("0"){
        //            if let patientname = values.patientName , let patientgender = values.patientGender {
        //               patientDetails = "\(patientname) \(patientgender)"
        //            }
        //        }else {
        //            if let patientname = values.patientName , let patientgender = values.patientGender , let patientage = values.age  {
        //                patientDetails = "\(patientname) \(patientgender),\(patientage)"
        //            }
        //        }
        //
        //        //new UI
        //        cell.lb_date.text = "\(day)"
        //        cell.lb_month.text = monthName
        //        cell.lb_patientName.text = patientDetails
        //        cell.lb_doctorName.text = values.doctorName
        //        cell.lb_specialisation.text = values.doctorSpecialisation
        //        if let time = dateString?.components(separatedBy: " ") {
        //            let convertedTime = getTimeInAMPM(date: time[1])
        //            cell.lb_time.text = convertedTime
        //        }
        //
        //        cell.lb_patientPic.layer.cornerRadius = cell.lb_patientPic.frame.size.width / 2
        //        cell.lb_patientPic.clipsToBounds = true
        //        if let imgeURl = URL(string:values.patientAvatar) {
        //           cell.lb_patientPic.kf.setImage(with: imgeURl)
        //        }else {
        //            cell.lb_patientPic.image = UIImage(named:"Default-avatar")
        //        }
        //
        //        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAppointmentTag = indexPath.row
        tableView.reloadData()
        print("appt id \(self.appointmentsList[selectedAppointmentTag].id)")
        selectedAppointmentId = "\(self.appointmentsList[selectedAppointmentTag].id!)"
        
    }
    
}

//extension PastAppointmentVC: GetAppointmentDelegate{
//    func getSelectedAppointmentId(id: Int) {
//        selectedAppointmentTag = id
//        tableView.reloadData()
//        print("appt id \(self.appointmentsList[selectedAppointmentTag].id)")
//        selectedAppointmentId = "\(self.appointmentsList[selectedAppointmentTag].id!)"
//
//    }
//}






extension PastAppointmentVC : UIScrollViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        
        if scrollOffset == 0
        {
            // then we are at the top
            print("YOure reached top")
        }
        else if scrollOffset + scrollViewHeight == scrollContentSizeHeight
        {
            // then we are at the end
            print("YOure reached Down")
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        if !decelerate {
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            if bottomEdge >= scrollView.contentSize.height {
                print("Scrolling did reach top")
                isScrollingFinished = true
                getAppointmentsList(urlStrin:  "", tag: 2)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Scrolling did end decelerating method called down")
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            isScrollingFinished = true
            getAppointmentsList(urlStrin:  "", tag: 2)
        }
    }
}

