//
//  LanguagesViewController.swift
//  DocOnline
//
//  Created by Kiran Kumar on 03/11/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

protocol LanguageSelectionDelegate {
    func didFinishPickingLanguages(_ success:Bool,languages:[Languages],prefered:String)
}

class LanguagesViewController: UIViewController ,NVActivityIndicatorViewable{

    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate : LanguageSelectionDelegate?
    
    var tempTotalLanguages = [Languages]()
    
    ///Instance array to append selected languages
    var selectedLanguages = [Languages]()
    ///Not used
    var languagesSelected = ""
    
    ///Used to count how many languages selected and restrict to select more than count
    var count = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedLanguages.removeAll()
        count = 2

        //tableView.setEditing(true, animated: true)
    
//        var preferedLang = [Languages]()
//        var unPreferredLang = [Languages]()
        

//        for lang in App.totalLanguages {
//            print("Language Name:\(lang.lang_name!) is selected:\(lang.isSelected!) ")
//            if lang.preferedCount! > 0 {
//                preferedLang.append(lang)
//            }else {
//                unPreferredLang.append(lang)
//            }
//        }
//
//       let sortedPrefred =  preferedLang.sorted { $0.preferedCount! < $1.preferedCount! }
//        App.totalLanguages = sortedPrefred
//        App.totalLanguages += unPreferredLang
        
        navigationController?.navigationBar.setGradientBackground(colors: Theme.navigationGradientColor as! [UIColor])

        self.tempTotalLanguages = App.totalLanguages
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    
    ///Performs the selection and changes the radio button
    @objc func checkBoxTapped(sender: UIButton) {
        
        
        for (index,lang) in self.tempTotalLanguages.enumerated()
        {
            print(lang.lang_name ?? "")
            let indxPath = IndexPath(item: index, section: 0)
            let cell : LanguagesTableViewCell = tableView.cellForRow(at: indxPath) as! LanguagesTableViewCell
            cell.bt_check_box.tag = index
        }
        
        for (index,lang) in self.tempTotalLanguages.enumerated()
        {
            if sender.tag == index { //|| lang.lang_name!.lowercased().isEqual("english")  {
                self.tempTotalLanguages[sender.tag].isSelected = true
//                lang.isSelected! = true
//                sender.setImage(UIImage(named: "RadioButtonCheck"), for: UIControlState.normal)
            }
            else if sender.tag != index //&& !lang.lang_name!.lowercased().isEqual("english")
            {
//                let indxPath = IndexPath(item: index, section: 0)
//                let cell : LanguagesTableViewCell = tableView.cellForRow(at: indxPath) as! LanguagesTableViewCell
//                cell.bt_check_box.setImage(UIImage(named: "RadioButtonUncheck"), for: UIControlState.normal)
//                lang.isSelected! = false
                 self.tempTotalLanguages[sender.tag].isSelected = false
            }
        }
        self.tableView.reloadData()
        
       /*
        let language = App.totalLanguages[sender.tag]
        if sender.currentImage == UIImage(named: "CheckedBox") {
            sender.setImage(UIImage(named: "UncheckedBox"), for: UIControlState.normal)
            count -= 1
           language.isSelected! = false
           
        }else  if sender.currentImage == UIImage(named: "UncheckedBox") {
            
            if count == 3 {
                didShowAlert(title: "", message: "Only two languages can be selected")
                return
            }
            language.isSelected! = true
            count += 1
            sender.setImage(UIImage(named: "CheckedBox"), for: UIControlState.normal)
        }
       */
    }

    
    func doHasPreferredLanguage() {
        var preferenceArray = [Int]()
        var pcount = 0
        for (index,lang) in self.tempTotalLanguages.enumerated() {
            if lang.isSelected! { //&& !lang.lang_name!.lowercased().isEqual("english"){
                pcount += 1
                selectedLanguages.append(lang)
                print("Languages prefered are :\(lang.lang_id!) Name:\(lang.lang_name!)")
                preferenceArray.append(lang.lang_id!)
                self.tempTotalLanguages[index].preferedCount! = pcount
                self.tempTotalLanguages[index].isSelected! = true
                print("Done prefered lang ids;\(preferenceArray)")
            }else {
                self.tempTotalLanguages[index].preferedCount! = 0
                self.tempTotalLanguages[index].isSelected! = false
            }
        }
        
        let sortedPreLang = self.selectedLanguages.sorted { $0.preferedCount! < $1.preferedCount! }
        if preferenceArray.count > 0 {
            startAnimating()
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let bookURL = AppURLS.URL_Set_lang_preferences
            let url = URL(string: bookURL)
            let preferedDic = [Keys.KEY_LANGUAGE_PREFERENCES : preferenceArray]
            print("Prefered lang array:\(preferedDic)")
            var jsondata : Data?
            do {
                jsondata = try JSONSerialization.data(withJSONObject: preferedDic, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
            
            
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.httpMethod = HTTPMethods.PUT
            let postData = "{\"\(Keys.KEY_LANGUAGE_PREFERENCES)\"=\(preferenceArray)}"
            print("Data==>\(postData)")
            print("URL==>\(bookURL)")
            request.httpBody = jsondata
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        print("Error==> : \(error.localizedDescription)")
                        self.didShowAlert(title: "", message: error.localizedDescription)
                        // AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                    })
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print("Preferred Response:\(resultJSON)")
                            let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                            if !errorStatus {
                                
                            }else {
                                if httpResponse.statusCode == 200 {
                                    DispatchQueue.main.async {
                                        App.totalLanguages = self.tempTotalLanguages
                                        PersonalInfoViewController.isFromLanguages = true
                                        self.selectedLanguages = sortedPreLang
                                        App.selectedLanguages = sortedPreLang
                                        App.lang_preferences = preferenceArray
                                        let selectedLangName = sortedPreLang.first?.lang_name ?? ""
                                        App.lang_preferences_values.append(selectedLangName)
                                        self.delegate?.didFinishPickingLanguages(true, languages: self.selectedLanguages, prefered: self.languagesSelected)
                                        self.stopAnimating()
                                    }
                                }
                            }
                        }
                    }catch let error {
                        DispatchQueue.main.async {
                            self.stopAnimating()
                        }
                        print("Recieved not a well formatted json:\(error.localizedDescription)")
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                    }
                }
            }).resume()
        }
    }

    @IBAction func doneTapped(_ sender: UIBarButtonItem) {
        doHasPreferredLanguage()
    }
    
    
    @IBAction func cancelTapped(_ sender: UIBarButtonItem) {
        PersonalInfoViewController.isFromLanguages = true
        self.tempTotalLanguages = App.totalLanguages
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LanguagesViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.tempTotalLanguages.count > 0  {
            return self.tempTotalLanguages.count
        }
        
        let rect = CGRect(x: 0,
                          y: 0,
                          width: self.tableView.bounds.size.width,
                          height: self.tableView.bounds.size.height)
        
        let noDataLabel: UILabel = UILabel(frame: rect)
        
        noDataLabel.text = "No Languages found"
        noDataLabel.textColor = UIColor.gray
        noDataLabel.textAlignment = NSTextAlignment.center
        self.tableView.backgroundView = noDataLabel
        self.tableView.separatorStyle = .none
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Language", for: indexPath) as! LanguagesTableViewCell
        let lanugage = self.tempTotalLanguages[indexPath.row]
        
        cell.lb_language.text = lanugage.lang_name!
        cell.bt_check_box.tag = indexPath.row
        cell.bt_check_box.addTarget(self, action: #selector(self.checkBoxTapped(sender:)), for: UIControl.Event.touchUpInside)

        cell.bt_check_box.isEnabled = true
        if lanugage.isSelected! { //&& lanugage.preferedCount! > 0 {
//            cell.bt_check_box.isEnabled = true
            let checkImg = UIImage(named: "radiochecker")?.withRenderingMode(.alwaysTemplate)
            cell.bt_check_box.setImage(checkImg, for: UIControl.State.normal)
            cell.bt_check_box.tintColor = Theme.navigationGradientColor![0]!

            self.languagesSelected = self.tempTotalLanguages[indexPath.row].lang_name!
           // self.count += 1
        }else {
            let uncheckImg = UIImage(named: "radiounchecker")?.withRenderingMode(.alwaysTemplate)
            cell.bt_check_box.setImage(uncheckImg, for: UIControl.State.normal)
            cell.bt_check_box.tintColor = Theme.navigationGradientColor![0]!
//             cell.bt_check_box.setImage(UIImage(named: "RadioButtonUncheck"), for: UIControl.State.normal)
        }
        
//        if lanugage.lang_name!.lowercased().isEqual("english") {
//            cell.bt_check_box.setImage(UIImage(named: "RadioButtonCheck"), for: UIControlState.normal)
//            cell.bt_check_box.isEnabled = false
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  print("Is selected:\(App.totalLanguages[indexPath.row].isSelected!)")
//        if indexPath.row == 0 && App.totalLanguages[indexPath.row].readOnly! && (App.totalLanguages[indexPath.row].lang_name ?? "").lowercased().isEqual("english"){
//            print("Index path english selected")
//            let cell : LanguagesTableViewCell = tableView.cellForRow(at: indexPath) as! LanguagesTableViewCell
//            cell.contentView.backgroundColor = UIColor.white
//            return
//        }
        
        self.languagesSelected = self.tempTotalLanguages[indexPath.row].lang_name ?? ""
        
        for (index,lang) in self.tempTotalLanguages.enumerated()
        {
            print(lang.lang_name ?? "")
            let indxPath = IndexPath(item: index, section: 0)
            let cell : LanguagesTableViewCell = tableView.cellForRow(at: indxPath) as! LanguagesTableViewCell
            cell.bt_check_box.tag = index
        }

        for (index,lang) in self.tempTotalLanguages.enumerated()
        {
            if indexPath.row == index {//|| lang.lang_name!.lowercased().isEqual("english")  {
                  self.tempTotalLanguages[index].isSelected = true
//                lang.isSelected! = true
//                let cell : LanguagesTableViewCell = tableView.cellForRow(at: indexPath) as! LanguagesTableViewCell
//                cell.bt_check_box.setImage(UIImage(named: "RadioButtonCheck"), for: UIControlState.normal)
//                cell.contentView.backgroundColor = UIColor.white
            }
            else if indexPath.row != index //&& !lang.lang_name!.lowercased().isEqual("english")
            {
                 self.tempTotalLanguages[index].isSelected = false
//                let indxPath = IndexPath(item: index, section: 0)
//                let cell : LanguagesTableViewCell = tableView.cellForRow(at: indxPath) as! LanguagesTableViewCell
//                cell.bt_check_box.setImage(UIImage(named: "RadioButtonUncheck"), for: UIControlState.normal)
//                lang.isSelected! = false
//                cell.contentView.backgroundColor = UIColor.white
            }
        }
         self.tableView.reloadData()
        self.doHasPreferredLanguage()
    }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let itemToMove = self.tempTotalLanguages[sourceIndexPath.row]
        self.tempTotalLanguages.remove(at: sourceIndexPath.row)
        self.tempTotalLanguages.insert(itemToMove, at: destinationIndexPath.row)
    }
    
}





