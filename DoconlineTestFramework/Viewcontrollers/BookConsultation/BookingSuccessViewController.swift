//
//  BookingSuccessViewController.swift
//  DocOnline
//
//  Created by dev-3 on 12/06/17.
//  Copyright © 2017 ConversionBug. All rights reserved.
//

/*"booked_for_user_id" = 2;
"call_type" = 2;
"created_at" = "2017-06-13 06:20:46";
id = 9;
notes = "<null>";
"scheduled_at" = "2017-06-14 02:45:00";
"updated_at" = "2017-06-13 06:20:46";
"user_id" = 2;

*/


import UIKit
import MobileCoreServices
import UserNotifications
import SwiftMessages
//import NVActivityIndicatorView
import Alamofire

class BookingSuccessViewController: UIViewController ,NVActivityIndicatorViewable{

    
    ///New UI
    @IBOutlet var lb_doctorSpecialization: UILabel!
    @IBOutlet var rv_doctorRating: CosmosView!
    @IBOutlet var lb_ratingAvaerage: UILabel!
    @IBOutlet var lb_patientName: UILabel!
    @IBOutlet weak var rightImg: UIImageView!
    
    @IBOutlet var vw_addToCalendarButton: UIView!
    
    @IBOutlet weak var lb_consultationFor: UILabel!
    
    @IBOutlet weak var lb_typeOdCall: UILabel!
    @IBOutlet weak var lb_scheduledOn: UILabel!
    @IBOutlet weak var lb_notes: UILabel!
    
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var imagesCVHeightConstraint: NSLayoutConstraint!
    
  
    @IBOutlet var lb_uploadStatus: UILabel!
    
    @IBOutlet weak var vw_blank: UIView!
    @IBOutlet weak var drProfilePic: UIImageView!
    @IBOutlet weak var lb_drName: UILabel!
    
    @IBOutlet weak var bt_calendar_outlet: UIButton!
    @IBOutlet weak var caledar_Height: NSLayoutConstraint!
    
    ///dataModal reference for booking success
    var dataModal : BookingSuccess!
    
    ///data modal referece for doctor
    var doctorModal : Doctor!
    ///selected image reference
    var selectedImage : UIImage!
    ///booking id reference
    var bookingID : Int!
    ///selected time slot instance variable
    var selectedTimeSlot = ""
    ///type of booking reference
    var bookingType = ""
    
    ///instance to store selected image from attachments
    var selectedAttachment = ""
    ///if images appended to appointment stores in attachments
    var attachments = [String]()
    
    var globalCountDownTimer : GlobalTimerViewController?
    
    var tempSelectedFiles = [ImagesCollectionViewCellModal]()
    var tempSelectedImages = [ImagesCollectionViewCellModal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.rv_doctorRating.settings.fillMode = .precise
        self.lb_patientName.textColor = Theme.buttonBackgroundColor
        self.lb_scheduledOn.textColor = Theme.buttonBackgroundColor
        self.rightImg.image = UIImage(named: "right_check" )?.tintedWithLinearGradientColors(colorsArr: [Theme.buttonBackgroundColor!.cgColor, Theme.buttonBackgroundColor!.cgColor])
        //buttonCornerRadius(buttons: [self.bt_calendar_outlet])
        self.getAppointmentDetails()
    }    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         calculateTimeToShowRemainder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vw_addToCalendarButton.backgroundColor = Theme.buttonBackgroundColor
    }
    
    func getAppointmentDetails() {
        guard let bookingId = self.bookingID else { return }
        NetworkCall.performGet(url: "\(AppURLS.URL_BookAppointment)/\(bookingId)") { (success, response, statusCode, error) in
            if success, let resp = response,
                let data = resp[Keys.KEY_DATA] as? [String:Any],
                let doctor = data[Keys.KEY_DOCTOR] as? [String:Any] ,
                let avatarUrlString = doctor[Keys.KEY_AVATAR_URL] as? String{
    
                DispatchQueue.main.async {
                    guard let avatarUrl = URL(string:avatarUrlString) else { return }
                    self.drProfilePic.kf.setImage(with: avatarUrl, placeholder: UIImage(named:"doctor_placeholder_nocircle") , options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
        }
    }
    
    /**
     Method stops animation shows success toast message
     */
    func toReloadImages() {
        print("Animation stopped**")
        DispatchQueue.main.async(execute: {
 
            App.ImagesArray.removeAll()
            
            if App.imagesURLString.count != 0 {
                self.attachments.removeAll()
                self.attachments = App.imagesURLString
                self.imagesCollectionView.reloadData()
                self.imagesCollectionView.isUserInteractionEnabled = true
            }
        })
    }
    
    
    /**
     Method shows or hides when images are uploading in background
     */
    func showOrStopUploadStatus(show:Bool,text:String,seconds:Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(seconds)) {
            self.lb_uploadStatus.isHidden = show
        }
        self.lb_uploadStatus.text = text
        if show {
            UIView.animate(withDuration: 0.8,
                           delay:0.0,
                           options:[.curveEaseInOut, .autoreverse, .repeat],
                           animations: { self.lb_uploadStatus.alpha = 0.40 },
                           completion: nil)
        }else {
            self.lb_uploadStatus.layer.removeAllAnimations()
            self.lb_uploadStatus.alpha = 1
        }
    }
    
    /**
     Method uploads images to server with parameters
     - Parameter id: appoitment id should be passed
     - Parameter tag: to know from which class it is called
     */
    func uploadSelectedImages(id:Int,tag:Int,selectedFiles:[ImagesCollectionViewCellModal],completion: @escaping ((_ success:Bool) -> Void)) {
      
        if App.bookingAttachedImages.count != 0 {// App.ImagesArray.count != 0 {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
                completion(false)
                return
            }
            
            let urlString = "\(AppURLS.URL_BookAppointment)/\(id)/attachments"
          
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let boundary = generateBoundaryString()
            print("ImageUrl = \(urlString)")
            let url = URL(string: urlString)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            request.setValue(NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API) //New header for identification
            // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethods.POST
            request.httpBody = createBodyWithParametersForCapetionedImage(parameters: selectedFiles , boundary: boundary) as Data
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        self.didShowAlert(title: "", message: error.localizedDescription)
                         completion(false)
                    }
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                             completion(false)
                            print("performing error handling uploading consultation images:\(resultJSON)")
                        }
                        else {
                            //clear images array after successfull upload **
                            if httpResponse.statusCode == 201 || httpResponse.statusCode == 200{
                                 print("Images uploaded successfully")
                                 completion(true)
                            }else {
                                DispatchQueue.main.async {
                                     completion(false)
                                    self.didShowAlert(title: "", message: "Images are not uploaded. Please try again attaching images in MyAppointments")
                                }
                            }
                            print("Images upload response:\(resultJSON)  ")
                        }
                    }catch let error{
                          completion(false)
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
            
        }else {
            print("User not logged in")
        }
    }
   
    /**
      Method updates Appointment summary UI
     */
    func setupView() {
        
        if let call_type = dataModal.call_type, let id = dataModal.id,   let bookedFor = dataModal.booked_for, let scheduledAt = dataModal.scheduled_at,let notes = dataModal.notes,let doctorName = dataModal.doctorName ,let docSpec = dataModal.doctorSpecialisation , let docAvatar = dataModal.doctorAvatar , let status = dataModal.status{
     
            let formatedDate = getFormattedDateAndTime(dateString: scheduledAt)
            self.lb_scheduledOn.text = formatedDate
            
            let appoint = Appointments(id: id, current_page: 0, scheduled_at: scheduledAt, calltype: call_type, booked_for: bookedFor, notes: notes, from: 0, last_page: 0, nextpageurl: "", to: 0, total: 0, status: status, doctorName: doctorName, doctorAvatar: docAvatar, doctorSpecialisation: docSpec, patientName: "", patientGender: "", patientAvatar: "",age:"")
            App.appointmentsList.append(appoint)
            App.tempAppointments.append(appoint)
            print("Appointments for timer count :\(App.appointmentsList.count) doctorName:\(doctorName) docSpec:\(docSpec)")
            RefreshTimer.sharedInstance.updateTimerValues()
          
          
                let sortedAppointments = App.tempAppointments.sorted(by: { (obj1, obj2) -> Bool in
                    if let firstDate = obj1.scheduled_at , let secDate = obj2.scheduled_at {
                        let convFstDate = self.stringToDateConverter(date: firstDate)  //
                        let convSecDate = self.stringToDateConverter(date: secDate)
                        return convFstDate < convSecDate
                    }
                    return false
                })
                App.tempAppointments = sortedAppointments
           
        }
        print("Booked for:\(dataModal.booked_for)")

        
        if let patientName = dataModal.patientName {
            self.lb_patientName.text = patientName
        }else {
            self.lb_patientName.text = ""
        }

        if let doctor = self.doctorModal {
            print("Doctor avatar url:\(self.doctorModal.avatar_url ?? "")")
            self.lb_drName.text = doctor.full_name
            self.drProfilePic.kf.setImage(with: URL(string:self.doctorModal.avatar_url)!, placeholder: UIImage(named:"doctor_placeholder_nocircle") , options: nil, progressBlock: nil, completionHandler: nil)
            self.lb_doctorSpecialization.text = doctor.specialisation
            if let rating = doctor.ratings {
                self.rv_doctorRating.rating = Double(rating)
                self.lb_ratingAvaerage.text = "\(rating)"
            }
            if let rating = doctor.ratings {
                self.rv_doctorRating.rating = Double(rating)
                self.lb_ratingAvaerage.text = "\(rating)"
            }else {
                self.rv_doctorRating.rating = Double(0)
                self.lb_ratingAvaerage.text = "0"
            }
        }
        
        if let bookingID = self.bookingID {
            self.uploadViaAlamofire(id: bookingID)
//            for file in App.bookingAttachedImages {
//                if file.picture?.type?.rawValue == FileType.file.rawValue {
//                    self.tempSelectedFiles.append(file)
//                }else {
//                    self.tempSelectedImages.append(file)
//                }
//            }
//
//            if !tempSelectedImages.isEmpty {
//                self.uploadSelectedImages(id: bookingID, tag: 1, selectedFiles: self.tempSelectedImages , completion: { (success) in
//                    if !self.tempSelectedFiles.isEmpty {
//                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) , execute: {
//                            self.uploadSelectedImages(id: bookingID, tag: 1, selectedFiles: self.tempSelectedFiles , completion: { (success) in
//                            })
//                        })
//                    }
//                })
//            }
        }
       
    }
    
    
    func uploadViaAlamofire(id:Int) {
        if !NetworkUtilities.isConnectedToNetwork()
        {
            self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
            // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
            return
        }
        
        if App.bookingAttachedImages.count != 0 {
            let urlString = "\(AppURLS.URL_BookAppointment)/\(id)/attachments"
            
            let headers = [
                NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION : App.getUserAccessToken(),
                NETWORK_REQUEST_KEYS.KEY_DOCONLINE_API : NETWORK_REQUEST_KEYS.KEY_VALUE_DOCONLINE,
                NETWORK_REQUEST_KEYS.KEY_ACCEPT : NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (index,file) in App.bookingAttachedImages.enumerated() {
                    if file.picture?.type?.rawValue == FileType.file.rawValue {
                        if let url = URL(string: file.picture?.fileURl ?? "") {
                            multipartFormData.append(url, withName: "attachments[\(index)]" )
                        }
                    }else if file.picture?.type?.rawValue == FileType.image.rawValue {
                        if let image = file.picture?.pic , let imageData = image.jpegData(compressionQuality: 0.4){
                            multipartFormData.append(imageData, withName: "attachments[\(index)]" ,fileName: "image\(index).jpg", mimeType: "image/jpg")
                        }
                    }
                    
                    multipartFormData.append((file.picture?.picCaption ?? "").data(using: String.Encoding.utf8)!, withName: "titles[\(index)]")
                }
            }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted) AdditionalDescr:\(progress.localizedAdditionalDescription)")
                    })
                    
                    upload.responseJSON { response in
                        print("Alamofire response : \(response.result.value) \(response.response?.statusCode)")
                        
                        if response.response?.statusCode ?? 0 == 422 ,let resp = response.result.value as? [String:Any] ,let data = resp[Keys.KEY_DATA] as? [String:Any] {
                            var errors : [String:Any] = [:]
                            if let singleError =  data[Keys.KEY_ERROR] as? [String:Any] {
                                errors = singleError
                            }
                            
                            if let multipleErrors = data[Keys.KEY_ERRORS] as? [String:Any] {
                                errors = multipleErrors
                            }
                            
                            var message = ""
                            for (_,value) in errors {
                                if let titleMsg = value as? [String] {
                                    message = titleMsg.first ?? ""
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.stopAnimating()
                                self.didShowAlert(title: "Failed to upload file(s)." , message: message)
                            }
                        }else {
                            let status = self.check_Status_Code(statusCode: response.response?.statusCode ?? 0, data: response.result.value as? NSDictionary)
                            
                            if status {
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                }
                            }else {
                                DispatchQueue.main.async {
                                    self.stopAnimating()
                                    self.didShowAlert(title: "Failed to upload file(s)", message: "Please try again attaching files in Appointment Summary")
                                }
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    DispatchQueue.main.async {
                        self.didShowAlert(title: "Failed to upload file(s)", message: "Please try again attaching files in Appointment Summary")
                    }
                }
            }
        }
    }
    
    

    /**
     Method updates collection view height setup after loading photos
     */
    func adjustViewPhotos() {
        DispatchQueue.main.async(execute: {
            if  App.ImagesArray.count ==  0 {
                self.imagesCollectionView.isHidden = true
                self.imagesCVHeightConstraint.constant = 0
            }else {
                self.imagesCollectionView.isHidden = false
                self.imagesCollectionView.reloadData()
                self.imagesCVHeightConstraint.constant = self.imagesCollectionView.contentSize.height
            }
            
            self.vw_blank.isHidden = true
           
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        })
    }
    
    /**
       Converts UTC date string to local string and date
       - Parameter dateString: takes string date format
       - Returns: date string and Date from given string
     */
    func convertDate(dateString :String) ->(String,Date) {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormator.date(from: dateString)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let localDate = dateFormator.string(from: dt!)
        return (localDate,dateFormator.date(from: localDate)!)
    }
    
    /**
     Method calculates time and removes 5 min from the appointment time to show the remainder before appointment
     */
    func calculateTimeToShowRemainder() {
        print("\(#function) called")
        let currentTime =  Date()
        let toTime = stringToDateConverter(date: dataModal.scheduled_at)
        let seconds = toTime.timeIntervalSince(currentTime)
        let timeToshow = seconds - 300
        print("date should show loacl notification local:\(convertDate(dateString: dataModal.scheduled_at).0) utc:\(dataModal.scheduled_at)")
        let calendar = Calendar(identifier: .indian)
        let reducedDate = calendar.date(byAdding: .minute, value: -5, to: convertDate(dateString: dataModal.scheduled_at).1)
        print("Reduced date:\(reducedDate!)")
        let components = calendar.dateComponents(in: .current, from: reducedDate!)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        
        
        //        let day = Int(timeToshow) / 86400
        //        let hours = Int(timeToshow) % 86400 / 3600
        //        let minutes = Int(timeToshow) / 60 % 60
        //        var dateComponents = DateComponents()
        //
        //        print("Days:\(day) hours;\(hours) minutes:\(minutes)")
        //        dateComponents.day = day
        //        dateComponents.hour = hours
        //        dateComponents.minute = minutes
        //        dateComponents.second = 0
        
        print("Time to show notification:\(timeToshow)")
        let selectedDate = dataModal.scheduled_at!
        let selectedTime = selectedDate.components(separatedBy: " ")
        let time = "\(getFormattedDate(dateString: dataModal.scheduled_at!)) at \(getTimeInAMPM(date:selectedTime[1]))"
        UNUserNotificationCenter.current().delegate = self
        schedulteAppointmentRemainder(timeInterVal: newComponents, time: time)
    }
    
    /**
     Method setups actions for notification remainder
     */
    func setupActionsForNotification() {
        let dismissAction = UNNotificationAction(identifier: NotificationActionIdentifiers.DismissAction , title: "Dismiss", options: UNNotificationActionOptions.destructive)
        let viewAction = UNNotificationAction(identifier: NotificationActionIdentifiers.ViewAction , title: "View", options: UNNotificationActionOptions.foreground)
        let notificationCategory = UNNotificationCategory(identifier: NotificationCategoryIdentifiers.AlertCategory , actions: [dismissAction,viewAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([notificationCategory])
    }
    
    /**
     Method setups local notification remainder with time interval 
      - Parameter timeInterVal: pass time or date components to schedule remainder 
      - Parameter time: to show time in notification
     */
    func schedulteAppointmentRemainder(timeInterVal : DateComponents , time:String) {
        setupActionsForNotification()
        print("\(#function) called--")
        let content = UNMutableNotificationContent()
        content.title = "Appointment remainder"
        content.body = "You have an appointment with DocOnline at \(time). Make sure you are logged in and internet connection is working properly. Thank You"
        content.categoryIdentifier = "AlertCategory"
        content.userInfo = ["local_appointment_id":"\(dataModal.id!)"]
        content.sound = UNNotificationSound.default
        
        // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterVal, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: timeInterVal, repeats: false)
        
        let requestIdentifier = "AppointmentAlert\(dataModal.id!)"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
                print("Notification scheduling error:\(error?.localizedDescription)")
            }else{
                print("Appointment notification scheduled successfully")
            }
        }
        //App.selectedSlotTime = ""
    }

    ///used to test local notification **Not used**
    @IBAction func notifyTapped(_ sender: UIButton) {
        var dateComponents = DateComponents()
        dateComponents.second = 5
        setupActionsForNotification()
        schedulteAppointmentRemainder(timeInterVal: dateComponents, time: "1:00 pm")
    }
    
    
    @IBAction func actionTapped(_ sender: UIBarButtonItem) {
        let actionSheet = UIAlertController(title: "", message: "Do you want to add this appointment to calendar?", preferredStyle: .actionSheet)
        let addToCalendar = UIAlertAction(title: "Add To Calendar", style: .default) { (action) in
          
            if let scheduledAt = self.dataModal.scheduled_at {
                let toTime = self.stringToDateConverter(date: scheduledAt)
                var eventAlreadyExists = false
                CalendarManager.shared.requestAuthorization() { (allowed) in
                    if allowed {
                        
                        CalendarManager.shared.getEvents(startDate: toTime, endDate: toTime.addingTimeInterval(15 * 60 * 1)) { (error, existingEvents) in
                            
                            for singleEvent in existingEvents! {
                                if singleEvent.title == "DocOnline" && singleEvent.startDate == toTime && singleEvent.endDate == toTime.addingTimeInterval(15 * 60 * 1) {
                                    self.didShowAlert(title: "Oops !", message: "Event has already been added to your calendar")
                                    eventAlreadyExists = true
                                    break
                                }
                            }
                            
                            if !eventAlreadyExists
                            {
                                CalendarManager.shared.createEvent(completion: { (event) in
                                    guard let event = event else { return }
                                    
                                    event.startDate = toTime
                                    event.endDate = event.startDate.addingTimeInterval(15 * 60 * 1)
                                    
                                    event.title = "DocOnline"
                                    event.notes = "Appointment with doctor"
                                    event.location = "Consultation"
                                    
                                    CalendarManager.shared.saveEvent(event: event) { (saveError) in
                                        
                                        if (saveError == nil)
                                        {
                                            self.didShowAlert(title: "Event Successfully Added", message: "Your appointment schedule has been added to your calendar successfully")
                                        }
                                        
                                    }
                                    
                                })
                            }
                        }
                    }
                    else
                    {
                        self.didShowAlert(title: "Calendar", message: "Grant access to calendar")
                    }
                }
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Cancel tapped")
        }
        actionSheet.addAction(addToCalendar)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIBarButtonItem) {
        dataModal = nil
        doctorModal = nil
        selectedImage = nil
        bookingID = nil
        selectedTimeSlot = ""
        bookingType = ""
        App.bookingAttachedImages.removeAll()
//        if App.isFromPaymentSuccessPage {
//           self.instantiate_to_view(withIdentifier: StoryBoardIds.ID_HOME_NAVIGATION)
//           App.isFromPaymentSuccessPage = false
//        }else {
           self.performSegue(withIdentifier: StoryboardSegueIDS.ID_GOTO_HOME, sender: sender)
//        }
    }
    
    // MARK: - Navigation

//    override func willMove(toParentViewController parent: UIViewController?) {
//         self.performSegue(withIdentifier: StoryboardSegueIDS.ID_GOTO_HOME, sender: self)
//    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
//        if segue.identifier == StoryboardSegueIDS.ID_IMAGE_VIEW {
//            if let destVC = segue.destination as? ImageViewController {
//                if App.ImagesArray.count == 0 {
//                    destVC.imageURl = self.selectedAttachment
//                }else{
//                    destVC.image = self.selectedImage
//                }
//            }
//        }else if segue.identifier == "AppointmentSummaryView" {
//            let destVC = segue.destination as! AppointmentSummaryViewController
//            destVC.appointmetnID = String(dataModal.id!)
//        }
    }
    
    //MARK:- Add upcoming appointment to calendar
    @IBAction func calendarTapped(_ sender: UIButton) {
   
        if let scheduledAt = self.dataModal.scheduled_at {
            let toTime = self.stringToDateConverter(date: scheduledAt)
            var eventAlreadyExists = false
            CalendarManager.shared.requestAuthorization() { (allowed) in
                if allowed {
                    
                    CalendarManager.shared.getEvents(startDate: toTime, endDate: toTime.addingTimeInterval(15 * 60 * 1)) { (error, existingEvents) in
                        
                        for singleEvent in existingEvents! {
                            if singleEvent.title == "DocOnline" && singleEvent.startDate == toTime && singleEvent.endDate == toTime.addingTimeInterval(15 * 60 * 1) {
                                self.didShowAlert(title: "Oops !", message: "Event has already been added to your calendar")
                                eventAlreadyExists = true
                                break
                            }
                        }
                        
                        if !eventAlreadyExists
                        {
                            CalendarManager.shared.createEvent(completion: { (event) in
                                guard let event = event else { return }
                                
                                event.startDate = toTime
                                event.endDate = event.startDate.addingTimeInterval(15 * 60 * 1)
                                
                                event.title = "DocOnline"
                                event.notes = "Appointment with doctor"
                                event.location = "Consultation"
                                
                                CalendarManager.shared.saveEvent(event: event) { (saveError) in
                                    
                                    if (saveError == nil)
                                    {
                                        self.didShowAlert(title: "Event Successfully Added", message: "Your appointment schedule has been added to your calendar successfully")
                                        self.vw_addToCalendarButton.isHidden = true
                                    }
                                    
                                }
                                
                            })
                        }
                    }
                }
                else
                {
                    self.didShowAlert(title: "Calendar", message: "Grant access to calendar")
                }
            }
        }
    }
}

extension BookingSuccessViewController : UNUserNotificationCenterDelegate {
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        completionHandler([.alert,.sound])
//    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "DismissAction":
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["AppointmentAlert"])
            
        case "ViewAction":
            // UIApplication.shared.cancelAllLocalNotifications()
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["AppointmentAlert"])
            self.performSegue(withIdentifier: "AppointmentSummaryView", sender: self)
            
//                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let vcToPresent = storyBoard.instantiateViewController(withIdentifier: "AppSummary") as! AppointmentSummaryViewController
//                        let navigation = UINavigationController(rootViewController: vcToPresent)
//                        let cancelButton = UIBarButtonItem()
//                        cancelButton.title = "Cancel"
//                        cancelButton.action = #selector(BookingSuccessViewController.dismisView)
//                        navigation.navigationItem.leftBarButtonItem = cancelButton
//                        self.present(navigation, animated: true, completion: nil)
            
        default:
            print("Something wrong in notification action")
        }
         print("LocalNotifiResponse:\(response)")
    }
    
    
}


extension BookingSuccessViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if App.ImagesArray.count == 0 {
            return App.imagesURLString.count
        }
        return App.ImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TV_CV_CELL_IDENTIFIERS.CELL, for: indexPath) as! ImagesCollectionViewCell
        
        if App.ImagesArray.count == 0 {
            //print("Image:=\(self.attachments[indexPath.row])")
            cell.iv_image.kf.setImage(with: URL(string: App.imagesURLString[indexPath.row])!)
            
        }else {
            cell.iv_image.image = App.ImagesArray[indexPath.row]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if App.ImagesArray.count == 0 {
            self.selectedAttachment = App.imagesURLString[indexPath.row]
            print("Image:=\(App.imagesURLString[indexPath.row])")
            
        }else {
            self.selectedImage = App.ImagesArray[indexPath.row]
        }
        self.performSegue(withIdentifier: StoryboardSegueIDS.ID_IMAGE_VIEW, sender: self)
    }
}

extension BookingSuccessViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 25
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/3, height: 100)
    }
}


extension UIViewController {
    /**
     Method creates body with parameters of uploading image
     */
    func createBodyWithParametersForCapetionedImage(parameters: [ImagesCollectionViewCellModal],boundary: String) -> NSData {
        let body = NSMutableData()
        
        if parameters.count != 0 {
            for (index,image) in parameters.enumerated() {
                if let picture = image.picture {
                    if let picCaption = picture.picCaption {
                        let titleKey = "titles[\(index)]"
                        body.appendString(string: "--\(boundary)\r\n")
                        body.appendString(string: "Content-Disposition: form-data; name=\"\(titleKey)\"\r\n\r\n")
                        body.appendString(string: "\(picCaption)\r\n")
                    }
                    
                    if picture.type?.rawValue == FileType.file.rawValue {
                        if let fileURlString = picture.fileURl ,let fileURl = URL(string:fileURlString) ,let data = NSData(contentsOf: fileURl) as Data? {
                            let filename = fileURl.lastPathComponent   //should change id to app id
                            let mimetype = mimeTypeForPath(path: fileURlString)
                            let key = "attachments[\(index)]"
                            body.appendString(string: "--\(boundary)\r\n")
                            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                            body.append(data)
                            body.appendString(string: "\r\n")
                            print("File data after converting:\(data)")
                        }
                    }
                    
                    if picture.type?.rawValue == FileType.image.rawValue {
                        if let pic = picture.pic {
                            let filename = "Appointment_attachments_\(index).jpg"   //should change id to app id
                            let data = pic.jpegData(compressionQuality: 0.4);
                            let mimetype = mimeTypeForPath(path: filename)
                            let key = "attachments[\(index)]"
                            body.appendString(string: "--\(boundary)\r\n")
                            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                            body.append(data!)
                            body.appendString(string: "\r\n")
                        }
                    }
                }
            }
        }
        body.appendString(string: "--\(boundary)--\r\n")
        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
        return body
    }
    
    /**
     Method creates body with parameters of uploading image
     */
    func createBodyWithParameters(parameters: [UIImage],boundary: String) -> NSData {
        let body = NSMutableData()
        
        if parameters.count != 0 {
//            for (key, value) in parameters! {
//                body.appendString("--\(boundary)\r\n")
//                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                body.appendString("\(value)\r\n")
//            }
            var i = 0;
            for image in parameters {
                let filename = "Appointment_attachments_\(i).jpg"   //should change id to app id
                let data = image.jpegData(compressionQuality: 0.4);
                let mimetype = mimeTypeForPath(path: filename)
                let key = "attachments[\(i)]"
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                body.append(data!)
                body.appendString(string: "\r\n")
                i += 1;
            }
            
        }
        body.appendString(string: "--\(boundary)--\r\n")
        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
        return body
    }
    
    /**
     Method genereates boundary string
     */
//    func generateBoundaryString() -> String {
//        return "Boundary-\(NSUUID().uuidString)"
//
//    }
    
    ///returns the mime string type
//    func mimeTypeForPath(path: String) -> String {
//        let pathUrl = NSURL(string: path)
//        let pathExtension = pathUrl!.pathExtension
//        var stringMimeType = "application/octet-stream"
//        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as! CFString, nil)?.takeRetainedValue() {
//            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
//                stringMimeType = mimetype as NSString as String
//            }
//        }
//        return stringMimeType;
//    }
    
    /**
     Method uploads images to server with parameters
     - Parameter id: appoitment id should be passed
     - Parameter tag: to know from which class it is called
     */
    func uploadImagesToServer(id:Int,tag:Int) {
        let defaults = UserDefaults.standard
        if App.bookingAttachedImages.count != 0 {
            
            if !NetworkUtilities.isConnectedToNetwork()
            {
                self.didShowAlert(title: "Network Error", message: AlertMessages.NETWORK_ERROR)
                // AlertView.sharedInsance.showFailureAlert(title: "Network Error" , message: AlertMessages.NETWORK_ERROR)
                return
            }
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StartAnimating"), object: nil)
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let boundary = generateBoundaryString()
            let urlString = "\(AppURLS.URL_BookAppointment)/\(id)/attachments"
            print("ImageUrl = \(urlString)")
            let url = URL(string: urlString)
            var request = URLRequest(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT)
            request.timeoutInterval = NETWORK_REQUEST_KEYS.REQUEST_TIME_OUT
            request.setValue(App.getUserAccessToken(), forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_AUTHORIZATION)
            // request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_FORM_URLENCODED, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_CONTENT_TYPE)
            //request.setValue(NETWORK_REQUEST_KEYS.KEY_APPLICATION_JSON, forHTTPHeaderField: NETWORK_REQUEST_KEYS.KEY_ACCEPT)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethods.POST
            request.httpBody = createBodyWithParameters(parameters: App.ImagesArray, boundary: boundary) as Data
            session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    self.didShowAlert(title: "", message: error.localizedDescription)
                    //  AlertView.sharedInsance.showFailureAlert(title: "", message: error.localizedDescription)
                }
                if let data = data
                {
                    print("data =\(data)")
                }
                if let response = response
                {
                    print("url = \(response.url!)")
                    print("response = \(response)")
                    let httpResponse = response as! HTTPURLResponse
                    print("response code = \(httpResponse.statusCode)")
                    //if you response is json do the following
                    do
                    {
                        let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        let errorStatus = self.check_Status_Code(statusCode: httpResponse.statusCode , data: resultJSON)
                        if !errorStatus {
                            print("performing error handling uploading consultation images:\(resultJSON)")
                        }
                        else {
                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopAnimating"), object: nil)
                            let status = resultJSON.object(forKey: Keys.KEY_STATUS) as! String
                            //clear images array after successfull upload **
                            if httpResponse.statusCode == 201 || httpResponse.statusCode == 200{
                                App.ImagesArray.removeAll()
                                App.imagesURLString.removeAll()
                                if tag == 4 {
                                    if let data = resultJSON.object(forKey: Keys.KEY_DATA) as? [NSDictionary] {
                                        for image in data {
                                            let fileName = image.object(forKey: Keys.KEY_FILE_NAME) as! String
                                            App.imagesURLString.append(fileName)
                                        }
                                        
                                    }
                                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "StopingAnimating"), object: nil)
                                }
                            }else {
                                if tag == 4 {
                                 NotificationCenter.default.post(name:NSNotification.Name(rawValue: "FailedMessageView"), object: nil)
                                }
                            }
                            print("Images upload response:\(resultJSON)  status:\(status)")
                        }
                    }catch let error{
                        self.checkOnlyStatus(statusCode: httpResponse.statusCode)
                        print("Received not-well-formatted JSON : \(error.localizedDescription)")
                    }
                }
                
            }).resume()
            
        }else {
            print("User not logged in")
        }
    }
    
}

