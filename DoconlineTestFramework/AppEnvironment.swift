//
//  AppEnvironment.swift
//  DocOnline
//
//  Created by kiran kumar Gajula on 21/01/19.
//  Copyright © 2019 ConversionBug. All rights reserved.
//

import Foundation
//import Firebase


/// Release or Debug environment setup class
final class AppEnvironment {
    
    // MARK: - Properties
    private static var sharedInstance: AppEnvironment = {
        return AppEnvironment()
    }()
    
    class func shared() -> AppEnvironment {
        return sharedInstance
    }
    
    enum Environment {
        case debug(String,String,String) //Takes baseURL string, cleintid and client secret
        case staging
        case beta
        case production
        case demo
    }
  
    var baseURL = AppURLS.BaseURLS.Production
    var razorpayKey = RazorpayCredentials.liveRKey
    var chatType = MessageServer.PRODUCTION
//    var clientId = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.CLIENT_ID).stringValue!
//    var clientSecret = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.CLIENT_SECRET).stringValue!
    var clientId = "3"
    var clientSecret = "test"
    
    var environment: Environment = .demo {
        didSet {
            switch environment {
            case .debug(let localURL, let clientId, let clientSecret):
                self.configValues(baseURL: localURL, razorpayKey: RazorpayCredentials.stagingRKey, chatType: MessageServer.LOCAl, cleintId: clientId, clientSecret: clientSecret)
                
            case .staging:
                self.configValues(baseURL: AppURLS.BaseURLS.Staging, razorpayKey: RazorpayCredentials.stagingRKey, chatType: MessageServer.STAGING, cleintId: "3", clientSecret: "test")
                
            case .beta:
                self.configValues(baseURL: AppURLS.BaseURLS.Beta, razorpayKey: RazorpayCredentials.stagingRKey, chatType: MessageServer.BETA, cleintId: "3", clientSecret: "test")
                
            case .demo:
            self.configValues(baseURL: AppURLS.BaseURLS.Demo, razorpayKey: RazorpayCredentials.stagingRKey, chatType: MessageServer.DEMO, cleintId: "3", clientSecret: "test")
                
            case .production:
//                let cid = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.CLIENT_ID).stringValue!
                let cid = "2"

//                let csecret = RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.CLIENT_SECRET).stringValue!
                let csecret = "CKjZKNQMfIWGWQCaaYWECsV5sV0qqDA9wuvalI9b"

                self.configValues(baseURL: AppURLS.BaseURLS.Production, razorpayKey: RazorpayCredentials.liveRKey, chatType: MessageServer.PRODUCTION, cleintId: cid, clientSecret: csecret)
            
            case .demo:
                self.configValues(baseURL: AppURLS.BaseURLS.Demo, razorpayKey: RazorpayCredentials.stagingRKey, chatType: MessageServer.DEMO, cleintId: "3", clientSecret: "test")
            }
            
            
            print("\n\n######## Environment Setup ########\n Base URL: \(self.baseURL) \n Razorpay Key:\(self.razorpayKey) \n ChatType:\(self.chatType) \n Client Id: \(self.clientId) \n Client Secret:\(self.clientSecret) \n ######## END #####\n\n")
        }
    }
    
    func configValues(baseURL:String,razorpayKey:String,chatType:String,cleintId:String,clientSecret:String) {
        self.baseURL = baseURL
        self.razorpayKey = razorpayKey
        self.chatType = chatType
        self.clientId = cleintId
        self.clientSecret = clientSecret
    }
    
}
